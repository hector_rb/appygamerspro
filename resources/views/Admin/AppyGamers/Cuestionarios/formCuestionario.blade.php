<div class="panel panel-default">
                <div class="panel-heading">Sube un archivo de Excel</div>
 
                @if (Session::has('success-message'))
                    <div class="alert alert-success">{{ Session::get('success-message') }}</div>
                @endif
 
                @if (Session::has('error-message'))
                    <div class="alert alert-danger">{{ Session::get('error-message') }}</div>
                @endif
 
                <div class="panel-body">
                        <div class="form-group">
                            <div class="btn" style="padding-left: 8%">
                                <h3>Descargar plantilla</h3>
                                 <a href="{{asset('archivos-excel/PLANTILLA_CUESTIONARIO.xlsx')}}" class="btn btn-block btn-info compose-mail">Descargar</a>
                            </div>
                            <input type="file" class="form-control" name="file" >
                        </div>
                </div>
            </div>
 