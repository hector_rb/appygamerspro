@extends('Admin.menuAdmin')
@section('content')
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">

<style>
    .tooltip-inner{max-width:600px;background: #2E2E2E;}
    .a{background-color: #F2F2F2;color: #81BEF7;}
    .b{color: #EB984E;}
    .c{color: #04B4AE;}
    .d{color: #34495E;}
    .e{color: #566573;}
    .f{color: #138D75;}
    /* css juegos */
    .card {box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;}
    .card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}
    .container {padding: 2px 16px;}
</style>
</head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Editar cuestionario
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes dar de alta preguntas para la AppyGames." aria-describedby="popover955887"></i> 
                    </small></h1>
                     <h3 class="font-bold c">En esta sección se puede agregar preguntas a los juegos</h3>
            </center>
        </form>
    </div>




@foreach ($cuestionario as $cuestionario)
   {{Form::Open(array('action'=>array('adminCuestionarioController@validarCasos'),'method'=>'post'))}} 
<div class="ibox-content">
    <div class="form-group">
                <div class="row">
                    
                   <div class="col-lg-6" >

                        <input type="hidden" name="id_cuestionario" value="{{$cuestionario->id_cuestionario}}">
                        <div class="col-sm-12" style="padding-left: 30%;padding-bottom: 2%;padding-top: 10%">
                            <h1 class="font-bold b" style="font-size:40px">{{$cuestionario->nombre}}</h1>
                        </div>

                        <div class="col-sm-6">
                          <script type="text/javascript">
                                    switch({{$cuestionario->nivel}})
                                    {
                                       case 1:
                                       $cat="Preescolar";
                                       break;
                                       case 2 :
                                       $cat="Primaria";
                                       break;
                                       case 3 :
                                       $cat= "Secundaria";
                                       break;
                                       case 4 :
                                       $cat= "Preparatoria";
                                       break;
                                       default:
                                    }
                                </script>
                                 <div class="col-sm-12" style="margin-left:45%">
                                       <h3 class="font-bold c" style="font-size: 25px">{{$cuestionario->grado}}° de <script type="text/javascript">document.writeln($cat)</script></h3>
                                 </div>
                        </div>

                        


                    </div>

                    <div class="col-lg-6">

                        <div class="col-sm-12" style="padding-bottom: 1.5%">
                            <label class="control-label">Categoria:</label>
                            {{ Form::select('materia', ['1' => 'Español', '2' => 'Matemáticas','3' => 'Historia', '4' => 'Arte','5' => 'Música','6' => 'Ciencias Naturales','7' => 'Geografía','8' => 'Tecnología','9' => 'Cívica y Ética','10' => 'Deportes', ], $cuestionario->categoria,['class' => 'form-control', 'required','id' => 'materia'] )}}
                        </div>

                        <div class="col-md-12" style="padding-bottom: 1.5%">
                            <label class="control-label">Descripción:</label>
                            <textarea required name="descripcion" style="width: 100%"> {{$cuestionario->descripcion}}</textarea>
                        </div>
                        
                        <div class="col-md-6">
                             <label class="control-label">Fecha inicio:</label>
                             <input  required type="date"  id=date name="fInicio" value="{{$cuestionario->fecha_inicio}}">
                        </div>

                        <div class="col-md-6">
                              <label class="control-label">Fecha termino:</label>
                              <input  required id="date" type="date" name="fFinal" value="{{$cuestionario->fecha_fin}}">
                        </div>
                        <div class="col-md-12">
                            <br>
                             <!-- <button type="submit" class="btn btn-w-n btn-info">Guardar</button> -->
                             {!! Form::submit( 'Guardar', ['class' => 'btn btn-w-n btn-info', 'name' => 'submitbutton', 'value' => 'general'])!!}
                        </div>
                    
                    </div>
            <br>
            <br>
            </div>
            
            
            <script>
                $valorCategoria=$('#materia').val();
                $("#materia").change(function(){
                $valorCategoria=$('#materia').val();
                document.getElementById("materia").value  = $valorCategoria;
                });
            </script>


           
             <div id ="preCues" class="row" style="padding-top: 2%;padding-left: 1.3%;padding-right: 1.3%; ">
              <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
                        <label class="control-label">Preguntas registradas en el cuestionario:</label>
                         <br>
                        <div class="table-responsive">
                             <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable2">
                                 <thead>
                                     <th>Pregunta</th>
                                     <th>Categoría</th>
                                     <th>Opción 1</th>
                                     <th>Opción 2</th>
                                     <th>Opción 3</th>
                                     <th>Opción 4</th>
                                     <th>Respuesta</th>
                                     <th>Eliminar</th>
                                </thead>
                             <tbody>
                            @foreach ($preguntas as $preguntas)
                            <tr>
                                <script>
                                    switch({{$preguntas->id_categoria}})
                                    {
                                       case 1:
                                       $cat="Español";
                                       break;
                                       case 2 :
                                       $cat="Matemáticas";
                                       break;
                                       case 3 :
                                       $cat= "Historia";
                                       break;
                                       case 4 :
                                       $cat= "Arte";
                                       break;
                                       case 5 :
                                       $cat= "Música";
                                       break;
                                       case 6 :
                                       $cat= "Ciencias Naturales";
                                       break;
                                       case 7 :
                                       $cat= "Geografía";
                                       break;
                                       case 8 :
                                       $cat= "Tecnología";
                                       break;
                                       case 9 :
                                       $cat= "Cívica y Ética";
                                       break;
                                       case 10 :
                                       $cat= "Deportes";
                                       break;
                                       default:
                                    }
                                </script>
                                
                                
                                
                                <td>{{$preguntas->pregunta}}</td>
                                <td> <script>document.writeln($cat)</script></td>
                                <td>{{$preguntas->opcion1}}</td>
                                <td>{{$preguntas->opcion2}}</td>
                                <td>{{$preguntas->opcion3}}</td>
                                <td>{{$preguntas->opcion4}}</td>
                                <td>{{$preguntas->respuesta}}</td>
                                <input type="hidden" name="id_pregu" value="{{$preguntas->id_pregunta}}">
                                <input type="hidden" name="id_cuestionario" value="{{$cuestionario->id_cuestionario}}">
                                <td><button type="button" class="btn btn-w-n- btn-danger" onclick="eliminarPregunta({{$preguntas->id_pregunta}})">Eliminar</button></td>
                                
                            </tr>
                            @endforeach
                             </tbody>
                             </table>
                         </div>
                     </div>
                 </div> 
             </div>
        </div>


    
</div>
</div>
@endforeach
<br>
<div class="ibox-content">
    <div class="row">
    <br>
    <div class="well well-sm text-center d" style="background-color:#ffffff">
        <h1 class="font-bold b" style="font-size:30px">Agregar Preguntas</h1>
        <label class="font-bold c">Preguntas registradas: </label>
        <input type="checkbox" id="PreRegis" name="registradas">
        <label class="font-bold c">Agregar nuevas preguntas: </label>
        <input type="checkbox" id="PreAgre" name="nuevas">
    </div>
</div>

<div class="ibox-content" id ="registradas" class="row" style="padding-top: 2%;display: none">
         
              <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
                        <label class="control-label">Preguntas registradas:</label>
                         <br>
                        <div class="table-responsive">
                             <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                                 <thead>
                                     <th>Pregunta</th>
                                     <th>Opción 1</th>
                                     <th>Opción 2</th>
                                     <th>Opción 3</th>
                                     <th>Opción 4</th>
                                     <th>Respuesta</th>
                                     <th>Seleccionar</th>
                                </thead>
                            <tbody id="myBody" >
                             </tbody>
                             </table>
                         </div>
                     </div>
                 </div> 
             </div>
             <input type="hidden" name="id_cuestionario" value="{{$cuestionario->id_cuestionario}}">
             <div style="padding-left: 92%;">
                 <!-- <button type="submit" class="btn btn-w-n btn-info">Guardar</button>-->
                  {!! Form::submit( 'Guardar', ['class' => 'btn btn-w-n btn-info', 'name' => 'submitbutton', 'value' => 'agregadas'])!!}
            </div>
        </div>


</div>

   


<div class="ibox-content" id="trash" style="display:none">
            <div class="row">
                
                 <label class="control-label">Agregar nuevas preguntas:</label>
                         <br><br>
                        
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Pregunta:</label>
                            <input type="text" name="pregunta[]" id="pregunta[]" class="form-control" required="false" style="width: 300px;" maxlength="88">
                        </div>
                    </div>

                    <div class="col-sm-8">
                       <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 1:</label>
                        <input type="text" name="op1[]" id="op1[]" class="form-control" required="false" style="width: 300px;" maxlength="20">
                      </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 2:</label>
                        <input type="text" name="op2[]" id="op2[]" class="form-control" required="false" style="width: 300px;" maxlength="20">
                    </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 3:</label>
                        <input type="text" name="op3[]" id="op3[]" class="form-control" required="false" style="width: 300px;" maxlength="20">
                    </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 4:</label>
                        <input type="text" name="op4[]" id="op4[]"  class="form-control" required="false" style="width: 300px;" maxlength="20">
                    </div>
                    </div>
                    <div class="col-sm-8">
                        <label class="col-sm-3 control-label">Respuesta:</label>
                         <div class="col-sm-4" >
                          <div class="btn-group" data-toggle="buttons" style="width: 200px;">
                            <label class="btn btn-info perso">
                                <input type="radio" value="1" name="respuesta0" id="idTipoInsignia" >
                                <H1>1</H1>
                            </label>
                            <label class="btn btn-info perso">
                                <input type="radio" value="2" name="respuesta0" id="idTipoInsignia" >
                                <H1>2</H1>
                            </label>
                            <label class="btn btn-info perso">
                                <input type="radio" value="3" name="respuesta0" id="idTipoInsignia" >
                               <H1>3</H1>
                            </label>
                            <label class="btn btn-info perso">
                                <input type="radio" value="4" name="respuesta0" id="idTipoInsignia" >
                               <H1>4</H1>
                            </label>
                           </div>
                       </div>
                   </div>
                    
                    <input type="hidden" name="categoria"       value="{{$cuestionario->categoria}}" id="categoria">
                    <input type="hidden" name="nivel"           value="{{$cuestionario->nivel}}" id="nivel">
                    <input type="hidden" name="grado"           value="{{$cuestionario->grado}}" id="grado">
                    <input type="hidden" name="institucion"     value="{{$id}}" id="id_inst">
                    <input type="hidden" name="id_cuestionario" value="{{$cuestionario->id_cuestionario}}" id="id_cuestionario">
 <div id="pregunta">  
                    </div>
                    <button type="button" name="nuevo" id="nuevo" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Agregar pregunta</button>
                    <button type="button" name="quitar" id="quitar" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Quitar pregunta</button>
                   <!-- <button type="submit" class="btn btn-w-n btn-info">Guardar</button> -->
                    {!! Form::submit( 'Guardar', ['class' => 'btn btn-w-n btn-info', 'name' => 'submitbutton', 'value' => 'nuevas'])!!}
{{Form::Close()}}
            </div>
</div> 



<script>
var idCat=$("#materia").val();
         $.ajax({
            method: 'GET',
            url: "{{ URL::to('admin/getPreguntasCategoria') }}",
            data: {Cat:idCat},
            success: function (data) 
            {
                $.each(data, function (i, item) {
                  $('#myTable').append('<tr><td>' + item.pregunta + '</td><td>' + item.opcion1 + '</td><td>' + item.opcion2 + '</td><td>' + item.opcion3 + '</td><td>' + item.opcion4 + '</td><td>' + item.respuesta + '</td><td><input type="checkbox" name="preguntaR[]" value="' +  item.id_pregunta + '"</td></tr>');
                      
                });
            }
         });

    $('#materia').change(function ()
    {
        $('#myBody').empty();
        var idCat=$(this).val();
         $.ajax({
            method: 'GET',
            url: "{{ URL::to('admin/getPreguntasCategoria') }}",
            data: {Cat:idCat},
            success: function (data) 
            {
                $.each(data, function (i, item) {
                  $('#myTable').append('<tr><td>' + item.pregunta + '</td><td>' + item.opcion1 + '</td><td>' + item.opcion2 + '</td><td>' + item.opcion3 + '</td><td>' + item.opcion4 + '</td><td>' + item.respuesta + '</td><td><input type="checkbox" name="preguntaR[]" value="' +  item.id_pregunta + '"</td></tr>');
                      
                });
            }
         });
    });
</script>

<script>
    var pregunta=0;

        $("#selectTipo").change(function(){
                    var idNivel = this.value;

                    $('#selectGrupo').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "/MyAppCollege/dashboard/admin/{{('getGruposPreguntas')}}",
                        data: {id:idNivel},
                        dataType: 'json',
                        success: function (data) {

                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                
                                var op =$('<option>',{
                                    value: item.idgrupo,
                                    text: item.grado 
                                });
                                $('#selectGrupo').append(op);
                                
                           
                                });
                            }
                        });
       });
        $('#nuevo').click(function()
        { 
        pregunta=pregunta+1;
        console.log(pregunta);
        if(pregunta<20)
        {
         $("#pregunta").val(pregunta);
        var salto=$('<br>');
        var div6=$('<div>', {class:'ibox float-e-margins',id:'opciones'+pregunta});
        var lCat=$('<label>', {class:'col-sm-3 control-label',text:'Categoria:'});
        var lPre=$('<label>', {class:'col-sm-3 control-label',text:'Pregunta:'});
        var vPre=$('<input>', {name: 'pregunta[]',class:'form-control',required:'false',});
        var lOp1=$('<label>', {class:'col-sm-3 control-label',text:'Opción 1:'});
        var vOp1=$('<input>', {name: 'op1[]',class:'form-control',required:'false',id:'idTipoInsignia',});
        var lOp2=$('<label>', {class:'col-sm-3 control-label',text:'Opción 2:'});
        var vOp2=$('<input>', {type: 'text',name: 'op2[]',class:'form-control',required:'false',id:'idTipoInsignia',});
        var lOp3=$('<label>', {class:'col-sm-3 control-label',text:'Opción 3:'});
        var vOp3=$('<input>', {name: 'op3[]',class:'form-control',required:'false',id:'idTipoInsignia',});
        var lOp4=$('<label>', {class:'col-sm-3 control-label',text:'Opción 4:'});
        var vOp4=$('<input>', {name: 'op4[]',class:'form-control',required:'false',id:'idTipoInsignia',});
        var lRes=$('<label>', {class:'col-sm-3 control-label',text:'Respuesta:'});
        var div7=$('<div class="btn-group" data-toggle="buttons">', {class: 'btn-group'});
        var div8=$('<label>', {class:'btn btn-info perso',});
        var v1=$('<input>', { name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '1',});
            var h11=$('<h1>', {text:'1' });
        var div9=$('<div>', {class:'btn btn-info perso',});
        var v2=$('<input>', { name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '2',});
        var h12=$('<h1>', { text:'2',});
        var div10=$('<div>', {class:'btn btn-info perso',});
        var v3=$('<input>', {name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '3',});
        var h13=$('<h1>', { text:'3',});
        var div11=$('<div>', {class:'btn btn-info perso',});
        var v4=$('<input>', { name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '4',});
        var h14=$('<h1>', {text:'4',});



        div6.append(salto);
        div6.append(salto);
        div6.append(lPre);
        div6.append(vPre);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp1);
        div6.append(vOp1);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp2);
        div6.append(vOp2);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp3);
        div6.append(vOp3);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp4);
        div6.append(vOp4);
        div6.append(salto);
        div6.append(salto);
        div6.append(lRes);

        div8.append(h11);
        div8.append(v1);
        div7.append(div8);
        div6.append(div7);

        div9.append(h12);
        div9.append(v2);
        div7.append(div9);
        div6.append(div7);

        div10.append(h13);
        div10.append(v3);
        div7.append(div10);
        div6.append(div7);

        div11.append(h14);
        div11.append(v4);
        div7.append(div11);
        div6.append(div7);

        $('#pregunta').append(div6);
        }
        else
        {
            pregunta=6;
        }
        });

        $('#quitar').click(function()
        {
            
                $('#'+'opciones' + pregunta).remove();
                pregunta=pregunta-1;
                $("#pregunta").val(pregunta);
                if(pregunta<=0)
                {
                    pregunta=0;
                    $("#pregunta").val(pregunta);
                }

        });

</script>

<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
<!-- Tags Input -->
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<!--swA-->
<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js')}}"></script> 
<script type="text/javascript">
    $(document).ready(function()
    {
    $('[data-toggle="tooltip"]').tooltip();   
    });
</script>   

<script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>        
<script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
<!-- jQuery UI -->
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<!-- Full Calendar -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>   

<script>
    document.getElementById("pregunta[]").required=false;
    document.getElementById("op1[]").required = false;
    document.getElementById("op2[]").required = false;
    document.getElementById("op3[]").required = false;
    document.getElementById("op4[]").required = false;
    document.getElementById("idTipoInsignia").required = false;
    
</script>

<script>
    $('input[type="checkbox"]').click(function () { 
        if(document.getElementById('PreRegis').checked === false && document.getElementById('PreAgre').checked === false)
        {
            $("#registradas").hide();
            $("#trash").hide();
        }

        if(document.getElementById('PreRegis').checked === true && document.getElementById('PreAgre').checked === false)
        {
            $("#registradas").show();
            $("#trash").hide();
        }

        if(document.getElementById('PreAgre').checked === true && document.getElementById('PreRegis').checked === false)
        {
            $("#registradas").hide();
            $("#trash").show();
            document.getElementById("pregunta[]").required = true;
            document.getElementById("op1[]").required = true;
            document.getElementById("op2[]").required = true;
            document.getElementById("op3[]").required = true;
            document.getElementById("op4[]").required = true;
            document.getElementById("idTipoInsignia").required = true;
        }
        
        if(document.getElementById('PreAgre').checked === true && document.getElementById('PreRegis').checked === true)
        {
            $("#registradas").show();
            $("#trash").show();
             document.getElementById("pregunta[]").required = true;
            document.getElementById("op1[]").required = true;
            document.getElementById("op2[]").required = true;
            document.getElementById("op3[]").required = true;
            document.getElementById("op4[]").required = true;
            document.getElementById("idTipoInsignia").required = true;
        }
        });
</script>

<script>
    function eliminarPregunta(id){
	var route = '/pregunta/'+id+'/borrar';
	
	
	var token = $('#token').val();
	
    var datos = {
           id_cuestionario: $('#id_cuestionario').val(),
           _token: '{{csrf_token()}}'
     };
         
	$.ajax({
		url: route,
 		type: 'DELETE',
		data: datos,
		dataType: 'json',
	}).done(function(response){
		if(response == "Exito"){
		          swal({
                        title: 'Pregunta Eliminada',
                        text: "",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Continuar'
                       }).then((result) => {
                                             location.reload();})
		}else if(response == "Error"){
			swal('Algo salió mal');
		}
	}).fail(function(response){
		swal('Algo salió mal');
    });
}
</script>
</body>
@endsection

