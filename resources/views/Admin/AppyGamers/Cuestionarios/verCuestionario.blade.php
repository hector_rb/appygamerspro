@extends('Admin.menuAdmin')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
<style>
    .tooltip-inner{max-width:600px;background: #2E2E2E;}
    .a{background-color: #F2F2F2;color: #81BEF7;}
    .b{color: #EB984E;}
    .c{color: #04B4AE;}
    .d{color: #34495E;}
    .e{color: #566573;}
    .f{color: #138D75;}
    /* css juegos */
    .card {box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;}
    .card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}
    .container {padding: 2px 16px;}
    .titulo{ color:#5AADB7; font-size:15px; font-weight: bold;}
    .cuerpo{font-size:15px;}
    .product-desc{padding:5px;margin:-1%;padding-left:-0.5%;}
</style>
</head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Cuestionarios registrados
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes dar de alta preguntas para la AppyGames." aria-describedby="popover955887"></i> 
                    </small></h1>
                     <h3 class="font-bold c">En esta sección se puede agregar preguntas a los juegos</h3>
            </center>
        </form>
    </div>
    
<div class="ibox-title">
    <div class="well well-sm text-center d">
        <h3 class="font-bold">Búsqueda de cuestionario</h3>
            <p>Ingrese la información requerida para buscar el cuestionario</p>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label">Nombre:</label>
                <input type="text" name="nombre" id="nombre_id" class="form-control" maxlength="20">
            </div>
            <div class="col-sm-4">
                <label class="control-label">Nivel:</label>
                <select class="form-control" required name="idSubGrupo" id="selectTipo">
                   <option value="">Selecciona un nivel</option>
                       @foreach ($niv as $niv)
                    <option value="{{$niv->idNivel}}">{{$niv->nombreNivel}}</option>
                       @endforeach
                </select>
            </div>

            <div class="col-sm-4">
                <label class="control-label">Grado:</label>
                <br>
                <select class="form-control" class="form-control" name="selectGrupo" id="selectGrupo" >
                    <option disabled selected value=""></option>
                    <option value=""></option>
                </select>
            </div>
            </div>
        <div class="row" style="padding-top:2%">
             <div class="col-sm-4">
                <label class="control-label">Categoria:</label>
                <select name="materia" class="form-control" id="materia">
                    <option value="">Selecciona una categoria</option>
                    @foreach($cat as $cat)
                    <option value="{{$cat->id_categoria}}">{{$cat->nombre}} </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                 <label class="control-label">Fecha inicio:</label>
                 <br>
                 <input type="date" class="form-control" id="dateI" name="fInicio">
            </div>
            <div class="col-md-4">
                <label class="control-label">Activo:</label>
                <select name="activo" class="form-control" id="activo">
                    <option value="">Selecciona una opción</option>
                    <option value="1">Activo</option>
                    <option value="0">Inactivo</option>
                </select>
            </div>
        </div>
        <div class="row" style="padding-top:2%;padding-left:1%">
           <button type="button" class="btn btn-w-m btn-info" onclick="buscarCuestionario()">Buscar</button>
       </div>
       
    </div>
</div>
<br>
<br>
<div id="contenedor">
  <div class="ibox-title">
    <div class="well well-sm text-center d">
        <h3 class="font-bold">Cuestionarios Registrados</h3>
    </div>
                        <div class="table-responsive">
                             <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable2">
                                 <thead>
                                     <th>Nombre</th>
                                     <th>Descripción</th>
                                     <th>Grado</th>
                                     <th>Inicio</th>
                                     <th>Finalización</th>
                                     <th>Editar</th>>
                                </thead>
                             <tbody>
                            <tr>
                            @foreach ($cuestionario as $cuestionario)
                                <td>{{$cuestionario->nombre}}</td>
                                <td>{{$cuestionario->descripcion}}</td>
                                <td>{{$cuestionario->grado}}</td>
                                <td>{{$cuestionario->fecha_inicio}}</td>
                                <td>{{$cuestionario->fecha_fin}}</td>
                                
                                <td><a href="{{URL::to('admin/editarCuestionario')}}?id={{$cuestionario->id_cuestionario}}" class="btn btn-w-m btn-info">Editar</a></td>
                            </tr>
                            @endforeach
                             </tbody>
                             </table>
                         </div>
                         </div>
      
    
<!--
<div class="col-md-6">
<div class="ibox ">
    <div class="ibox-title" style="background-color:#E39E41;">
        <p style="color:#FFFFFF;font-size:20px;margin:-2%;font-weight: bold">{{$cuestionario->nombre}}</p>
        <div class="ibox-tools">
            <a class="collapse-link">
            <i class="fa fa-chevron-up" style="color:#FFFFFF"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"></a>
            <ul class="dropdown-menu dropdown-user"></ul></a>
        </div>
    </div>
    <div class="ibox-content" hidden>
        {{Form::Open(array('action'=>array('adminCuestionarioController@editarCuestionario','id' => $cuestionario->id_cuestionario),'method'=>'get'))}}
        <div>
              <p><span style="color:#01afc3;font-size:18px">Descripción: </span></p>
              <p><span style="font-size:18px">{{$cuestionario->descripcion}}</span></p>    
        </div>
        <div>
            <p><span style="color:#01afc3;font-size:18px;padding-right:20%">Grado: </span><span style="color:#01afc3;font-size:18px;padding-right:23%">Inicio: </span></span><span style="color:#01afc3;font-size:18px">Finalización: </span></p>
            <p><span style="font-size:18px;padding-right:30%">{{$cuestionario->grado}}</span><span style="font-size:18px;padding-right:15%">{{$cuestionario->fecha_inicio}}</span><span style="font-size:18px">{{$cuestionario->fecha_fin}}</span></p>    
        </div>
         <button type="submit" class="btn btn-w-m btn-info">Editar</button>
         
         {{Form::Close()}}
    </div>
</div>
</div>
-->
<!-- 
<div class="col-md-4" style="border-radius: 15px">
                    <div class="ibox"  style="padding-left:2%">
                        <div class="ibox-content product-box">
                            <div class="product-desc" style="background-color:#E39E41">
                                <h2 style="color:#FFFFFF">{{$cuestionario->nombre}}</h2>
                                <p style="color:#FFFFFF">{{$cuestionario->descripcion}}</p>
                                </div>
                                <div class="product-desc" style="background-color:#FFFFFF;">
                                <div class="small m-t-xs">
                                    <p><span class="titulo">Fecha inicio:</span>     <span class="cuerpo">{{$cuestionario->fecha_inicio}}</span></p>
                                    <p><span class="titulo">Fecha Finalización:</span>   <span class="cuerpo">{{$cuestionario->fecha_fin}}</span></p>
                                    <p><span class="titulo">Grado : </span>               <span class="cuerpo">{{$cuestionario->grado}}</span></p>
                                </div>
                                   <button type="button" class="btn btn-w-m btn-info">Editar</button>
                            </div>
                        </div>
                    </div>
                </div>
-->



</div>
<div id="cont-busqueda" style="visibility:hidden">
    <div class="ibox-title">
        <div class="well well-sm text-center d">
             <h3 class="font-bold">Resultados de la búsqueda</h3>
        </div>
        <div id="busqueda">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                    <thead>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Grado</th>
                        <th>Inicio</th>
                        <th>Finalización</th>
                        <th>Editar</th>>
                     </thead>
                     <tbody id="tbody">
                     </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    $("#selectTipo").change(function(){
                    var idNivel = this.value;

                    $('#selectGrupo').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getGruposPreguntas')}}",
                        data: {id:idNivel},
                        dataType: 'json',
                        success: function (data) {

                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                
                                var op =$('<option>',{
                                    value: item.idgrupo,
                                    text: item.grado 
                                });
                                $('#selectGrupo').append(op);
                                
                           
                                });
                            }
                        });
       });
</script>
<script>
    function buscarCuestionario(){
     $("#tbody").empty();
	var route = '/buscarCuestionario';
	var cont = document.getElementById("contenedor");
	var contBus = document.getElementById("cont-busqueda");
	var bus = document.getElementById("busqueda");
	var tabla= document.getElementById("myTable");
    var datos = {
           nombre: $('#nombre_id').val(),
           grado: $('#selectGrupo').val(),
           categoria: $('#materia').val(),
           fecha_inicio: $('#dateI').val(),
           fecha_fin: $('#dateF').val(),
           activo: $('#activo').val(),
     };
	$.ajax({
		url: route,
 		type: 'GET',
		data: datos,
		dataType: 'json',
	}).done(function(response){
	    
	  if(response.length==0)
	  {
	      swal('No se encontraron resultados');
	  }
	  else
	  {
	    $.each(response, function(i, val){
        cont.style.display="none";
        contBus.style.visibility ="visible";
        });
        $.each(response, function(i, val){
            
             /*<td><a href="{{URL::to('admin/editarCuestionario')}}?id={{$cuestionario->id_cuestionario}}" class="btn btn-w-m btn-info">Editar</a></td>*/
             
            var tr = $("<tr>");
            
            
            
            var nombre = $("<td>",{
               text: val.nombre 
            });
            
            var descripcion = $("<td>",{
               text: val.descripcion 
            });
            
            var grado = $("<td>",{
               text: val.grado 
            });
            
            var inicio = $("<td>",{
               text: val.fecha_inicio 
            });
            
            var final = $("<td>",{
               text: val.fecha_fin 
            });
            
            
            tr.append(nombre)
            tr.append(descripcion)
            tr.append(grado)
            tr.append(inicio)
            tr.append(final)
            
            
            
            
            var a =  $("<a>", {
                href: "{{ URL::to('admin/editarCuestionario') }}" + "?id=" + val.id_cuestionario,
                class: "btn btn-w-m btn-info",
                text: "Editar",
                style: "margin: 8px;"
             });
             
             tr.append(a);
            
            $('#myTable tbody').append(tr);
        });
	  }
	}).fail(function(response){
	    
	    console.log(response);
		swal('Algo salió mal');
    });
}
</script>


<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
<!-- Tags Input -->
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<!--swA-->
<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js')}}"></script> 
<script type="text/javascript">
    $(document).ready(function()
    {
    $('[data-toggle="tooltip"]').tooltip();   
    });
</script>   

<script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>        
<script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
<!-- jQuery UI -->
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<!-- Full Calendar -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>    
</body>
@endsection

