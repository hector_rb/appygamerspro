@extends('Admin.menuAdmin')
@section('content')
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
        /* css juegos */
                 .card {
                 /* Add shadows to create the "card" effect */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                }

                /* On mouse-over, add a deeper shadow */
                .card:hover {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }

                /* Add some padding inside the card container */
                .container {
                    padding: 2px 16px;
                }
</style>
</head>
<body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Catálogo de juegos 
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes consultar los juegos disponibles" aria-describedby="popover955887"></i> 
                    </small></h1>
                    <h3 class="font-bold c">En esta sección se puede manejar todo lo relacionado con los juegos</h3>
            </center>
        </form>
    </div>
@foreach($juego as $jue)
@if($loop->iteration % 4 == 0)
<div class="row">
@endif
 <div class="col-md-4">
            <div class="ibox">
                        <div class="ibox-content product-box">

                            <div class="product-imitation">
                           <img src="{{asset($jue->imagen)}}" alt="Avatar" style="max-height:150px">
                            </div>
                            <div class="product-desc">
                               <class="product-name"><h2> {{$jue->nombre}}</h2></a>
                                <div class="small m-t-xs">
                                  <p style="font-size:15px;text-align: justify;">{{$jue->descripcion}}</p>
                                    <form action="{{ url('admin/juegoDetalle') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="text" value="{{$jue->nombre}}" name="juego" hidden>
                                      <button type="submit" class="btn btn-primary">Detalles</button>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
@if($loop->iteration % 4 == 0)
</div>
@endif
@endforeach

</div>
</body>
@endsection
