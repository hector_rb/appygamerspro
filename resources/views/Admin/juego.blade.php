@extends('Admin.menuAdmin')
@section('content')
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
        /* css juegos */
                 .card {
                 /* Add shadows to create the "card" effect */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                }

                /* On mouse-over, add a deeper shadow */
                .card:hover {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }

                /* Add some padding inside the card container */
                .container {
                    padding: 2px 16px;
                }
            </style>
</head>
<body>
    <br><br>
        @foreach($juego as $juego)
    <div class="ibox-content">
        <br><br>
        <div class="row">
            <div class="col-md-4"  style="padding-top: 1%">
                <center>
                 <img src="{{asset($juego->icono)}}" style="max-height:200px">
                </center>
            </div>
            <div class="col-md-8" style="padding-bottom: 5%;">
                <p style="font-size: 60px;text-align: left;color:#000000">{{$juego->nombre}}</p>
                <p style="font-size: 20px">{{$juego->descripcion}}</p>
                <a href={{$juego->linkAndroid}}><img src="../games/google.png" style="max-height:50px" /></a>        
                <a href={{$juego->linkIos}}><img src="../games/ios.png" style="max-height:52px" /></a>               
            </div>
            </div> 
        </div>
<br><br>
        <div class="row">
            <div class="col-md-3">
                <img src="{{asset($juego->cap1)}}" style="max-height:500px">
            </div>
            <div class="col-md-3">
                <img src="{{asset($juego->cap2)}}" style="max-height:500px">
            </div>
            <div class="col-md-3">
                <img src="{{asset($juego->cap3)}}" style="max-height:500px">
            </div>
            <div class="col-md-3">
                <img src="{{asset($juego->cap4)}}" style="max-height:500px">
            </div>
        </div>
        <div class="row" style="padding-left: 9%;">
            <div class="col-md-6">
                
        </div>
    </div>
@endforeach
</body>
@endsection
