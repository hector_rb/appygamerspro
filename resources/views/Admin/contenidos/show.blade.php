@extends('Admin.menuAdmin')

@section('content')
<html>
	<head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
        <style>
      #map {
        height: 400px;
        width: 85%;
       }
       .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         
    </style>
	</head>
    <body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Contenidos
                </h1>
                <h3 class="font-bold c">Appy College te brinda contenidos, donde contamos con un conjunto de libros para ayuda a nuestros usuarios de AppyCollege</h3>
            </center>
        </form>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" id="ibox1">
                        
                        <div class="ibox-content">
                            
                            <h3>{{$contenido->titulo}}</h3>
                            <p>
                                {{$contenido->descripcion}}
                            </p>
                            
                        </div>

                    </div>
                </div>
            </div>
            <div class="text-center pdf-toolbar">

                <div class="btn-group">
                    <button id="prev" class="btn btn-white prev"><i class="fa fa-long-arrow-left"></i> <span class="hidden-xs">Anterior</span></button>
                    <button id="next" class="btn btn-white next"><i class="fa fa-long-arrow-right"></i> <span class="hidden-xs">Siguiente</span></button>
                    <button id="zoomin" class="btn btn-white"><i class="fa fa-search-minus"></i> <span class="hidden-xs">Zoom +</span></button>
                    <button id="zoomout" class="btn btn-white"><i class="fa fa-search-plus"></i> <span class="hidden-xs">Zoom -</span> </button>
                    <button id="zoomfit" class="btn btn-white"> 100%</button>
                    <span class="btn btn-white hidden-xs">Página: </span>

                    <div class="input-group">
                        <input type="text" class="form-control" id="page_num">

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-white" id="page_count">/ 22</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="text-center m-t-md">
                <canvas id="the-canvas" class="pdfcanvas border-left-right border-top-bottom b-r-md"></canvas>
            </div>
            <div class="text-center pdf-toolbar">

                <div class="btn-group">
                    <a href="#next"><button class="btn btn-white prev"><i class="fa fa-long-arrow-left"></i> <span class="hidden-xs">Anterior</span></button></a>
                    <a href="#next"><button class="btn btn-white next"><i class="fa fa-long-arrow-right"></i> <span class="hidden-xs">Siguiente</span></button></a>
                    

                </div>
            </div>



        </div>
        <!--swA-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
        
        <script src="{{asset('js/plugins/pdfjs/pdf.js')}}"></script>
        <script id="script">
            //
            // If absolute URL from the remote server is provided, configure the CORS
            // header on that server.
            //
            var anexo
            var url = '../../contenidos/' + <?=json_encode($contenido->anexo)?>;


            var pdfDoc = null,
                    pageNum = 1,
                    pageRendering = false,
                    pageNumPending = null,
                    scale = 1,
                    zoomRange = 0.25,
                    canvas = document.getElementById('the-canvas'),
                    ctx = canvas.getContext('2d');

            /**
             * Get page info from document, resize canvas accordingly, and render page.
             * @param num Page number.
             */
            function renderPage(num, scale) {
                pageRendering = true;
                // Using promise to fetch the page
                pdfDoc.getPage(num).then(function(page) {
                    var viewport = page.getViewport(scale);
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    // Render PDF page into canvas context
                    var renderContext = {
                        canvasContext: ctx,
                        viewport: viewport
                    };
                    var renderTask = page.render(renderContext);

                    // Wait for rendering to finish
                    renderTask.promise.then(function () {
                        pageRendering = false;
                        if (pageNumPending !== null) {
                            // New page rendering is pending
                            renderPage(pageNumPending);
                            pageNumPending = null;
                        }
                    });
                });

                // Update page counters
                document.getElementById('page_num').value = num;
            }

            /**
             * If another page rendering in progress, waits until the rendering is
             * finised. Otherwise, executes rendering immediately.
             */
            function queueRenderPage(num) {
                if (pageRendering) {
                    pageNumPending = num;
                } else {
                    renderPage(num,scale);
                }
            }

            /**
             * Displays previous page.
             */
            function onPrevPage() {
                if (pageNum <= 1) {
                    return;
                }
                pageNum--;
                var scale = pdfDoc.scale;
                queueRenderPage(pageNum, scale);
            }
            var prev=document.getElementsByClassName('prev');
            for (i = 0; i < prev.length; i++) {
                prev[i].addEventListener('click', onPrevPage);
            }

            /**
             * Displays next page.
             */
            function onNextPage() {
                if (pageNum >= pdfDoc.numPages) {
                    return;
                }
                pageNum++;
                var scale = pdfDoc.scale;
                queueRenderPage(pageNum, scale);
            }
            var next=document.getElementsByClassName('next');
            
            for (i = 0; i < next.length; i++) {
                next[i].addEventListener('click', onNextPage);
            }
            /**
             * Zoom in page.
             */
            function onZoomIn() {
                if (scale >= pdfDoc.scale) {
                    return;
                }
                scale += zoomRange;
                var num = pageNum;
                renderPage(num, scale)
            }
            document.getElementById('zoomin').addEventListener('click', onZoomIn);

            /**
             * Zoom out page.
             */
            function onZoomOut() {
                if (scale >= pdfDoc.scale) {
                    return;
                }
                scale -= zoomRange;
                var num = pageNum;
                queueRenderPage(num, scale);
            }
            document.getElementById('zoomout').addEventListener('click', onZoomOut);

            /**
             * Zoom fit page.
             */
            function onZoomFit() {
                if (scale >= pdfDoc.scale) {
                    return;
                }
                scale = 1;
                var num = pageNum;
                queueRenderPage(num, scale);
            }
            document.getElementById('zoomfit').addEventListener('click', onZoomFit);


            /**
             * Asynchronously downloads PDF.
             */
            PDFJS.getDocument(url).then(function (pdfDoc_) {
                pdfDoc = pdfDoc_;
                var documentPagesNumber = pdfDoc.numPages;
                document.getElementById('page_count').textContent = '/ ' + documentPagesNumber;

                $('#page_num').on('change', function() {
                    var pageNumber = Number($(this).val());

                    if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
                        queueRenderPage(pageNumber, scale);
                    }

                });

                // Initial/first page rendering
                renderPage(pageNum, scale);
            });
        </script>
    </body>
</html>
@endsection