@extends('Admin.menuAdmin')
@section('content')
<html>
<head>
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<link href="{{asset('css/animate.css')}}" rel="stylesheet">
<link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            color: #E1B16F;
            
        }

         .f{
            background-color: #42C4D1;
            color: #FFFFFF;
        }

         .azul{
            background-color: #42C4D1;
            color: #E0E4E5;
        }

         .gris{
            background-color: #E0E4E5;
            color: #42C4D1;
        }

</style>
<body>
<br>
<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Contenidos
                </h1>
                <h3 class="font-bold c">Appy College te brinda contenidos, donde contamos con un conjunto de libros para ayuda a nuestros usuarios.</h3>
            </center>
        </form>
</div>
<div class="row">
    <br>
    <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/contenidos')}}"> <i class="fa fa-book "></i>Libros<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/contenidos/videos')}}"> <i class="fa fa-tv "></i>Videos<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
                    <div class="ibox ">
                            <div class="col-lg-3"></div>
                            @include('Admin.contenidos.searchVideos')
                            <br>
                                <div class="col-lg-12">
                                        <div class="dd" id="nestable2">
                                            <ol class="dd-list">
                                                @foreach($categoria as $c)
                                                <li class="dd-item" data-id="{{$c->idCategoria}}">
                                                    
                                                    <div class="dd-handle">
                                                        <span class="label label-info"><i class="fa fa-tv "></i></span><strong>CATEGORIA: {{$c->nombre}}</strong>
                                                    </div>
                                                    
                                                    @foreach($videos as $v)
                                                    @if($v->idCategoria == $c->idCategoria)
                                                    <ol class="dd-list">
                                                        <li class="dd-item" data-id="2">
                                                            <div class="dd-handle">
                                                                <span class="pull-right"> 
                                                                    <a href="{{URL::to('admin/contenidos/vervideo?idvideo='.$v->idvideo)}}">
                                                                        <i class="fa fa-play fa-2x e"></i><label></label> </span></a>
                                                                <p><strong>{{$v->titulo}}</strong>|{{$v->descripcion}}</p>
                                                            </div>
                                                        </li>
                                                    </ol>
                                                    @endif
                                                    @endforeach
                                                </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                </div>
                        </div>
                    </div>
        </div>
</div>
<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Nestable List -->
<script src="{{asset('js/plugins/nestable/jquery.nestable.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('js/inspinia.js')}}"></script>
<script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>
<script>
         $(document).ready(function(){

             var updateOutput = function (e) {
                 var list = e.length ? e : $(e.target),
                         output = list.data('output');
                 if (window.JSON) {
                     output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                 } else {
                     output.val('JSON browser support required for this demo.');
                 }
             };
             // activate Nestable for list 1
             $('#nestable').nestable({
                 group: 1
             }).on('change', updateOutput);

             // activate Nestable for list 2
             $('#nestable2').nestable({
                 group: 1
             }).on('change', updateOutput);

             // output initial serialised data
             updateOutput($('#nestable').data('output', $('#nestable-output')));
             updateOutput($('#nestable2').data('output', $('#nestable2-output')));

             $('#nestable-menu').on('click', function (e) {
                 var target = $(e.target),
                         action = target.data('action');
                 if (action === 'expand-all') {
                     $('.dd').nestable('expandAll');
                 }
                 if (action === 'collapse-all') {
                     $('.dd').nestable('collapseAll');
                 }
             });
         });
    </script>

</body>
@endsection