{!! Form::open(array('url'=>'/admin/contenidos/videos', 'method'=>'GET', 'autocomplete' => 'off','role'=>'search' )) !!}
<div class="col-lg-6">
	<div class="form-group">
		<div class="input-group">
			<input type="text" class="form-control" name="searchText2" placeholder="Buscar Categoria, Titulo del video o descripción" value="{{$searchText2}}">
			<span class="input-group-btn">
			<button type= "submit" class="btn btn-info">Buscar</button>
			</span>
		</div>
	</div>
</div>
{{Form::close()}}