@extends('Admin.menuAdmin')
@section('content')
<html>
<head>
<link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

<link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #E1B16F;
            color: #FDFEFE;
        }

         .f{
            background-color: #42C4D1;
            color: #FFFFFF;
        }

        .div {
            width: 23%;
        }
</style>
<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Contenidos
                </h1>
                <h3 class="font-bold c">Appy College te brinda contenidos, donde contamos con un conjunto de libros para ayuda a nuestros usuarios.</h3>
            </center>
        </form>
</div>

<div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/contenidos')}}"> <i class="fa fa-book "></i>Libros<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/contenidos/videos')}}"> <i class="fa fa-tv "></i>Videos<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

<div class="col-lg-9">
<div class="product-imitation">
    <video src="{{asset('img/videos/'.$videos[0]->anexo)}}" width="640" height="360" controls  preload></video>
</div>
</div>

@endsection