@extends('Admin.menuAdmin')
@section('content')
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
 <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
        /* css juegos */
                 .card {
                 /* Add shadows to create the "card" effect */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                }

                /* On mouse-over, add a deeper shadow */
                .card:hover {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }

                /* Add some padding inside the card container */
                .container {
                    padding: 2px 16px;
                }
</style>
</head>

<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Registro de Alumno
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes dar de alta a los alumnos" aria-describedby="popover955887"></i> 
                    </small>
                </h1>
            </center>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12">                   
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h1>Registrar Alumno</h1>
                </div>
                <div class="ibox-content">
                    <div class="row">
                    <div class="col-md-6">
                         <form action="{{ url('admin/altaAlumno') }}" method="POST">
                        {{ csrf_field() }}

                       <div class="col-md-6">
                            <h2>Nivel:</h2>
                            <select class="form-control" required name="nivel" id="selectTipo">
                              <option value="">Selecciona un nivel</option>
                              @foreach ($niv as $niv)
                              <option value="{{$niv->idNivel}}">{{$niv->nombreNivel}}</option>
                              @endforeach
                            </select>
                        </div>

                        <div class="col-md-6">
                          <h2>Grado:</h2>
                          <select class="form-control" class="form-control" style="width:150px" name="grado" id="selectGrupo"  required="required" >
                          <option disabled selected value=""></option>
                          <option value=""></option>
                          </select>
                      </div>
                   

<br>
<br>


                        <h2>Nombre:</h2>
                         <input id="name" type="text" placeholder="Tu nombre" class="form-control" name="name" required> 
                         <h2>Apellido Paterno:</h2>  
                         <input type="text"  placeholder="Apellido Paterno" class="form-control" name="apepat">
                         <h2>Apellido Materno:</h2>
                         <input type="text" placeholder="Apellido Materno" class="form-control" name="apemat" >
                         <h2>Email:</h2>
                         <input id="email" type="email" placeholder="Email" class="form-control" name="email"required>
                         <h2>Contraseña:</h2>
                         <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>
                     </div>
                     <div class="col-md-6">
                        <h2>Fecha de nacimiento:</h2>
                        <input type="date" placeholder="Fecha de nacimiento: AAAA-MM-DD" class="form-control" name="fechanac">
                        <h2>Celular:</h2>
                        <input type="text" placeholder="Telefono celular" class="form-control" name="cel">
                        <h2>Género:</h2>
                        <select placeholder="Genero" class="form-control" name="genero" required>
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                        </select>
                         <h2>Tutor:</h2>
                         <select placeholder="tutor" class="form-control" name="tutor">
                            <option value="">Selecciona un tutor</option>
                              @foreach ($tutor as $tutor)
                              <option value="{{$tutor->idtutor}}">{{$tutor->nombreTutor .' '.$tutor->apepat.' '. $tutor->apemat}}</option>
                              @endforeach
                        </select>

                         <div class="col-md-6 col-md-offset-4" style="padding-top: 2.5%">
                             <button type="submit" class="btn btn-primary">Registrar</button>
                         </div>
                         </form>
                     </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
    
<script type="text/javascript">
     $("#selectTipo").change(function(){
                    var idNivel = this.value;

                    $('#selectGrupo').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getGruposPreguntas')}}",
                        data: {id:idNivel},
                        dataType: 'json',
                        success: function (data) {

                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                
                                var op =$('<option>',{
                                    value: item.idgrupo,
                                    text: item.grado 
                                });
                                $('#selectGrupo').append(op);
                                });
                            }
                        });
       });
</script>
</body>
@endsection