@extends('Admin.menuAdmin')

@section('content')
<html>
	<head>
		<title>AppyCollege | Registro</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
        <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

        <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
        <style>
    
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
      #map {
        height: 400px;
        width: 85%;
       }
       .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

       .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
        
         .e{
            background-color: #F0B27A;
            color: #F0F3F4;
        }
         .f{
            background-color: #76D7C4;
            color: #F0F3F4;
        }

         .g{
            background-color: #E6B0AA;
            color: #F0F3F4;
        }
         .h{
            color: #F2F4F4;
        }
        img
    {
        max-width: 100%;
    }
    </style>
	</head>
    <body>
        <div class="container">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="col-lg-11">
                <div class="tooltip-demo">
                    <form>
                        <center>
                            <h1 class="font-bold b">Administración 
                                <small>
                                    <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right" title="Aqui puedes guardar los datos de la institución, asi como el logo de la escuela y el baner." aria-describedby="popover955887"></i> 
                                </small>
                            </h1>
                            <h2 class="font-bold c">Registro de instituciones</h2>
                        </center>
                    </form>
                </div> 
            </div>
        </div>
    </div>
        
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" id="ibox1">
                            <div class="ibox-content">
                                <div class="form-group">
                                    <div class="tooltip-demo">
                                        <div class="well well-sm text-center g">
                                            <label>CÓDIGO DE REGISTRO 
                                                <small>
                                                    <i  class="fa fa-question-circle h" data-toggle="tooltip" data-placement="right" title="Código de registro para los profesores que puedan registrarse a la aplicación." aria-describedby="popover955887"></i> 
                                                </small>
                                            </label>
                                        </div>
                                            <form>
                                                <center>
                                                    <h1 class="font-bold">{{$array[0]->codregistro}}</h1>
                                                </center>
                                            </form>
                                    </div>
                                </div>
                            </div>
                            <br></br>
                        <div class="ibox-content">
                            @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                            
                            {!!Form::open(['method'=>'PATCH','route'=>['RegistroInstitucion.update', $array[0]->idInstitucion], 'class'=>'form-horizontal'])!!}
                            
                                <div class="well well-sm text-center f">
                                    <label>INFORMACIÓN DE LA INSTITUCIÓN</label>
                                </div>
                            

                                <div class="form-group">

                                	<label class="col-sm-2 control-label" >CLAVE</label>

                                    <div class="col-sm-3"><input type="text" name="clave" class="form-control" value="{{ $array[0]->clave}}"></div>
                                    <label class="col-sm-2 control-label">CENTRO EDUCATIVO</label>

                                    <div class="col-sm-5"><input type="text" name="nombre" class="form-control" value="{{ $array[0]->nombre}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >PAIS</label>

                                    <div class="col-sm-4"><select name="pais" class="form-control">
                                            <option value="{{ $array[0]->idpais}}">{{ $array[0]->nombrePais}}</option>
                                            @foreach($pais as $p)
                                            <option value="{{$p->idPais}}">{{$p->nombrePais}}</option>
                                            @endforeach
                                    </select></div>
                                    <label class="col-sm-2 control-label" >ESTADO</label>

                                    <div class="col-sm-4"><select name="estado" class="form-control">
                                            <option value="{{ $array[0]->idestado}}">{{ $array[0]->nombreEstado}}</option>
                                            @foreach($estado as $est)
                                            <option value="{{$est->idEstado}}">{{$est->nombreEstado}}</option>
                                            @endforeach
                                    </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >CIUDAD</label>

                                    <div class="col-sm-3"><input type="text" name="ciudad" class="form-control" value="{{ $array[0]->ciudad}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >PERSONA DE CONTACTO</label>

                                    <div class="col-sm-9"><input type="text" name="contacto" class="form-control" value="{{ $array[0]->contacto}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >TELEFONO</label>

                                    <div class="col-sm-4"><input type="text" name="telefono" class="form-control" value="{{ $array[0]->telefono}}"></div>
                                    <label class="col-sm-2 control-label">CORREO</label>

                                    <div class="col-sm-4"><input type="text" name="correo" class="form-control" value="{{ $array[0]->correo}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">DESCRIPCION</label>

                                    <div class="col-sm-10"><textarea rows="5" maxlength="500" style="resize:none; width: 100%" name="desc">{{ $array[0]->desc}}</textarea></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >DIRECCION</label>

                                    <div class="col-sm-9"><input type="text" name="direccion" class="form-control" id="direccion" value="{{ $array[0]->direccion}}"></div>
                                </div> 
                                <div class="form-group">
                                    <div class="col-sm-9"></div>
                                    <div class="col-sm-3 control-label">
                                    <button type="submit" name="guardar" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button></div>
                                </div>                                                         
                            {!!Form::close()!!}
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!--swA-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
        <script>
            $(document).ready(function() {
                $("form").bind("keypress", function(e) {
                    if (e.keyCode == 13) {
                        return false;
                    }
                });
            });
          //function initMap() {

            /*var uluru = {lat: <?= $array[0]->lat ?>, lng: <?= $array[0]->lng ?>};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 18,
              center: uluru
            });*/

            /*var marker = new google.maps.Marker({
              position: uluru,
              map: map
            });
          }*/

           function initAutocomplete() {
            var latitude=<?= json_encode($array[0]->lat)?>;
            var longitude=<?= json_encode($array[0]->lng)?>;
            if (latitude!=""&&longitude!="")
            {
                var uluru= {lat: parseFloat(<?= json_encode($array[0]->lat)?>), lng: parseFloat(<?= json_encode($array[0]->lng)?>)}
                console.log(uluru)
            }
            else
            {
                var uluru= {lat:22.149738, lng:-100.987120 }
            }
        var map = new google.maps.Map(document.getElementById('map'), {
          center: uluru ,
          zoom: 18,
          mapTypeId: 'roadmap'
        });
        var marker = new google.maps.Marker({
              position: uluru,
              map: map});
        var geocoder = new google.maps.Geocoder();
          

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");

              return;
            }
            var lat =document.getElementById('lat');
            lat.value=place.geometry.location.lat();
            var lng =document.getElementById('lng');
            lng.value=place.geometry.location.lng();
            marker.setPosition(place.geometry.location);
            marker.setAnimation(google.maps.Animation.DROP);

            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
              
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });

        google.maps.event.addListener(map, 'drag', function(){
            marker.setPosition(map.getCenter());
        });

            google.maps.event.addListener(map, 'dragend', function(){ 
            marker.setPosition(map.getCenter());
                geocoder.geocode( { 'location': marker.getPosition()}, function(results, status) {
                      if (status == 'OK') {
                        var lat=results[0].geometry.location.lat();
                        var lng=results[0].geometry.location.lng();
                        var dir=results[0].formatted_address;
                        var elem = document.getElementById("direccion"); // Get text field
                        elem.value = dir; // Change field
                        var elem = document.getElementById("lat"); // Get text field
                        elem.value = lat; // Change field
                        var elem = document.getElementById("lng"); // Get text field
                        elem.value = lng; // Change field
                       }
                   }); 
            });
      }
      //logo institucion
        var $image = $("#image")
        var cropper;
        $image.cropper({
            aspectRatio: 5 / 1,
            preview: "#preview",
           
            
            
            crop: function(e) {
                // Output the result data for cropping image.
                
                console.log(e.x);
                console.log(e.y);
                console.log(e.width);
                console.log(e.height);
                console.log(e.rotate);
                console.log(e.scaleX);
                console.log(e.scaleY);
            },            
        });
        var $inputImage = $("#inputImage2");
        if (window.FileReader) {
            $inputImage.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage.val();
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $('#saveImage').click(function(){

                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

                $image.cropper('getCroppedCanvas').toBlob(function (blob) {
                

                var formData = new FormData();
                var token = $("input[name=_token]").val();
                formData.append('croppedImage',$image.cropper('getCroppedCanvas',{
                  
                  height: 256,
                  fillColor: '#fff',
                  
                }).toDataURL("image/png"));
                formData.append("_token", token);
                formData.append("tipo", 1);
                
                // Use `jQuery.ajax` method
                $.ajax( {
                url:"{{('saveLogo')}}",
                type:'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    swal('Logo de la escuela guardado')
                },
                error: function (e) {
                console.log(e.responseJSON);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                swal('Ocurrio un error, no se almaceno el logo de la escuela')
                }
                });
            });
        });

               
        

        //Banner

        var $image2 = $("#image2")
        var cropper;
        $image2.cropper({
            aspectRatio: 5 / 3,
            preview: "#preview2",
            autoCropArea: 0.8,
            crop: function(e) {
                // Output the result data for cropping image.
                
                console.log(e.x);
                console.log(e.y);
                console.log(e.width);
                console.log(e.height);
                console.log(e.rotate);
                console.log(e.scaleX);
                console.log(e.scaleY);
            },            
        });
        var $inputImage2 = $("#inputImage");
        if (window.FileReader) {
            $inputImage2.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage2.val();
                        $image2.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage2.addClass("hide");
        }

        $('#saveImage2').click(function(){

                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

                $image2.cropper('getCroppedCanvas').toBlob(function (blob) {
                

                var formData = new FormData();
                var token = $("input[name=_token]").val();
                formData.append('croppedImage',$image2.cropper('getCroppedCanvas',{
                  fillColor: '#fff',
                  weight: 350,
                  
                }).toDataURL("image/png"));
                formData.append("_token", token);
                formData.append("tipo", 2);
                
                // Use `jQuery.ajax` method
                $.ajax( {
                url:"{{('saveLogo')}}",
                type:'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    swal('Imagen de Banner guardada')
                },
                error: function (e) {
                    console.log(e.responseJSON);
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    swal('Ocurrio un error, no se pudo guardar la im芍gen de banner')
                }
                });
            });
        });

          
        </script>
       

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSRCDpFoU9S-4i6xp15NXe9vujX-5WkW0&libraries=places&callback=initAutocomplete"
        async defer></script>
        
        
    </body>
</html>
@endsection