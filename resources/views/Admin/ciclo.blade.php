@extends('Admin.menuAdmin')

@section('content')
<body>
	 <div class="wrapper wrapper-content animated fadeInRight">
	        <div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox float-e-margins">
	                        <div class="ibox-title">
	                            <h2>Seleccionar ciclo escolar actual</h2>
	                             <div class="ibox-tools">
	                               <a href="{{URL::action('CicloController@create')}}"><button type="button" name="guardar" id="next" class="btn btn-success ">Nuevo Ciclo</button> </a>
	                        </div>
	                        </div>
	                        <div class="ibox-content">
	                            @if (count($errors)>0)
	                            <div class="alert alert-danger">
	                                <ul>
	                                @foreach ($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                                @endforeach
	                                </ul>
	                            </div>
	                            @endif
	                            {!!Form::open(['method'=>'GET','url'=>['admin/cicloCambiar'], 'class'=>'form-horizontal'])!!}
	                            

	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >NIVEL</label>

	                                    <div class="col-sm-8">
	                                    	<select class="form-control" id="selectNivel" name="nivel" required="required">
	                                    		<option></option>
	                                    		@foreach($niv as $n)
	                                            	<option value="{{$n->idNivel}}">{{$n->nombreNivel}}</option>
	                                            @endforeach
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                
	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >CICLO</label>

	                                    <div class="col-sm-8">
	                                    	<select name="ciclo" id="selectCiclo" class="form-control" required="required">
	                                    		<option> </option>
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                    <div class="col-sm-9"></div>
	                                    <div class="col-sm-3 control-label">
	                                    	<button type="submit" name="guardar" class="btn btn-success ">Seleccionar</button>
	                                    </div>
                                </div>                        
                            </form>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <script >
	        	$("#selectNivel").change(function(){
            var idNivel = this.value;


            $('#selectCiclo').find('option').not(':first').remove();

            
            $.ajax({
                method: 'get',
                url: "{{('getCiclo')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#selectCiclo').append($('<option>', {
                            value: item.idCiclo,
                            text : item.nombreCiclo
                        }));
                    });
                }
            });
        });
	        </script>
	    </body>
@endsection