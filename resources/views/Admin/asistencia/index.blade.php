@extends('Admin.menuAdmin')
@section('content')
<head>
     <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>
<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Asistencias
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="Selecciona Materia-Grupo-Nivel, y la fecha para consultar la asistencia de los alumnos." aria-describedby="popover955887"></i> 
                    </small></h1>
                <h3 class="font-bold c">Consulta de asistencias</h3>
            </center>
        </form>
    </div>
 <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-3 col-md-3 col-xs-12">
                            
                                <div class="tooltip-demo">
                                    <label for="idmateria">Materia-Grupo-Nivel
                                    </label>
                                </div>
                                    <select name="idSubgrupo" id="selectSubgrupo" class="form-control">
                                        <option value=""></option>
                                            @foreach ($sg as $sg)
                                                <option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
                                            @endforeach
                                    </select>
                            
                        </div>



                        <div class="col-lg-2 col-md-2 col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="numevaluaciones">Periodo</label>
                                    <select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
                                        <option></option>
                                    </select>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="fecha">Fecha</label>
                                    <input type="date" name="fecha" id="selectFecha" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-mail">

                                        <thead>
                                            <th>Id</th>
                                            <th>Alumno</th>
                                            <th><p>Asistencias</p></th>
                                            <th><p>Faltas</p></th>
                                            <th><p>Retardos</p></th>
                                        </thead>
                                        <tbody id="rows">

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            {!!Form::close()!!}
                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<script >
            $("#selectSubgrupo").change(function(){
                    var idsg = this.value;

                    $('#selectPeriodos').find('option').not(':first').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getPeriodosAdmin')}}",
                        data: {id:idsg},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var op =$('<option>',{
                                    value: item.idPeriodo,
                                    text: item.nombrePeriodo
                                });
                                
                                
                                $('#selectPeriodos').append(op);
                           
                                });
                            }
                        });
                    
                });
             $("#selectPeriodos").change(function(){
                    var idP = this.value;
                    var idsg=$('#selectSubgrupo').val();
                    
                    $('#rows').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getAsistencia')}}",
                        data: {idSubgrupo:idsg,periodo:idP},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var fila = $('<tr>');

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.A
                                });

                                var falta=$('<td>',{
                                    text:item.F
                                });

                                var ret=$('<td>',{
                                    text:item.R
                                });
                                
                                
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                fila.append(falta);
                                fila.append(ret);
                                
                                $('#rows').append(fila);
                           
                                });
                            }
                        });
                    $.ajax({
                        method: 'get',
                        url: "{{('getFechasAsistencia')}}",
                        data: {id:idsg,periodo:idP},
                        dataType: 'json',
                        success: function (data) {
                           $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var op =$('<input>',{
                                    value: item.fecha,
                                    text: item.fecha
                                });
                                
                                
                                $('#selectFecha').append(op);
                           
                                });
                            }
                        });
                });


             $("#selectFecha").change(function(){
                    var fecha= this.value;
                    var idP = $('#selectPeriodos').val();
                    var idsg=$('#selectSubgrupo').val();
                $('#rows').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getFechasAsistencia2')}}",
                        data: {idSubgrupo:idsg,periodo:idP,fecha:fecha},
                        dataType: 'json',
                        success: function (data) {

                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var fila = $('<tr>');

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.A
                                });

                                var falta=$('<td>',{
                                    text:item.F
                                });

                                var ret=$('<td>',{
                                    text:item.R
                                });
                                
                                
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                fila.append(falta);
                                fila.append(ret);
                                
                                $('#rows').append(fila);
                           
                                });
                            }
                        });


                });

        </script>
@endsection