@extends('Admin.menuAdmin')

@section('content')
<head>
	<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style type="text/css">
    	.tooltip-inner{
		    max-width:600px;
		}
		<style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
    </style>
</head>
<body>
	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Administración de Profesores
                    </h1>
                <h3 class="font-bold c">En esta sección se puede manejar todo lo relacionado con los profesores de tu escuela en AppyCollege</h3>
            </center>
        </form>
    </div>
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/registroProfesor')}}">Agregar profesor</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/adminUsuarios')}}"> <i class="fa fa-inbox "></i>Lista de profesores<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/adminUsuarios/ProfesoresEliminados')}}"> <i class="fa fa-trash-o "></i>Profesores eliminados<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
     <div class="col-lg-9 animated fadeInRight">   
	 <div class="wrapper wrapper-content animated fadeInRight">
	        <div class="row">
	                        <div class="ibox-content">	           
                            	<div class="table-responsive">
				                    <table class="table table-striped table-bordered table-hover dataTables-example" >
					                    <thead>
						                    <tr>
						                    	<th></th>
						                        <th>ID</th>
						                        <th>Profesor</th>
						                        <th>E-Mail</th>
						                        <th></th>
						                    </tr>
					                    </thead>
					                    <tbody id="gradosTable">
						                    @foreach($profs as $p)						
							                    	<tr>
							                    		<td><form><center><img src="{{asset('img/profile_pics/' .$p->imagen)}}" height="40" width="40" class="img-circle circle-border m-b-md" alt="profile"></center></form></td>
							                    		<td>{{$p->idprofesor}}</td>
							                    		<td>{{$p->nombreprof}} {{$p->apepat}} {{$p->apemat}} </td>
							                    		<td>{{$p->correo}}</td>
							                    		<td>
							                    			<a href="" data-target="#modal-delete-{{$p->idprofesor}}" data-toggle="modal" title="Reactivar"><button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Reactivar</button></a>
							                    			<!--<a href="{{URL::to('admin/adminUsuarios/reasignarMaterias?id='.$p->idprofesor)}}">
							                    			<button class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Reasignar Materias</button></a>-->
							                    		</td>
							                    	</tr>  
						                    	@include('Admin.registro.reactivar')         
						                    @endforeach         
						                    
					                    </tbody>
					                    
				                    </table>
		                        </div>
		                        <div class="form-horizontal" id="divGrados">
		                        	<div class="form-group">
		                        		<div class="col-sm-9"></div>
		                                <!--<div class="col-sm-3 control-label">
		                        			<a href="{{URL::to('admin/registroProfesor')}}"><button class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs" id="btnNGrado">Agregar Profesor</button></a>
		                        		</div>-->

		                        	</div>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>	        

	        </div>    
	        </div>

	        <script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
	         <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
	         <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
	         <!-- Tags Input -->
    		<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

    		<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
    		<script >
    			$(document).ready(function(){
	            $('.dataTables-example').DataTable({
	                pageLength: 25,
	                responsive: true,
	                dom: '<"html5buttons"B>lTfgitp',
	                language: {
						            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						        },
	                buttons: [

	                    {
	                     customize: function (win){
	                            $(win.document.body).addClass('white-bg');
	                            $(win.document.body).css('font-size', '10px');

	                            $(win.document.body).find('table')
	                                    .addClass('compact')
	                                    .css('font-size', 'inherit');
	                    }
	                    }
	                ]

	            });

	        });
    		</script>

@endsection