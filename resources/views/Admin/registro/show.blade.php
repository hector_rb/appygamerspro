@extends('Admin.menuAdmin')
@section('content')
<head>
	<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style type="text/css">
    	.tooltip-inner{
		    max-width:600px;
		}
		<style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

        .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #5499C7;
            color: #FDFEFE;
        }

        .f{
            background-color: #48C9B0;
            color: #FDFEFE;
        } 

        .g{
            background-color: #F0B27A;
            color: #FDFEFE;
        } 

        .h{
            background-color: #7DCEA0;
            color: #FDFEFE;
        } 

        .i{
            background-color: #E1B16F;
            color: #FDFEFE;
        }
    </style>
</head>
<body>
	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Administración de Profesores
                    </h1>
                <h3 class="font-bold c">En esta sección se puede manejar todo lo relacionado con los profesores de tu escuela en AppyCollege</h3>
            </center>
        </form>
    </div>
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/registroProfesor')}}">Agregar profesor</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/adminUsuarios')}}"> <i class="fa fa-inbox "></i>Lista de profesores<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/adminUsuarios/ProfesoresEliminados')}}"> <i class="fa fa-trash-o "></i>Profesores eliminados<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
     <div class="col-lg-9 animated fadeInRight"> 
     	<div class="wrapper wrapper-content">
		<div class="row animated fadeInDown">
			<div class="col-lg-3">

				<div class="ibox-content text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                
                            </h2>
                            </div>
                            <img src="{{asset('img/profile_pics/' .$profesor[0]->imagen)}}" height="130" width="130" class="img-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="widget-text-box">
                        	<form><center>
                            <h1 class="font-bold"></h1>
                            <span class="fa fa-calendar m-r-xs"></span>
                            <label>Fecha de nacimiento:</label>{{$profesor[0]->fechanac}}
                            <br><br>
                            <span class="fa fa-phone m-r-xs"></span>
                            <label>Teléfono:</label>{{$profesor[0]->telefono}}
                            <br>
                            <span class="fa fa-envelope m-r-xs"></span>
                            <label>Correo:</label>{{$profesor[0]->correo}}
                            </center></form>
                        </div>
			</div>
			<div class="col-lg-9">
				<div class="ibox-content">
					<div class="well well-sm text-center e"> 
			        	<label>DATOS PERSONALES</label>
			        </div>  						
	                     <div class="row vertical-align">
							<div class="col-xs-1">
							</div>
							<div class="col-xs-2">
                                <i class="fa fa-user fa-5x"></i>
                        	</div>
	                        <div class="col-xs-4">
								<label>Teléfono:</label>
								{{$profesor[0]->telefono}}
								<label>Célular:</label>
								{{$profesor[0]->celular}}
							</div>
							<div class="col-xs-4">
								<label>Género:</label>
								{{$profesor[0]->genero}}
								<label>Fecha de nacimiento:</label>
								{{$profesor[0]->fechanac}}
							</div>
						</div>
	            </div>
				<div class="ibox-content">
					<div class="well well-sm text-center d">
						<label>SECCIÓN ACADÉMICA</label>
					</div>
						<div class="table-responsive">
	                        <table class="table table-hover table-mail">
	                            <tbody>
	                                <thead>
                                        <th>Materias:</th>
                                        <th>Grado y Grupo:</th>
                                        <th>Nivel</th>
	                                </thead>
	                                @foreach ($profs as $p)
	                                <thead>
	                                	<td>{{$p->nombreMateria}} </td>
	                                	<td>{{$p->grado}} {{$p->grupo}} </td>
	                                	<td>{{$p->nombreNivel}}</td>
	                                </thead> 
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>  
	            </div>
			</div>
		</div>
	</div>  
     </div>
   </div>
</body>  
@endsection