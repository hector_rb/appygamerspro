@extends('Admin.menuAdmin')

@section('content')
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style type="text/css">
         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
    </style>
</head>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Registrar nuevo profesor
                    </h1>
                <h3 class="font-bold c">En esta sección puede manejar todo lo relacionado con los usuarios de tu escuela en AppyCollege</h3>
            </center>
        </form>
    </div>
        <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/registroProfesor')}}">Agregar profesor</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/adminUsuarios')}}"> <i class="fa fa-inbox "></i>Lista de profesores<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/adminUsuarios/ProfesoresEliminados')}}"> <i class="fa fa-trash-o "></i>Profesores eliminados<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-lg-9 animated fadeInRight">
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox-content">
                        <h4>Añade los datos del profesor.</h4>
                        <div class="row">

                            <form class="form-horizontal" method="POST" action="{{ route('registroProfesor') }}" id="form" >
                                        {{ csrf_field() }}

                
                                <div class="col-md-6">
                                    <div class="ibox-content">
                                        @if (count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        <br>
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                                <div class="col-md-12">
                                                    <input type="hidden" class="form-control" name="tipo_user" value="3" required>
                                                    <input type="hidden" class="form-control" name="admin" value="1" required>
                                                    <h4>Nombre:</h4>
                                                    <input id="name" type="text" placeholder="" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group" >

                                                <div class="col-md-12">
                                                    <h4>Apellido paterno:</h4>
                                                    <input type="text"  placeholder="" class="form-control" name="apepat" >
                                                </div>
                                            </div>

                                            <div class="form-group" >

                                                <div class="col-md-12">
                                                    <h4>Apellido materno:</h4>
                                                    <input type="text" placeholder="" class="form-control" name="apemat" >
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <div class="col-md-12">
                                                    <h4>Email del profesor:</h4>
                                                    <input id="email" type="email" placeholder="profesor@email.com" class="form-control" name="email" value="{{ old('email') }}" required>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <div class="col-md-12">
                                                    <h4>Contraseña:</h4>
                                                    <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-md-12">
                                                    <h4>Confirmar contraseña:</h4>
                                                    <input id="password-confirm" placeholder="Confirma contraseña" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                            </div>
                                            

                                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="ibox-content">
                                        <br>
                                        <div class="form-horizontal">
                                            {{ csrf_field() }}
                                        
                                        <div class="form-group">

                                                <div class="col-md-12">
                                                    <h4>Matrícula:</h4>
                                                    <input type="text"         placeholder="" class="form-control" name="matricula" required >
                                                </div>
                                        </div>
                                            
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                                <div class="col-md-12">
                                                    <h4>Fecha de nacimiento del profesor:</h4>
                                                    <input type="date" placeholder="Fecha de nacimiento: AAAA-MM-DD" class="form-control" name="fechanac" >
                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <div class="col-md-12">
                                                    <h4>Teléfono:</h4>
                                                    <input type="text" placeholder="" class="form-control" name="tel" required >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <h4>Teléfono celular del profesor:</h4>
                                                    <input type="text" placeholder="" class="form-control" name="cel">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <h4>Género</h4>
                                                    <select placeholder="Genero" class="form-control" name="genero" required>
                                                        <option value="M">Masculino</option>
                                                        <option value="F">Femenino</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group" id="oculto" style="display: none">

                                                <div class="col-md-12">
                                                    <input type="text" id="clave" placeholder="Clave de Registro"  class="form-control" name="clave" >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-9 col-md-offset-6">
                                                    <button type="submit"  class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">
                                                        Registrar a profesor
                                                    </button>
                                                </div>
                                            </div>
                                       </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <!-- <div class="row">
                <div class="col-md-6">
                    Copyright Example Company
                </div>
                <div class="col-md-6 text-right">
                   <small>© 2014-2015</small>
                </div>
            </div> -->
        </div>
    </div>
     <!--swA-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>

@endsection