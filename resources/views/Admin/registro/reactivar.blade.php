<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$p->idprofesor}}">
{{Form::Open(array('action'=>array('adminUsuariosController@reactivar', $p->idprofesor),'method'=>'post'))}}

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-Label="Close">
			<span aria-hidden="true">x</span>
			</button>
			<h4 class="modal-title">Reactivar profesor</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$p->idprofesor}}">
			<p>¿Desea reactivar al profesor <strong>{{$p->nombreprof}} {{$p->apepat}} {{$p->apemat}}?</strong></p>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-w-m btn-w-m btn-outline btn-default btn-xs" data-dismiss="modal"> Cerrar</button>
			<button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Confirmar</button>
			<p>&nbsp;</p>
			</div>
		</div>
	</div>
</div>

{{Form::Close()}}
</div>
</div>
