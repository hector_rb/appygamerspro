@extends('Admin.menuAdmin')

@section('content')
<head>
	<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style type="text/css">
    	.tooltip-inner{
		    max-width:600px;
		}
		<style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
    </style>
</head>
<body>
	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">¿Deseas eliminar a un profesor?
                    </h1>
                <h3 class="font-bold c">Para eliminar a el profesor tendras que reasignar sus materias.</h3>
            </center>
        </form>
    </div>
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/registroProfesor')}}">Agregar profesor</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/adminUsuarios')}}"> <i class="fa fa-inbox "></i>Lista de profesores<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/adminUsuarios/ProfesoresEliminados')}}"> <i class="fa fa-trash-o "></i>Profesores eliminados<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    {!!Form::open(array('url'=>'/admin/adminUsuarios','method'=>'POST','autocomplete'=>'off'))!!}
	{{Form::token()}}
     <div class="col-lg-9 animated fadeInRight">   
	 <div class="wrapper wrapper-content animated fadeInRight">
	        <div class="row">
	                        <div class="ibox-content">	      
		                        <div class="form-horizontal">
		                        	<h3 class="font-bold b">Reasignación de materias</h3>
	                        	</div>
	                        	<div class="table-responsive">
			                        <table class="table table-hover table-mail">
			                            <tbody>
			                                <thead style="background-color:#F79729; color:#FAF8F6">
		                                        <th>Materias:</th>
		                                        <th>Grado y Grupo:</th>
		                                        <th>Profesor que impartirá esta materia</th>
		                                        <input type="hidden" name="idprofesor" value="{{$id}}">
			                                </thead>
			                                
			                                @foreach ($prof as $p)
			                                <thead>
			                                	<td> 
			                                		<input type="hidden" name="idSubgrupo[]" value="{{$p->idSubgrupo}}">
			                                		<h5>{{$p->nombreMateria}}</h5></td>
			                                	<td><h5>{{$p->grado}} {{$p->grupo}} </h5></td>
			                                	<td><select class="form-control" name="profesor[]" required="true">
													<option value=""></option>
													@foreach ($sg as $s)
													@if($s->idprofesor != $id)
													<option value="{{$s->idprofesor}}">{{$s->nombreprof}} {{$s->apepat}} {{$s->apemat}}</option>
													@endif
													@endforeach
													</select></td>
			                                </thead>
			                                @endforeach
			                            </tbody>
			                        </table>
			                    </div>
			                    <div class="col-sm-2 control-label">
		                              <button  type="submit" name="enviar" id="enviar" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Eliminar profesor</button>
		                    	</div> 
		                    	<br></br> 
	                        </div>
	                    </div>
	                </div>
	            </div>	        

	        </div>    
	        </div>
	{!!Form::close()!!} 
	        <script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
	         <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
	         <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
	         <!-- Tags Input -->
    		<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

    		<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
    		<script >
    			$(document).ready(function(){
	            $('.dataTables-example').DataTable({
	                pageLength: 25,
	                responsive: true,
	                dom: '<"html5buttons"B>lTfgitp',
	                language: {
						            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						        },
	                buttons: [

	                    {
	                     customize: function (win){
	                            $(win.document.body).addClass('white-bg');
	                            $(win.document.body).css('font-size', '10px');

	                            $(win.document.body).find('table')
	                                    .addClass('compact')
	                                    .css('font-size', 'inherit');
	                    }
	                    }
	                ]

	            });

	        });
    		</script>

@endsection
