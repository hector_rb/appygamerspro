@extends('Admin.menuAdmin')
@section('content')
<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
        .azul{
            background: #5AADB7;
            color:#FDFDFC;
        }
        .naranja{
            background: #E39E41;
            color:#FDFDFC;
        }
        .blanco{
            background: #FDFEFE;
            color:#2C3E50;
        }
    </style>
</head>

<body>
    <div id="wrapper">

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            	<div class="col-lg-6">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title azul">
	                    	<h5>Detalles</h5>
	                        <div class="ibox-tools">
	                        </div>
	                    </div>
	                    	<div class="ibox float-e-margins">
				                <div class="ibox-content text-center p-md">
				                	<h2><span class="text-navy">Evento "{{$eventos->titulo}}"</span></h2>
				                    @if($eventos->tipoDestinatario==1)
			                        <h2>Entrada: General</h2>
			                        @endif
			                        @if($eventos->tipoDestinatario==2)
			                        <h2>Entrada: Nivel {{$nivel->nombreNivel}}</h2>
			                        @endif
			                        @if($eventos->tipoDestinatario==3)
			                        <h2>Entrada: Grado</h2>
			                        @endif
			                        @if($eventos->tipoDestinatario==4)
			                        <h2>Entrada: Grupos {{$grupo->grado}}{{$grupo->grupo}}</h2>
			                        @endif
			                        @if($eventos->tipoDestinatario==5)
			                        <h2>Entrada: Profesores</h2>
			                        @endif
			                    	<h2 class="col-lg-6">Inicio del evento<br>{{$eventos->inicio}}</h2>
		                    		<h2 class="col-lg-6">Fin del evento<br>{{$eventos->fin}}</h2>
		                    		<br><br><br><br><br><br><br>
				                </div>
				            </div>
	                    	
	                </div>
            	</div>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title azul">
                            <h5>Número de asistencias al evento</h5>

                        </div>
                        <div class="ibox-content">
                            <div>
                                <canvas id="doughnutChart" height="139"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title naranja">
	                        <h5>Asistencia al evento "{{$eventos->titulo}}"</h5>
	                        <div class="ibox-tools">
	                            <a class="collapse-link">
	                                <i class="fa fa-chevron-up naranja"></i>
	                            </a>
	                            <a class="close-link">
	                                <i class="fa fa-times naranja"></i>
	                            </a>
	                        </div>
	                    </div>
	                    <div class="ibox-content">

	                        <div class="table-responsive">
	                    <table class="table table-striped table-bordered table-hover dataTables-example" >
	                    <thead>
	                    <tr>
	                        <th>Tipo de usuario</th>
	                        <th>Nombre</th>
	                        <th>Asistencia</th>
	                    </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($calendario_asistencia as $ca)
		                    <tr class="gradeX">
		                    	@if($ca->tipo_user==2)
		                        <td><span class="label label-info">Tutor</span></td>
		                        @endif
		                        @if($ca->tipo_user==3)
		                        <td><span class="label label-succes">Profesor</span></td>
		                        @endif
		                        <td>{{$ca->name}}</td>
		                        @if($ca->asistencia==1)
		                        <td><i class="fa fa-check text-navy"></td>
		                        @endif
		                        @if($ca->asistencia==0)
		                        <td><i class="fa fa-times"></td>
		                        @endif
		                    </tr>
		                    @endforeach
	                    </table>
	                        </div>

	                    </div>
	                </div>
            	</div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <script src="{{ asset('js/plugins/dataTables/datatables.min.js')}}"></script>
    <!-- ChartJS-->
    <script src="{{ asset('js/demo/chartjs-demo.js')}}"></script>
    <script type="text/javascript">
    $(document).ready(function()
   	{
    	var id=<?=$eventos->idCalendario?>;
    	$.ajax({
        method: 'get',
        url: "{{('SiCalendario')}}",
        data: {idCalendario:id},
        dataType: 'json',
        success: function (data) {
        	var doughnutData = {
			        labels: ["Sí","No"],
			        datasets: [{
			            data: [data.si, data.no],
			            backgroundColor: ["#39CACA","#EFB032"]
			        }]
			    } ;


			    var doughnutOptions = {
			        responsive: true
			    };


			    var ctx4 = document.getElementById("doughnutChart").getContext("2d");
			    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
	        }
	    });
	});
    </script>
     <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>

</body>

</html>

@endsection