@extends('Admin.menuAdmin')

@section('content')
<html>
	<head>
    
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css" />
	<style>
    .swal-title {
  margin: 0px;
  font-size: 10px;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
  margin-bottom: 28px;
}
.naranja {
    background-color: #f8ac59;
    border-color: #f8ac59;
    color: #FFFFFF;
    padding: 10px; 
	}
	.verde{
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul1 {
    background-color: #1c84c6;
    border-color: #1c84c6;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul2 {
    background-color: #23c6c8;
    border-color: #23c6c8;
    color: #FFFFFF;
    padding: 10px; 
	}
	.rojo {
    background-color: #ed5565;
    border-color: #ed5565;
    color: #FFFFFF;
    padding: 10px; 
	}
	 .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
</style>

	</head>
	<body>
	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Calendario/Eventos</h1>
                <h3 class="font-bold c">Aquí podrás crear y consultar los eventos que habrá en tu escuela</h3>
            </center>
        </form>
    </div>
		<div class="wrapper wrapper-content">
		    <div class="row animated fadeInDown">
		        <div class="col-lg-3">
		            <div class="ibox float-e-margins">
		                <div class="ibox-title">
		                    <h5>Crear un nuevo evento</h5>
		                    
		                </div>
		                <div class="ibox-content">
		                    <div id='external-events'>
		                        <div class="form-group">
                                	<label class="control-label" >Nombre Evento</label>

                                </div>
                                <div class="form-group">

                                    <input type="text" name="titulo" id="titulo" class="form-control" >
                                </div>
                                <div class="form-group">
                                	<label class="control-label"  >Descripción</label>

                                </div>
                                <div class="form-group">

                                    <textarea rows="3" maxlength="500" style="resize:none; width: 100%" name="desc" id="desc"></textarea>
                                </div>
                                <div class="form-group">
                                	<label class="control-label">Color</label>
                                </div>
                                <div class="form-group">
                                	
                                </div>
                                <div class="form-group">
                                	<div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
										<label class="btn btn-warning">
											<input type="radio" name="priority" id="option1" value="naranja" checked>
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-primary">
											<input type="radio" name="priority" id="option2" value="verde">
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-success">
											<input type="radio" name="priority" id="option3" value="azul1">
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-info">
											<input type="radio" name="priority" id="option4" value="azul2">
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-danger">
											<input type="radio" name="priority" id="option5" value="rojo">
											<i class="fa  txt-color-white"></i> </label>
										
									</div>
								</div>
								<div class="form-group">
                                	<label class="control-label">Destinatario</label>
                                </div>
                                <div class="form-group">
                                	<select class="form-control" name="tipo" id="selectTipo" required="required">
                                		<option></option>
                                		<option value="1">General</option>
                                		<option value="2">Nivel</option>
                                		<option value="3">Grado</option>
                                		<option value="4">Grupo</option>
                                		<option value="5">Maestros</option>
                                	</select>
                                </div>
                                <div id="div">
                                	
                                </div>
                                <div class="form-group">
                                	<i  class="btn btn-warning" id="evento"  >Crear</i>
                                </div>


		                        
		                    </div>
		                </div>
		            </div>
		            
		            <div class="ibox-content" >
		                <div class="external-events" id="eventos">
		                    <h2>Eventos</h2> 
		                </div>
		            </div>
		        </div>
		        <div class="col-lg-9">
		            <div class="ibox float-e-margins">
		                
		                <div class="ibox-content">
		                    <div id="calendar"></div>
		                </div>
		                <div class="ibox-content" id="trash" >
		                <div class="external-events" >
		                    <center><span class="fa fa-trash"  style="font-size: 100px"></span></center>
		                </div>
		            </div>
		            </div>
		        </div>
		    </div>
		</div>
		
		<!-- Mainly scripts -->
        <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
        <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>

        

		<script>
			/*$(document).bind("contextmenu",function(e) {
     e.preventDefault();
});*/
			
			

			var currentMousePos = {
			    x: -1,
			    y: -1
			};
			 
			jQuery(document).on("mousemove", function (event) {
			   currentMousePos.x = event.pageX;
			   currentMousePos.y = event.pageY;
			});

		    $(document).ready(function() {

		    		$('#selectTipo').change(function(){
		    			$('#div').find('div').remove();
		    			switch(this.value)
		    			{
		    				case '1':
		    						var div1=$('<div>',{
		    							class:'form-group deletable'
		    						});
		    						var id=$('<input>',{
									     	type:'hidden',
									     	id:'user',
									     	required:'required',
									     	val:'0'
									     });
		    						div1.append(id);
		    						$('#div').append(div1);
		    						break;
		    				case '2':
		    						var div1=$('<div>',{
		    							class:'form-group '
		    						});
		    						var sNivel=$('<select>',{
		    							class:'form-control',
		    							id:'user',
		    							required:'required'
		    						})
		    						div1.append(sNivel);
		    						$('#div').append(div1);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivel')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        //console.log(item.nombre);
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });
		    						break;
		    				case '3':
		    						var sNivel	
		    						var div1=$('<div>',{
		    							class:'form-group'

		    						});
		    						
									var sgrado=$('<select>',{
		    							class:'form-control',
		    							id:'user',
		    							required:'required'
		    						});


		    						sNivel=$('<select>',{
		    							class:'form-control',
		    							id:'selectNivel',
		    							on:{
		    								change:function()
		    								{
		    									sgrado.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGrados')}}",
									                data:{idNivel:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                        sgrado.append($('<option>', {
									                            value: item.grado,
									                            text : item.grado
									                        }));
									                    });
									                }
									            });
				    						}
		    							}
		    						});
		    						var opt=$('<option>');
		    						var opt2=$('<option>');
		    						sNivel.append(opt);
		    						sgrado.append(opt2);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivel')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        //console.log(item.nombre);
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });

		    						div1.append(sNivel);
		    						div1.append(sgrado);
		    						$('#div').append(div1);
		    						break;
		    				case '4':
		    						var sNivel	
		    						var div1=$('<div>',{
		    							class:'form-group'

		    						});
		    						
		    						var sgrupo=$('<select>',{
		    							class:'form-control',
		    							id:'user',
		    							required:'required'
		    						});
									var sgrado=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    									sgrupo.find('option').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGrupos')}}",
									                data:{idNivel:sNivel.val(),grado:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                        sgrupo.append($('<option>', {
									                            value: item.idgrupo,
									                            text : item.grado + item.grupo
									                        }));
									                    });
									                }
									            });
				    						}
				    					}
		    						});


		    						sNivel=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    									sgrado.find('option').not(':first').remove();
		    									sgrupo.find('option').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGrados')}}",
									                data:{idNivel:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                        sgrado.append($('<option>', {
									                            value: item.grado,
									                            text : item.grado
									                        }));
									                    });
									                }
									            });
				    						}
		    							}
		    						});
		    						var opt=$('<option>');
		    						var opt2=$('<option>');
		    						sNivel.append(opt);
		    						sgrado.append(opt2);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivel')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        //console.log(item.nombre);
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });

		    						div1.append(sNivel);
		    						div1.append(sgrado);
		    						div1.append(sgrupo);
		    						$('#div').append(div1);
		    						break;
		    				default: 
		    						console.log('default');
		    						break;
		    			}
		    		});

		            $('.i-checks').iCheck({
		                checkboxClass: 'icheckbox_square-green',
		                radioClass: 'iradio_square-green'
		            });

		        /* initialize the external events
		         -----------------------------------------------------------------*/
		         $( "#evento" ).click(function() {

		         	//$( "<div><p>Hello</p></div>" ).appendTo( "#eventos" )

		         	if ($('#titulo').val()!='' && $('#desc').val()!='') 
		         	{	
		         		var tipoDest=$('<input>',{
						     	val:$('#selectTipo').val(),
						     	type:'hidden'
						     });
		         		var idDest=$('<input>',{
						     	val:$('#user').val(),
						     	type:'hidden'
						     });
		         		if($('#selectTipo').val()!=3)
						{
			         	     $( "<div>", {
							  class: $('input[name=priority]:checked').val(),
							  html: '<h4>' + $('#titulo').val().replace(/\s\s+/g, ' ') + '</h4>  <p>' + $('#desc').val().replace(/\s\s+/g, ' ') + '</p>',
							  val:$('#desc').val(), 
							  on: {
							    mouseover: function( event ) {
							      // Do something
							      	var content = $(this).text().split('  ');
							      	var divclass = $(this).attr("class");
							      	//console.log(content);

							      	$(this).data('event', {

								      	


						                title:content[0], // use the element's text as the event title
						                description:$(this).val(),
						                stick: true, // maintain when user navigates (see docs on the renderEvent method)
						                tipo:tipoDest.val(),
						                idDest:idDest.val(),
						                editable:true,
						                resourceEditable: true ,
						                className:divclass,
					            });



					            // make the event draggable using jQuery UI
					            $(this).draggable({
					                zIndex: 1111999,
					                revert: true,      // will cause the event to go back to its
					                revertDuration: 0  //  original position after the drag

					            });
									    }
									  }

							}).append(tipoDest).append(idDest).appendTo( "#eventos" )
			         	 }

			         	 else
			         	 {
			         	 	$( "<div>", {
							  class: $('input[name=priority]:checked').val(),
							  html: '<h4>' + $('#titulo').val().replace(/\s\s+/g, ' ') + '</h4>  <p>' + $('#desc').val().replace(/\s\s+/g, ' ') + '</p>',
							  val:$('#desc').val(), 
							  on: {
							    mouseover: function( event ) {
							      // Do something
							      	var content = $(this).text().split('  ');
							      	var divclass = $(this).attr("class");
							      	//console.log(content);
							      	var niv;
							      	if($('#selectNivel').val()==1)
							      	{
							      		niv='K';
							      	}
							      	if($('#selectNivel').val()==2)
							      	{
							      		niv='P';
							      	}
							      	if($('#selectNivel').val()==3)
							      	{
							      		niv='S';
							      	}
							      	if($('#selectNivel').val()==4)
							      	{
							      		niv='B';
							      	}
							      	$(this).data('event', {

								      	


						                title:content[0], // use the element's text as the event title
						                description:$(this).val(),
						                stick: true, // maintain when user navigates (see docs on the renderEvent method)
						                start:'2018-03-07T02:00:00',
						                tipo:tipoDest.val(),
						                idDest:idDest.val()+niv,
						                editable:true,
						                resourceEditable: true ,
						                className:divclass,
					            });



					            // make the event draggable using jQuery UI
					            $(this).draggable({
					                zIndex: 1111999,
					                revert: true,      // will cause the event to go back to its
					                revertDuration: 0  //  original position after the drag

					            });
									    }
									  }

							}).append(tipoDest).append(idDest).appendTo( "#eventos" )
			         	 }
						/*.data('event', {
		                title: $.trim($(this).text()), // use the element's text as the event title
		                stick: true // maintain when user navigates (see docs on the renderEvent method)
		            	}).draggable({
			                zIndex: 1111999,
			                revert: true,      // will cause the event to go back to its
			                revertDuration: 0  //  original position after the drag
			            	});*/
			         }
			         else
			         {
			         	swal({
			                title: "No se puede crear el evento",
			                text: "No has llenado todos los campos",
			                type:'warning'
			                
			            });

			         }


					});

		        $('#external-events div.external-event').each(function() {

		            // store data so the calendar knows to render an event upon drop
		            $(this).data('event', {
		                title: $.trim($(this).text()), // use the element's text as the event title
		                stick: true // maintain when user navigates (see docs on the renderEvent method)
		            });

		            // make the event draggable using jQuery UI
		            $(this).draggable({
		                zIndex: 1111999,
		                revert: true,      // will cause the event to go back to its
		                revertDuration: 0  //  original position after the drag
		            });

		        });


		        /* initialize the calendar
		         -----------------------------------------------------------------*/
		        var date = new Date();
		        var d = date.getDate();
		        var m = date.getMonth();
		        var y = date.getFullYear();		      
				var zone = "-05:00";
				var eventos= <?= json_encode($array)?>;

				function isElemOverDiv() {
				   var trashEl = jQuery('#trash');
				   var ofs = trashEl.offset();
				   var x1 = ofs.left;
				   var x2 = ofs.left + trashEl.outerWidth(true);
				   var y1 = ofs.top;
				   var y2 = ofs.top + trashEl.outerHeight(true);
				   if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&      currentMousePos.y >= y1 && currentMousePos.y <= y2) 
				   	{      return true;    }    
				   return false; 
				}
		        $('#calendar').fullCalendar({
		        	locale:'es',
		            header: {
		                left: 'prev,next today',
		                center: 'title',

		                right: 'month,agendaWeek,agendaDay'
		            },
		            
		            editable: true,
		            droppable: true, // this allows things to be dropped onto the calendar
		            drop: function() {
		                // is the "remove after drop" checkbox checked?
		                //if ($('#drop-remove').is(':checked')) {
		                    // if so, remove the element from the "Draggable Events" list
		                    $(this).remove();
		                //}
		            },
					eventReceive: function(event){
					    var title = event.title;
					    var start = event.start.format();
					    var clase= event.className[0];
					    $.ajax({
					      url: "{{('saveEvent')}}",
					      data: {type:1,title:title,startdate:start,zone:zone, description:event.description, class:clase,tipo:event.tipo,id:event.idDest},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					        event.id = response.eventid;
					        $('#calendar').fullCalendar('updateEvent',event);
					        //console.log('funciona');
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					    $('#calendar').fullCalendar('updateEvent',event);
					},
					eventClick: function(event, jsEvent, view) {
						
					   swal({
							  title: 'Evento',
							  text: event.title,
							  type: 'warning',
							  showCancelButton: true,
							  confirmButtonText: 'Detalles',
							  cancelButtonText: 'Editar'
							}).then(function (result){
							  if (result) {
							  	window.location.href="http://www.appycollege.com/dashboard/admin/detallesCalendario?id="+event.id;

							    }
							}, function(dixsmiss) {
								  swal({
						                title: "Editar Evento",
						                text: "Titulo del evento",
						                input: 'text',
						                inputValue: event.title,
						                showCancelButton: true, 
						            }).then(function(title) {
											event.title = title;
										   //event.description=desc.inputValue;
											//console.log(event.id);
										   $.ajax({
										     url: "{{('saveEvent')}}",
										     data:{type:2,title:title,id:event.id},
										     type: 'get',
										     dataType: 'json',
										     success: function(response){
										       if(response.status == 'success')
										       $('#calendar').fullCalendar('updateEvent',event);
										     },
										     error: function(e){
										       alert('Error processing your request: '+e.responseText);
										     }
										   });
										   swal({
								                title: "Editar Evento",
								                text: "Descripcion del evento",
								                input: 'text',
								                inputValue: event.description,
								                showCancelButton: true, 
								            }).then(function(desc) {
													event.description = desc;
												   //event.description=desc.inputValue;
													//console.log(event.id);
												   $.ajax({
												     url: "{{('saveEvent')}}",
												     data:{type:3,desc:desc,id:event.id},
												     type: 'get',
												     dataType: 'json',
												     success: function(response){
												       if(response.status == 'success')
												       $('#calendar').fullCalendar('updateEvent',event);
												     },
												     error: function(e){
												       alert('Error processing your request: '+e.responseText);
												     }
												   });
						  						}
								            );
				  						}
						            );
								});
					   
			            
					   /*var desc = swal({
			                title: "Editar Evento",
			                text: "Descripción del evento",
			                inputValue: event.description,
			                type:"input",
			            });*/
			            
						
					  
					},
					eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) 
					{	
						var start = event.start.format();
					    var end = event.end.format();
					    $.ajax({
					      url: "{{('saveEvent')}}",
					      data: {type:4,end:end, start:start,id:event.id},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					},
					eventDrop: function( event, delta, revertFunc, jsEvent, ui, view )
					{
						var start = event.start.format();
					    var end = (event.end == null) ? start : event.end.format();
					    $.ajax({
					      url: "{{('saveEvent')}}",
					      data: {type:4,end:end, start:start,id:event.id},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					},
					eventDragStop: function (event, jsEvent, ui, view) {
									   if (isElemOverDiv()) {
									     //var con = confirm('Are you sure to delete this event permanently?');
									     swal({
								                title: "¿Seguro que quieres eliminar el evento?",
								                text: "Esta acción sera permanente",
								                type:'warning',
								                showCancelButton: true, 
								            }).then(function(desc) {
												   //event.description=desc.inputValue;
													//console.log(event.id);
												   $.ajax({
												     url: "{{('saveEvent')}}",
												     data:{type:5,id:event.id},
												     type: 'get',
												     dataType: 'json',
												     success: function(response){
												       if(response.status == 'success')
												       $('#calendar').fullCalendar('removeEvents', event.id);
          											   
												     },
												     error: function(e){
												       alert('Error processing your request: '+e.responseText);
												     }
												   });
						  						}
								            );
									     /*if(con == true) {
									        $.ajax({
									          url: 'process.php',
									          data: 'type=remove&eventid='+event.id,
									          type: 'POST',
									          dataType: 'json',
									          success: function(response){
									            if(response.status == 'success')
									              $('#calendar').fullCalendar('removeEvents');
									              $('#calendar').fullCalendar('addEventSource', JSON.parse(json_events));
									          },
									          error: function(e){
									          alert('Error processing your request: '+e.responseText);
									          }
									       });
									      }*/
									    }
									},
		            events: eventos,
		            eventRender: function(event, element) { 
	            		element.find('.fc-title').append("<br/>" + event.description);
	            		if(typeof event.start._i !== 'undefined' && typeof event.start._i== 'string')
	            		{
		            		var d = event.start._i.split(" ");
		            		var n= d[1].split(":");
		            		element.find('.fc-time').empty();
		            		element.find('.fc-time').append(n[0]+":"+n[1]);
	            		}
            		} 
		        });


		    });

		</script>

	</body>
	@endsection
	