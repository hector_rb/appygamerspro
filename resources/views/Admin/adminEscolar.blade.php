@extends('Admin.menuAdmin')

@section('content')
<head>
	<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style type="text/css">
    	.tooltip-inner{
		    max-width:600px;
		}
		  .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #FDFEFE;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }

         .g{
            background-color: #3498DB;
            color: #FDFEFE;
        }

         .h{
            background-color: #1ABC9C;
            color: #FDFEFE;
        }

         .i{
            background-color: #DC7633;
            color: #FDFEFE;
        }

         .j{
            background-color: #F39C12;
            color: #FDFEFE;
        }

         .k{
            background-color: #E74C3C;
            color: #FDFEFE;
        }

         .l{
            background-color: #566573;
            color: #FDFEFE;
        }

         .m{
            background-color: #2ECC71;
            color: #FDFEFE;
        }
       
    </style>
</head>
<body>
	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Administración escolar
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes manejar todo lo relacionado con cliclos escolares, grados, grupos." aria-describedby="popover955887"></i> 
                    </small></h1>
            </center>
        </form>
        <div class="col-lg-12">
        	<div class="col-lg-9">
        	<h3 class="font-bold c">Comienza seleccionando un nivel para agregar la información correspondiente a esta página.  </h3>
        	</div>
	        <div class="col-lg-3">
	            <div class="ibox-tools has-success">
		            <select class="form-control" name="nivel" required="required" id="selectNivel">
			    		<option disabled selected value=" ">Selecciona un nivel academico</option>
			    		@foreach($niv as $n)
			            	<option value="{{$n->idNivel}}">{{$n->nombreNivel}}</option>
			            @endforeach
			    	</select>
			    	
	            </div>  
	        </div>
    	</div>
    </div>
    <br><br>
	<!--<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h1>Administración Escolar</h1>
            <p>En esta sección puesde manejar todo lo relacionado con cliclos escolares, grados, grupos</p>
        </div>
        <div class="col-lg-3">
            <br><br><br>
            <div class="ibox-tools has-success">
	            <select class="form-control" name="nivel" required="required" id="selectNivel">
		    		<option disabled selected value=" ">Selecciona un nivel academico</option>
		    		@foreach($niv as $n)
		            	<option value="{{$n->idNivel}}">{{$n->nombreNivel}}</option>
		            @endforeach
		    	</select>
            </div>  
        </div>
    </div>-->
	 <div class="wrapper wrapper-content animated fadeInRight">
	     <div class="row">
	                <div class="col-lg-12" id="ciclo">
	                </div>
	            <div id="grados1">
	                <div class="col-lg-12" >
	                    <div class="ibox float-e-margins">
	                        <div class="ibox-title">
	                            <div class="tooltip-demo">
	                            	<div class="well well-sm text-center h">
                                    	<h3 class="font-bold">ADMINISTRAR GRADOS
                                    		<small><i  class="fa fa-question-circle d" data-toggle="tooltip" data-placement="right"  title="Es en lo que se encuentra dividido cada nivel escolar, por ejemplo, en México la secundaria tiene 3 grados comúnmente.
	                            			Para crear un nuevo grado selecciona primero el nivel escolar en la parte superior de esta página, después da clic en el botón que dice 'Agregar Grado' en esta sección, a continuación aparecerá un campo de texto donde deberá escribir el nombre del grado (un número entero) y cuando este seguro da clic en guardar" aria-describedby="popover955887"></i> </small>
                                    	</h3>
                                    	<p class="font-bold">Administra los grados que tiene cada uno de tus niveles escolares</p>
                                	</div>
	                            </div>
	                        </div>
	                        <div class="ibox-content">	           
                            	<div class="table-responsive">
				                    <table class="table table-striped table-bordered table-hover " >
					                    <thead>
						                    <tr>
						                        <th>Nivel</th>
						                        <th>Grado</th>
						                        <th>Acción</th>
						                    </tr>
					                    </thead>
					                    <tbody id="gradosTable">
					                    </tbody>
				                    </table>
		                        </div>
		                        <div class="form-horizontal" id="divGrados">
		                        	<div class="form-group">
		                        		<div class="col-sm-9"></div>
		                                <div class="col-sm-3 control-label">
		                        			<button class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs" id="btnNGrado">Agregar Grado</button>
		                        		</div>

		                        	</div>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	               
	                
	            </div>
	    </div>
	</div>
	           
	       
	        <script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
	         <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
	         <!-- Tags Input -->
    		<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
    		<!--swA-->
        	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
	        <script>
	        	$(document).ready(function() 
	        	{
	        		$('.chosen-select').chosen();
	        		$('#selectNivel').change( function(){
	        			var nivel=this.value;

	        			var tGrados=$('#gradosTable');
	        			getGradosT(nivel, tGrados);

	        			var tPeriodo=$('#periodosTable');
	        			getPeriodosT(nivel, tPeriodo);

	        			var SCiclo=$('#selectCiclo');
	        			getCiclosS(nivel, SCiclo);

	        			

	        			var SGrados=$('#selectGradosMat');
	        			getGradosS(nivel, SGrados);

	        			var SGrados=$('#selectGradosMat2');
	        			getGradosS(nivel, SGrados);
	        		});

	        		$('#selectGradosMat').change(function(){
	        			var grado=this.value;
	        			var nivel = $('#selectNivel').val();

	        			var TMateria=$('#materiasTable');
	        			getMateriasT(nivel,grado, TMateria);

	        			var SMateria=$('#selectMaterias');
	        			getMateriasS(nivel,grado,SMateria);
	        			$("#form_field").trigger("liszt:updated");
	        		});

	        		$('#selectGradosMat2').change(function(){
	        			var grado=this.value;
	        			var nivel = $('#selectNivel').val();

	        			var TGrupo=$('#gruposTable');
	        			getGruposT(nivel,grado, TGrupo);

	        		});

	        		$('#btnNGrupo').click(function(){

	        			var DGrupo=$('#divGrupo');
	        			var nivel=$('#selectNivel');
	        			var grado=$('#selectGradosMat2');
	        			appendGrupo(DGrupo, nivel, grado);
	        		});
	        		$('#btnNMateria').click(function(){

	        			var DMateria=$('#divMateria');
	        			var nivel=$('#selectNivel');
	        			var grado=$('#selectGradosMat');
	        			appendMateria(DMateria, nivel, grado);
	        		});
	        		$('#btnNPeriodo').click(function(){

	        			var DPeriodos=$('#divPeriodos');
	        			var nivel=$('#selectNivel');	        			
	        			appendPeriodo(DPeriodos, nivel);
	        		});
	        		$('#btnNGrado').click(function(){

	        			var DGrados=$('#divGrados');
	        			var nivel=$('#selectNivel');	        			
	        			appendGrado(DGrados, nivel);
	        		});  
	        		$('#btnNCiclo').click(function(){

	        			var DCiclo=$('#divCiclo');
	        			var nivel=$('#selectNivel');	        			
	        			appendCiclo(DCiclo, nivel);
	        		});   	
	        		$('#btnSCiclo').click(function(){

	        			var nivel=$('#selectNivel');	
	        			var ciclo=$('#selectCiclo');
	        			setCiclo(nivel,ciclo);
	        			$('#grados1').show();
	        		});  
	        		$('#btnProfMat').click(function(){

	        			var prof=$('#selectProfesor');	
	        			var mat=$('#selectMaterias');
	        			//console.log(mat);
	        			setMatProf(prof,mat);
	        		});
	        	});
	        </script>
	    </body>
@endsection