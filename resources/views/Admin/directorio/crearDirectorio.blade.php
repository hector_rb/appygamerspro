@extends('Admin.menuAdmin')
@section('content')
<html>
<head>
     <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
        
    </style>
</head>
    <body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Nuevo contacto</h1>
            </center>
        </form>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::action('directorioController@create')}}">Nuevo registro</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/directorio')}}"> <i class="fa fa-file "></i>Contactos<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/directorio/filtro')}}"> <i class="fa fa-eye "></i>Filtros</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

<div class="col-lg-9">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                            {!!Form::open(['method'=>'POST','route'=>['directorio.store'], 'class'=>'form-horizontal'])!!}
                            

                                <div class="form-group">
                                	
                                    <label class="col-sm-2 control-label">NOMBRE CONTACTO</label>

                                    <div class="col-sm-8"><input type="text" name="nombre" class="form-control" required="required"></div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >TELEFONO</label>

                                    <div class="col-sm-3"><input type="text" name="tel" class="form-control" required="required"></div>
                                    <label class="col-sm-2 control-label" >EMAIL</label>

                                    <div class="col-sm-3"><input type="email" name="email" class="form-control" required="required"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >PUESTO/AREA</label>

                                    <div class="col-sm-8"><input type="text" name="puesto" class="form-control" required="required"></div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">DESCRIPCION BREVE DEL CONTACTO</label>

                                    <div class="col-sm-8"><textarea rows="5" maxlength="500" style="resize:none; width: 100%" name="desc" required="required"></textarea></div>
                                </div>                       
                        </div>
                        <br>
                        <div class="col-sm-10"></div>
                        <button type="submit" name="guardar" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>                       
    </div>
    </body>
</html>

@endsection