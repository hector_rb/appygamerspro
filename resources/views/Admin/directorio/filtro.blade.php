@extends('Admin.menuAdmin')
@section('content')
<head>

    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .azul{
            background: #5AADB7;
            color:#FDFDFC;
        }
        .naranja{
            background: #E39E41;
            color:#FDFDFC;
        }
        .blanco{
            background: #FDFEFE;
            color:#2C3E50;
        }
        
    </style>
</head>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">¿Quien puede ver los contactos?
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta secci¨®n puedes dar de alta la informaci¨®n de las personas con las que los padres se pueden contactar." aria-describedby="popover955887"></i> 
                    </small></h1>
            </center>
        </form>
    </div>
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::action('directorioController@create')}}">Nuevo registro</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/directorio')}}"> <i class="fa fa-file "></i>Contactos<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/directorio/filtro')}}"> <i class="fa fa-eye "></i>Filtros</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
{{Form::Open(array('route'=> 'cambiarFiltro' , 'method'=>'post'))}}
<!-- {{Form::token()}}  -->
<div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                @foreach($dir as $key =>  $d)
                    <div class="col-lg-6">
                        <div class="ibox-title naranja">
                                <div class="btn-group" data-toggle="buttons">
                                    @if($d->verProfesor == 1)
                                        <div class="i-checks"><label> <input type="checkbox" checked="true" name="contactos[{{$key}}][verProfesor]" value="1"> <i></i> &nbsp;Profesores </label></div>
                                    @else
                                        <div class="i-checks"><label> <input type="checkbox" name="contactos[{{$key}}][verProfesor]" value="1"> <i></i> &nbsp;Profesores </label></div>
                                    @endif
                                    @if($d->verTutor == 1)
                                        <div class="i-checks"><label> <input type="checkbox" checked="true" name="contactos[{{$key}}][verTutor]" value="1"> <i></i> &nbsp;Tutores </label></div>
                                    @else
                                        <div class="i-checks"><label> <input type="checkbox"  name="contactos[{{$key}}][verTutor]" value="1"> <i></i> &nbsp;Tutores </label></div>
                                    @endif
                                        <input type="hidden" name="contactos[{{$key}}][id]" value="{{$d->idDirectorio}}">
                                    
                                </div>
                        </div>
                        <div class="contact-box">
                            <div class="col-sm-3">
                                <div class="text-center">
                                    <img alt="image" class="img-circle m-t-xs img-responsive" src="{{asset('img/contact/'.$d->imagen)}}">
                                    <div class="m-t-xs font-bold b">{{$d->puesto}}</div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <h3 class="font-bold c"><strong>{{$d->nombre}}</strong></h3>
                                
                                <address>
                                    <p class="font-bold d"><i class="fa fa-phone d"></i><strong> {{$d->telefono}}</strong></p>
                                    <p class="font-bold f"><strong>
                                    <i class="fa fa-at f"></i> {{$d->email}}<br>
                                    {{$d->descripcion}}</strong></p>
                                </address>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endforeach
                <button type="submit" name="guardar" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button>
            </div> 
        </div>
</div>
{!!Form::close()!!}
    <!-- iCheck -->
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
@endsection
