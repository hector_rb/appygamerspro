@extends('Admin.menuAdmin')

@section('content')
<head>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background: #FDFEFE;
            color: #5AADB7;
        }

         .b{
            color: #EB984E;
        }
        .azul{
            background: #5AADB7;
            color:#FDFDFC;
        }
        .naranja{
            background: #E39E41;
            color:#FDFDFC;
        }
        .blanco{
            background: #FDFEFE;
            color:#2C3E50;
        }
        
    </style>
</head>
<body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Directorio
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta secci��n puedes dar de alta la informaci��n de las personas con las que los padres se pueden contactar." aria-describedby="popover955887"></i> 
                    </small></h1>
            </center>
        </form>
    </div>
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::action('directorioController@create')}}">Nuevo registro</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/directorio')}}"> <i class="fa fa-file "></i>Contactos<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/directorio/filtro')}}"> <i class="fa fa-eye "></i>Filtros</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

<div class="col-lg-9">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @foreach($dir as $d)
                <div class="col-lg-6">
                    <div class="ibox-title naranja">
                        <div class="ibox-tools">
                            <a href="" data-target="#modal-delete-{{$d->idDirectorio}}" data-toggle="modal" title="Eliminar" class="fa fa-times naranja"></a>
                        </div>
                    </div>
                    <a href="{{URL::action('directorioController@edit',[$d->idDirectorio])}}">
                        <div class="contact-box">
                            <div class="col-sm-4">
                                <div class="text-center">
                                    <img alt="image" class="img-circle m-t-xs img-responsive" src="{{asset('img/contact/'.$d->imagen)}}">
                                    <div class="m-t-xs font-bold a">{{$d->puesto}}</div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h3 class="font-bold a"><strong>{{$d->nombre}}</strong></h3>
                                
                                <address>
                                    <p class="font-bold a"><i class="fa fa-phone d"></i><strong> {{$d->telefono}}</strong></p>
                                    <p class="font-bold a"><strong>
                                    <i class="fa fa-at a"></i> {{$d->email}}<br>
                                    {{$d->descripcion}}</strong></p>
                                </address>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
                @include('Admin.directorio.modal')
            @endforeach
        </div>                    
    </div>
</div>




</body>
@endsection
