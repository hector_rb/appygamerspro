@extends('Admin.menuAdmin')

@section('content')
<html>
	<head>
         <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }

         .g{
            background-color: #F0B27A;
            color: #F0F3F4;
        }

         .h{
            background-color: #76D7C4;
            color: #F0F3F4;
        }
        
    </style>
    
    
	</head>
    <body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Directorio
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes editar la información de las personas con las que los padres se pueden contactar." aria-describedby="popover955887"></i> 
                    </small></h1>
                <h2 class="font-bold c">Editar directorio</h2>
            </center>
        </form>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::action('directorioController@create')}}">Nuevo registro</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/directorio')}}"> <i class="fa fa-file "></i>Contactos<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('admin/directorio/filtro')}}"> <i class="fa fa-eye "></i>Filtros</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
<div class="col-lg-9">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
                <div class="col-lg-12">
                        <div class="ibox-content">
                            @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                            {!!Form::open(['method'=>'PATCH','route'=>['directorio.update',$dir->idDirectorio], 'class'=>'form-horizontal'])!!}
                            
                                <div class="well well-sm text-center h">
                                    <label>INFORMACIÓN DE CONTACTO</label>
                                </div>

                                <div class="form-group">
                                	
                                    <label class="col-sm-2 control-label">NOMBRE CONTACTO</label>

                                    <div class="col-sm-8"><input type="text" name="nombre" class="form-control" required="required" value="{{$dir->nombre}}"></div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >TELEFONO</label>

                                    <div class="col-sm-3"><input type="text" name="tel" class="form-control" required="required" value="{{$dir->telefono}}"></div>
                                    <label class="col-sm-2 control-label" >EMAIL</label>

                                    <div class="col-sm-3"><input type="email" name="email" class="form-control" required="required" value="{{$dir->email}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >PUESTO/AREA</label>

                                    <div class="col-sm-8"><input type="text" name="puesto" class="form-control" required="required" value="{{$dir->puesto}}"></div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">DESCRIPCION BREVE DEL CONTACTO</label>

                                    <div class="col-sm-8"><textarea rows="5" maxlength="500" style="resize:none; width: 100%" name="desc" required="required">{{$dir->descripcion}}</textarea></div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <div class="col-sm-9"></div>
                                    <div class="col-sm-3 control-label">
                                    <button type="submit" name="guardar" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button></div>
                                </div>                        
                            {!!Form::close()!!}
                        </div>
                    <div class="ibox-content">
                        <div class="well well-sm text-center g">
                            <label>FOTO DE CONTACTO</label>
                        </div>
                            <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="image-crop">
                                        <img id="image" src="{{asset('img/contact/'.$dir->imagen)}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h4>Imagen previa</h4>
                                    <div class="img-preview img-preview-sm"></div>
                                    <h4>cambiar imagen</h4>
                                    <p>
                                        Para seleccionar una fotografia da click en "Subir"
                                    </p>
                                    
                                    <div class="btn-group">
                                        <label title="Upload image file" for="inputImage" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">
                                            <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                            Subir
                                        </label>

                                        <label  class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">
                                            <input id="saveImage" class="hide">
                                            Guardar Imagen
                                        </label>
                                    </div>
                                   
                                </div>
                                
                            </div>
                        </div>
                </div>
        </div>
    </div>
</div>                       
        <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{asset('js/inspinia.js')}}"></script>
        <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>
        <script>
            $(document).ready(function()
            {
                var $image = $(".image-crop > img")
                var cropper;
                $image.cropper({
                    aspectRatio: 1 / 1,
                    preview: ".img-preview",
                    
                    crop: function(e) {
                        // Output the result data for cropping image.
                        
                        console.log(e.x);
                        console.log(e.y);
                        console.log(e.width);
                        console.log(e.height);
                        console.log(e.rotate);
                        console.log(e.scaleX);
                        console.log(e.scaleY);
                    },            
                });

                var $inputImage = $("#inputImage");
                if (window.FileReader) {
                    $inputImage.change(function() {
                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        if (!files.length) {
                            return;
                        }

                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {
                                $inputImage.val();
                                $image.cropper("reset", true).cropper("replace", this.result);
                            };
                        } else {
                            showMessage("Please choose an image file.");
                        }
                    });
                } else {
                    $inputImage.addClass("hide");
                }

                $('#saveImage').click(function(){

                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

                        $image.cropper('getCroppedCanvas').toBlob(function (blob) {
                        

                        var formData = new FormData();
                        var token = $("input[name=_token]").val();

                       // $('meta[name="csrf-token"]').attr('content');
                       console.log(token);
                        formData.append('croppedImage',$image.cropper('getCroppedCanvas',{
                          width: 256,
                          height: 256,
                          
                        }).toDataURL("image/png"));
                        formData.append("_token", token);
                        formData.append("id", {{$dir->idDirectorio}});
                        // Use `jQuery.ajax` method
                        $.ajax( {
                        url:"{{('../../saveImageDir')}}",
                        type:'post',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            //$('#profileImage').attr("src","{{('../img/profile_pics/')}}"+data);
                            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                            window.location.href = '{{url("admin/directorio")}}'
                        },
                        error: function (e) {
                        console.log(e.responseJSON);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        }
                        });
                    });
                });
            });
        </script>
    </body>
</div>
</html>
@endsection