@extends('Admin.menuAdmin')
@section('content')
<!DOCTYPE html>
<html>

<head>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
        .azul{
            background: #5AADB7;
            color:#FDFDFC;
        }
        .naranja{
            background: #E39E41;
            color:#FDFDFC;
        }
        .blanco{
            background: #FDFEFE;
            color:#2C3E50;
        }
    </style>

</head>

<body>
    <div id="wrapper">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-4">

                    <div class="ibox-content text-center azul">
                        <br><br>
                            <h2 class="font-bold">{{$institucion[0]->nombre}}</h2>
                            <h3 class="font-bold">{{$institucion[0]->ciudad}}</h3>
                            <h3 class="font-bold">CLAVE: {{$institucion[0]->clave}}</h3>
                            <h3 class="font-bold">DIRECCIÓN: {{$institucion[0]->direccion}}</h3>
                        <br><br>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title naranja">
                            <h5>Usuarios de la institución que usan la aplicación</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up naranja"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times naranja"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div>
                                <canvas id="doughnutChart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title naranja">
                            <h5>Altas y bajas de los alumnos de la institución</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up naranja"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times naranja"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="text-center">
                                <canvas id="polarChart" height="140"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title naranja">
                            <h5>Seguridad escolar</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up naranja"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times naranja"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div>
                                <canvas id="barChart" height="140"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="col-lg-4">
            <div class="ibox-title naranja">
                    <h5>Profesor de la materia.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up naranja"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times naranja"></i>
                        </a>
                    </div>
             </div>
            <div class="ibox-content text-center azul" id="fotoPerfil">                            </div>
            <div class="widget-text-box" id="nombrePerfil">
            </div>
            <div class="ibox-content">
                <h5 class="font-bold" id="totalcalif"></h5>
                <h5 class="font-bold" id="aprobados"></h5>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title naranja">
                    <h5>Materia, grupo y nivel <small class="naranja">En este apartado podrás optener un resumen del control de estadísticas de la institución.</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up naranja"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times naranja"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <select name="idSubgrupo" id="selectSubgrupo" class="form-control">
                        @foreach ($sg as $sg)
                            <option value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>


        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title naranja">
                    <h5>Control de calificaciones por ciclo <small class="naranja">Muestra los promedios de cada uno de los periodos de la materia seleccionada.</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up naranja"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times naranja"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="graficasCalif">
                        <canvas id="barChart" height="110"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title naranja">
                    <h5>Promedio del grupo.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up naranja"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times naranja"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <h1 class="jumbotron font-bold blanco" id="rows"></h1>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title naranja">
                    <h5>Alumnos con mejor rendimiento.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up naranja"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times naranja"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover table-mail">
                            <tbody id="rows1">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title naranja">
                    <h5>Alumnos con bajo rendimiento.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up naranja"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times naranja"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover table-mail">
                            <tbody id="rows2">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> 



    <!-- ChartJS-->
<script src="{{asset('js/plugins/chartJs/Chart.min.js')}}"></script>
<script type="text/javascript">
$.ajax({
        method: 'get',
        url: "{{('TotalPersonas')}}",
        dataType: 'json',
        success: function (data) {
            var barData = {
                labels: ["Sismo", "Incendio", "Violencia","Robo"],
                datasets: [
                    {
                        label: "Numero de alertas",
                        backgroundColor: 'rgba(91, 169, 169)',
                        borderColor: "rgba(91, 169, 169)",
                        pointBackgroundColor: "rgba(136, 243, 243)",
                        pointBorderColor: "#fff",
                        data: [data.sismo, data.incendio, data.violencia, data.robo]
                    }
                ]

            };
            var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});

                    var doughnutData = {
                        labels: ["Alumnos","Profesores","Tutores"],
                        datasets: [{
                            data: [data.alumnos,data.profesores,data.tutores],
                            backgroundColor: ["#39CACA","#EFB032","#EF5D32"]
                        }]
                    } ;


                    var doughnutOptions = {
                        responsive: true
                    };


                    var ctx4 = document.getElementById("doughnutChart").getContext("2d");
                    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

                  
                var polarData = {
                    datasets: [{
                        data: [
                            data.altas, data.bajas, 
                        ],
                        backgroundColor: [
                            "#2CD1D9", "#B68D16"
                        ],
                        label: [
                            "My Radar chart"
                        ]
                    }],
                    labels: [
                        "Altas","Bajas"
                    ]
                };

                var polarOptions = {
                    segmentStrokeWidth: 2,
                    responsive: true

                };

                var ctx3 = document.getElementById("polarChart").getContext("2d");
                new Chart(ctx3, {type: 'polarArea', data: polarData, options:polarOptions});

        }
});

 ban=true;
    $(document).ready(function(){
        document.getElementById("selectSubgrupo").selectedIndex=0;
        cargar(document.getElementById("selectSubgrupo").value);
    });
    function cargar(idsg){
        $('#rows').find('tr').remove();
                        $.ajax({
                            method: 'get',
                            url: "{{('getPromedioGrupo')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                    var promedio=0;
                                    var contador=0; 
                                    var grado;
                                $.each(data, function (i, item) {
                                    //console.log(item.nombre);
                                    promedio=promedio+item.promedio;
                                    contador++;
                                    grado=item.grado;
                                    grupo=item.grupo;
                                    //aqui se calcula
                                    });
                                    promedio=promedio/contador;
                                    var fila = $('<h1>');
                                    if(contador>0)
                                    {
                                        var resultado = Math.round(promedio*Math.pow(10,2))/Math          .pow(10,2     );
                                    }else
                                    {
                                        var resultado="";
                                    }

                                    /*var Grupo =$('<td>',{
                                        text: grado + ' ' +grupo
                                    });*/                                 
                                    var calif =$('<h1>',
                                        {
                                            text:resultado,
                                            class:'font-bold',
                                        });
                                    //fila.append(Grupo);
                                    fila.append(calif);
                                    $('#rows').empty();
                                    $('#rows').append(fila);
                                    
                                }
                            });

                        $('#rows1').find('tr').remove();
                        $.ajax({
                            method: 'get',
                            url: "{{('getPromedioAlumnoSobresaliente')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                $.each(data, function (i, item) {
                                    
                                        var fila = $('<tr>');

                                        var resultado = Math.round(item         .promedio*Math.pow(10,2))/Math          .pow(10,2);

                                        /*var id =$('<td>',{
                                            text: item.idAlumno,
                                            
                                        });*/
                                        console.log("{{('img/profile_pics/')}}"+item.imagen);
                                        var id =$('<img>',{
                                            src:"{{asset('img/profile_pics/')}}"+'/'+item.imagen,
                                            height:'40', 
                                            width:'40', 
                                            class:'img-circle m-b-md', 
                                            alt:'profile'
                                            
                                        });
                                        var nombre =$('<td>',{
                                            text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                        });                                 
                                        var calif =$('<td>',
                                            {
                                                text:resultado
                                            });
                                        
                                        fila.append(id);
                                        fila.append(nombre);
                                        fila.append(calif);
                                        
                                        $('#rows1').append(fila);
                                    
                                    });
                                }
                            });
                        $('#rows2').find('tr').remove();
                        $.ajax({
                            method: 'get',
                            url: "{{('getPromedioAlumnoRegular')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                $.each(data, function (i, item) {
                                    if(item.promedio<8)
                                    {
                                        var fila = $('<tr>');

                                        var resultado = Math.round(item.promedio*Math.pow(10,2))/Math.pow(10,2);

                                        var id =$('<img>',{
                                            src:"{{asset('img/profile_pics/')}}"+'/'+item.imagen,
                                            height:'40', 
                                            width:'40', 
                                            class:'img-circle m-b-md', 
                                            alt:'profile'
                                            
                                        });
                                        var nombre =$('<td>',{
                                            text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                        });                                 
                                        var calif =$('<td>',
                                            {
                                                text:resultado
                                            });
                                        
                                        fila.append(id);
                                        fila.append(nombre);
                                        fila.append(calif);
                                        
                                        $('#rows2').append(fila);
                                    }
                                    });
                                }
                            }); 

                        $('#totalcalif').find('p').remove();
                        $.ajax({
                            method: 'get',
                            url: "{{('getCalifCount')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                    var contador = 0;
                                    var suma= 0
                                console.log("data" + data)
                                $.each(data, function (i, item) {

                                    suma=item.calif;
                                    contador++;
                                    });
                                    suma=contador
                                    var fila = $('<h4>');                                
                                    
                                    var calif=$('<h4>',{
                                        text:'Número de calificaciones del ciclo:' + ' ' + suma,
                                        class:'font-bold'
                                    });
                                    
                                    fila.append(calif);
                                     
                                     $('#totalcalif').empty();

                                     $('#totalcalif').append(fila);
                               
                                    
                                }
                            });

                        $('#aprobados').find('p').remove();
                        $.ajax({
                            method: 'get',
                            url: "{{('getCalifAprobCount')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                    var contador = 0;
                                    var suma= 0
                                console.log("data" + data)
                                $.each(data, function (i, item) {

                                    suma=item.calif;
                                    contador++;
                                    });
                                    suma=contador
                                    var fila = $('<h4>');                                
                                    
                                    var calif=$('<h4>',{
                                        text:'Número de alumnos aprobados:' + ' ' + suma,
                                        class:'font-bold'
                                    });
                                    
                                    fila.append(calif);

                                    $('#aprobados').empty();

                                     $('#aprobados').append(fila);
                               
                                    
                                }
                            });

                        $('#graficasCalif').empty();
                        $.ajax({
                            method: 'get',
                            url: "{{('graficasCalif')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                    var canvas =  document.createElement("CANVAS");
                                    canvas.id="barChart";

                                    var barData = {
                                        labels: data.califs,
                                        datasets: [
                                            {
                                                label: "Control de calificaciones del ciclo",
                                                backgroundColor: 'rgba(87, 151, 175)',
                                                borderColor: "rgba(26,179,148,0.7)",
                                                pointBackgroundColor: "rgba(26,179,148,1)",
                                                pointBorderColor: "#fff",
                                                data: data.barra
                                            }
                                        ]
                                    };
                                    
                                    var barOptions = {
                                        responsive: true
                                    };
                                    $('#graficasCalif').append(canvas);

                                    var ctx2 = canvas.getContext("2d");
                                    
                                    new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});   
                                    
                                    

                                }
                            });
                        $('#fotoPerfil').find('p').remove();
                        $.ajax({
                            method: 'get',
                            url: "{{('getFotoProfesores')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                $.each(data, function (i, item) {

                                        var id =$('<img>',{
                                            src:"{{asset('img/profile_pics/')}}"+'/'+item.imagen,
                                            height:'170', 
                                            width:'175', 
                                            class:'img-circle circle-border m-b-md', 
                                            alt:'profile'
                                        });
                                        $('#fotoPerfil').empty();
                                        $('#fotoPerfil').append(id);
                                    
                                    });
                                }
                            });
                        $.ajax({
                            method: 'get',
                            url: "{{('getFotoProfesores')}}",
                            data: {idsg:idsg},
                            dataType: 'json',
                            success: function (data) {
                                $.each(data, function (i, item) {
                                        var nombre =$('<h3>',{
                                            text: 'Profesor:' + item.nombreProf + ' ' + item.apepat + ' ' + item.apemat
                                        });

                                        var correo =$('<h3>',{
                                            text: 'Correo:' + item.correo
                                        });

                                        var telefono =$('<h3>',{
                                            text: 'Telefono:' + item.telefono
                                        });

                                        var celular =$('<h3>',{
                                            text: 'Celular:' + item.celular
                                        });

                                        $('#nombrePerfil').empty();
                                        $('#nombrePerfil').append(nombre);
                                        $('#nombrePerfil').append(correo);
                                        $('#nombrePerfil').append(telefono);
                                        $('#nombrePerfil').append(celular);
                                    
                                    });
                                }
                            });

    }
    $("#selectSubgrupo").change(function(){
                        var idsg = this.value;
                        cargar(idsg);
         

    });
    </script>

</body>

</html>

@endsection