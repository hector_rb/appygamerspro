@extends('Profesor.menuProfesor')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Control y estadisticas</h2>
<ol class="breadcrumb">
<li class="active">
<a href="{{URL::to('profesor/graficas')}}">Calificaciones</a>
</li>
<li class="active">
<a href="{{URL::to('profesor/graficas/create')}}"><strong>Asistencias</strong></a>
</li>
</ol>
</div>
</div>


<div class="container">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="wrapper wrapper-content">
		<div class="col-lg-11">	
        <div class="row">
        	<!--///////////////////////////////////////////-->
        	<div class="col-lg-6 col-md-3 col-md-3 col-xs-12">
					<div class="form-group">
					<label for="idmateria">Materia-Grupo-Nivel</label>
					<select name="idSubgrupo" id="selectSubgrupo" class="form-control">
					<option value=""></option>
					@foreach ($sg as $sg)
					<option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
					@endforeach
					</select>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-md-4 col-xs-12">
					<div class="form-group">
					<label for="numevaluaciones">Periodo</label>
					<select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
						<option></option>
					</select>
					</div>
				</div>
				<!--//////////////////////////////////////////////////////-->
        	
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Annual</span>
                                <h5>Alumno con mas asistencias</h5>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover table-mail">
									<tbody>
									<tr class="unread">
									<thead >
									<th>id</th>
									<th>Alumno</th>
									<th>Asistencias</th>
									</thead>
									<tbody id="rows1">
									</tbody>
									</tr>
									</tbody>
								</table>
                                <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                                <small>New orders</small>
                            </div>
                        </div>
                    </div>
            <!--//////////////////////////////////-->
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Today</span>
                                <h5>Alumno con mas retardos</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="retardos">106,120</h1>
                                <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                                <small>New visits</small>
                            </div>
                        </div>
                    </div>
            <!--//////////////////////////////////-->
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Low value</span>
                                <h5>Alumno con mas faltas</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="faltas">80,600</h1>
                                <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                                <small>In first month</small>
                            </div>
                        </div>
            		</div>
            <!--//////////////////////////////////-->
         </div>

        <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Control de gráficas.- Asistencias de alumnos</h5>
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-white active">Today</button>
                                        <button type="button" class="btn btn-xs btn-white">Monthly</button>
                                        <button type="button" class="btn btn-xs btn-white">Annual</button>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                        <li>
                                            <h2 class="no-margins">2,346</h2>
                                            <small>Total de asistencias de alumnos</small>
                                            <div class="stat-percent">48% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 48%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>Total de asistencias de alumnos</small>
                                            <div class="stat-percent">60% <i class="fa fa-level-down text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 60%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">9,180</h2>
                                            <small>Total de faltas de alumnos</small>
                                            <div class="stat-percent">22% <i class="fa fa-bolt text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 22%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>
         </div>   		
	</div>
</div>	
<script>

	$("#selectSubgrupo").change(function(){
            		var idsg = this.value;

		            $('#selectPeriodos').find('option').not(':first').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getPeriodosAsistenciasProfesor')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        var op =$('<option>',{
		                        	value: item.idPeriodo,
		                        	text: item.nombrePeriodo
		                        });
		                        
		                        
		                        $('#selectPeriodos').append(op);
		                   
		                		});
		            		}
		        		});
		            
		        });
	$("#selectPeriodos").change(function(){
            		var id = this.value;
            		var idsg = $('#selectSubgrupo').val();
		            
		            $('#rows1').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getMayorAsistencia')}}",
		                data: {idP:id,idsg:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.idAlumno,
		                        	
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
 		                        }); 		                        
		                        var calif =$('<td>',
		                        	{
		                        		text:item.calif
		                        	});
		                        
		                        fila.append(id);
		                        fila.append(nombre);
		                        fila.append(calif);
		                        
		                        $('#rows1').append(fila);
		                   
		                		});
		            		}
		        		});
		            }
		        		
///////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////			
			
</script>
    <script>
        $(document).ready(function() {
            $('.chart').easyPieChart({
                barColor: '#f8ac59',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            $('.chart2').easyPieChart({
                barColor: '#1c84c6',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            var data2 = [
                [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
                [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
                [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6]
            ];

            var data3 = [
                [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
                [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
                [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700]
            ];

            var data4 = [
                [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
            ];

            var dataset = [
                {
                    label: "Asistencias",
                    data: data3,
                    color: "#1ab394",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth:0
                    }

                },
                {
                    label: "Faltas",
                    data: data4,
                    color: "#CD5C5C",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth:0
                    }

                }, {
                    label: "Retardos",
                    data: data2,
                    yaxis: 2,
                    color: "#1C84C6",
                    lines: {
                        lineWidth:1,
                            show: true,
                            fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0.4
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    },
                }
            ];


            var options = {
                xaxis: {
                    mode: "time",
                    tickSize: [3, "day"],
                    tickLength: 0,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 10,
                    color: "#d5d5d5"
                },
                yaxes: [{
                    position: "left",
                    max: 1070,
                    color: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 3
                }, {
                    position: "right",
                    clolor: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: ' Arial',
                    axisLabelPadding: 67
                }
                ],
                legend: {
                    noColumns: 1,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: false,
                    borderWidth: 0
                }
            };

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }

            var previousPoint = null, previousLabel = null;

            $.plot($("#flot-dashboard-chart"), dataset, options);

            var mapData = {
                "US": 298,
                "SA": 200,
                "DE": 220,
                "FR": 540,
                "CN": 120,
                "AU": 760,
                "BR": 550,
                "IN": 200,
                "GB": 120,
            };

            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },

                series: {
                    regions: [{
                        values: mapData,
                        scale: ["#1ab394", "#22d6b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });
        });
    </script>	 


@endsection