<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$mensajes->id}}">
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-Label="Close">
                    <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Eliminar tarea</h4>
                </div>
                <div class="modal-body">
                    <p>Confirme si desea Eliminar tarea</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cerrar</button>
                    <a class="btn btn-w-m btn-outline btn-danger" href="{{ route('Papelera',['id' => $mensajes->id] )}}" >Borrar</a>
                </div>
            </div>
    </div>
</div>

</div>
</div>
</div>
