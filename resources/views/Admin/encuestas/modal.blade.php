<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$e->idEncuesta}}">
{{Form::Open(array('action'=>array('EncuestasAdminController@destroy', $e->idEncuesta),'method'=>'delete'))}}

<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-Label="Close">
<span aria-hidden="true">x</span>
</button>
<h4 class="modal-title">Eliminar encuesta</h4>
</div>
<div class="modal-body">
<p>Confirme si desea Eliminar encuesta</p>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-w-m btn-w-m btn-outline btn-default btn-xs" data-dismiss="modal"> Cerrar</button>
<button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Confirmar</button>
<p>&nbsp;</p>
</div>
</div>
</div>
</div>

{{Form::Close()}}
</div>
</div>