@extends('Admin.menuAdmin')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Encuesta</h2>
<ol class="breadcrumb">
<li>
<a href="{{URL::to('admin/encuestas/create')}}">Crear encuesta</a>
</li>
<li class="active">
<a href="{{URL::to('admin/encuestas')}}"><strong>Consultar encuestas</strong></a>
</li>

</ol>
</div>
<div class="col-lg-2">
</div>
</div>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
	.swal-title {
		margin: 0px;
		font-size: 10px;
		box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
		margin-bottom: 28px;
	}
	.naranja {
		background-color: #f8ac59;
		border-color: #f8ac59;
		color: #FFFFFF;
		padding: 10px; 
	}
	.verde{
		background-color: #1ab394;
		border-color: #1ab394;
		color: #FFFFFF;
		padding: 10px; 
	}
	.azul1 {
		background-color: #1c84c6;
		border-color: #1c84c6;
		color: #FFFFFF;
		padding: 10px; 
	}
	.azul2 {
		background-color: #23c6c8;
		border-color: #23c6c8;
		color: #FFFFFF;
		padding: 10px; 
	}
	.rojo {
		background-color: #ed5565;
		border-color: #ed5565;
		color: #FFFFFF;
		padding: 10px; 
	}
	.tooltip-inner{
		max-width:600px;
	}
	.preguntas{
		width: 45%;
		position: relative;
	}
</style>

    </head>
    <body>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
	<div class="wrapper wrapper-content">
		<div class="row animated fadeInDown">
			<div class="col-lg-3">

				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h2>Destinatario</h2>   
					</div>
					<div class="ibox-content">
						<div id='external-events'>
							@foreach ($tienc as $e)
							<h4> <?php
							switch($e->tipoEncuestado)
							{
								case 1:
									echo "Profesor";
									break;
								case 2:
									echo "Alumno";
									break;
								case 3:
									echo "Tutor";
									break;
							}

							?></h4>
							@endforeach   
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">

				<div class="ibox float-e-margins">
					<div class="ibox-title">
	    				<h2>Preguntas</h2>
					</div>

					<div class="ibox-content" id="trash" >
						<label>Nombre de la encuesta:</label>
						<p>{{$encuesta[0]->nombre}}</p>

					</div>
					<div class="ibox-content" id="trash" >
						<label>Descripción:</label>
						<p>{{$encuesta[0]->descripcion}}</p>
					</div>
					<!--<div class="ibox-content" >
						<canvas id="densityChart"  width='300' height='150' ></canvas>
						<canvas id="densityChart1" width="600" height="400"></canvas>
						<canvas id="speedChart" width="600" height="400"></canvas>
					</div>-->
					<div class="ibox-content" id="trash" >
					<select id="preguntas" name="preguntas" class="form-control">
							<option></option>
							@foreach ($preguntas as $e)	
								<option value="{{$e->idPregunta}}">{{$e->nombrePregunta}}</option>
								<canvas id="densityChart"  width='300' height='150' ></canvas>
							@endforeach
					</select>
					<div id="id">
					<canvas id="densityChart"  width='300' height='150' ></canvas>
					</div>
					</div>
					</div>
			</div>
		</div>
	</div>          
    </body>
<script>    
		
	$('#preguntas').change(function(){
		var idpre=this.value;
		$('#id').find('div').empty();
		$.ajax({
            method: 'get',
            url: "{{('../getContador')}}",
            data: {idpre:idpre},
            dataType: 'json',
            success: function (data) {

            	switch(data.tipo)
            	{
            		case 1:
                        var densityCanvas = document.getElementById("densityChart");
						Chart.defaults.global.defaultFontFamily = "Lato";
						Chart.defaults.global.defaultFontSize = 18;
						var densityData = {
						  label: 'Si',
						  data: [data.Si, 0],
						  backgroundColor: 'rgba(0, 99, 132, 0.6)',
						  borderWidth: 0,
						  yAxisID: "y-axis-density"
						};

						var gravityData = {
						  label: 'No',
						  data: [data.No, 0],
						  backgroundColor: 'rgba(99, 132, 0, 0.6)',
						  borderWidth: 0,
						  yAxisID: "y-axis-density"
						};

						var planetData = {
						  labels: ["resultado"],
						  datasets: [densityData, gravityData]
						};

						var chartOptions = {
						  scales: {
						    xAxes: [{
						      barPercentage: 1,
						      categoryPercentage: 1
						    }],
						    yAxes: [{
						      id: "y-axis-density"
						    }, {
						      id: "y-axis-density"
						    }]
						  }
						};

						var barChart = new Chart(densityCanvas, {
						  type: 'bar',
						  data: planetData,
						  options: chartOptions
						});
                              
           				barChart.draw();
           			break;
           			case 2:
           			
           			break;
           		}
			},
			error:function(data){console.log(data.responseJSON);},
		});
	});

</script>
@endsection
