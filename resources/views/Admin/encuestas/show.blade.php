@extends('Admin.menuAdmin')
@section('content')
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
	
	.tooltip-inner{
		max-width:600px;
	}
	.a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #E1B16F;
            color: #FDFEFE;
        }

        .div {
            width: 23%;
        }
</style>

    </head>
    <body>
    	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Detalles de la encuesta
                </h1>
            </center>
        </form>
		</div>
	<div class="col-lg-12">
        <a href="{{URL::to('admin/encuestas')}}">
        <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" href="{{URL::to('admin/encuestas')}}">Regresar</button></a>
	</div>
	<br>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
	<div class="wrapper wrapper-content">
		<div class="row animated fadeInDown">
			<div class="col-lg-3">

				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h2>Destinatario</h2>   
					</div>
					<div class="ibox-content">
						<div id='external-events'>
							
							@foreach ($tienc as $e)

							<h4> <?php
							switch($e->tipoEncuestado)
							{
								case 1:
									echo "Profesor";
									break;
								case 2:
									echo "Alumno";
									break;
								case 3:
									echo "Tutor";
									break;
							}

							?></h4>
							@endforeach   
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">

				<div class="ibox float-e-margins">
					<div class="ibox-title">
	    				<h2>Preguntas</h2>
	    				<div class="col-lg-10"></div>
	    				<button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs" type="submit" onclick='window.print();' >Imprimir</button>
					</div>

					<div class="ibox-content" id="trash" >
						<label>Nombre de la encuesta:</label>
						<p>{{$encuesta[0]->nombre}}</p>

					</div>
					<div class="ibox-content" id="trash" >
						<label>Descripción:</label>
						<p>{{$encuesta[0]->descripcion}}</p>
					</div>
					<!--<div class="ibox-content" >
						<canvas id="densityChart"  width='300' height='150' ></canvas>
						<canvas id="densityChart1" width="600" height="400"></canvas>
						<canvas id="speedChart" width="600" height="400"></canvas>
					</div>-->
					<div class="ibox-content" id="trash" >
					<!--<select id="preguntas" name="preguntas" class="form-control">
							<option></option>
							@foreach ($preguntas as $e)	
								<option value="{{$e->idPregunta}}">{{$e->nombrePregunta}}</option>
								<canvas id="densityChart"  width='300' height='150' ></canvas>
							@endforeach
					</select>-->
					<div id="contenedor">
						
					</div>
						
					</div>
					</div>
			</div>
		</div>
	</div>          
    </body>
   <script >
    
   	$(document).ready(function()
   	{
   		var idEncuesta= <?=$idenc?>;
   		$.ajax({
            method: 'get',
            url: "{{('../getPreguntas')}}",
            data: {idEncuesta:idEncuesta},
            dataType: 'json',
            success: function (data) {
            	$.each(data, function (i, item) {
            		
            		var idPre=item.idPregunta;
            		$.ajax({
			            method: 'get',
			            url: "{{('../getContador')}}",
			            data: {idpre:idPre},
			            dataType: 'json',
			            success: function (data) {
			            	var cont=1;
			            	var divcon=$("#contenedor");
			            	var div=$('<div>', {
                            		id:'id'
                        		});
			            	var h3=$('<h3>',{
			            		text:item.nombrePregunta
			            	});
                        	var densityCanvas=	$('<canvas>', {
                            		id:'densityChart',
                            		width:'300',
                            		height:'150',
                        		});
                        	div.append(h3);
                        	div.append(densityCanvas);
                        	divcon.append(div);
			            	switch(data.tipo)
			            	{
			            		case 1:
									Chart.defaults.global.defaultFontFamily = "Lato";
									Chart.defaults.global.defaultFontSize = 18;
									var densityData = {
									  label: 'Si',
									  data: [data.Si],
									  backgroundColor: 'rgba(0, 99, 132, 0.6)',
									  borderWidth: 0,
									  yAxisID: "y-axis-density"
									};

									var gravityData = {
									  label: 'No',
									  data: [data.No],
									  backgroundColor: 'rgba(99, 132, 0, 0.6)',
									  borderWidth: 0,
									  yAxisID: "y-axis-density"
									};

									var planetData = {
									  labels: ["resultado"],
									  datasets: [densityData, gravityData]
									};

									var chartOptions = {
									  scales: {
									    xAxes: [{
									      barPercentage: 1,
									      categoryPercentage: 1
									    }],
									    yAxes: [{
									      id: "y-axis-density"
									    }, {
									      id: "y-axis-density"
									    }]
									  }
									};

									var barChart = new Chart(densityCanvas, {
									  type: 'bar',
									  data: planetData,
									  options: chartOptions
									});
			                              
			           				barChart.draw();
			           			break;
			           			case 2:
									Chart.defaults.global.defaultFontFamily = "Lato";
									Chart.defaults.global.defaultFontSize = 18;
									var densityData = {
									  label: 'Opciones',
									  data: [data.primero, data.segundo, data.tercero, data.cuarto, data.quinto]
									};

									var opcion=data.opciones[0].opciones.split("¨");
									
									var barChart = new Chart(densityCanvas, {
									  type: 'bar',
									  data: {
									    labels:opcion,
									    datasets: [densityData]
									  }
									});
									barChart.draw();
			           			break;
			           			case 3:
								Chart.defaults.global.defaultFontFamily = "Lato";
									Chart.defaults.global.defaultFontSize = 18;
									var densityData = {
									  label: 'Opciones',
									  data: [data.uno, data.dos, data.tres, data.cuatro, data.cinco, data.seis, data.siete, data.ocho, data.nueve, data.diez]
									};
                                    
									var barChart = new Chart(densityCanvas, {
									  type: 'bar',
									  data: {
									    labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
									    datasets: [densityData]
									  }
									});
									barChart.draw();
			           			break;
			           		}

			            }
			        });
            	});
            }
   		});
   	});
   </script> 
<script>    
/*		
	$('#preguntas').change(function(){
		var idpre=this.value;
		$('#id').hide();
		$('#id2').hide();
		$('#id3').hide();
		$.ajax({
            method: 'get',
            url: "{{('../getContador')}}",
            data: {idpre:idpre},
            dataType: 'json',
            success: function (data) {
            	switch(data.tipo)
            	{
            		case 1:
                        var densityCanvas = document.getElementById("densityChart");
						Chart.defaults.global.defaultFontFamily = "Lato";
						Chart.defaults.global.defaultFontSize = 18;
						$('#id').show();
						$('#id2').hide();
						$('#id3').hide();
						var densityData = {
						  label: 'Si',
						  data: [data.Si],
						  backgroundColor: 'rgba(0, 99, 132, 0.6)',
						  borderWidth: 0,
						  yAxisID: "y-axis-density"
						};

						var gravityData = {
						  label: 'No',
						  data: [data.No],
						  backgroundColor: 'rgba(99, 132, 0, 0.6)',
						  borderWidth: 0,
						  yAxisID: "y-axis-density"
						};

						var planetData = {
						  labels: ["resultado"],
						  datasets: [densityData, gravityData]
						};

						var chartOptions = {
						  scales: {
						    xAxes: [{
						      barPercentage: 1,
						      categoryPercentage: 1
						    }],
						    yAxes: [{
						      id: "y-axis-density"
						    }, {
						      id: "y-axis-density"
						    }]
						  }
						};

						var barChart = new Chart(densityCanvas, {
						  type: 'bar',
						  data: planetData,
						  options: chartOptions
						});
                              
           				barChart.draw();
           			break;
           			case 2:
           				var densityCanvas = document.getElementById("densityChart3");
						Chart.defaults.global.defaultFontFamily = "Lato";
						Chart.defaults.global.defaultFontSize = 18;
						$('#id').hide();
						$('#id2').show();
						$('#id3').hide();

						var densityData = {
						  label: 'Opciones',
						  data: [data.primero, data.segundo, data.tercero, data.cuarto, data.quinto]
						};

						var opcion=data.opciones[0].opciones.split(",");
						
						var barChart3 = new Chart(densityCanvas, {
						  type: 'bar',
						  data: {
						    labels:opcion,
						    datasets: [densityData]
						  }
						});
						barChart3.draw();
           			break;
           			case 3:
           			var densityCanvas = document.getElementById("densityChart2");

					Chart.defaults.global.defaultFontFamily = "Lato";
					Chart.defaults.global.defaultFontSize = 18;
						$('#id').hide();
						$('#id2').hide();
						$('#id3').show();
					var densityData = {
					  label: 'rango 1-10',
					  data: [data.uno, data.dos, data.tres, data.cuatro, data.cinco, data.seis, data.siete, data.ocho, data.nueve, data.diez],
					  backgroundColor: [
					    'rgba(0, 99, 132, 0.6)',
					    'rgba(30, 99, 132, 0.6)',
					    'rgba(60, 99, 132, 0.6)',
					    'rgba(90, 99, 132, 0.6)',
					    'rgba(120, 99, 132, 0.6)',
					    'rgba(150, 99, 132, 0.6)',
					    'rgba(180, 99, 132, 0.6)',
					    'rgba(210, 99, 132, 0.6)',
					    'rgba(240, 99, 132, 0.6)',
					    'rgba(270, 99, 132, 0.6)'
					  ],
					  borderColor: [
					    'rgba(0, 99, 132, 1)',
					    'rgba(30, 99, 132, 1)',
					    'rgba(60, 99, 132, 1)',
					    'rgba(90, 99, 132, 1)',
					    'rgba(120, 99, 132, 1)',
					    'rgba(150, 99, 132, 1)',
					    'rgba(180, 99, 132, 1)',
					    'rgba(210, 99, 132, 1)',
					    'rgba(240, 99, 132, 1)',
					    'rgba(270, 99, 132, 1)',
					  ],
					  borderWidth: 2,
					  hoverBorderWidth: 0
					};

					var chartOptions = {
					  scales: {
					    yAxes: [{
					      barPercentage: 0.5
					    }]
					  },
					  elements: {
					    rectangle: {
					      borderSkipped: 'left',
					    }
					  }
					};

					var barChart2 = new Chart(densityCanvas, {
					  type: 'horizontalBar',
					  data: {
					    labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
					    datasets: [densityData],
					  },
					  options: chartOptions
					});

					barChart2.draw();
           			break;
           		}
			},
			error:function(data){console.log(data.responseJSON);},
		});
	});
*/
</script>
@endsection
