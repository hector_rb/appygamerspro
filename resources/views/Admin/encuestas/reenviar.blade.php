@extends('Admin.menuAdmin')
@section('content')
<html>
       <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
	.tooltip-inner{
		max-width:600px;
	}
	.preguntas{
		width: 45%;
		position: relative;
	}

	input[type=text2] {
    width: 60%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: none;
    background-color: #CDD6D5;
    color: black;
}
input[type=text3] {
    width: 80%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: none;
    background-color: #8ACAC3;
    color: black;
}
input[type=text4] {
    width: 80%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: none;
    background-color: #E1E8E8;
    color: black;
}
input[type=text5] {
    width: 80%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: none;
    background-color: #B9ECE2;
    color: black;
}
.a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #E1B16F;
            color: #FDFEFE;
        }

        .div {
            width: 23%;
        }
</style>

    </head>
<body>
<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Reenviar encuestas
                </h1>
                <h3 class="font-bold c">Aquí, puedes reenviar las encuestas para la institución</h3>
            </center>
        </form>
</div>
<div class="col-lg-12">
        <a href="{{URL::to('admin/encuestas')}}">
        <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" href="{{URL::to('admin/encuestas')}}">Regresar</button></a>
</div>
<br>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/admin/encuestas','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}} 
	<div class="wrapper wrapper-content">
		<div class="row animated fadeInDown">
			<div class="col-lg-3">
				<div class="ibox-content">
                            <div id='external-events'>
                                
                                <div class="form-group">
                                    <label class="control-label">Nivel:</label>
                                </div>
                                <div class="form-group">
                                    <select required class="form-control" name="idSubGrupo" id="selectTipo">
                                      <option value="">None</option>
                                      @foreach ($niv as $niv)
                                      <option value="{{$niv->idNivel}}">{{$niv->nombreNivel}}</option>
                                       @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label class="control-label">Destinatario:</label>
                                </div>
                                <div class="form-group">
                                    <select required class="chosen-select" name="destinatario[]" id="destinatario" multiple  tabindex="2">
                                        <option selected value=""></option>
                                        <option value="1" >Profesores</option>
                                        <option value="2" >Alumnos</option>
                                        <option value="3" >Tutores</option>
                                </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Grupo:</label>
                                </div>
                                <div class="form-group">
                                    <select required class="chosen-select" name="selectGrupo[]" id="selectGrupo"  multiple  tabindex="2">
                                        <option selected value=""></option>
                                        <option value=""></option>
                                </select>
                                </div>
                            </div>
                        </div>
				
			</div>
			<div class="col-lg-9">

				<div class="ibox float-e-margins">
					<div class="ibox-title">
	    				<h2>Preguntas</h2>
					</div>

					<div class="ibox-content" id="trash" >
						<label>Nombre de la encuesta:</label>
						<input  type="text4" class="form-control" name="nombreEncuesta" value="{{$encuesta[0]->nombre}}">

					</div>
					<div class="ibox-content" id="trash" >
						<label>Descripción:</label>
						<input  type="text4" class="form-control" name="Descripcion" value="{{$encuesta[0]->descripcion}}">
					</div>
					<div class="ibox-content" id="trash" >
						<label>Preguntas</label>
						<?php
						$pre=1;
						?>
							@foreach ($preguntas as $e)
								<br>
								<input  type="text3" class="form-control" value="{{$e->nombrePregunta}}">
								<input  type="text2" class="form-control" 
									value="<?php
									switch($e->tipoRespuesta)
									{
										case 1:
											echo "si/no";
											break;
										case 2:
											echo "$e->opciones";
											break;
										case 3:
											echo "rango 1-10";
											break;
									}

									?>
									">
									<input type="text" name="{{'selectMateria'.$pre}}" style="visibility:hidden" value="{{$e->nombrePregunta}}">
									<input type="text" name="{{'tipopregunta'.$pre}}" style="visibility:hidden" value="{{$e->tipoRespuesta}}">
                                    <input type="text" name="{{'opcion_tipopregunta'.$pre}}" style="visibility:hidden" value="{{$e->opciones}}">
                                    <input type="text" name="ree" style="visibility:hidden" value=1>
                                   <?php
                                   
									$pre=$pre+1;
									?>
									
							@endforeach	
							<input type="text" name="pre" style="visibility:hidden" value="{{$pre-1}}">
							<div class="col-sm-9"></div>
	                        <button  type="submit" name="enviar" id="enviar" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Reenviar</button>
                    </div>
					
				</div>
			</div>
		</div>
	</div>          
</body>
{!!Form::close()!!} 
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
    <!-- Tags Input -->
    <script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
    <!--swA-->
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js')}}"></script> 
     <!-- Mainly scripts -->
        <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
        <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
            
            <script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
            <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
            <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
    <script>
    	$("#selectTipo").change(function(){
                    var idNivel = this.value;

                    $('#selectGrupo').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getGruposEncuestas')}}",
                        data: {id:idNivel},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                
                                var op =$('<option>',{
                                    value: item.idgrupo,
                                    text: item.grado,
                                });
                                $('#selectGrupo').append(op);
                                $('#selectGrupo').trigger("chosen:updated");
                                $('#selectGrupo').trigger("liszt:updated");
                                
                           
                                });
                            }
                        });
       });
    </script>    
    <script>
        $(document).ready(function() 
        {
                    
            $('.chosen-select').chosen();   
        });     
        function Desactiva()
        {
            $grupo

            if($('#selectGrupo').is(':checked')==true)
            {
                document.getElementById("selectAlumno").disabled=true;
                
            }
            else
            {   
                document.getElementById("selectAlumno").disabled=false;
                
            }
            $('#selectAlumno').trigger("chosen:updated");
            $('#selectAlumno').trigger("liszt:updated");
            console.log($('#selectAlumno').is(':disabled'));
        }

    </script>

@endsection