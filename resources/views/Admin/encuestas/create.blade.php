@extends('Admin.menuAdmin')
@section('content')
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #E1B16F;
            color: #FDFEFE;
        }

        .div {
            width: 23%;
        }
</style>

    </head>
    <body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Encuestas
                </h1>
                <h3 class="font-bold c">Aquí, puedes realizar las encuestas para la institución</h3>
            </center>
        </form>
    </div>
<!--<a href="{{URL::to('admin/encuestas/create')}}">
<button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Crear encuesta</button>
</a>-->
<div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/encuestas/create')}}">Crear encuesta</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/encuestas')}}"> <i class="fa fa-inbox "></i> Historial de encuestas<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        @if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/admin/encuestas','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
                                 
    <div class="col-lg-9 animated fadeInRight">

                            <div class="ibox-title">
                                <div class="well well-sm text-center d">
                                        <h3 class="font-bold">DESTINATARIO
                                        </h3>
                                        <p>Selecciona destinatario</p>
                                </div>
                                            <div class="form-group">
                                                <label class="control-label">Nivel:</label>
                                                    <select required class="chosen-select div" name="idSubGrupo" id="selectTipo">
                                                        <option value="">Selecciona un nivel</option>
                                                    @foreach ($niv as $niv)
                                                        <option value="{{$niv->idNivel}}">{{$niv->nombreNivel}}</option>
                                                    @endforeach
                                                    </select>
                                                <label class="control-label">Destinatario:</label>
                                                    <select class="chosen-select div" name="destinatario[]" id="destinatario" multiple  tabindex="2">
                                                        <option disabled selected value=""></option>
                                                        <option value="1" >Profesores</option>
                                                        <option value="2" >Alumnos</option>
                                                        <option value="3" >Tutores</option>
                                                    </select>
                                                <label class="control-label">Grado:</label>
                                                <select class="chosen-select div" name="selectGrupo[]" id="selectGrupo"  required="required" multiple  tabindex="2">
                                                    <option disabled selected value=""></option>
                                                    <option value=""></option>
                                            </select>
                                            </div>
                                    
                            </div>
                            <div class="ibox-content" id="div_contenedor">
                                <div class="well well-sm text-center e">
                                        <h3 class="font-bold">PREGUNTAS
                                        </h3>
                                        <p>Llena nombre de encuesta y descripcion, y dar click en agregar las preguntas que deseas para realizar la encuesta</p>
                                </div>
                            <input name="pre"  id="pre" required="required" value=0 hidden="true">

                        <div  id="trash" >
                            <label>Nombre de la encuesta:</label>
                            <input type="text" class="form-control" name="nombreEncuesta" required="true" >
                            <label>Descripcion de la encuesta:</label>
                            <textarea class="form-control" name="Descripcion" required="required"></textarea>
                        </div>
                        <div class="ibox-content" id="trash" >
                            <div id="pregunta">
                             </div>
                             <button type="button" name="nuevo" id="nuevo" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Agregar pregunta</button>
                             <button type="button" name="quitar" id="quitar" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Quitar pregunta</button>
                              <button  type="submit" style="display: none;" name="enviar" id="enviar" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" >Enviar</button>
                    </div>
                    
                    
            </div>
    </div>
{!!Form::close()!!}     

</div>
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
    <!-- Tags Input -->
    <script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
    <!--swA-->
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js')}}"></script> 
<script type="text/javascript">
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>   
<script>
    var pregunta=0;

        $("#selectTipo").change(function(){
                    var idNivel = this.value;

                    $('#selectGrupo').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getGruposEncuestas')}}",
                        data: {id:idNivel},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                
                                var op =$('<option>',{
                                    value: item.idgrupo,
                                    text: item.grado 
                                });
                                $('#selectGrupo').append(op);
                                $('#selectGrupo').trigger("chosen:updated");
                                $('#selectGrupo').trigger("liszt:updated");
                                
                           
                                });
                            }
                        });
       });
        $('#nuevo').click(function()
        { 
            pregunta=pregunta+1;
        if(pregunta<7)
        {
         $("#pre").val(pregunta);
         var es=$('<br>');
         var div=$('<div>', {
                            class:'ibox float-e-margins',
                            id:'opciones' + pregunta
                        });  
         var l=$('<label>', {
                            class:'ibox float-e-margins',
                            text:'Pregunta ' + pregunta
                        });   
         var s=$('<input>', {
                            name: 'selectMateria' + pregunta,
                            id: 'selectMateria'+ pregunta,
                            class:'form-control',
                            required:'required',
                        });
         var tipo=$('<select>', {
                            name: 'tipopregunta'+ pregunta,
                            id: 'tipopregunta'+ pregunta ,
                            class:'form-control',
                            required:'required',
                            on: {
                                    change: function()
                                    {
                                        if(tipo.val()==2)
                                        {
                                            opc.show();
                                            opc.prop('required',true);
                                            opc1.show();
                                            opc1.prop('required',true);
                                            opc2.show();
                                            opc2.prop('required',true);
                                            opc3.show();
                                            opc4.show();
                                            la1.show();
                                            la2.show();
                                            la3.show();
                                            la4.show();
                                            la5.show();
                                            
                                        }
                                        else
                                        {
                                            opc.hide();
                                            opc.prop('required',false);
                                            opc1.hide();
                                            opc1.prop('required',false);
                                            opc2.hide();
                                            opc2.prop('required',false);
                                            opc3.hide();
                                            opc4.hide();
                                            la1.hide();
                                            la2.hide();
                                            la3.hide();
                                            la4.hide();
                                            la5.hide();
                                        }

                                    }
                                }
                        });
         tipo.append($('<option>', {
                            value:0,
                            text:'Seleccione',
                            }));
         tipo.append($('<option>', {
                            value:1,
                            text:'si/no',
                            }));
         tipo.append($('<option>', {
                            value:2,
                            text:'varias opciones (A,B,C,D,E)',
                            }));
         tipo.append($('<option>', {
                            value:3,
                            text:'rango 1-10',
                            }));
         var opc = $('<input>', {
            class:'form-control',
            placeholder:'opción 1',
            width: '50%',
            name:'opcion_'+ tipo.attr("name")+'[]',
            id:'opcion_'+ tipo.attr("name")+'[]',
            hide: 'true'
        });
        var la1=$('<label>', {
                class:'ibox float-e-margins',
                text:'',
                hide: 'true'
            });
        var opc1 = $('<input>', {
            class:'form-control',
            placeholder:'opción 2',
            width: '50%',
            name:'opcion_'+ tipo.attr("name")+'[]',
            id:'opcion_'+ tipo.attr("name")+'[]',
            hide: 'true'           
        });
        var la2=$('<label>', {
                class:'ibox float-e-margins',
                text:'',
                hide: 'true'
            });
        var opc2 = $('<input>', {
            class:'form-control',
            placeholder:'opción 3',
            width: '50%',
            name:'opcion_'+ tipo.attr("name")+'[]',
            id:'opcion_'+ tipo.attr("name")+'[]',
            hide: 'true'
        });
        var la3=$('<label>', {
                class:'ibox float-e-margins',
                text:'',
                hide: 'true'
            });
        var opc3 = $('<input>', {
            class:'form-control',
            placeholder:'opción 4',
            width: '50%',
            name:'opcion_'+ tipo.attr("name")+'[]',
            id:'opcion_'+ tipo.attr("name")+'[]',
            hide: 'true'            
        });
        var la4=$('<label>', {
                class:'ibox float-e-margins',
                text:'',
                hide: 'true'
            });
        var opc4 = $('<input>', {
            class:'form-control',
            placeholder:'opción 5',
            width: '50%',
            name:'opcion_'+ tipo.attr("name")+'[]',
            id:'opcion_'+ tipo.attr("name")+'[]',
            hide: 'true'
        });
        var la5=$('<label>', {
                class:'ibox float-e-margins',
                text:'',
                hide: 'true'
            });
        
        div.append(es);
        div.append(l);
        div.append(s);
        div.append(es);
        div.append(tipo);
        div.append($('<br>'));
        //div.append(la1);
        div.append(opc);
        //div.append($('<br>'));
        div.append(la2);
        div.append(opc1);
        //div.append($('<br>'));
        div.append(la3);
        div.append(opc2);
        //div.append($('<br>'));
        div.append(la4);
        div.append(opc3);
        //div.append($('<br>'));
        div.append(la5);
        div.append(opc4);
        //div.append($('<br>'));
         $('#pregunta').append(div);
        }
        else
        {
            pregunta=6;
        }
        $('#enviar').show();
        });

        $('#quitar').click(function()
        {
                $('#'+'opciones' + pregunta).remove();
                pregunta=pregunta-1;
                $("#pre").val(pregunta);
                if(pregunta<=0)
                {
                    pregunta=0;
                    $('#enviar').hide();
                    $("#pre").val(pregunta);
                }

        });
        
    </script>
    <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
        <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
            
            <script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
            <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
            <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
        
    <script>
        $(document).ready(function() 
        {
                    
            $('.chosen-select').chosen();   
        });     
        function Desactiva()
        {
            $grupo

            if($('#selectGrupo').is(':checked')==true)
            {
                document.getElementById("selectAlumno").disabled=true;
                
            }
            else
            {   
                document.getElementById("selectAlumno").disabled=false;
                
            }
            $('#selectAlumno').trigger("chosen:updated");
            $('#selectAlumno').trigger("liszt:updated");
            console.log($('#selectAlumno').is(':disabled'));
        }

    </script>
</body>
@endsection