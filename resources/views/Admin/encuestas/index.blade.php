@extends('Admin.menuAdmin')
@section('content')
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
</style>

</head>
<body>
	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Encuestas
                </h1>
                <h3 class="font-bold c">Aquí, puedes realizar las encuestas para la institución</h3>
            </center>
        </form>
    </div>
    <br>
    <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/encuestas/create')}}">Crear encuesta</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/encuestas')}}"> <i class="fa fa-inbox "></i> Historial de encuestas<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
	 <div class="col-lg-9 animated fadeInRight">
	 <div class="row">
		<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
			<div class="ibox-content">
				<div class="table-responsive">
						<table class="table table-hover table-mail">
							<tbody>
								<tr class="unread">
								    <thead>
								                    <center>
								                        <th>No. de encuesta</th>
								                        <th>Nombre</th>
								                        <th>Descripción</th>
								                        <th>Fecha</th>
								                        <th></th>
								                    </center>
								    </thead>
								</tr>
								@foreach ($encuesta as $e)
									<thead>
										<td>{{$e->idEncuesta}}</td>
										<td>{{$e->nombre}}</td>
										<td>{{$e->descripcion}}</td>
										<td>{{$e->fecha}}</td>
										<td>
											<a href="" data-target="#modal-delete-{{$e->idEncuesta}}" data-toggle="modal">
												<button class="btn btn-w-m btn-w-m btn-outline btn-danger btn-xs">Eliminar
												</button>
											</a>
											<br>
											<br>
											<a href="{{URL::to('admin/encuestas/reenviar?idEncuesta='.$e->idEncuesta)}}">
												<button class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Reenviar
												</button>
											</a>
											<br>
											<br>
											<a href="{{URL::to('admin/encuestas/'.$e->idEncuesta)}}" ><button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Detalles</button></a>
										</td>
									</thead>
									@include('Admin.encuestas.modal')
									@endforeach
							</tbody>
						</table>
				</div>
			</div>
		</div>
	</div>
	</div>		
</body>
@endsection