@extends('Admin.menuAdmin')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Expedientes</h2>
<ol class="breadcrumb">
<li>
<a href="{{URL::to('admin/expedientes')}}"><strong>Regresar</strong></a>
</li>
</ol>
</div>
<div class="col-lg-2">
</div>
</div>


<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
    .swal-title {
        margin: 0px;
        font-size: 10px;
        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
        margin-bottom: 28px;
    }
    .naranja {
        background-color: #f8ac59;
        border-color: #f8ac59;
        color: #FFFFFF;
        padding: 10px; 
    }
    .verde{
        background-color: #1ab394;
        border-color: #1ab394;
        color: #FFFFFF;
        padding: 10px; 
    }
    .azul1 {
        background-color: #1c84c6;
        border-color: #1c84c6;
        color: #FFFFFF;
        padding: 10px; 
    }
    .azul2 {
        background-color: #23c6c8;
        border-color: #23c6c8;
        color: #FFFFFF;
        padding: 10px; 
    }
    .rojo {
        background-color: #ed5565;
        border-color: #ed5565;
        color: #FFFFFF;
        padding: 10px; 
    }
    .tooltip-inner{
        max-width:600px;
    }
    .preguntas{
        width: 45%;
        position: relative;
    }
    .btn span.glyphicon {               
    opacity: 0;             
    }
    .btn.active span.glyphicon {                
    opacity: 1;             
    }
</style>

    </head>
<body>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/admin/expedientes','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}} 

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInDown">
            <div class="col-lg-3">

                <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$alumno[0]->nombreAlumno}} {{$alumno[0]->apepat}} {{$alumno[0]->apemat}}
                            </h2>
                            </div>
                            <img src="{{asset('img/profile_pics/product-1508194262.png')}}" class="img-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="widget-text-box">
                            <label>Grado y grupo:</label><h4>{{$alumno[0]->grado}} {{$alumno[0]->grupo}}</h4>
                            <label>Fecha de nacimiento:</label><h4>{{$alumno[0]->fechanac}}</h4>
                            <label>Teléfono:</label><h4>{{$alumno[0]->telefono}}</h4>
                            <label>Correo:</label><h4>{{$alumno[0]->correo}}</h4>
                        </div>
            </div>
            <div class="col-lg-9">
                    <div class="ibox-content" id="trash" >
                      <h3>Conocimientos adicionales</h3>  
                      <p>Agregar los conocimientos adicionales del alumno.</p>
                    </div>
                    <div class="ibox-content" id="trash" >

                        <input type="text" name="idAlumno" value="{{$alumno[0]->idAlumno}}" style="visibility:hidden">
        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Primer conocimiento adicional:</label>
                            <div class="col-sm-8"> 
                                <input type="text" class="form-control" name="conocimiento1" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Segundo Conocimiento adicional:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="conocimiento2" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tercer conocimiento adicional:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="conocimiento3" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="col-lg-9"></div>
                        <button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Guardar</button>
                        <br></br>
                    </div>
            </div>
    </div>
</body>
{!!Form::close()!!} 
<script>    
</script>
@endsection
