@extends('Admin.menuAdmin')
@section('content')
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

<style type="text/css">
.tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }    
</style>

    </head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Expedientes
                </h1>
                <h3 class="font-bold c">Muestran los datos de todos los alumnos de la institución</h3>
            </center>
        </form>
    </div>
    <div class="col-lg-12">
        <a href="{{URL::to('admin/expedientes')}}">
        <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" href="{{URL::to('admin/expedientes')}}">Regresar</button></a>
    </div>
    <br>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/admin/expedientes','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}} 

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInDown">
            <div class="col-lg-3">

                <div class="ibox-content text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$alumno[0]->nombreAlumno}} {{$alumno[0]->apepat}} {{$alumno[0]->apemat}}
                            </h2>
                            </div>
                            <img src="{{asset('img/profile_pics/' .$alumno[0]->imagen)}}" height="170" width="175" class="img-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="widget-text-box">
                            <label>Grado y grupo:</label><h4>{{$alumno[0]->grado}} {{$alumno[0]->grupo}}</h4>
                            <label>Fecha de nacimiento:</label><h4>{{$alumno[0]->fechanac}}</h4>
                            <label>Teléfono:</label><h4>{{$alumno[0]->telefono}}</h4>
                            <label>Correo:</label><h4>{{$alumno[0]->correo}}</h4>
                        </div>
            </div>
            <div class="col-lg-9">
                     <div class="ibox-content" id="trash" >
                      <h3>Conocimientos adicionales</h3>  
                      <p>Agregar los conocimientos adicionales del alumno.</p>
                    </div>
                    <div class="ibox-content" id="trash" >

                        <input type="text" name="datos" value="{{$alumno[0]->idAlumno}}" style="visibility:hidden">
        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Primer conocimiento adicional:</label>
                            <div class="col-sm-8"> 
                                <input type="text" class="form-control" name="conocimiento1" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Segundo Conocimiento adicional:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="conocimiento2" >
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tercer conocimiento adicional:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="conocimiento3" >
                            </div>
                        </div>
                        <br></br>
                        <div class="col-lg-9"></div>
                        <!--<button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Guardar</button>-->
                        <br></br>
                    <div class="ibox-content" id="trash" >
                      <h3>Datos médicos</h3>  
                      <p>Llenar los datos médicos correspondientes para obtener la información necesaria del alumno.</p>
                    </div>
                    <div class="ibox-content" id="trash" >

                        <input type="text" name="idAlumno" value="{{$alumno[0]->idAlumno}}" style="visibility:hidden">
        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Alergias:</label>
                            <div class="col-sm-8"> 
                                <input type="text" class="form-control" name="alergias" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Enfermedades:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="enfermedades" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Enfermedades crónicas:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="enfCro" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cirugías:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="cirugias" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número del seguro:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="numSeguro" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo de sangre:</label>
                            <div class="col-sm-8">  
                                <!--<input type="text" class="form-control" name="tipoSangre" required="true">-->
                                <select class="form-control" name="tipoSangre" required="true">
                                    <option value=""></option>
                                    <option value="A POSITIVO">A POSITIVO</option>
                                    <option value="A NEGATIVO">A NEGATIVO</option>
                                    <option value="B POSITIVO">B POSITIVO</option>
                                    <option value="B NEGATIVO">B NEGATIVO</option>
                                    <option value="O POSITIVO">O POSITIVO</option>
                                    <option value="O NEGATIVO">O NEGATIVO</option>
                                    <option value="AB POSITIVO">AB POSITIVO</option>
                                    <option value="AB NEGATIVO">AB NEGATIVO</option>
                                </select>
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número de emergencia:</label>
                            <div class="col-sm-8">  
                                <input type="text" class="form-control" name="numEme" required="true">
                            </div>
                        </div>
                        <br></br>
                        <div class="col-lg-9"></div>
                        <button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Guardar</button>
                        <br></br>
                    </div>
            </div>
    </div>
</body>
{!!Form::close()!!} 
<script>    
</script>
@endsection
