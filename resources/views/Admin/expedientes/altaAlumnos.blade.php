@extends('Admin.menuAdmin')
@section('content')
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">


<link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

<link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">
<style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #E1B16F;
            color: #FDFEFE;
        }

        .div {
            width: 23%;
        }
</style>
</head>
    <body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Administración de alumnos
                </h1>
                <h3 class="font-bold c">Registrar alumnos</h3>
            </center>
        </form>
    </div>
<div class="col-lg-3">
    <div class="ibox float-e-margins">
        <div class="ibox-content mailbox-content">
            <div class="file-manager">
                <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/expedientes/altaAlumnos')}}">Agregar alumno</a>
                <div class="space-25"></div>
                <h5>Folders</h5>
                <ul class="folder-list m-b-md" style="padding: 0">
                    <li><a href="{{URL::to('admin/expedientes')}}"> <i class="fa fa-inbox "></i> Alumnos</a></li>
                </ul>
                
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-lg-9 animated fadeInRight">
        <div class="ibox-content">
                <div class="row">
                    <form class="form-horizontal"  method="POST" action="{{ route('AltaAlumnos') }}">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                                    
                                    <br>
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">

                                                <input type="hidden" class="form-control" name="admin" value="2" required>
                                                <input id="name" type="text" placeholder="Nombre" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group" >

                                            <div class="col-md-12">
                                                <input type="text"  placeholder="Apellido Paterno" class="form-control" name="apepat" required>
                                            </div>
                                        </div>

                                        <div class="form-group" >

                                            <div class="col-md-12">
                                                <input type="text"  placeholder="Apellido Materno" class="form-control" name="apemat" required>
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <input id="password-confirm" placeholder="confirma tu contraseña" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>
                    
                            </div>
                            <div class="col-md-6">
                                    <br>
                                    <div class="form-horizontal">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">
                                                <input type="date" placeholder="Fecha de nacimiento" class="form-control" name="fechanac" >
                                            </div>
                                        </div>


                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <input type="text" placeholder="Telefono" class="form-control" name="tel" required >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Matrícula" class="form-control" name="matricula"  required >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <select placeholder="Genero" class="form-control" name="genero" required>
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Femenino</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" id="oculto" >

                                            <div class="col-md-12">
                                                <input type="text" id="clave" placeholder="Clave del grupo" class="form-control" name="clave" required>
                                                <input type="hidden" name="tipo_user" value="4" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Registrar
                                                </button>
                                            </div>
                                        </div>
                                   </div>
                            </div>
                    </form>
                </div>
        </div>
    </div>
</div>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
</body>

@endsection