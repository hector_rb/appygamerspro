@extends('Admin.menuAdmin')
@section('content')
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
	.tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #5499C7;
            color: #FDFEFE;
        }

        .f{
            background-color: #48C9B0;
            color: #FDFEFE;
        } 

        .g{
            background-color: #F0B27A;
            color: #FDFEFE;
        } 

        .h{
            background-color: #7DCEA0;
            color: #FDFEFE;
        } 

        .i{
            background-color: #E1B16F;
            color: #FDFEFE;
        }  
</style>

    </head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Expedientes
                </h1>
                <h3 class="font-bold c">Muestran los datos de todos los alumnos de la institución</h3>
            </center>
        </form>
    </div>
    <div class="col-lg-12">
    	<a href="{{URL::to('admin/expedientes')}}">
    	<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" href="{{URL::to('admin/expedientes')}}">Regresar</button></a>
    </div>
    <br>
	<div class="wrapper wrapper-content">
		<div class="row animated fadeInDown">
			<div class="col-lg-3">

				<div class="ibox-content text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$alumno[0]->nombreAlumno}} {{$alumno[0]->apepat}} {{$alumno[0]->apemat}}
                            </h2>
                            </div>
                            <img src="{{asset('img/profile_pics/' .$alumno[0]->imagen)}}" height="170" width="175" class="img-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="widget-text-box">
                        	<form><center>
                            <h1 class="font-bold">{{$alumno[0]->grado}} {{$alumno[0]->grupo}}</h1>
                            <span class="fa fa-calendar m-r-xs"></span>
                            <label>Fecha de nacimiento:</label>
                            <br>{{$alumno[0]->fechanac}}<br>
                            <span class="fa fa-phone m-r-xs"></span>
                            <label>Teléfono:</label>{{$alumno[0]->telefono}}
                            <br>
                            <span class="fa fa-envelope m-r-xs"></span>
                            <label>Correo:</label>{{$alumno[0]->correo}}
                            </center></form>
                        </div>
			</div>
			<div class="col-lg-9">
				<div class="ibox-content" id="trash" >
					<div class="well well-sm text-center d">
						<label>SECCIÓN ACADÉMICA</label>
					</div>
					<div class="row text-center">
					<div class="col-lg-1">
					</div>
					<div class="col-lg-1">
						<i class="fa fa-book fa-5x"></i>
					</div>
					<div class="col-lg-4">
						<h5>Promedio general de calificaciones</h5>
						{{$calif}}

						
					</div>
					<div class="col-lg-1">
						<i class="fa fa-calendar fa-5x"></i>
					</div>
					<div class="col-lg-4">
						<h5>Asistencias del periodo</h5>
						{{$asistencia}}
					</div>
				</div>
	            </div>
			    <div class="ibox-content" id="trash" >
			    	<div class="well well-sm text-center e"> 
			        	<label>INSIGNIAS</label>
			        </div>  
			        	<form> 
			        		<center>	
			            	<Table>
							      	<tr>
							      		<?php $con=1; 
							      		if($insignias != array())
							      		{
							      		?>
							      	@foreach($insignias as $i)
												<th><image src="{{ asset('img/insignias/'.$i->rutaImagen)}}"  height="150" width="125" >
													<br>
													{{$i->fecha}}
												</th>
											<?php
											if($con >= 4)
											{
												echo '</tr>';
												echo '<tr>';
												$con=1;	
											}
											else
											{
												$con++;
											}
											?>
									@endforeach
									<?php 
									}
									?>
									</tr>
							</Table>
						  </center>
						</form>
					</div>			
				<div class="ibox float-e-margins">
					<div class="ibox-content" id="trash" >
						<div class="well well-sm text-center f">
							<label>DATOS PERSONALES</label>
						</div>
						<div class="row vertical-align">
							<div class="col-xs-1">
							</div>
							<div class="col-xs-2">
                                <i class="fa fa-user fa-5x"></i>
                        	</div>
	                        <div class="col-xs-4">
								<label>Nombre de tutor:</label>
								{{$alumno[0]->nombreTutor}} {{$alumno[0]->tutorapepat}} {{$alumno[0]->tutorapemat}}
								<br><span class="fa fa-envelope m-r-xs"></span>
								<label>Correo:</label>
								{{$alumno[0]->tutorcorreo}}
							</div>
							<div class="col-xs-4">
								<br><span class="fa fa-phone m-r-xs"></span>
								<label>Telélono del tutor:</label>
								{{$alumno[0]->tutortelefono}}
								<br><span class="fa fa-phone m-r-xs"></span>
								<label>Celular del tutor:</label>
								{{$alumno[0]->tutorcelular}}
							</div>
						</div>
					</div>
					<div class="ibox-content" id="trash" >
						<div class="well well-sm text-center g">
							<label>CONOCIMIENTOS ADICIONALES</label>
						</div>
						<div class="row vertical-align">
							<div class="col-xs-1"></div>
							<div class="col-xs-2">
								<i class="fa fa-thumbs-up fa-5x"></i>
							</div>
							<br>
							@foreach($conocimientos as $c)
							<h5></h5>
							{{$c->conocimiento1}}
							<h5></h5>
							{{$c->conocimiento2}}
							<h5></h5>
							{{$c->conocimiento3}}
							@endforeach
							<br>
						</div>
					</div>
					<div class="ibox-content" id="trash" >
						<div class="well well-sm text-center h">
							<label>DATOS MÉDICOS</label>
						</div>
						<div class="row vertical-align">
							<div class="col-xs-1"></div>
							<div class="col-xs-2">
								<i class="fa fa-gear fa-5x"></i>
							</div>
								<br>
								<?php
								$a=true;
								?>
								@foreach($datosmedicos as $d)
								<?php
								$a=false;
								?>
									<div class="col-xs-4">
										<form><center>
										<h5>Alergias:</h5>
										{{$d->alergias}}
										<h5>Enfermedades:</h5>
										{{$d->enfermedades}}
										<h5>Enfermedades crónicas:</h5>
										{{$d->enfCro}}
										<h5>Cirugías:</h5>
										{{$d->cirugias}}
										</center></form>
									</div>
									<div class="col-xs-4">
										<form><center>
										<h5>No. del seguro:</h5>
										{{$d->numSeguro}}
										<h5>Tipo de sangre:</h5>
										{{$d->tipoSangre}}
										<h5>Número de emergencia:</h5>
										{{$d->numEme}}
										</center></form>
									</div>
								@endforeach
								<br>
						</div>
						<div class="col-lg-9"></div>
						<a href="{{URL::to('admin/expedientes/datosmedicos?idAlumno='.$alumno[0]->idAlumno)}}">
							<?php 
							if($a)
							{echo "<button class='btn btn-w-m btn-w-m btn-outline btn-primary btn-xs'>Agregar datos adicionales</button></a>"; }
							else
							{echo "<button class='btn btn-w-m btn-w-m btn-outline btn-primary btn-xs' style='visibility:hidden';>Agregar datos adicionales</button></a>"; }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>    

</script>
@endsection
