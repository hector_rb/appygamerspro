@extends('Admin.menuAdmin')
@section('content')
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
     .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

         .d{
            background-color: #6FE1B2;
            color: #FDFEFE;
        }

         .e{
            background-color: #E1B16F;
            color: #FDFEFE;
        }

        .div {
            width: 23%;
        }
</style>

    </head>
    <body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Administrar alumnos
                </h1>
                <h3 class="font-bold c">Muestran los datos de todos los alumnos de la institución</h3>
            </center>
        </form>
    </div>
    <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                       <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/expedientes/altaAlumnos')}}">Agregar alumno</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/expedientes')}}"> <i class="fa fa-inbox "></i> Alumnos</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    <div class="container">
		<div class="col-lg-9 animated fadeInRight">
			<div class="row">
				<div class="ibox-content">
					<div class="table-responsive">
					            <table class="table table-striped table-bordered table-hover dataTables-example" >
					                
					                
					                <thead>
					                    <center>
					                    	<th></th>
					                    	<th>Matricula</th>
					                        <th>Nombre</th>
					                        <th>Grado, Grupo</th>
					                        <th></th>
					                    </center>
					                </thead>
					                <tbody>
					                    @foreach ($alumno as $a)
					                    <tr class="gradeX">
					                        <td><form><center><img src="{{asset('img/profile_pics/' .$a->imagen)}}" height="40" width="40" class="img-circle circle-border m-b-md" alt="profile"></center></form></td>
					                        <td>{{$a->matricula}}</td>
					                        <td>{{$a->apepat}} {{$a->apemat}} {{$a->nombreAlumno}}</td>
					                        <td>{{$a->grado}} {{$a->grupo}}</td> 
					                        <td><a href="{{URL::to('admin/expedientes/'.$a->idAlumno)}}" ><button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Detalles</button></a>
					                        	<!--<a href="{{URL::to('admin/expedientes/datosmedicos?idAlumno='.$a->idAlumno)}}">
												<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Agregar datos médicos</button></a>-->
											</td>
					                    </tr>
					                    @endforeach			                
					                </tbody>
					            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script>
	$(document).ready(function(){
	            $('.dataTables-example').DataTable({
	                pageLength: 25,
	                responsive: true,
	                dom: '<"html5buttons"B>lTfgitp',
	                language: {
						            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						        },
	                buttons: [
	                ]

	            });

	        });
</script>
 </body>
@endsection