@extends('Admin.menuAdmin')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/mensajes/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                             <!-- Muestra el numero de mensajes sin ver -->
                            <li><a href="{{URL::to('admin/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('admin/mensajes/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <li><a href="{{URL::to('/admin/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                     
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">

            <h2>
                Bandeja de entrada
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
              
            </div>
        </div>
            <div class="mail-box">

            <table class="table table-hover table-mail">
            <tbody>
                 <tr>
                <td><strong>Emisor</strong></td>
                <td><strong>Asunto</strong></td>
                <td> </td>
                <td><strong>Fecha</strong></td>
                <td><strong>Borrar</strong></td>
                </tr>
                @foreach($mensajes as $mensajes)

                @php
                if($mensajes->Visto!=0) //selecciona si el mensaje tiene la bandera de visto
                    echo '<tr class="read">' ;
                else
                    echo '<tr class="unread">'
                @endphp


                    @php

                        switch ($mensajes->TipoEmisor) { //en base al tipo de emisor se adjunta el nombre de este
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($alumno as $alumnos) //recorrido para obtener el nombre del emisor
                                {
                                    if($alumnos->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '3':
                                foreach($profesor as $profesores) //recorrido para obtener el nombre del emisor
                                {
                                    if($profesores->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$profesores->Nombre.'</td>';

                                }
                            break;

                        }
                    @endphp


                    <td class="mail-subject"><a href="">{{ $mensajes->Asunto }}</a></td>  <!-- Muestra el asunto del mensaje-->
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td> <!-- Muestra cuando se redacto el mensaje-->

                   <td class="check-mail">
                       <a class="btn btn-w-m btn btn-w-m btn-outline btn-danger btn-xs" href="" data-target="#modal-delete-{{$mensajes->id}}" data-toggle="modal" >Borrar</a>  <!-- modal que permite eliminar un mensaje (mandarlo a la carpeta de eliminados-->
                   </td>

                </tr>
                </div>
                </div>


                @include('Admin.modal')

                @endforeach
            </tbody>
            </table>




            </div>
        </div>
    </div>




@endsection
