@extends('Admin.menuAdmin')
@section('content')
<head>
     <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }
        
    </style>
</head>
<body>
<div class="tooltip-demo">
    <form>
        <center>
            <h1 class="font-bold b">Información general
                <small>
                    <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="aqui puedes guardar la misión, visión, valores y reglamento de tu institucion" aria-describedby="popover955887"></i> 
                </small></h1>
        </center>
    </form>
</div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!!Form::open(['method'=>'PATCH','route'=>['InformacionGeneral.update', $institucion->idInstitucion], 'class'=>''])!!}
            <div class="row">
                <div class="col-lg-6">

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background: #48C9B0;">
                            <center> <h2 style="color: #FEF9E7; font-family: 'Raleway', sans-serif;">Misión</h2></center>
                            
                        </div>
                        <div class="ibox-content">

                        <div class="form-group">
                            <textarea placeholder="Anota una breve descripción de tu misión" class="form-control" rows="9" id="mision" style="resize:none;" name="mision">{{ $institucion->mision }}</textarea>
                        </div>
                        <div class="col-lg-9"></div>
                            <button style="margin-top: 20px;" type="submit" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button>
                    </div>
                    </div>
                    
                </div>
                <div class="col-lg-6">

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background: #85C1E9;">
                            <center><h2 style="color: #FEF9E7; font-family: 'Raleway', sans-serif;">Visión </h2></center>
                            
                        </div>
                        <div class="ibox-content">

                        <div class="form-group">
                            <textarea placeholder="Anota una breve descripción de tu Visión" class="form-control" rows="9" id="vision" style="resize:none;" name="vision">{{ $institucion->vision }}</textarea>
                        </div>
                        <div class="col-lg-9"></div>
                            <button style="margin-top: 20px;" type="submit" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button>
                    </div>
                    </div>
                    
                </div>

            </div>
            <div class="row">
                <div class="col-lg-6">

                    <div class="ibox float-e-margins" >
                        <div class="ibox-title" style="background: #F0B27A;">
                            <center><h2 style="color: #FEF9E7; font-family: 'Raleway', sans-serif;">Valores </h2></center>
                            
                        </div>
                        <div class="ibox-content">

                            <div class="form-group">
                                <textarea placeholder="Aquí puedes enlistar los valores que fomentas en tu colegio" class="form-control" rows="9" style="resize:none;" id="valores" name="valores">{{ $institucion->valores }}</textarea>
                            </div>
                            <div class="col-lg-9"></div>
                            <button style="margin-top: 20px;" type="submit" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-6">

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background: #F1948A;">
                            <center><h2 style="color: #FEF9E7; font-family: 'Raleway', sans-serif;">Reglamento</h2></center>
                            
                        </div>
                        <div class="ibox-content">

                        <div class="form-group">
                            <textarea placeholder="Aquí puedes enlistar tu reglamento" class="form-control" rows="9" style="resize:none;" id="reglamento" name="reglamento">{{ $institucion->reglamento }}</textarea>
                        </div>
                        <div class="col-lg-9"></div>
                            <button style="margin-top: 20px;" type="submit" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Guardar</button>
                    </div>
                    </div>
                    
                </div>

            </div>
            
        {!!Form::close()!!}
    </div>
@endsection
