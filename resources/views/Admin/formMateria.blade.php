 <style type="text/css">
      .form-group
      {
      padding-right: 15%;
      }
</style>


 <div class="row" style="margin-left: 0.5%;margin-right: 0.5%">
            <div class="panel panel-default">
                <div class="panel-heading">Sube un archivo de Excel</div>
 <!--
                @if (Session::has('success-message'))
                     <div class="alert alert-success">{{ Session::get('success-message') }}</div>
                @endif
 
                @if (Session::has('error-message'))
                    <div class="alert alert-danger">{{ Session::get('error-message') }}</div>
                @endif
            -->
 
                <div class="panel-body" style="padding-left:10%">
                    <!--{!! Form::open(array('url' => 'uploadsMateria/save', 'method' => 'post', 'files' => true)) !!}-->
                        
<form id="subirArchivo" enctype="multipart/form-data">
                        <div class="form-group" >
                           <!-- {!! Form::label('file', 'Archivo') !!}-->
                            <input type="text" class="hidden" name="_token" value="{{csrf_token()}}" id="token">
                            <span class="btn btn-default btn-file">
                              <input name="file" type="file">
                            </span>
                            <div class="btn">
                                <button type="button" id="subirExcel" class="btn btn-block btn-info compose-mail" value="Submit">Enviar</button>
                           <!-- {!! Form::submit('Enviar', ["class" => "btn btn-block btn-info compose-mail"]) !!}-->
                            </div>
                            <div class="btn" style="padding-left: 8%">
                                <h3>Descargar plantilla</h3>
                                <a href="{{asset('archivos-excel/PLANTILLA_MATERIA.xlsx')}}" class="btn btn-block btn-info compose-mail">Descargar</a>
                            </div>
                        </div>
                    </form>
                    <!--{!! Form::close() !!}-->
                </div>
            </div>
    </div>


<script type="text/javascript">
    $("#subirExcel").click(function(){ // Cambiar por el id de tu boton
        var formulario = new FormData(); //Declarar el formdata
        formulario.append('file',  $('input[type=file]')[0].files[0]);
         formulario.append('idNivel',parseInt($('#selectNivel').val()));
        console.log($('#selectNivel').val())
         //Agrega el primer archivo del primer input file
        //El primer cero representa que quieres el primero (posición 0) de todos los input type file del HTML y en files que quieres el primero también. 
        //Esto porque puede haber varios inputs de tipo archivo o varios archivos en el mismo input.
        var route = "{{route('materia.save')}}" //Variable que guarda la ruta que te lleva a la función del controlador
        var token = $('#token').val(); //Token definido arriba.
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            dataType: 'json',
            data: formulario,
            contentType: false,
            processData: false,
        }).done(function(response){ // Aquí es donde se recibe el código de error que envíes desde el controlador. 
            switch(response) {
                case 1:
                    // En caso de éxito imprimir la alerta aquí.
                    swal("Se han registrado las materias.");
                    break;
                case 2:
                    // En caso de que se repitan registros imprimir la alerta aquí.
                    swal("Hay registros repetidos.");
                    break;
                case 3:
                    // En caso de ser incorrecto el formato imprimir la alerta aquí.
                    swal("El archivo introducido no es válido.");
                    break;
                case 4:
                    swal("Debes elegir un archivo.");
                    break;
            } 
        }).fail(function(response){
            // Imprimir que tal vez ocurrió un error desconocido, Esto puede o no puede ir
        });

    });
</script>
