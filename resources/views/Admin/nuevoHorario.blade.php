@extends('Admin.menuAdmin')

@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
        
    </style>
</head>

<body>
    <div class="tooltip-demo">
        
            <center>
                <h1 class="font-bold b">Nuevo horario
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="Llena los datos correspondientes, para continuar con la siguiente página" aria-describedby="popover955887"></i> 
                    </small></h1>
                <h3 class="font-bold c">Selecciona el grupo</h3>
            </center>
        
    </div>
<div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::action('horarioController@create')}}">Nuevo horario</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/horario')}}"> <i class="fa fa-inbox "></i>Horarios<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
<div class="col-lg-9">
     <div class="wrapper wrapper-content animated fadeInRight">
        {!!Form::open(['method'=>'POST','route'=>['horario.store'], 'class'=>'form-horizontal'])!!}
            <div class="row" id="first">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                @if (count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                    </ul>
                                </div>
                                @endif
                                
                                
                                    <br><br>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >NIVEL</label>

                                        <div class="col-sm-8">
                                            <select class="form-control" name="nivel" id="selectNivel" required="required">
                                                <option></option>
                                                @foreach($niv as $n)
                                                    <option value="{{$n->idNivel}}">{{$n->nombreNivel}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >GRADO</label>

                                        <div class="col-sm-8">
                                            <select name="grado" id="selectGrados" class="form-control" required="required">
                                                <option> </option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >GRUPO</label>

                                        <div class="col-sm-8">
                                            <select name="grupo" id="selectGrupo" class="form-control" required="required">
                                                <option> </option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >CICLO</label>

                                        <div class="col-sm-8">
                                            <select name="ciclo" id="selectCiclo" class="form-control" required="required">
                                                <option> </option>
                                            </select>
                                        </div>
                                        
                                    </div>
                            </div>
                                        <div class="col-sm-9"></div>
                                        <div class="col-sm-3 control-label">
                                             <button type="button" name="guardar" id="next" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Siguiente</button> 
                                        </div> 
                        </div>
                    </div>
                </div>
                <div class="row" id="second" style="display: none">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                            <div class="ibox-content" id="div">
                                    <div class="form-group">
                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-6 control-label">
                                            
                                            <button type="button" name="nuevo" id="nuevo" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Agregar Materia</button>
                                            <button type="button" name="quitar" id="quitar" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs">Quitar Materia</button>
                                            <button type="submit" name="guardar" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs ">Crear</button>
                                        </div>
                                    </div>                        
                            
                            </div>
                        </div>
                    </div>
                </div>
               {!!Form::close()!!}
    </div>
</div>
            <script >
            var contador=1;
            var mat;

            $('#next').click(function()
                {                    
                    $("#first").hide();
                
                    $("#second").show();
                    var idGrados =$("#selectGrados").val(); 
                    var idNivel = $("#selectNivel").val(); 
                    
                    //-----select profesores
                    var sp=$('<select>', {
                            name: 'profesor[]',
                            id: 'selectProfesor[]',
                            class:'form-control',
                            required:'required',
                            
                        });
                    var lp=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Profesor'
                        });
                    
                    var dp1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var dp2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var dp3=$('<div>', {
                            class:'form-group'
                        });
                    sp.append($('<option>', {
                                
                            }));
                    //-----select materias
                    var s=$('<select>', {
                            name: 'materia[]',
                            id: 'selectMateria[]',
                            class:'form-control',
                            required:'required',
                            on: {
                                    change: function()
                                    {
                                        var idMat = this.value;
                                        sp.find('option').not(':first').remove();
                                        $.ajax({
                                            method: 'get',
                                            url: "{{('../getProf')}}",
                                            data: {idMat:idMat},
                                            dataType: 'json',
                                            success: function (data) {
                                                $.each(data, function (i, item) {
                                                    //console.log(item.nombre);
                                                    sp.append($('<option>', {
                                                        value: item.idProfesor,
                                                        text : item.nombreprof + ' '+ item.apepat + ' ' + item.apemat
                                                    }));
                                                });
                                            }
                                        });

                                    }
                                }
                        });
                    var l=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Materia'
                        });
                    
                    var d1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var d2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var d3=$('<div>', {
                            class:'form-group'
                        });
                    s.append($('<option>', {
                                
                            }));

                    
                    d2.append(s);
                    d3.append(l);
                    d3.append(d2);

                    dp2.append(sp);
                    dp3.append(lp);
                    dp3.append(dp2);



                    $.ajax({
                    method: 'get',
                    url: "{{('../getMaterias')}}",
                    data: {idNivel:idNivel, grado:idGrados},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, item) {
                            //console.log(item.nombre);
                            s.append($('<option>', {
                                value: item.idMateria,
                                text : item.nombreMateria
                            }));
                        });
                    }
                });

                $("#div").append(d3);  
                $("#div").append(dp3);   
            });

            //----nueva materia
            $('#nuevo').click(function()
                {    

                    var idGrados =$("#selectGrados").val(); 
                    var idNivel = $("#selectNivel").val(); 
                    var div=$('<div>',{
                        id:'nombre' + contador
                    });
                    contador++;
                    console.log(contador);
                    div.append($('<div>',{
                        class:'hr-line-dashed'
                    }));
                    //-----select profesores
                    var sp=$('<select>', {
                            name: 'profesor[]',
                            id: 'selectProfesor[]',
                            class:'form-control',
                            required:'required',
                            
                        });
                    var lp=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Profesor'
                        });
                    
                    var dp1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var dp2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var dp3=$('<div>', {
                            class:'form-group'
                        });
                    sp.append($('<option>', {
                                
                            }));
                    //-----select materias
                    var s=$('<select>', {
                            name: 'materia[]',
                            id: 'selectMateria[]',
                            required:'required',
                            class:'form-control',
                            on: {
                                    change: function()
                                    {
                                        var idMat = this.value;
                                        sp.find('option').not(':first').remove();
                                        $.ajax({
                                            method: 'get',
                                            url: "{{('../getProf')}}",
                                            data: {idMat:idMat},
                                            dataType: 'json',
                                            success: function (data) {
                                                $.each(data, function (i, item) {
                                                    //console.log(item.nombre);
                                                    sp.append($('<option>', {
                                                        value: item.idProfesor,
                                                        text : item.nombreprof+ ' '+ item.apepat + ' ' + item.apemat
                                                    }));
                                                });
                                            }
                                        });

                                    }
                                }
                        });
                    var l=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Materia'
                        });
                    
                    var d1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var d2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var d3=$('<div>', {
                            class:'form-group'
                        });
                    s.append($('<option>', {
                                
                            }));
                    d2.append(s);
                    d3.append(l);
                    d3.append(d2);

                    dp2.append(sp);
                    dp3.append(lp);
                    dp3.append(dp2);
                    $.ajax({
                    method: 'get',
                    url: "{{('../getMaterias')}}",
                    data: {idNivel:idNivel, grado:idGrados},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, item) {
                            //console.log(item.nombre);
                            s.append($('<option>', {
                                value: item.idMateria,
                                text : item.nombreMateria
                            }));
                        });
                    }
                });

                div.append(d3);
                div.append(dp3);
                $("#div").append(div);   
            });
            $("#selectNivel").change(function(){
            var idNivel = this.value;

            $('#selectGrados').find('option').not(':first').remove();

            $('#selectGrupo').find('option').not(':first').remove();

            $('#selectCiclo').find('option').not(':first').remove();

            $.ajax({
                method: 'get',
                url: "{{('../getGrados')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        //console.log(item.nombre);
                        $('#selectGrados').append($('<option>', {
                            value: item.grado,
                            text : item.grado
                        }));
                    });
                }
            });
            $.ajax({
                method: 'get',
                url: "{{('../getCiclo')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#selectCiclo').append($('<option>', {
                            value: item.idCiclo,
                            text : item.nombreCiclo
                        }));
                    });
                }
            });
        });
            $("#selectGrados").change(function(){
            var idGrados = this.value;
            var idNivel = $("#selectNivel").val();
            $('#selectGrupo').find('option').not(':first').remove();
            $.ajax({
                method: 'get',
                url: "{{('../getGrupos')}}",
                data: {idNivel:idNivel, grado:idGrados},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#selectGrupo').append($('<option>', {
                            value: item.idgrupo,
                            text : item.grado + item.grupo
                        }));
                    });
                }
            });
        });

        $('#quitar').click(function()
        {
            console.log(contador)
                $('#'+'nombre' + contador).remove();
                contador=contador-1;
                if(contador<=1)
                {
                    contador=1;
                }

        });
        </script>
        </body>
@endsection