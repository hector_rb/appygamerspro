@extends('Admin.menuAdmin')
@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Alerta escolar
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="Una alerta escolar se debe usar solo en una situación de emergencia" aria-describedby="popover955887"></i> 
                    </small></h1>
                <h3 class="font-bold c">Crear una alerta</h3>
            </center>
        </form>
    </div>
<br></br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/alerta/create')}}">Redactar Alerta Escolar</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/alerta/')}}"> <i class="fa fa-inbox "></i>Historial de alertas</a></li>

                        </ul>


                    </div>
                </div>
            </div>
        </div>



        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
                        <h2>Redactar nueva alerta escolar</h2>
                    </div>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        {!!Form::open(array('url'=>'/admin/alerta','method'=>'POST','autocomplete'=>'off', 'files'=>'true' ))!!}
                        {{Form::token()}}

                                <div class="form-group">
                                    {!! Form::label('tipoAlerta', 'Seleccione el tipo de alerta que desea enviar') !!}
                                    <select class="form-control m-b" id="tipoAlerta" name="tipoAlerta" required="true">
                                            <option></option>
                                        @foreach ($Destinatarios as $key)
                                            <option value="{{$key->id}}" >{{$key->Tipo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                  <div class="form-group">
                                        {!! Form::label('descripcion', 'Que es lo que sucede?') !!}
                                        {!! Form::textarea('desc', null,array('placeholder' => 'desc','class' => 'form-control','style'=>'height:150px')) !!}
                                    </div>



                                <div class="form-group">
                                    {!! Form::submit('Enviar', ['class' => 'btn btn-w-m btn-w-m btn-outline btn-info btn-xs' ] ) !!}
                                </div>

                            {!! Form::close() !!}
                    </div>
                </div>
                </div>
            </div>
        </div>







@endsection
