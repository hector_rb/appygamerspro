@extends('Admin.menuAdmin')
@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>  
    <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-9">
                <h1>Alerta escolar</h1>
                <p>Una alerta escolar se debe usar solo en una situación de emergencia</p>
                 
            </div>
            
        </div>
    
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="{{URL::to('admin/alerta/create')}}">Redactar Alerta Escolar</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/alerta')}}"> <i class="fa fa-inbox "></i>Registro de alertas</a></li>
                            <li><a href="{{URL::to('admin/alerta/create')}}"> <i class="fa fa-envelope-o"></i>Enviar alerta escolar</a></li>
                           
                        </ul>
                        <h5>Categories</h5>
                        <ul class="category-list" style="padding: 0">
                            <li><a href="#"> <i class="fa fa-circle text-navy"></i> Work </a></li>
                            <li><a href="#"> <i class="fa fa-circle text-danger"></i> Documents</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-primary"></i> Social</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-info"></i> Advertising</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-warning"></i> Clients</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">
        <div class="row">
        <div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
        <h2>Registro de alertas</h2>
        </div>
        
        <div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">        
<div class="ibox-content">
<div class="table-responsive">
<table class="table table-hover table-mail">
<tbody>
<tr class="unread">
@foreach ($alerta as $a)
<thead>
<td>{{$a->titulo}}</td>
<td>{{$a->descripcion}}</td>
<td>{{$a->created_at}}</td>
<td><a href="" data-target="#modal-delete-{{$a->idAlerta}}" data-toggle="modal"><button class="btn btn-w-m btn-w-m btn-outline btn-danger btn-xs">Eliminar</button></td>
</thead>
@include('Admin.alerta.modal')
@endforeach
</tr>
</tbody>
</table>

</div>
</div>
</div>

        </div>
        </div>
        </div>

</body>	

@endsection

