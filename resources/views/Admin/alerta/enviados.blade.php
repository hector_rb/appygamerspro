@extends('Admin.menuAdmin')
@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Alerta escolar
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="Una alerta escolar se debe usar solo en una situación de emergencia" aria-describedby="popover955887"></i> 
                    </small></h1>
                <h3 class="font-bold c">Una alerta escolar se debe usar solo en una situación de emergencia</h3>
            </center>
        </form>
    </div>
<br></br>
<div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/alerta/create')}}">Crear alerta</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/alerta')}}"> <i class="fa fa-inbox "></i> Historial de alerta<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="ibox-content">
                <table class="table table-hover table-mail">
                     <tr>
                        <th>TipoAlerta</th> 
                        
                        <th>Descripcion</th>
                        <th>Fecha</th>
                        <th></th>
                      </tr>
                    <tbody>

                        @foreach($alerta as $a)
                            <td class="mail-subject">
                                @php
                                switch($a->tipoAlerta )
                                {
                                 case '0':
                                 echo 'Desconocido';
                                 break;
                                 case '1':
                                  echo 'Sismo';
                                 break;
                                 case '2':
                                  echo 'Incendio';
                                 break;
                                 case '3':
                                  echo 'Simulacro';
                                 break;
                                 case '4':
                                  echo 'Violencia Cercana';
                                 break;
                                 case '5':
                                  echo 'Robo';
                                 break;
                                 case '6':
                                  echo 'Otro';
                                 break;
                                 case '7':
                                  echo 'Asegurar Salon';
                                 break;
                                
                                }
                                @endphp
                               
                                </td>
                                
                           <td>{{ $a->descripcion }}</td>
                           <td>{{ $a->created_at}}</td>
                           <td><a href="" data-target="#modal-delete-{{$a->idAlerta}}" data-toggle="modal"><button class="btn btn-w-m btn-w-m btn-outline btn-danger btn-xs">Eliminar</button></td>
                        </tr>
                        @include('Admin.alerta.modal')
                        @endforeach
                    </tbody>
                </table>
            </div>        
        </div> 
</div>
</body>

@endsection
