@extends('Admin.menuAdmin')

@section('content')
<body>
	 <div class="wrapper wrapper-content animated fadeInRight">
	        <div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox float-e-margins">
	                        <div class="ibox-title">
	                            <h2>Agregar Materias</h2>
	                            
	                        </div>
	                        <div class="ibox-content">
	                            @if (count($errors)>0)
	                            <div class="alert alert-danger">
	                                <ul>
	                                @foreach ($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                                @endforeach
	                                </ul>
	                            </div>
	                            @endif
	                            {!!Form::open(['method'=>'POST','route'=>['aMaterias.store'], 'class'=>'form-horizontal'])!!}
	                            

	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >NIVEL</label>

	                                    <div class="col-sm-8">
	                                    	<select class="form-control" name="nivel" id="selectNivel" required="required">
	                                    		<option></option>
	                                    		@foreach($niv as $n)
	                                            	<option value="{{$n->idNivel}}">{{$n->nombreNivel}}</option>
	                                            @endforeach
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >GRADO</label>

	                                    <div class="col-sm-8">
	                                    	<select name="grado" id="selectGrados" class="form-control" required="required">
	                                    		<option> </option>
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >MATERIA</label>

	                                    <div class="col-sm-8">
	                                    	<select name="materia" id="selectMateria" class="form-control" required="required">
	                                    		<option> </option>
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >Profesor</label>

	                                    <div class="col-sm-8">
	                                    	<select class="form-control" name="prof" id="selectNivel" required="required">
	                                    		<option></option>
	                                    		@foreach($prof as $p)
	                                            	<option value="{{$p->idprofesor}}">{{$p->nombreprof}} {{$p->apepat}} {{$p->apemat}} </option>
	                                            @endforeach
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                    <div class="col-sm-9"></div>
	                                    <div class="col-sm-3 control-label">
	                                    	<button type="submit" name="guardar" class="btn btn-success ">Crear</button>
	                                    </div>
                                </div>                        
                            </form>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <script >
	        $("#selectNivel").change(function(){
            var idNivel = this.value;

            $('#selectGrados').find('option').not(':first').remove();

            $('#selectMateria').find('option').not(':first').remove();
            $.ajax({
                method: 'get',
                url: "{{('getGrados')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        //console.log(item.nombre);
                        $('#selectGrados').append($('<option>', {
                            value: item.grado,
                            text : item.grado
                        }));
                    });
                }
            });
        });
	        $("#selectGrados").change(function(){
            var idGrados = this.value;
            var idNivel = $("#selectNivel").val();
            $('#selectMateria').find('option').not(':first').remove();

            $.ajax({
                method: 'get',
                url: "{{('getMaterias')}}",
                data: {idNivel:idNivel, grado:idGrados},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        //console.log(item.nombre);
                        $('#selectMateria').append($('<option>', {
                            value: item.idMateria,
                            text : item.nombreMateria
                        }));
                    });
                }
            });
        });
	        </script>
	    </body>
@endsection