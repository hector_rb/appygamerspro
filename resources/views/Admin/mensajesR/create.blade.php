@extends('Admin.menuAdmin')
@section('content')
<html>
   <head>
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

	<link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

      
    <style type="text/css">
    .swal-title {
      margin: 0px;
      font-size: 10px;
      box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
      margin-bottom: 28px;
    }
    
    .b{
            color: #EB984E;
      }
    </style>
    <body>
         <form>
        <center>
            <h1 class="font-bold b">Crear un nuevo mensaje</h1>
        </center>
    </form>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/mensajesR/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/mensajesR')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('admin/mensajesR/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <!--<li><a href="{{URL::to('/admin/mensajesR/papelera')}}"> <i class="fa fa-trash-o"></i>Papelera</a></li>-->
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/admin/mensajesR','method'=>'POST','autocomplete'=>'off', 'files'=>'true' ))!!}
{{Form::token()}}         
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    Redactar mensaje
                </h2>
            </div>
            <div class="mail-box">
                <div class="mail-body">

                        <form class="form-horizontal" method="get">
                            <div class="form-group"><label class="col-sm-2 control-label">Para:</label>

                                <div class="col-sm-10">
                                <select class="chosen-select" name="IdDestinatario[]" multiple  tabindex="2" required>
                                        
                                        @foreach ($variable as $v)
                                        <option  value="1,{{$v->iduser}}" >Admin|{{$v->Nombre}}</option>
                                        @endforeach
                                        @foreach ($variable2 as $v2)
                                        <option  value="3,{{$v2->iduser}}" >Profesor|{{$v2->name}}</option>
                                        @endforeach
                                        @foreach ($variable3 as $v3)
                                        <option  value="4,{{$v3->iduser}}" >Alumno|{{$v3->grado}}{{$v3->grupo}}|{{$v3->name}}</option>
                                        @endforeach
                                        @foreach ($variable4 as $v4)
                                        <option value="2,{{$v4->iduser}}" >Tutor|{{$v4->name}}</option>
                                        @endforeach
                                </select></div>
                            </div>
                            <br></br>
                            <div class="form-group"><label class="col-sm-2 control-label">Asunto:</label>

                                <div class="col-sm-10"><input type="text" class="form-control" name="Asunto" value="" required="required" placeholder="Asunto..."></div>
                            </div>
                            <br></br>
                            <br></br>
                            <textarea class="form-control" name="Mensaje" placeholder="mensaje..." required="required"></textarea>
                            <br>
                            <input type="file" class="form-control" name="anexo" value="">
                            <br>
                            <div class="col-sm-10"></div>
                            <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>

                        </form>
                </div>
            </div>
            
        </div>
                <!--<div class="col-lg-3"></div>
                <div class="col-lg-9">
                    <div class="ibox">
                        <div class="ibox-content">
                            <form action="#" class="dropzone" id="dropzoneForm">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->
        </div>
{!!Form::close()!!} 
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.chosen-select').chosen();   
    });
</script> 
</body>
@endsection