@extends('Admin.menuAdmin')
@section('content')
<head>
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        
         .b{
            color: #EB984E;
        }
        
    </style>
</head>
<body>

        <form>
        <center>
            <h1 class="font-bold b">Mensajes enviados</h1>
        </center>
    </form>
    <br>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/mensajesR/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/mensajesR')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('admin/mensajesR/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <!--<li><a href="{{URL::to('/admin/mensajesR/papelera')}}"> <i class="fa fa-trash-o"></i>Papelera</a></li>-->
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>


        
        <div class="col-lg-9">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-hover table-mail">
                        <tbody>
                            <thead>
                                        <th></th>
                                        <th>Para:</th>
                                        <th>Asunto:</th>
                                        <th>Fecha:</th>
                            </thead>
                            @foreach ($mensajes as $m)
                            <thead>
                                        <td>
                                                <a href="{{URL::to('admin/mensajesR/verEnviados?id='.$m->id)}}">
                                                <button class="btn btn-white btn-sm fa fa-envelope a"></button></a>
                                                <!--<a href="" data-target="#modal-delete-{{$m->id}}" data-toggle="modal" title="Eliminar"><button class="btn btn-white btn-sm fa fa-trash-o a" data-toggle="tooltip" data-placement="top" title="Eliminar"></button></a>
                                                <!--<a href="" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fa fa-trash-o" data-target="#modal-delete-{{$m->id}}" data-toggle="modal"></i> </a>-->
                                        </td>
                                        <td><strong>{{$m->destinatario}}</strong>|{{$m->usuarios}}</td>
                                        <td>{{$m->Asunto}}</td>
                                        <td><strong>{{$m->fecha}}</strong></td>
                            </thead>
                            @include('Admin.mensajesR.modal')
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection