@extends('Admin.menuAdmin')

@section('content')
<html>
		<title>MyAppCollege | Registro</title>
        <style>
      #map {
        height: 400px;
        width: 85%;
       }
       .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }
      img
    {
        max-width: 100%;
    }
    </style>
	</head>
    <body>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h1>Administración <small>Registro de instituciones</small></h1>
                            
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        
                        <div class="ibox-content">
                            @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                            {!!Form::open(['method'=>'PATCH','route'=>['RegistroInstitucion.update', $array[0]->idInstitucion], 'class'=>'form-horizontal'])!!}
                            

                                <div class="form-group">
                                	<label class="col-sm-2 control-label" >CLAVE</label>

                                    <div class="col-sm-3"><input type="text" name="clave" class="form-control" value="{{ $array[0]->clave}}"></div>
                                    <label class="col-sm-2 control-label">CENTRO EDUCATIVO</label>

                                    <div class="col-sm-5"><input type="text" name="nombre" class="form-control" value="{{ $array[0]->nombre}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >PAIS</label>

                                    <div class="col-sm-4"><select name="pais" class="form-control">
                                            <option value="">  </option>
                                            @foreach($pais as $p)
                                            <option value="{{$p->idPais}}">{{$p->nombrePais}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                    <label class="col-sm-2 control-label" >ESTADO</label>

                                    <div class="col-sm-4"><select name="estado" class="form-control">
                                            <option value="">  </option>
                                            @foreach($estado as $est)
                                            <option value="{{$est->idEstado}}">{{$est->nombreEstado}}</option>
                                            @endforeach
                                    </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >CIUDAD</label>

                                    <div class="col-sm-3"><input type="text" name="ciudad" class="form-control" value="{{ $array[0]->ciudad}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >DIRECCION</label>

                                    <div class="col-sm-9"><input type="text" name="direccion" class="form-control" value="{{ $array[0]->direccion}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >PERSONA DE CONTACTO</label>

                                    <div class="col-sm-9"><input type="text" name="contacto" class="form-control" value="{{ $array[0]->contacto}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >TELEFONO</label>

                                    <div class="col-sm-4"><input type="text" name="telefono" class="form-control" value="{{ $array[0]->telefono}}"></div>
                                    <label class="col-sm-2 control-label">CORREO</label>

                                    <div class="col-sm-4"><input type="text" name="correo" class="form-control" value="{{ $array[0]->correo}}"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">DESCRIPCION</label>

                                    <div class="col-sm-10"><textarea rows="5" maxlength="500" style="resize:none; width: 100%" name="desc">{{ $array[0]->desc}}</textarea></div>
                                </div>
                                <div class="form-group" >
                                    <input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">
                                    <center><div id="map"></div>
                                    <input type="hidden" value="{{$array[0]->lat}}" name="lat" id="lat">
                                    <input type="hidden" value="{{$array[0]->lng}}" name="lng" id="lng">
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9"></div>
                                    <div class="col-sm-3 control-label">
                                    <button type="submit" name="guardar" class="btn btn-success ">Guardar</button></div>
                                </div>    
                                <div class="form-group">
                                    <div class="ibox-content">
                                        <div class="sk-spinner sk-spinner-wave">
                                            <div class="sk-rect1"></div>
                                            <div class="sk-rect2"></div>
                                            <div class="sk-rect3"></div>
                                            <div class="sk-rect4"></div>
                                            <div class="sk-rect5"></div>
                                        </div>
                                    </div>
                                </div>  
                                                       
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
          //function initMap() {

            /*var uluru = {lat: <?= $array[0]->lat ?>, lng: <?= $array[0]->lng ?>};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 18,
              center: uluru
            });*/

            /*var marker = new google.maps.Marker({
              position: uluru,
              map: map
            });
          }*/
           function initAutocomplete() {
            var uluru= {lat:22.149738, lng:-100.987120 }
        var map = new google.maps.Map(document.getElementById('map'), {
          center: uluru ,
          zoom: 18,
          mapTypeId: 'roadmap'
        });
        var marker = new google.maps.Marker({
              position: uluru,
              map: map});
          

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");

              return;
            }
            var lat =document.getElementById('lat');
            lat.value=place.geometry.location.lat();
            console.log(place.geometry.location.lat());
            var lng =document.getElementById('lng');
            lng.value=place.geometry.location.lng();
            console.log(place.geometry.location.lng());
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
              
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
//logo institucion
        var $image = $("#image")
        var cropper;
        $image.cropper({
            aspectRatio: 5 / 1,
            preview: "#preview",
            
            crop: function(e) {
                // Output the result data for cropping image.
                
                console.log(e.x);
                console.log(e.y);
                console.log(e.width);
                console.log(e.height);
                console.log(e.rotate);
                console.log(e.scaleX);
                console.log(e.scaleY);
            },            
        });
        var $inputImage = $("#inputImage2");
        if (window.FileReader) {
            $inputImage.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage.val();
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $('#saveImage').click(function(){

                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

                $image.cropper('getCroppedCanvas').toBlob(function (blob) {
                

                var formData = new FormData();
                var token = $("input[name=_token]").val();
                formData.append('croppedImage',$image.cropper('getCroppedCanvas',{
                  
                  height: 256,
                  
                }).toDataURL("image/png"));
                formData.append("_token", token);
                formData.append("tipo", 1);
                
                // Use `jQuery.ajax` method
                $.ajax( {
                url:"{{('saveLogo')}}",
                type:'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    swal('Logo de la escuela guardado')
                },
                error: function (e) {
                console.log(e.responseJSON);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                swal('Ocurrio un error, no se almaceno el logo de la escuela')
                }
                });
            });
        });

               
        

        //Banner

        var $image2 = $("#image2")
        var cropper;
        $image2.cropper({
            aspectRatio: 5 / 3,
            preview: "#preview2",
            crop: function(e) {
                // Output the result data for cropping image.
                
                console.log(e.x);
                console.log(e.y);
                console.log(e.width);
                console.log(e.height);
                console.log(e.rotate);
                console.log(e.scaleX);
                console.log(e.scaleY);
            },            
        });
        var $inputImage2 = $("#inputImage");
        if (window.FileReader) {
            $inputImage2.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage2.val();
                        $image2.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage2.addClass("hide");
        }

        $('#saveImage2').click(function(){

                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

                $image2.cropper('getCroppedCanvas').toBlob(function (blob) {
                

                var formData = new FormData();
                var token = $("input[name=_token]").val();
                formData.append('croppedImage',$image2.cropper('getCroppedCanvas',{
                  
                  height: 350,
                  
                }).toDataURL("image/png"));
                formData.append("_token", token);
                formData.append("tipo", 2);
                
                // Use `jQuery.ajax` method
                $.ajax( {
                url:"{{('saveLogo')}}",
                type:'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    swal('Imagen de Banner guardada')
                },
                error: function (e) {
                    console.log(e.responseJSON);
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    swal('Ocurrio un error, no se pudo guardar la imágen de banner')
                }
                });
            });
        });
        </script>
       

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRBXwLlp6la2p9BbKSjEwJdryxcnW1lHc&libraries=places&callback=initAutocomplete"
         async defer></script>
        
        
    </body>
</html>
@endsection