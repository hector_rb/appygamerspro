@extends('Admin.menuAdmin')

@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
        
    </style>
</head>
<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Administración de horarios
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="Se muestran los horarios actuales, tambien puedes agregar nuevos horarios, eliminarlos y editarlos." aria-describedby="popover955887"></i> 
                    </small></h1>
                <h3 class="font-bold c">Se muestran los horarios actuales</h3>
            </center>
        </form>
    </div>

    <!--<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h1>Administracion de Horarios <small>Se muestran los horarios actuales</small></h1>
             
        </div>
        <div class="col-lg-3">
            <br><br>
            <a href="{{URL::action('horarioController@create')}}"><button type="button" name="guardar" id="next" class="btn btn-success ">Nuevo Horario</button> </a>
        </div>
        
    </div>-->
 <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::action('horarioController@create')}}">Nuevo horario</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/horario')}}"> <i class="fa fa-inbox "></i>Horarios<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
<div class="col-lg-9">
	 <div class="wrapper wrapper-content animated fadeInRight">
	 	
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="ibox float-e-margins">
	                    
	                    <div class="ibox-content">

	                        <div class="table-responsive">
			                    <table class="table table-striped table-bordered table-hover dataTables-example" >
				                    <thead>
					                    <tr>
					                        <th>Nivel</th>
					                        <th>Grupo</th>
					                        <th>Ciclo</th>
					                        <th>Opciones</th>
					                    </tr>
				                    </thead>
				                    <tbody>
					                    @foreach($horarios as $h)
					                    	<tr class="gradeX">
						                    	<td>{{$h->nombreNivel}}</td>
						                    	<td>{{$h->grado.' '.$h->grupo}}</td>
						                    	<td>{{$h->nombreCiclo}}</td>
						                    	<td><a href="{{URL::action('horarioController@edit',['grupo'=>$h->idGrupo,'ciclo'=>$h->idCiclo])}}"><button class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Editar</button></a>
						                    	<a href="{{URL::action('horarioController@show',['grupo'=>$h->idGrupo,'ciclo'=>$h->idCiclo])}}"><button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Ver</button></a>
						                    	</td>
						                    </tr>
					                    @endforeach
				                    </tbody>
			                    </table>
	                        </div>

	                    </div>
	                </div>
	            </div>
            </div>
	</div>
</div>
	        <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
		    <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

		    <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>

	        <script >
	        $(document).ready(function(){
	            $('.dataTables-example').DataTable({
	                pageLength: 25,
	                responsive: true,
	                dom: '<"html5buttons"B>lTfgitp',
	                language: {
						            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						        },
	                buttons: [
	                ]

	            });

	        });
	        var mat;
	        var horarios=<?= json_encode($horarios)?>;
	        console.log(horarios);

	        $('#next').click(function()
                {                    
                   	$("#first").hide();
                
                    $("#second").show();
                    var idGrados =$("#selectGrados").val(); 
            		var idNivel = $("#selectNivel").val(); 
                    
            		//-----select profesores
            		var sp=$('<select>', {
                            name: 'profesor[]',
                            id: 'selectProfesor[]',
                            class:'form-control',
                            
                        });
                    var lp=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Profesor'
                        });
                    
                    var dp1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var dp2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var dp3=$('<div>', {
                            class:'form-group'
                        });
                    sp.append($('<option>', {
	                            
	                        }));
                    //-----select materias
                    var s=$('<select>', {
                            name: 'materia[]',
                            id: 'selectMateria[]',
                            class:'form-control',
                            on: {
								    change: function()
								    {
								    	var idMat = this.value;
								    	sp.find('option').not(':first').remove();
								    	$.ajax({
							                method: 'get',
							                url: "{{('getProf')}}",
							                data: {idMat:idMat},
							                dataType: 'json',
							                success: function (data) {
							                    $.each(data, function (i, item) {
							                        //console.log(item.nombre);
							                        sp.append($('<option>', {
							                            value: item.idProfesor,
							                            text : item.nombreprof
							                        }));
							                    });
							                }
							            });

								    }
								}
                        });
                    var l=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Materia'
                        });
                    
                    var d1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var d2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var d3=$('<div>', {
                            class:'form-group'
                        });
                    s.append($('<option>', {
	                            
	                        }));
                    d2.append(s);
                    d3.append(l);
                    d3.append(d2);

                    dp2.append(sp);
                    dp3.append(lp);
                    dp3.append(dp2);
                    $.ajax({
	                method: 'get',
	                url: "{{('getMaterias')}}",
	                data: {idNivel:idNivel, grado:idGrados},
	                dataType: 'json',
	                success: function (data) {
	                    $.each(data, function (i, item) {
	                        //console.log(item.nombre);
	                        s.append($('<option>', {
	                            value: item.idMateria,
	                            text : item.nombreMateria
	                        }));
	                    });
	                }
	            });

                $("#div").append(d3);  
                $("#div").append(dp3);   
            });

            //----nueva materia
            $('#nuevo').click(function()
                {    

                    var idGrados =$("#selectGrados").val(); 
            		var idNivel = $("#selectNivel").val(); 
                    $("#div").append($('<div>',{
                    	class:'hr-line-dashed'
                    }));
            		//-----select profesores
            		var sp=$('<select>', {
                            name: 'profesor[]',
                            id: 'selectProfesor[]',
                            class:'form-control',
                            
                        });
                    var lp=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Profesor'
                        });
                    
                    var dp1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var dp2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var dp3=$('<div>', {
                            class:'form-group'
                        });
                    sp.append($('<option>', {
	                            
	                        }));
                    //-----select materias
                    var s=$('<select>', {
                            name: 'materia[]',
                            id: 'selectMateria[]',
                            class:'form-control',
                            on: {
								    change: function()
								    {
								    	var idMat = this.value;
								    	sp.find('option').not(':first').remove();
								    	$.ajax({
							                method: 'get',
							                url: "{{('getProf')}}",
							                data: {idMat:idMat},
							                dataType: 'json',
							                success: function (data) {
							                    $.each(data, function (i, item) {
							                        //console.log(item.nombre);
							                        sp.append($('<option>', {
							                            value: item.idProfesor,
							                            text : item.nombreprof
							                        }));
							                    });
							                }
							            });

								    }
								}
                        });
                    var l=$('<label>', {
                            class:'col-sm-2 control-label',
                            text:'Materia'
                        });
                    
                    var d1=$('<div>', {
                            class:'col-sm-3 control-label'
                        });
                    var d2=$('<div>', {
                            class:'col-sm-9'
                        });
                    var d3=$('<div>', {
                            class:'form-group'
                        });
                    s.append($('<option>', {
	                            
	                        }));
                    d2.append(s);
                    d3.append(l);
                    d3.append(d2);

                    dp2.append(sp);
                    dp3.append(lp);
                    dp3.append(dp2);
                    $.ajax({
	                method: 'get',
	                url: "{{('getMaterias')}}",
	                data: {idNivel:idNivel, grado:idGrados},
	                dataType: 'json',
	                success: function (data) {
	                    $.each(data, function (i, item) {
	                        //console.log(item.nombre);
	                        s.append($('<option>', {
	                            value: item.idMateria,
	                            text : item.nombreMateria
	                        }));
	                    });
	                }
	            });

                $("#div").append(d3);  
                $("#div").append(dp3);   
            });
	        $("#selectNivel").change(function(){
            var idNivel = this.value;

            $('#selectGrados').find('option').not(':first').remove();

            $('#selectGrupo').find('option').not(':first').remove();

            $('#selectCiclo').find('option').not(':first').remove();

            $.ajax({
                method: 'get',
                url: "{{('getGrados')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        //console.log(item.nombre);
                        $('#selectGrados').append($('<option>', {
                            value: item.grado,
                            text : item.grado
                        }));
                    });
                }
            });
            $.ajax({
                method: 'get',
                url: "{{('getCiclo')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#selectCiclo').append($('<option>', {
                            value: item.idCiclo,
                            text : item.nombreCiclo
                        }));
                    });
                }
            });
        });
	        $("#selectGrados").change(function(){
            var idGrados = this.value;
            var idNivel = $("#selectNivel").val();
            $('#selectGrupo').find('option').not(':first').remove();
            $.ajax({
                method: 'get',
                url: "{{('getGrupos')}}",
                data: {idNivel:idNivel, grado:idGrados},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#selectGrupo').append($('<option>', {
                            value: item.idgrupo,
                            text : item.grado + item.grupo
                        }));
                    });
                }
            });
        });
	    </script>
	    </body>
@endsection