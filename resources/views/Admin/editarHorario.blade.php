@extends('Admin.menuAdmin')

@section('content')
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css" />
	<style>
	.tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
    .swal-title {
  margin: 0px;
  font-size: 10px;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
  margin-bottom: 28px;
}
.naranja {
    background-color: #f8ac59;
    border-color: #f8ac59;
    color: #FFFFFF;
    padding: 10px; 
	}
	.verde{
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul1 {
    background-color: #1c84c6;
    border-color: #1c84c6;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul2 {
    background-color: #23c6c8;
    border-color: #23c6c8;
    color: #FFFFFF;
    padding: 10px; 
	}
	.rojo {
    background-color: #ed5565;
    border-color: #ed5565;
    color: #FFFFFF;
    padding: 10px; 
	}

	  .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
         .e{
            background-color: #3498DB;
            color: #FDFEFE;
        }

         .f{
            background-color: #1ABC9C;
            color: #FDFEFE;
        }
</style>

	</head>
	<body>

	<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Editar horarios
                </h1>
            </center>
        </form>
    </div>
    <div class="col-lg-12">
    	<a href="{{URL::to('admin/horario')}}">
    	<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" href="{{URL::to('admin/horario')}}">Regresar</button></a>
    </div>
    <br><br>
    

		<div class="wrapper wrapper-content">
		    <div class="row animated fadeInDown">
		    	
		        <div class="col-lg-3">
		            <div class="ibox float-e-margins ">
		                <div class="ibox-title e">
		                    <h5>Editar horario</h5>
		                    
		                </div>
		                <div class="ibox-content">
		                    <div id='external-events'>
		                        <div class="form-group">
                                	<label class="control-label" >Selecciona una materia</label>

                                </div>
                                <div class="form-group">

                                    <select name="materia" id="selectMateria" class="form-control" >
                                    	<option></option>
	                                    		@foreach($matgrupo as $n)
	                                            	<option value="{{$n->idSubgrupo}}">{{$n->nombreMateria. ' - ' . $n->nombreprof. ' ' . $n->apepat. ' '.$n->apemat.' | '.$n->matricula}}</option>
	                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                	<label class="control-label">Color</label>
                                </div>
                                <div class="form-group">
                                	<div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
										<label class="btn btn-warning">
											<input type="radio" name="priority" id="option1" value="naranja" checked>
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-primary">
											<input type="radio" name="priority" id="option2" value="verde">
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-success">
											<input type="radio" name="priority" id="option3" value="azul1">
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-info">
											<input type="radio" name="priority" id="option4" value="azul2">
											<i class="fa  txt-color-white"></i> </label>
										<label class="btn btn-danger">
											<input type="radio" name="priority" id="option5" value="rojo">
											<i class="fa  txt-color-white"></i> </label>
										
									</div>
								</div>
                                <div class="form-group">
                                	<i  class="btn btn-warning" id="evento"  >Crear</i>
                                </div>


		                        
		                    </div>
		                </div>
		            </div>
		            
		            <div class="ibox-content" >
		                <div class="external-events" id="eventos">
		                    <h2>Periodo</h2> 
		                </div>
		            </div>
		        </div>
		        <div class="col-lg-9">

		            <div class="ibox float-e-margins">
		                <div class="ibox-title f">
	                        <h3>Edición de Horario de {{$datos[0]->grado}} {{$datos[0]->grupo}} de {{$datos[0]->nombreNivel}}</h3>
	                        
	            </div>
		                <div class="ibox-content">
		                    <div id="calendar"></div>
		                </div>
		                <div class="ibox-content" id="trash" >
		                <div class="external-events" >
		                    <center><span class="fa fa-trash"  style="font-size: 100px"></span></center>
		                </div>
		            </div>
		            </div>
		        </div>
		    </div>
		    </div>
		</div>
		
		<!-- Mainly scripts -->
        <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
        
        <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

        

		<script>
			/*$("#selectMateria").change(function(){
            var idMat = this.value;

            $('#selectProf').find('option').not(':first').remove();
            $.ajax({
                method: 'get',
                url: "{{('getProf')}}",
                data: {idMat:idMat},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        //console.log(item.nombre);
                        $('#selectProf').append($('<option>', {
                            value: item.idProfesor,
                            text : item.nombreprof
                        }));
                    });
                }
            });
        });*/
			/*$(document).bind("contextmenu",function(e) {
     e.preventDefault();
});*/
			var eventos= <?= json_encode($horario)?>;
			console.log(eventos);
			var idCiclo={{$idCiclo}};
			var idGrupo={{$idGrupo}};

			console.log(idCiclo);
			console.log(idGrupo);
			var currentMousePos = {
			    x: -1,
			    y: -1
			};
			 
			jQuery(document).on("mousemove", function (event) {
			   currentMousePos.x = event.pageX;
			   currentMousePos.y = event.pageY;
			});

		    $(document).ready(function() {

		            $('.i-checks').iCheck({
		                checkboxClass: 'icheckbox_square-green',
		                radioClass: 'iradio_square-green'
		            });

		        /* initialize the external events
		         -----------------------------------------------------------------*/
		         $( "#evento" ).click(function() {

		         	//$( "<div><p>Hello</p></div>" ).appendTo( "#eventos" )

		         	if ($('#titulo').val()!='' && $('#desc').val()!='') 
		         	{
		         	     $( "<div></div>", {
						  "class": $('input[name=priority]:checked').val(),
						  html: '<h4>' + $('#selectMateria option:selected').text().replace(/\s\s+/g, ' ') + '</h4> ',
						  val:$('#selectMateria option:selected').val(), 
						  on: {
						    mouseover: function( event ) {
						      // Do something
						      	var content = $(this).text().split('-');
						      	var divclass = $(this).attr("class");
						      	var value=$(this).val();
						      	//console.log(content);
						      	$(this).data('event', {

							      	


					                title:content[0], // use the element's text as the event title
					                description:content[1],
					                stick: true, // maintain when user navigates (see docs on the renderEvent method)
					                val:value,
					                editable:true,
					                resourceEditable: true ,
					                className:divclass,
				            });

				            // make the event draggable using jQuery UI
				            $(this).draggable({
				                zIndex: 1111999,
				                revert: true,      // will cause the event to go back to its
				                revertDuration: 0  //  original position after the drag

				            });
								    }
								  }

						}).appendTo( "#eventos" )/*.data('event', {
		                title: $.trim($(this).text()), // use the element's text as the event title
		                stick: true // maintain when user navigates (see docs on the renderEvent method)
		            	}).draggable({
			                zIndex: 1111999,
			                revert: true,      // will cause the event to go back to its
			                revertDuration: 0  //  original position after the drag
			            	});*/
			         }
			         else
			         {
			         	swal({
			                title: "No se puede crear el evento",
			                text: "No has llenado todos los campos",
			                type:'warning'
			                
			            });

			         }


					});

		        $('#external-events div.external-event').each(function() {

		            // store data so the calendar knows to render an event upon drop
		            $(this).data('event', {
		                title: $.trim($(this).text()), // use the element's text as the event title
		                stick: true // maintain when user navigates (see docs on the renderEvent method)
		            });

		            // make the event draggable using jQuery UI
		            $(this).draggable({
		                zIndex: 1111999,
		                revert: true,      // will cause the event to go back to its
		                revertDuration: 0  //  original position after the drag
		            });

		        });


		        /* initialize the calendar
		         -----------------------------------------------------------------*/
		        var date = new Date('2017-10-07');
		        var d = date.getDate();
		        var m = date.getMonth();
		        var y = date.getFullYear();		      
				var zone = "-05:00";

				function isElemOverDiv() {
				   var trashEl = jQuery('#trash');
				   var ofs = trashEl.offset();
				   var x1 = ofs.left;
				   var x2 = ofs.left + trashEl.outerWidth(true);
				   var y1 = ofs.top;
				   var y2 = ofs.top + trashEl.outerHeight(true);
				   if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&      currentMousePos.y >= y1 && currentMousePos.y <= y2) 
				   	{      return true;    }    
				   return false; 
				}
		        $('#calendar').fullCalendar({
		        	locale:'es',
		        	contentHeight:500,
		            header: {
		                left: '',
		                center: '',

		                right: ''
		            },
		            views: {
				        settimana: {
				            type: 'agendaWeek',
				            duration: {
				                days: 7
				            },
				            title: 'Apertura',
				            columnFormat: 'dddd', // Format the day to only show like 'Monday'
				            hiddenDays: [0, 7] // Hide Sunday and Saturday?
				        }
				    },
				    defaultView: 'settimana',
					allDaySlot: false,
		            editable: true,
		            minTime: "07:00:00",
  					maxTime: "21:00:00",
  					defaultDate:date,
  					slotDuration:'00:10:00',
		            droppable: true, // this allows things to be dropped onto the calendar
		            drop: function() {
		                // is the "remove after drop" checkbox checked?
		                //if ($('#drop-remove').is(':checked')) {
		                    // if so, remove the element from the "Draggable Events" list
		                    $(this).remove();
		                //}
		            },
					eventReceive: function(event){
					    var sg = event.val;
					    console.log(sg);
					    var start = event.start.format();
					    var clase= event.className[0];
					    var dia = event.start.format('dddd');
					    var horaini = event.start.format('h:mm:ss a');
					    $.ajax({
					      url: "{{('../../saveHorario')}}",
					      data: {type:1,sg:sg,start:start,zone:zone, ciclo:idCiclo,grupo:idGrupo,dia:dia,ini:horaini,class:clase},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					        event.id = response.eventid;
					        $('#calendar').fullCalendar('updateEvent',event);
					        console.log('funciona');
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					    $('#calendar').fullCalendar('updateEvent',event);
					},
					
					eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) 
					{	
						var start = event.start.format();
					    var end = (event.end == null) ? start : event.end.format();
					    var dia = event.start.format('dddd');
					    var horaini = event.start.format('h:mm:ss a');
					    var horafin= (event.end == null) ? start : event.end.format('h:mm:ss a');
					    $.ajax({
					      url: "{{('../../saveHorario')}}",
					      data: {type:2,end:end, start:start,id:event.id,dia:dia,horaini:horaini, horafin:horafin},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					},
					eventDrop: function( event, delta, revertFunc, jsEvent, ui, view )
					{
						var start = event.start.format();
					    var end = (event.end == null) ? start : event.end.format();
					    var dia = event.start.format('dddd');
					    var horaini = event.start.format('h:mm:ss a');
					    var horafin= (event.end == null) ? start : event.end.format('h:mm:ss a');
					    $.ajax({
					      url: "{{('../../saveHorario')}}",
					      data: {type:2,end:end, start:start,id:event.id,dia:dia,horaini:horaini, horafin:horafin},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					},
					eventDragStop: function (event, jsEvent, ui, view) {
									   if (isElemOverDiv()) {
									     //var con = confirm('Are you sure to delete this event permanently?');
									     swal({
								                title: "¿Seguro que quieres eliminar el evento?",
								                text: "Esta acción sera permanente",
								                type:'warning',
								                showCancelButton: true, 
								            }).then(function(desc) {
												   //event.description=desc.inputValue;
													console.log(event.id);
												   $.ajax({
												     url: "{{('../../saveHorario')}}",
												     data:{type:3,id:event.id},
												     type: 'get',
												     dataType: 'json',
												     success: function(response){
												       if(response.status == 'success')
												       $('#calendar').fullCalendar('removeEvents', event.id);
          											   
												     },
												     error: function(e){
												       alert('Error processing your request: '+e.responseText);
												     }
												   });
						  						}
								            );
									     /*if(con == true) {
									        $.ajax({
									          url: 'process.php',
									          data: 'type=remove&eventid='+event.id,
									          type: 'POST',
									          dataType: 'json',
									          success: function(response){
									            if(response.status == 'success')
									              $('#calendar').fullCalendar('removeEvents');
									              $('#calendar').fullCalendar('addEventSource', JSON.parse(json_events));
									          },
									          error: function(e){
									          alert('Error processing your request: '+e.responseText);
									          }
									       });
									      }*/
									    }
									},
					events:eventos,
		            eventRender: function(event, element) { 
            		element.find('.fc-title').append("<br/>" + event.description);
            		} 
		        });


		    });

		</script>

	</body>
	@endsection
	