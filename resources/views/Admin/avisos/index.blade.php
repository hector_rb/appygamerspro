@extends('Admin.menuAdmin')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }
        
    </style>
</head>
<body>
<div class="tooltip-demo">
    <form>
        <center>
            <h1 class="font-bold b">Bienvenido al sistema de avisos
            <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes consultar, enviar los avisos para la institución." aria-describedby="popover955887"></i> 
                    </small>
            </h1>
        </center>
    </form>
</div>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/avisos/create')}}">Crear aviso</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/avisos')}}"> <i class="fa fa-inbox "></i> Historial de avisos<span class="label label-warning pull-right"></span></a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

<div class="col-lg-9">
       <div class="wrapper wrapper-content">
        <div class="row">
            
            <div class="col-lg-12 animated fadeInRight">
            
                <div class="ibox-content">

                <table class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                    <tr>
                        <th></th>
                        <th>Asunto</th>
                        <th>Destinatarios</th>
                        <th class="text-right mail-date">Fecha</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($avisos2 as $a)
                    <tr class="read">
                        
                        <td align="right"><a href="{{URL::action('avisosAdminController@show',['id'=>$a->idAviso])}}"
                            title="Ver aviso"><button class="btn btn-white btn-sm fa fa-bell a"></button></a>
                        <a href="" data-target="#modal-delete-{{$a->idAviso}}" data-toggle="modal" title="eliminar"><button class="btn btn-white btn-sm fa fa-trash-o a"></button></a></td>
                        <td class="mail-subject"><a href="mail_detail.html">{{$a->asunto}}</a></td>
                        @if($a->destinatario==1)
                            <td>General</td>
                        @endif
                        @if($a->destinatario==2)
                            <td>Tutores</td>
                        @endif
                        @if($a->destinatario==3)
                            <td>Profesores</td>
                        @endif
                        @if($a->destinatario==4)
                            <td>Alumnos</td>
                        @endif
                        @if($a->destinatario==5)
                            <td>Todos los grupos</td>
                        @endif
                        @if($a->destinatario==6)
                            <td>Grupo {{$a->grado}}{{$a->grupo}}</td>
                        @endif
                        <td class="text-right mail-date"><a href="mail_detail.html">{{$a->fecha}}</a></td>
                    </tr>
                    @include('Admin.avisos.modal')
                @endforeach
                
                </tbody>
                </table>
                {{ $avisos->links() }}

                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    language: {
                                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                                },
                    buttons: [
                    ]

                });

            });
</script>
 </body>

@endsection