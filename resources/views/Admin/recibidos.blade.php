@extends('Admin.menuAdmin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h1>Bienvenido al sistema de mensajes</h1>
             
        </div>
        
    </div>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('admin/mensajes/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('admin/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('admin/mensajes/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <li><a href="{{URL::to('admin/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Papelera</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">




            {!! Form::open(['route' => 'BuscaAdmin', 'method' => 'post', 'novalidate', 'class' => 'pull-right mail-search']) !!}
             <div class="input-group">
                 <input type="text" class="form-control input-sm" name="Asunto" placeholder="Buscar">
                 <div class="input-group-btn">
                     <button type="submit" class="btn btn-sm btn-info">
                         Buscar
                     </button>
             </div>
         </div>

            {!! Form::close() !!}


            <h2>
                Bandeja de entrada
            </h2>
           
        </div>
            <div class="mail-box">

            <table class="table table-hover table-mail">
            <tbody>
                <tr>
                <td><strong>Tipo</strong></td>
                <td><strong>Emisor</strong></td>
                <td><strong>Asunto</strong></td>
                <td> </td>
                <td><strong>Fecha</strong></td>
                <td><strong>Borrar</strong></td>
                </tr>
                    
                @foreach($mensajes as $mensajes)

                @php
                if($mensajes->Visto!=0) //permite ver si el mensaje ya fue visto o sigue sin ser mostrado
                    echo '<tr class="read">' ; 
                else
                    echo '<tr class="unread">'
                @endphp


                    @php
                          
                         if($mensajes->TipoEmisor==1) //permite poner las estiqutas deacuerdo al usuario que envio el mensaje (actualmente solo esta el 1 y el 3 que 1 es admin y 3 profesor
                        echo '<td> <span class="label label-danger pull-right">Admin</span>  </td>';
                        
                    else

                        echo '<td> <span class="label label-success pull-right">Profe</span></td>';
                        
                    switch ($mensajes->TipoEmisor) { //este switch permite ver quien envio el mensaje, actualmente solo esta el tipo 1 admin y 3 profesor
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($alumno as $alumnos) //a pesar de que dice alumno es administrador
                                {
                                    if($alumnos->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }
                            break;

                            case '3':
                                foreach($profesor as $profesores)// filtra profesores
                                {
                                    if($profesores->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$profesores->Nombre.'</td>';
                                }
                            break;

                        }
                    @endphp


                    <td class="mail-subject"><a href="{{ route('MuestraAdmin',['id' => $mensajes->id])}}">{{ $mensajes->Asunto }}</a></td>  <!-- Muestra el asunto del mensaje y es un href para abrir su contentido  -->
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td> <!-- Muestra cuando fue creado el mensaje -->

                   <td class="check-mail">
                       <a class="btn btn-w-m btn btn-w-m btn-outline btn-danger btn-xs" href="" data-target="#modal-delete-{{$mensajes->id}}" data-toggle="modal" >Borrar</a>  <!--permite borrar el mensaje por medio de un modal y solo se le pasa el id -->
                   </td>

                </tr>
                </div>
                </div>

                 <!-- Modal que se abre para eliminar el mensaje -->
                <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$mensajes->id}}">
                    <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-Label="Close">
                                    <span aria-hidden="true">x</span>
                                    </button>
                                    <h4 class="modal-title">Eliminar tarea</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Confirme si desea Eliminar tarea</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cerrar</button>
                                    <a class="btn btn-w-m btn-outline btn-danger" href="{{ route('PapeleraAdmin',['id' => $mensajes->id] )}}" >Borrar</a>
                                </div>
                            </div>
                    </div>
                </div>

                </div>
                </div>
                </div>


                @endforeach
            </tbody>
            </table>




            </div>
        </div>
    </div>




@endsection
