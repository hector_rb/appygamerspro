@extends('Admin.menuAdmin')
@section('content')
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
     <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
        /* css juegos */
                 .card {
                 /* Add shadows to create the "card" effect */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                }

                /* On mouse-over, add a deeper shadow */
                .card:hover {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }

                /* Add some padding inside the card container */
                .container {
                    padding: 2px 16px;
                }
</style>
</head>

<body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Preguntas Registradas para AppyGames
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección podrá visualizar las preguntas registradas" aria-describedby="popover955887"></i> 
                    </small></h1>
                    <h3 class="font-bold c">En esta sección se puede manejar todo lo relacionado a las preguntas de los juegos</h3>
            </center>
        </form>
    </div>     
    <div class="row">
       

<div class="col-lg-12">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
        
            <div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                    
                        <thead>
                            <th>Pregunta</th>
                            <th>Categoría</th>
                            <th>Opción 1</th>
                            <th>Opción 2</th>
                            <th>Opción 3</th>
                            <th>Opción 4</th>
                            <th>Respuesta</th>
                        </thead>
                        <tbody>
                            @foreach($preguntas as $pre)
                            <tr>
                                <script type="text/javascript">
                                    switch({{$pre->id_categoria}})
                                    {
                                       case 1:
                                       $cat="Español";
                                       break;
                                       case 2 :
                                       $cat="Matemáticas";
                                       break;
                                       case 3 :
                                       $cat= "Historia";
                                       break;
                                       case 4 :
                                       $cat= "Arte";
                                       break;
                                       case 5 :
                                       $cat= "Música";
                                       break;
                                       case 6 :
                                       $cat= "Ciencias Naturales";
                                       break;
                                       case 7 :
                                       $cat= "Geografía";
                                       break;
                                       case 8 :
                                       $cat= "Tecnología";
                                       break;
                                       case 9 :
                                       $cat= "Cívica y Ética";
                                       break;
                                       case 10 :
                                       $cat= "Deportes";
                                       break;
                                       default:
                                    }
                                </script>
                                <td>{{$pre->pregunta}}</td>
                                <td> <script type="text/javascript">document.writeln($cat)</script></td>
                                <td>{{$pre->opcion1}}</td>
                                <td>{{$pre->opcion2}}</td>
                                <td>{{$pre->opcion3}}</td>
                                <td>{{$pre->opcion4}}</td>
                                <td>{{$pre->respuesta}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>            
    </div>
</div>
<script type="text/javascript">
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>

</body>
@endsection
