@extends('Admin.menuAdmin')
@section('content')
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
<style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
        /* css juegos */
                 .card {
                 /* Add shadows to create the "card" effect */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                }

                /* On mouse-over, add a deeper shadow */
                .card:hover {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }

                /* Add some padding inside the card container */
                .container {
                    padding: 2px 16px;
                }
</style>
</head>
<body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Preguntas para AppyGames
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes dar de alta preguntas para la AppyGames." aria-describedby="popover955887"></i> 
                    </small></h1>
                     <h3 class="font-bold c">En esta sección se puede agregar preguntas a los juegos</h3>
            </center>
        </form>
    </div>

    <div class="row">
        {{Form::Open(array('action'=>array('preguntasController@RegistraPregunta'),'method'=>'GET'))}} 
    <div class="col-lg-12">
        <div class="ibox-title">
            <div class="well well-sm text-center d">
                <h3 class="font-bold">Instrucciones</h3>
                <p>Seleccione el nivel y grupo. Para agregar más preguntas de clic en 'Agregar preguntas'</p>
            </div>
           
    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label">Nivel:</label>
                <select class="form-control" required name="idSubGrupo" id="selectTipo">
                   <option value="">Selecciona un nivel</option>
                       @foreach ($niv as $niv)
                    <option value="{{$niv->idNivel}}">{{$niv->nombreNivel}}</option>
                       @endforeach
                </select>
            </div>
            <div class="col-sm-3">
                <label class="control-label">Grado:</label>
                <br>
                <select class="form-control" class="form-control" style="width:150px" name="selectGrupo" id="selectGrupo"  required="required" >
                    <option disabled selected value=""></option>
                    <option value=""></option>
                </select>
            </div>    
                <div class="col-sm-4">
                        <label class="col-sm-3 control-label">Categoria:</label>
                        <select name="materia" class="form-control" style="width: 300px;" required>
                            <option value="">Seleccione</option>
                            <option value=1>Español</option>
                            <option value=2>Matemáticas</option>
                            <option value=3>Historia</option>
                            <option value=4>Arte</option>
                            <option value=5>Música</option>
                            <option value=6>Ciencias Naturales</option>
                            <option value=7>Geografía</option>
                            <option value=8>Tecnología</option>
                            <option value=9>Cívica y Ética</option>
                            <option value=10>Deportes</option>
                        </select>
                </div>
        </div>
        </div> 
        </div>                          
    </div>

<div class="col-lg-12" style="float: right;">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox-content" id="trash">
            <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Pregunta:</label>
                            <input type="text" name="pregunta[]" class="form-control" required="required" style="width: 300px;" maxlength="88">
                        </div>
                    </div>

                    <div class="col-sm-8">
                       <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 1:</label>
                        <input type="text" name="op1[]" class="form-control" required="required" style="width: 300px;" maxlength="20">
                      </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 2:</label>
                        <input type="text" name="op2[]" class="form-control" required="required" style="width: 300px;" maxlength="20">
                    </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 3:</label>
                        <input type="text" name="op3[]" class="form-control" required="required" style="width: 300px;" maxlength="20">
                    </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Opción 4:</label>
                        <input type="text" name="op4[]" class="form-control" required="required" style="width: 300px;" maxlength="20">
                    </div>
                    </div>
                    <div class="col-sm-8">
                        <label class="col-sm-3 control-label">Respuesta:</label>
                         <div class="col-sm-4" >
                          <div class="btn-group" data-toggle="buttons" style="width: 200px;">
                            <label class="btn btn-info perso">
                                <input type="radio" required="required" value="1" name="respuesta0" id="idTipoInsignia" required="required">
                                <H1>1</H1>
                            </label>
                            <label class="btn btn-info perso">
                                <input type="radio" required="required" value="2" name="respuesta0" id="idTipoInsignia" required="required" >
                                <H1>2</H1>
                            </label>
                            <label class="btn btn-info perso">
                                <input type="radio" required="required" value="3" name="respuesta0" id="idTipoInsignia" required="required" >
                               <H1>3</H1>
                            </label>
                            <label class="btn btn-info perso">
                                <input type="radio" required="required" value="4" name="respuesta0" id="idTipoInsignia" required="required" >
                               <H1>4</H1>
                            </label>
                           </div>
                       </div>
                   </div>

                   <div id="pregunta">  
                   </div>

            </div>
            
        </div>
        </div>
                <div class="ibox-content">
                    <button type="button" name="nuevo" id="nuevo" class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Agregar pregunta</button>
                    <button type="button" name="quitar" id="quitar" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Quitar pregunta</button>
                    <button  type="submit" name="enviar" id="enviar" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" >Enviar</button>    
                </div>

{{Form::Close()}}
</div>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
<!-- Tags Input -->
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<!--swA-->
<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js')}}"></script> 
<script type="text/javascript">
    $(document).ready(function()
    {
    $('[data-toggle="tooltip"]').tooltip();   
    });
</script>   


<script>
    var pregunta=0;

        $("#selectTipo").change(function(){
                    var idNivel = this.value;

                    $('#selectGrupo').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getGruposPreguntas')}}",
                        data: {id:idNivel},
                        dataType: 'json',
                        success: function (data) {

                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                
                                var op =$('<option>',{
                                    value: item.idgrupo,
                                    text: item.grado 
                                });
                                $('#selectGrupo').append(op);
                                
                           
                                });
                            }
                        });
       });
        $('#nuevo').click(function()
        { 
        pregunta=pregunta+1;
        console.log(pregunta);
        if(pregunta<20)
        {
         $("#pregunta").val(pregunta);
        var salto=$('<br>');
        var div6=$('<div>', {class:'ibox float-e-margins',id:'opciones'+pregunta});
        var lCat=$('<label>', {class:'col-sm-3 control-label',text:'Categoria:'});
        var lPre=$('<label>', {class:'col-sm-3 control-label',text:'Pregunta:'});
        var vPre=$('<input>', {name: 'pregunta[]',class:'form-control',required:'required',});
        var lOp1=$('<label>', {class:'col-sm-3 control-label',text:'Opción 1:'});
        var vOp1=$('<input>', {name: 'op1[]',class:'form-control',required:'required',id:'idTipoInsignia',});
        var lOp2=$('<label>', {class:'col-sm-3 control-label',text:'Opción 2:'});
        var vOp2=$('<input>', {type: 'text',name: 'op2[]',class:'form-control',required:'required',id:'idTipoInsignia',});
        var lOp3=$('<label>', {class:'col-sm-3 control-label',text:'Opción 3:'});
        var vOp3=$('<input>', {name: 'op3[]',class:'form-control',required:'required',id:'idTipoInsignia',});
        var lOp4=$('<label>', {class:'col-sm-3 control-label',text:'Opción 4:'});
        var vOp4=$('<input>', {name: 'op4[]',class:'form-control',required:'required',id:'idTipoInsignia',});
        var lRes=$('<label>', {class:'col-sm-3 control-label',text:'Respuesta:'});
        var div7=$('<div class="btn-group" data-toggle="buttons">', {class: 'btn-group'});
        var div8=$('<label>', {class:'btn btn-info perso',});
        var v1=$('<input>', { name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '1',});
            var h11=$('<h1>', {text:'1' });
        var div9=$('<div>', {class:'btn btn-info perso',});
        var v2=$('<input>', { name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '2',});
        var h12=$('<h1>', { text:'2',});
        var div10=$('<div>', {class:'btn btn-info perso',});
        var v3=$('<input>', {name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '3',});
        var h13=$('<h1>', { text:'3',});
        var div11=$('<div>', {class:'btn btn-info perso',});
        var v4=$('<input>', { name: 'respuesta'+pregunta,type: 'radio',class:'form-control',required:'required',value: '4',});
        var h14=$('<h1>', {text:'4',});



        div6.append(salto);
        div6.append(salto);
        div6.append(lPre);
        div6.append(vPre);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp1);
        div6.append(vOp1);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp2);
        div6.append(vOp2);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp3);
        div6.append(vOp3);
        div6.append(salto);
        div6.append(salto);
        div6.append(lOp4);
        div6.append(vOp4);
        div6.append(salto);
        div6.append(salto);
        div6.append(lRes);

        div8.append(h11);
        div8.append(v1);
        div7.append(div8);
        div6.append(div7);

        div9.append(h12);
        div9.append(v2);
        div7.append(div9);
        div6.append(div7);

        div10.append(h13);
        div10.append(v3);
        div7.append(div10);
        div6.append(div7);

        div11.append(h14);
        div11.append(v4);
        div7.append(div11);
        div6.append(div7);


        $('#pregunta').append(div6);
        }
        else
        {
            pregunta=6;
        }
        });

        $('#quitar').click(function()
        {
            
                $('#'+'opciones' + pregunta).remove();
                pregunta=pregunta-1;
                $("#pregunta").val(pregunta);
                if(pregunta<=0)
                {
                    pregunta=0;
                    $("#pregunta").val(pregunta);
                }

        });
</script>
<script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>        
<script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
<!-- jQuery UI -->
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<!-- Full Calendar -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>    
<script>
    $(document).ready(function() 
        {            
            $('.chosen-select').chosen();   
        });     
        function Desactiva()
        {
          $grupo
          if($('#selectGrupo').is(':checked')==true)
          {
              document.getElementById("selectAlumno").disabled=true; 
          }
          else
          {   
              document.getElementById("selectAlumno").disabled=false;
          }
          $('#selectAlumno').trigger("chosen:updated");
          $('#selectAlumno').trigger("liszt:updated");
          console.log($('#selectAlumno').is(':disabled'));
        }
</script>
</body>
@endsection
