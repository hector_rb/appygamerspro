 <style type="text/css">
      .form-group
      {
      padding-right: 15%;
      }
</style>
            <div class="panel panel-default">
                <div class="panel-heading">Sube un archivo de Excel</div>
 
                @if (Session::has('success-message'))
                    <div class="alert alert-success">{{ Session::get('success-message') }}</div>
                @endif
 
                @if (Session::has('error-message'))
                    <div class="alert alert-danger">{{ Session::get('error-message') }}</div>
                @endif
 
                <div class="panel-body">
                    {!! Form::open(array('url' => 'uploads/save', 'method' => 'post', 'files' => true)) !!}
                        <div class="form-group">
                            <div class="btn" style="padding-left: 8%">
                                <h3>Descargar plantilla</h3>
                                 <a href="{{asset('archivos-excel/PLANTILLA.xlsx')}}" class="btn btn-block btn-info compose-mail">Descargar</a>
                            </div>
                            
                            <span class="btn btn-default btn-file">
                               Selecciona un archivo {!! Form::file('file') !!}
                            </span>
                            <div class="btn">
                            {!! Form::submit('Enviar', ["class" => "btn btn-block btn-info compose-mail"]) !!}
                            </div>
                            
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
 