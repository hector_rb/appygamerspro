@extends('Admin.menuAdmin')

@section('content')
<head>
	<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
</head>
<body>
	 <div class="wrapper wrapper-content animated fadeInRight">
	 	
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="ibox float-e-margins">
	                    <div class="ibox-title">
	                        <h2>Administracion de Horarios <small>Se muestran los horarios actuales</small></h2>
	                        
	                    </div>
	                    <div class="ibox-content">

	                        <div class="table-responsive">
			                    <table class="table table-striped table-bordered table-hover dataTables-example" >
				                    <thead>
					                    <tr>
					                        <th>Nivel</th>
					                        <th>Grupo</th>
					                        <th>Codigo</th>
					                    </tr>
				                    </thead>
				                    <tbody>
					                    
					                    
					                    @foreach($grupos as $h)
					                    	<tr class="gradeX">
						                    	<td>{{$h->nombreNivel}}</td>
						                    	<td>{{$h->grado.' '.$h->grupo}}</td>
						                    	<td>{{$h->contrasena}}</td>
						                    	
						                    </tr>
					                    @endforeach
				                    </tbody>
				                    <tfoot>
					                    <tr>
					                        <th>Nivel</th>
					                        <th>Grupo</th>
					                        <th>Ciclo</th>
					                    </tr>
				                    </tfoot>
			                    </table>
	                        </div>

	                    </div>
	                </div>
	            </div>
            </div>
	        </div>
	        <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
		    <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

		    <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>

	        <script >
	        $(document).ready(function(){
	            $('.dataTables-example').DataTable({
	                pageLength: 25,
	                responsive: true,
	                dom: '<"html5buttons"B>lTfgitp',
	                language: {
						            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						        },
	                buttons: [

	                    {
	                     customize: function (win){
	                            $(win.document.body).addClass('white-bg');
	                            $(win.document.body).css('font-size', '10px');

	                            $(win.document.body).find('table')
	                                    .addClass('compact')
	                                    .css('font-size', 'inherit');
	                    }
	                    }
	                ]

	            });

	        });
	    </script>
	    </body>
@endsection