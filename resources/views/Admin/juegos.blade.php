@extends('Admin.menuAdmin')

@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
     <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Catálogo de juegos 
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes consultar los juegos disponibles" aria-describedby="popover955887"></i> 
                    </small></h1>
            </center>
        </form>
    </div>
    <div class="row">
        <table class="table table-hover table-mail">
                    
                        <thead>
                            <th>N°</th>
                            <th>Pregunta</th>
                            <th>Opción 1</th>
                            <th>Opción 2</th>
                            <th>Opción 3</th>
                            <th>Opción 4</th>
                            <th>Respuesta</th>
                        </thead>
                        <tbody>
                            @foreach($juegos as $jue)
                            <tr>
                                <td>{{$jue->nombre}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
    </div>
</body>
@endsection