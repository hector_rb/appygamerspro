-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-10-2017 a las 20:09:56
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `myappcollegeauth`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadmin` int(11) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `id_institucion` int(11) DEFAULT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `teloficina` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `idAlumno` int(11) NOT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `idgrado` int(11) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `idgrupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`idAlumno`, `idinstitucion`, `nombre`, `apepat`, `apemat`, `idgrado`, `genero`, `fechanac`, `telefono`, `correo`, `f_alta`, `f_baja`, `f_mod`, `activo`, `iduser`, `idgrupo`) VALUES
(0, 1, 'pepe', 'diaz', 'martinez', 1, 'M', '2000-09-04', '4443333343', 'pepe@gmail.com', '2017-09-06', '2017-09-14', '2017-09-16', 'A', 5, 1),
(1, 1, 'Rosa', 'Niño', 'Castillo', 1, 'F', '1990-04-08', '44425353256', 'rosa@gmail.com', '2006-09-14', '2012-09-06', '2017-09-20', 'A', 4, 2),
(2, 1, 'Diana Yadira', 'Reyes', 'Castillo', 2, 'F', '1993-09-04', '234567654321', 'diana@gmail.com', '2012-09-12', '2012-09-27', '2013-09-12', 'A', 4, 1),
(3, 1, 'Cesar Enrique', 'Niño', 'Castillo', 3, 'M', '1982-04-05', '2454325', 'cesar@gmail.com', '2017-09-11', '2020-09-20', '2017-09-18', 'A', 4, 1),
(4, 1, 'Felipe', 'Castillo', 'Niño', 4, 'M', '1993-05-26', '65435678', 'felipe@gmail.com', '2017-09-20', '2029-09-21', '2017-09-12', 'A', 4, 2),
(5, 1, 'Cesar Enrique', 'Niño', 'Castillo', 5, 'M', '1982-04-05', '2454325', 'cesar@gmail.com', '2017-09-11', '2020-09-20', '2017-09-18', 'A', 4, 1),
(6, 1, 'Felipe', 'Castillo', 'Niño', 4, 'M', '1993-05-26', '65435678', 'felipe@gmail.com', '2017-09-20', '2029-09-21', '2017-09-12', 'A', 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `idAlumno` int(11) NOT NULL,
  `idgrupo` varchar(40) NOT NULL,
  `fecha` date NOT NULL,
  `asistencia` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asistencia`
--

INSERT INTO `asistencia` (`idAlumno`, `idgrupo`, `fecha`, `asistencia`) VALUES
(0, '1', '2017-09-29', 'A'),
(0, '2', '2017-09-29', 'A'),
(1, '1', '2017-09-29', 'A'),
(1, '2', '2017-09-29', 'A'),
(2, '1', '2017-09-29', 'A'),
(2, '2', '2017-09-29', 'A'),
(3, '1', '2017-09-29', 'A'),
(3, '2', '2017-09-29', 'A'),
(4, '1', '2017-09-29', 'A'),
(4, '2', '2017-09-29', 'A'),
(5, '1', '2017-09-29', 'A'),
(5, '2', '2017-09-29', 'A'),
(6, '1', '2017-09-29', 'A'),
(6, '2', '2017-09-29', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario`
--

CREATE TABLE `calendario` (
  `idCalendario` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `desc` varchar(250) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `iduser` int(11) NOT NULL,
  `clase` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `calendario`
--

INSERT INTO `calendario` (`idCalendario`, `titulo`, `desc`, `inicio`, `fin`, `idgrupo`, `iduser`, `clase`, `status`) VALUES
(1, 'Evento especial', 'prueba edicion!!!!!!!!', '2017-09-26 00:00:00', '2017-10-01 00:00:00', NULL, 1, 'azul2', 1),
(2, 'Prueba 2!!!!!!', 'evento de prueba!!', '2017-09-23 02:00:00', '2017-09-23 02:00:00', NULL, 1, 'naranja', 1),
(3, 'Preba urgente', 'Emergencia', '2017-09-30 03:00:00', '2017-09-30 10:00:00', NULL, 1, 'rojo', 0),
(4, 'Junta Padres', 'Junta para padres del 3a 12:10', '2017-10-01 09:00:00', '2017-10-01 12:00:00', NULL, 1, 'rojo', 1),
(5, 'Nuevo evento', 'primer evento', '2017-09-21 09:00:00', '2017-09-21 15:00:00', NULL, 1, 'verde', 1),
(6, 'fgd', 'fddf', '2017-09-26 08:00:00', '2017-09-26 08:00:00', NULL, 1, 'naranja', 0),
(7, 'scd', 'scsc', '2017-09-25 08:00:00', '2017-09-25 08:00:00', NULL, 1, 'naranja', 0),
(8, 'Esto es', 'un horario', '2017-09-25 07:00:00', '2017-09-25 08:00:00', NULL, 1, 'verde', 1),
(9, 'esto es', 'un horario', '2017-09-25 07:00:00', '2017-09-25 08:00:00', NULL, 1, 'verde', 1),
(10, 'esto es', 'un horario', '2017-09-28 07:10:00', '2017-09-28 09:10:00', NULL, 1, 'naranja', 1),
(11, 'esto es', 'un horario', '2017-09-26 09:40:00', '2017-09-26 09:40:00', NULL, 1, 'naranja', 1),
(12, 'esto es', 'un horario', '2017-09-28 10:30:00', '2017-09-28 10:30:00', NULL, 1, 'verde', 1),
(13, 'iuyt', 'yukl', '2017-09-25 08:20:00', '2017-09-25 08:20:00', NULL, 1, 'naranja', 0),
(14, 'iuyt', 'yukl', '2017-09-28 08:40:00', '2017-09-28 08:40:00', NULL, 1, 'naranja', 0),
(15, 'wdsf', 'dacs', '2017-09-25 11:00:00', '2017-09-25 11:00:00', NULL, 1, 'naranja', 0),
(16, 'dqw', 'qwde', '2017-09-25 07:00:00', '2017-09-25 07:00:00', NULL, 1, 'verde', 0),
(17, 'asdsf', 'svc', '2017-09-25 07:10:00', '2017-09-25 08:00:00', NULL, 1, 'verde', 0),
(18, 'Prueba', 'Pa\' Juan Martin', '2017-10-02 00:00:00', '2017-10-31 00:00:00', NULL, 1, 'azul1', 1),
(19, 'Prueba', 'Pa\' Juan Martin', '2017-10-02 07:00:00', '2017-10-02 09:00:00', NULL, 1, 'azul1', 1),
(20, 'Prueba', 'Pa\' Juan Martin', '2017-10-03 07:00:00', '2017-10-03 07:00:00', NULL, 1, 'azul1', 1),
(21, 'Prueba', 'Pa\' Juan Martin', '2017-10-04 07:00:00', '2017-10-04 07:00:00', NULL, 1, 'azul1', 1),
(22, 'Prueba', 'Pa\' Juan Martin', '2017-10-05 07:00:00', '2017-10-05 07:00:00', NULL, 1, 'azul1', 1),
(23, 'Prueba', 'Pa\' Juan Martin', '2017-10-06 07:00:00', '2017-10-06 07:00:00', NULL, 1, 'azul1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calif`
--

CREATE TABLE `calif` (
  `idcalif` int(11) NOT NULL,
  `idmateria` int(11) DEFAULT NULL,
  `idalumno` int(11) DEFAULT NULL,
  `calif` int(11) DEFAULT NULL,
  `desc` varchar(300) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclo`
--

CREATE TABLE `ciclo` (
  `IdCiclo` int(11) NOT NULL,
  `nombreCiclo` varchar(70) NOT NULL,
  `inicioCiclo` date NOT NULL,
  `finCiclo` date NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinatarios`
--

CREATE TABLE `destinatarios` (
  `id` int(11) NOT NULL,
  `TipoDestinatario` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `destinatarios`
--

INSERT INTO `destinatarios` (`id`, `TipoDestinatario`) VALUES
(1, 'Alumno'),
(2, 'Profesor'),
(3, 'Grado'),
(4, 'Grupo'),
(5, 'Escuela'),
(6, 'Profesores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `idpais` int(11) DEFAULT NULL,
  `nombreEstado` varchar(100) DEFAULT NULL,
  `lada` varchar(10) DEFAULT NULL,
  `abreviatura` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idEstado`, `idpais`, `nombreEstado`, `lada`, `abreviatura`) VALUES
(1, 1, 'Aguascalientes', NULL, 'Ags.'),
(2, 1, 'Baja California', NULL, 'BC'),
(3, 1, 'Baja California Sur', NULL, 'BCS'),
(4, 1, 'Campeche', NULL, 'Camp.'),
(5, 1, 'Coahuila de Zaragoza', NULL, 'Coah.'),
(6, 1, 'Colima', NULL, 'Col.'),
(7, 1, 'Chiapas', NULL, 'Chis.'),
(8, 1, 'Chihuahua', NULL, 'Chih.'),
(9, 1, 'Distrito Federal', NULL, 'DF'),
(10, 1, 'Durango', NULL, 'Dgo.'),
(11, 1, 'Guanajuato', NULL, 'Gto.'),
(12, 1, 'Guerrero', NULL, 'Gro.'),
(13, 1, 'Hidalgo', NULL, 'Hgo.'),
(14, 1, 'Jalisco', NULL, 'Jal.'),
(15, 1, 'México', NULL, 'Mex.'),
(16, 1, 'Michoacán de Ocampo', NULL, 'Mich.'),
(17, 1, 'Morelos', NULL, 'Mor.'),
(18, 1, 'Nayarit', NULL, 'Nay.'),
(19, 1, 'Nuevo León', NULL, 'NL'),
(20, 1, 'Oaxaca', NULL, 'Oax.'),
(21, 1, 'Puebla', NULL, 'Pue.'),
(22, 1, 'Querétaro', NULL, 'Qro.'),
(23, 1, 'Quintana Roo', NULL, 'Q. Roo'),
(24, 1, 'San Luis Potosí', NULL, 'SLP'),
(25, 1, 'Sinaloa', NULL, 'Sin.'),
(26, 1, 'Sonora', NULL, 'Son.'),
(27, 1, 'Tabasco', NULL, 'Tab.'),
(28, 1, 'Tamaulipas', NULL, 'Tamps.'),
(29, 1, 'Tlaxcala', NULL, 'Tlax.'),
(30, 1, 'Veracruz de Ignacio de la Llave', NULL, 'Ver.'),
(31, 1, 'Yucatán', NULL, 'Yuc.'),
(32, 1, 'Zacatecas', NULL, 'Zac.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado`
--

CREATE TABLE `grado` (
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL,
  `grado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grado`
--

INSERT INTO `grado` (`idInstitucion`, `idNivel`, `grado`) VALUES
(1, 2, 1),
(1, 2, 2),
(1, 2, 3),
(1, 2, 4),
(1, 2, 5),
(1, 2, 6),
(1, 3, 1),
(1, 3, 2),
(1, 3, 3),
(1, 4, 1),
(1, 4, 2),
(1, 4, 3),
(1, 4, 4),
(1, 4, 5),
(1, 4, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `idgrupo` int(40) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL,
  `grado` int(11) NOT NULL,
  `grupo` varchar(2) NOT NULL,
  `contrasena` varchar(45) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `materianombre` varchar(50) NOT NULL,
  `idmateria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`idgrupo`, `idInstitucion`, `idNivel`, `grado`, `grupo`, `contrasena`, `status`, `materianombre`, `idmateria`) VALUES
(1, 1, 2, 1, 'A', 'ACCXTUF4', 1, 'español', 1),
(2, 1, 2, 1, 'B', '8M4TSIDM', 1, 'matematica', 2),
(3, 1, 2, 1, 'C', '8VNLQIXE', 1, 'inglés', 3),
(4, 1, 2, 2, 'A', '0TO1CMV6', 1, '0', 0),
(5, 1, 2, 2, 'B', 'D8YWDJWR', 1, '0', 0),
(6, 1, 2, 2, 'C', '1V7VL2TU', 1, '0', 0),
(7, 1, 2, 3, 'A', 'QG7RQXC7', 1, '0', 0),
(8, 1, 2, 3, 'B', '9ROAJU8L', 1, '0', 0),
(9, 1, 2, 3, 'C', 'BJF8SM8R', 1, '0', 0),
(10, 1, 2, 4, 'A', 'PI5B8Z4G', 1, '0', 0),
(11, 1, 2, 4, 'B', '4FMALOLE', 1, '0', 0),
(12, 1, 2, 4, 'C', 'D2KWNGGE', 1, '0', 0),
(13, 1, 2, 5, 'A', '56RX43G5', 1, '0', 0),
(14, 1, 2, 5, 'B', '8IY4RAAF', 1, '0', 0),
(15, 1, 2, 5, 'C', 'ESW6CTRZ', 1, '0', 0),
(16, 1, 2, 6, 'A', 'CRBWMGLK', 1, '0', 0),
(17, 1, 2, 6, 'B', 'U8Z1AXJW', 1, '0', 0),
(18, 1, 2, 6, 'C', 'OVNA6YCS', 1, '0', 0),
(19, 1, 3, 1, 'A', 'JI2GZCSX', 1, '0', 0),
(20, 1, 3, 1, 'B', '7UYAGTK3', 1, '0', 0),
(21, 1, 3, 1, 'C', 'FM3HD4H1', 1, '0', 0),
(22, 1, 3, 2, 'A', 'YM8UG09I', 1, '0', 0),
(23, 1, 3, 2, 'B', 'JL34H8O9', 1, '0', 0),
(24, 1, 3, 2, 'C', 'XAG06KF0', 1, '0', 0),
(25, 1, 3, 3, 'A', 'VE2BAPBK', 1, '0', 0),
(26, 1, 3, 3, 'B', '4ROSLF2M', 1, '0', 0),
(27, 1, 3, 3, 'C', 'F205UIGY', 1, '0', 0),
(28, 1, 4, 1, 'A', 'J3U5RO6B', 1, '0', 0),
(29, 1, 4, 1, 'B', '7KXJ4N1F', 1, '0', 0),
(30, 1, 4, 1, 'C', '69Z3N8R3', 1, '0', 0),
(31, 1, 4, 2, 'A', '7XSJ3530', 1, '0', 0),
(32, 1, 4, 2, 'B', '1A3N85SZ', 1, '0', 0),
(33, 1, 4, 2, 'C', 'E3N4SZMP', 1, '0', 0),
(34, 1, 4, 3, 'A', '356SJEBY', 1, '0', 0),
(35, 1, 4, 3, 'B', 'U5GB76MH', 1, '0', 0),
(36, 1, 4, 3, 'C', 'EU8IK0A0', 1, '0', 0),
(37, 1, 4, 4, 'A', 'H093DP3C', 1, '0', 0),
(38, 1, 4, 4, 'B', 'WE9UBYQ6', 1, '0', 0),
(39, 1, 4, 4, 'C', 'CQ0H7K0A', 1, '0', 0),
(40, 1, 4, 5, 'A', 'MS8RS9NF', 1, '0', 0),
(41, 1, 4, 5, 'B', 'GAQFSSFC', 1, '0', 0),
(42, 1, 4, 5, 'C', 'K079ZW1S', 1, '0', 0),
(43, 1, 4, 6, 'A', 'QPFZ3CAH', 1, '0', 0),
(44, 1, 4, 6, 'B', 'Q34DVVW8', 1, '0', 0),
(45, 1, 4, 6, 'C', '9Y2538MQ', 1, '0', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `idHorario` int(11) NOT NULL,
  `idGrupo` int(11) NOT NULL,
  `idSubgrupo` int(11) NOT NULL,
  `idCiclo` int(11) NOT NULL,
  `inicio` datetime NOT NULL,
  `fin` datetime NOT NULL,
  `dia` varchar(15) NOT NULL,
  `horaIni` varchar(20) NOT NULL,
  `horaFin` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion`
--

CREATE TABLE `institucion` (
  `idInstitucion` int(11) NOT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `contacto` varchar(100) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  `ciudad` varchar(80) DEFAULT NULL,
  `idestado` int(11) DEFAULT NULL,
  `idpais` int(11) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `cp` varchar(8) DEFAULT NULL,
  `nivelescolar` varchar(45) DEFAULT NULL,
  `siglas` varchar(10) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `desc` varchar(1200) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `lng` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `codregistro` varchar(45) DEFAULT NULL,
  `mision` varchar(1000) DEFAULT NULL,
  `vision` varchar(1000) DEFAULT NULL,
  `valores` varchar(1000) DEFAULT NULL,
  `reglamento` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `institucion`
--

INSERT INTO `institucion` (`idInstitucion`, `clave`, `nombre`, `contacto`, `direccion`, `id_usuario`, `fecha_alta`, `fecha_baja`, `activo`, `ciudad`, `idestado`, `idpais`, `telefono`, `cp`, `nivelescolar`, `siglas`, `correo`, `desc`, `lat`, `lng`, `logo`, `codregistro`, `mision`, `vision`, `valores`, `reglamento`) VALUES
(1, '09.028.502/0001', 'Colegio Hispano Inglés', 'Juan Perez', 'Benigno Arriaga 805, De Tequisquiapan, 78233 San Luis, S.L.P.', 1, '2017-09-14', NULL, '1', 'San Luis Potosí', 24, 1, '01 444 813 8205', '78233', 'Maternal - Preparatoria', 'CHI', 'admin@hispano.com', 'El Instituto Hispano Inglés es un colegio católico fundado por la Congregación de las Hermanas de la Caridad del Verbo Encarnado en 1937 en la Ciudad de San Luis Potosí, S.L.P.', '22.1475339', '-100.987012', NULL, 'tUfivTU7g8', 'La Universidad Politécnica de San Luis Potosí, está comprometida con la excelencia en la formación integral y humana, y con el aprendizaje, el desarrollo y la aplicación del nuevo conocimiento.\r\nLa Universidad Politécnica une a estudiantes, docentes y personal administrativo para crear una comunidad de la más alta calidad académica basada en un modelo educativo abierto y flexible, y comprometida con el progreso social y económico del estado y del país.', '\"Aspiramos a ser una Universidad consolidada en su tarea de formar profesionales competentes, que se distinguen por su aportación al desarrollo de México y de San Luis Potosí y cuentan con reconocimiento en sus campos de actividad profesional y en su vida comunitaria.\r\n\r\nLa Universidad Politécnica de San Luis Potosí representa un espacio de desarrollo intelectual que atrae el mejor talento, tanto de estudiantes como profesores, que se sienten orgullosos de pertenecer a esta comunidad académica.\"', 'Integridad\r\nResponsabilidad\r\nTrascendencia\r\nConstancia\r\nResponsabilidad Social\r\nRespeto\r\nAfecto\r\nServicio\r\nLibertad\r\nJusticia\r\nEsfuerzo\r\nSolidaridad\r\nTrabajo en Equipo\r\nCompromiso\r\nMejora Continua\r\nCongruencia', '------------------------------------'),
(3, 'NMAKLN1789SEPSLP', 'Marista', 'Pedro Perez', 'Benigno Arriaga 608, Col. Moderna C.P. 78270 San Luis Potosí, S.L.P.', 7, '2017-09-26', NULL, '1', 'SLP', 24, 1, '8135465', NULL, NULL, NULL, 'adm', 'El Instituto Potosino Marista, es dirigido por los Hermanos Maristas, Desde 1935 ofrece a la sociedad potosina la opción de una formación integral, de acuerdo a la concepción cristiana de la persona, de la vida y del mundo.', '22.1496857', '-100.98800849999998', NULL, 'OIHO4IBI', NULL, NULL, NULL, NULL),
(4, NULL, 'Colegio Hispano Mexicano', NULL, NULL, 9, '2017-09-26', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(9FUPCDN', NULL, NULL, NULL, NULL),
(5, 'IUHugIJ/78', '1 de Mayo', 'Luis Alberto Mendez', '---------', 10, '2017-09-26', NULL, '1', 'SLP', 24, 1, '863879', NULL, NULL, NULL, 'alberto@gmail.com', 'hbiohuihuih', '22.1497904', '-100.94722539999998', NULL, 'QSDT5HJT', 'kjgiuhk', 'bjhjk', 'nbjh', 'hvhjbkj'),
(6, NULL, 'Lala', NULL, NULL, 19, '2017-09-27', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'H3SMC4IU', NULL, NULL, NULL, NULL),
(7, NULL, 'lalo', NULL, NULL, 20, '2017-09-27', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Q4102I68', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `idMateria` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `nombreMateria` varchar(40) NOT NULL,
  `creditos` int(11) DEFAULT NULL,
  `idNivel` int(11) NOT NULL,
  `grado` varchar(15) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`idMateria`, `idInstitucion`, `nombreMateria`, `creditos`, `idNivel`, `grado`, `activo`) VALUES
(0, 1, 'Inglés', NULL, 1, '1', 1),
(1, 1, 'Ciencias Sociales', NULL, 4, '1', 1),
(2, 1, 'Educación Física', NULL, 2, '1', 1),
(3, 1, 'Algebra', NULL, 4, '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia_profesor`
--

CREATE TABLE `materia_profesor` (
  `idMateria` int(11) NOT NULL,
  `idProfesor` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia_profesor`
--

INSERT INTO `materia_profesor` (`idMateria`, `idProfesor`, `idInstitucion`) VALUES
(1, 1, 1),
(1, 2, 1),
(2, 1, 1),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE `mensaje` (
  `idMensaje` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `desc` varchar(25) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `encargado` int(11) DEFAULT NULL,
  `destinatario` int(11) DEFAULT NULL,
  `tipo` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Asunto` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mensaje` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TipoDestinatario` bigint(20) NOT NULL,
  `IdDestinatario` bigint(20) NOT NULL,
  `IdEmisor` bigint(20) NOT NULL,
  `TipoEmisor` bigint(20) NOT NULL,
  `Visto` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `Asunto`, `Mensaje`, `TipoDestinatario`, `IdDestinatario`, `IdEmisor`, `TipoEmisor`, `Visto`, `created_at`, `updated_at`) VALUES
(35, 'Prueba de emisor', 'Estimado profesor esto es un mensaje de prueba', 2, 2, 1, 1, 1, '2017-09-29 22:39:43', '2017-09-29 22:39:43'),
(36, 'ijhuydsA', 'lñkjhfg', 1, 3, 1, 1, 1, '2017-09-29 23:03:32', '2017-09-29 23:03:32'),
(37, 'Prueba de emisor', 'Si este mensaje sale el where es correcto', 1, 1, 1, 1, 1, '2017-09-29 23:19:25', '2017-09-29 23:19:25'),
(38, 'Carta de pasantia', 'Te envio tu carta de pasantia.', 1, 1, 1, 1, 0, '2017-10-02 18:42:14', '2017-10-02 18:42:14'),
(39, 'mensaje de prueba', 'buenos dias esto es un mensaje de prueba', 2, 3, 1, 1, 1, '2017-10-02 18:55:39', '2017-10-02 18:55:39'),
(41, 'Esto es otro mensaje de prueba', 'Buenos dias esto es un mensaje de prueba', 1, 1, 3, 2, 1, '2017-10-02 19:21:14', '2017-10-02 19:21:14'),
(42, 'Hola', 'como estas', 2, 2, 1, 1, 0, '2017-10-02 21:08:23', '2017-10-02 21:08:23'),
(43, 'Mario', '-----------', 1, 2, 1, 1, 1, '2017-10-02 21:23:52', '2017-10-02 21:23:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveles`
--

CREATE TABLE `niveles` (
  `idNivel` int(11) NOT NULL,
  `nombreNivel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `niveles`
--

INSERT INTO `niveles` (`idNivel`, `nombreNivel`) VALUES
(1, 'Preescolar'),
(2, 'Primaria'),
(3, 'Secundaria'),
(4, 'Preparatoria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_inst`
--

CREATE TABLE `nivel_inst` (
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nivel_inst`
--

INSERT INTO `nivel_inst` (`idInstitucion`, `idNivel`) VALUES
(1, 2),
(1, 3),
(1, 4),
(7, 2),
(7, 3),
(7, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `idPais` int(11) NOT NULL,
  `nombrePais` varchar(45) DEFAULT NULL,
  `idioma` varchar(45) DEFAULT NULL,
  `lada` varchar(5) DEFAULT NULL,
  `abrev` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`idPais`, `nombrePais`, `idioma`, `lada`, `abrev`) VALUES
(1, 'México', 'Español', '+521', 'MEX');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idprofesor` int(11) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `id_institucion` int(11) DEFAULT NULL,
  `nombreprof` varchar(75) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `teloficina` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`idprofesor`, `correo`, `id_institucion`, `nombreprof`, `apepat`, `apemat`, `genero`, `fechanac`, `telefono`, `celular`, `teloficina`, `f_alta`, `f_baja`, `f_mod`, `activo`, `iduser`) VALUES
(1, 'miguel@gmail.com', 1, 'pepe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18),
(2, 'jorge@gmail.com', 1, 'Jorge Martinez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21),
(3, 'victor@gmail.com', 3, 'Victor Ruiz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subgrupo`
--

CREATE TABLE `subgrupo` (
  `idSubgrupo` int(11) NOT NULL,
  `idGrupo` int(11) NOT NULL,
  `idMateria` int(11) NOT NULL,
  `idProfesor` int(11) NOT NULL,
  `idCiclo` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `idTareas` int(11) NOT NULL,
  `idmateria` int(11) DEFAULT NULL,
  `idgrupo` varchar(45) DEFAULT NULL,
  `fechaentrega` date DEFAULT NULL,
  `titulo` varchar(60) DEFAULT NULL,
  `descripcion` varchar(600) DEFAULT NULL,
  `anexo` varchar(45) DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`idTareas`, `idmateria`, `idgrupo`, `fechaentrega`, `titulo`, `descripcion`, `anexo`, `condicion`) VALUES
(1, 1, '1', '2017-09-30', 'Tarea 1', 'Hagan la tarea', NULL, 1),
(2, 1, '2', '2017-09-29', 'ohj', 'hjvkljñk', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_users`
--

CREATE TABLE `tipo_users` (
  `idtipo_users` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE `tutor` (
  `idtutor` int(11) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `id_institucion` int(11) DEFAULT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `teloficina` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutoralumno`
--

CREATE TABLE `tutoralumno` (
  `idtutor` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `generacion` varchar(42) DEFAULT NULL,
  `nombrealumno` varchar(75) DEFAULT NULL,
  `gradoalumno` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor_institucion`
--

CREATE TABLE `tutor_institucion` (
  `idTutor` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_user` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `tipo_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mario Eduardo', 'mario@gmail.com', '$2y$10$SC00a5X6FclMW/JuFNA67enQm3mXIU1B2aWtEzuD.TBj/G98KQ4T.', 1, '1corDaECzPUkIQ4cigWgxFWc7ly1ZwzXtG6mg0lCQ566aiu9JAeqcc4QLkoP', '2017-09-06 23:26:14', '2017-09-06 23:26:14'),
(2, 'Juan', 'juan@gmail.com', '$2y$10$DHoWzV/Fe2IT62iIGHgg7O3TS3Eqb6.SK0CpEmLsGFK8TQ3t/gysS', 5, 'uj5b0PCwLszmdM68ixsHqIi8n08s2L4oDaHvmpNEcRLSzNCnIfMz8IwPKbcc', '2017-09-07 02:42:42', '2017-09-07 02:42:42'),
(3, 'Luis', 'luis@gmail.com', '$2y$10$OEnsKoTPY6xwXlKK3UgDVePX01eiP.w8pWsHyX0D3gmmbEQ1YGCwK', 2, 'eHXKsZq5dt6SBHgGIL9FM0ahzdovgA1xaNujdZ4OdCUDrSFF2FwPPEYedooi', '2017-09-07 20:54:17', '2017-09-07 20:54:17'),
(4, 'Mayra', 'mayra@gmail.com', '$2y$10$uhIDvyDufb94AhYYXOmZ/e4GVWfC9re0vKdmnEENXa49/LzCuVQ4q', 3, 'U3jZpPkb35BELpZqbRg3Fiorjdy5fwU1VcN3w5XwlCC2zo9YH2SA8Xje0ykM', '2017-09-07 20:57:22', '2017-09-07 20:57:22'),
(5, 'Pepe', 'pepe@gmail.com', '$2y$10$wJ8b2482TKmGNb9s9fbqHujko/fkRgEKkX2K13IArkTDRX3j0zdGe', 4, 'IAwUr43kJ9P086Vr84TbVl3PSPtXOGZdGvHeOlx7wiJMueINYJXU926mCjnu', '2017-09-08 19:22:49', '2017-09-08 19:22:49'),
(7, 'Pedro Perez', 'pedroperez@gmail.com', '$2y$10$N/LslkFWotspc22etfxi3OwF6dwkvcFhXfsSCKvwdsLuKgAsyd3DW', 1, 'tnwjKTfQhdQ3T3eGc1iNNgVPKuYBdgNWUH2cVaHum2dEylskU4Y2HH4kcYnC', '2017-09-26 20:13:23', '2017-09-26 20:13:23'),
(9, 'Jose Jose', 'jose@gmail.com', '$2y$10$iTZir6IDmdczCVEoXc9tpeeLTRNvpyLvha1hBaOkYIvS/nY9Pe4ou', 1, 'Kn1WwPAEPsvRbFsWFuPqMV2lwYABZz0on78woDlyw4VL2RSV8aokTU19sRbu', '2017-09-26 20:42:35', '2017-09-26 20:42:35'),
(10, 'Luis Alberto Mendez', 'alberto@gmail.com', '$2y$10$hveQMlnjloAXYNY0Zy/46u5vFufFWzF/K/NZHmZkJj3F0nzFNItFC', 1, '4LmHHKwd9bDEG9bUHVFoq2TzKuoB9eWu3CJfFxp2wF9kxBKepJXp0WF6EX9z', '2017-09-26 21:12:44', '2017-09-26 21:12:44'),
(18, 'Miguel Flores', 'miguel@gmail.com', '$2y$10$rnsM1CYGo96K6fmFOSTvx.F3z5JXfjMs75Opt33aUvrqVMbg2X4Tu', 3, NULL, '2017-09-27 00:48:31', '2017-09-27 00:48:31'),
(19, 'lala', 'lala@gmail.com', '$2y$10$xzF5cAa2z0bUEVHVUMB3we/bCV3DEkkAQIaE/oQxEmVJ0K6glmNWy', 1, NULL, '2017-09-28 00:14:56', '2017-09-28 00:14:56'),
(20, 'lalo', 'lalo@gmail.com', '$2y$10$2LA0mSeDQsShpnCQYQ310eMfG44u77cSz2XeN5G8ciLDN1.aj/VN6', 1, NULL, '2017-09-28 00:16:37', '2017-09-28 00:16:37'),
(21, 'Jorge Martinez', 'jorge@gmail.com', '$2y$10$/XtIo0pwXPhf26phzIofteXm/hVF95iS0BchimWjNj7NzD.CaX76W', 3, '9kXCXM7qjsUuOWTgLyAwy62TsR1XjTEZNP3tIAzfOak0gtk5FDUTnxU5FE3R', '2017-09-28 22:55:30', '2017-09-28 22:55:30'),
(23, 'Victor Ruiz', 'victor@gmail.com', '$2y$10$V1thaPVUjH5r6C/fAm.dG.141VKNJ9MflRelq9Yi2cqUBeJ7Zzk8y', 3, 'tTVUbnmkm3aFClLOSb8H3opzx37PVjUDjO1kXNuANiezK0DnpEgXrR9RzLMI', '2017-09-28 22:56:54', '2017-09-28 22:56:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadmin`),
  ADD KEY `FKinst_idx` (`id_institucion`),
  ADD KEY `FKinuser_idx` (`iduser`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idAlumno`),
  ADD KEY `FKinst_idx` (`idinstitucion`),
  ADD KEY `FKiduser_idx` (`iduser`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`idAlumno`,`idgrupo`,`fecha`),
  ADD KEY `FKgrupo_idx` (`idgrupo`);

--
-- Indices de la tabla `calendario`
--
ALTER TABLE `calendario`
  ADD PRIMARY KEY (`idCalendario`),
  ADD KEY `FKgrupo_idx` (`idgrupo`);

--
-- Indices de la tabla `calif`
--
ALTER TABLE `calif`
  ADD PRIMARY KEY (`idcalif`),
  ADD KEY `FKmateria_idx` (`idmateria`),
  ADD KEY `fkalumno_idx` (`idalumno`);

--
-- Indices de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  ADD PRIMARY KEY (`IdCiclo`);

--
-- Indices de la tabla `destinatarios`
--
ALTER TABLE `destinatarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`),
  ADD KEY `FKpais|_idx` (`idpais`);

--
-- Indices de la tabla `grado`
--
ALTER TABLE `grado`
  ADD PRIMARY KEY (`idInstitucion`,`idNivel`,`grado`),
  ADD KEY `idInstitucion` (`idInstitucion`,`idNivel`,`grado`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`idgrupo`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`idHorario`);

--
-- Indices de la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD PRIMARY KEY (`idInstitucion`),
  ADD KEY `FKusuario` (`id_usuario`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`idMateria`);

--
-- Indices de la tabla `materia_profesor`
--
ALTER TABLE `materia_profesor`
  ADD PRIMARY KEY (`idMateria`,`idProfesor`,`idInstitucion`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`idMensaje`),
  ADD KEY `FKenc_idx` (`encargado`),
  ADD KEY `FKdest_idx` (`destinatario`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `niveles`
--
ALTER TABLE `niveles`
  ADD PRIMARY KEY (`idNivel`);

--
-- Indices de la tabla `nivel_inst`
--
ALTER TABLE `nivel_inst`
  ADD PRIMARY KEY (`idInstitucion`,`idNivel`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idprofesor`),
  ADD KEY `FKinst_idx` (`id_institucion`),
  ADD KEY `FKinuser_idx` (`iduser`);

--
-- Indices de la tabla `subgrupo`
--
ALTER TABLE `subgrupo`
  ADD PRIMARY KEY (`idSubgrupo`);

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`idTareas`),
  ADD KEY `FKmateria_idx` (`idmateria`),
  ADD KEY `FKgrupo_idx` (`idgrupo`);

--
-- Indices de la tabla `tipo_users`
--
ALTER TABLE `tipo_users`
  ADD PRIMARY KEY (`idtipo_users`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`idtutor`),
  ADD KEY `FKinst_idx` (`id_institucion`),
  ADD KEY `FKiduser_idx` (`iduser`);

--
-- Indices de la tabla `tutoralumno`
--
ALTER TABLE `tutoralumno`
  ADD PRIMARY KEY (`idtutor`,`idalumno`),
  ADD KEY `FKalumno_idx` (`idalumno`);

--
-- Indices de la tabla `tutor_institucion`
--
ALTER TABLE `tutor_institucion`
  ADD PRIMARY KEY (`idTutor`,`idInstitucion`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `calendario`
--
ALTER TABLE `calendario`
  MODIFY `idCalendario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  MODIFY `IdCiclo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `destinatarios`
--
ALTER TABLE `destinatarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `idgrupo` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `institucion`
--
ALTER TABLE `institucion`
  MODIFY `idInstitucion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `idMateria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `niveles`
--
ALTER TABLE `niveles`
  MODIFY `idNivel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idprofesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `subgrupo`
--
ALTER TABLE `subgrupo`
  MODIFY `idSubgrupo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `idTareas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
  MODIFY `idtutor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `FKinstAdmin` FOREIGN KEY (`id_institucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKinuserAdmon` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `FKiduser2` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKinst2` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD CONSTRAINT `FKalumno3` FOREIGN KEY (`idalumno`) REFERENCES `alumno` (`idAlumno`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `calif`
--
ALTER TABLE `calif`
  ADD CONSTRAINT `FKmateria2` FOREIGN KEY (`idmateria`) REFERENCES `materia` (`idMateria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkalumno2` FOREIGN KEY (`idalumno`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `FKpais` FOREIGN KEY (`idpais`) REFERENCES `pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD CONSTRAINT `FKusuario` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD CONSTRAINT `FKdest` FOREIGN KEY (`destinatario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKenc` FOREIGN KEY (`encargado`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `FKinst00` FOREIGN KEY (`id_institucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKinuser0` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD CONSTRAINT `FKmateria4` FOREIGN KEY (`idmateria`) REFERENCES `materia` (`idMateria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD CONSTRAINT `FKiduser` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKinst` FOREIGN KEY (`id_institucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tutoralumno`
--
ALTER TABLE `tutoralumno`
  ADD CONSTRAINT `FKalumno` FOREIGN KEY (`idalumno`) REFERENCES `alumno` (`idAlumno`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKtutor` FOREIGN KEY (`idtutor`) REFERENCES `tutor` (`idtutor`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
