@extends('Admin.menuAdmin')

@section('content')
<body>
	 <div class="wrapper wrapper-content animated fadeInRight">
	        <div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox float-e-margins">
	                        <div class="ibox-title">
	                            <h2>Seleccionar ciclo escolar actual</h2>
	                             
	                        </div>
	                        <div class="ibox-content">
	                            @if (count($errors)>0)
	                            <div class="alert alert-danger">
	                                <ul>
	                                @foreach ($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                                @endforeach
	                                </ul>
	                            </div>
	                            @endif
	                            {!!Form::open(['method'=>'POST','route'=>['ciclo.store'], 'class'=>'form-horizontal'])!!}
	                            

	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >NIVEL</label>

	                                    <div class="col-sm-8">
	                                    	<select class="form-control" name="nivel" required="required">
	                                    		<option></option>
	                                    		@foreach($niv as $n)
	                                            	<option value="{{$n->idNivel}}">{{$n->nombreNivel}}</option>
	                                            @endforeach
	                                    	</select>
	                                    </div>
	                                    
	                                </div>
	                                
	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >NOMBRE DEL CICLO ESCOLAR</label>

	                                    <div class="col-sm-8">
	                                    	<input type="text" name="ciclo" class="form-control">
	                                    </div>
	                                    
	                                </div>

	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >FECHA INICIO</label>

	                                    <div class="col-sm-8">
	                                    	<input type="date" name="inicio" class="form-control">
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                	<label class="col-sm-2 control-label" >FECHA FIN</label>

	                                    <div class="col-sm-8">
	                                    	<input type="date" name="fin" class="form-control">
	                                    </div>
	                                    
	                                </div>
	                                <div class="form-group">
	                                    <div class="col-sm-9"></div>
	                                    <div class="col-sm-3 control-label">
	                                    	<button type="submit" name="guardar" class="btn btn-success ">Crear</button>
	                                    </div>
                                </div>                        
                            </form>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </body>
@endsection