@extends('Tutor.menuTutor')
@section('content')

  <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
  <script src="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>

<div class="container">
  <h2>Insignias</h2>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Individuales</a></li>
    <li><a data-toggle="tab" href="#menu1">Grupales</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <!--//////////INDIVIDUALES/////////////////-->
      <Table>
      	<tr>
      		<?php $con=1; ?>
      	@foreach($insignias as $i)
					<th><image src="{{ asset('img/insignias/'.$i->rutaImagen)}}"  height="210" width="180" >
						<br>
						{{$i->nombreprof}} {{$i->apepat}} {{$i->apemat}}
						<br>
						{{$i->nombreMateria}}
						<br>
						{{$i->fecha}}
					</th>
				<?php
				if($con >= 4)
				{
					echo '</tr>';
					echo '<tr>';
					$con=1;	
				}
				else
				{
					$con++;
				}
				?>
		@endforeach
		</tr>
	  </Table>
    </div>
    <div id="menu1" class="tab-pane fade">
      <!--//////////////GRUPALES/////////////-->
    </div>
  </div>



@endsection

