@extends('Alumno.menuAlumno')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Asistencia</h2>
<h5>Visualización de las asistencias del alumno.</h5>
</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5>Administración de asistencias</h5>
<div class="ibox-tools">
</div>
</div>
<div class="ibox-content">
<div class="form-horizontal">
        <div class="form-group">
        	<label class="col-sm-2 control-label" >Selecciona un Periodo</label>

            <div class="col-sm-5">
            	<select class="form-control" name="nivel" id="selectPeriodos" required="required">
            		<option></option>
            		@foreach($periodo as $a)
                    	<option value="{{$a->idPeriodo}}">{{$a->nombrePeriodo}}</option>
                    @endforeach
            	</select>
            </div>

        </div>
    </div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Registros de asistencia, A = asistencia, F = falta, R = retardo </p>
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="table-responsive">
<table class="table table-striped table-bordered table-condensed table-hover">
<thead>

<th>Materia</th>
<th>Asistencias</th>
<th>Faltas</th>
<th>Retardos</th>
</thead>
<tbody id="rows">
</tbody>
</table>

</div>
</div>
{!!Form::close()!!}
</div>
</div>
                </div>
            </div>
            </div>
        </div>

<script >
	$("#selectPeriodos").change(function(){
		var idP = this.value;
        $('#rows').find('tr').remove();
        $.ajax({
            method: 'get',
            url: "{{('getAsistenciaAlumno')}}",
            data: { idP:idP},
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    //console.log(item.nombre);
                    var fila = $('<tr>');
                    var materia =$('<td>',{
                    	text: item.nombreMateria
                    });
                    var asis =$('<td>',{
                    	text: item.A
                    });
                    var falta =$('<td>',{
                    	text: item.F
                    });
                    var ret =$('<td>',{
                    	text: item.R
                    });	                        
                    
                    
                    fila.append(materia);
                    
                    fila.append(asis);
                    fila.append(falta);
                    fila.append(ret);
                    $('#rows').append(fila);
               
            		});
        		}
    		});
    });
</script>
@endsection