<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <!--cropper-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.min.css" />

</head>

<body>
    @section('menu')
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element" style="text-align: center;"> <span>
                            <img alt="image" class="img-circle" id="profileImage" style="width: 100px; height: 100px;" src="{{asset('img/profile_pics/'.Auth::user()->imagen)}}" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">Alumno <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{URL::to('alumno/perfilAlum')}}">Perfil</a></li>
                                <li class="divider"></li>
                                <!--<li><a href="login.html">Logout</a></li>-->
                            </ul>
                        </div>
                        <div class="logo-element">
                            MAC
                        </div>
                    </li>
                    <li>
                        <a href="{{URL::to('alumno')}} "><i class="fa fa-align-justify"></i> <span class="nav-label">Inicio</span></a>
                    </li>
                                      
                    <li>
                        <a href="{{URL::to('alumno/eventos')}}"><i class="fa fa-calendar"></i> <span class="nav-label">Eventos</span>  </a>
                    </li>
                    <li>
                        <a href="{{URL::to('alumno/mensajesR')}}"><i class="fa fa-envelope"></i> <span class="nav-label">Mensajes</span></a>
                    </li>     
                    <li>
                        <a href="{{URL::to('alumno/avisosAlum')}}"><i class="fa fa-location-arrow"></i> <span class="nav-label">Avisos</span></a>
                    </li>
                    <li>
                        <a href="{{URL::to('alumno/tareas')}}"><i class="fa fa-edit"></i> <span class="nav-label">Tareas</span></a>
                    </li>                                                                      
                    <li>
                        <a href="{{URL::to('alumno/horarioAlumno')}}"><i class="fa fa-tasks"></i> <span class="nav-label">Horario</span></a>
                    </li>
                    <li>
                        <a href="{{URL::to('alumno/asistencia')}}"><i class="fa fa-check-square"></i> <span class="nav-label">Asistencia</span></a>
                    </li>
                    <li>
                        <a href="{{URL::to('alumno/calificaciones')}}"><i class="fa fa-book"></i> <span class="nav-label">Calificaciones</span></a>
                    </li> 
                     <li>
                        <a href={{URL::to('/alumno/insignias')}}><i class="fa fa-star"></i> <span class="nav-label">Insignias</span> </a>
                    </li>
                    <li>
                        <a href={{URL::to('/alumno/encuestas/create')}}><i class="fa fa-book"></i> <span class="nav-label">Encuestas</span></a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!--<form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>-->
        </div>
        <div class="navbar-header">
            <span class="navbar-minimalize minimalize-styl-2" style="margin: 7px 5px 5px 20px;"><img src="{{asset('img/logo-header.png')}}" data-rjs="3" alt="Bold Logo" ></span>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                
                <!--<li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">1</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">1</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>-->


                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Salir
                    </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                </li>
                <!--<li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>-->
            </ul>

        </nav>
        </div>
         <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Flot -->
    <script src="{{ asset('js/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>

    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{ asset('js/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js')}}"></script>

    <!--cropper-->
    <script src="{{asset('js/canvas-to-blob.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.min.js"></script>


         @yield('content')
   
</body>