@extends('Profesor.menuProfesor')
@section('content')
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
		<title>MyAppCollege | Eventos</title>
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css" />
	<style>
    .swal-title {
  margin: 0px;
  font-size: 10px;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
  margin-bottom: 28px;
}
.naranja {
    background-color: #f8ac59;
    border-color: #f8ac59;
    color: #FFFFFF;
    padding: 10px; 
	}
	.verde{
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul1 {
    background-color: #1c84c6;
    border-color: #1c84c6;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul2 {
    background-color: #23c6c8;
    border-color: #23c6c8;
    color: #FFFFFF;
    padding: 10px; 
	}
	.rojo {
    background-color: #ed5565;
    border-color: #ed5565;
    color: #FFFFFF;
    padding: 10px; 
	}
</style>

	</head>
	<body>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/profesor/insignias','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}} 
		<div class="wrapper wrapper-content">
		    <div class="row animated fadeInDown">
		        <div class="col-lg-3">
		            <div class="ibox float-e-margins">
		                <div class="ibox-title">
		                    <h5>Insignias</h5>   
		                </div>
		                <div class="ibox-content">
		                    <div id='external-events'>
		                        
								<div class="form-group">
                                	<label class="control-label">Grupo-Materia</label>
                                </div>
                                <div class="form-group">
                                	<select class="form-control" name="idSubGrupo" id="selectTipo" required="required">
                                		<option>Seleccione</option>
                                	@foreach ($sg as $sg)
                                		<option value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}}</option>
                                	@endforeach
                                	</select>
                                </div>
                                <div class="form-group">
                                	<input type="checkbox" id="selectGrupo" name="selectGrupo" checked="checked" onchange="Desactiva()">Enviar al grupo
                                </div>
								<div class="form-group">
                                	<select class="chosen-select" name="selectAlumno[]" id="selectAlumno" disabled required="required" multiple  tabindex="2" data-placeholder="selecciona alumno">
                                		<option disabled selected value=" "></option>
                                	</select>
                                
                                <!--
                                <div class="form-group">
                                	<select class="form-control" name="Alumno" id="selectAlumno" required="required">
                                		<option>Seleccione</option>
                                		<option value="1"></option>
                                	</select>
                                </div>
                                <div id="div">
                                -->
                                	
                                </div>
                                <div class="form-group">
                                	<button type="submit"  class="btn btn-warning" id="enviar"  >Enviar</i>
                                </div>


		                        
		                    </div>
		                </div>
		            </div>
		            <!--
		            <div class="ibox-content" >
		                <div class="external-events" id="insignia">
		                    <h2>Insignia seleccionada</h2> 
							
		                </div>
		            </div>
		        -->
		        </div>
		        <div class="col-lg-9">
		            <div class="ibox float-e-margins">
		                
		                <div class="ibox-content">
		                    
		                <div class="ibox-content" id="trash" >
		                <div class="external-events" >
		                	<h3>Selecciona una insignia</h3>
						<form  >
		                    <Table>
								<tr>
									<th>1<input type="radio" value="1" required="required" name="idTipoInsignia"><image src="{{ asset('img/insignias/INSIGNIASF_Mesa_de_trabajo_1.png')}}" value="1"  height="210" width="180" method="POST"></th>
									<th>2<input type="radio" required="required" value="2" name="idTipoInsignia"><image src="{{ asset('img/insignias/INSIGNIASF-02.png')}}"  value="2" height="210" width="180" ></th> 
									<th>3<input type="radio" required="required" value="3" name="idTipoInsignia"><image src="{{ asset('img/insignias/INSIGNIASF-03.png')}}"  value="3" height="210" width="180"></th>
									<th>4<input type="radio" value="4" name="idTipoInsignia"><image src="{{ asset('img/insignias/INSIGNIASF-04.png')}}"  value="4" height="210" width="180"></th>
								</tr>
								<br></br>
								<tr>
									<th>5<input type="radio" required="required" value="5" name="idTipoInsignia"><image src="{{ asset('img/insignias/INSIGNIASF-05.png')}}"  value="5" height="210" width="180" ></th>
									<th>6<input type="radio" required="required" value="6" name="idTipoInsignia"><image src="{{ asset('img/insignias/INSIGNIASF-06.png')}}" value="6" height="210" width="180" ></th> 
								</tr>
							</Table>
						</form>
		                </div>
		            </div>
		            </div>
		        </div>
		    </div>
		</div>
{!!Form::close()!!}		
		<script>
			$("#selectTipo").change(function(){
            		var idsg = this.value;

		            $('#selectAlumno').find('option').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('../getAlumnoInsignia')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        
		                        var op =$('<option>',{
		                        	value: item.idAlumno,
		                        	text: item.nombreAlumno + ' ' + item.apepat +' '+ item.apemat
		                        });
		                        $('#selectAlumno').append(op);
		                        $('#selectAlumno').trigger("chosen:updated");
		                        $('#selectAlumno').trigger("liszt:updated");
		                        
		                   
		                		});
		            		}
		        		});
       });

			
		</script>
		<!-- Mainly scripts -->
        <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
        <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
			
			<script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
	        <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
	        <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
        
	<script>
	    $(document).ready(function() 
	    {
	        		
	    	$('.chosen-select').chosen();	
		});		
		function Desactiva()
		{

			if($('#selectGrupo').is(':checked')==true)
			{
				document.getElementById("selectAlumno").disabled=true;
				
			}
			else
			{	
				document.getElementById("selectAlumno").disabled=false;
				
			}
			$('#selectAlumno').trigger("chosen:updated");
		    $('#selectAlumno').trigger("liszt:updated");
			console.log($('#selectAlumno').is(':disabled'));
		}
	</script>
	</body>
	@endsection
