@extends('Alumno.menuAlumno')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('alumno/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('alumno/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Papelera </a></li>
                        </ul>
                       
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">




           

            <h2>
                Bandeja de entrada
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
                <div class="btn-group pull-right">
                    <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                    <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                </div>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh in"><i class="fa fa-refresh"></i> Actualizar</button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as read"><i class="fa fa-eye"></i> </button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as important"><i class="fa fa-exclamation"></i> </button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>

            </div>
        </div>
            <div class="mail-box">

            <table class="table table-hover table-mail">
            <tbody>
                 <tr>
                <td><strong>Tipo</strong></td>
                <td><strong>Emisor</strong></td>
                <td><strong>Asunto</strong></td>
                <td> </td>
                <td><strong>Fecha</strong></td>
                <td><strong>Borrar</strong></td>
                </tr>
                
                @foreach($mensajes as $mensajes)
                @php
                if($mensajes->Visto!=0)
                    echo '<tr class="read">' ;
                else
                    echo '<tr class="unread">'
                @endphp


                    @php

                        switch ($mensajes->TipoEmisor) {
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($alumno as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '2':
                                foreach($profesor as $profesores)
                                {
                                    if($profesores->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$profesores->Nombre.'</td>';

                                }
                            break;

                        }
                    @endphp


                    <td class="mail-subject"><a href="{{ route('Muestra2',['id' => $mensajes->id])}}">{{ $mensajes->Asunto }}</a></td>
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td>

                </tr>
                @endforeach
            </tbody>
            </table>


            </div>
        </div>
    </div>




        </div>

@endsection
