@extends('Alumno.menuAlumno')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Calificaciones</h2>
<ol class="breadcrumb">
</ol>
</div>
<div class="col-lg-2">
</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5>Administración de calificaciones</h5>
<div class="ibox-tools">
</div>
</div>
<div class="ibox-content">

<div class="row">
<div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Boleta de calificaciones</p>
</div>
</div>
<div class="form-horizontal">
        <div class="form-group">
        	

			<label class="col-sm-2 control-label">Periodo</label>
			<div class="col-sm-8">
				<select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
					<option></option>
            		@foreach($periodo as $p)
                    	<option value="{{$p->idPeriodo}}">{{$p->nombrePeriodo}} </option>
                    @endforeach
            	</select>
			</div>
        </div>
    </div>
<div class="row">
<div class="col-lg-12 col-md-6 col-md-6 col-xs-6">
<div class="table-responsive">
<table class="table table-striped table-bordered table-condensed table-hover">
<th width="50" height="50">id</th>
<th>Grupo</th>
<th>Alumno</th>
<th>Materia</th>
<th>Evaluacion</th>
<tbody id="rows">
	
</tbody>

</table>
</div>
</div>
</div>
<script >
	$("#selectPeriodos").change(function(){
		var idP = this.value;	         
        $('#rows').find('tr').remove();
        $.ajax({
            method: 'get',
            url: "{{('getCalificacionesAlumno')}}",
            data: {idP:idP},
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    //console.log(item.nombre);
                    var fila = $('<tr>');

                    var id =$('<td>',{
                    	text: item.idAlumno,
                    	
                    });

                    var grupo =$('<td>',{
                    	text: item.grado + item.grupo
                    });
                    var nombre =$('<td>',{
                    	text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                        });
                    var materia =$('<td>',{
                    	text: item.nombreMateria 
                    });
                    var calif =$('<td>',{
                    	text: item.calif
                    });
                    
                    fila.append(id);
                    fila.append(grupo);
                    fila.append(nombre);
                    fila.append(materia);
                    fila.append(calif);
                    
                    $('#rows').append(fila);
               
            		});
        		}
    		});
        });
</script>
@endsection