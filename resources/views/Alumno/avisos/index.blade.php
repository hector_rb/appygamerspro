@extends('Alumno.menuAlumno')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h1>Avisos</h1>
            <p>En esta seccion se visualizan los avisos enviados por la escuela</p>
        </div>
        
    </div>
<div class="wrapper wrapper-content">
        <div class="row">
            
            <div class="col-lg-12 animated fadeInRight">
            <div class="mail-box-header">

                <form method="get" action="index.html" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="search" placeholder="termino a buscar...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                Buscar
                            </button>
                        </div>
                    </div>
                </form>
                <h2>
                    Avisos Recibidos
                </h2>

                
            </div>
                <div class="ibox-content">

                <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Asunto</th>
                        <th>Para</th>
                        <th class="text-right mail-date">Fecha</th>
                        <th>Ver</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($avisos as $a)
                    <tr class="read">
                        
                        <td class="mail-subject">{{$a->asunto}}</td>
                        @if($a->destinatario==1)
                            <td>General</td>
                        @endif
                        
                        @if($a->destinatario==2)
                            <td>Tutores</td>                            
                        @endif
                        @if($a->destinatario==4)
                            <td>Alumnos</td>                            
                        @endif
                        <td class="text-right mail-date">{{$a->fecha}}</td>
                        <td ><a href="{{URL::action('avisosAlumnoController@show',['id'=>$a->idAviso])}}"><button class="btn btn-success">Ver</button></a></td>
                    </tr>
                @endforeach
                
                </tbody>
                </table>
                {{ $avisos->links() }}

                </div>
            </div>
        </div>
        </div>
@endsection