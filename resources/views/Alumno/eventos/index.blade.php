@extends('Alumno.menuAlumno')

@section('content')
<html>
	<head>
		<title>MyAppCollege | Eventos</title>
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css" />
	<style>
    .swal-title {
  margin: 0px;
  font-size: 10px;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
  margin-bottom: 28px;
}
.naranja {
    background-color: #f8ac59;
    border-color: #f8ac59;
    color: #FFFFFF;
    padding: 10px; 
	}
	.verde{
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul1 {
    background-color: #1c84c6;
    border-color: #1c84c6;
    color: #FFFFFF;
    padding: 10px; 
	}
	.azul2 {
    background-color: #23c6c8;
    border-color: #23c6c8;
    color: #FFFFFF;
    padding: 10px; 
	}
	.rojo {
    background-color: #ed5565;
    border-color: #ed5565;
    color: #FFFFFF;
    padding: 10px; 
	}
</style>

	</head>
	<body>

		<div class="wrapper wrapper-content">
		    <div class="row animated fadeInDown">
		        
		        <div class="col-lg-12">
		            <div class="ibox float-e-margins">
		                
		                <div class="ibox-content">
		                    <div id="calendar"></div>
		                </div>
		                <div class="ibox-content" id="trash" >
		                <div class="external-events" >
		                    <center><span class="fa fa-trash"  style="font-size: 100px"></span></center>
		                </div>
		            </div>
		            </div>
		        </div>
		    </div>
		</div>
		
		<!-- Mainly scripts -->
        <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>

        

		<script>
			/*$(document).bind("contextmenu",function(e) {
     e.preventDefault();
});*/

			var currentMousePos = {
			    x: -1,
			    y: -1
			};
			 
			jQuery(document).on("mousemove", function (event) {
			   currentMousePos.x = event.pageX;
			   currentMousePos.y = event.pageY;
			});

		    $(document).ready(function() {

		            $('.i-checks').iCheck({
		                checkboxClass: 'icheckbox_square-green',
		                radioClass: 'iradio_square-green'
		            });

		        /* initialize the external events
		         -----------------------------------------------------------------*/
		         $( "#evento" ).click(function() {

		         	//$( "<div><p>Hello</p></div>" ).appendTo( "#eventos" )

		         	if ($('#titulo').val()!='' && $('#desc').val()!='') 
		         	{
		         	     $( "<div></div>", {
						  "class": $('input[name=priority]:checked').val(),
						  html: '<h4>' + $('#titulo').val().replace(/\s\s+/g, ' ') + '</h4>  <p>' + $('#desc').val().replace(/\s\s+/g, ' ') + '</p>',
						  val:$('#desc').val(), 
						  on: {
						    mouseover: function( event ) {
						      // Do something
						      	var content = $(this).text().split('  ');
						      	var divclass = $(this).attr("class");
						      	console.log(content);
						      	$(this).data('event', {

							      	


					                title:content[0], // use the element's text as the event title
					                description:$(this).val(),
					                stick: true, // maintain when user navigates (see docs on the renderEvent method)
					                editable:false,
					                eventDurationEditable:false,
					                resourceEditable: true ,
					                className:divclass,
				            });

				            // make the event draggable using jQuery UI
				            $(this).draggable({
				                zIndex: 1111999,
				                revert: true,      // will cause the event to go back to its
				                revertDuration: 0  //  original position after the drag

				            });
								    }
								  }

						}).appendTo( "#eventos" )/*.data('event', {
		                title: $.trim($(this).text()), // use the element's text as the event title
		                stick: true // maintain when user navigates (see docs on the renderEvent method)
		            	}).draggable({
			                zIndex: 1111999,
			                revert: true,      // will cause the event to go back to its
			                revertDuration: 0  //  original position after the drag
			            	});*/
			         }
			         else
			         {
			         	swal({
			                title: "No se puede crear el evento",
			                text: "No has llenado todos los campos",
			                type:'warning'
			                
			            });

			         }


					});

		        $('#external-events div.external-event').each(function() {

		            // store data so the calendar knows to render an event upon drop
		            $(this).data('event', {
		                title: $.trim($(this).text()), // use the element's text as the event title
		                stick: true // maintain when user navigates (see docs on the renderEvent method)
		            });

		            // make the event draggable using jQuery UI
		            $(this).draggable({
		                zIndex: 1111999,
		                revert: true,      // will cause the event to go back to its
		                revertDuration: 0  //  original position after the drag
		            });

		        });


		        /* initialize the calendar
		         -----------------------------------------------------------------*/
		        var date = new Date();
		        var d = date.getDate();
		        var m = date.getMonth();
		        var y = date.getFullYear();		      
				var zone = "-05:00";
				var eventos= <?= json_encode($array)?>;

				function isElemOverDiv() {
				   var trashEl = jQuery('#trash');
				   var ofs = trashEl.offset();
				   var x1 = ofs.left;
				   var x2 = ofs.left + trashEl.outerWidth(true);
				   var y1 = ofs.top;
				   var y2 = ofs.top + trashEl.outerHeight(true);
				   if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&      currentMousePos.y >= y1 && currentMousePos.y <= y2) 
				   	{      return true;    }    
				   return false; 
				}
		        $('#calendar').fullCalendar({
		        	locale:'es',
		            header: {
		                left: 'prev,next today',
		                center: 'title',

		                right: 'month,agendaWeek,agendaDay'
		            },
		            
		            editable: false,
		            droppable: true, // this allows things to be dropped onto the calendar
		            drop: function() {
		                // is the "remove after drop" checkbox checked?
		                //if ($('#drop-remove').is(':checked')) {
		                    // if so, remove the element from the "Draggable Events" list
		                    $(this).remove();
		                //}
		            },
					eventReceive: function(event){
					    var title = event.title;
					    var start = event.start.format();
					    var clase= event.className[0];
					    $.ajax({
					      url: "{{('saveEvent')}}",
					      data: {type:1,title:title,startdate:start,zone:zone, description:event.description, class:clase},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					        event.id = response.eventid;
					        $('#calendar').fullCalendar('updateEvent',event);
					        console.log('funciona');
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					    $('#calendar').fullCalendar('updateEvent',event);
					},
					eventClick: function(event, jsEvent, view) {
					   swal({
			                title: "Titulo del evento",
			                text: event.title,
			                showCancelButton: true, 
			            }).then(function(title) {
								
							   //event.description=desc.inputValue;
								
							   swal({
					                title: "Descripcion del evento",
					                text: event.description,
					                showCancelButton: true, 
					            }).then(function(desc) {
										
									   
			  						}
					            );
	  						}
			            );
			            
					   /*var desc = swal({
			                title: "Editar Evento",
			                text: "Descripción del evento",
			                inputValue: event.description,
			                type:"input",
			            });*/
			            
						
					  
					},
					eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) 
					{	
						var start = event.start.format();
					    var end = event.end.format();
					    $.ajax({
					      url: "{{('saveEvent')}}",
					      data: {type:4,end:end, start:start,id:event.id},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					},
					eventDrop: function( event, delta, revertFunc, jsEvent, ui, view )
					{
						var start = event.start.format();
					    var end = (event.end == null) ? start : event.end.format();
					    $.ajax({
					      url: "{{('saveEvent')}}",
					      data: {type:4,end:end, start:start,id:event.id},
					      method: 'get',
					      dataType: 'json',
					      success: function(response){
					      },
					      error: function(e){
					        console.log(e.responseText);
					      }
					   });
					},
					eventDragStop: function (event, jsEvent, ui, view) {
									   if (isElemOverDiv()) {
									     //var con = confirm('Are you sure to delete this event permanently?');
									     swal({
								                title: "¿Seguro que quieres eliminar el evento?",
								                text: "Esta acción sera permanente",
								                type:'warning',
								                showCancelButton: true, 
								            }).then(function(desc) {
												   //event.description=desc.inputValue;
													console.log(event.id);
												   $.ajax({
												     url: "{{('saveEvent')}}",
												     data:{type:5,id:event.id},
												     type: 'get',
												     dataType: 'json',
												     success: function(response){
												       if(response.status == 'success')
												       $('#calendar').fullCalendar('removeEvents', event.id);
          											   
												     },
												     error: function(e){
												       alert('Error processing your request: '+e.responseText);
												     }
												   });
						  						}
								            );
									     /*if(con == true) {
									        $.ajax({
									          url: 'process.php',
									          data: 'type=remove&eventid='+event.id,
									          type: 'POST',
									          dataType: 'json',
									          success: function(response){
									            if(response.status == 'success')
									              $('#calendar').fullCalendar('removeEvents');
									              $('#calendar').fullCalendar('addEventSource', JSON.parse(json_events));
									          },
									          error: function(e){
									          alert('Error processing your request: '+e.responseText);
									          }
									       });
									      }*/
									    }
									},
		            events: eventos,
		            eventRender: function(event, element) { 
            		element.find('.fc-title').append("<br/>" + event.description);
            		} 
		        });


		    });

		</script>

	</body>
	@endsection
	