@extends('Alumno.menuAlumno')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Tareas</h2>
<ol class="breadcrumb">
<li class="active">
<a href="/alumno/tareas">Tareas Pendientes</a>
</li>
<li>
<a href="maestro/create"><strong>Enviar tarea</strong></a>
</li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>

 <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Administración de tareas</h5>
                    <div class="ibox-content">
<div class="row">
<div class="col-lg-6 col-md-6 col-md-6 col-xs-6">
<h4>Crear nueva tarea</h4>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
@endif
</div>
</div>
{!!Form::open(array('url'=>'tareas/maestro','method'=>'POST','autocomplete'=>'off', 'files'=>'true' ))!!}
{{Form::token()}}

<div class="row">
<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="idmateria">Materia</label>
<select name="idmateria" class="form-control">
<option value=""></option>
@foreach ($materia as $m)
<option required value="{{$m->idMateria}}">{{$m->nombre}}</option>
@endforeach
</select>
</div>
</div>

<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="idgrupo">Grupo</label>
<select name="idgrupo" class="form-control">
<option value=""></option>
@foreach ($grupo as $g)
<option required value="{{$g->idgrupo}}">{{$g->idgrupo}}</option>
@endforeach
</select>
</div>
</div>

<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="fechaentrega">Fecha a entregar</label>
<input type="date" name="fechaentrega" required value="{{old('fechaentrega')}}" class="form-control" placeholder="fechaentrega...">
</div>
</div>

<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
<div class="form-group">
<label for="anexo">Anexo</label>
<input type="file" name="anexo" required value="{{old('anexo')}}" class="form-control">
</div>
</div>

<div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
<div class="form-group">
<label class="col-sm-2 control-label" for="titulo">Título:</label>
<input type="text" name="titulo" required value="{{old('titulo')}}" class="form-control" placeholder="título...">
</div>
</div>


<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<div class="form-group">
<label for="descripcion">Descripción</label>
<textarea name="descripcion" required value="{{old('descripcion')}}" class="form-control" rows="10" cols="150" placeholder="descripción..."></textarea>
</div>
</div>

<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
<div class="form-group">
<button class="btn btn-primary" type="submit">Guardar cambios</button>
<button class="btn btn-danger" type="reset">Cancelar</button>
</div></div></div>
</div></div></div>
</div></div></div>
</div>
{!!Form::close()!!}
@endsection