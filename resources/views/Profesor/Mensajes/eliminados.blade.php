@extends('Profesor.menuProfesor')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="{{URL::to('admin/mensajes/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('profesor/mensajes/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <li><a href="{{URL::to('profesor/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">


            <h2>
                Bandeja de entrada
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
              
            </div>
        </div>
            <div class="mail-box">

            <table class="table table-hover table-mail">
            <tbody>

                @foreach($mensajes as $mensajes)

                @php
                if($mensajes->Visto!=0)
                    echo '<tr class="read">' ;
                else
                    echo '<tr class="unread">'
                @endphp


                    @php

                        switch ($mensajes->TipoEmisor) {
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($alumno as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '2':
                                foreach($profesor as $profesores)
                                {
                                    if($profesores->id==$mensajes->IdEmisor)
                                        echo '<td class="mail-ontact"> ' .$profesores->Nombre.'</td>';

                                }
                            break;
                        }
                    @endphp

                    <td class="mail-subject"><a href="">{{ $mensajes->Asunto }}</a></td>
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td>

                   <td class="check-mail">
                       <a class="btn btn-w-m btn btn-w-m btn-outline btn-danger btn-xs" href="" data-target="#modal-delete-{{$mensajes->id}}" data-toggle="modal" >Borrar</a>
                   </td>

                </tr>
                </div>
                </div>


                @include('Admin.modal')

                @endforeach
            </tbody>
            </table>




            </div>
        </div>
    </div>




@endsection
