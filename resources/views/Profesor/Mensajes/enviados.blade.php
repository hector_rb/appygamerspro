@extends('Profesor.menuProfesor')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="{{URL::to('profesor/mensajes/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('profesor/mensajes/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <li><a href="{{URL::to('profesor/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                      
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">



            <h2>
                Bandeja de entrada
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
              
        </div>
            <div class="mail-box">

            <table class="table table-hover table-mail">
            <tbody>
                 <tr>
                <td><strong>Destinatario</strong></td>
                <td><strong>Asunto</strong></td>
                <td> </td>
                <td><strong>Fecha</strong></td>
                <td><strong>Borrar</strong></td>
                </tr>

                @foreach($mensajes as $mensajes)
                @php
                if($mensajes->Visto!=0)
                    echo '<tr class="read">' ;
                else
                    echo '<tr class="unread">'
                @endphp

                    @php
                        switch ($mensajes->TipoDestinatario) {
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($var as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '2':
                                foreach($var as $profesores)
                                {
                                    if($profesores->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$profesores->Nombre.'</td>';

                                }
                            break;

                            case '3':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($var as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '4':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($var as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '5':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($var as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '6':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '7':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }
                            break;
                            
                            case '8':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;
                            case '9':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;
                            case '10':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;
                            case '11':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;
                            case '12':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($grupo as $alumnos)
                                {
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                        echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                        }
                    @endphp


                    <td class="mail-subject"><a href="{{ route('mensajes.show',['id' => $mensajes->id])}}">{{ $mensajes->Asunto }}</a></td>
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td>

                   <td class="check-mail">
                       <a class="btn btn-w-m btn btn-w-m btn-outline btn-danger btn-xs" href="" data-target="#modal-delete-{{$mensajes->id}}" data-toggle="modal" >Borrar</a>

                    </td>

                </tr>


                <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$mensajes->id}}">
                    <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-Label="Close">
                                    <span aria-hidden="true">x</span>
                                    </button>
                                    <h4 class="modal-title">Eliminar Mensaje</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Confirme si desea Eliminar Mensaje</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cerrar</button>
                                    <a class="btn btn-w-m btn-outline btn-danger" href="{{ route('mensajes/destroy',['id' => $mensajes->id] )}}" >Borrar</a>
                                </div>
                            </div>
                    </div>
                </div>

                </div>
                </div>
                </div>



                @endforeach
            </tbody>
            </table>


            </div>
        </div>
    </div>




        </div>

@endsection
