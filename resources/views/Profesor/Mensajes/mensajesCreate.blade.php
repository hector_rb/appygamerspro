@extends('Profesor.menuProfesor')
@section('content')

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                       <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/mensajes/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('profesor/mensajes/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <li><a href="{{URL::to('profesor/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                       
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">
                 <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                                {!! Form::open(['route' => 'mensajes.store', 'method' => 'post','files'=>true,'novalidate']) !!}
                                    <div class="form-group">
                                          {!! Form::label('Asunto', 'Asunto') !!}
                                          {!! Form::text('Asunto', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('TipoDestinatario', 'A quien deseas enviar tu mensaje?') !!}
                                        <select class="form-control m-b" id="TipoDestinatario" name="TipoDestinatario">
                                            <option> </option>>
                                            @foreach ($Destinatarios as $key)
                                                <option value="{{$key->id}}">{{$key->TipoDestinatario}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div id="div">
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('IdDestinatario', 'Selecciona el destinatario') !!}
                                        <select class="form-control m-b" name="IdDestinatario" id="Destinatario">
                                            <option value="" disabled selected>Selecciona</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('Mensaje', 'Mensaje') !!}
                                        {!! Form::textarea('Mensaje', null,array('placeholder' => 'Mensaje','class' => 'form-control','style'=>'height:150px')) !!}
                                    </div>

                                    

                                    <div class="form-group">
                                        {!! Form::submit('Enviar', ['class' => 'btn btn-info ' ] ) !!}
                                    </div>

                                {!! Form::close() !!}
                        </div>
                 </div>
             </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script type="text/javascript" src="script.js"></script>
    </div>
    </div>


    <!-- Mainly scripts -->
    <script src={{ asset("js/jquery-3.1.1.min.js")}}></script>
    <script src={{ asset("js/bootstrap.min.js")}}></script>
    <script src={{ asset("js/plugins/metisMenu/jquery.metisMenu.js")}}></script>
    <script src={{ asset("js/plugins/slimscroll/jquery.slimscroll.min.js")}}></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src={{ asset("js/plugins/pace/pace.min.js")}}></script>

    <!-- iCheck -->
    <script src={{ asset("js/plugins/iCheck/icheck.min.js")}}></script>

    <!-- SUMMERNOTE -->
    <script src={{ asset("js/plugins/summernote/summernote.min.js")}}></script>
    
    <script>
        
        
          $(document).ready(function() {

		    		$('#TipoDestinatario').change(function(){
		    		    var TipoDes = this.value;
                        $('#Destinatario').find('option').not(':first').remove();
		    			$('#div').find('div').remove();
		    			switch(this.value)
		    			{
		    			    
		    			    			
		    			    case '2':
		    			        	$('#Destinatario').find('option').remove();
		    					
		    						var sNivel	
		    						var div1=$('<div>',{
		    							class:'form-group'

		    						});
		    					
		    						var sgrupo=$('<select>',{
		    							class:'form-control',
		    							
		    							on:{
		    								change:function()
		    								{
		    									$('#Destinatario').find('option').remove();
		    								
                        		    									
                        		    			$.ajax(
                                                {
                                                    method: 'get',
                                                    url: "{{('getAlumnosP')}}",
                                                    data: {id:this.value},
                                                    dataType: 'json',
                                                    success: function (data)
                                                    {
                        
                                                        console.log(data);
                                                        $.each(data, function (i, item)
                                                        {
                        
                                                            console.log(item.Nombre);
                                                            $('#Destinatario').append($('<option>', 
                                                            {
                                                                value: item.iduser,
                                                                text : item.nombreAlumno
                                                            }));
                                                        });
                                                    }
                                                });
				    						}
				    					}
		    						});
		    						
		    						
		    						
		    						
		    						
                                     $('#Destinatario').find('option').not(':first').remove();
                                     
									var sgrado=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    								    console.log(this.value);
		    								    $('#Destinatario').find('option').remove();
		    									sgrupo.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGruposP')}}",
									                data:{idNivel:sNivel.val(),grado:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                         sgrupo.append($('<option>', {
									                            value: item.idgrupo,
									                            text : item.grado + item.grupo
									                        }));
									                    });
									                }
									            });
				    						}
				    					}
		    						});
		    						
		    					    
		    					    
		    					
		    					    


		    						sNivel=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    									sgrado.find('option').not(':first').remove();
		    									$('#Destinatario').find('option').remove();
		    									sgrupo.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGradosP')}}",
									                data:{idNivel:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                        sgrado.append($('<option>', {
									                            value: item.grado,
									                            text : item.grado
									                        }));
									                    });
									                }
									            });
				    						}
		    							}
		    						});
		    					
		    						var opt=$('<option>');
		    						var opt2=$('<option>');
		    						var opt3=$('<option>');
		    						sNivel.append(opt);
		    						sgrado.append(opt2);
		    						sgrupo.append(opt3);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivelP')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        console.log('holis');
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });

		    						div1.append(sNivel);
		    						div1.append(sgrado);
		    						div1.append(sgrupo);
		    						$('#div').append(div1);
		    				break;
		    						
		    						
		    			
		    						
		    			    case '4':
		    					
		    						var sNivel	
		    						var div1=$('<div>',{
		    							class:'form-group'

		    						});
		    					
		    						var sgrupo=$('<select>',{
		    							class:'form-control',
		    							
		    							on:{
		    								change:function()
		    								{
		    									$('#Destinatario').find('option').remove();
		    								
                        		    									
                        		    			$.ajax(
                                                {
                                                    method: 'get',
                                                    url: "{{('getAlumnosP')}}",
                                                    data: {id:this.value},
                                                    dataType: 'json',
                                                    success: function (data)
                                                    {
                        
                                                        console.log(data);
                                                        $.each(data, function (i, item)
                                                        {
                        
                                                            console.log(item.Nombre);
                                                            $('#Destinatario').append($('<option>', 
                                                            {
                                                                value: item.iduser,
                                                                text : item.nombreAlumno
                                                            }));
                                                        });
                                                    }
                                                });
				    						}
				    					}
		    						});
		    						
		    						
		    						
		    						
		    						
                                     $('#Destinatario').find('option').not(':first').remove();
                                     
									var sgrado=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    								    console.log(this.value);
		    								    $('#Destinatario').find('option').remove();
		    									sgrupo.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGruposP')}}",
									                data:{idNivel:sNivel.val(),grado:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                         sgrupo.append($('<option>', {
									                            value: item.idgrupo,
									                            text : item.grado + item.grupo
									                        }));
									                    });
									                }
									            });
				    						}
				    					}
		    						});
		    						
		    					    
		    					    
		    					
		    					    


		    						sNivel=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    									sgrado.find('option').not(':first').remove();
		    									$('#Destinatario').find('option').remove();
		    									sgrupo.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGradosP')}}",
									                data:{idNivel:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                        sgrado.append($('<option>', {
									                            value: item.grado,
									                            text : item.grado
									                        }));
									                    });
									                }
									            });
				    						}
		    							}
		    						});
		    					
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivelP')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        console.log('holis');
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });
                                    	
		    						var opt=$('<option>');
		    						var opt2=$('<option>');
		    						var opt3=$('<option>');
		    						var opt4=$('<option>');
		    						
		    						sNivel.append(opt);
		    						sgrado.append(opt2);
		    						sgrupo.append(opt3);
		    						div1.append(sNivel);
		    						div1.append(sgrado);
		    						div1.append(sgrupo);
		    						$('#div').append(div1);
		    						break;
		    				case '10':
		    				    	$('#Destinatario').find('option').remove();
		    						var div1=$('<div>',{
		    							class:'form-group '
		    						});
		    						var sNivel=$('<select>',{
		    							class:'form-control',
		    							id:'user',
		    							required:'required'
		    						})
		    					//	div1.append(sNivel);
		    					//	$('#div').append(div1);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivelP')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        //console.log(item.nombre);
						                         $('#Destinatario').append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });
		    				break;
		    				case '6':
		    				    	$('#Destinatario').find('option').remove();
		    						var sNivel	
		    						var div1=$('<div>',{
		    							class:'form-group'

		    						});
		    						
									var sgrado=$('<select>',{
		    							class:'form-control',
		    							id:'user',
		    							required:'required'
		    						});


		    						sNivel=$('<select>',{
		    							class:'form-control',
		    							id:'selectNivel',
		    							on:{
		    								change:function()
		    								{
		    									sgrado.find('option').not(':first').remove();
		    									//$('#Destinatario').find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGradosP')}}",
									                data:{idNivel:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    var hue;
									                    $.each(data, function (i, item) {
									                        
									                        hue=item.idNivel+'-'+item.grado;
									                        $('#Destinatario').append($('<option>', {
									                            value: hue,
									                            text : item.grado
									                        }));
									                    });
									                }
									            });
				    						}
		    							}
		    						});
		    						var opt=$('<option>');
		    						var opt2=$('<option>');
		    						sNivel.append(opt);
		    						sgrado.append(opt2);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivelP')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        //console.log(item.nombre);
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });
                                    console.log('holis');
		    						div1.append(sNivel);
		    						//div1.append(sgrado);
		    						$('#div').append(div1);
		    						break;
		    				case '7':
		    				    	$('#Destinatario').find('option').remove();
		    						var sNivel	
		    						var div1=$('<div>',{
		    							class:'form-group'

		    						});
		    						
		    						var sgrupo=$('<select>',{
		    							class:'form-control',
		    							id:'user',
		    							required:'required'
		    						});
		    						
                                     $('#Destinatario').find('option').not(':first').remove();
                                     
									var sgrado=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    									sgrupo.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGruposP')}}",
									                data:{idNivel:sNivel.val(),grado:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                         $('#Destinatario').append($('<option>', {
									                            value: item.idgrupo,
									                            text : item.grado + item.grupo
									                        }));
									                    });
									                }
									            });
				    						}
				    					}
		    						});


		    						sNivel=$('<select>',{
		    							class:'form-control',

		    							on:{
		    								change:function()
		    								{
		    									sgrado.find('option').not(':first').remove();
		    									sgrupo.find('option').not(':first').remove();
		    									$.ajax({
									                method: 'get',
									                url: "{{('getGradosP')}}",
									                data:{idNivel:this.value},
									                dataType: 'json',
									                success: function (data) {
									                    $.each(data, function (i, item) {
									                        //console.log(item.nombre);
									                        sgrado.append($('<option>', {
									                            value: item.grado,
									                            text : item.grado
									                        }));
									                    });
									                }
									            });
				    						}
		    							}
		    						});
		    						var opt=$('<option>');
		    						var opt2=$('<option>');
		    						sNivel.append(opt);
		    						sgrado.append(opt2);
		    						$.ajax({
						                method: 'get',
						                url: "{{('getNivelP')}}",
						                dataType: 'json',
						                success: function (data) {
						                    $.each(data, function (i, item) {
						                        //console.log(item.nombre);
						                        sNivel.append($('<option>', {
						                            value: item.idNivel,
						                            text : item.nombreNivel
						                        }));
						                    });
						                }
						            });

		    						div1.append(sNivel);
		    						div1.append(sgrado);
		    					//	div1.append(sgrupo);
		    						$('#div').append(div1);
		    						break;
		    				default: 
		    					$('#Destinatario').find('option').remove();
		    						var div1=$('<div>',{
		    							class:'form-group deletable'
		    						});
		    						var id=$('<input>',{
									     	type:'hidden',
									     	id:'user',
									     	required:'required',
									     	val:'0'
									     });
		    						div1.append(id);
		    						$('#div').append(div1);
		    						
		    						
		    						$.ajax(
                        {
                            method: 'get',
                            url: "{{('getDestinatario')}}",
                            data: {id:TipoDes},
                            dataType: 'json',
                            success: function (data)
                            {

                                console.log(data);
                                $.each(data, function (i, item)
                                {

                                    console.log(item.Nombre);
                                    $('#Destinatario').append($('<option>', 
                                    {
                                        value: item.id,
                                        text : item.Nombre
                                    }));
                                });
                            }
                        });
                        
		    				break;
		    			}
		    		});
          });
       
        </script>

    
    
    <script>
        $(document).ready(function(){

            $('.summernote').summernote();

        });

    </script>
    
    
    
@endsection
