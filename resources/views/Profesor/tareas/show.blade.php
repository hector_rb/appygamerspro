@extends('Profesor.menuProfesor')
@section('content')

<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>   
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Tareas
                </h1>
                <h3 class="font-bold c">tareas de la institución</h3>
            </center>
        </form>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/tareas/create')}}">Redactar tarea</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/tareas')}}"> <i class="fa fa-inbox "></i>Registro de tareas</a></li>
                            <li><a href="{{URL::to('profesor/tareas/create')}}"> <i class="fa fa-envelope-o"></i>Enviar tarea</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
                    <div class="mail-box-header">
                        <!--  botones de eliminar <div class="pull-right tooltip-demo">
                            <a href="mail_compose.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Reply</a>
                            <a href="mail_detail.html#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
                            <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
                        </div> -->
                        <h2>
                            Detalles de Tarea
                        </h2>
                        <div class="mail-tools tooltip-demo m-t-md">


                            <h3>
                                <span class="font-normal">Titulo:  </span>{{$tareas->titulo}}
                            </h3>
                            <h5>
                                <span class="pull-right font-normal">Fecha de entrega: {{$tareas->fechaentrega}}</span>
                                <span class="font-normal">Materia: </span>{{$tareas->nombreMateria." ".$tareas->grado.$tareas->grupo}}
                            </h5>
                        </div>
                    </div>
                        <div class="mail-box">


                        <div class="mail-body">
                            <p>
                            {{$tareas->descripcion}}
                            </p>
                        </div>
                        @if(!$tareas->anexo == null)
                            <div class="mail-attachment">
                                <p>
                                    <span><i class="fa fa-paperclip"></i> Archivo adjunto </span>

                                </p>

                                <div class="attachment">
                                    <div class="file-box">
                                        <div class="file">
                                            <a target="_blank" href="{{asset('anexo/tareas/'.$tareas->anexo)}}">
                                                <span class="corner"></span>

                                                <div class="icon">
                                                    <i class="fa fa-file"></i>
                                                </div>
                                                <div class="file-name">
                                                    {{$tareas->anexo}}
                                                    <br>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                @endif

                                </div>

                                <div class="clearfix"></div>


                        </div>
                    </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>

@endsection
