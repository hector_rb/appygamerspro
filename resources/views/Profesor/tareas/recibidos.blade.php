@extends('Profesor.menuProfesor')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="/admin/mensajes/create">Redactar Correo</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="/admin/mensajes/recibidos"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">16</span> </a></li>
                            <li><a href="mailbox.html"> <i class="fa fa-envelope-o"></i> Enviar Correo</a></li>
                            <li><a href="mailbox.html"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                        <h5>Categories</h5>
                        <ul class="category-list" style="padding: 0">
                            <li><a href="#"> <i class="fa fa-circle text-navy"></i> Work </a></li>
                            <li><a href="#"> <i class="fa fa-circle text-danger"></i> Documents</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-primary"></i> Social</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-info"></i> Advertising</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-warning"></i> Clients</a></li>
                        </ul>

                        <h5 class="tag-title">Labels</h5>
                        <ul class="tag-list" style="padding: 0">
                            <li><a href=""><i class="fa fa-tag"></i> Family</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Work</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Home</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Children</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Holidays</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Music</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Photography</a></li>
                            <li><a href=""><i class="fa fa-tag"></i> Film</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>




        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">

            


            {!! Form::open(['route' => 'mensajes/search', 'method' => 'post', 'novalidate', 'class' => 'pull-right mail-search']) !!}
             <div class="input-group">
                 <input type="text" class="form-control input-sm" name="Asunto" placeholder="Buscar">
                 <div class="input-group-btn">
                     <button type="submit" class="btn btn-sm btn-primary">
                         Buscar
                     </button>
             </div>
         </div>

            {!! Form::close() !!}


            <h2>
                Bandeja de entrada
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
                <div class="btn-group pull-right">
                    <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                    <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                </div>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh in"><i class="fa fa-refresh"></i> Actualizar</button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as read"><i class="fa fa-eye"></i> </button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as important"><i class="fa fa-exclamation"></i> </button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>

            </div>
        </div>
            <div class="mail-box">

            <table class="table table-hover table-mail">
            <tbody>

                @foreach($mensajes as $mensajes)
                @php
                if($mensajes->Visto!=0)
                    echo '<tr class="read">' ;
                else
                    echo '<tr class="unread">'
                @endphp
                    <td class="check-mail">
                        <input type="checkbox" class="i-checks">
                    </td>

                    @php

                        switch ($mensajes->TipoDestinatario) {
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($alumno as $alumnos)
                                    if($alumnos->id==$mensajes->IdDestinatario)
                                    echo '<td class="mail-ontact"> ' .$alumnos->Nombre.'</td>';


                            break;
                            case '2':
                                foreach($profesor as $profesores)
                                if($profesores->id==$mensajes->IdDestinatario)
                                echo '<td class="mail-ontact"> ' .$profesores->Nombre.'</td>';

                            break;

                        }
                    @endphp


                    <td class="mail-subject"><a href="{{ route('mensajes.show',['id' => $mensajes->id])}}">{{ $mensajes->Asunto }}</a></td>
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td>

                </tr>
                @endforeach
            </tbody>
            </table>


            </div>
        </div>
    </div>




        </div>

@endsection
