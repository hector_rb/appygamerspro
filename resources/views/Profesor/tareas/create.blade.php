@extends('Profesor.menuProfesor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>   
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Tareas
                </h1>
                <h3 class="font-bold c">Crear tarea</h3>
            </center>
        </form>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/tareas/create')}}">Redactar tarea</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                             <li><a href="{{URL::to('profesor/tareas')}}"> <i class="fa fa-inbox "></i>Registro de tareas</a></li>
                            <li><a href="{{URL::to('profesor/tareas/create')}}"> <i class="fa fa-envelope-o"></i>Enviar tarea</a></li>
                        </ul>
                        <ul class="category-list" style="padding: 0">
                        </ul>
                    </div>
                </div>
            </div>
        </div>

 <div class="col-lg-9 animated fadeInRight">

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">

@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
@endif

{!!Form::open(array('url'=>'/profesor/tareas','method'=>'POST','autocomplete'=>'off', 'files'=>'true' ))!!}
{{Form::token()}}

<div class="row">
<div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
<div class="form-group">
<label for="idmateria">Materia - Grupo - Nivel</label>
<select name="idSubgrupo" class="form-control" required>
<option value=""></option>
@foreach ($sg as $sg)
<option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
@endforeach
</select>
</div>
</div>



<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="fechaentrega">Fecha a entregar</label>
<input type="date" id="inputFecha" name="fechaentrega" required value="{{old('fechaentrega')}}" class="form-control">

</div>
</div>

<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
<div class="form-group">
<label for="anexo">Anexo</label>
<input type="file" name="anexo" value="{{old('anexo')}}" class="form-control">
</div>
</div>

<div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
<div class="form-group">
<label class="col-sm-2 control-label" for="titulo">Título:</label>
<input type="text" name="titulo" required value="{{old('titulo')}}" class="form-control" placeholder="título...">
</div>
</div>


<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<div class="form-group">
<label for="descripcion">Descripción</label>
<textarea name="descripcion" required value="{{old('descripcion')}}" class="form-control" rows="10" cols="150" placeholder="descripción..."></textarea>
</div>
</div>

<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
<div class="form-group">
<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar tarea</button>
<button class="btn btn-w-m btn-w-m btn-outline btn-danger btn-xs" type="reset">Cancelar</button>
</div></div></div>
</div></div></div>
</div></div></div>
</div>
{!!Form::close()!!}
          <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

    <!-- iCheck -->
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

    <!-- SUMMERNOTE -->
    <script src="{{ asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
    
    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{ asset('js/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js')}}"></script>
    <script >
        $("#selectNivel").change(function(){
            var idNivel = this.value;

            $('#selectGrados').find('option').not(':first').remove();

            $('#selectGrupo').find('option').not(':first').remove();

            $('#selectCiclo').find('option').not(':first').remove();

            $.ajax({
                method: 'get',
                url: "{{('../getGrados')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        //console.log(item.nombre);
                        $('#selectGrados').append($('<option>', {
                            value: item.grado,
                            text : item.grado
                        }));
                    });
                }
            });
            $.ajax({
                method: 'get',
                url: "{{('../getCiclo')}}",
                data: {idNivel:idNivel},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#selectCiclo').append($('<option>', {
                            value: item.idCiclo,
                            text : item.nombreCiclo
                        }));
                    });
                }
            });
        });
    </script>
    <script>
    Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });
            $(document).ready( function() {
                $('#inputFecha').val(new Date().toDateInputValue());
            })
    </script>
@endsection