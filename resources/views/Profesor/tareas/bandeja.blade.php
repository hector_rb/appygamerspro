@extends('Profesor.menuProfesor')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Tareas</h2>
<ol class="breadcrumb">
<li class="active">
<a href="/tareas/maestro"><strong>Tareas</strong></a>
</li>
<li>
<a href="maestro/create">Nuevas tareas</a>
</li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>

 <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Administración de tareas</h5>
                        <div class="ibox-tools">
                            
                        </div>
                    </div>
                    <div class="ibox-content">

<div class="row">
<div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
@include('Profesor.maestro.tareas.search')
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="ibox-content">
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTables-example" >
<thead>
<th>Id</th>
<th>Materia</th>
<th>Grupo</th>
<th>Fecha de entrega</th>
<th>Título</th>
<th>Descripcíon</th>
<th>Anexo</th>
<th></th>

</thead>
@foreach ($tareas as $t)
<tr>
<td>{{$t->idTareas}}</td>
<td>{{$t->materia}}</td>
<td>{{$t->idgrupo}}</td>
<td>{{$t->fechaentrega}}</td>
<td>{{$t->titulo}}</td>
<td>{{$t->descripcion}}</td>
<td><a href="{{asset('anexo/tareas/'.$t->anexo)}}">{{$t->anexo}}</a></td>
<td>
<a href="" data-target="#modal-delete-{{$t->idTareas}}" data-toggle="modal"><button class="btn btn-delete">Eliminar</button>
</td>
</a>
</td>
</tr>
@include('Profesor.maestro.tareas.modal')
@endforeach
</table>
</div>
{{$tareas->render()}}
</div>
</div>
</div>
      </div>
                </div>
            </div>
            </div>
        </div>
@endsection