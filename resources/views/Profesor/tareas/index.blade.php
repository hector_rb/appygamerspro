@extends('Profesor.menuProfesor')
@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>   
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Tareas
                </h1>
                <h3 class="font-bold c">tareas de la institución</h3>
            </center>
        </form>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/tareas/create')}}">Redactar tarea</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/tareas')}}"> <i class="fa fa-inbox "></i>Registro de tareas</a></li>
                            <li><a href="{{URL::to('profesor/tareas/create')}}"> <i class="fa fa-envelope-o"></i>Enviar tarea</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
    <div class="row">
        <div class="ibox-content">
            <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <center>
                                    <th></th>
                                    <th>Materia</th>
                                    <th>Nivel</th>
                                    <th>Grado, Grupo</th>
                                    <th>Fecha de entrega</th>
                                    <th>Titulo</th>
                                    <th>Descripción</th>
                                </center>
                            </thead>
                            <tbody>
                                @foreach ($tareas as $t)
                                <tr class="gradeX">
                                    <td align="right"><a href="{{URL::action('TareaController@show',['id'=>$t->idTareas])}}"
                                    title="Ver tarea"><button class="btn btn-white btn-sm fa fa-edit a"></button></a>
                                    <a href="" data-target="#modal-delete-{{$t->idTareas}}" data-toggle="modal" title="Eliminar"><button class="btn btn-white btn-sm fa fa-trash-o a"></button></a>
                                    </td>
                                    <td>{{$t->nombreMateria}}</td>
                                    <td>{{$t->nombreNivel}}
                                    <td>{{$t->grado}}{{$t->grupo}}</td>
                                    <td>{{$t->fechaentrega}}</td>
                                    <td>{{$t->titulo}}</td>
                                    <td>{{$t->descripcion}}</td>
                                </tr>
                                @include('Profesor.tareas.modal')
                                @endforeach                         
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
</div>


</body>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    language: {
                                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                                },
                    buttons: [

                        {
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]

                });

            });
</script>    
@endsection