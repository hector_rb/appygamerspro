@extends('Admin.menuAdmin')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Encuesta</h2>
<ol class="breadcrumb">
<li>
<a href="{{URL::to('admin/encuestas')}}"><strong>Consultar encuestas</strong></a>
</li>
<li class="active">
<a href="{{URL::to('admin/encuestas/create')}}">Nueva encuesta</a>
</li>
</ol>
</div>
<div class="col-lg-2">
</div>
</div>

@endsection