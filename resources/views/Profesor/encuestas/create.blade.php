@extends('Profesor.menuProfesor')
@section('content')
<!--<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Encuesta</h2>
<ol class="breadcrumb">
<li>
<a href="{{URL::to('admin/encuestas')}}">Consultar encuestas</a>
</li>
<li class="active">
<a href="{{URL::to('admin/encuestas/create')}}"><strong>Nueva encuesta</strong></a>
</li>

</ol>
</div>
<div class="col-lg-2">
</div>
</div>
-->
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.css')}}" rel="stylesheet">

    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css" />
    <style>
.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 15px;
    border-radius: 5px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}
.btn span.glyphicon {               
    opacity: 0;             
}
.btn.active span.glyphicon {                
    opacity: 1;             
}
</style>


    </head>
    <body>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/profesor/encuestas','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}} 
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInDown">
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Encuestas Pendientes</h5>   
                        </div>
                        <div class="ibox-content">
                            <div id='external-events'>
                                
                                <div class="form-group">
                                    <label class="control-label">Encuesta:</label>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="encuestas" id="encuestas" required="required">
                                        <option value=0>Seleccione</option>
                                    @foreach ($encuestas as $enc)
                                        <option value="{{$enc->idEncuesta}}">{{$enc->nombre}}</option>
                                    @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content" id="div_contenedor">
                        </div>
                    </div>
                    <div class="col-sm-2 control-label">
                              <button  type="submit" name="enviar" id="enviar" class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" style="display: none;">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
{!!Form::close()!!}     
        
        <!-- Mainly scripts -->
        <script src="{{ asset('js/plugins/fullcalendar/moment.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
        <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>


        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- Full Calendar -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/locale-all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
 

            <script type="text/javascript" src="{{asset('js/adminEscolar.js')}} "></script>
            <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
            <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
        
    <script>
        $("#encuestas").change(function(){
            var idenc = this.value;
            
            var br = $('<br>',{
                            });
            
            var div= $('#div_contenedor');
            div.empty();
            if(idenc==0)
            {
                var butt=document.getElementById("enviar");
                butt.style.display="none";
            }
            else
            {
                $.ajax({
                    method: 'get',
                    url: "{{('../getPreguntas')}}",
                    data: {idenc:idenc},
                    success: function (data) {
                        var butt=document.getElementById("enviar");
                        butt.style.display="block";
                        $.each(data, function (i, item) {
                            var l=$('<label>', {
                                class:'form-control',
                                text: item.nombrePregunta
                            });
                            var div_con=$('<div>',{
    
                                id:'pregunta'+(i+1),
                                name:'pregunta'+(i+1)
    
                            });
                            div_con.attr('data-toggle', 'buttons');
                            var con=1;
                            div_con.append(l);
                            switch(item.tipoRespuesta)
                                {
                                    case 1:
                                    
                                        var lo1=$('<label>', {
                                                text: 'Si      ',
                                                required:'required',
                                            });
                                        var lo2=$('<label>', {
                                                text: 'No      ',
                                                required:'required',
                                            });
                                        var o1=$('<input>', {
                                                name: 'respuesta'+i,
                                                id: 'respuesta'+i,
                                                required:'required',
                                                value: 1,
                                                type:'radio'
                                            });
                                        var o2=$('<input>', {
                                                name: 'respuesta'+i,
                                                id: 'respuesta'+i,
                                                required:'required',
                                                value: 2,
                                                type:'radio'
                                            });
                                        var label=$('<label>', {
                                                class:'btn btn-info',
                                        });
                                        var label2=$('<label>', {
                                                class:'btn btn-info',
                                        });
                                        var span=$('<span>', {
                                            class:'glyphicon glyphicon-ok',
                                        });
                                        var span2=$('<span>', {
                                            class:'glyphicon glyphicon-ok',
                                        });
                                        
                                        label.append(o2);
                                        label.append(span);
                                        label2.append(o1);
                                        label2.append(span2);
                                        div_con.append(label2);
                                        div_con.append(lo1);
                                        div_con.append($('<br>'));
                                        div_con.append(label);
                                        div_con.append(lo2);
    
                                        break;
                                    case 2:
                                    
                                        var op= item.opciones.split('¨');
                                        $.each(op,function(index,valor){
                                            if(valor != '')
                                            {
                                                var label=$('<label>', {
                                                        text: '  '+valor
                                                    });
                                                var mul=$('<input>', {
                                                        name: 'respuesta'+i,
                                                        id: 'respuesta'+i,
                                                        value: index+1,
                                                        type:'radio',
                                                        required:'required',
                                                    });
                                                var label2=$('<label>', {
                                                class:'btn btn-info',
                                                });
                                                var span=$('<span>', {
                                                class:'glyphicon glyphicon-ok',
                                                });
    
                                                label2.append(mul);
                                                label2.append(span);
                                                div_con.append(label2);
                                                div_con.append(label);
                                                div_con.append($('<br>'));
                                                
    
    
                                            }
                                        });
                                        break;
                                        break;
                                    case 3:
                                    
                                        var label=$('<label>', {
                                                text: 'Rango 1-10 '
                                            });
                                        var label2=$('<label>', {
                                                text: 'Valor',
                                                id: 'valor'+i,
                                                name: 'valor'+i
                                            });
                                        var ran=$('<input>', {
                                                name: 'respuesta'+i,
                                                id: 'respuesta'+i,
                                                min:1,
                                                max:10,
                                                value:1,
                                                type:'range',
                                                class:'slider',
                                                on: {
                                                        change: function()
                                                        {
                                                            label2.text('Valor '+ran.val());
                                                        }
                                                    }
    
                                            });
                                        ran.prop('required',true);
                                        div_con.append(label);
                                        div_con.append(ran);
                                        div_con.append($('<br>'));
                                        div_con.append(label2);
                                        break;
                                }
                                
                                div_con.append($('<br>'));
                                div_con.append($('<br>'));
                                div.append(div_con);
                            });
                            
                        },
                        error:function(e){
                            console.log(e.responseJSON);
                        }
                    });
            
            }    
            
            
       });
    </script>
    </body>

@endsection