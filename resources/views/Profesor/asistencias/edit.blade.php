@extends('Profesor.menuProfesor')
@section('content')
<head>
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/green.css')}}" />
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/yellow.css')}}" />
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/red.css')}}" />
</head>
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Editar Asistencias</h2>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5>Administración de asistencia</h5>
<div class="ibox-tools">
</div>
</div>
<div class="ibox-content">



<div class="col-lg-4 col-md-2 col-md-2 col-xs-12">
<div class="form-group" id="editar">

</select>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Registros de asistencias.</p>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="table-responsive">
	{!!Form::model($asistencia,['method'=>'PATCH','route'=>['asistencias.update',$id, 'idSubgrupo'=>$sg, 'periodo'=> $periodo]])!!}
<table class="table table-hover table-mail">
<tbody>
<tr class="unread">
<thead >
<th width="50" height="50">id</th>
<th>Alumno</th>
<th>Asistencia-Retardo-Falta</th>
</thead>
<tbody id="rows">
	@foreach($asistencia as $a)
	<tr>
		<td>

			{{$a->idAlumno}}
		</td>
		<td>
			{{$a->nombreAlumno}} {{$a->apepat}} {{$a->apemat}}
		</td>
		<td>
			<div class="col-md-3"></div>
			<div class="col-md-2">
				<input type="radio" class="green" name="asistencia[{{$a->idAlumno}}]"  <?php if($a->asistencia == "A") echo "checked"; ?> value="A">
				<label>A</label>
			</div>
			<div class="col-md-2">
				<input type="radio" class="yellow" name="asistencia[{{$a->idAlumno}}]"  <?php if($a->asistencia == "R") echo "checked"; ?> value="R">	
				<label>R</label>
			</div>
			<div class="col-md-2">
				<input type="radio" class="red" name="asistencia[{{$a->idAlumno}}]"  <?php if($a->asistencia == "F") echo "checked"; ?> value="F">				
				<label>F</label>
			</div>
			<div class="col-md-3"></div>
		</td>
	</tr>
	@endforeach
	<tr>
		<td></td>
		<td></td>
		<td align="center"><button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Guardar</button>
		
	</tr>
</tbody>
</tr>
</tbody>
</table>
{!!Form::close()!!}
</div>
</div>
{!!Form::close()!!}
</div></div></div>
</div></div></div>
</div></div></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js"></script>
<script >
	$(document).ready(function(){
		  $('.green').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      checkboxClass: 'icheckbox_line-green',
		      radioClass: 'iradio_line-green',

		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
		  });
		  $('.yellow').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      checkboxClass: 'icheckbox_line-yellow',
		      radioClass: 'iradio_line-yellow',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
		  });
		  $('.red').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      radioClass: 'iradio_line-red',
		      style:'background:#d0011b',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
		  });
		});
</script>

@endsection
