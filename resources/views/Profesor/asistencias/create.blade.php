@extends('Profesor.menuProfesor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/green.css')}}" />
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/yellow.css')}}" />
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/red.css')}}" />
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

	<link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

        .b{
            color: #EB984E;
        }
        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>
    <form>
        <center>
            <h1 class="font-bold b">Asistencia</h1>
            <h3 class="font-bold c">Dar de alta nuevas asistencias</h3>
        </center>
    </form>
		<div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/asistencias/create')}}">Nueva asistencia</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/asistencias')}}"> <i class="fa fa-inbox "></i>Registro de Asistencias</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
<div class="container">
	<div class="col-lg-9">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
		<div class="col-lg-12">
		<div class="ibox float-e-margins">
		<div class="ibox-title">
		<h5>Administración de asistencias</h5>
		<div class="ibox-content">
					<h4>Control de asistencia</h4>
			<div class="row">
			<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
				<p>Para registrar la asistencia solo debe seleccionar grupo en cuestión así como la fecha en que desee registrar dicha asistencia.</p>
				@if(count ($errors)>0)
				<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
				</ul>
				</div>
			</div>
			@endif
			{!!Form::open(array('url'=>'/profesor/asistencias','method'=>'POST','autocomplete'=>'off'))!!}
			{{Form::token()}}
		<br></br>
				<div class="col-lg-6 col-md-8 col-md-8 col-xs-12">
					<div class="form-group">
						<label for="idmateria">Materia - Grupo - Nivel</label>
							<select name="idSubgrupo" class="form-control" id="selectSubgrupo">
								<option value=""></option>
								@foreach ($sg as $sg)
								<option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
								@endforeach
							</select>
					</div>
				</div>

				<div class="col-lg-2 col-md-2 col-md-2 col-xs-12">
					<div class="form-group">
						<label for="numevaluaciones">Periodo</label>
						<select type="text" name="periodo" id="selectPeriodos" required="true"  class="form-control">
							<option></option>
						</select>
					</div>
				</div>


				<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
					<div class="form-group">
						<label for="fecha">Fecha</label>
						<input type="date" name="fecha" id="inputFecha" required value="{{old('fecha')}}" class="form-control" placeholder="fecha">
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
					<div class="form-group">
						<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit" style="display: none;" id="enviar1">Enviar</button>
					</div>
				</div>

				<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-condensed table-hover" id="tabla">
						<thead>
						<th>Matrícula</th>
						<th>Grupo</th>
						<th>Nombre</th>
						<th><p align="center">Asistencia</p>  
						<p align="center"> Asistencia - Retardo - Falta</p></th>
						</thead>
						<tbody id="rows">

						</tbody>

						</table>

					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
					<div class="form-group">
						<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit" style="display: none;" id="enviar2">Enviar</button>
					</div>
				</div>	
		</div>
	</div>
	</div>
</div>
{!!Form::close()!!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js"></script>
<script >
	
        	$("#selectSubgrupo").change(function(){
            		var idsg = this.value;
            		$('#selectPeriodos').find('option').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('../getPeriodosProfesor')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    
		                    $.each(data, function (i, item) {
		                        
		                        //console.log(item.nombre);
		                        var op =$('<option>',{
		                        	value: item.idPeriodo,
		                        	text: item.nombrePeriodo
		                        });
		                        
		                        
		                        $('#selectPeriodos').append(op);
		                   
		                		});
		            		}
		        		});

		            $('#rows').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('../getAlumnoAsis')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                	 var i=0;
		                	 var butt=document.getElementById("enviar1");
                                butt.style.display="none";
                            var butt2=document.getElementById("enviar2");
                                butt2.style.display="none";
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                       if(i>0)
		                        {
		                            var butt=document.getElementById("enviar1");
                                        butt.style.display="block";
                                    var butt2=document.getElementById("enviar2");
                                        butt2.style.display="block";
		                        }
		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.matricula,
		                        	
		                        });

		                        var idalum=$('<input>',{
		                        	value:item.idAlumno,
		                        	name:'id[]',
		                        	type:'hidden'
		                        })

		                        var grupo =$('<td>',{
		                        	text: item.grado + item.grupo
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.apepat + ' ' + item.apemat + ' ' + item.nombreAlumno
 		                        });
 		                        var col1=$('<div>',{
 		                        	class:'col-md-3'
 		                        });
 		                        var colA=$('<div>',{
 		                        	class:'col-md-2'
 		                        });
 		                        var colR=$('<div>',{
 		                        	class:'col-md-2'
 		                        });
 		                        var colF=$('<div>',{
 		                        	class:'col-md-2'
 		                        });
 		                        var col2=$('<div>',{
 		                        	class:'col-md-3'
 		                        });
 		                        var a=$('<input>',{
 		                        	type:'radio',
 		                        	id:'green',
 		                        	required:'required',
 		                        	name:'asis['+i+']',
 		                        	value:'A'

 		                        });

 		                        var r=$('<input>',{
 		                        	type:'radio',
 		                        	required:'required',
 		                        	id:'yellow',
 		                        	name:'asis['+i+']',
 		                        	value:'R'

 		                        });
 		                        var f=$('<input>',{
 		                        	type:'radio',
 		                        	id:'red',
 		                        	required:'required',
 		                        	name:'asis['+i+']',
 		                        	value:'F'

 		                        });
 		                        var cent =$('<center>');
		                        var asis =$('<td>');
		                        id.append(idalum);
		                        colA.append(a);
		                        colR.append(r);
		                        colF.append(f);
		                        asis.append(col1);
		                        asis.append(colA);
		                        asis.append(colR);
		                        asis.append(colF);
		                        asis.append(col2);

		                        fila.append(id);
		                        fila.append(grupo);
		                        fila.append(nombre);
		                        fila.append(asis);
		                        
		                        $('#rows').append(fila);
		                        a.iCheck({
							      checkboxClass: 'icheckbox_line-green',
							      radioClass: 'iradio_line-green',
							      insert: '<div class="icheck_line-icon"></div>' + "A"
							    });
							    r.iCheck({
							      checkboxClass: 'icheckbox_line-yellow',
							      radioClass: 'iradio_line-yellow',
							      insert: '<div class="icheck_line-icon"></div>' + "R"
							    });
							    f.iCheck({
							      checkboxClass: 'icheckbox_line-red',
							      radioClass: 'iradio_line-red',
							      insert: '<div class="icheck_line-icon"></div>' + "F"
							    });

		                   		i++;
		                		});
		            		}
		        		});
		        });
        </script>
         <script>
    Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });
            $(document).ready( function() {
                $('#inputFecha').val(new Date().toDateInputValue());
            })
    </script>
</body>
@endsection