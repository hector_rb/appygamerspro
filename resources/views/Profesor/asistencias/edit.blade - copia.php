@extends('Profesor.menuProfesor')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Editar Asistencias</h2>

</div>
<div class="col-lg-2">
</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5>Administración de asistencia</h5>
<div class="ibox-tools">
</div>
</div>
<div class="ibox-content">



<div class="col-lg-4 col-md-2 col-md-2 col-xs-12">
<div class="form-group" id="editar">
</select>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Registros de asistencias.</p>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="table-responsive">
<div class="table-responsive">
	{!!Form::model($asistencia, ['method'=>'PATCH','route'=>['asistencia.update',$id, 'idSubgrupo'=>$sg, 'periodo'=> $periodo]])!!}
<table class="table table-hover table-mail">
<tbody>
<tr class="unread">
<thead >
<th width="50" height="50">id</th>
<th>Alumno</th>
<th>Asistencia-Falta-Retardo</th>
</thead>
<tbody id="rows">
	@foreach($asistencia as $a)
	<tr>
		<td>

			{{$a->idAlumno}}
		</td>
		<td>
			{{$a->nombreAlumno}} {{$a->apepat}} {{$a->apemat}}
		</td>
		<td>
		<input type="radio" name="asistencia[{{$a->idAlumno}}]" <?php if($a->asistencia == "A") echo "checked"; ?> value="A">
		<input type="radio" name="asistencia[{{$a->idAlumno}}]" <?php if($a->asistencia == "F") echo "checked"; ?> value="F">	
		<input type="radio" name="asistencia[{{$a->idAlumno}}]" <?php if($a->asistencia == "R") echo "checked"; ?> value="R">				
		</td>
	</tr>
	@endforeach
	<tr>
		<td></td>
		<td></td>
		<td align="center"><button type="submit" class="btn btn-w-m btn-w-m btn-outline btn-warning btn-xs">Guardar</button>
		
	</tr>
</tbody>
</tr>
</tbody>
</table>
{!!Form::close()!!}
</div>
</div>
{!!Form::close()!!}
</div></div></div>
</div></div></div>
</div></div></div>
@endsection