@extends('Profesor.menuProfesor')
@section('content')
<style >
    img
    {
        max-width: 100%;
    }
</style>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Perfil de {{ Auth::user()->name }}</h2>
                    
                </div>
                <div class="col-lg-2">
                <meta type="hidden" name="csrf-token" content="{{csrf_token()}}">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight" >
            
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins" id="ibox1">
                    <div class="ibox-title  back-change">
                        <h5>Foto de perfil</h5>
                        
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image-crop">
                                    <img id="image" src="{{asset('img/profile_pics/'.$img)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>Imagen previa</h4>
                                <div class="img-preview img-preview-sm"></div>
                                <h4>cambiar imagen</h4>
                                <p>
                                    Si quieres cambiar tu fotografia da click en "Subir"
                                </p>
                                
                                <div class="btn-group">
                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                        <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                        Subir
                                    </label>

                                    <label  class="btn btn-success">
                                        <input id="saveImage" class="hide">
                                        Guardar Imagen
                                    </label>
                                </div>
                               
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins" id="ibox1">
                    <div class="ibox-title  back-change">
                        <h5>Información personal</h5>
                        
                    </div>
                    <div class="ibox-content">                        
                        {!!Form::open(['method'=>'PATCH','route'=>['perfilProf.update',$info->idprofesor], 'class'=>'form-horizontal'])!!}
                            <div class="form-group">
                                
                                <label class="col-sm-2 control-label">NOMBRE</label>

                                <div class="col-sm-4"><input type="text" name="nombre" class="form-control" required="required" value="{{$info->nombreProf}}"></div>

                                <label class="col-sm-2 control-label">APELLIDO PATERNO</label>

                                <div class="col-sm-4"><input type="text" name="apepat" class="form-control" required="required" value="{{$info->apepat}}"></div>
                            </div>
                            <div class="form-group">
                                
                                
                                <label class="col-sm-2 control-label">APELLIDO MATERNO</label>

                                <div class="col-sm-4"><input type="text" name="apemat" class="form-control" required="required" value="{{$info->apemat}}"></div>

                                <label class="col-sm-2 control-label">MATRÍCULA</label>

                                <div class="col-sm-4"><input type="text" name="matricula" class="form-control" required="required" value="{{$info->matricula}}"></div>
                            </div>
                            <div class="form-group">
                                
                                <label class="col-sm-2 control-label">GENERO</label>

                                <div class="col-sm-4">
                                    <select type="text" name="genero" class="form-control" required="required">
                                        <option value="{{$info->genero}}">{{$info->genero}}</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label">FECHA DE NACIMIENTO</label>

                                <div class="col-sm-4"><input type="date" name="fechanac" class="form-control" required="required" value="{{$info->fechanac}}"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" >TELEFONO</label>

                                <div class="col-sm-4"><input type="text" name="tel" class="form-control" required="required" value="{{$info->telefono}}"></div>
                                <label class="col-sm-2 control-label" >CELULAR</label>

                                <div class="col-sm-4"><input type="text" name="cel" class="form-control" required="required" value="{{$info->celular}}"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9"></div>
                                <div class="col-sm-3 control-label">
                                <button type="submit" name="guardar" class="btn btn-success ">Guardar</button></div>
                            </div> 
                            
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>


        </div>
        <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{asset('js/inspinia.js')}}"></script>
        <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>
        <script>
            $(document).ready(function()
            {
                var $image = $(".image-crop > img")
                var cropper;
                $image.cropper({
                    aspectRatio: 1 / 1,
                    preview: ".img-preview",
                    
                    crop: function(e) {
                        // Output the result data for cropping image.
                        
                        console.log(e.x);
                        console.log(e.y);
                        console.log(e.width);
                        console.log(e.height);
                        console.log(e.rotate);
                        console.log(e.scaleX);
                        console.log(e.scaleY);
                    },            
                });

                var $inputImage = $("#inputImage");
                if (window.FileReader) {
                    $inputImage.change(function() {
                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        if (!files.length) {
                            return;
                        }

                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {
                                $inputImage.val();
                                $image.cropper("reset", true).cropper("replace", this.result);
                            };
                        } else {
                            showMessage("Please choose an image file.");
                        }
                    });
                } else {
                    $inputImage.addClass("hide");
                }

                $('#saveImage').click(function(){

                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');

                        $image.cropper('getCroppedCanvas').toBlob(function (blob) {
                        

                        var formData = new FormData();
                        var token = $('meta[name="csrf-token"]').attr('content');
                        formData.append('croppedImage',$image.cropper('getCroppedCanvas',{
                          width: 256,
                          height: 256,
                          
                        }).toDataURL("image/png"));
                        formData.append("_token", token);
                        
                        // Use `jQuery.ajax` method
                        $.ajax( {
                        url:"{{('saveImageProf')}}",
                        type:'post',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('#profileImage').attr("src","{{('../img/profile_pics/')}}"+data);
                            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        },
                        error: function (e) {
                        console.log(e.responseJSON);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        }
                        });
                    });
                });
            });
        </script>
@endsection
