<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    

    <link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}">

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
 
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet')}}">

    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

    <link href="{{ asset('css/plugins/chartist/chartist.min.css')}}" rel="stylesheet">
    <!--cropper-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.min.css" />
    <style type="text/css">
        
    </style>

</head>

<body>
    @section('menu')
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element" style="text-align: center;"> <span>
                            <img alt="image" class="img-circle" id="profileImage" style="width: 100px; height: 100px;" src="{{asset('img/profile_pics/'.Auth::user()->imagen)}}" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">Profesor<b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{URL::to('profesor/perfilProf')}}">Perfil</a></li>
                                
                                <li class="divider"></li>
                                <!--<li><a href="login.html">Logout</a></li>-->
                            </ul>
                        </div>
                        <div class="logo-element">
                            MAC
                        </div>
                    </li>
                    <li>
                        <a href="{{URL::to('profesor/graficas')}}"><i class="fa fa-align-justify"></i> <span class="nav-label">Inicio</span></a>
                    </li> 
                    <li>
                        <a href="{{URL::to('profesor/horarioP')}}"><i class="fa fa-tasks"></i> <span class="nav-label">Horario</span></a>
                    </li>  
                      
                    <li>
                        <a href={{URL::to('/profesor/tareas')}}><i class="fa fa-edit"></i> <span class="nav-label">Tareas</span></a>
                    </li>                                                                       
                    <li>
                        <a href={{URL::to('/profesor/asistencias/create')}}><i class="fa fa-check-square"></i> <span class="nav-label">Asistencia</span></a>
                    </li>
                    <li>
                        <a href={{URL::to('/profesor/calificacion/create')}}><i class="fa fa-book"></i> <span class="nav-label">Calificaciones</span></a>
                    </li>
                    <li>
                        <a href={{URL::to('/profesor/insignias/create')}}><i class="fa fa-star"></i> <span class="nav-label">Insignias</span></a> 
                    </li>
                    <li>
                        <a href={{URL::to('/profesor/encuestas/create')}}><i class="fa fa-book"></i> <span class="nav-label">Encuestas</span></a>
                    </li> 
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!--<form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>-->
        </div>
        <div class="navbar-header">
            <span class="navbar-minimalize minimalize-styl-2" style="margin: 7px 5px 5px 20px;"><img src="{{asset('img/logo-header.png')}}" data-rjs="3" alt="Bold Logo" ></span>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" id="men"  href="{{URL::to('profesor/mensajesR')}}" data-toggle="tooltip" data-placement="top" title="Mensajes">
                        <i class="fa fa-envelope fa-2x"></i>  <span class="label label-warning"></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info"  id="avi"  href="{{URL::to('profesor/avisosProf')}}" data-toggle="tooltip" data-placement="top" title="Avisos">
                        <i class="fa fa-bell fa-2x"></i>  <span class="label label-warning"></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" id="eve"  href="{{URL::to('profesor/eventos')}}" data-toggle="tooltip" data-placement="top" title="Eventos">
                        <i class="fa fa-calendar fa-2x"></i>  <span class="label label-warning"></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href={{URL::to('/profesor/chats/create')}} data-toggle="tooltip" data-placement="top" title="Chat">
                        <i class="fa fa-comments fa-2x"></i>
                    </a>
                    
                </li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Salir
                    </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                </li>
                <!--<li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>-->
            </ul>

        </nav>
        </div>

         <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    
   
 

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- Flot -->
    <script src="{{ asset('js/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.categories.js')}}"></script>    
    

    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{ asset('js/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js')}}"></script>
    <script src="{{ asset('js/plugins/chartist/chartist.min.js')}}"></script>

    
    <script src="{{ asset('js/plugins/flot/jquery.flot.symbol.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.time.js')}}"></script>

    <!-- Jvectormap -->
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- EayPIE -->
    <script src="{{ asset('js/plugins/easypiechart/jquery.easypiechart.js')}}"></script>

    <!--cropper-->
    <script src="{{asset('js/canvas-to-blob.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $.ajax({
                method: 'get',
                url: "{{('MenuProfesor')}}",
                success: function (data) {
                    if(data.sinver>0)
                    {
                        var span= $('<span>',
                            {
                                class:'label label-warning pull-right',
                                text:data.sinver,
                            });
                        $('#men').append(span);
                    }
                    if(data.avisos>0)
                    {
                        var span1= $('<span>',
                            {
                                class:'label label-warning pull-right',
                                text:data.avisos,
                            });
                        $('#avi').append(span1);
                    }
                    if(data.eventos>0)
                    {
                        
                        var span2= $('<span>',
                            {
                                class:'label label-warning pull-right',
                                text:data.eventos,
                            });
                        $('#eve').append(span2);
                    }
                }
             });
        });        
    </script>

    @yield('content')
</body>