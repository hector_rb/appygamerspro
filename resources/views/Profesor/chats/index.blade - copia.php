@extends('Profesor.menuProfesor')
@section('content')
<html>
   <head>
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">
      
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }
    .swal-title {
      margin: 0px;
      font-size: 10px;
      box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
      margin-bottom: 28px;
    }
    </style>
    <body>
            <h1>Crear nuevo mensaje</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/mensajesR/create')}}">Redactar Correo</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/mensajesR')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('profesor/mensajesR/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <!--<li><a href="{{URL::to('/admin/mensajesR/papelera')}}"> <i class="fa fa-trash-o"></i>Papelera</a></li>-->
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/profesor/mensajesR','method'=>'POST','autocomplete'=>'off', 'files'=>'true' ))!!}
{{Form::token()}}         
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    Redactar correo
                </h2>
            </div>
            <div class="mail-box">
                <div class="mail-body">

                        <form class="form-horizontal" method="get">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Para:</label>

                                <div class="col-sm-10">
                            </div>
                            <br></br>
                            <div class="form-group"><label class="col-sm-2 control-label">Asunto:</label>

                                <div class="col-sm-10"><input type="text" class="form-control" name="Asunto" value="" required="required" placeholder="Asunto..."></div>
                            </div>
                            <br></br>
                            <br></br>
                            <textarea class="form-control" name="Mensaje" placeholder="mensaje..."></textarea>
                            <br>
                            <input type="file" class="form-control" name="anexo" value="">
                            <br>
                            <div class="col-sm-10"></div>
                            <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>

                        </form>
                </div>
            </div>
            
        </div>
                <!--<div class="col-lg-3"></div>
                <div class="col-lg-9">
                    <div class="ibox">
                        <div class="ibox-content">
                            <form action="#" class="dropzone" id="dropzoneForm">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->
        </div>
{!!Form::close()!!} 
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.chosen-select').chosen();   
    });
</script> 
 
</body>
@endsection 