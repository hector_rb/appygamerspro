@extends('Profesor.menuProfesor')
@section('content')
<head>
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        .b{
            background-color: #F2F2F2;
            color: #819FF7;
        }
        .c{
            background-color: #F2F2F2;
            color: #81DAF5;
        }
        .d{
            background-color: #81BEF7;
            color: #FFFFFF;
        }
        .e{
            background-color: #0489B1;
            color: #FFFFFF;
        }
        .f{
            background-color: #A9A9F5;
            color: #FFFFFF;
        }
        .g{
            background-color: #F2F2F2;
            color: #04B486;
        }
        .h{
            background-color: #F2F2F2;
            color: #04B4AE;
        }
        
        .sobre{
            width: 500%;
            height: 500%;
            margin: 8px 0;
            border: none;
            color: black;
        }


    </style>
</head>
<body>

    <form>
        <center>
            <h1 class="font-bold h">Información general</h1>
                <h3 class="font-bold h">En este apartado podrás obtener un resumen del control de estadísticas de la institución</h3>
        </center>
    </form>

    <div class="container">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="col-lg-11">
                <div class="tooltip-demo">
                    <form>
                        <center>
                            <h2>Comunicaciones
                                <small>
                                        <i  class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" aria-describedby="popover955887"></i> 
                                </small>
                            </h2>
                        </center>
                    </form>
                </div>
                    

            <div class="row">
                    <div class="col-lg-4">
                        <a href="{{URL::to('profesor/mensajesR')}}">
                            <div class="jumbotron a">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-envelope-o fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$sinver}}
                                            </span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Mensajes</h2>
                                        </center>
                                    </form>
                            </div>

                        </a>
                    </div>

                    

                    <div class="col-lg-4">
                        <a href="{{URL::to('profesor/avisosProf')}}">
                            <div class="jumbotron c">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-bell fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$avisos}}</span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Avisos</h2>
                                        </center>
                                    </form>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="{{URL::to('profesor/eventos')}}">
                            <div class="jumbotron b">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-calendar fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$eventos}}</span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Eventos</h2>
                                        </center>
                                    </form>
                            </div>
                        </a>
                    </div>
     </div>
        <div class="tooltip-demo">
                <form>
                    <center>
                        <h2>Materia-Grupo-Nivel
                            <small>
                                <i  class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" aria-describedby="popover955887"></i> 
                            </small>
                        </h2>
                    </center>
                </form> 
            </div>
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <select name="idSubgrupo" id="selectSubgrupo" class="form-control">
                                    @foreach ($sg as $sg)
                                        <option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
                                    @endforeach
                            </select>
                            <br></br>
                        </div>   
                    <div id="col-lg-9" class="col-lg-9">
                       
                            <div class="flot-chart" id="graficasCalif">
                                <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                            </div>
                            <br>
                    </div>
                    <div class="col-lg-3">
                            <div class="jumbotron g">
                                    <div class="row">
                                            <form>
                                                <center>
                                                    <i class="fa fa-book fa-2x"></i>
                                                    <h4>Número de calificaciones totales del ciclo</h4>
                                                    <h2 class="font-bold" id="totalcalif"></h2>
                                                </center>
                                            </form>
                                    </div>
                            </div>
                            <div class="jumbotron g">
                                    <div class="row">
                                            <form>
                                                <center>
                                                    <i class="fa fa-trophy fa-3x"></i>
                                                    <h3>Número de alumnos aprobados totales del ciclo</h3>
                                                    <h1 class="font-bold" id="aprobados"></h1>
                                                </center>
                                            </form>
                                    </div>  
                            </div>
                    </div>

        <!--//////////////////////////////////////////////-->

            <div class="row">
                    <div class="col-lg-4">
                            <div class="jumbotron d">
                                    <div class="row">
                                        <form>
                                            <center>
                                                <h3>Promedio del grupo:</h3>
                                                    <h1 class="font-bold" id="rows"></h1>
                                                    <br>
                                            </center>
                                        </form>    
                                    </div>
                            </div>
                    </div>

                    <div class="col-lg-4">
                            <div class="jumbotron e">
                                <h3>Alumnos Sobresalientes:</h3>
                                    <div class="row">
                                        <div class="table-responsive">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-mail">
                                                    <tbody id="rows1">
                                                        <tr class="unread">
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="jumbotron f">
                                <h3>Alumnos de bajo rendimiento:</h3>
                                    <div class="row">
                                        <div class="table-responsive">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-mail">
                                                    <tbody id="rows2">
                                                        <tr class="unread">
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    </div>  
                
            <script>
            $(document).ready(function(){
                document.getElementById("selectSubgrupo").selectedIndex=0;
                console.log(document.getElementById("selectSubgrupo").value);
                cargar(document.getElementById("selectSubgrupo").value);
            });
            function cargar(idsg){
                $('#rows').find('tr').remove();
                                $.ajax({
                                    method: 'get',
                                    url: "{{('getPromedioGrupoProf')}}",
                                    data: {idsg:idsg},
                                    dataType: 'json',
                                    success: function (data) {
                                            var promedio=0;
                                            var contador=0; 
                                            var grado;
                                        $.each(data, function (i, item) {
                                            //console.log(item.nombre);
                                            promedio=promedio+item.promedio;
                                            contador++;
                                            grado=item.grado;
                                            grupo=item.grupo;
                                            //aqui se calcula
                                            });
                                            promedio=promedio/contador;
                                            var fila = $('<h1>');
                                            
                                            var resultado = Math.round(promedio*Math.pow(10,2))/Math          .pow(10,2     );

                                            /*var Grupo =$('<td>',{
                                                text: grado + ' ' +grupo
                                            });*/
                                            console.log(contador);
                                            if (contador!=0){
                                            var calif =$('<h1>',
                                                {
                                                    text:resultado,
                                                    class:'font-bold',
                                                });
                                                fila.append(calif);
                                            }
                                            //fila.append(Grupo);
                                            
                                            $('#rows').empty();
                                            $('#rows').append(fila);
                                            
                                        }
                                    });

                                $('#rows1').find('tr').remove();
                                $.ajax({
                                    method: 'get',
                                    url: "{{('getPromedioAlumnoSobresalienteProf')}}",
                                    data: {idsg:idsg},
                                    dataType: 'json',
                                    success: function (data) {
                                        $.each(data, function (i, item) {
                                            
                                                var fila = $('<tr>');

                                                var resultado = Math.round(item         .promedio*Math.pow(10,2))/Math          .pow(10,2);

                                                /*var id =$('<td>',{
                                                    text: item.idAlumno,
                                                    
                                                });*/
                                                console.log("{{('img/profile_pics/')}}"+item.imagen);
                                                var id =$('<img>',{
                                                    src:"{{asset('img/profile_pics/')}}"+'/'+item.imagen,
                                                    height:'40', 
                                                    width:'40', 
                                                    class:'img-circle m-b-md', 
                                                    alt:'profile'
                                                    
                                                });
                                                var nombre =$('<td>',{
                                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                                });                                 
                                                var calif =$('<td>',
                                                    {
                                                        text:resultado
                                                    });
                                                
                                                fila.append(id);
                                                fila.append(nombre);
                                                fila.append(calif);
                                                $('#rows1').append(fila);
                                            
                                            });
                                        }
                                    });
                                $('#rows2').find('tr').remove();
                                console.log('#rows2');
                                $.ajax({
                                    method: 'get',
                                    url: "{{('getPromedioAlumnoRegularProf')}}",
                                    data: {idsg:idsg},
                                    dataType: 'json',
                                    success: function (data) {
                                        $.each(data, function (i, item) {
                                        
                                                var fila = $('<tr>');

                                                var resultado = Math.round(item.promedio*Math.pow(10,2))/Math.pow(10,2);

                                                var id =$('<img>',{
                                                    src:"{{asset('img/profile_pics/')}}"+'/'+item.imagen,
                                                    height:'40', 
                                                    width:'40', 
                                                    class:'img-circle m-b-md', 
                                                    alt:'profile'
                                                    
                                                });
                                                var nombre =$('<td>',{
                                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                                });                                 
                                                var calif =$('<td>',
                                                    {
                                                        text:resultado
                                                    });
                                                
                                                fila.append(id);
                                                fila.append(nombre);
                                                fila.append(calif);
                                                $('#rows2').append(fila);
                                            
                                            });
                                        }
                                    }); 

                                $('#totalcalif').find('p').remove();
                                $.ajax({
                                    method: 'get',
                                    url: "{{('getCalifCount')}}",
                                    data: {idSubgrupo:idsg},
                                    dataType: 'json',
                                    success: function (data) {
                                            var contador = 0;
                                            var suma= 0
                                        console.log("data" + data)
                                        $.each(data, function (i, item) {

                                            suma=item.calif;
                                            contador++;
                                            });
                                            suma=contador
                                            var fila = $('<h2>');                                
                                            
                                            var calif=$('<h2>',{
                                                text:suma,
                                                class:'font-bold'
                                            });
                                            
                                            fila.append(calif);
                                             
                                             $('#totalcalif').empty();

                                             $('#totalcalif').append(fila);
                                       
                                            
                                        }
                                    });

                                $('#aprobados').find('p').remove();
                                $.ajax({
                                    method: 'get',
                                    url: "{{('getCalifAprobCount')}}",
                                    data: {idSubgrupo:idsg},
                                    dataType: 'json',
                                    success: function (data) {
                                            var contador = 0;
                                            var suma= 0
                                        console.log("data" + data)
                                        $.each(data, function (i, item) {

                                            suma=item.calif;
                                            contador++;
                                            });
                                            suma=contador
                                            var fila = $('<h2>');                                
                                            
                                            var calif=$('<h2>',{
                                                text:suma,
                                                class:'font-bold'
                                            });
                                            
                                            fila.append(calif);

                                            $('#aprobados').empty();

                                             $('#aprobados').append(fila);
                                       
                                            
                                        }
                                    });

                                $('#graficasCalif').find('p').remove();
                                $.ajax({
                                    method: 'get',
                                    url: "{{('graficasCalifProf')}}",
                                    data: {idsg:idsg},
                                    dataType: 'json',
                                    success: function (data) {
                                            
                                            $("#col-lg-9").empty();
                                            var densityCanvas =$('<canvas>',{
                                                id: "densityChart",
                                                width: 600,
                                                height:400
                                                });  
                                            $("#col-lg-9").append(densityCanvas);
                                            Chart.defaults.global.defaultFontFamily = "Lato";
                                            Chart.defaults.global.defaultFontSize = 18;
                                            var densityData = {
                                              label: 'Control de calificaciones del ciclo',
                                              backgroundColor:"#58FAAC",
                                              data: data.barra
                                            };
                                            var barChart = new Chart(densityCanvas, {
                                              type: 'bar',
                                              data: {
                                                labels: data.califs,
                                                datasets: [densityData]
                                              }
                                            });
                                            barChart.draw();

                                        }
                                    });
            }
            $("#selectSubgrupo").change(function(){
                                var idsg = this.value;
                                cargar(idsg);                  
                 
                            

            });

                                                                            
            </script>
           
</body>

@endsection