@extends('Profesor.menuProfesor')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Información general</h2>
<ol class="breadcrumb">
<li class="active">
<a href="{{URL::to('profesor/graficas')}}">Calificaciones</a>
</li>
<li class="active">
<a href="{{URL::to('profesor/graficas/create')}}"><strong>Asistencias</strong></a>
</li>
</ol>
</div>
</div>


<div class="container">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="wrapper wrapper-content">
		<div class="col-lg-11">	
        <div class="row">
            <!--//////////////////////////////////////////////////-->
<div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Control de gráficas.- Asistencias de alumnos</h5>
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <!--
                                        <button type="button" class="btn btn-xs btn-white active">Today</button>
                                        <button type="button" class="btn btn-xs btn-white">Monthly</button>
                                        <button type="button" class="btn btn-xs btn-white">Asistencias</button>
                                    -->
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                                    </div>
                                    <br></br>
                                    <p>Gráfica de control de asistencias totales del ciclo</p>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                        <li>
                                            <h4 class="no-margins " id="total"></h4>
                                            <small>Asistencias totales del ciclo:</small>
                                            <div class="stat-percent">100% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 100%;" class="progress-bar"></div>
                                            </div>  
                                        </li>
                                        <li>
                                                <h4 class="no-margins " id="sumar1"></h4>
                                            <small>Asistencias del ciclo:</small>
                                            <div class="stat-percent">100% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 100%;" class="progress-bar"></div>
                                            </div> 
                                        </li>
                                        <li>
                                            <h4 class="no-margins " id="sumar2"></h4>
                                            <small>Retardos del ciclo:</small>
                                            <div class="stat-percent">100% <i class="fa fa-level-down text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 100%;" class="progress-bar"></div>
                                            </div> 
                                        </li>
                                        <li>
                                            <h4 class="no-margins " id="sumar3"></h4>
                                            <small>Faltas del ciclo:</small>
                                           <div class="stat-percent">100% <i class="fa fa-level-down text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 100%;" class="progress-bar"></div>
                                            </div> 
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
            <!--//////////////////////////////////////////////////-->
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Retardos</span>
                                <h5>Alumno con mas retardos</h5>
                            </div>
                            <div class="ibox-content">
                                	<table class="table table-hover table-mail">
                                		<tr class="unread">
										<tbody id="rows1">
										</tbody>
								</table>
                            </div>
                        </div>
                    </div>
            <!--//////////////////////////////////-->
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Faltas</span>
                                <h5>Alumno con mas faltas</h5>
                            </div>
                            <div class="ibox-content">
                            		<table class="table table-hover table-mail">
                                		<tr class="unread">
										<tbody id="rows2">
										</tbody>
									</table>
                            </div>
                        </div>
            		</div>
            <!--//////////////////////////////////-->
         </div>
         </div>   		
	</div>
</div>	
<script>
var promedios=<?= json_encode($asis)?>;
console.log(promedios);


 $('#rows').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getMayorAsistencia')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {

                        	console.log("data" + data)
                            $.each(data, function (i, item) {

                                var fila = $('<tr>');

                                var grupo =$('<td>',{
                                    text: item.grado + ' ' + item.grupo 
                                    
                                });

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.A
                                });
                                
                                fila.append(grupo);
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                
                                $('#rows').append(fila);
                           
                                });
                            }
                        });
 </script>
 <script>
 $('#rows1').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getAlumnosRetardos')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {

                        	console.log("data" + data)
                            $.each(data, function (i, item) {

                                var fila = $('<tr>');

                                var grupo =$('<td>',{
                                    text: item.grado + ' ' + item.grupo
                                    
                                });

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.R
                                });
                                
                                fila.append(grupo);
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                
                                $('#rows1').append(fila);
                           
                                });
                            }
                        });
 </script>
 <script>
 $('#rows2').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getAlumnosFaltas')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {

                        	console.log("data" + data)
                            $.each(data, function (i, item) {

                                var fila = $('<tr>');

                                var grupo =$('<td>',{
                                    text: item.grado + ' ' + item.grupo
                                    
                                });

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.F
                                });
                                
                                fila.append(grupo);
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                
                                $('#rows2').append(fila);
                           
                                });
                            }
                        });
                
    </script>

    <script>
        $('#sumar1').find('h4').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getMayorAsistencia')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.asistencia;
                                contador++;
                                });
                                suma=contador
                                var fila = $('<h4>');                                
                                
                                var asis=$('<h4>',{
                                    text:suma
                                });
                                
                                fila.append(asis);
                                
                                 $('#sumar1').append(fila);
                           
                                
                            }
                        });
    </script>
        <script>
        $('#sumar2').find('h4').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getRetardo')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.asistencia;
                                contador++;
                                });
                                suma=contador
                                var fila = $('<h4>');                                
                                
                                var asis=$('<h4>',{
                                    text:suma
                                });
                                
                                fila.append(asis);
                                
                                 $('#sumar2').append(fila);
                           
                                
                            }
                        });
    </script>
        <script>
        $('#sumar3').find('h4').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getFalta')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.asistencia;
                                contador++;
                                });
                                suma=contador
                                var fila = $('<h4>');                                
                                
                                var asis=$('<h4>',{
                                    text:suma
                                });
                                
                                fila.append(asis);
                                
                                 $('#sumar3').append(fila);
                           
                                
                            }
                        });
    </script>
    <script>
        $('#retardo').find('p').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getRetardo')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.R;
                                contador++;
                                });
                                suma=contador 
                                var fila = $('<p>');                                
                                
                                var asis=$('<p>',{
                                    text:suma
                                });
                                
                                fila.append(asis);
                                
                                 $('#retardo').append(fila);
                           
                                
                            }
                        });
    </script>
    <script>
        $('#total').find('p').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../TotalAsistencias')}}",
                        data: {idSubgrupo:"1",periodo:"1"},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.asistencia;
                                contador++;
                                });
                                suma=contador
                                var fila = $('<h4>');                                
                                
                                var asis=$('<h4>',{
                                    text:suma
                                });
                                
                                fila.append(asis);
                                  
                                 $('#total').append(fila);
                           
                                
                            }
                        });
    </script>
     <script>
        $(document).ready(function() {
            $('.chart').easyPieChart({
                barColor: '#f8ac59',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            $('.chart2').easyPieChart({
                barColor: '#1c84c6',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });


            var data3 = promedios;


            var dataset = [
                {
                    label: "Calificaciones",
                    data: data3,
                    color: "#58CBD7",
                    bars: {
                        show: true,
                        align: 'center',
                        barWidth: 0.5,
                        fill: 1,
                        lineWidth: 1
                    }

                }, 
            ];


            var options = {
            
            series: {
				bars: {
					show: true,
					barWidth: 0.6,
					align: "center"
				}
			},
            
            xaxis: {
                mode:"categories",
                tickLength: 0
            },

            
            };
            
            function gd(year, month, day) {
                return new Date(year, month, day).getTime();
            }

            var previousPoint = null, previousLabel = null;

            $.plot($("#flot-dashboard-chart"), [data3], options);

            var mapData = {
                "US": 298,
                "SA": 200,
                "DE": 220,
                "FR": 540,
                "CN": 120,
                "AU": 760,
                "BR": 550,
                "IN": 200,
                "GB": 120,
            };

            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },

                series: {
                    regions: [{
                        values: mapData,
                        scale: ["#1ab394", "#22d6b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });
        });
    </script>    


@endsection