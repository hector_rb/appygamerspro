@extends('Profesor.menuProfesor')
@section('content')
<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">
      
    <style type="text/css">
    .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        
        
         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
    </style>
    <body>
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Bienvenido al sistema de mensajes
                </h1>
                <h3 class="font-bold c">Crear mensaje</h3>
            </center>
        </form>
    </div>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/mensajesR/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/mensajesR')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('profesor/mensajesR/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <!--<li><a href="{{URL::to('/admin/mensajesR/papelera')}}"> <i class="fa fa-trash-o"></i>Papelera</a></li>-->
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/profesor/mensajesR','method'=>'POST','autocomplete'=>'off', 'files'=>'true' ))!!}
{{Form::token()}}         
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    Redactar Mensaje
                </h2>
            </div>
            <div class="mail-box">
                <div class="mail-body">

                        <form class="form-horizontal" method="get">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Para:</label>

                                <div class="col-sm-10">
                                <select required class="chosen-select" data-placeholder="" name="IdDestinatario[]" multiple  tabindex="2">
                                        @foreach ($variable as $v)
                                        <option  value="1,{{$v->iduser}}" >Admin|{{$v->Nombre}}</option>
                                        @endforeach
                                        @foreach ($variable2 as $v2)
                                        <option  value="3,{{$v2->iduser}}" >Profesor|{{$v2->name}} {{$v2->apepat}} {{$v2->apemat}}|{{$v2->nombreNivel}}</option>
                                        @endforeach
                                        @foreach ($variable3 as $v3)
                                        <option  value="4,{{$v3->iduser}}" >Alumno|{{$v3->grado}}{{$v3->grupo}}|{{$v3->name}} {{$v3->apepat}} {{$v3->apemat}}|{{$v3->nombreNivel}}</option>
                                        @endforeach
                                        @foreach ($variable4 as $v4)
                                        <option value="2,{{$v4->iduser}}" >Tutor|{{$v4->name}} (tutelado:{{$v4->NombreAlumno}} {{$v4->apepat}} {{$v4->apemat}}|{{$v3->nombreNivel}})</option>
                                        @endforeach
                                </select></div>
                            </div>
                            <br></br>
                            <div class="form-group"><label class="col-sm-2 control-label">Asunto:</label>

                                <div class="col-sm-10"><input type="text" class="form-control" name="Asunto" value="" required="required" placeholder="Asunto..."></div>
                            </div>
                            <br></br>
                            <br></br>
                            <textarea class="form-control" name="Mensaje" placeholder="mensaje..."></textarea>
                            <br>
                            <input type="file" class="form-control" name="anexo" value="">
                            <br>
                            <div class="col-sm-10"></div>
                            <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>

                        </form>
                </div>
            </div>
            
        </div>
                <!--<div class="col-lg-3"></div>
                <div class="col-lg-9">
                    <div class="ibox">
                        <div class="ibox-content">
                            <form action="#" class="dropzone" id="dropzoneForm">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->
        </div>
{!!Form::close()!!} 
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.chosen-select').chosen();   
    });
</script> 
 
</body>
@endsection