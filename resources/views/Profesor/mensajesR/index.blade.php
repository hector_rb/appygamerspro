@extends('Profesor.menuProfesor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        
        
         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        
    </style>
</head>
<body>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Bienvenido al sistema de mensajes
                </h1>
                <h3 class="font-bold c">Bandeja de entrada</h3>
            </center>
        </form>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/mensajesR/create')}}">Redactar Mensaje</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/mensajesR')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada<span class="label label-warning pull-right">{{$sinver}}</span></a></li>
                            <li><a href="{{URL::to('profesor/mensajesR/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                            <!--<li><a href="{{URL::to('/admin/mensajesR/papelera')}}"> <i class="fa fa-trash-o"></i>Papelera</a></li>-->
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>


        
        <div class="col-lg-9">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-hover table-mail">
                        <tbody>
                            <thead>
                                        <th></th>
                                        <th>Enviado por:</th>
                                        <th>Asunto</th>
                                        <th>Fecha</th>
                                        <th></th>
                            </thead>
                            @foreach ($mensajes as $m)
                            <thead>
                                <?php
                                if($m->Visto!=0)
                                    echo '<tr bgcolor="#FAFAFA">';
                                else
                                    echo '<tr bgcolor="#E6E6E6">';
                                ?>

                                        <td>
                                                <a href="{{URL::to('profesor/mensajesR/'.$m->id)}}" data-toggle="tooltip" data-placement="top" title="Ver mensaje"><button class="btn btn-white btn-sm fa fa-envelope a"></button></a>
                                                <a href="" data-target="#modal-delete-{{$m->id}}" data-toggle="modal" title="Eliminar"><button class="btn btn-white btn-sm fa fa-trash-o a"></button></a>
                                                <!--<a href="" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fa fa-trash-o" data-target="#modal-delete-{{$m->id}}" data-toggle="modal"></i> </a>-->
                                        </td>
                                        <td><strong>{{$m->destinatario}}</strong>|{{$m->usuarios}}</td>
                                        <td>{{$m->Asunto}}</td>
                                        <td><strong>{{$m->fecha}}</strong></td>
                                        <!--<td><a href="" data-target="#modal-delete-{{$m->id}}" data-toggle="modal"><button class="btn btn-w-m btn-w-m btn-outline btn-danger btn-xs">Eliminar</button></a>
                                            <br></br>
                                        <a href="{{URL::to('admin/mensajesR/'.$m->id)}}"><button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Ver</button></a>
                                        </td>-->
                                </tr>
                            </thead>
                            @include('Profesor.mensajesR.modal')
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection