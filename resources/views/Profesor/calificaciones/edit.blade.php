@extends('Profesor.menuProfesor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/green.css')}}" />
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/yellow.css')}}" />
	<link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/red.css')}}" />
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

	<link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

        .b{
            color: #EB984E;
        }
        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>
    <form>
        <center>
            <h1 class="font-bold b">Calificaciones</h1>
            <h3 class="font-bold c">editar calificación</h3>
        </center>
    </form>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5>Administración de calificaciones</h5>
<div class="ibox-tools">
</div>
</div>
<div class="ibox-content">



<div class="col-lg-4 col-md-2 col-md-2 col-xs-12">
<div class="form-group" id="editar">

<br>
</select>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Registros de calificaciones.</p>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="table-responsive">
<div class="table-responsive">
	{!!Form::model($calif,['method'=>'PATCH','route'=>['calificacion.update', $sg,'periodo'=>$periodo]])!!}
<table class="table table-hover table-mail">
<tbody>
<tr class="unread">
<thead >
<th width="50" height="50">id</th>
<th>Alumno</th>
<th>Calificación</th>
</thead>
<tbody id="rows">
	@foreach($calif as $c)
	<tr>
		<td>
			<input type="hidden" name="id[]" value="{{$c->idcalif}}">
			<input type="hidden" name="iduser[]" value="{{$c->idAlumno}}">
			{{$c->idAlumno}}
		</td>
		<td>
			{{$c->nombreAlumno}} {{$c->apepat}} {{$c->apemat}}
		</td>
		<td>
		<input type="number" max="10" min="0" name="calif[]" step=".01" value="{{$c->calif}}" class="form-control">			
		</td>
	</tr>
	@endforeach
	<tr>
		<td></td>
		<td></td>
		<td align="center"><button type="submit" class="btn btn-warning">Enviar</button></td>
	</tr>
</tbody>
</tr>
</tbody>
</table>
{!!Form::close()!!}
</div>
</div>
</div></div></div>
</div></div></div>
</div></div></div>
<script >
	/*$("#selectSubgrupo").change(function(){
            		var idsg = this.value;

		            $('#selectPeriodos').find('option').not(':first').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getPeriodosProfesor')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        var op =$('<option>',{
		                        	value: item.idPeriodo,
		                        	text: item.nombrePeriodo
		                        });
		                        
		                        
		                        $('#selectPeriodos').append(op);
		                   
		                		});
		            		}
		        		});
		            
		        });
	$("#selectPeriodos").change(function(){
            		var id = this.value;
            		var idsg = $('#selectSubgrupo').val();
		            
		            $('#rows').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getCalificacionesProfesor')}}",
		                data: {idP:id,idsg:idsg},
		                dataType: 'json',
		                success: function (data) {
		                	
	                    	var boton= $('<button>',{
	                        	class:"btn btn-info",
	                        	text:'Editar Calificaciones'
	                        });
	                        var a=$('<a>',{
	                        	href:"{{('calificaciones')}}/" + $('#selectSubgrupo').val() + "/edit?periodo=" + $('#selectPeriodos').val()
	                        	//http://localhost/MyAppCollege/public/admin/horario/28?ciclo=3
	                        });
	                        a.append(boton);

                   			$('#editar').append(a);
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);

		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.idAlumno,
		                        	
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
 		                        }); 		                        
		                        var calif =$('<td>',
		                        	{
		                        		text:item.calif
		                        	});
		                        
		                        fila.append(id);
		                        fila.append(nombre);
		                        fila.append(calif);
		                        
		                        $('#rows').append(fila);
		                   
	                		});
	                    	var fila = $('<tr>');

	                        var td1 =$('<td>');
	                        var td2 =$('<td>');
	                        var td3 =$('<td>',{
	                        	align:"center"
	                        });
	                    	var boton= $('<button>',{
	                        	class:"btn btn-info",
	                        	text:'Editar Calificaciones'
	                        });
	                        var a=$('<a>',{
                        		href:"{{('calificaciones')}}/" + $('#selectSubgrupo').val() + "/edit?periodo=" + $('#selectSubgrupo').val()
	                        	//href:"{{('horarioTutor')}}/" + item.idGrupo + "?ciclo=" + item.idCiclo 
	                        	//http://localhost/MyAppCollege/public/admin/horario/28?ciclo=3
	                        });
	                        a.append(boton);
	                        td3.append(a);
	                        fila.append(td1);
	                        fila.append(td2);
	                        fila.append(td3);

                   			$('#rows').append(fila);
		            	}
		        	});
		        });*/
        </script>
@endsection