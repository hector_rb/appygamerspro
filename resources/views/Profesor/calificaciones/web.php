<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('registro', 'Auth\RegisterController@showRegistrationForm2')->name('registro');
Route::post('registro', 'Auth\RegisterController@register');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','admin'], 'prefix'=>'admin'], function(){
	Route::resource('/','adminDashController');
	Route::resource('RegistroInstitucion','registroInstitucionController');
	Route::resource('eventos','eventosAdminController');
	Route::get('saveEvent','eventosAdminController@event');
	Route::resource('InformacionGeneral', 'informacionGeneralController');
	Route::resource('grupos','gruposController');
	Route::resource('materias','materiasController');
	Route::get('getGrados','materiasController@getGrados');
	Route::resource('aMaterias','profesor_materiaController');
	Route::get('getMaterias','materiasController@getMaterias');
	Route::resource('horario','horarioController');
	Route::get('getGrupos','horarioController@getGrupos');
	Route::get('getProf','horarioController@getProf');
	Route::get('getCiclo','horarioController@getCiclo');
	Route::get('saveHorario','horarioController@event');
	Route::get('horario/editar', 'horarioController@editar');
	Route::resource('directorio','directorioController');
	Route::post('saveImageDir','directorioController@saveImageDir');

	Route::get('getNivel','eventosAdminController@getNivel');
	
	Route::resource('ciclo','CicloController');
	Route::get('cicloCambiar','CicloController@cicloCambiar');
	Route::post('saveLogo','registroInstitucionController@saveImage');

	Route::get('mensajes/getDestinatario','mensajes@getDestinatario');

	Route::get('mensajes/eliminados','mensajes@eliminados');
	Route::get('mensajes/getDestinatario','mensajes@getDestinatario');
    Route::get('mensajes/recibidos/show/{id}', ['as' => 'MuestraAdmin', 'uses'=>'mensajes@show']);
	Route::get('mensajes/recibidos','mensajes@recibidos');
	Route::get('mensajes/enviados','mensajes@enviados');
	Route::resource('mensajes','mensajes');
	Route::post('mensajes/recibidos/search', ['as' => 'BuscaAdmin', 'uses'=>'mensajes@search']);
	Route::post('mensajes/create/store', ['as' => 'AltaMensajeAdmin', 'uses'=>'mensajes@store']);
	Route::get('mensajes/recibidos/destroy/{id}', ['as' => 'mensajes/destroy', 'uses'=>'mensajes@destroy']);
	Route::get('mensajes/recibidos/edit/{id}', ['as' => 'EditarAdmin', 'uses'=>'mensajes@edit']);
    Route::get('/mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraAdmin', 'uses'=>'mensajes@papelera']);
    Route::get('mensajes/getNivel',['as' => 'getNivelAdmin', 'uses'=>'mensajes@getNivel']);

    

	Route::resource('/alerta','AlertaController');
	
});

Route::group(['middleware' => ['auth','tutor'], 'prefix'=>'tutor'], function(){

	Route::get('registroA', 'Auth\registrarAlumno@showRegistrationForm3')->name('registroA');
	Route::post('registroA', 'Auth\registrarAlumno@register');
	Route::resource('/horarioTutor','horarioTutorController');
	Route::get('getHorarioTutor','horarioTutorController@getHorario');

	Route::resource('/','tutorDashController');
	Route::resource('/tareas','TareaTutorController');
	Route::get('getTareasTutor','TareaTutorController@getTareas');

	Route::resource('/asistencia','AsistenciaTutorController');
	Route::get('getAsistenciaTutor','AsistenciaTutorController@getAsistencia');

	Route::resource('/calificaciones','CalificacionesTutorController');
	Route::get('getCalificacionesTutor','CalificacionesTutorController@getCalificaciones');
	Route::get('getPeriodosTutor','CalificacionesTutorController@getPeriodos');

	Route::resource('/alerta','AlertaTutorController');

	Route::resource('/perfilTut','perfilTutorController');
	Route::post('/saveImageTut','perfilTutorController@saveImage');

	Route::get('/mensajes/recibidos','TutorMensajes@recibidos');
	Route::get('/mensajes/','TutorMensajes@index');
	Route::post('/mensajes/recibidos/search', ['as' => 'Busca', 'uses'=>'TutorMensajes@search']);
	Route::get('/mensajes/recibidos/show/{id}', ['as' => 'Muestra', 'uses'=>'TutorMensajes@show']);
	Route::get('/mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraTutor', 'uses'=>'TutorMensajes@papelera']);
	Route::get('/mensajes/eliminados/desaparece/{id}', ['as' => 'Desaparece', 'uses'=>'TutorMensajes@desaparece']);
});

Route::group(['middleware' => ['auth','profesor'], 'prefix'=>'profesor'], function(){
	Route::resource('/','profesorDashController');
	Route::resource('/tareas','TareaController');
	Route::resource('/eventos','eventosProfesorController');
	Route::resource('/asistencia','AsistenciaController');
	Route::resource('/calificacion','CalificacionesController');
	Route::resource('/graficas','GraficasProfesorController');
	Route::get('getPeriodosProfesor','CalificacionesController@getPeriodosProfesor');
	Route::get('getPeriodosCalificados','CalificacionesController@getPeriodosCalificados');
	Route::get('getPeriodosNoCalificados','CalificacionesController@getPeriodosNoCalificados');
	Route::get('getAlumnoCalifProfesor','CalificacionesController@getAlumnoCalifProfesor');
	Route::get('getCalificacionesProfesor','CalificacionesController@getCalificacionesProfesor');
	Route::get('getAlumnoAsis','AsistenciaController@getAlumno');
	Route::get('getAsistenciasProf','AsistenciaController@getAsistencia');

	Route::resource('/perfilProf','perfilProfesorController');
	Route::post('/saveImageProf','perfilProfesorController@saveImage');
	Route::get('/getFechasAsistencia','AsistenciaController@getFechasAsistencia');


	Route::get('getPromedioGrupo','GraficasProfesorController@getPromedioGrupo');
	Route::get('getPromedioAlumnoSobresaliente','GraficasProfesorController@getPromedioAlumnoSobresaliente');
	Route::get('getPromedioAlumnoRegular','GraficasProfesorController@getPromedioAlumnoRegular'
	);
	Route::get('getGraficas1','GraficasProfesorController@getGraficas1'
	);
	Route::get('getGraficas2','GraficasProfesorController@getGraficas2'
	);
		
Route::get('mensajes/eliminados','ProfesorMensaje@eliminados');
Route::get('mensajes/getDestinatario','ProfesorMensaje@getDestinatario');
Route::get('mensajes/recibidos/show/{id}', 'ProfesorMensaje@show');
Route::get('mensajes/recibidos','ProfesorMensaje@recibidos');
Route::get('mensajes/enviados','ProfesorMensaje@enviados');
Route::resource('mensajes','ProfesorMensaje');
Route::get('mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraProfe', 'uses'=>'ProfesorMensaje@papelera']);
Route::post('mensajes/recibidos/search', ['as' => 'mensajes/search', 'uses'=>'ProfesorMensaje@search']);
});

Route::group(['middleware' => ['auth','alumno'], 'prefix'=>'alumno'], function(){
	Route::resource('/','alumnoDashController');
	Route::resource('/tareas','TareaAlumnoController');
	Route::resource('/asistencia','AsistenciaAlumnoController');
	Route::resource('/calificaciones','CalificacionesAlumnoController');
	Route::get('getCalificacionesAlumno','CalificacionesAlumnoController@getCalificaciones');
	Route::get('getAsistenciaAlumno','AsistenciaAlumnoController@getAsistencia');
	Route::resource('/eventos','eventosAlumnoController');

	Route::resource('/horarioAlumno','horarioAlumnoController');
	
	Route::resource('/perfilAlum','perfilAlumnoController');
	Route::post('/saveImageAlum','perfilAlumnoController@saveImage');
	
	Route::get('mensajes/recibidos','AlumnoMensajes@recibidos');
    Route::get('mensajes/','AlumnoMensajes@index');
    Route::post('mensajes/recibidos/search', ['as' => 'Busca2', 'uses'=>'AlumnoMensajes@search']);
    Route::get('mensajes/recibidos/show/{id}', ['as' => 'Muestra2', 'uses'=>'AlumnoMensajes@show']);
    Route::get('mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraAlumno', 'uses'=>'AlumnoMensajes@papelera']);
    Route::get('mensajes/eliminados',['as' => 'Eliminados2', 'uses'=>'AlumnoMensajes@eliminados']);
    Route::get('mensajes/eliminados/desaparece/{id}', ['as' => 'Desaparece2', 'uses'=>'AlumnoMensajes@desaparece']);

});

Route::group(['middleware' => ['auth','super-admin'], 'prefix'=>'super-admin'], function(){
	Route::resource('/','superAdminDashController');
});