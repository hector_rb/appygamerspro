@extends('Profesor.menuProfesor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/green.css')}}" />
    <link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/yellow.css')}}" />
    <link rel="stylesheet" href="{{asset('css/plugins/icheck-1.x/skins/line/red.css')}}" />
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

        .b{
            color: #EB984E;
        }
        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body>
    <form>
        <center>
            <h1 class="font-bold b">Calificaciones</h1>
            <h3 class="font-bold c">Dar de alta nueva calificación</h3>
        </center>
    </form>
<!--<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Calificaciones</h2>
<ol class="breadcrumb">
<li>
<a href="{{URL::to('profesor/calificacion/create')}}"><strong>Nueva calificación</strong></a>
</li>
<li class="active">
<a href="{{URL::to('profesor/calificacion')}}">Calificaciones</a>
</li>

</ol>
</div>
<div class="col-lg-2">

</div>
</div>-->
<div class="wrapper wrapper-content animated fadeInRight">
            <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content mailbox-content">
                                <div class="file-manager">
                                    <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/calificacion/create')}}">Nueva Calificación</a>
                                    <div class="space-25"></div>
                                    <h5>Folders</h5>
                                    <ul class="folder-list m-b-md" style="padding: 0">
                                        <li><a href="{{URL::to('profesor/calificacion')}}"> <i class="fa fa-inbox "></i>Registro de Calificación</a></li>
                                    </ul>
            
                                </div>
                            </div>
                        </div>
                    </div>
                               
            <div class="container">
                <div class="col-lg-9">
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                            <h5>Administración de calificaciones</h5>
                            <div class="ibox-content">
                            
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
                            <h4>Crear nueva calificación</h4>
                            
                            @if(count ($errors)>0)
                            <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                            </ul>
                            </div>
                            </div>
                            @endif
                            {!!Form::open(array('url'=>'/profesor/calificacion','method'=>'POST','autocomplete'=>'off'))!!}
                            {{Form::token()}}
                            
                            <div class="col-lg-4 col-md-3 col-md-3 col-xs-12">
                            <div class="form-group">
                            <label for="idmateria">Materia-Grupo-Nivel</label>
                            <select name="idSubgrupo" id="selectSubgrupo" class="form-control" required="true">
                            <option value=""></option>
                            @foreach ($sg as $sg)
                            <option  value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
                            @endforeach
                            </select>
                            </div>
                            </div>
                            
                            
                            
                            <div class="col-lg-2 col-md-2 col-md-2 col-xs-12">
                            <div class="form-group">
                            <label for="numevaluaciones">Periodo</label>
                            <select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
                                <option></option>
                            </select>
                            </div>
                            </div>
                            
                            
                            <div class="col-lg-3 col-md-2 col-md-2 col-xs-12">
                            <div class="form-group">
                            <label for="fecha">Fecha</label>
                            <input type="date" id="inputFecha" name="fecha" required value="{{old('fecha')}}" class="form-control" placeholder="fecha">
                            </div>
                            </div>
                            
                            <div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
                            <div class="form-group">
                            <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>
                            </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-condensed table-hover">
                            <th>Matrícula</th>
                            <th>Grupo</th>
                            <th>Nombre del alumno</th>
                            <th>Calificación</th>
                            </thead>
                            <tbody id="rows">
                                
                            </tbody>
                            </table>
                            </div>
                            </div>
                            
                            <div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
                            <div class="form-group">
                            <button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>
                            </div>
            </div>
</div>


{!!Form::close()!!}
<script >
    var i=false;
            $("#selectSubgrupo").change(function(){
                    var idsg = this.value;

                    $('#selectPeriodos').find('option').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getPeriodosNoCalificados')}}",
                        data: {id:idsg},
                        async:false,
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                i=true;
                                var op =$('<option>',{
                                    value: item.idPeriodo,
                                    text: item.nombrePeriodo
                                });
                                
                                
                                $('#selectPeriodos').append(op);
                           
                                });
                            }
                        });
                    $('#rows').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('../getAlumnoCalifProfesor')}}",
                        data: {id:idsg},
                        async:false,
                        dataType: 'json',
                        success: function (data) {
                            console.log();
                            if($('#selectPeriodos option').length>0)
                            {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var fila = $('<tr>');

                                var id =$('<td>',{
                                    text: item.matricula,
                                    
                                });

                                var idalum=$('<input>',{
                                    value:item.idAlumno,
                                    name:'id[]',
                                    type:'hidden'
                                })

                                var grupo =$('<td>',{
                                    text: item.grado + item.grupo
                                });
                                var nombre =$('<td>',{
                                    text: item.apepat + ' ' + item.apemat + ' ' + item.nombreAlumno
                                });
                                var input=$('<input>',{
                                    type:'number',
                                    min:0,
                                    max:10,
                                    required:'required',
                                    name:'calif[]',
                                    step:'.01',
                                    class:'form-control'

                                })
                                var calif =$('<td>');
                                calif.append(input);
                                
                                id.append(idalum);
                                
                                fila.append(id);
                                fila.append(grupo);
                                fila.append(nombre);
                                fila.append(calif);
                                
                                $('#rows').append(fila);
                           
                                });
                            }
                            else
                           {
                             alert("no hay periodos por calificar");   
                            }
                            }
                        });
                        
                });
        </script>
        <script>
    Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });
            $(document).ready( function() {
                $('#inputFecha').val(new Date().toDateInputValue());
            })
    </script>
</body>
@endsection