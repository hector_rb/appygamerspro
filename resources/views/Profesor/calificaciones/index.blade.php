@extends('Profesor.menuProfesor')
@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body> 
<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Calificaciones
                </h1>
                <h3 class="font-bold c">Registro de Calificaciones</h3>
            </center>
        </form>
    </div>
<!--<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Calificaciones</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{URL::to('profesor/calificacion/create')}}"">Nueva Calificación</a>
			</li>
			<li class="active">
				<a href="{{URL::to('profesor/calificacion')}}""><strong>Calificaciones</strong></a>
			</li>

		</ol>
	</div>
	<div class="col-lg-2">
	</div>
</div>-->

<div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/calificacion/create')}}">Nueva Calificación</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/calificacion')}}"> <i class="fa fa-inbox "></i>Registro de Calificaciones</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
<div class="container">
    <div class="col-lg-9 animated fadeInRight">
        <div class="ibox-content">
                    <div class="ibox-title">
                    <h5>Administración de calificaciones</h5>
                    
                    </div>
                    <div class="ibox-content">
                    
                    <div class="col-lg-4 col-md-3 col-md-3 col-xs-12">
                    <div class="form-group">
                    <label for="idmateria">Materia-Grupo-Nivel</label>
                    <select name="idSubgrupo" id="selectSubgrupo" class="form-control">
                    <option value=""></option>
                    @foreach ($sg as $sg)
                    <option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
                    @endforeach
                    </select>
                    </div>
                    </div>
                    <div class="col-lg-4 col-md-2 col-md-2 col-xs-12">
                    <div class="form-group">
                    <label for="numevaluaciones">Periodo</label>
                    <select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
                    	<option></option>
                    </select>
                    </div>
                    </div>
                    <div class="col-lg-4 col-md-2 col-md-2 col-xs-12">
                    <div class="form-group" id="editar">
                    
                    <br>
                    </select>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
                    <p>Registros de calificaciones.</p>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
                    <div class="table-responsive">
                    	<form>
                    					<center> 
                    <input type='button' class="btn btn-primary" onclick='descargarExcel()' value='Excel' />
                    <input type='button' class="btn btn-info" onclick='window.print();' value='Imprimir' />
                    </center></form>
                    <div class="table-responsive">
                        <table id="idtabla" class="table table-hover table-mail">
                            <thead >
                                <th>Matrícula</th>
                                <th>Alumno</th>
                                <th>Calificación</th>
                            </thead>
                            <tbody id="rows">	
                            </tbody>
                        </table>
                    
                    </div>
                    </div>
                    {!!Form::close()!!}
                    </div>
        </div>
    </div>
</div>
<script >
	$("#selectSubgrupo").change(function(){
            		var idsg = this.value;
            		$('#rows').find('tr').remove();
		            $('#selectPeriodos').find('option').not(':first').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getPeriodosCalificados')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        var op =$('<option>',{
		                        	value: item.idPeriodo,
		                        	text: item.nombrePeriodo
		                        });
		                        
		                        
		                        $('#selectPeriodos').append(op);
		                   
		                		});
		            		}
		        		});
		            
		        });
	$("#selectPeriodos").change(function(){
            		var id = this.value;
            		var idsg = $('#selectSubgrupo').val();
		            
		            $('#rows').find('tr').remove();
		            $('#editar').find('a').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getCalificacionesProfesor')}}",
		                data: {idP:id,idsg:idsg},
		                dataType: 'json',
		                success: function (data) {
		                	
	                    	var boton= $('<button>',{
	                        	class:"btn btn-info",
	                        	text:'Editar Calificaciones'
	                        });
	                        var a=$('<a>',{
	                        	href:"{{('calificacion')}}/" + $('#selectSubgrupo').val() + "/edit?periodo=" + $('#selectPeriodos').val()
	                        	//http://localhost/MyAppCollege/public/admin/horario/28?ciclo=3
	                        });
	                        a.append(boton);

                   			$('#editar').append(a);
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);

		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.matricula,
		                        	
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.apepat + ' ' + item.apemat + ' ' + item.nombreAlumno
 		                        }); 		                        
		                        var calif =$('<td>',
		                        	{
		                        		text:item.calif
		                        	});
		                        
		                        fila.append(id);
		                        fila.append(nombre);
		                        fila.append(calif);
		                        
		                        $('#rows').append(fila);
		                   
	                		});
	                    	var fila = $('<tr>');

	                        var td1 =$('<td>');
	                        var td2 =$('<td>');
	                        var td3 =$('<td>',{
	                        	align:"center"
	                        });
	                    	var boton= $('<button>',{
	                        	class:"btn btn-info",
	                        	text:'Editar Calificaciones'
	                        });
	                        var a=$('<a>',{
                        		href:"{{('calificacion')}}/" + $('#selectSubgrupo').val() + "/edit?periodo=" + $('#selectPeriodos').val()
	                        	//href:"{{('horarioTutor')}}/" + item.idGrupo + "?ciclo=" + item.idCiclo 
	                        	//http://localhost/MyAppCollege/public/admin/horario/28?ciclo=3
	                        });
	                        a.append(boton);
	                        td3.append(a);
	                        fila.append(td1);
	                        fila.append(td2);
	                        fila.append(td3);

                   			$('#rows').append(fila);
		            	}
		        	});
		        });
        </script>
        <script>
			function descargarExcel(){
		        //Creamos un Elemento Temporal en forma de enlace
		        var tmpElemento = document.createElement('a');
		        // obtenemos la información desde el div que lo contiene en el html
		        // Obtenemos la información de la tabla
		        var data_type = 'data:application/vnd.ms-excel';
		        var tabla_div = document.getElementById('idtabla');
		        var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
		        tmpElemento.href = data_type + ', ' + tabla_html;
		        //Asignamos el nombre a nuestro EXCEL
		        tmpElemento.download = 'Calificaciones.xls';
		        // Simulamos el click al elemento creado para descargarlo
		        tmpElemento.click();
		    }
		    //descargarExcel();
		</script>
</body>
@endsection