@extends('Profesor.menuProfesor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
         .b{
            color: #EB984E;
        }

        .c{
            color: #1ABC9C;
        }

        .d{
            background-color: #76D7C4;
        }


    </style>
</head>
<body>


        <div class="tooltip-demo">
            <form>
                <center>
                    <h1 class="font-bold b">Horarios
                        
                    </h1>
                    <h3 class="font-bold c">Horarios del Profesor(a)  {{$prof->nombreprof}} {{$prof->apepat}} {{$prof->apemat}}
                    </h3>
                </center>
            </form>
        </div>
        <br>


        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="ibox-content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Nivel</th>
                                    <th>Grupo</th>
                                    <th>Ciclo</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="rows">
                            @foreach($horario as $h)
                            <tr>
                                <th>{{$h->nombreNivel}}</th>  
                                <th>{{$h->grado}} {{$h->grupo}} </th>
                                <th>{{$h->nombreCiclo}} </th>
                                <th><a href="{{URL::action('HorarioProfesorController@show',['grupo'=>$h->idGrupo,'ciclo'=>$h->idCiclo])}}"><button class="btn btn-w-m btn-w-m btn-outline btn-info btn-xs">Ver</button></a></th>
                            </tr>   
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    language: {
                                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                                },
                    buttons: [

                        {
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]

                });

            });
</script>           
</body>

@endsection