@extends('Profesor.menuProfesor')

@section('content')
<head>
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

        .b{
            color: #EB984E;
        }
        
    </style>
</head>
<body>
    <form>
        <center>
            <h1 class="font-bold b">Avisos</h1>
        </center>
    </form>
    <br>
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/avisosProf/create')}}">Crear aviso</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/avisosProf')}}"> <i class="fa fa-inbox "></i> Historial de avisos<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('profesor/avisosProf/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="wrapper wrapper-content">
    <div class="row">        
        <div class="col-lg-12 animated fadeInRight">
        <div class="mail-box-header">
            <div class="pull-right tooltip-demo">
                {{-- <a href="mail_compose.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Reply</a>
                <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
                <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a> --}}
            </div>
            <h2>
                {{$aviso->asunto}}
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">


                <h3>
                    <span class="font-normal">Asunto: </span>{{$aviso->asunto}}
                </h3>
                <h5>
                    <span class="pull-right font-normal">{{$aviso->fecha}}</span>
                    <span class="font-normal">Para: </span>
                    	@if($aviso->destinatario==1)
                            General
                        @endif
                        @if($aviso->destinatario==2)
                            Tutores
                        @endif
                        @if($aviso->destinatario==3)
                            Profesores
                        @endif
                        @if($aviso->destinatario==4)
                            Alumnos
                        @endif
                </h5>
            </div>
        </div>
            <div class="mail-box">


            <div class="mail-body">
                {!!html_entity_decode($aviso->cuerpo)!!}
            </div>
                
                   
                    <div class="clearfix"></div>


            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
</body>
@endsection