@extends('Profesor.menuProfesor')
@section('content')
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }
        
    </style>
</head>
<body>
<div class="tooltip-demo">
    <form>
        <center>
            <h1 class="font-bold b">Bienvenido al sistema de avisos
            </h1>
            <h3 class="font-bold c">Avisos enviados</h3>
        </center>
    </form>
</div>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-info compose-mail" href="{{URL::to('profesor/avisosProf/create')}}">Crear aviso</a>
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('profesor/avisosProf')}}"> <i class="fa fa-inbox "></i> Avisos recibidos<span class="label label-warning pull-right"></span></a></li>
                            <li><a href="{{URL::to('profesor/avisosProf/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="wrapper wrapper-content">
                <div class="row">
            
            <div class="col-lg-12 animated fadeInRight">
            
                <div class="ibox-content">

                <table class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                    <tr>
                        <th></th>
                        <th>Asunto</th>
                        <th>Destinatarios</th>
                        <th class="text-right mail-date">Fecha</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($avisos as $a)
                    <tr class="read">
                        
                        <td align="right"><a href="{{URL::action('avisosProfesorController@show',['id'=>$a->idAviso])}}"
                            title="Ver aviso"><button class="btn btn-white btn-sm fa fa-bell a"></button></a></td>
                        <td class="mail-subject"><a href="mail_detail.html">{{$a->asunto}}</a></td>
                        @if($a->destinatario==1)
                            <td>General</td>
                        @endif
                        @if($a->destinatario==2)
                            <td>Tutores</td>
                        @endif
                        @if($a->destinatario==3)
                            <td>Profesores</td>
                        @endif
                        @if($a->destinatario==4)
                            <td>Alumnos</td>
                        @endif
                        @if($a->destinatario==5)
                            <td>Todos los grupos</td>
                        @endif
                        @if($a->destinatario==6)
                            <td>Grupo {{$a->grado}}{{$a->grupo}}</td>
                        @endif
                        <td class="text-right mail-date"><a href="mail_detail.html">{{$a->fecha}}</a></td>
                    </tr>
                @endforeach
                
                </tbody>
                </table>

                </div>
            </div>
                </div>
            </div>
        </div>

</body>
@endsection