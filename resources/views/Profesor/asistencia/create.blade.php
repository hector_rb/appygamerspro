@extends('Profesor.menuProfesor')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Asistencias</h2>
<ol class="breadcrumb">
<li class="active">
<a href="/profesor/asistencia">Asistencia</a>
</li>
<li>
<a href="asistencia/create"><strong>Nuevas asistencias</strong></a>
</li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>


 <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Administración de asistencias</h5>
                    <div class="ibox-content">
<h4>Control de asistencia</h4>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Para registrar la asistencia solo debe seleccionar grupo en cuestión, así como la fecha en que desee registrar dicha asistencia.</p>
@if(count ($errors)>0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif
{!!Form::open(array('url'=>'/profesor/asistencia','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
<br></br>
<div class="col-lg-6 col-md-8 col-md-8 col-xs-12">
<div class="form-group">
<label for="idmateria">Materia - Grupo - Nivel</label>
<select name="idSubgrupo" class="form-control" id="selectSubgrupo">
<option value=""></option>
@foreach ($sg as $sg)
<option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
@endforeach
</select>
</div>
</div>

<div class="col-lg-2 col-md-2 col-md-2 col-xs-12">
<div class="form-group">
<label for="numevaluaciones">Periodo</label>
<select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
	<option></option>
</select>
</div>
</div>


<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="fecha">Fecha</label>
<input type="date" name="fecha" id="inputFecha" required value="{{old('fecha')}}" class="form-control" placeholder="fecha">
</div>
</div>

<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>
</div>
</div>

<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="table-responsive">
<table class="table table-striped table-bordered table-condensed table-hover" id="tabla">
<thead>
<th>Id</th>
<th>Grupo</th>
<th>Nombre</th>
<th><p align="center">Asistencia</p>  
<p align="center"> Asistencia - Retardo - Falta</p></th>
</thead>
<tbody id="rows">
	
</tbody>

</table>

</div>
</div>
<div class="col-lg-6 col-md-6 col-md-6 col-xs-12">
<div class="form-group">
<button class="btn btn-w-m btn-w-m btn-outline btn-primary btn-xs" type="submit">Enviar</button>
</div>
</div></div></div>
</div></div></div>

    </div>
    </div>
{!!Form::close()!!}

<script >
        	$("#selectSubgrupo").change(function(){
            		var idsg = this.value;
            		$('#selectPeriodos').find('option').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('../getPeriodosProfesor')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        var op =$('<option>',{
		                        	value: item.idPeriodo,
		                        	text: item.nombrePeriodo
		                        });
		                        
		                        
		                        $('#selectPeriodos').append(op);
		                   
		                		});
		            		}
		        		});

		            $('#rows').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('../getAlumnoAsis')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                	 var i=0;
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                       
		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.idAlumno,
		                        	
		                        });

		                        var idalum=$('<input>',{
		                        	value:item.idAlumno,
		                        	name:'id[]',
		                        	type:'hidden'
		                        })

		                        var grupo =$('<td>',{
		                        	text: item.grado + item.grupo
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
 		                        });
 		                        var a=$('<input>',{
 		                        	type:'radio',
 		                        	required:'required',
 		                        	name:'asis['+i+']',
 		                        	value:'A'

 		                        });
 		                        var r=$('<input>',{
 		                        	type:'radio',
 		                        	required:'required',
 		                        	name:'asis['+i+']',
 		                        	value:'R'

 		                        });
 		                        var f=$('<input>',{
 		                        	type:'radio',
 		                        	required:'required',
 		                        	name:'asis['+i+']',
 		                        	value:'F'

 		                        });
 		                        var cent =$('<center>');
		                        var asis =$('<td>');
		                        id.append(idalum);
		                        cent.append(a);
		                        cent.append(r);
		                        cent.append(f);
		                        asis.append(cent);

		                        fila.append(id);
		                        fila.append(grupo);
		                        fila.append(nombre);
		                        fila.append(asis);
		                        
		                        $('#rows').append(fila);
		                   		i++;
		                		});
		            		}
		        		});
		        });
        </script>
         <script>
    Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });
            $(document).ready( function() {
                $('#inputFecha').val(new Date().toDateInputValue());
            })
    </script>
@endsection