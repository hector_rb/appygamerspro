@extends('Profesor.menuProfesor')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Asistencia</h2>
<ol class="breadcrumb">
<li class="active">
<a href="{{URL::to('profesor/asistencia')}}"><strong>Asistencia</strong></a>
</li>
<li>
<a href="{{URL::to('profesor/asistencia/create')}}">Nuevas asistencias</a>
</li>
</ol>
</div>
</div>

 <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Administración de asistencias</h5>
                        <div class="ibox-tools">
                            
                        </div>
                    </div>
                    <div class="ibox-content">

<div class="row">
<div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
<p>Registros de asistencia.</p>
</div>
</div>

<div class="col-lg-4 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="idmateria">Materia-Grupo-Nivel</label>
<select name="idSubgrupo" id="selectSubgrupo" class="form-control">
<option value=""></option>
@foreach ($sg as $sg)
<option required value="{{$sg->idSubgrupo}}">{{$sg->nombreMateria}} - {{$sg->grado}}{{$sg->grupo}} - {{$sg->nombreNivel}} </option>
@endforeach
</select>
</div>
</div>



<div class="col-lg-2 col-md-2 col-md-2 col-xs-12">
<div class="form-group">
<label for="numevaluaciones">Periodo</label>
<select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
    <option></option>
</select>
</div>
</div>
<!--
<div class="col-lg-2 col-md-2 col-md-2 col-xs-12">
<div class="form-group">
<label for="numevaluaciones">Fecha</label>
<select type="text" name="fecha" id="selectFecha" required  class="form-control">
    <option></option>
</select>
</div>
</div>
-->
<div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
<div class="form-group">
<label for="fecha">Fecha</label>
<input type="date" name="fecha" id="selectFecha" class="form-control">
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
<div class="table-responsive">
<table class="table table-hover table-mail">

<tr class="unread">
<thead>
<th>Id</th>
<th>Alumno</th>
<th><p>Asistencias</p></th>
<th><p>Faltas</p></th>
<th><p>Retardos</p></th>
</thead>
<tbody id="rows">

</tbody>
</table>
</div>
</div>
{!!Form::close()!!}
		
</div>
</div>
                </div>
            </div>
            </div>
        </div>

<script >
            $("#selectSubgrupo").change(function(){
                    var idsg = this.value;

                    $('#selectPeriodos').find('option').not(':first').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getPeriodosProfesor')}}",
                        data: {id:idsg},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var op =$('<option>',{
                                    value: item.idPeriodo,
                                    text: item.nombrePeriodo
                                });
                                
                                
                                $('#selectPeriodos').append(op);
                           
                                });
                            }
                        });
                    
                });
             $("#selectPeriodos").change(function(){
                    var idP = this.value;
                    var idsg=$('#selectSubgrupo').val();
                    
                    $('#rows').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getAsistenciasProf')}}",
                        data: {idSubgrupo:idsg,periodo:idP},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var fila = $('<tr>');

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.A
                                });

                                var falta=$('<td>',{
                                    text:item.F
                                });

                                var ret=$('<td>',{
                                    text:item.R
                                });
                                
                                
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                fila.append(falta);
                                fila.append(ret);
                                
                                $('#rows').append(fila);
                           
                                });
                            }
                        });
                    $.ajax({
                        method: 'get',
                        url: "{{('getFechasAsistencia')}}",
                        data: {id:idsg,periodo:idP},
                        dataType: 'json',
                        success: function (data) {
                           $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var op =$('<input>',{
                                    value: item.fecha,
                                    text: item.fecha
                                });
                                
                                
                                $('#selectFecha').append(op);
                           
                                });
                            }
                        });
                });


             $("#selectFecha").change(function(){
                    var fecha= this.value;
                    var idP = $('#selectPeriodos').val();
                    var idsg=$('#selectSubgrupo').val();
                $('#rows').find('tr').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getFechasAsistencia2')}}",
                        data: {idSubgrupo:idsg,periodo:idP,fecha:fecha},
                        dataType: 'json',
                        success: function (data) {

                            $.each(data, function (i, item) {
                                //console.log(item.nombre);
                                var fila = $('<tr>');

                                var id =$('<td>',{
                                    text: item.idAlumno,
                                    
                                });

                                var nombre =$('<td>',{
                                    text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
                                });
                                
                                
                                var asis=$('<td>',{
                                    text:item.A
                                });

                                var falta=$('<td>',{
                                    text:item.F
                                });

                                var ret=$('<td>',{
                                    text:item.R
                                });
                                
                                
                                fila.append(id);
                                fila.append(nombre);
                                fila.append(asis);
                                fila.append(falta);
                                fila.append(ret);
                                
                                $('#rows').append(fila);
                           
                                });
                            }
                        });


                });

        </script>
@endsection