<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>appycollege.com | Registra tu escuela </title>
    <link rel="shortcut icon" href="img/favicon.ico">

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">


</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown" style="padding: 50px 20px 20px 20px">
        <div class="row">

            <div class="col-md-6">
                <h1 class="font-bold">Registro Institución AppyCollege</h1>
                <br><br><br>
                <center><img src="{{asset('img/acceso.png')}}" data-rjs="3" alt="Bold Logo" height="300px"></center>

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                    <br><br>
                    <center ><img src="{{asset('img/logo.png')}}" data-rjs="3" alt="Bold Logo" style="max-width: 100%"></center>
                    <br><br>
                    
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        
                        <div id="validacion">
                            <div class="form-group">
    
                                <div class="col-md-12"  style="display: none;">
                                    <input placeholder="Código de registro" type="text" class="form-control" name="codigoregistro" 
                                    id="codigoregistro">
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="email" type="email" placeholder="Email" class="form-control" name="email" required>
    
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <input type="hidden" name="tipo_user" value="1">
                            </div>
                        
                        </div>
                        
                        <div id="registro">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>
    
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group">
    
                                <div class="col-md-12">
                                    <input id="password-confirm" placeholder="Confirma tu contraseña" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            
                            
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    
                                <div class="col-md-12">
                                    <input id="name1" type="text" placeholder="Nombre de la Escuela" class="form-control" name="name_esc" value="{{ old('name_esc') }}" required autofocus>
                                    <input type="hidden" class="form-control" name="admin" value="2" required>
    
                                    @if ($errors->has('name_esc'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name_esc') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
    
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    
                                <div class="col-md-12">
                                    <input id="name2" type="text" placeholder="Nombre del Director" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
    
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Preescolar</label>
                                <div class="col-md-8">
    
                                 <input type="checkbox" value="1" class="form-group" name="nivel[]" > </div>
    
    
                            </div>
                            <div class="form-group">
    
                                <label class="col-md-4 control-label">Primaria</label>
                                <div class="col-md-8">
    
                                 <input type="checkbox" value="2" class="form-group" name="nivel[]"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Secundaria</label>
                                <div class="col-md-8">
    
                                 <input type="checkbox" value="3" class="form-group" name="nivel[]"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Preparatoria</label>
                                <div class="col-md-8">
    
                                 <input type="checkbox" value="4" class="form-group" name="nivel[]"> </div>
                            </div>
    
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Registrarme
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <hr/>
        <!-- <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div> -->
    </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>


    
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
            
        $('#registro').show(); 
        </script>


</body>

</html>
