<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>appycollege.com | Registra tu escuela </title>
    <link rel="shortcut icon" href="img/favicon.ico">

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">

</head>

<body class="gray-bg">
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                    <div class="row">

                        <form class="form-horizontal" method="POST" action="{{ url('admin/registroNuevo') }}" id="form" >
                                    {{ csrf_field() }}

            
                            <div class="col-md-6">
                                <div class="ibox-content">
                                    <center><img src="{{asset('img/logo.png')}}" data-rjs="3" alt="Bold Logo" style="max-width: 100%"></center>
                                    <br>
                                    
                                    

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">
                                                <!-- <input id="name" type="text" placeholder="Nombre de la Escuela" class="form-control" name="name_esc" value="{{ old('name') }}" required autofocus> -->
                                                <select name="tipo_user" id="tipo" class="form-control">
                                                    <option value="2">Tutor</option>                              
                                                </select>
                                                <input type="hidden" class="form-control" name="admin" value="2" required>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">
                                                <input id="name" type="text" placeholder="Tu nombre" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group" >

                                            <div class="col-md-12">
                                                <input type="text"  placeholder="Apellido Paterno" class="form-control" name="apepat" >
                                            </div>
                                        </div>

                                        <div class="form-group" >

                                            <div class="col-md-12">
                                                <input type="text" placeholder="Apellido Materno" class="form-control" name="apemat" >
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <input id="password-confirm" placeholder="Confirma tu contraseña" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>
                                        

                                        
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="ibox-content">
                                    <center><h1>Registro de Tutores</h1></center>
                                    <br>
                                    <br><br><br>
                                    <div class="form-horizontal">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">
                                                <input type="date" placeholder="Fecha de nacimiento: AAAA-MM-DD" class="form-control" name="fechanac" >
                                            </div>
                                        </div>

                                        <div class="form-group" id="dtel">
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Telefono" class="form-control" id="tel" name="tel" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Telefono celular" class="form-control" name="cel">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <select placeholder="Genero" class="form-control" name="genero" required>
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="clave">
                                            <div class="col-md-12">
                                                <input type="text" id="clave" placeholder="Clave de Registro"  class="form-control" name="clave" required="required" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit"  class="btn btn-primary">
                                                    Registrarme
                                                </button>
                                            </div>
                                        </div>
                                        
                                   </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <!-- <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div> -->
    </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>
    <!--swA-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>

    <script>
       /* $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');

            }, 1300);*/


           $('#tipo').change(function()
                {
                    var tipo = this.value;
                    if (tipo==2) //alumno
                    {
                        $("#dtel").show();
                        $("#dtel").attr("required",true);
                        $("#matricula").hide();
                    }
                    else
                    {
                        $("#dtel").hide();
                        $("#oculto").show();   
                        $("#matricula").attr("required",false);
                    }
                });

           $('#form').submit(function( event ) {
               
                //event.preventDefault();
                var cod=$('#clave').val();
                
                if ($('#tipo').val()==3) {
                    $.ajax({
                        method: 'get',
                        url: './valCod',
                        data:{codigo:cod,tipo:1},                        
                        async:false,
                        success: function (data) {
                                //console.log(item.nombre);
                                if(data==1)
                                {
                                    //$('#form').submit();
                                    return;
                                }
                                //console.log(data);

                        },
                        error: function (e) {
                            console.log(e);                        
                        }
                    });
                }
            });

    </script>

</body>

</html>
