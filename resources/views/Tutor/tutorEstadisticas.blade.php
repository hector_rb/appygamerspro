<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}">
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
    <!-- Gritter -->
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">
    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <!--cropper-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.min.css" />
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
     <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
        /* css juegos */
                 .card {
                 /* Add shadows to create the "card" effect */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                }

                /* On mouse-over, add a deeper shadow */
                .card:hover {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }

                /* Add some padding inside the card container */
                .container {
                    padding: 2px 16px;
                }
</style>
</head>

<body>
    @section('menu')
    <div id="wrapper">
        <meta type="hidden" name="csrf-token" content="{{csrf_token()}}">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element" style="text-align: center;"> <span>
                            <img alt="image" class="img-circle" id="profileImage" style="width: 100px; height: 100px;" src="{{asset('img/profile_pics/'.Auth::user()->imagen)}}" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">Tutor <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{URL::to('tutor/perfilTut')}}">Perfil</a></li>
                                <li class="divider"></li>
                                <!--<li><a href="login.html">Logout</a></li>-->
                            </ul>
                        </div>
                        <div class="logo-element">
                            MAC
                        </div>
                    </li>
                    <li>
                        <a href="{{URL::to('tutor/')}}"><i class="fa fa-align-justify"></i> <span class="nav-label">Inicio</span></a>
                    </li>                                    
                    <li>
                       <a href="{{URL::to('tutor/RegistrarAlumno')}}"><i class="fa fa-calendar"></i> <span class="nav-label">Registrar Alumno</span>  </a>
                    </li>
                    <li>
                        <a href="{{URL::to('tutor/estadisticas')}}"><i class="fa fa-envelope"></i> <span class="nav-label">Estadisticas</span></a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!--<form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>-->
        </div>
            <ul class="nav navbar-top-links navbar-right" id="header">

                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Salir
                    </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                </li>
                <!--<li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>-->
            </ul>
        </nav>
        </div>

    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Estadisticas
                    <small>
                        <i  class="fa fa-question-circle b" data-toggle="tooltip" data-placement="right"  title="En esta sección puedes consultar los juegos disponibles" aria-describedby="popover955887"></i> 
                    </small></h1>
                    <h3 class="font-bold c">En esta sección se puede manejar todo lo relacionado con los juegos</h3>
            </center>
        </form>
    </div>

    <!--
<div class="row">
    <div class="ibox-content">
          @foreach($tutelados as $alumno)
          <div class="ibox-content product-box">
              <div class="product-imitation">
                  <div class="product-desc">
                      <class="product-name"> {{$alumno[0]->nombreAlumno.' '.$alumno[0]->apepat.' '.$alumno[0]->apemat}}</a>
                      @foreach($alumno as $nombre)
                      <div class="small m-t-xs">
                       <p> {{$nombre->id_juego}}</p>
                   </div>
                </div>
                    @endforeach

           @endforeach

                </div>
            </div>
       </div>
    </div>
         -->

@foreach($tutelados as $alumno)
@if($loop->iteration % 4 == 0)
<div class="row">
@endif
 <div class="col-md-4">
            <div class="ibox">
                        <div class="ibox-content product-box">
                            <div class="product-desc">
                               <h2 style="color: #000000"> Nombre del Alumno:</h2>
                               <p style="font-size: 20px;color:#000000"> {{$alumno[0]->nombreAlumno.' '.$alumno[0]->apepat.' '.$alumno[0]->apemat}}</p>
                                 @foreach($alumno as $nombre)
                                 <hr>
                                <div class="small m-t-xs">
                                  <p style="font-size: 12px;color:#000000"> Juego: {{$nombre->nombre}} </p>
                                  <p style="font-size: 12px;color:#000000">Tiempo de juego : {{$nombre->tiempo}}</p>

                                </div>

@endforeach

                            </div>
                        </div>
                    </div>
                </div>
@if($loop->iteration % 4 == 0)
</div>
@endif
@endforeach

</div>


    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

    <!-- Flot -->
    <script src="{{ asset('js/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>

    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{ asset('js/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>


    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js')}}"></script>

    <!--cropper-->
    <script src="{{asset('js/canvas-to-blob.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.1/cropper.min.js"></script>

    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('js/plugins/chosen/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    @yield('content')
</body>