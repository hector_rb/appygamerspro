@extends('Tutor.menuTutor')
@section('content')
<html>
   <head>
   		<style type="text/css">
   			#cabecera { height: 300px; }
   			 .a{
            background-color: #E0E5E4;
            color: #34776A;
        }
         .b{
            background-color: #40AAB2;
            color: #FEF9E7;
        }
         .c{
            background-color: #A9CCE3;
            color: #2E86C1;
        }
         .d{
            background-color: #A3E4D7;
            color: #BA4A00;
        }
         .e{
            color: #6E2C00;
        }

         .g{
            color: #1C2833;
        }

         .f{
            background-color: #D89426;
            color: #EAF3F1;
        }
         .h{
            color: #DC7633;
        }
        .i{
            color: #F8C471;
        }
        .j{
            color: #2471A3;
        }
        .especial{
		   font-weight: normal;
		   font-style: italic;
		   padding: 2px;
		}

		.tamaño {
		  padding: 15px;
		  height: 650px;
		}
		.tamañousuarios {
		  overflow-y: auto;
		  height: 650px;
		}

		.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 15px;
  padding: 10px;
  width: 80px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 2px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 15px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

   		</style>
    </head>
    <body>
    <div class="row">
       <!--<div class="col-lg-12">-->

                <div class="ibox chat-view">
                    	<input type="hidden" name="userTutor" id="userTutor" value="{{$tutor->iduser}}">


                    <div class="ibox-content ">

                        <div class="row ">

                            <div class="col-md-9 ">
                                <div class="chat-discussion tamaño">
                                     <div id="cabecera">
                                     	<div class="chat-discussion" id="fondo">
                                     		<textarea rows="2" cols="50" style='visibility:hidden';></textarea>
                                     		<form>
                                     			<center>
		                                     		<h2 class="font-bold j" style="font-size:50px;">
		                                     			Bienvenido
		                                     		</h2>
		                                     		<h4 class="font-bold i" style="font-size:25px;">
		                                     			al
		                                     		</h4>
		                                     		<h1 class="font-bold h" style="font-size:80px;">
		                                     			Chat
		                                     		</h1>
		                                     		<h4 class="font-bold i" style="font-size:25px;">
		                                     			de
		                                     		</h4>
		                                     		<h2 class="font-bold j" style="font-size:50px;">
		                                     			AppyCollege.
		                                     		</h2>
		                                     	</center>
                                     		</form>
                                     	</div>
                                     </div>

                                </div>

                            </div>
                            <div class="col-md-3 ">
                                <div class="chat-users b tamañousuarios">
                                    <div class="users-list b">
                                    	<input type="hidden" name="idprofselec" id="idprofselec" value=0>
                                    	<div class="chat-user">
                                    		<h4 class="font-bold">Profesores de {{$sg[0]->grado}} {{$sg[0]->grupo}}</h4>
                                    		
                                    	</div>
                                    	@foreach($sg as $sg)
                                        <div class="chat-user">
                                        	<div class="col-md-3 ">
                                                <img src="{{asset('img/profile_pics/' .$sg->imagen)}}" height="40" width="40" class="img-circle circle-border m-b-md" alt="profile">
                                            </div>
	                                            <div class="chat-user-name">
	                                                <span class="pull-right label label f">{{$sg->nombreMateria}}</span>
	                                                <a onclick="chats({{$sg->profesor}}, {{$sg->iduser}}, '{{$sg->nombreTutor}} {{$sg->apepat}} {{$sg->apemat}}')" value="{{$sg->profesor}}{{$sg->iduser}}">

	                                                	<h4 class="font-bold">{{$sg->nombreprof}} {{$sg->profapepat}} {{$sg->profapemat}}
	                                                	</h4>
	                                                	<p class="font-bold especial">...{{$sg->ultimoMsj}}</p>
	                                                </a> 
	                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                                <div class="chat-message-form">

                                    <div class="form-group col-lg-9">

                                        <input class="form-control message-input " type="text" name="mensaje" placeholder="Escribir mensaje..." id="mensaje" ></input>
                                    </div>
                                    <!--<a class="btn-default" onclick="writeUserData()">enviar</a>-->
                                    <br>
                                    <button class="button" style="vertical-align:middle" onclick="writeUserData()" id="enviar"><span>Enviar</span></button>
                                </div>
                        </div>


                    </div>

                </div>
       <!--</div>-->

    </div>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-database.js"></script>    
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script>
	

    $('#enviar').click(function(){
    	var idsg = $('#idprofselec').val();
    	var men= $('#mensaje').val();
        $.ajax({
        	method: 'get',
        	url: "{{('../getChatsTutor')}}", 
        	data: {idprofselec:idsg,mensaje:men},
		    dataType: 'json',
		    success: function(data){
		    	console.log('si entro');
		    }
        });
        $('#mensaje').val('');
    });

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCK0guwmHRdNgXS_EaNf3wn-qmhhUA4Yy8",
    authDomain: "myappcollege-54c3b.firebaseapp.com",
    databaseURL: "https://myappcollege-54c3b.firebaseio.com",
    projectId: "myappcollege-54c3b",
    storageBucket: "myappcollege-54c3b.appspot.com",
    messagingSenderId: "798024302258"
  };
  firebase.initializeApp(config);

</script> 
<script type="text/javascript">
	// Get a key for a new Post.

	var referencia = "";

	var prof="";

	var tutor="";

	var hoy = new Date();
	var hora = hoy.getHours() + ':' + hoy.getMinutes();
	$('#fecha').html(hora);

	var database=firebase.database();
	function writeUserData(){
		var aaa=$('#mensaje').val(); 
		
		var newPostKey = firebase.database().ref().child(referencia).push().key;
		firebase.database().ref(referencia + "/" +newPostKey).set({
			foto:'ghjklñ.jpg', 
			hora:hora,
			mensaje: aaa,
			nombre:prof,
			type_mensaje: $('#userTutor').val()
		});

		//
	}

function aaa(){

	database.ref(referencia).once('value').then(function(snapshot) {
		//console.log(snapshot.val());
		   $.each(snapshot.val(),function (i, item){
		   	 //console.log(item.type_mensaje);
		   	 if(item.type_mensaje== $('#userTutor').val()){
				   		var div =$('<div>',
				   		{
				   			class:'chat-message left'
				   		});

				   		var div2 =$('<div>',
				   		{
				   			class:'message left d',
				   		});

				   		var label =$('<label>',
				   		{
				   			class:'message-author',
				   			text:'Tu',
				   		});

				   		var span =$('<span>',
				   		{
				   			class:'message-date d',
				   			text:item.hora,
				   		});
						var mensaje =$('<h4>',
		                    {
		                    	text:item.mensaje,
		                    	class:'font-bold e',
		                    });
						//&('#mensaje').value(item.mensaje);
						/*var mensajeoculto =$('<input>',
		                    {
		                    	value:item.mensaje,
		                    	name:'mensaje',
		                    	type:'hidden',
		                    });*/

					div2.append(span);
					div2.append(label);
					div2.append(mensaje);
					//div2.append(mensajeoculto);
					div.append(div2);

					
				   	$("#cabecera").append(div);
				   	abajo();
				   }
				   else{
				   	var div =$('<div>',
				   		{
				   			class:'chat-message left'
				   		});

				   		var div2 =$('<div>',
				   		{
				   			class:'message left c',
				   		});

				   		var label =$('<label>',
				   		{
				   			class:'message-author',
				   			text:item.nombre,
				   		});

				   		var span =$('<span>',
				   		{
				   			class:'message-date c',
				   			text:item.hora,
				   		});
						var mensaje =$('<h4>',
		                    {
		                    	text:item.mensaje,
		                    	class:'font-bold g'
		                    });
					div2.append(span);
					div2.append(label);
					div2.append(mensaje);
					//div2.append(mensajeoculto);
					div.append(div2);
					
				   	$("#cabecera").append(div);
				   	abajo();
				   }
				   
		   });

		});

}

function bbb(){

database.ref(referencia).on('value', function(snapshot) {
  //updateStarCount(postElement, snapshot.val());
  //console.log(snapshot.val());
  	$("#cabecera").empty();
  	$.each(snapshot.val(),function (i, item){
		   	 if(item.type_mensaje== $('#userTutor').val()){
				   		var div =$('<div>',
				   		{
				   			class:'chat-message left'
				   		});

				   		var div2 =$('<div>',
				   		{
				   			class:'message left d',
				   		});

				   		var label =$('<label>',
				   		{
				   			class:'message-author',
				   			text:'Tu:',
				   		});

				   		var span =$('<span>',
				   		{
				   			class:'message-date d',
				   			text:item.hora,
				   		});
						var mensaje =$('<h4>',
		                    {
		                    	text:item.mensaje,
		                    	class:'font-bold e',
		                    });
						var mensajeoculto =$('<input>',
		                    {
		                    	value:item.mensaje,
		                    	name:'mensaje',
		                    	type:'hidden'
		                    });

					div2.append(span);
					div2.append(label);
					div2.append(mensaje);
					div.append(div2);
					
				   	$("#cabecera").append(div);
				   	abajo();
				   }
				   else{
				   	var div =$('<div>',
				   		{
				   			class:'chat-message left'
				   		});

				   		var div2 =$('<div>',
				   		{
				   			class:'message left c',
				   		});

				   		var label =$('<label>',
				   		{
				   			class:'message-author',
				   			text:item.nombre,
				   		});

				   		var span =$('<span>',
				   		{
				   			class:'message-date c',
				   			text:item.hora,
				   		});
						var mensaje =$('<h4>',
		                    {
		                    	text:item.mensaje,
		                    	class:'font-bold g'

		                    });

						var sonido =$(
							{
								
							});
					div2.append(span);
					div2.append(label);
					div2.append(mensaje);
					div.append(div2);
					
				   	$("#cabecera").append(div);
				   	abajo();
				   }
				   
		   });

});

}

	 
function chats(a, b, c){

	prof=c;

	document.getElementById('idprofselec').value=a;
	$('#idprofselec').val(a);

	$('#cabecera').empty();

	if(referencia != ""){
		database.ref(referencia).off();
	}

	referencia = a + "" + b; 
	guardar(a, b);
	aaa();
	bbb();
	var div =$('<div>',
	{
		id:'abajo'
	});
	


}

function guardar(prof, tutor){

	$.ajax({
        	method: 'get',
        	url: "{{('../crearReferenciaTutor')}}", 
        	data: {idProfesor:prof, idTutor:tutor},
		    dataType: 'json',
		    success: function(data){
		    	console.log('si entro');
		    }
	});

}

function abajo()
{
	var cab=document.getElementById('cabecera');
	var div=document.createElement('div');
	cab.appendChild(div);
	div.scrollIntoView();
}

</script>
</body>
@endsection 