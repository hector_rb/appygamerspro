@extends('Tutor.menuTutor')
@section('content')

    <h1>Alerta escolar</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="/admin/alerta"> <i class="fa fa-inbox "></i>Registro de alertas</a></li>
                           
                        </ul>
                        <h5>Categories</h5>
                        <ul class="category-list" style="padding: 0">
                            <li><a href="#"> <i class="fa fa-circle text-navy"></i> Work </a></li>
                            <li><a href="#"> <i class="fa fa-circle text-danger"></i> Documents</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-primary"></i> Social</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-info"></i> Advertising</a></li>
                            <li><a href="#"> <i class="fa fa-circle text-warning"></i> Clients</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">
        <div class="row">
        <div class="col-lg-8 col-md-8 col-md-8 col-xs-12">
        <h2>Registro de alertas</h2>
        </div>
        
        <div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">        
<div class="ibox-content">
@include('Admin.alerta.search')
<div class="table-responsive">
<table class="table table-hover table-mail">
<tbody>
<tr class="unread">
@foreach ($alerta as $a)
<thead>
<td>{{$a->idAlerta}}</td>
<td>{{$a->titulo}}</td>
<td>{{$a->descripcion}}</td>
<td>{{$a->fecha}}</td>
<td><a href="" data-target="#modal-delete-{{$a->idAlerta}}" data-toggle="modal"><button class="btn btn-w-m btn-w-m btn-outline btn-danger btn-xs">Eliminar</button></td>
</thead>
@include('Admin.alerta.modal')
@endforeach
</tr>
</tbody>
</table>

</div>
</div>
</div>
{{$alerta->render()}}

        </div>
        </div>
        </div>

        
         <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

    <!-- iCheck -->
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

    <!-- SUMMERNOTE -->
    <script src="{{ asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
	
    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{ asset('js/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js')}}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js')}}"></script>
	

@endsection

