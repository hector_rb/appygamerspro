@extends('Admin.menuAdmin')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Expedientes</h2>
<ol class="breadcrumb">
<li>
<a href="{{URL::to('admin/expedientes')}}"><strong>Regresar</strong></a>
</li>
</ol>
</div>
<div class="col-lg-2">
</div>
</div>


<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    

    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css')}}">

      
    <style type="text/css">
	.swal-title {
		margin: 0px;
		font-size: 10px;
		box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
		margin-bottom: 28px;
	}
	.naranja {
		background-color: #f8ac59;
		border-color: #f8ac59;
		color: #FFFFFF;
		padding: 10px; 
	}
	.verde{
		background-color: #1ab394;
		border-color: #1ab394;
		color: #FFFFFF;
		padding: 10px; 
	}
	.azul1 {
		background-color: #1c84c6;
		border-color: #1c84c6;
		color: #FFFFFF;
		padding: 10px; 
	}
	.azul2 {
		background-color: #23c6c8;
		border-color: #23c6c8;
		color: #FFFFFF;
		padding: 10px; 
	}
	.rojo {
		background-color: #ed5565;
		border-color: #ed5565;
		color: #FFFFFF;
		padding: 10px; 
	}
	.tooltip-inner{
		max-width:600px;
	}
	.preguntas{
		width: 45%;
		position: relative;
	}
</style>

    </head>
<body>

	<div class="wrapper wrapper-content">
		<div class="row animated fadeInDown">
			<div class="col-lg-3">

				<div class="ibox-content text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$alumno[0]->nombreAlumno}} {{$alumno[0]->apepat}} {{$alumno[0]->apemat}}
                            </h2>
                            </div>
                            <img src="{{asset('img/profile_pics/' .$alumno[0]->imagen)}}" height="170" width="175" class="img-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="widget-text-box">
                        	<form><center>
                            <h1 class="font-bold">{{$alumno[0]->grado}} {{$alumno[0]->grupo}}</h1>
                            <span class="fa fa-calendar m-r-xs"></span>
                            <label>Fecha de nacimiento:</label>
                            <br>{{$alumno[0]->fechanac}}<br>
                            <span class="fa fa-phone m-r-xs"></span>
                            <label>Teléfono:</label>{{$alumno[0]->telefono}}
                            <br>
                            <span class="fa fa-envelope m-r-xs"></span>
                            <label>Correo:</label>{{$alumno[0]->correo}}
                            </center></form>
                        </div>
			</div>
			<div class="col-lg-9">
				<div class="ibox-content" id="trash" >
					<div class="well well-sm text-center">
						<label>SECCIÓN ACADÉMICA</label>
					</div>
					<div class="row text-center">
					<div class="col-lg-1">
					</div>
					<div class="col-lg-1">
						<i class="fa fa-book fa-5x"></i>
					</div>
					<div class="col-lg-4">
						<h5>Promedio general de calificaciones</h5>
						{{$calif}}

						
					</div>
					<div class="col-lg-1">
						<i class="fa fa-calendar fa-5x"></i>
					</div>
					<div class="col-lg-4">
						<h5>Asistencias del periodo</h5>
						{{$asistencia}}
					</div>
				</div>
	            </div>
			    <div class="ibox-content" id="trash" >
			    	<div class="well well-sm text-center"> 
			        	<label>INSIGNIAS</label>
			        </div>  
			        	<form> 
			        		<center>	
			            	<Table>
							      	<tr>
							      		<?php $con=1; 
							      		if($insignias != array())
							      		{
							      		?>
							      	@foreach($insignias as $i)
												<th><image src="{{ asset('img/insignias/'.$i->rutaImagen)}}"  height="150" width="125" >
													<br>
													{{$i->fecha}}
												</th>
											<?php
											if($con >= 4)
											{
												echo '</tr>';
												echo '<tr>';
												$con=1;	
											}
											else
											{
												$con++;
											}
											?>
									@endforeach
									<?php 
									}
									?>
									</tr>
							</Table>
						  </center>
						</form>
					</div>			
				<div class="ibox float-e-margins">
					<div class="ibox-content" id="trash" >
						<div class="well well-sm text-center">
							<label>DATOS PERSONALES</label>
						</div>
						<div class="row vertical-align">
							<div class="col-xs-1">
							</div>
							<div class="col-xs-2">
                                <i class="fa fa-user fa-5x"></i>
                        	</div>
	                        <div class="col-xs-4">
								<label>Nombre de tutor:</label>
								{{$alumno[0]->nombreTutor}} {{$alumno[0]->tutorapepat}} {{$alumno[0]->tutorapemat}}
								<br><span class="fa fa-envelope m-r-xs"></span>
								<label>Correo:</label>
								{{$alumno[0]->tutorcorreo}}
							</div>
							<div class="col-xs-4">
								<br><span class="fa fa-phone m-r-xs"></span>
								<label>Telélono del tutor:</label>
								{{$alumno[0]->tutortelefono}}
								<br><span class="fa fa-phone m-r-xs"></span>
								<label>Celular del tutor:</label>
								{{$alumno[0]->tutorcelular}}
							</div>
						</div>
					</div>
					<div class="ibox-content" id="trash" >
						<div class="well well-sm text-center">
							<label>CONOCIMIENTOS ADICIONALES</label>
						</div>
						<div class="row vertical-align">
							<div class="col-xs-1"></div>
							<div class="col-xs-2">
								<i class="fa fa-thumbs-up fa-5x"></i>
							</div>
							<br>
							@foreach($conocimientos as $c)
							<h5></h5>
							{{$c->conocimiento1}}
							<h5></h5>
							{{$c->conocimiento2}}
							<h5></h5>
							{{$c->conocimiento3}}
							@endforeach
							<br>
						</div>
					</div>
					<div class="ibox-content" id="trash" >
						<div class="well well-sm text-center">
							<label>DATOS MÉDICOS</label>
						</div>
						<div class="row vertical-align">
							<div class="col-xs-1"></div>
							<div class="col-xs-2">
								<i class="fa fa-gear fa-5x"></i>
							</div>
								<br>
								<?php
								$a=true;
								?>
								@foreach($datosmedicos as $d)
								<?php
								$a=false;
								?>
									<div class="col-xs-4">
										<form><center>
										<h5>Alergias:</h5>
										{{$d->alergias}}
										<h5>Enfermedades:</h5>
										{{$d->enfermedades}}
										<h5>Enfermedades crónicas:</h5>
										{{$d->enfCro}}
										<h5>Cirugías:</h5>
										{{$d->cirugias}}
										</center></form>
									</div>
									<div class="col-xs-4">
										<form><center>
										<h5>No. del seguro:</h5>
										{{$d->numSeguro}}
										<h5>Tipo de sangre:</h5>
										{{$d->tipoSangre}}
										<h5>Número de emergencia:</h5>
										{{$d->numEme}}
										</center></form>
									</div>
								@endforeach
								<br>
						</div>
						<div class="col-lg-9"></div>
						<a href="{{URL::to('admin/expedientes/datosmedicos?idAlumno='.$alumno[0]->idAlumno)}}">
							<?php 
							if($a)
							{echo "<button class='btn btn-w-m btn-w-m btn-outline btn-primary btn-xs'>Agregar datos adicionales</button></a>"; }
							else
							{echo "<button class='btn btn-w-m btn-w-m btn-outline btn-primary btn-xs' style='visibility:hidden';>Agregar datos adicionales</button></a>"; }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>    

</script>
@endsection
