@extends('Tutor.menuTutor')

@section('content')
<head>
	<link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
</head>
<body>
	 <div class="wrapper wrapper-content animated fadeInRight">
	        <div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox float-e-margins">
	                        <div class="ibox-title">
	                            <h2 id="titulo">Ver horarios</h2>
	                            
	                        </div>
	                        <div class="ibox-content">
	                            @if (count($errors)>0)
	                            <div class="alert alert-danger">
	                                <ul>
	                                @foreach ($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                                @endforeach
	                                </ul>
	                            </div>
	                            @endif
	                            <div class="form-horizontal">
	                                
	                                <br><br>
                                    <div class="form-group">
	                                	<div class="col-sm-12">
			                                <div class="table-responsive">
							                    <table class="table table-striped table-bordered table-hover dataTables-example" >
								                    <thead>
									                    <tr>
									                        <th>Nivel</th>
									                        <th>Grupo</th>
									                        <th>Ciclo</th>
									                        <th>Opciones</th>
									                    </tr>
								                    </thead>
								                    <tbody id="rows">
									                    @foreach($horarios as $h)
									                    	<tr>
									                    		<td>{{$h->nombreNivel}} </td>
									                    		<td>{{$h->grado}} {{$h->grupo}} </td>
									                    		<td>{{$h->nombreCiclo}} </td>
									                    		<td><a href="{{URL::action('horarioTutorController@show',['grupo'=>$h->idGrupo,'ciclo'=>$h->idCiclo])}}"><button class="btn btn-info">Ver</button></a></td>
									                    	</tr>
									                    @endforeach								                    
									                    
								                    </tbody>
								                    
							                    </table>
					                        </div>
					                    </div>
                                	</div>         
                            	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
		    <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

		    <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>

	        <script >
	        $(document).ready(function(){
	            $('.dataTables-example').DataTable({
	                pageLength: 25,
	                responsive: true,
	                dom: '<"html5buttons"B>lTfgitp',
	                language: {
						            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						        },
	                

	            });
	            if(<?= json_encode($horarios)?>!=null)
	            {
	            	var alum=<?= json_encode($horarios)?>;
	            	
	            	$('#titulo').text('Ver horarios ' + alum[0].nombreAlumno)
	        	}
	        });
	    	</script>
	    </body>
@endsection