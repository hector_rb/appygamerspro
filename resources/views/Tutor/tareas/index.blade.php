@extends('Tutor.menuTutor')
@section('content')
<head>
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        
        
         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        
    </style>
</head>
<body>
    <form>
        <center>
            <h1 class="font-bold b">Tareas</h1>
            <h3 class="font-bold c">En este apartado podrá ver las tareas del alumno y podrá descargar el material de apoyo en caso que este disponible.</h3>
        </center>
    </form
    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('tutor/tareas')}}"> <i class="fa fa-inbox "></i>Tareas del alumno</a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
<div class="col-lg-9 animated fadeInRight">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content">
                
            
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Descripción</th>
                                    <th>Materia</th>
                                    <th>Fecha de entrega</th>
                                    <th>Anexo</th>
                                </tr>
                            </thead>
                            <tbody id="rows">
                                @foreach($tareas as $t)
                                    <tr class="read">
                                        <td>{{$t->titulo}}</td>
                                        <td>{{$t->descripcion}}</td>
                                        <td>{{$t->nombreMateria}}</td>
                                        <td>{{$t->fechaentrega}}</td>
                                        <td><a href="{{('../anexo/tareas/').$t->anexo}}">{{$t->anexo}}</a></td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        </div>
            </div>
        </div>
</div>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    language: {
                                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                                },
                    buttons: [

                        {
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]

                });

            });
</script>
</body>
@endsection