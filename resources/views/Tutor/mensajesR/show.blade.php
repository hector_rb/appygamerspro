@extends('Tutor.menuTutor')
@section('content')
    
    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a href="{{URL::to('tutor/mensajesR')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <!--<li><a href="{{URL::to('admin/mensajesR/enviados')}}"> <i class="fa fa-envelope-o"></i> Enviados</a></li>-->
                            <!--<li><a href="{{URL::to('/admin/mensajesR/eliminados')}}"> <i class="fa fa-trash-o"></i>Papelera</a></li>-->
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>


        
        <div class="col-lg-9">
            <div class="mail-box-header">
                <!--<div class="pull-right tooltip-demo">
                    <a href="{{URL::to('admin/mensajesR')}}" class="btn btn-white btn-sm" data-toggle="" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Regresar</a>
                    <a href="" class="btn btn-white btn-sm" data-toggle="" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
                    <a href="" class="btn btn-white btn-sm" data-toggle="" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
                </div>-->
                <h2>
                    Ver mensaje
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">


                    <h3>
                        <span class="font-normal">Asunto: </span>{{$mensajes[0]->Asunto}}
                    </h3>
                    <h5>
                        <span class="pull-right font-normal">{{$mensajes[0]->fecha}}</span>
                        <span class="font-normal">De: </span>{{$mensajes[0]->destinatario}}|{{$mensajes[0]->usuarios}}
                    </h5>
                </div>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <p><h4>{{$mensajes[0]->Mensaje}}</p>
                </div>
                <div class="mail-attachment">
                    <h4>
                        <span><i class="fa fa-paperclip"></i>archivos adjuntos</span>
                        <a href="{{asset('archivo/mensajes/'.$mensajes[0]->anexo)}}">{{$mensajes[0]->anexo}}</a>
                    </h4>
                </div>
            </div>
        </div>
    </div>

@endsection
