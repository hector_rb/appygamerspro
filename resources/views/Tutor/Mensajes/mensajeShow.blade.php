@extends('Tutor.menuTutor')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                             <!-- Muestra el numero de mensajes sin ver-->
                            <li><a href="{{URL::to('tutor/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('tutor/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>


<div class="col-lg-9 animated fadeInRight">
<div class="mail-box-header">
    <div class="pull-right tooltip-demo">
        
    </div>
    <h2>
        {{$mensaje->Asunto}}   <!-- Muestra el asunto del mensaje-->
    </h2>
    <div class="mail-tools tooltip-demo m-t-md">


        <h3>
            <span class="font-normal">Asunto: </span>{{$mensaje->Asunto}}  <!-- Muestra el asunto del mensaje-->
        </h3>
        <h5>
            <span class="pull-right font-normal">{{$mensaje->created_at}}</span> <!-- Muestra cuando se creo el mensaje-->
            <span class="font-normal">De: </span>  <!-- Muestra quien redacto el mensaje-->
            @foreach ($variable as $variable)
                {{$variable->Nombre}}  <!-- obtiene quien envio el mensaje-->
            @endforeach
            <br>
            <br>

            <span class="font-normal">Para: </span>
            @foreach ($variable2 as $variable)
                {{$variable->Nombre}} <!-- obtiene el destinatario del mensaje-->
            @endforeach

        </h5>
    </div>
</div>
    <div class="mail-box">
    <div class="mail-body">
        <p> {{$mensaje->Mensaje}} </p> <!-- muestra el mensaje-->
    </div>
</div>
</div>
</div>

@endsection
