@extends('Tutor.menuTutor')
@section('content')

    <h1>Bienvenido al sistema de mensajes</h1>
    <br>

    <div class="row">

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <div class="space-25"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                             <!-- Muestra el numero de mensajes sin ver -->
                            <li><a href="{{URL::to('tutor/mensajes/recibidos')}}"> <i class="fa fa-inbox "></i> Bandeja de entrada <span class="label label-warning pull-right">{{$sinver}}</span> </a></li>
                            <li><a href="{{URL::to('tutor/mensajes/eliminados')}}"> <i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                       
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-9 animated fadeInRight">
        <div class="mail-box-header">


            <h2>
                Bandeja de entrada
            
            </h2>
            
             <div class="form-group">
                                      
            <div class="mail-box">

            <table class="table table-hover table-mail" id='hue2'>
            <tbody id='hue'>
                 <tr>
                <td><strong>De</strong></td>
                <td><strong>Emisor</strong></td>
                <td><strong>Para</strong></td>
                <td><strong>Asunto</strong></td>
                <td> </td>
                <td><strong>Fecha</strong></td>
                <td><strong>Borrar</strong></td>
                </tr>
                @foreach($mensajes as $mensajes)
                @php
                if($mensajes->Visto!=0) //muestra si un mensaje ha sido leido o aun no
                    echo '<tr id="leido" class="read">' ;
                else
                    echo '<tr id="leido" class="unread">'
                @endphp


                    @php
                    
                     
                         if($mensajes->TipoEmisor==1) //en base al tipo de emisor se selecciona si el mensaje lo envio un administrador o un profesor
                        echo '<td> <span class="label label-danger pull-right">Admin</span>  </td>';
                        
                    else

                        echo '<td> <span class="label label-success pull-right">Profe</span></td>';
                    
                        switch ($mensajes->TipoEmisor) {// switch para obtener el nombre del emisor
                            case '1':
                            //$alumno->where('id',$mensajes->IdDestinatario);
                                foreach($alumno as $alumnos)//recorre la variable para encontrar el emisor
                                {
                                    if($alumnos->id==$mensajes->IdEmisor)
                                        echo '<td id="recibido" class="mail-ontact"> ' .$alumnos->Nombre.'</td>';
                                }

                            break;

                            case '3':
                                foreach($profesor as $profesores) //recorre la variable para obtener el emisor
                                {
                                    if($profesores->id==$mensajes->IdEmisor)
                                        echo '<td id="emisor" class="mail-ontact"> ' .$profesores->Nombre.'</td>';

                                }
                            break;

                        }
                        
                        
                         if($mensajes->TipoDestinatario==4||$mensajes->TipoDestinatario==6||$mensajes->TipoDestinatario==7)// en caso de que el tipo destinatario sea alguno de estos el mensaje fue enviado al alumno y se agrega la etiqueta
                        echo '<td> <span class="label label-warning pull-right">Alumno</span>  </td>';
                        
                    else

                        echo '<td> <span class="label label-info pull-right">Tutor</span></td>';//en caso contrario es un mensaje para tutor 
                        
                    @endphp
                    
                    <td class="mail-subject"><a href="{{ route('Muestra',['id' => $mensajes->id])}}">{{ $mensajes->Asunto }}</a></td>  <!-- Muestra el asunto del mensaje-->
                   <td class=""></td>
                   <td>{{ $mensajes->created_at }}</td> <!-- Muestra cuando fue creado el mensaje-->
                   <td class="check-mail">
                       <a class="btn btn-w-m btn btn-w-m btn-outline btn-danger btn-xs" href="{{ route('PapeleraTutor',['id' => $mensajes->id] )}}" >Borrar</a>  <!-- permite borrar el mensaje-->
                   </td>

                </tr>
                @endforeach
            </tbody>
            </table>


            </div>
        </div>
    </div>
        </div>
        
        
        
        <script>
        
        
          $(document).ready(function() {

		    		$('#TipoDestinatario').change(function(){
		    		    var TipoDes = this.value;
		    		    $("#hue").empty()
		    			$.ajax(
                                {
                                        method: 'get',
                                        url: "{{('../getMensajes')}}",
                                        data: {idalumno:TipoDes},
                                        dataType: 'json',
                                        success: function (data)
                                        {
                                           
                                            $mensajes=data;
                                            $("#hue").append(
                                             "<tr>"+
                                            "<td><strong>De</strong></td>"+
                                            "<td><strong>Emisor</strong></td>"+
                                            "<td><strong>Para</strong></td>"+
                                           "<td><strong>Asunto</strong></td>"+
                                            "<td> </td>"+
                                            "<td><strong>Fecha</strong></td>"+
                                            "<td><strong>Borrar</strong></td>"+
                                            "</tr>");
                                            
                                            $.each( $mensajes, function( i, mensajes ) 
                                            {
                                                 if(mensajes.Visto!=0)
                                                    $("#hue").append('<tr id="leido" class="read">');
                                                else
                                                    $("#hue").append('<tr id="leido" class="unread">');
                                                    
                                                    
                                                if(mensajes.TipoEmisor==1)
                                                     $("#hue").append('<td> <span class="label label-danger pull-right">Admin</span>  </td>');
                                                    
                                                else
                                                     $("#hue").append('<td> <span class="label label-success pull-right">Profe</span></td>');
                                                     
                                                if(mensajes.TipoEmisor==1)
                                                   
                                                    $("#hue").append('<td id="recibido" class="mail-ontact"> Administrador</td>');
                                                              
                                                if(mensajes.TipoEmisor==3)
                                                     $("#hue").append('<td id="recibido" class="mail-ontact"> Profesor</td>');
                                                     
                                                     
                                                if(mensajes.TipoDestinatario==4||mensajes.TipoDestinatario==6||mensajes.TipoDestinatario==7)
                                                    $("#hue").append('<td> <span class="label label-warning pull-right">Alumno</span>  </td>');
                                                    
                                                else
                                                   $("#hue").append('<td> <span class="label label-info pull-right">Tutor</span></td>');
                                                   
                                                                            
                                                $("#hue").append('<td class="mail-subject"><a href "http://myappcollege.com/MyAppCollege/public/tutor/mensajes/recibidos/show/'+mensajes.id+'"> '+mensajes.Asunto+'</a></td>');
                                                $("#hue").append('<td class=""></td>');
                                                $("#hue").append('<td>'+mensajes.created_at+'</td>');
                                               // $("#hue").append('<td class="check-mail">');
                                                $("#hue").append('<a class="btn btn-w-m btn btn-w-m btn-outline btn-danger btn-xs" href="" >Borrar</a>');
                                                $("#hue").append('</td>');
                                                 $("#hue").append('</tr>');
                                            });
                                           
                                        }
                                    });
		    		});
          });
       
        </script>

    
    
    <script>
        
        
        
        
        
        

@endsection
