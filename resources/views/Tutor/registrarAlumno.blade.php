@extends('Tutor.menuTutor')

@section('content')
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
</head>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-title">
                    <h2>Registrar un tutelado</h2>                            
                </div>
                <div class="ibox-content">
                    <div class="row">

                        <form class="form-horizontal" id="form" method="POST" action="{{ route('registroA') }}">
                                    {{ csrf_field() }}
            
                            <div class="col-md-6">
                                <div class="ibox-content">
                                    
                                    <br>
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">

                                                <input type="hidden" class="form-control" name="admin" value="2" required>
                                                <input id="name" type="text" placeholder="Nombre" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group" >

                                            <div class="col-md-12">
                                                <input type="text"  placeholder="Apellido Paterno" class="form-control" name="apepat" >
                                            </div>
                                        </div>

                                        <div class="form-group" >

                                            <div class="col-md-12">
                                                <input type="text"  placeholder="Apellido Materno" class="form-control" name="apemat" >
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <input id="password-confirm" placeholder="confirma tu contraseña" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>
                                        

                                        
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="ibox-content">
                                    <br>
                                    <div class="form-horizontal">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                            <div class="col-md-12">
                                                <input type="date" placeholder="Fecha de nacimiento" class="form-control" name="fechanac" >
                                            </div>
                                        </div>


                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <input type="text" placeholder="Telefono" class="form-control" name="tel" required >
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                                <input type="hidden" placeholder="Matrícula" class="form-control" name="matricula" value="1" required >
                                            </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <select placeholder="Genero" class="form-control" name="genero" required>
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Femenino</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" id="oculto" >

                                            <div class="col-md-12">
                                                <input type="text" id="clave" placeholder="Clave del grupo" class="form-control" name="clave" >
                                                <input type="hidden" name="tipo_user" value="4" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Registrar
                                                </button>
                                            </div>
                                        </div>
                                   </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <!-- <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div> -->
    </div>
     <!--swA-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>



    <script>
       /* $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');

            }, 1300);*/


          /* $('#tipo').change(function()
                {
                    var tipo = this.value;
                    if (tipo==2)
                    {
                        $("#oculto").hide();
                        $("#clave").attr("required",false);
                    }
                    else
                    {
                        $("#oculto").show();   
                        $("#clave").attr("required",true);
                    }
                });*/
            $('#form').submit(function( event ) {              
                var cod=$('#clave').val();
                
            
                    $.ajax({
                        method: 'get',
                        url: './valCodTutor',
                        data:{codigo:cod,tipo:2},
                        async:false,
                        success: function (data) {
                                //console.log(item.nombre);
                                if(data==1)
                                {
                                    return;
                                }
                                //console.log(data);
                                if(data==0)
                                {
                                    event.preventDefault();
                                    swal("La clave del grupo no existe");                                    
                                }

                        },
                        error: function (e) {
                            console.log(e); 
                            event.preventDefault();                       
                        }
                    });
                
            });
    </script>
@endsection
