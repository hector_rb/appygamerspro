@extends('Tutor.menuTutor')
@section('content')
<style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>

  <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
  <script src="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>

<div class="container">
    <div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Insignias
                </h1>
                <h3 class="font-bold c">Visualización de las insignias del alumno.</h3>
            </center>
        </form>
    </div>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Individuales</a></li>
    <li><a data-toggle="tab" href="#menu1">Grupales</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <!--//////////INDIVIDUALES/////////////////-->
      <Table>
      	<tr>
      		<?php $con=1; 
      		if($insignias != array())
      		{
      		?>
      	@foreach($insignias as $i)
					<th><image src="{{ asset('img/insignias/'.$i->rutaImagen)}}"  height="210" width="210" >
						<br>
						Prof:{{$i->nombreprof}} {{$i->apepat}} {{$i->apemat}}
						<br>
						{{$i->nombreMateria}}
						<br>
						{{$i->fecha}}
					</th>
				<?php
				if($con >= 4)
				{
					echo '</tr>';
					echo '<tr>';
					$con=1;	
				}
				else
				{
					$con++;
				}
				?>
		@endforeach
		<?php 
		}
		?>
		</tr>
	  </Table>
    </div>
    <div id="menu1" class="tab-pane fade">
      <!--//////////////GRUPALES/////////////-->
      <Table>
      	<tr>
      		<?php $con=1; 
      		if($insignias != array())
      		{
      		?>
      	@foreach($insigniasgpo as $ig)
					<th><image src="{{ asset('img/insignias/'.$ig->rutaImagen)}}"  height="210" width="210" >
						<br>
						Profesor:{{$ig->nombreprof}} {{$ig->apepat}} {{$ig->apemat}}
						<br>
						{{$ig->nombreMateria}}
						<br>
						{{$ig->fecha}}
					</th>
				<?php
				if($con >= 4)
				{
					echo '</tr>';
					echo '<tr>';
					$con=1;	
				}
				else
				{
					$con++;
				}
				?>
		@endforeach
		<?php 
		}
		?>
		</tr>
	  </Table>
    </div>
  </div>



@endsection

