@extends('Tutor.menuTutor')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">        
        <div class="col-lg-12 animated fadeInRight">
        <div class="mail-box-header">
            <div class="pull-right tooltip-demo">
                {{-- <a href="mail_compose.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Reply</a>
                <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
                <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a> --}}
            </div>
            <h2>
                {{$aviso->asunto}}
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">


                <h3>
                    <span class="font-normal">Asunto: </span>{{$aviso->asunto}}
                </h3>
                <h5>
                    <span class="pull-right font-normal">{{$aviso->fecha}}</span>
                    <span class="font-normal">Para: </span>
                    	@if($aviso->destinatario==1)
                            General
                        @endif
                        @if($aviso->destinatario==2)
                            Tutores
                        @endif
                        @if($aviso->destinatario==3)
                            Profesores
                        @endif
                        @if($aviso->destinatario==4)
                            Alumnos
                        @endif
                </h5>
            </div>
        </div>
            <div class="mail-box">


            <div class="mail-body">
                {!!html_entity_decode($aviso->cuerpo)!!}
            </div>
                
                   
                    <div class="clearfix"></div>


            </div>
        </div>
    </div>
</div>

@endsection