@extends('Tutor.menuTutor')

@section('content')
<head>
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        
        
         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        
    </style>
</head>
<body>
    <form>
        <center>
            <h1 class="font-bold b">Avisos</h1>
            <h3 class="font-bold c">En esta seccion se visualizan los avisos enviados por la escuela</h3>
        </center>
    </form>
<div class="wrapper wrapper-content">
        <div class="row">
            
            <div class="col-lg-12 animated fadeInRight">
                <div class="ibox-content">

                 <table class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                    <tr>
                        <th></th>
                        <th>Asunto</th>
                        <th>Destinatarios</th>
                        <th class="text-right mail-date">Fecha</th>
                    </tr>
                </thead>
                <tbody>
                @if($avisos!=null)
                    @foreach($avisos as $a)
                        <tr class="read">
                            
                            <td align="right"><a href="{{URL::action('avisosTutorController@show',['id'=>$a->idAviso])}}"
                                title="Ver aviso"><button class="btn btn-white btn-sm fa fa-bell a"></button></a>
                           </td>
                            <td class="mail-subject">{{$a->asunto}}</td>
                            @if($a->destinatario==1)
                                <td>General</td>
                            @endif
                            @if($a->destinatario==2)
                                <td>Tutores</td>
                            @endif
                            @if($a->destinatario==3)
                                <td>Profesores</td>
                            @endif
                            @if($a->destinatario==4)
                                <td>Alumnos</td>
                            @endif
                            @if($a->destinatario==5)
                                <td>Todos los grupos</td>
                            @endif
                            @if($a->destinatario==6)
                                <td>Grupo {{$a->grado}}{{$a->grupo}}</td>
                            @endif
                            <td class="text-right mail-date">{{$a->fecha}}</td>
                        </tr>
                        @include('Admin.avisos.modal')
                    @endforeach
                
                
                </tbody>
                </table>
                @endif

                </div>
            </div>
        </div>
        </div>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 10,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    language: {
                                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                                },
                    buttons: [

                        {
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]

                });

            });
</script>
</body>
@endsection