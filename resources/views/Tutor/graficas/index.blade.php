@extends('Tutor.menuTutor')
@section('content')
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }
        .b{
            background-color: #F2F2F2;
            color: #819FF7;
        }
        .c{
            background-color: #F2F2F2;
            color: #81DAF5;
        }
        .d{
            background-color: #F2F2F2;
            color: #04B486;
        }
        .e{
            background-color: #0489B1;
            color: #FFFFFF;
        }
        .f{
            background-color: #A9A9F5;
            color: #FFFFFF;
        }
        .g{
            background-color: #F2F2F2;
            color: #04B486;
        }
        .h{
            background-color: #F2F2F2;
            color: #04B4AE;
        }
        
        .sobre{
            width: 500%;
            height: 500%;
            margin: 8px 0;
            border: none;
            color: black;
        }


    </style>
</head>
<body>

    <form>
        <center>
            <h1 class="font-bold h">Informacion general</h1>
                <h3 class="font-bold h">En este apartado podras optener un resumen del control de estadisticas de la institucion</h3>
        </center>
    </form>
<div class="container">
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="col-lg-11">

	<div class="ibox float-e-margins">
		<div class="row">
		        
                <div class="col-lg-5">
                        <a href="{{URL::to('tutor/avisosTutor')}}">
                            <div class="jumbotron c">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-bell fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$avisos}}</span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Avisos</h2>
                                        </center>
                                    </form>
                            </div>
                        </a>
                    </div>
                <div class="col-lg-5">
                        <a href="{{URL::to('tutor/eventos')}}">
                            <div class="jumbotron b">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-calendar fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$eventos}}</span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Eventos</h2>
                                        </center>
                                    </form>
                            </div>
                        </a>
                 </div>
        <br></br>
                <div class="col-lg-5">
                        <a href="{{URL::to('tutor/tareas')}}">
                            <div class="jumbotron d">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-edit fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$tareas}}</span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Tareas</h2>
                                        </center>
                                    </form>
                            </div>
                        </a>
                 </div>
                <div class="col-lg-5">
                        <a href="{{URL::to('tutor/mensajesR')}}">
                            <div class="jumbotron a">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-envelope-o fa-5x" style="font-size:160px"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span style="font-size:48px" class="label label-warning" >{{$sinver}}
                                            </span>
                                        </div>
                                    </div>
                                    <form>
                                        <center>
                                            <h2 class="font-bold">Mensajes</h2>
                                        </center>
                                    </form>
                            </div>

                        </a>
                    </div>
                  
        </div>
    </div>
</body>   
@endsection