@extends('Profesor.menuProfesor')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Información general</h2>
<ol class="breadcrumb">
<li class="{{URL::to('profesor/graficas')}}">
<a href="graficas"><strong>Calificaciones</strong></a>
</li>
<li class="active">
<a href="{{URL::to('profesor/graficas/create')}}">Asistencias</a>
</li>
</ol>
</div>
<div class="col-lg-2">
</div>
</div>
<div class="container">
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="col-lg-11">

	<div class="ibox float-e-margins">
		<div class="row">
                <div class="col-lg-4">
                    <div class="ibox-content">
                        <form><center><h1>2</h1>
                        <p><a href="" target="_blank" class="btn btn-info btn-lg" role="button">Avisos</a>
                        </p>
                        </center></form>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ibox-content">
                        <form><center><h1>5</h1>
                        <p><a href="{{URL::to('profesor/eventos')}}" target="_blank" class="btn btn-primary btn-lg" role="button">Eventos</a>
                        </p>
                        </center></form>
                    </div>
                </div>
              <div class="col-lg-4">
                    <div class="ibox-content">
                        <form><center><h1>4</h1>
                        <p><a href="{{URL::to('profesor/mensajes')}}" target="_blank" class="btn btn-warning btn-lg" role="button">Mensajes</a>
                        </p>
                        </center></form>
                    </div>
                </div>
                  
        </div>
    </div>
                
		<div class="ibox float-e-margins">
				<div class="ibox-title" id="dataTables-example">
				<h5>Gráficas 1.-Control de calificaciones de la Materia {{$sg->nombreMateria}} del grupo {{$sg->grado}} {{$sg->grupo}}</h5>
				</div>
			<div class="ibox-content" >
		<!--//////////////////////////////////////////////-->
		<div class="ibox-content">

                                <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                                    </div>
                                    <br></br>
                                    <p>Gráfica de promedio de calificaciones totales del ciclo por periodos.</p>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                        <li>
                                            <h2 class="no-margins " id="totalcalif"></h2>
                                            <small>Número de calificaciones totales del ciclo</small>
                                            <div class="stat-percent">100% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 100%;" class="progress-bar"></div>
                                            </div>  
                                        </li>
                                        <li>
                                            <h2 class="no-margins " id="aprobados"></h2>
                                            <small>Número de alumnos aprobados del ciclo</small>
                                            <div class="stat-percent">100% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 100%;" class="progress-bar"></div>
                                            </div> 
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
				

		<!-- ///////////////////////////////////////////////-->
		<div class="row">
			                <div class="col-lg-6">
			                    <div class="ibox float-e-margins">
			                        <div class="ibox-title">
			                            <h5>Grafica 2.- Calificaciones de alumnos
			                            </h5>
			                        </div>
			                        <div id="graficas1" class="ibox-content">
			                            <div id="ct-chart1" class="ct-perfect-fourth"></div>
			                        </div>
			                    </div>
			                </div>

		<!-- ///////////////////////////////////////////////-->
		  <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-warning pull-right">Materias</span>
                        <h5>Materias que imparte el profesor</h5>
                    </div>
                    <div class="ibox-content">
                    	<div class="table-responsive">
								<table class="table table-hover table-mail">
									<tbody>
									<tr class="unread">
									<thead >
									<th>id</th>
									<th>Nombre de la materia</th>
									</thead>
									<tbody id="materias">
									</tbody>
									</tr>
									</tbody>
								</table>
							</div>
                    </div>
                </div>
            </div>
            </div>
            <!--/////////////////////////////////////////////////////-->
			<div class="row">	
					<div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Promedio</span>
                                <h5>Promedio por grupo</h5>
                            </div>
                            <div class="ibox-content">
                               <div class="table-responsive">
								<table class="table table-hover table-mail">
									<tbody>
									<tr class="unread">
									<thead >
									<th>Grupo</th>
									<th>Promedio</th>
									</thead>
									<tbody id="rows">
									</tbody>
									</tr>
									</tbody>
								</table>
							</div>
                            </div>
                        </div>
                    </div>
                
            <!--////////////////////////////////////////////-->
          
            			
		             <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Sobresaliente</span>
                                <h5>Alumno sobresaliente</h5>
                            </div>
                            <div class="ibox-content">
                            	<div class="table-responsive">
									<div class="table-responsive">
										<table class="table table-hover table-mail">
											<tbody >
											<tr class="unread">
											<thead >
											<th>id</th>
											<th>Alumno</th>
											<th>Promedio</th>
											</thead>
											<tbody id="rows1">
											</tbody>
											</tr>
											</tbody>
										</table>
									</div>
								</div>

                            </div>
                        </div>
                    </div>
			<!-- ///////////////////////////////////////////////-->

                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Regular</span>
                                <h5>Alumno regular</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
							<div class="table-responsive">
								<table class="table table-hover table-mail">
									<tbody >
									<tr class="unread">
									<thead >
									<th>id</th>
									<th>Alumno</th>
									<th>Promedio</th>
									</thead>
									<tbody id="rows2">
									</tbody>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
                            </div>
                        </div>
                    </div>
                </div>  
		<!-- ///////////////////////////////////////////////-->


            </div>
				
		
			  </div>
		 </div>


		</div>
	</div>
</div>		 

<script>
var promedios=<?= json_encode($califs)?>;
console.log(promedios);

		        $('#rows').find('tr').remove();
		    		$.ajax({
		                method: 'get',
		                url: "{{('getPromedioGrupo')}}",
		                data: {},
		                dataType: 'json',
		                success: function (data) {
		                		var promedio=0;
		                		var contador=0; 
		                		var grado;
		                		console.log("Data: " + data);
		                		if(data==0)
		                		{
		                			console.log('error');
		                		}
		                		else
		                		{
				                    $.each(data, function (i, item) {
				                    	$.each(item, function (i2, item2) {
					                        console.log(item2.calif);
					                        promedio=promedio+item2.calif;
					                        contador++;
					                        grado=item2.grado;
					                        grupo=item2.grupo;
					                        //aqui se calcula
					                    });
			                		});
		                		
			                		promedio=promedio/contador;
			                        var fila = $('<tr>');

			                        var Grupo =$('<td>',{
			                        	text: grado + ' ' +grupo
	 		                        }); 		                        
			                        var calif =$('<td>',
			                        	{
			                        		text:promedio
			                        	});
			                        fila.append(Grupo);
			                        fila.append(calif);
			                        
			                        $('#rows').append(fila);
			                    }
		                		
		            		}
		        		});

</script>
<script>		            	

		            $('#rows1').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getPromedioAlumnoSobresaliente')}}",
		                data: {},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.idAlumno,
		                        	
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
 		                        }); 		                        
		                        var calif =$('<td>',
		                        	{
		                        		text:item.calif
		                        	});
		                        
		                        fila.append(id);
		                        fila.append(nombre);
		                        fila.append(calif);
		                        
		                        $('#rows1').append(fila);
		                   
		                		});
		            		}
		        		});
</script>
<script>
		            $('#rows2').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getPromedioAlumnoRegular')}}",
		                data: {},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.idAlumno,
		                        	
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.nombreAlumno + ' ' + item.apepat + ' ' + item.apemat
 		                        }); 		                        
		                        var calif =$('<td>',
		                        	{
		                        		text:item.calif
		                        	});
		                        
		                        fila.append(id);
		                        fila.append(nombre);
		                        fila.append(calif);
		                        
		                        $('#rows2').append(fila);
		                   
		                		});
		            		}
		        		});
///////////////////////////////////////////////////////////////////////////////////////
</script>
<script>
$('#graficas1').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getGraficas1')}}",
		                data: {},
		                dataType: 'json',
		                success: function (data)
						{
							var alumno=[];
							var cal=[];
							var id=[];
		                    $.each(data, function (i, item)
							{
								cal[i]=item.calif
								id[i]=item.nombreAlumno
								alumno[i]=i;
		                    	//calif=item.calif;
				                var a=$('<p>',
				                		{
											text:i
				                		});
				                
		                	//$('#graficas1').append(a);

		                   	});

		                    var char = {
		                    	labels: [],
		                    	series:[[]]
		                    }

		                    for(var i = 0; i<= id.length; i++)
		                    {
		                    	char.labels.push(id[i]);
		                    	char.series[0].push(cal[i]);
		                    }

							new Chartist.Line('#ct-chart1', char, {
								fullWidth: true,
								chartPadding: {
									right: 40
								}
							});

							$('#graficas1').append(a);

		            		}
		        	});
</script>
<script>
		        	$('#materias').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getMaterias')}}",
		                data: {},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        var fila = $('<tr>');

		                        var id =$('<td>',{
		                        	text: item.idMateria,
		                        	
		                        });
		                        var nombre =$('<td>',{
		                        	text: item.nombreMateria
 		                        });
		                        
		                        fila.append(id);
		                        fila.append(nombre);
		                        
		                        $('#materias').append(fila);
		                   
		                		});
		            		}
		        		});
									        							
</script>
<script>
        $('#totalcalif').find('p').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getCalifCount')}}",
                        data: {},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.calif;
                                contador++;
                                });
                                suma=contador
                                var fila = $('<h2>');                                
                                
                                var calif=$('<h2>',{
                                    text:suma
                                });
                                
                                fila.append(calif);
                                  
                                 $('#totalcalif').append(fila);
                           
                                
                            }
                        });
    </script>
    <script>
        $('#aprobados').find('p').remove();
                    $.ajax({
                        method: 'get',
                        url: "{{('getCalifAprobCount')}}",
                        data: {},
                        dataType: 'json',
                        success: function (data) {
                                var contador = 0;
                                var suma= 0
                            console.log("data" + data)
                            $.each(data, function (i, item) {

                                suma=item.calif;
                                contador++;
                                });
                                suma=contador
                                var fila = $('<h2>');                                
                                
                                var calif=$('<h2>',{
                                    text:suma
                                });
                                
                                fila.append(calif);
                                  
                                 $('#aprobados').append(fila);
                           
                                
                            }
                        });
    </script>				
  <script>
        $(document).ready(function() {
            $('.chart').easyPieChart({
                barColor: '#f8ac59',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            $('.chart2').easyPieChart({
                barColor: '#1c84c6',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            // Colors 
				var color01 = '#00cde2';
				var color02 = '#ffb700';
				var color03 = '#7ac70c';
				var color04 = '#313541';
				var color05 = '#fc3232';
				var color06 = '#1cb0f6';
				var color07 = '#00c07f';

            var data3 = promedios;


            var dataset = [
                {
                    label: "Calificaciones",
                    data: data3,
                    color: "#58CBD7",
                    bars: {
                        show: true,
	                    align: 'center',
	                    barWidth: 0.5,
	                    fill: 1,
	                    lineWidth: 1
                    }

                }, 
            ];


            var options = {
                series: {
				bars: {
					show: true,
					barWidth: 0.7,
					align: "center"
				}

			},
			xaxis: {
                tickLength: 0,
                ticks: [
                    
                    [1, "Periodo 1"],
                    [2, "Periodo 2"],
                    [3, "Periodo 3"],
                    [4, "Periodo 4"],
                    [5, "Periodo 5"],
                    [6, "Periodo 6"]]
            },
            yaxis: {
                min: 0
            },

            grid: {
                hoverable: true,
                backgroundColor: {
                    colors: ["#fff", "#fff"]
                },
                borderWidth: {
                    top: 1,
                    right: 1,
                    bottom: 2,
                    left: 2
                },
                borderColor: {
                    top: "#e5e5e5", 
                    right: "#e5e5e5",
                    bottom: "#e5e5e5",
                    left: "#e5e5e5"
                }
            }
            };

            function gd(year, month, day) {
                return new Date(year, month, day).getTime();
            }

            var previousPoint = null, previousLabel = null;

            $.plot($("#flot-dashboard-chart"), [data3], options);

            var mapData = {
                "US": 298,
                "SA": 200,
                "DE": 220,
                "FR": 540,
                "CN": 120,
                "AU": 760,
                "BR": 550,
                "IN": 200,
                "GB": 120,
            };

            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },

                series: {
                    regions: [{
                        values: mapData,
                        scale: ["#1ab394", "#22d6b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });
        });
    </script>	 

@endsection