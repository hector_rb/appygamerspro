@extends('Tutor.menuTutor')
@section('content')
<head>
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css">
    <style>
        .tooltip-inner{
            max-width:600px;
            background: #2E2E2E;
        }

         .a{
            background-color: #F2F2F2;
            color: #81BEF7;
        }

         .b{
            color: #EB984E;
        }

        .c{
            color: #04B4AE;
        }

        .d{
            color: #34495E;
        }

        .e{
            color: #566573;
        }
        .f{
            color: #138D75;
        }
        
    </style>
</head>
<body> 
<div class="tooltip-demo">
        <form>
            <center>
                <h1 class="font-bold b">Asistencia
                </h1>
                <h3 class="font-bold c">Visualización de las asistencias del alumno.</h3>
            </center>
        </form>
    </div>


<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">

<div class="ibox-content">

<div class="row">
<div class="col-lg-12 col-md-12 col-md-12 col-xs-12">

</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-md-8 col-xs-12">
	<div class="form-horizontal">
        <div class="form-group">
        	

			<label class="col-sm-2 control-label">Periodo</label>
			<div class="col-sm-7">
				<select type="text" name="periodo" id="selectPeriodos" required  class="form-control">
					<option></option>
					@foreach($periodos as $p)
                    	<option value="{{$p->idPeriodo}}">{{$p->nombrePeriodo}}</option>
                    @endforeach
				</select> 
			</div>
        </div>
    </div>
    <p>Registros de asistencia, A = asistencia, F = falta, R = retardo </p>
<div class="table-responsive">
<table class="table table-striped table-bordered table-condensed table-hover">
<thead>

<th>Materia</th>
<th>Asistencias</th>
<th>Faltas</th>
<th>Retardos</th>
</thead>
<tbody id="rows">
	
</tbody>

</table>

</div>
</div>


</div>
</div>
                </div>
            </div>
            </div>
        </div>
        <script >
        	$("#selectAlum").change(function(){
            		var idsg = this.value;

		            $('#selectPeriodos').find('option').not(':first').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getPeriodosTutor')}}",
		                data: {id:idsg},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        var op =$('<option>',{
		                        	value: item.idPeriodo,
		                        	text: item.nombrePeriodo
		                        });
		                        
		                        
		                        $('#selectPeriodos').append(op);
		                   
		                		});
		            		}
		        		});
		        });

        	$("#selectPeriodos").change(function(){
            		var idP = this.value;
            		var idAlum= $("#selectAlum").val();
		            $('#rows').find('tr').remove();
		            $.ajax({
		                method: 'get',
		                url: "{{('getAsistenciaTutor')}}",
		                data: {id:idAlum, idP:idP},
		                dataType: 'json',
		                success: function (data) {
		                    $.each(data, function (i, item) {
		                        //console.log(item.nombre);
		                        var fila = $('<tr>');
		                        var materia =$('<td>',{
		                        	text: item.nombreMateria
		                        });
		                        var asis =$('<td>',{
		                        	text: item.A
		                        });
		                        var falta =$('<td>',{
		                        	text: item.F
		                        });
		                        var ret =$('<td>',{
		                        	text: item.R
		                        });	                        
		                        
		                        
		                        fila.append(materia);
		                        
		                        fila.append(asis);
		                        fila.append(falta);
		                        fila.append(ret);
		                        $('#rows').append(fila);
		                   
		                		});
		            		}
		        		});
		        });
        </script>
</body>
@endsection