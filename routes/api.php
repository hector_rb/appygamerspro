<?php

use Illuminate\Http\Request;

/*
** Ruta sin middleware para la autentificacion, en este tipo de ruta no es
** necesario enviar el token desde el usuario, para lograr la autentificacion.
** No son seguras para la comunicacion entre el usuario y servidor.
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::resource('PregGames','preguntasjuegoController');

Route::get('getPreguntasJuego', 'preguntasjuegoController@getPreguntasJuego');
Route::post('guardaRespuesta', 'preguntasjuegoController@guardaRespuesta');
Route::post('guardaEstadisticas', 'preguntasjuegoController@guardaEstadisticas');