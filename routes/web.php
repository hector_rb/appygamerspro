<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Redirect;

Route::get('/', function () {
    return Redirect::to('/login');
});


Route::delete('pregunta/{id}/borrar', 'adminCuestionarioController@destroy');

Route::post('uploadsCuestionario/save','adminCrearCuestionarioControllerr@altaCuestionario');


Route::get('tutor/RegistrarAlumno','TutorRegistroAlumnoController@index');
Route::get('tutor/getGrupo','TutorRegistroAlumnoController@getGrupo');
Route::post('getTutelados','perfilTutorController@getTutelados');
Route::post('tutor/actualizar','perfilTutorController@update');
Route::get('tutor/estadisticas','TutorEstadisticasController@index');
Route::post('tutor/juegoDetalle','juegoTutorDetallesController@index');






//games


Route::post('uploads/save','UploadsController@postSave');
//Route::post('uploadsMateria/save','UploadsMateriaController@postSave');
Route::post('uploadsMateria/save', ['uses' => 'UploadsMateriaController@postSave', 'as' => 'materia.save']);
Route::get('uploads/descargar','UploadsController@descargarPlantilla');
Route::get('uploads/descargarMateria','UploadsMateriaController@descargarPlantillaMateria');
Route::get('buscarCuestionario', 'adminCuestionarioController@buscar');

Auth::routes();

Route::get('registro', 'Auth\RegisterController@showRegistrationForm2')->name('registro');
Route::post('registro', 'Auth\RegisterController@register');
Route::get('valCod','Auth\RegisterController@valCod');
Route::get('codigo','Auth\RegisterController@codigo');
Route::get('/home', 'HomeController@index')->name('home');

//Marco juego



Route::group(['middleware' => ['auth','admin'], 'prefix'=>'admin'], function(){
    
    Route::resource('games','juegosController');
 
    
Route::get('getPreguntasCategoria','adminCuestionarioController@getPreguntasCategoria');
Route::post('validarCasos','adminCuestionarioController@validarCasos');
Route::post('editadoNuevas','adminCuestionarioController@editadoNuevasCuestionario');
Route::post('editadoAgregadas','adminCuestionarioController@editadoAgregadasCuestionario');
Route::post('eliminarPregunta','adminCuestionarioController@eliminarPregunta');
Route::post('editadoCuestionario','adminCuestionarioController@editadoCuestionario');
Route::get('editarCuestionario/{idR}','adminCuestionarioController@editarCuestionario')->name('cuestionario.editar');

Route::get('editarCuestionario','adminCuestionarioController@editarCuestionarioDos');

Route::get('getPreguntas','adminCuestionarioController@getPreguntas');
Route::get('verCuestionario','adminCuestionarioController@getCuestionario');
Route::post('agregarCuestionarioExcel','adminCuestionarioController@registrarExcel');
Route::get('agregarCuestionario','adminCuestionarioController@agregarCuestionario');
Route::post('altaCuestionario','adminCrearCuestionarioController@altaCuestionario');
Route::get('RegistroTutor','RegistroTutorController@index');
Route::get('RegistroAlumno','RegistroAlumnoController@index');

Route::post('altaTutor','RegistroTutorController@Registrar');
Route::post('altaAlumno','RegistroAlumnoController@Registrar');

Route::post('registroNuevo','RegistroNuevoController@Registrar');
Route::post('juegoDetalle','juegoDetallesController@index');


Route::get('AdministracionEscolar','adminEscolar@index');


    
    
    
    
    
    
    //preguntas Marco
    Route::resource('Preguntas','preguntasController');
    Route::get('Pregistradas','preguntasController@Pregistradas');
    Route::get('RegistraPregunta','preguntasController@RegistraPregunta');
    Route::get('getGruposPreguntas','preguntasController@getGruposPreguntas');



    // Fin preguntas
	Route::resource('/','adminDashController');
	Route::resource('RegistroInstitucion','registroInstitucionController');
	Route::get('detallesCalendario','eventosAdminController@detallesCalendario');
	Route::get('SiCalendario','eventosAdminController@SiCalendario');
	Route::resource('eventos','eventosAdminController');
	Route::get('saveEvent','eventosAdminController@event');
	Route::resource('InformacionGeneral', 'informacionGeneralController');
	Route::resource('grupos','gruposController');
	Route::resource('materias','materiasController');
	//-------------------------------------------------------------------
	Route::resource('tareas','TareaAdminController');
	Route::get('mensajesR/enviados','MensajesRController@Enviado');
	Route::get('mensajesR/verEnviados','MensajesRController@verEnviados');
	Route::get('mensajesR/papelera','MensajesRController@papelera');
	Route::get('mensajesR/verPapelera','MensajesRController@verPapelera');
	Route::resource('mensajesR','MensajesRController');
	Route::resource('asistencia','AsistenciaAdminController');
	//despues del login
	Route::resource('graficas','juegosController');
	//
	Route::get('contenidos/videos','ContenidosController@videos');
	Route::get('contenidos/vervideo','ContenidosController@vervideo');
	Route::resource('contenidos','ContenidosController');
	Route::get('encuestas/reenviar','EncuestasAdminController@reenviar');
	Route::resource('encuestas','EncuestasAdminController');
	Route::get('expedientes/altaAlumnos', 'Auth\registrarAlumno@showRegistrationForm5')->name('altaAlumnos');
	Route::post('expedientes/AltaAlumnos', 'Auth\registrarAlumno@register')->name('AltaAlumnos');
	Route::get('expedientes/datosmedicos','ExpedientesAdminController@datosmedicos');
	Route::get('expedientes/conocimientos','ExpedientesAdminController@conocimientos');
	Route::resource('expedientes','ExpedientesAdminController');
	Route::get('getFotoProfesores','GraficasAdminController@getFotoProfesores');
	Route::get('getPeriodosCalificadosAdmin','GraficasAdminController@getPeriodosCalificadosAdmin');
	Route::get('MenuAdmin','juegosController@index');
	Route::get('getPeriodosAdmin','AsistenciaAdminController@getPeriodosAdmin');
	Route::get('getAlumnoAsis','AsistenciaAdminController@getAlumno');
	Route::get('getAsistencia','AsistenciaAdminController@getAsistencia');
	Route::get('/getFechasAsistencia','AsistenciaAdminController@getFechasAsistencia');
	Route::get('/getFechasAsistencia2','AsistenciaAdminController@getFechasAsistencia2');
	Route::get('TotalPersonas','GraficasAdminController@TotalPersonas'
	);
    Route::get('getPromedioGrupo','GraficasAdminController@getPromedioGrupo');
	Route::get('getPromedioAlumnoSobresaliente','GraficasAdminController@getPromedioAlumnoSobresaliente');
	Route::get('getPromedioAlumnoRegular','GraficasAdminController@getPromedioAlumnoRegular'
	);
	Route::get('graficasCalif','GraficasAdminController@graficasCalif');
	Route::get('getGraficas1','GraficasAdminController@getGraficas1'
	);
	Route::get('getGraficas2','GraficasAdminController@getGraficas2'
	);
	Route::get('getMayorAsistencia','GraficasAdminController@getMayorAsistencia'
	);
	Route::get('getRetardo','GraficasAdminController@getRetardo'
	);
	Route::get('getFalta','GraficasAdminController@getFalta'
	);
	Route::get('getCalifCount','GraficasAdminController@getCalifCount');
	Route::get('getCalifAprobCount','GraficasAdminController@getCalifAprobCount');
	Route::get('getRetardo','GraficasAdminController@getRetardo');
	Route::get('getFalta','GraficasAdminController@getFalta');
	Route::get('TotalAsistencias','GraficasAdminController@TotalAsistencias');
	Route::get('getMayorAsistencia','GraficasAdminController@getMayorAsistencia');
	Route::get('getGruposEncuestas','EncuestasAdminController@getGruposEncuestas');
	Route::get('getContador','EncuestasAdminController@getContador');
	Route::get('getPreguntas','EncuestasAdminController@getPreguntas');
	//-------------------------------------------------------------------
	//Route::get('getGrados','materiasController@getGrados');
	Route::resource('aMaterias','profesor_materiaController');
	Route::get('getMaterias','materiasController@getMaterias');
	Route::resource('horario','horarioController');
	Route::get('getGrupos','horarioController@getGrupos');
	Route::get('getProf','horarioController@getProf');
	Route::get('getCiclo','horarioController@getCiclo');
	Route::get('saveHorario','horarioController@event');
	Route::get('horario/editar', 'horarioController@editar');
	
	Route::get('directorio/filtro','directorioController@filtro');
	Route::post('directorio/cambiarFiltro','directorioController@cambiarFiltro')->name('cambiarFiltro');
	Route::resource('directorio','directorioController');
	Route::post('saveImageDir','directorioController@saveImageDir');

	Route::get('getNivel','eventosAdminController@getNivel');
	
	Route::resource('ciclo','CicloController');
	Route::get('cicloCambiar','CicloController@cicloCambiar');
	Route::post('saveLogo','registroInstitucionController@saveImage');
	
	Route::resource('/perfilAdmin','perfilAdminController');
	Route::post('/saveImageAdmin','perfilAdminController@saveImage');
	
	Route::resource('adminEscolar', 'adminEscolar');
	Route::get('getPeriodos','adminEscolar@getPeriodos');
	Route::get('getMateriasT','adminEscolar@getMaterias');
	Route::get('getGruposT','adminEscolar@getGrupos');
	Route::get('saveGrupo','adminEscolar@saveGrupo');
	Route::get('saveMateria','adminEscolar@saveMateria');
	Route::get('savePeriodo','adminEscolar@savePeriodo');
	Route::get('saveGrado','adminEscolar@saveGrado');
	Route::get('saveCiclo','adminEscolar@saveCiclo');
	Route::get('setCiclo','adminEscolar@setCiclo');
	Route::get('setMatProf','adminEscolar@setMatProf');
	Route::get('updatePeriodo','adminEscolar@updatePeriodo');
	Route::get('updateMateria','adminEscolar@updateMateria');
	Route::get('updateGrupo','adminEscolar@updateGrupo');
	Route::get('deleteGrado','adminEscolar@deleteGrado');
	Route::get('deleteGrupo','adminEscolar@deleteGrupo');
	Route::get('getGrados','adminEscolar@getGrados');
	
	Route::resource('avisos','avisosAdminController');
	Route::get('registroProfesor', 'Auth\registrarUsuarios@showRegistrationForm4')->name('registroProfesor');
	Route::post('registroProfesor', 'Auth\registrarUsuarios@register');
	Route::get('adminUsuarios/ProfesoresEliminados', 'adminUsuariosController@ProfesoresEliminados');
	Route::get('adminUsuarios/reasignarMaterias', 'adminUsuariosController@reasignarMaterias');
	Route::post('reactivar', 'adminUsuariosController@reactivar');
	Route::resource('adminUsuarios', 'adminUsuariosController');

	Route::get('mensajes/getDestinatario','mensajes@getDestinatario');
	Route::get('getAlumnos','mensajes@getAlumnos');


	Route::get('mensajes/eliminados','mensajes@eliminados');
	Route::get('getDestinatario','mensajes@getDestinatario');
	Route::put('mensajes/recibidos/{id}', ['as' => 'updateAdmin', 'uses'=>'mensajes@update']);
    Route::get('mensajes/recibidos/show/{id}', ['as' => 'MuestraAdmin', 'uses'=>'mensajes@show']);
	Route::get('mensajes/recibidos','mensajes@recibidos');
	Route::get('mensajes/enviados','mensajes@enviados');
	Route::resource('mensajes','mensajes');
	Route::post('mensajes/recibidos/search', ['as' => 'BuscaAdmin', 'uses'=>'mensajes@search']);
	Route::post('mensajes/create/store', ['as' => 'AltaMensajeAdmin', 'uses'=>'mensajes@store']);
	Route::get('mensajes/recibidos/destroy/{id}', ['as' => 'mensajes/destroy', 'uses'=>'mensajes@destroy']);
	Route::get('mensajes/recibidos/edit/{id}', ['as' => 'EditarAdmin', 'uses'=>'mensajes@edit']);
    Route::get('/mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraAdmin', 'uses'=>'mensajes@papelera']);

    

	Route::resource('/alerta','AlertaController');
	
});

Route::group(['middleware' => ['auth','tutor'], 'prefix'=>'tutor'], function(){

	Route::get('registroA', 'Auth\registrarAlumno@showRegistrationForm3')->name('registroA');
	Route::post('registroA', 'Auth\registrarAlumno@register');
	Route::get('valCodTutor','Auth\registrarAlumno@valCod');
	Route::resource('/horarioTutor','horarioTutorController');
	Route::resource('/chat','ChatTutorController');
	Route::get('getChatsTutor','ChatTutorController@getChatsTutor');
	Route::get('/crearReferenciaTutor','ChatTutorApiController@crearReferenciaTutor');
	Route::get('getHorarioTutor','horarioTutorController@getHorario');

	Route::resource('/','tutorDashController');
	Route::resource('/tareas','TareaTutorController');
	Route::get('getTareasTutor','TareaTutorController@getTareas');

	Route::resource('/asistencia','AsistenciaTutorController');
	Route::get('getAsistenciaTutor','AsistenciaTutorController@getAsistencia');
    
	Route::resource('/calificaciones','CalificacionesTutorController');
	Route::get('getCalificacionesTutor','CalificacionesTutorController@getCalificaciones');
	Route::get('getPeriodosTutor','CalificacionesTutorController@getPeriodos');
    Route::resource('/avisosTutor','avisosTutorController');
    
	Route::resource('/alerta','AlertaTutorController');
	Route::resource('/eventos','eventosTutorController');
	Route::resource('/graficas','GraficasTutorController');
    Route::resource('/insignias','InsigniasTutorController');
    Route::resource('/encuestas','RespuestasTutorController');
    Route::resource('/expedientes','ExpedientesTutorController');
    Route::resource('/mensajesR','MensajesRTutorController');
	Route::resource('/perfilTut','perfilTutorController');
	Route::post('/saveImageTut','perfilTutorController@saveImage');

    Route::post('getTutelados','perfilTutorController@getTutelados');
	Route::post('setTutelado','perfilTutorController@setTutelado');

    Route::get('getMensajes','TutorMensajes@getMensajes');
	Route::get('/mensajes/recibidos','TutorMensajes@recibidos');
	Route::get('/mensajes/','TutorMensajes@index');
	Route::post('/mensajes/recibidos/search', ['as' => 'Busca', 'uses'=>'TutorMensajes@search']);
	Route::get('/mensajes/recibidos/show/{id}', ['as' => 'Muestra', 'uses'=>'TutorMensajes@show']);
	Route::get('/mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraTutor', 'uses'=>'TutorMensajes@papelera']);
	Route::get('/mensajes/eliminados/desaparece/{id}', ['as' => 'Desaparece', 'uses'=>'TutorMensajes@desaparece']);
	Route::get('mensajes/eliminados','TutorMensajes@eliminados');
    Route::get('getPreguntasTutor','RespuestasTutorController@getPreguntasTutor');

});

    Route::get('profesor/mensajes/getNivelP','ProfesorMensaje@GetNivelP');
    Route::get('profesor/mensajes/getGradosP','ProfesorMensaje@getGradosP');
    Route::get('profesor/mensajes/getGruposP','ProfesorMensaje@getGruposP');
    Route::get('profesor/mensajes/getAlumnosP','ProfesorMensaje@getAlumnosP');
    




Route::group(['middleware' => ['auth','profesor'], 'prefix'=>'profesor'], function(){



 
	Route::resource('/','profesorDashController');
	Route::resource('/tareas','TareaController');
	Route::resource('/eventos','eventosProfesorController');
	Route::resource('/asistencias','AsistenciaController');
	Route::resource('/calificacion','CalificacionesController');
	Route::resource('/graficas','GraficasProfesorController');
	Route::resource('/insignias','InsigniasProfesorController');
	Route::resource('/encuestas','RespuestasController');
	Route::resource('/horarioP','HorarioProfesorController');
	Route::get('MenuProfesor','GraficasProfesorController@MenuProfesor');
	Route::get('getChats','ChatProfesorController@getChats');
	Route::resource('/chats','ChatProfesorController');
	Route::get('/crearReferencia','ChatController@crearReferencia');
	Route::resource('/chats','ChatController');
	Route::get('mensajesR/enviados','MensajesRProfesorController@Enviado');
	Route::get('mensajesR/verEnviados','MensajesRProfesorController@verEnviados');
	Route::get('mensajesR/papelera','MensajesRProfesorController@papelera');
	Route::get('mensajesR/verPapelera','MensajesRProfesorController@verPapelera');
	Route::resource('mensajesR','MensajesRProfesorController');
	Route::get('getPeriodosProfesor','CalificacionesController@getPeriodosProfesor');
	Route::get('getPeriodosCalificados','CalificacionesController@getPeriodosCalificados');
	Route::get('getPeriodosNoCalificados','CalificacionesController@getPeriodosNoCalificados');
	Route::get('getAlumnoCalifProfesor','CalificacionesController@getAlumnoCalifProfesor');
	Route::get('getCalificacionesProfesor','CalificacionesController@getCalificacionesProfesor');
	Route::get('getAlumnoAsis','AsistenciaController@getAlumno');
	Route::get('getAsistenciasProf','AsistenciaController@getAsistencia');
	Route::get('/getFechasAsistencia','AsistenciaController@getFechasAsistencia');
	Route::get('/getFechasAsistencia2','AsistenciaController@getFechasAsistencia2');
	Route::get('getAlumnoInsignia','InsigniasProfesorController@getAlumnoInsignia');

	Route::resource('/perfilProf','perfilProfesorController');
	Route::post('/saveImageProf','perfilProfesorController@saveImage');
	Route::get('/getFechasAsistencia','AsistenciaController@getFechasAsistencia');
	Route::get('/avisosProf/enviados','avisosProfesorController@enviados');
    Route::resource('/avisosProf','avisosProfesorController');
    

    Route::get('getPromedioGrupoProf','GraficasProfesorController@getPromedioGrupoProf');
	Route::get('getPromedioAlumnoSobresaliente','GraficasProfesorController@getPromedioAlumnoSobresaliente');
	Route::get('getPeriodosAsistenciasProfesor','GraficasProfesorController@getPeriodosAsistenciasProfesor');
	Route::get('getPromedioAlumnoSobresalienteProf','GraficasProfesorController@getPromedioAlumnoSobresalienteProf');
	Route::get('getPromedioAlumnoRegularProf','GraficasProfesorController@getPromedioAlumnoRegularProf'
	);
	Route::get('getGraficas1','GraficasProfesorController@getGraficas1'
	);
	Route::get('getGraficas2','GraficasProfesorController@getGraficas2'
	);
	Route::get('getMayorAsistencia','GraficasProfesorController@getMayorAsistencia'
	);
	Route::get('getAlumnosRetardos','GraficasProfesorController@getAlumnosRetardos'
	);
	Route::get('getAlumnosFaltas','GraficasProfesorController@getAlumnosFaltas'
	);
	Route::get('getRetardo','GraficasProfesorController@getRetardo'
	);
	Route::get('getFalta','GraficasProfesorController@getFalta'
	);
	Route::get('TotalAsistencias','GraficasProfesorController@TotalAsistencias'
	);
	
	Route::get('getCalifCount','GraficasProfesorController@getCalifCount'
	);
	Route::get('getMaterias','GraficasProfesorController@getMaterias'
	);
	Route::get('getCalifAprobCount','GraficasProfesorController@getCalifAprobCount');
	Route::get('graficasCalifProf','GraficasProfesorController@graficasCalifProf');
    Route::get('getPreguntas','RespuestasController@getPreguntas');
    Route::get('mensajes/eliminados','ProfesorMensaje@eliminados');
    Route::get('mensajes/getDestinatario','ProfesorMensaje@getDestinatario');
    Route::get('mensajes/recibidos/show/{id}', ['as' => 'MuestraProfesor', 'uses'=>'ProfesorMensaje@show']);
    Route::get('mensajes/recibidos','ProfesorMensaje@recibidos');
    Route::get('mensajes/enviados','ProfesorMensaje@enviados');
    Route::resource('mensajes','ProfesorMensaje');
    Route::get('mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraProfe', 'uses'=>'ProfesorMensaje@papelera']);
    Route::post('mensajes/recibidos/search', ['as' => 'mensajes/search', 'uses'=>'ProfesorMensaje@search']);
    

   

});

  

Route::group(['middleware' => ['auth','alumno'], 'prefix'=>'alumno'], function(){
    Route::resource('/mensajesR','MensajesRAlumnoController');
	Route::resource('/','alumnoDashController');
	Route::resource('/tareas','TareaAlumnoController');
	Route::resource('/graficas','GraficasAlumnoController');
	Route::resource('/asistencia','AsistenciaAlumnoController');
	Route::resource('/calificaciones','CalificacionesAlumnoController');
	Route::resource('/encuestas','RespuestasAlumnoController');
	Route::resource('/insignias','InsigniasAlumnoController');
	Route::get('getCalificacionesAlumno','CalificacionesAlumnoController@getCalificaciones');
	Route::get('getAsistenciaAlumno','AsistenciaAlumnoController@getAsistencia');
	Route::resource('/eventos','eventosAlumnoController');
    Route::resource('/avisosAlum','avisosAlumnoController');
	Route::resource('/horarioAlumno','horarioAlumnoController');
	
	Route::resource('/perfilAlum','perfilAlumnoController');
	Route::post('/saveImageAlum','perfilAlumnoController@saveImage');
	
	Route::get('mensajes/recibidos','AlumnoMensajes@recibidos');
    Route::get('mensajes/','AlumnoMensajes@index');
    Route::post('mensajes/recibidos/search', ['as' => 'Busca2', 'uses'=>'AlumnoMensajes@search']);
    Route::get('mensajes/recibidos/show/{id}', ['as' => 'Muestra2', 'uses'=>'AlumnoMensajes@show']);
    Route::get('/mensajes/recibidos/papelera/{id}', ['as' => 'PapeleraAlumno', 'uses'=>'AlumnoMensajes@papelera']);
    Route::get('mensajes/eliminados',['as' => 'Eliminados2', 'uses'=>'AlumnoMensajes@eliminados']);
    Route::get('mensajes/eliminados/desaparece/{id}', ['as' => 'Desaparece2', 'uses'=>'AlumnoMensajes@desaparece']);
   
    Route::get('getPreguntasAlumno','RespuestasAlumnoController@getPreguntasAlumno');

});

Route::group(['middleware' => ['auth','super-admin'], 'prefix'=>'super-admin'], function(){
	Route::resource('/','superAdminDashController');
});