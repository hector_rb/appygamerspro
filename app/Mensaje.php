<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Mensaje extends Model
{
    protected $fillable = [
        'Mensaje', 'Asunto','TipoDestinatario','IdDestinatario'
    ];

    public function setPathAtributte($path)
    {
        $this->attributes['path']= Carbon::now()->second.$path->getClientOriginalName();
        $name=Carbon::now()->second.$path->getClientOriginalName();
        \Storage::disk('local')->put($name,\File::get($path));

    }
}
