<?php

namespace App;
use App\cuestionario;
use Illuminate\Database\Eloquent\Model;

class pregunta extends Model
{
   protected $table='juego-preguntas';
   protected $primaryKey="id_pregunta";
   public $timestamps=false;
   protected $filetable=[
   'idInstitucion',
   'grado',
   'id_nivel',
   'pregunta',
   'opcion1',
   'opcion2',
   'opcion3',
   'opcion4',
   'respuesta',
   'id_categoria'
   ];
   protected $guarded=[];
   
    public function cuestionarios()
   {
      return $this->belongsToMany(cuestionario::class,"juego-preguntas-cuestionario", "id_pregunta", "id_cuestionario");
   }
}
