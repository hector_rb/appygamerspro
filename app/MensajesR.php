<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MensajesR extends Model
{
   protected $table='mensajes';
   protected $primaryKey="id";
   public $timestamps=false;
   protected $filetable=[
   'Asunto',
   'Mensaje',
   'TipoDestinatario',
   'IdDestinatario',
   'IdEmisor',
   'TipoEmisor',
   'Visto',
   'idInstitucion',
   'created_at',
   'updated_at',
   'Eliminado',
   'anexo',

   ];
   protected $guarded=[];
}
