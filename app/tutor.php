<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tutor extends Model
{
    //
    protected $table='tutor';
    public $timestamps=false;
    protected $fillable =[
        'idtutor',
        'correo',
        'nombreTutor',
        'apepat',
        'apemat',
        'genero',
        'fechanac',
        'telefono',
        'iduser',
        'telefono',
        'celular',
        'teloficina',
        'idInstitucion',
        ];
}
