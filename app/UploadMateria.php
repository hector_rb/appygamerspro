<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class UploadMaterial extends Model
{
 
    public    $timestamps =     true;
    protected $table      = 'uploads';
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['filename'];
    protected $fillable = ['niveles'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}