<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profesor extends Model
{
    //
    protected $table='profesor';

    protected $primaryKey = 'idprofesor';

    public $timestamps=false;

    protected $fillable =[
        'correo',
        'id_institucion',
        'nombreprof',
        'apepat',
        'apemat',
        'genero',
        'fechanac',
        'telefono',
        'iduser',
        'telefono',
        'celular',
        'teloficina',
        'f_alta',
        'f_baja',
        'f_mod',
        'matricula'
        ];

    public function institucion()
    {
        return $this->belongsTo('App\institucion');
    }

    protected $guarded=[];
}
