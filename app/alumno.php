<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
    protected $table='alumno';
    protected $primaryKey = 'idAlumno';
    public $timestamps=false;
    protected $fillable =[
        'idAlumno',
        'correo',
        'nombreAlumno',
        'apepat',
        'apemat',
        'genero',
        'fechanac',
        'telefono',
        'iduser',
        'telefono',
        'f_alta',
        'f_baja',
        'f_mod',
        'idgrupo',
        'idinstitucion',
        'idtutor',
        ];
    protected $guarded=[];
}
