<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class directorio extends Model
{
    //
    protected $table='directorio';

    protected $primaryKey = 'idDirectorio';

    public $timestamps=false;

    protected $fillable =[
        'nombre',
        'telefono',
        'email',
        'puesto',
        'descripcion',
        'imagen',
        'idUser',
		'idInstitucion',
        ];

    protected $guarded=[];
}
