<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class avisos extends Model
{
    //
    protected $table='avisos';

    protected $primaryKey = 'idAviso';

    public $timestamps=false;

    protected $fillable =[
        'asunto',
        'cuerpo',
        'idInstitucion',
        'idUser',
        'fecha',
        
        ];

    protected $guarded=[];
}
