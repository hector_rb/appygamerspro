<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class institucion extends Model
{
    //
    protected $table='institucion';

    protected $primaryKey = 'idInstitucion';

    public $timestamps=false;

    protected $fillable =[
        'clave',
        'nombre',
        'idestado',
        'ciudad',
        'direccion',
        'correo',
        'id_usuario',
        'telefono',
        'contacto',
        'desc',
        'codregistro',
        'lat',
        'lng',
        'fecha_alta',
        'logo',
        'mision',
        'vision',
        'valores',
        'reglamento',
        ];

    protected $guarded=[];
}
