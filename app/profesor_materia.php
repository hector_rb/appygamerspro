<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profesor_materia extends Model
{
    //
    protected $table='materia_profesor';

    protected $primaryKey = 'idMateria';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idProfesor',
        ];

    protected $guarded=[];
}
