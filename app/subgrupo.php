<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subgrupo extends Model
{
    //
    protected $table='subgrupo';

    protected $primaryKey = 'idSubgrupo';

    public $timestamps=false;

    protected $fillable =[
        'idGrupo',
        'idMateria',
        'idProfesor',
        'idCiclo',
        ];

    protected $guarded=[];
}
