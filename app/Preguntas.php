<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas extends Model
{
   protected $table='enc_preguntas';
   protected $primaryKey="idPregunta";
   public $timestamps=false;
   protected $filetable=[
   'idencuesta',
   'nombrePregunta',
   'tipoRespuesta',
   'opciones',
   ];
   protected $guarded=[];
}
