<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\pregunta;

class cuestionario extends Model
{
    protected $table='juego-cuestionario';
    protected $primaryKey = 'id_cuestionario';
    public $timestamps=false;

    protected $fillable =[
        'idinstitucion',
        'categoria',
        'grado',
        'fecha_fin',
        'fecha_inicio',
        'descripcion',
        'nombre',
        'nivel'
        ];
        
         public function preguntas()
   {
      return $this->belongsToMany(pregunta::class,"juego-preguntas-cuestionario","id_cuestionario","id_pregunta");
   }
}
