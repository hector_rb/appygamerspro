<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class adminUsuarios extends Model
{
   protected $table='profesor';
   protected $primaryKey="idprofesor";
   public $timestamps=false;
   protected $filetable=[
   'idprofesor',
   'correo',
   'id_institucion',
   'nombreprof',
   'apepat',
   'apemat',
   'genero',
   'fechanac',
   'telefono',
   'celular',
   'teloficina',

   ];
   protected $guarded=[];
}
