<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JuegoPregunta extends Model
{
   protected $table='juego-preguntas';
   protected $primaryKey="id_pregunta";
   protected $filetable=[
   'idInstitucion',
   'grado',
   'id_nivel',
   'pregunta',
   'opcion1',
   'opcion2',
   'opcion3',
   'opcion4',
   'respuesta',
   'id_categoria',
   ];

   
   protected $guarded=[];
}
