<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class juegoCuestionario extends Model
{
    protected $table='juego-preguntas-cuestionario';
    public $timestamps=false;
    protected $fillable =[
        'id_pregunta',
        'id_cuestionario',
        ];
    protected $guarded=[];
}
