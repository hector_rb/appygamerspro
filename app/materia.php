<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class materia extends Model
{
    //
    protected $table='materia';

    protected $primaryKey = 'idMateria';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idNivel',
        'nombreMateria',
        'grado',
        'creditos',
        ];

    protected $guarded=[];
}
