<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model
{
   protected $table='enc_respuestas';
   protected $primaryKey="id";
   public $timestamps=false;
   protected $filetable=[
   'idEncuestado',
   'tipoEncuestado',
   'respuesta',
   'idPregunta',
   'idEncuesta',
   ];

   
   protected $guarded=[];
}
