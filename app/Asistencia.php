<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
   protected $table='asistencia';
   public $timestamps=false;
   protected $filetable=[
   'idAlumno',
   'idSubgrupo',
   'idPeriodo',
   'fecha',
   'asistencia',
   ];
   protected $guarded=[];
}
