<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class horario extends Model
{
    //
    //
    protected $table='horario';

    protected $primaryKey = 'idHorario';

    public $timestamps=false;

    protected $fillable =[
        'idGrupo',
        'idSubgrupo',
        'idCiclo',
        'inicio',
        'fin',
        'dia',
        'horaIni',
        'horaFin',
        
        
        ];

    protected $guarded=[];
}
