-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-12-2017 a las 08:04:24
-- Versión del servidor: 10.2.11-MariaDB-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `myappc5_myappcollegeauth`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadmin` int(11) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `id_institucion` int(11) DEFAULT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `teloficina` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerta`
--

CREATE TABLE `alerta` (
  `idAlerta` int(11) NOT NULL,
  `tipoAlerta` int(11) NOT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `condicion` tinyint(4) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alerta`
--

INSERT INTO `alerta` (`idAlerta`, `tipoAlerta`, `descripcion`, `created_at`, `condicion`, `idInstitucion`, `idUser`, `updated_at`) VALUES
(19, 6, 'Inundación', '2017-10-19 18:45:30', 1, 1, 1, NULL),
(26, 1, 'Alerta de sismo', '2017-11-10 02:07:45', 1, 24, 144, '2017-11-10 02:07:45'),
(27, 1, 'alerta de sismo a as 3', '2017-11-11 04:17:30', 1, 25, 148, '2017-11-11 04:17:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `idAlumno` int(11) NOT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `nombreAlumno` varchar(80) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT '1',
  `tokenFCM` varchar(160) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `idgrupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`idAlumno`, `idinstitucion`, `nombreAlumno`, `apepat`, `apemat`, `genero`, `fechanac`, `telefono`, `correo`, `f_alta`, `f_baja`, `f_mod`, `activo`, `tokenFCM`, `iduser`, `idgrupo`) VALUES
(1, 1, 'Gilberto', 'Lopez', 'Ruiz', 'M', '2001-02-28', '4444444444', 'gilberto@gmail.com', '2017-10-06', NULL, NULL, '1', 'd5c0kC71ZFE:APA91bHPfafNviEuAt2RWEGsCnMZ2bZBaSH2bI5dUPISr4dS95klhhhOAJDyxwpjGKGZJKQ8WlpGjaXr7QzBCcy63Sad3j_umRuRTqeKWRc_aHbshPWADUGiyFvTxBdlDL45ZAl3J-yh', 42, 1),
(3, 1, 'Ana Paola', 'Huerta', 'Hernandez', 'F', '2004-08-01', '4441175199', 'ana_h_h@gmail.com', NULL, NULL, NULL, '1', NULL, 52, 1),
(4, 8, 'Rosa Maria', 'Mejia', 'Hernandez', 'F', '1999-12-19', '787667865', 'rosa@gmail.com', '2017-10-09', NULL, NULL, '1', NULL, 57, 2),
(5, 8, 'Lorenzo', 'Mejia', 'Hernandez', 'M', '1999-11-10', '5678667865', 'lorenzo@gmail.com', '2017-10-09', NULL, NULL, '1', NULL, 60, 94),
(6, 8, 'Hector', 'Hernandez', 'Hernandez', NULL, '1992-08-08', '4', 'a@gmail.com', NULL, NULL, NULL, '1', NULL, 62, 94),
(7, 10, 'Miguel', 'Huerta', 'Hernandes', 'M', '2002-06-07', '23412412412', 'miguel_h_h@gmail.com', '2017-10-10', NULL, NULL, '1', 'cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs', 71, 118),
(8, 11, 'Ari', 'zona', 'Zona', 'M', '2017-10-10', '234243', 'arizona@gmail.com', '2017-10-10', NULL, NULL, '1', NULL, 75, 130),
(9, 1, 'luis', 'melgarejo', 'torres', NULL, '2004-05-02', '222222222', 'luis@abc.com', NULL, NULL, NULL, '1', NULL, 80, 1),
(10, 1, 'Jose Herrero', 'Huerta', 'Gomez', NULL, '2004-08-01', '4441175199', 'jose_huerta@gmail.com', NULL, NULL, NULL, '1', NULL, 82, 1),
(11, 1, 'Sebastian Arturo', 'melgarejo', 'torres', NULL, '2007-01-06', '1444444444', 'sebas@gmail.com', NULL, NULL, NULL, '1', NULL, 86, 1),
(12, 17, 'Luisito', 'Perez', 'Lopez', 'M', '2017-10-13', '3242321', 'luisito@motolinea.com', '2017-10-23', NULL, NULL, '1', NULL, 98, 202),
(13, 18, 'hj', 'khkh', 'kjkjn', 'M', '2017-10-24', '85465', 'tino2@gmail.com', '2017-10-24', NULL, NULL, '1', NULL, 109, 206),
(14, 17, 'Carla', 'Lopez', 'Ruiz', 'F', '2017-10-25', '2345678', 'carla@motolinea.com', '2017-10-25', NULL, NULL, '1', NULL, 111, 205),
(15, 8, 'Mario', 'Robles', 'Mejia', NULL, '2003-05-13', '44415615', 'marior@gmail.com', NULL, NULL, NULL, '1', NULL, 113, 94),
(16, 8, 'Juan', 'Huerta', 'Hernández', NULL, '1999-07-15', '4441175199', 'juan18@gmail.com', NULL, NULL, NULL, '1', NULL, 130, 49),
(17, 1, 'Diego', 'Arrez', 'Gamboa', NULL, '2004-07-19', '4442317929', 'alejandro@gmail.com', NULL, NULL, NULL, '1', NULL, 136, 2),
(37, 1, 'Jean Herrero', 'Huerta', 'Gomez', NULL, '2004-08-01', '4441175199', 'jose_huerta@gmail.com', NULL, NULL, NULL, '1', NULL, 82, 1),
(38, 1, 'Mario Arturo', 'melgarejo', 'torres', NULL, '2007-01-06', '1444444444', 'sebas@gmail.com', NULL, NULL, NULL, '1', NULL, 86, 1),
(39, 1, 'Rosa', 'De la rosa', 'Medina', 'F', '2000-04-08', '11122233344', 'rosita@gmail.com', '2017-12-13', NULL, NULL, '1', NULL, 152, 1),
(40, 1, 'Emma', 'Robles', 'Rodríguez', NULL, '2010-09-16', '4444238263', 'emma@gmail.com', NULL, NULL, NULL, '1', NULL, 153, 1),
(41, 1, 'Citlalli', 'reyna', 'gallego', 'F', '2017-12-13', '014444259139', 'citlalli@gmail.com', '2017-12-13', NULL, NULL, '1', NULL, 155, 1),
(42, 1, 'Arturito', 'Torres', 'Meza', NULL, '2010-07-15', '548788787', 'arturito@gmail.com', NULL, NULL, NULL, '1', NULL, 157, 1),
(43, 1, 'Mauricio', 'Reyna', 'Lopéz', NULL, '2002-07-15', '4441267658', 'mauricio_reyna@gmail.com', NULL, NULL, NULL, '1', NULL, 161, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `idAlumno` int(11) NOT NULL,
  `idSubgrupo` int(40) NOT NULL,
  `idPeriodo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `asistencia` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asistencia`
--

INSERT INTO `asistencia` (`idAlumno`, `idSubgrupo`, `idPeriodo`, `fecha`, `asistencia`) VALUES
(1, 1, 1, '2017-10-19', 'A'),
(1, 1, 1, '2017-10-20', 'A'),
(1, 1, 1, '2017-10-24', 'A'),
(1, 1, 2, '2017-10-26', 'R'),
(1, 1, 1, '2017-10-27', 'A'),
(1, 1, 2, '2017-11-02', 'F'),
(1, 1, 1, '2017-11-03', 'A'),
(1, 1, 4, '2017-11-06', 'F'),
(3, 1, 1, '2017-10-03', 'A'),
(3, 1, 1, '2017-10-04', 'R'),
(3, 1, 1, '2017-10-05', 'A'),
(3, 1, 1, '2017-10-06', 'A'),
(3, 1, 1, '2017-10-09', 'A'),
(3, 1, 1, '2017-10-12', 'A'),
(3, 1, 1, '2017-10-19', 'A'),
(3, 1, 1, '2017-10-20', 'A'),
(3, 1, 1, '2017-10-24', 'A'),
(3, 1, 2, '2017-10-26', 'R'),
(3, 1, 1, '2017-10-27', 'R'),
(3, 1, 2, '2017-11-02', 'F'),
(3, 1, 1, '2017-11-03', 'F'),
(3, 1, 4, '2017-11-06', 'R'),
(3, 2, 1, '2017-10-03', 'F'),
(3, 2, 1, '2017-10-04', 'R'),
(3, 2, 1, '2017-10-05', 'A'),
(3, 2, 1, '2017-10-06', 'A'),
(3, 2, 1, '2017-10-09', 'A'),
(4, 12, 11, '2017-10-08', 'A'),
(4, 12, 29, '2017-10-09', 'A'),
(5, 12, 29, '2017-10-09', 'A'),
(5, 12, 29, '2017-10-20', 'R'),
(5, 12, 29, '2017-10-23', 'R'),
(5, 12, 29, '2017-10-30', 'A'),
(5, 12, 29, '2017-10-31', 'F'),
(5, 12, 30, '2017-11-02', 'F'),
(6, 12, 29, '2017-10-09', 'A'),
(6, 12, 1, '2017-10-20', 'F'),
(6, 12, 29, '2017-10-23', 'A'),
(6, 12, 29, '2017-10-30', 'R'),
(6, 12, 29, '2017-10-31', 'R'),
(6, 12, 30, '2017-11-02', 'R'),
(7, 15, 32, '2017-10-10', 'A'),
(7, 15, 32, '2017-10-11', 'A'),
(7, 15, 32, '2017-10-12', 'R'),
(9, 1, 1, '2017-10-12', 'A'),
(9, 1, 1, '2017-10-19', 'A'),
(9, 1, 1, '2017-10-20', 'R'),
(9, 1, 1, '2017-10-24', 'A'),
(9, 1, 2, '2017-10-26', 'A'),
(9, 1, 1, '2017-10-27', 'A'),
(9, 1, 2, '2017-11-02', 'F'),
(9, 1, 1, '2017-11-03', 'R'),
(9, 1, 4, '2017-11-06', 'A'),
(10, 1, 1, '2017-10-12', 'A'),
(10, 1, 1, '2017-10-19', 'A'),
(10, 1, 1, '2017-10-20', 'F'),
(10, 1, 1, '2017-10-24', 'A'),
(10, 1, 2, '2017-10-26', 'F'),
(10, 1, 1, '2017-10-27', 'A'),
(10, 1, 2, '2017-11-02', 'F'),
(10, 1, 1, '2017-11-03', 'R'),
(10, 1, 4, '2017-11-06', 'A'),
(11, 1, 1, '2017-10-19', 'A'),
(11, 1, 1, '2017-10-20', 'A'),
(11, 1, 1, '2017-10-24', 'R'),
(11, 1, 2, '2017-10-26', 'A'),
(11, 1, 1, '2017-10-27', 'F'),
(11, 1, 2, '2017-11-02', 'F'),
(11, 1, 1, '2017-11-03', 'A'),
(11, 1, 4, '2017-11-06', 'A'),
(12, 22, 60, '2017-10-23', 'A'),
(12, 22, 60, '2017-10-27', 'A'),
(15, 12, 29, '2017-10-30', 'F'),
(15, 12, 29, '2017-10-31', 'A'),
(15, 12, 30, '2017-11-02', 'R'),
(1, 1, 70, '2017-11-07', 'A'),
(3, 1, 70, '2017-11-07', 'F'),
(10, 1, 70, '2017-11-07', 'A'),
(9, 1, 70, '2017-11-07', 'F'),
(11, 1, 70, '2017-11-07', 'R'),
(1, 1, 1, '2017-11-08', 'A'),
(10, 1, 1, '2017-11-08', 'A'),
(9, 1, 1, '2017-11-08', 'A'),
(11, 1, 1, '2017-11-08', 'R'),
(3, 1, 1, '2017-11-08', 'F'),
(9, 1, 1, '2017-11-09', 'A'),
(11, 1, 1, '2017-11-09', 'A'),
(1, 1, 1, '2017-11-09', 'A'),
(3, 1, 1, '2017-11-09', 'A'),
(10, 1, 1, '2017-11-09', 'A'),
(10, 1, 5, '2017-12-06', 'F'),
(1, 1, 5, '2017-12-06', 'R'),
(3, 1, 5, '2017-12-06', 'F'),
(9, 1, 5, '2017-12-06', 'A'),
(11, 1, 5, '2017-12-06', 'A'),
(1, 1, 3, '2017-12-10', 'A'),
(3, 1, 3, '2017-12-10', 'F'),
(9, 1, 3, '2017-12-10', 'A'),
(10, 1, 3, '2017-12-10', 'R'),
(11, 1, 3, '2017-12-10', 'A'),
(37, 1, 3, '2017-12-10', 'A'),
(38, 1, 3, '2017-12-10', 'F'),
(9, 1, 2, '2017-12-12', 'A'),
(1, 1, 2, '2017-12-12', 'A'),
(37, 1, 2, '2017-12-12', 'A'),
(3, 1, 2, '2017-12-12', 'F'),
(10, 1, 2, '2017-12-12', 'R'),
(38, 1, 2, '2017-12-12', 'A'),
(11, 1, 2, '2017-12-12', 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avisos`
--

CREATE TABLE `avisos` (
  `idAviso` int(11) NOT NULL,
  `asunto` varchar(200) NOT NULL,
  `cuerpo` varchar(20000) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `destinatario` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `avisos`
--

INSERT INTO `avisos` (`idAviso`, `asunto`, `cuerpo`, `idInstitucion`, `idUser`, `destinatario`, `fecha`) VALUES
(10, 'No hay clases', 'No hay clases el dia de mañana', 24, 144, 1, '2017-09-11 17:57:39'),
(11, 'dfghjkkjh', 'fghjklñ', 25, 148, 3, '2017-10-11 20:14:31'),
(9, 'Saludos Junta de profesores', 'junta a las 11 am', 24, 144, 3, '2017-09-11 17:56:48'),
(8, 'Matenimiento', '<p>Se les recuerda que por el mantenimiento al servidor, el sistema escolar no estará disponible el dia 11 de noviembre, y se reanudara hasta el 15 de noviembre.&nbsp;</p><p><b><span style=\"font-size: 14px;\">Favor de tomar todas las acciones necesarias antes de la fecha de baja.</span></b><br></p>', 1, 1, 1, '2017-07-11 19:54:23'),
(6, 'Junta de padres', 'el lunes 13 de noviembre habrá junta de padre de familia', 1, 1, 2, '2017-06-11 17:01:55'),
(7, 'Junta de profesores', 'Junta el lunes 13 de noviembre a las 14:00 h', 1, 1, 3, '2017-07-11 00:07:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario`
--

CREATE TABLE `calendario` (
  `idCalendario` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `desc` varchar(250) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  `tipoDestinatario` int(11) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `iduser` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `clase` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `calendario`
--

INSERT INTO `calendario` (`idCalendario`, `titulo`, `desc`, `inicio`, `fin`, `tipoDestinatario`, `id`, `iduser`, `idInstitucion`, `clase`, `status`) VALUES
(1, 'Algebra', 'Miguel Torres', '2017-09-26 00:00:00', '2017-10-01 00:00:00', 0, NULL, 1, 1, 'azul2', 1),
(2, 'Prueba 2!!!!!!', 'evento de prueba!!', '2017-09-23 02:00:00', '2017-09-23 02:00:00', 0, NULL, 1, 1, 'naranja', 1),
(3, 'Preba urgente', 'Emergencia', '2017-09-30 03:00:00', '2017-09-30 10:00:00', 0, NULL, 1, 1, 'rojo', 0),
(4, 'Junta Padres', 'Junta para padres del 3a 12:10', '2017-10-01 09:00:00', '2017-10-01 12:00:00', 0, NULL, 1, 1, 'rojo', 1),
(5, 'Nuevo evento', 'primer evento', '2017-09-21 09:00:00', '2017-09-21 15:00:00', 0, NULL, 1, 1, 'verde', 1),
(43, 'Día del niño', 'piñata', '2017-09-27 00:00:00', '2017-10-02 00:00:00', 1, 0, 94, 17, 'azul2', 1),
(44, 'Día de la niña', 'piñata2', '2017-10-30 00:00:00', '2017-10-31 00:00:00', 1, 0, 94, 17, 'rojo', 1),
(45, 'baile de navidad', 'Baile de navidad organizado por el colegio, es para todos los alumnos de la institución......', '2017-11-14 08:00:00', '2017-11-14 14:00:00', 4, 1, 1, 1, 'rojo', 1),
(46, 'Junta profesores', 'junta profesores 11 am', '2017-11-14 00:00:00', '2017-11-14 00:00:00', 1, 0, 144, 24, 'azul2', 1),
(47, 'kermes', 'kermes escolar', '2017-11-15 00:00:00', '2017-11-15 00:00:00', 2, 3, 148, 25, 'azul2', 0),
(48, 'POSADA NAVIDEÑA', 'En este evento se realizará la entrega de calificaciones', '2017-12-21 08:00:00', '2017-12-21 12:00:00', 1, 0, 1, 1, 'rojo', 1),
(49, 'Wiii', 'lallalskjjo', '2017-12-14 00:00:00', '2017-12-16 00:00:00', 1, 0, 1, 1, 'azul1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario_asistencia`
--

CREATE TABLE `calendario_asistencia` (
  `idCalendarioAsistencia` int(11) NOT NULL,
  `idCalendario` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `asistencia` int(2) NOT NULL COMMENT '0: No     1: Si',
  `sincronizar` int(2) NOT NULL DEFAULT 0 COMMENT '0: No     1: Si'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `calendario_asistencia`
--

INSERT INTO `calendario_asistencia` (`idCalendarioAsistencia`, `idCalendario`, `idUsuario`, `asistencia`, `sincronizar`) VALUES
(27, 49, 48, 1, 0),
(26, 48, 48, 1, 1),
(25, 45, 48, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calif`
--

CREATE TABLE `calif` (
  `idcalif` int(11) NOT NULL,
  `idSubgrupo` int(11) DEFAULT NULL,
  `idalumno` int(11) DEFAULT NULL,
  `idPeriodo` int(11) NOT NULL,
  `calif` float DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `calif`
--

INSERT INTO `calif` (`idcalif`, `idSubgrupo`, `idalumno`, `idPeriodo`, `calif`, `fecha`) VALUES
(3, 12, 4, 11, 10, '2017-10-12'),
(4, 12, 5, 11, 7, '2017-10-12'),
(5, 12, 4, 29, 9, '2017-10-02'),
(6, 12, 5, 29, 9, '2017-10-02'),
(7, 12, 6, 29, 5, '2017-10-02'),
(9, 15, 7, 33, 2, '2017-10-20'),
(24, 12, 5, 30, 10, '2017-10-18'),
(25, 12, 6, 30, 9, '2017-10-18'),
(32, 22, 12, 60, 9, '2017-10-23'),
(33, 12, 5, 1, 8, '2017-10-27'),
(36, 12, 6, 31, 10, '2017-10-30'),
(37, 12, 5, 31, 10, '2017-10-30'),
(38, 12, 15, 31, 10, '2017-10-30'),
(42, 2, 10, 1, 8, '2017-10-30'),
(43, 2, 11, 1, 9, '2017-10-30'),
(44, 12, 15, 30, 7, '2017-10-30'),
(49, 12, 15, 29, 100, '2017-10-31'),
(121, 1, 1, 1, 9, '2017-11-08'),
(125, 2, 1, 1, 10, '2017-11-08'),
(127, 2, 9, 1, 6, '2017-11-08'),
(128, 2, 3, 1, 5, '2017-11-08'),
(131, 1, 9, 2, 7, '2017-11-08'),
(132, 1, 10, 2, 10, '2017-11-08'),
(135, 1, 11, 3, 5, '2017-11-09'),
(136, 1, 1, 3, 5, '2017-11-09'),
(137, 1, 9, 3, 5, '2017-11-09'),
(138, 1, 3, 3, 5, '2017-11-09'),
(139, 1, 10, 3, 5, '2017-11-09'),
(142, 1, 9, 1, 8, '2017-11-09'),
(151, 1, 10, 1, 8, '2017-11-14'),
(152, 1, 11, 1, 8, '2017-11-14'),
(153, 1, 3, 1, 8, '2017-11-14'),
(155, 1, 3, 2, 4, '2017-11-14'),
(157, 1, 11, 2, 9, '2017-11-14'),
(158, 1, 1, 2, 5, '2017-11-14'),
(160, 1, 3, 4, 9, '2017-11-14'),
(161, 1, 10, 4, 5, '2017-11-14'),
(162, 1, 1, 4, 9, '2017-11-14'),
(163, 1, 9, 4, 7, '2017-11-14'),
(164, 1, 11, 4, 8, '2017-11-14'),
(165, 1, 10, 5, 9, '2017-11-14'),
(166, 1, 9, 5, 9, '2017-11-14'),
(167, 1, 3, 5, 9, '2017-11-14'),
(168, 1, 1, 5, 9, '2017-11-14'),
(169, 1, 11, 5, 9, '2017-11-14'),
(170, 1, 31, 70, 7, '2017-11-29'),
(171, 1, 26, 70, 7, '2017-11-29'),
(172, 1, 36, 70, 7, '2017-11-29'),
(173, 1, 21, 70, 6, '2017-11-29'),
(174, 1, 10, 70, 7, '2017-11-29'),
(175, 1, 29, 70, 9, '2017-11-29'),
(176, 1, 24, 70, 8, '2017-11-29'),
(177, 1, 3, 70, 9, '2017-11-29'),
(178, 1, 34, 70, 8, '2017-11-29'),
(179, 1, 19, 70, 6, '2017-11-29'),
(180, 1, 28, 70, 9, '2017-11-29'),
(181, 1, 23, 70, 9, '2017-11-29'),
(182, 1, 18, 70, 9, '2017-11-29'),
(183, 1, 1, 70, 9, '2017-11-29'),
(184, 1, 33, 70, 9, '2017-11-29'),
(185, 1, 20, 70, 9, '2017-11-29'),
(186, 1, 9, 70, 9, '2017-11-29'),
(187, 1, 32, 70, 9, '2017-11-29'),
(188, 1, 27, 70, 9, '2017-11-29'),
(189, 1, 35, 70, 9, '2017-11-29'),
(190, 1, 30, 70, 9, '2017-11-29'),
(191, 1, 22, 70, 9, '2017-11-29'),
(192, 1, 11, 70, 9, '2017-11-29'),
(193, 1, 25, 70, 9, '2017-11-29'),
(196, 1, 38, 5, 10, '2017-12-12'),
(198, 1, 37, 5, 6, '2017-12-12'),
(215, 1, 38, 70, 10, '2017-12-12'),
(219, 1, 37, 70, 6, '2017-12-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclo`
--

CREATE TABLE `ciclo` (
  `idCiclo` int(11) NOT NULL,
  `nombreCiclo` varchar(70) NOT NULL,
  `inicioCiclo` date NOT NULL,
  `finCiclo` date NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciclo`
--

INSERT INTO `ciclo` (`idCiclo`, `nombreCiclo`, `inicioCiclo`, `finCiclo`, `idInstitucion`, `idNivel`, `status`) VALUES
(1, '2017-2018', '2017-08-19', '2018-07-19', 1, 2, 0),
(2, '2017-2018', '2017-08-19', '2018-07-19', 1, 3, 0),
(3, 'Otoño 2017', '2017-08-19', '2017-11-19', 1, 4, 1),
(4, 'Primavera 2018', '2017-10-20', '2017-10-19', 1, 4, 0),
(5, 'Ciclo 2017-2018 Preescolar', '2017-10-08', '2017-10-08', 8, 1, 1),
(6, 'Ciclo 2017-2018 Primaria', '2017-10-08', '2017-10-08', 8, 2, 1),
(7, 'Ciclo 2017-2018 Secundaria', '2017-10-08', '2017-10-08', 8, 3, 0),
(8, 'Ciclo 2017-2018 Secundaria', '2017-10-08', '2017-10-08', 8, 3, 0),
(9, 'Otoño 2017', '2017-10-08', '2017-10-08', 8, 4, 1),
(10, 'Ciclo 2017-2018 Secundaria', '2017-10-10', '2017-10-10', 10, 3, 1),
(11, 'Ciclo 2017-2018 Primaria', '2017-10-10', '2017-10-10', 11, 2, 1),
(12, 'preescolar 2017 - 2018', '2017-10-10', '2017-10-10', 10, 1, 1),
(13, 'semestre', '2017-01-01', '2018-01-01', 13, 1, 0),
(14, '2017-2018', '2017-08-15', '2018-07-19', 15, 2, 1),
(15, 'NO SÉ', '2017-01-01', '2017-01-01', 16, 3, 0),
(16, '2017-2018', '2017-08-18', '2018-07-04', 17, 2, 1),
(17, '2017-2018', '2017-01-01', '2018-01-01', 18, 4, 1),
(18, '2017-2018', '2017-08-21', '2018-07-23', 23, 2, 1),
(19, '2017-2018', '2017-08-21', '2018-08-20', 24, 2, 1),
(20, '2017-2018', '2017-11-10', '2018-06-18', 25, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conocimientos_adicionales`
--

CREATE TABLE `conocimientos_adicionales` (
  `idConocimientos` int(11) NOT NULL,
  `idAlumno` int(11) NOT NULL,
  `conocimiento1` text NOT NULL,
  `conocimiento2` text DEFAULT NULL,
  `conocimiento3` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `conocimientos_adicionales`
--

INSERT INTO `conocimientos_adicionales` (`idConocimientos`, `idAlumno`, `conocimiento1`, `conocimiento2`, `conocimiento3`) VALUES
(1, 1, 'Ganador de olimpiadas', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenidos`
--

CREATE TABLE `contenidos` (
  `idContenido` int(11) NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descripcion` text NOT NULL,
  `anexo` varchar(80) NOT NULL,
  `imagen` varchar(80) NOT NULL,
  `año` date NOT NULL,
  `editorial` varchar(100) NOT NULL,
  `autores` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_medicos`
--

CREATE TABLE `datos_medicos` (
  `idHistorialMedico` int(11) NOT NULL,
  `idAlumno` int(11) NOT NULL,
  `alergias` varchar(60) DEFAULT NULL,
  `enfermedades` varchar(60) DEFAULT NULL,
  `enfCro` varchar(60) DEFAULT NULL COMMENT 'Enfermedades Cronicas',
  `cirugias` varchar(60) DEFAULT NULL COMMENT 'Cirugias pasadas o proximas',
  `numSeguro` varchar(60) DEFAULT NULL COMMENT 'Número de seguro',
  `tipoSangre` varchar(11) NOT NULL COMMENT 'Tipo Sanguineo',
  `numEme` varchar(11) DEFAULT NULL COMMENT 'Numero de Emergencia'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datos_medicos`
--

INSERT INTO `datos_medicos` (`idHistorialMedico`, `idAlumno`, `alergias`, `enfermedades`, `enfCro`, `cirugias`, `numSeguro`, `tipoSangre`, `numEme`) VALUES
(1, 1, 'ninguna', 'ninguna', 'ninguna', 'ninguna', 'IMSS124323', 'A POSITIVO', '124322');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinatarios`
--

CREATE TABLE `destinatarios` (
  `id` int(11) NOT NULL,
  `TipoDestinatario` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `destinatarios`
--

INSERT INTO `destinatarios` (`id`, `TipoDestinatario`) VALUES
(1, 'Administrador'),
(2, 'Tutor'),
(3, 'Profesor'),
(4, 'Alumno'),
(5, 'Super Administrador'),
(6, 'Grado'),
(7, 'Grupo'),
(8, 'Todos'),
(9, 'Profesores'),
(10, 'Nivel'),
(11, 'Todos Los Tutores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directorio`
--

CREATE TABLE `directorio` (
  `idDirectorio` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `puesto` varchar(200) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `imagen` varchar(40) NOT NULL DEFAULT 'profile_small.jpg',
  `idUser` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `directorio`
--

INSERT INTO `directorio` (`idDirectorio`, `nombre`, `telefono`, `email`, `puesto`, `descripcion`, `imagen`, `idUser`, `idInstitucion`) VALUES
(1, 'Juan Martin', '2345432', 'jmmmm@gmail.com', 'Director', '....-----.....', '2017110815101632601.jpg', 1, 1),
(2, 'Jean Robles', '81377', 'jean@gmail.com', 'Subdirector', '.-.-.-.-.-.-.--', '2017110815101633282.jpg', 1, 1),
(3, 'Juan Jose', '98239', 'jjjjjjjjj@gmail.com', 'Servicios Escolares', '-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-', '2017110815101633913.jpg', 1, 1),
(4, 'Mario Mejia', '283928', 'mario@gmail.com', 'Super Intendencia', ',-,-,-,-,-,-,-,-,-,-,-,,-,-,-,-,-,,-,,,', '2017110815101634734.jpg', 1, 1),
(5, 'jesus Corona', '678976', 'zuno@gmail.com', 'Director', 'Director de la escuela', '2017101815083473255.jpg', 53, 8),
(6, 'Alejandro Arrez', '413413413', 'alejandro.ulises@gmail.com', 'gerente', 'fadfadfsaafad', '2017101815083548886.jpg', 88, 13),
(7, 'Juan Martín Huerta', '2323423323', 'jmartin@myappcollege.com', 'Desarrollador', 'djfvqñdvqu`hbvufbqubuqeuvqubvqqfb', '2017101915084247417.jpg', 92, 15),
(8, 'Ulises Méndez', '1434134143', 'jdajf@jfldsjfla.com', 'profesor', 'Es muyyy bueno.', 'profile_small.jpg', 93, 16),
(9, 'Ulises Méndez', '1434134143', 'jdajf@jfldsjfla.com', 'profesor', 'Es muyyy bueno.', '2017102215087155619.jpg', 93, 16),
(10, 'Juan Jose', '6879087', 'juanjose@hotmail.com', 'Subdirector', 'Ejoiikniik', '20171023150876909610.jpg', 94, 17),
(11, 'hklj', '780', 'hjjkn@hbh.uijio', 'ygiuh', 'huihoijoi', '20171026150904722211.jpg', 94, 17),
(12, 'adcsd', 'sdcsdc', 'dcvsdc@gmai.com', 'ygih', 'hkjghjklñ', '20171026150904725212.jpg', 94, 17),
(13, 'thomy', 'dzfs53547', 'dgdfh.ghyhfQq@afdhfh', 'sareaabaw', 'wr5naw4betnz', '20171026150904730613.jpg', 94, 17),
(14, 'Angélica Castillo', '444322353', 'angelica@gmail.com', 'Subdirector', 'Subdirector de la escuela Jesus Silva Herzog', 'profile_small.jpg', 143, 23),
(15, 'Diana Castro', '444124323', 'diana@gmail.com', 'Prefecto', 'Prefecto de la escuela Jesus Silva Herzog', '20171109151024025815.jpg', 143, 23),
(16, 'Christina Appycollege', '4441234237', 'Christina@appycollege.com', 'Subdirector academico', 'Subdirector academico de la escuela appycollege', '20171109151024761116.jpg', 144, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

CREATE TABLE `encuesta` (
  `idEncuesta` int(11) NOT NULL,
  `idAdmin` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `fecha` date NOT NULL,
  `estatus` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`idEncuesta`, `idAdmin`, `idInstitucion`, `nombre`, `descripcion`, `fecha`, `estatus`) VALUES
(229, 1, 1, 'VACUNACION ESCOLAR', 'CONOCER EL UNIVERSO DE ESTUDIANTES VACUNADOS', '2017-12-13', 1),
(228, 1, 1, 'VACUNACION ESCOLAR', 'CONOCER EL UNIVERSO DE ESTUDIANTES VACUNADOS', '2017-12-13', 1),
(227, 1, 1, 'VACUNACION ESCOLAR', 'CONOCER EL UNIVERSO DE ESTUDIANTES VACUNADOS', '2017-12-13', 1),
(226, 1, 1, 'VACUNACION ESCOLAR', 'CONOCER EL UNIVERSO DE ESTUDIANTES VACUNADOS', '2017-12-13', 1),
(224, 10, 10, 'Prueba 4', 'Prueba 4', '2017-12-12', 1),
(225, 1, 1, 'SALUD ESCOLAR', 'CONOCER EL ESTADO DE VACUNACIÓN DE LOS ALUMNOS DE EDUCACIÓN BÁSICA', '2017-12-13', 1),
(222, 10, 10, 'Prueba 2', 'Prueba 2', '2017-12-12', 1),
(223, 10, 10, 'Prueba 3', 'Prueba 3', '2017-12-12', 1),
(221, 10, 10, 'Prueba 1', 'Prueba 1', '2017-12-12', 1),
(220, 10, 10, 'quinta encuesta', 'quinta encuesta', '2017-12-12', 1),
(218, 10, 10, 'tercera encuesta', 'tercera encuesta', '2017-12-12', 1),
(219, 10, 10, 'cuarta encuesta', 'cuarta encuesta', '2017-12-12', 1),
(217, 10, 10, 'segunda encuesta', 'primera encuesta', '2017-12-12', 1),
(215, 1, 1, '12 12 2017', '12 12 2017', '2017-12-12', 1),
(216, 10, 10, 'primera encuesta', 'primera encuesta', '2017-12-12', 1),
(214, 1, 1, '12 12 2017', '12 12 2017', '2017-12-12', 1),
(213, 1, 1, '12 12 2017', '12 12 2017', '2017-12-12', 1),
(212, 1, 1, '12 12 2017', '12 12 2017', '2017-12-12', 1),
(211, 1, 1, 'Perfil del alumno', 'En ésta encuesta se conocerá el perfil de cada alumno', '2017-12-12', 1),
(210, 1, 1, 'Perfil del alumno', 'En ésta encuesta se conocerá el perfil de cada alumno', '2017-12-12', 1),
(208, 1, 1, 'encuesta', 'encuesta', '2017-12-12', 1),
(209, 1, 1, 'Encuesta', '12345', '2017-12-12', 1),
(206, 1, 1, 'encuesta', 'encuesta', '2017-12-12', 1),
(207, 1, 1, 'Perfil del alumno', 'En ésta encuesta se conocerá el perfil de cada alumno', '2017-12-12', 1),
(205, 1, 1, 'Perfil del alumno', 'En ésta encuesta se conocerá el perfil de cada alumno', '2017-12-12', 1),
(204, 1, 1, 'CALIDAD ESCOLAR', 'ESTA ENCUESTA TIENE COMO FINALIDAD SABER COMO ESTÁ EL NIVEL EDUCATIVO DE LA ESCUELA', '2017-12-11', 1),
(203, 1, 1, 'CALIDAD ESCOLAR', 'ESTA ENCUESTA TIENE COMO FINALIDAD SABER COMO ESTÁ EL NIVEL EDUCATIVO DE LA ESCUELA', '2017-12-11', 1),
(202, 1, 1, 'CALIDAD ESCOLAR', 'ESTA ENCUESTA TIENE COMO FINALIDAD SABER COMO ESTÁ EL NIVEL EDUCATIVO DE LA ESCUELA', '2017-12-11', 1),
(201, 1, 1, '123456789', '123456789', '2017-12-11', 1),
(200, 1, 1, 'marco', 'marco', '2017-12-11', 1),
(199, 1, 1, 'una', 'una', '2017-12-11', 1),
(198, 1, 1, 'jean', 'jeaaaaaaan¡¡¡¡¡¡¡', '2017-12-11', 1),
(197, 1, 1, 'jean', 'jeaaaaaaan¡¡¡¡¡¡¡', '2017-12-11', 1),
(196, 1, 1, 'abcd', 'abcd', '2017-12-11', 1),
(195, 1, 1, 'abcd', 'abcd', '2017-12-11', 1),
(193, 1, 1, '123456', '123456', '2017-12-11', 1),
(194, 1, 1, '123456', '123456', '2017-12-11', 1),
(191, 1, 1, '123456', '123456', '2017-12-11', 1),
(192, 1, 1, '123456', '123456', '2017-12-11', 1),
(190, 1, 1, '123456', '123456', '2017-12-11', 1),
(189, 1, 1, '123456', '123456', '2017-12-11', 1),
(188, 1, 1, 'sdfghvfcd', 'sdfghjjhygt', '2017-12-11', 1),
(186, 1, 1, '1 prueba', 'prueba', '2017-12-11', 1),
(187, 1, 1, '2 prueba', '2 prueba', '2017-12-11', 1),
(185, 1, 1, 'nueva encuesta', 'nueva encuesta', '2017-12-11', 1),
(183, 1, 1, 'Calidad escolar', 'Calidad escolar', '2017-12-11', 1),
(184, 1, 1, 'prueba', 'prueba', '2017-12-11', 1),
(181, 1, 1, '3 encuesta', '3 encuesta', '2017-12-09', 0),
(182, 1, 1, 'CALIDAD ESCOLAR', 'ESTA ENCUESTA TIENE COMO FINALIDAD SABER COMO ESTÁ EL NIVEL EDUCATIVO DE LA ESCUELA', '2017-12-11', 1),
(179, 1, 1, '1 encuesta', '1 encuesta', '2017-12-09', 1),
(180, 1, 1, '2 encuesta', '2 encuesta', '2017-12-09', 1),
(177, 1, 1, 'encuesta 2', 'encuesta 2', '2017-12-09', 1),
(178, 1, 1, 'tutores', 'tutores', '2017-12-09', 1),
(176, 1, 1, 'encuesta', 'encuesta', '2017-12-09', 1),
(174, 1, 1, 'esto es una prueba', 'esto es una prueba', '2017-12-09', 0),
(175, 1, 1, 'prueba 1', 'esta es una prueba', '2017-12-09', 0),
(173, 1, 1, '11:33', 'prueba', '2017-12-09', 1),
(171, 1, 1, 'Prueba', 'Prueba', '2017-12-09', 1),
(172, 1, 1, '11:31 am', 'prueba', '2017-12-09', 1),
(169, 1, 1, 'dfg', 'dfg', '2017-12-09', 0),
(170, 1, 1, 'prueba', 'prueba', '2017-12-09', 1),
(167, 1, 1, 'tutores', 'tutores', '2017-12-09', 1),
(168, 1, 1, 'Tutor', 'Tutor', '2017-12-09', 1),
(166, 1, 1, 'adfv', 'asdf', '2017-12-09', 1),
(165, 1, 1, 'Prueba', 'Prueb tutot', '2017-12-09', 1),
(164, 1, 1, 'asdf', 'asdf', '2017-12-09', 1),
(163, 1, 1, 'asdf', 'asdf', '2017-12-09', 1),
(162, 1, 1, 'fxgh', 'fgj', '2017-12-09', 1),
(161, 1, 1, '11:31 am', 'prueba', '2017-12-09', 1),
(160, 1, 1, '11:33', 'prueba', '2017-12-09', 1),
(159, 1, 1, '11:31 am', 'prueba', '2017-12-09', 1),
(158, 1, 1, 'fasdf', 'asdf', '2017-12-09', 1),
(157, 1, 1, 'siguiente', 'siguiente', '2017-12-09', 1),
(156, 1, 1, 'siguiente', 'siguiente', '2017-12-09', 1),
(146, 1, 1, 'nueva encuesta', 'esta es una encuesta de prueba', '2017-12-08', 1),
(147, 1, 1, 'nueva encuesta', 'esta es una encuesta de prueba', '2017-12-08', 1),
(148, 1, 1, 'EMPRESAS SLP', 'ESTUDIO DE MERCADO', '2017-12-08', 1),
(149, 1, 1, 'prueba con \",\"', 'prueba con \",\"', '2017-12-08', 1),
(150, 1, 1, 'prueba con \",\"', 'prueba con \",\"', '2017-12-08', 1),
(151, 1, 1, 'Prueba 2', 'Prueba 2', '2017-12-08', 1),
(152, 1, 1, 'prueba con \",\"', 'prueba con \",\"', '2017-12-08', 1),
(153, 1, 1, 'prueba con \",\"', 'prueba con \",\"', '2017-12-08', 1),
(154, 1, 1, 'Prueba 2', 'Prueba 2', '2017-12-08', 1),
(155, 1, 1, 'Prueba 2', 'Prueba 2', '2017-12-08', 1),
(230, 1, 1, 'VACUNACION ESCOLAR', 'CONOCER EL UNIVERSO DE ESTUDIANTES VACUNADOS', '2017-12-13', 1),
(231, 1, 1, 'VACUNACION ESCOLAR', 'CONOCER EL UNIVERSO DE ESTUDIANTES VACUNADOS', '2017-12-13', 1),
(232, 1, 1, 'numeros', '1234', '2017-12-13', 1),
(233, 1, 1, 'numeros', '1234', '2017-12-13', 1),
(234, 1, 1, 'numeros', '1234', '2017-12-13', 1),
(235, 1, 1, 'numeros', '1234', '2017-12-13', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enc_completa`
--

CREATE TABLE `enc_completa` (
  `idEncuesta` int(11) NOT NULL,
  `idEncuestado` int(11) NOT NULL,
  `numPreguntas` tinyint(4) NOT NULL,
  `contestadas` tinyint(4) NOT NULL,
  `tipoEncuestado` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enc_completa`
--

INSERT INTO `enc_completa` (`idEncuesta`, `idEncuestado`, `numPreguntas`, `contestadas`, `tipoEncuestado`) VALUES
(2, 1, 3, 3, 1),
(2, 2, 3, 3, 1),
(2, 1, 3, 3, 3),
(2, 3, 3, 0, 3),
(2, 8, 3, 0, 3),
(2, 9, 3, 0, 3),
(2, 10, 3, 0, 3),
(7, 1, 2, 0, 3),
(7, 3, 2, 0, 3),
(7, 8, 2, 0, 3),
(7, 9, 2, 0, 3),
(7, 10, 2, 0, 3),
(9, 1, 1, 0, 3),
(9, 3, 1, 0, 3),
(9, 8, 1, 0, 3),
(9, 9, 1, 0, 3),
(9, 10, 1, 0, 3),
(11, 1, 1, 0, 3),
(12, 1, 1, 0, 3),
(13, 1, 1, 0, 3),
(13, 3, 1, 0, 3),
(13, 8, 1, 0, 3),
(13, 9, 1, 0, 3),
(13, 10, 1, 0, 3),
(14, 1, 3, 0, 1),
(15, 1, 1, 0, 1),
(16, 1, 1, 0, 1),
(17, 1, 1, 0, 3),
(17, 3, 1, 0, 3),
(17, 8, 1, 0, 3),
(17, 9, 1, 0, 3),
(17, 10, 1, 0, 3),
(18, 1, 1, 0, 3),
(18, 3, 1, 0, 3),
(18, 8, 1, 0, 3),
(18, 9, 1, 0, 3),
(18, 10, 1, 0, 3),
(19, 1, 1, 0, 1),
(21, 1, 1, 0, 1),
(22, 1, 1, 0, 3),
(22, 3, 1, 0, 3),
(22, 8, 1, 0, 3),
(22, 9, 1, 0, 3),
(22, 10, 1, 0, 3),
(23, 1, 1, 0, 1),
(24, 1, 1, 0, 1),
(25, 1, 1, 0, 1),
(26, 1, 1, 0, 1),
(27, 1, 1, 0, 1),
(28, 1, 1, 0, 1),
(29, 1, 1, 0, 3),
(29, 3, 1, 0, 3),
(29, 8, 1, 0, 3),
(29, 9, 1, 0, 3),
(29, 10, 1, 0, 3),
(30, 1, 1, 0, 1),
(31, 1, 1, 0, 1),
(32, 1, 1, 0, 1),
(33, 1, 2, 0, 1),
(34, 1, 1, 0, 3),
(34, 3, 1, 0, 3),
(34, 8, 1, 0, 3),
(34, 9, 1, 0, 3),
(34, 10, 1, 0, 3),
(35, 1, 1, 0, 2),
(35, 3, 1, 0, 2),
(35, 9, 1, 0, 2),
(35, 10, 1, 0, 2),
(35, 11, 1, 0, 2),
(36, 1, 1, 0, 1),
(37, 1, 1, 0, 2),
(37, 3, 1, 0, 2),
(37, 9, 1, 0, 2),
(37, 10, 1, 0, 2),
(37, 11, 1, 0, 2),
(38, 1, 1, 0, 1),
(39, 1, 1, 0, 3),
(39, 3, 1, 0, 3),
(39, 8, 1, 0, 3),
(39, 9, 1, 0, 3),
(39, 10, 1, 0, 3),
(40, 1, 1, 0, 1),
(41, 1, 1, 0, 1),
(42, 1, 1, 0, 2),
(42, 3, 1, 0, 2),
(42, 9, 1, 0, 2),
(42, 10, 1, 0, 2),
(42, 11, 1, 0, 2),
(43, 1, 1, 0, 1),
(44, 1, 1, 0, 3),
(44, 3, 1, 0, 3),
(44, 8, 1, 0, 3),
(44, 9, 1, 0, 3),
(44, 10, 1, 0, 3),
(45, 1, 1, 0, 3),
(45, 3, 1, 0, 3),
(45, 8, 1, 0, 3),
(45, 9, 1, 0, 3),
(45, 10, 1, 0, 3),
(46, 1, 1, 0, 1),
(46, 2, 1, 0, 1),
(47, 1, 1, 0, 1),
(47, 2, 1, 0, 1),
(48, 1, 1, 0, 1),
(49, 1, 1, 0, 1),
(50, 1, 1, 0, 3),
(51, 1, 1, 0, 3),
(51, 3, 1, 0, 3),
(51, 8, 1, 0, 3),
(51, 9, 1, 0, 3),
(51, 10, 1, 0, 3),
(52, 1, 1, 0, 3),
(52, 3, 1, 0, 3),
(52, 8, 1, 0, 3),
(52, 9, 1, 0, 3),
(52, 10, 1, 0, 3),
(53, 1, 1, 0, 3),
(53, 3, 1, 0, 3),
(53, 8, 1, 0, 3),
(53, 9, 1, 0, 3),
(53, 10, 1, 0, 3),
(54, 1, 1, 0, 3),
(55, 1, 1, 0, 3),
(55, 3, 1, 0, 3),
(55, 8, 1, 0, 3),
(55, 9, 1, 0, 3),
(55, 10, 1, 0, 3),
(56, 1, 1, 0, 1),
(56, 2, 1, 0, 1),
(56, 1, 1, 0, 2),
(56, 3, 1, 0, 2),
(56, 9, 1, 0, 2),
(56, 10, 1, 0, 2),
(56, 11, 1, 0, 2),
(56, 1, 1, 0, 3),
(56, 3, 1, 0, 3),
(56, 8, 1, 0, 3),
(56, 9, 1, 0, 3),
(56, 10, 1, 0, 3),
(57, 1, 3, 0, 3),
(57, 3, 3, 0, 3),
(57, 8, 3, 0, 3),
(57, 9, 3, 0, 3),
(57, 10, 3, 0, 3),
(58, 1, 1, 0, 3),
(58, 3, 1, 0, 3),
(58, 8, 1, 0, 3),
(58, 9, 1, 0, 3),
(58, 10, 1, 0, 3),
(59, 1, 1, 0, 3),
(59, 3, 1, 0, 3),
(59, 8, 1, 0, 3),
(59, 9, 1, 0, 3),
(59, 10, 1, 0, 3),
(60, 1, 1, 0, 3),
(60, 3, 1, 0, 3),
(60, 8, 1, 0, 3),
(60, 9, 1, 0, 3),
(60, 10, 1, 0, 3),
(61, 1, 1, 0, 1),
(61, 2, 1, 1, 1),
(62, 1, 2, 0, 1),
(62, 2, 2, 2, 1),
(63, 1, 6, 0, 1),
(63, 2, 6, 0, 1),
(64, 1, 2, 0, 1),
(64, 2, 2, 0, 1),
(65, 1, 2, 0, 1),
(65, 2, 2, 2, 1),
(66, 1, 1, 1, 3),
(66, 3, 1, 0, 3),
(66, 8, 1, 0, 3),
(66, 9, 1, 0, 3),
(66, 10, 1, 0, 3),
(67, 1, 1, 0, 1),
(67, 2, 1, 1, 1),
(68, 1, 1, 0, 1),
(68, 2, 1, 1, 1),
(69, 1, 3, 0, 3),
(69, 3, 3, 0, 3),
(69, 8, 3, 0, 3),
(69, 9, 3, 0, 3),
(69, 10, 3, 0, 3),
(70, 1, 1, 0, 1),
(70, 2, 1, 1, 1),
(71, 1, 1, 0, 1),
(71, 2, 1, 1, 1),
(72, 1, 3, 0, 1),
(72, 2, 3, 3, 1),
(73, 1, 3, 0, 1),
(73, 2, 3, 3, 1),
(74, 1, 3, 3, 1),
(74, 2, 3, 3, 1),
(75, 1, 3, 0, 1),
(76, 1, 3, 0, 1),
(77, 1, 3, 0, 1),
(78, 1, 3, 0, 1),
(78, 2, 3, 3, 1),
(79, 1, 3, 0, 1),
(79, 2, 3, 3, 1),
(80, 1, 3, 0, 1),
(80, 2, 3, 3, 1),
(81, 1, 3, 0, 1),
(81, 2, 3, 3, 1),
(82, 1, 1, 0, 1),
(82, 2, 1, 1, 1),
(83, 1, 1, 0, 1),
(83, 2, 1, 1, 1),
(84, 1, 1, 0, 1),
(84, 2, 1, 1, 1),
(84, 1, 1, 0, 3),
(84, 3, 1, 0, 3),
(84, 8, 1, 0, 3),
(84, 9, 1, 0, 3),
(84, 10, 1, 0, 3),
(85, 1, 1, 0, 1),
(85, 2, 1, 1, 1),
(85, 1, 1, 0, 3),
(85, 3, 1, 0, 3),
(85, 8, 1, 0, 3),
(85, 9, 1, 0, 3),
(85, 10, 1, 0, 3),
(86, 1, 1, 0, 1),
(86, 2, 1, 1, 1),
(86, 1, 1, 0, 3),
(86, 3, 1, 0, 3),
(86, 8, 1, 0, 3),
(86, 9, 1, 0, 3),
(86, 10, 1, 0, 3),
(87, 1, 1, 0, 1),
(88, 1, 1, 0, 1),
(88, 2, 1, 1, 1),
(92, 1, 1, 0, 1),
(92, 2, 1, 1, 1),
(92, 1, 1, 0, 3),
(92, 3, 1, 0, 3),
(92, 8, 1, 0, 3),
(92, 9, 1, 0, 3),
(92, 10, 1, 0, 3),
(95, 1, 1, 0, 1),
(95, 2, 1, 1, 1),
(108, 1, 1, 0, 1),
(108, 2, 1, 1, 1),
(109, 1, 1, 0, 1),
(109, 2, 1, 1, 1),
(110, 1, 3, 0, 1),
(110, 2, 3, 1, 1),
(112, 1, 3, 0, 1),
(112, 2, 3, 3, 1),
(113, 1, 1, 0, 1),
(113, 2, 1, 1, 1),
(113, 1, 1, 0, 3),
(113, 3, 1, 0, 3),
(113, 8, 1, 0, 3),
(113, 9, 1, 0, 3),
(113, 10, 1, 0, 3),
(114, 1, 1, 0, 1),
(114, 2, 1, 1, 1),
(114, 1, 1, 0, 3),
(114, 3, 1, 0, 3),
(114, 8, 1, 0, 3),
(114, 9, 1, 0, 3),
(114, 10, 1, 0, 3),
(115, 1, 1, 0, 1),
(115, 2, 1, 1, 1),
(116, 1, 4, 0, 3),
(116, 3, 4, 0, 3),
(116, 8, 4, 0, 3),
(116, 9, 4, 0, 3),
(116, 10, 4, 0, 3),
(117, 1, 3, 0, 3),
(117, 3, 3, 0, 3),
(117, 8, 3, 0, 3),
(117, 9, 3, 0, 3),
(117, 10, 3, 0, 3),
(118, 1, 1, 0, 2),
(118, 3, 1, 1, 2),
(118, 9, 1, 0, 2),
(118, 10, 1, 0, 2),
(118, 11, 1, 0, 2),
(118, 1, 1, 0, 3),
(118, 3, 1, 1, 3),
(118, 8, 1, 0, 3),
(118, 9, 1, 0, 3),
(118, 10, 1, 0, 3),
(119, 1, 3, 0, 1),
(119, 2, 3, 3, 1),
(119, 1, 3, 0, 2),
(119, 3, 3, 3, 2),
(119, 9, 3, 0, 2),
(119, 10, 3, 0, 2),
(119, 11, 3, 0, 2),
(119, 1, 3, 0, 3),
(119, 3, 3, 3, 3),
(119, 8, 3, 0, 3),
(119, 9, 3, 0, 3),
(119, 10, 3, 0, 3),
(120, 1, 1, 0, 1),
(120, 2, 1, 1, 1),
(122, 1, 5, 0, 1),
(122, 2, 5, 5, 1),
(127, 1, 3, 0, 1),
(127, 2, 3, 3, 1),
(128, 1, 3, 0, 1),
(128, 2, 3, 3, 1),
(130, 1, 4, 0, 1),
(130, 2, 4, 4, 1),
(134, 1, 4, 0, 1),
(134, 2, 4, 4, 1),
(136, 1, 4, 0, 1),
(136, 2, 4, 4, 1),
(136, 1, 4, 0, 2),
(136, 3, 4, 0, 2),
(136, 9, 4, 0, 2),
(136, 10, 4, 0, 2),
(136, 11, 4, 0, 2),
(138, 1, 2, 0, 1),
(138, 2, 2, 1, 1),
(139, 1, 2, 0, 1),
(139, 2, 2, 1, 1),
(140, 1, 2, 0, 1),
(140, 2, 2, 1, 1),
(141, 1, 2, 0, 1),
(141, 2, 2, 1, 1),
(142, 1, 2, 0, 1),
(142, 2, 2, 1, 1),
(143, 1, 2, 0, 1),
(143, 2, 2, 2, 1),
(144, 1, 4, 0, 1),
(144, 2, 4, 4, 1),
(145, 1, 4, 0, 1),
(145, 2, 4, 4, 1),
(146, 1, 5, 0, 1),
(146, 2, 5, 5, 1),
(147, 1, 5, 0, 1),
(147, 2, 5, 0, 1),
(148, 1, 3, 3, 1),
(148, 2, 3, 3, 1),
(148, 1, 3, 3, 2),
(148, 3, 3, 0, 2),
(148, 9, 3, 0, 2),
(148, 10, 3, 0, 2),
(148, 11, 3, 0, 2),
(148, 37, 3, 0, 2),
(148, 38, 3, 0, 2),
(148, 1, 3, 3, 3),
(148, 3, 3, 0, 3),
(148, 8, 3, 0, 3),
(148, 9, 3, 0, 3),
(148, 10, 3, 0, 3),
(149, 1, 1, 0, 1),
(149, 2, 1, 1, 1),
(150, 1, 1, 0, 1),
(150, 2, 1, 0, 1),
(151, 1, 2, 0, 1),
(151, 2, 2, 0, 1),
(152, 1, 1, 0, 1),
(152, 2, 1, 0, 1),
(153, 1, 1, 0, 1),
(153, 2, 1, 1, 1),
(154, 1, 2, 0, 1),
(154, 2, 2, 1, 1),
(155, 1, 2, 0, 1),
(155, 2, 2, 1, 1),
(156, 1, 3, 0, 1),
(156, 2, 3, 3, 1),
(157, 1, 2, 0, 1),
(157, 2, 2, 1, 1),
(158, 1, 3, 0, 1),
(158, 2, 3, 3, 1),
(159, 1, 4, 4, 1),
(159, 2, 4, 4, 1),
(159, 1, 4, 4, 3),
(159, 3, 4, 0, 3),
(159, 8, 4, 0, 3),
(159, 9, 4, 0, 3),
(159, 10, 4, 0, 3),
(160, 1, 4, 4, 3),
(160, 3, 4, 0, 3),
(160, 8, 4, 0, 3),
(160, 9, 4, 0, 3),
(160, 10, 4, 0, 3),
(161, 1, 4, 4, 3),
(161, 3, 4, 0, 3),
(161, 8, 4, 0, 3),
(161, 9, 4, 0, 3),
(161, 10, 4, 0, 3),
(162, 1, 1, 0, 3),
(162, 3, 1, 0, 3),
(162, 8, 1, 0, 3),
(162, 9, 1, 0, 3),
(162, 10, 1, 0, 3),
(163, 1, 1, 0, 3),
(163, 3, 1, 0, 3),
(163, 8, 1, 0, 3),
(163, 9, 1, 0, 3),
(163, 10, 1, 0, 3),
(164, 1, 1, 0, 3),
(164, 3, 1, 0, 3),
(164, 8, 1, 0, 3),
(164, 9, 1, 0, 3),
(164, 10, 1, 0, 3),
(165, 1, 3, 3, 3),
(165, 3, 3, 0, 3),
(165, 8, 3, 0, 3),
(165, 9, 3, 0, 3),
(165, 10, 3, 0, 3),
(166, 1, 1, 1, 3),
(166, 3, 1, 0, 3),
(166, 8, 1, 0, 3),
(166, 9, 1, 0, 3),
(166, 10, 1, 0, 3),
(167, 1, 1, 1, 3),
(167, 3, 1, 0, 3),
(167, 8, 1, 0, 3),
(167, 9, 1, 0, 3),
(167, 10, 1, 0, 3),
(168, 1, 1, 1, 3),
(168, 3, 1, 0, 3),
(168, 8, 1, 0, 3),
(168, 9, 1, 0, 3),
(168, 10, 1, 0, 3),
(169, 1, 1, 1, 1),
(169, 2, 1, 0, 1),
(169, 1, 1, 1, 2),
(169, 3, 1, 0, 2),
(169, 9, 1, 0, 2),
(169, 10, 1, 0, 2),
(169, 11, 1, 0, 2),
(169, 37, 1, 0, 2),
(169, 38, 1, 0, 2),
(169, 1, 1, 1, 3),
(169, 3, 1, 0, 3),
(169, 8, 1, 0, 3),
(169, 9, 1, 0, 3),
(169, 10, 1, 0, 3),
(170, 1, 1, 0, 1),
(170, 2, 1, 1, 1),
(171, 1, 1, 1, 3),
(171, 3, 1, 0, 3),
(171, 8, 1, 0, 3),
(171, 9, 1, 0, 3),
(171, 10, 1, 0, 3),
(172, 1, 4, 4, 3),
(172, 3, 4, 0, 3),
(172, 8, 4, 0, 3),
(172, 9, 4, 0, 3),
(172, 10, 4, 0, 3),
(173, 1, 4, 4, 3),
(173, 3, 4, 0, 3),
(173, 8, 4, 0, 3),
(173, 9, 4, 0, 3),
(173, 10, 4, 0, 3),
(174, 1, 6, 6, 1),
(174, 2, 6, 0, 1),
(174, 1, 6, 6, 3),
(174, 3, 6, 0, 3),
(174, 8, 6, 0, 3),
(174, 9, 6, 0, 3),
(174, 10, 6, 0, 3),
(175, 1, 6, 0, 1),
(175, 2, 6, 0, 1),
(176, 1, 2, 0, 1),
(176, 2, 2, 2, 1),
(177, 1, 4, 0, 1),
(177, 2, 4, 4, 1),
(178, 1, 6, 6, 3),
(178, 3, 6, 0, 3),
(178, 8, 6, 0, 3),
(178, 9, 6, 0, 3),
(178, 10, 6, 0, 3),
(179, 1, 6, 6, 1),
(179, 2, 6, 6, 1),
(179, 1, 6, 6, 3),
(179, 3, 6, 0, 3),
(179, 8, 6, 0, 3),
(179, 9, 6, 0, 3),
(179, 10, 6, 0, 3),
(180, 1, 6, 0, 1),
(180, 2, 6, 6, 1),
(181, 1, 6, 0, 1),
(181, 2, 6, 0, 1),
(181, 1, 6, 0, 3),
(181, 3, 6, 0, 3),
(181, 8, 6, 0, 3),
(181, 9, 6, 0, 3),
(181, 10, 6, 0, 3),
(182, 1, 2, 0, 1),
(182, 2, 2, 1, 1),
(183, 1, 2, 0, 1),
(183, 2, 2, 2, 1),
(184, 1, 6, 6, 1),
(184, 2, 6, 6, 1),
(185, 1, 6, 6, 1),
(185, 2, 6, 6, 1),
(186, 1, 6, 6, 1),
(186, 2, 6, 6, 1),
(187, 1, 2, 2, 1),
(187, 2, 2, 2, 1),
(188, 1, 2, 0, 1),
(188, 2, 2, 2, 1),
(189, 1, 3, 3, 1),
(189, 2, 3, 3, 1),
(189, 1, 3, 3, 3),
(189, 3, 3, 0, 3),
(189, 8, 3, 0, 3),
(189, 9, 3, 0, 3),
(189, 10, 3, 0, 3),
(190, 1, 3, 3, 1),
(190, 2, 3, 3, 1),
(190, 1, 3, 3, 3),
(190, 3, 3, 0, 3),
(190, 8, 3, 0, 3),
(190, 9, 3, 0, 3),
(190, 10, 3, 0, 3),
(191, 1, 3, 3, 1),
(191, 2, 3, 3, 1),
(191, 1, 3, 3, 3),
(191, 3, 3, 0, 3),
(191, 8, 3, 0, 3),
(191, 9, 3, 0, 3),
(191, 10, 3, 0, 3),
(192, 1, 3, 0, 1),
(192, 2, 3, 0, 1),
(193, 1, 3, 3, 3),
(193, 3, 3, 0, 3),
(193, 8, 3, 0, 3),
(193, 9, 3, 0, 3),
(193, 10, 3, 0, 3),
(194, 1, 3, 3, 1),
(194, 2, 3, 3, 1),
(194, 1, 3, 3, 3),
(194, 3, 3, 0, 3),
(194, 8, 3, 0, 3),
(194, 9, 3, 0, 3),
(194, 10, 3, 0, 3),
(195, 1, 3, 3, 1),
(195, 2, 3, 0, 1),
(195, 1, 3, 3, 3),
(195, 3, 3, 0, 3),
(195, 8, 3, 0, 3),
(195, 9, 3, 0, 3),
(195, 10, 3, 0, 3),
(196, 1, 3, 3, 1),
(196, 2, 3, 3, 1),
(196, 1, 3, 3, 3),
(196, 3, 3, 0, 3),
(196, 8, 3, 0, 3),
(196, 9, 3, 0, 3),
(196, 10, 3, 0, 3),
(197, 1, 3, 0, 1),
(197, 2, 3, 3, 1),
(198, 1, 3, 3, 1),
(198, 2, 3, 3, 1),
(198, 1, 3, 3, 3),
(198, 3, 3, 0, 3),
(198, 8, 3, 0, 3),
(198, 9, 3, 0, 3),
(198, 10, 3, 0, 3),
(199, 1, 1, 1, 1),
(199, 2, 1, 0, 1),
(199, 1, 1, 1, 3),
(199, 3, 1, 0, 3),
(199, 8, 1, 0, 3),
(199, 9, 1, 0, 3),
(199, 10, 1, 0, 3),
(200, 1, 1, 1, 1),
(200, 2, 1, 0, 1),
(200, 1, 1, 1, 3),
(200, 3, 1, 0, 3),
(200, 8, 1, 0, 3),
(200, 9, 1, 0, 3),
(200, 10, 1, 0, 3),
(201, 1, 5, 5, 1),
(201, 2, 5, 5, 1),
(201, 1, 5, 5, 3),
(201, 3, 5, 5, 3),
(201, 8, 5, 0, 3),
(201, 9, 5, 5, 3),
(201, 10, 5, 5, 3),
(202, 1, 2, 2, 1),
(202, 2, 2, 1, 1),
(202, 1, 2, 2, 3),
(202, 3, 2, 2, 3),
(202, 8, 2, 0, 3),
(202, 9, 2, 0, 3),
(202, 10, 2, 0, 3),
(203, 1, 2, 0, 1),
(203, 2, 2, 1, 1),
(203, 1, 2, 0, 3),
(203, 3, 2, 0, 3),
(203, 8, 2, 0, 3),
(203, 9, 2, 0, 3),
(203, 10, 2, 0, 3),
(204, 1, 2, 0, 1),
(204, 2, 2, 1, 1),
(205, 1, 4, 4, 3),
(205, 3, 4, 0, 3),
(205, 8, 4, 0, 3),
(205, 9, 4, 0, 3),
(205, 10, 4, 0, 3),
(207, 1, 4, 0, 1),
(207, 2, 4, 0, 1),
(207, 1, 4, 0, 2),
(207, 3, 4, 0, 2),
(207, 9, 4, 0, 2),
(207, 10, 4, 0, 2),
(207, 11, 4, 0, 2),
(207, 37, 4, 0, 2),
(207, 38, 4, 0, 2),
(207, 1, 4, 0, 3),
(207, 3, 4, 0, 3),
(207, 8, 4, 0, 3),
(207, 9, 4, 0, 3),
(207, 10, 4, 0, 3),
(208, 1, 1, 0, 1),
(208, 2, 1, 0, 1),
(208, 1, 1, 0, 3),
(208, 3, 1, 0, 3),
(208, 8, 1, 0, 3),
(208, 9, 1, 0, 3),
(208, 10, 1, 0, 3),
(209, 1, 1, 1, 3),
(209, 3, 1, 0, 3),
(209, 8, 1, 0, 3),
(209, 9, 1, 0, 3),
(209, 10, 1, 0, 3),
(210, 1, 4, 4, 1),
(210, 2, 4, 0, 1),
(210, 1, 4, 0, 2),
(210, 3, 4, 4, 2),
(210, 9, 4, 0, 2),
(210, 10, 4, 0, 2),
(210, 11, 4, 0, 2),
(210, 37, 4, 0, 2),
(210, 38, 4, 0, 2),
(210, 1, 4, 4, 3),
(210, 3, 4, 4, 3),
(210, 8, 4, 0, 3),
(210, 9, 4, 0, 3),
(210, 10, 4, 0, 3),
(211, 1, 4, 4, 1),
(211, 2, 4, 4, 1),
(211, 1, 4, 0, 2),
(211, 3, 4, 0, 2),
(211, 9, 4, 0, 2),
(211, 10, 4, 0, 2),
(211, 11, 4, 0, 2),
(211, 37, 4, 0, 2),
(211, 38, 4, 0, 2),
(211, 1, 4, 0, 3),
(211, 3, 4, 4, 3),
(211, 8, 4, 0, 3),
(211, 9, 4, 0, 3),
(211, 10, 4, 0, 3),
(212, 1, 5, 0, 1),
(212, 2, 5, 5, 1),
(212, 1, 5, 0, 2),
(212, 3, 5, 0, 2),
(212, 9, 5, 0, 2),
(212, 10, 5, 0, 2),
(212, 11, 5, 0, 2),
(212, 37, 5, 0, 2),
(212, 38, 5, 0, 2),
(212, 1, 5, 0, 3),
(212, 3, 5, 0, 3),
(212, 8, 5, 0, 3),
(212, 9, 5, 0, 3),
(212, 10, 5, 0, 3),
(213, 1, 5, 5, 3),
(213, 3, 5, 0, 3),
(213, 8, 5, 0, 3),
(213, 9, 5, 0, 3),
(213, 10, 5, 0, 3),
(214, 1, 5, 0, 3),
(214, 3, 5, 5, 3),
(214, 8, 5, 0, 3),
(214, 9, 5, 0, 3),
(214, 10, 5, 0, 3),
(215, 1, 5, 0, 3),
(215, 3, 5, 0, 3),
(215, 8, 5, 0, 3),
(215, 9, 5, 0, 3),
(215, 10, 5, 0, 3),
(216, 12, 6, 4, 1),
(217, 12, 6, 5, 1),
(218, 12, 1, 1, 1),
(219, 12, 3, 3, 1),
(220, 12, 2, 2, 1),
(221, 12, 1, 0, 1),
(222, 12, 1, 0, 1),
(223, 12, 3, 1, 1),
(224, 12, 2, 2, 1),
(225, 1, 2, 0, 1),
(225, 2, 2, 1, 1),
(225, 1, 2, 0, 3),
(225, 3, 2, 1, 3),
(225, 8, 2, 0, 3),
(225, 9, 2, 0, 3),
(225, 10, 2, 0, 3),
(226, 1, 3, 0, 1),
(226, 2, 3, 0, 1),
(226, 1, 3, 0, 3),
(226, 3, 3, 3, 3),
(226, 8, 3, 0, 3),
(226, 9, 3, 0, 3),
(226, 10, 3, 0, 3),
(227, 1, 3, 0, 1),
(227, 2, 3, 3, 1),
(227, 1, 3, 0, 3),
(227, 3, 3, 3, 3),
(227, 8, 3, 0, 3),
(227, 9, 3, 0, 3),
(227, 10, 3, 0, 3),
(228, 1, 3, 0, 1),
(228, 2, 3, 3, 1),
(228, 1, 3, 0, 3),
(228, 3, 3, 3, 3),
(228, 8, 3, 0, 3),
(228, 9, 3, 0, 3),
(228, 10, 3, 0, 3),
(229, 1, 3, 3, 1),
(229, 2, 3, 3, 1),
(229, 1, 3, 3, 3),
(229, 3, 3, 0, 3),
(229, 8, 3, 0, 3),
(229, 9, 3, 0, 3),
(229, 10, 3, 0, 3),
(229, 29, 3, 0, 3),
(229, 30, 3, 0, 3),
(229, 31, 3, 0, 3),
(229, 32, 3, 3, 3),
(230, 1, 3, 3, 1),
(230, 2, 3, 3, 1),
(230, 1, 3, 3, 3),
(230, 3, 3, 0, 3),
(230, 8, 3, 0, 3),
(230, 9, 3, 0, 3),
(230, 10, 3, 0, 3),
(230, 29, 3, 0, 3),
(230, 30, 3, 0, 3),
(230, 31, 3, 0, 3),
(230, 32, 3, 3, 3),
(231, 1, 3, 0, 2),
(231, 3, 3, 0, 2),
(231, 9, 3, 0, 2),
(231, 10, 3, 0, 2),
(231, 11, 3, 0, 2),
(231, 37, 3, 0, 2),
(231, 38, 3, 0, 2),
(231, 39, 3, 0, 2),
(231, 40, 3, 0, 2),
(231, 41, 3, 0, 2),
(231, 42, 3, 0, 2),
(231, 43, 3, 0, 2),
(232, 1, 4, 0, 2),
(232, 3, 4, 0, 2),
(232, 9, 4, 0, 2),
(232, 10, 4, 0, 2),
(232, 11, 4, 0, 2),
(232, 37, 4, 0, 2),
(232, 38, 4, 0, 2),
(232, 39, 4, 0, 2),
(232, 40, 4, 0, 2),
(232, 41, 4, 0, 2),
(232, 42, 4, 0, 2),
(232, 43, 4, 0, 2),
(233, 1, 4, 0, 1),
(233, 2, 4, 0, 1),
(233, 1, 4, 0, 3),
(233, 3, 4, 4, 3),
(233, 8, 4, 0, 3),
(233, 9, 4, 0, 3),
(233, 10, 4, 0, 3),
(233, 29, 4, 0, 3),
(233, 30, 4, 4, 3),
(233, 31, 4, 0, 3),
(233, 32, 4, 0, 3),
(233, 33, 4, 4, 3),
(234, 1, 4, 4, 1),
(234, 2, 4, 0, 1),
(234, 1, 4, 4, 3),
(234, 3, 4, 4, 3),
(234, 8, 4, 0, 3),
(234, 9, 4, 0, 3),
(234, 10, 4, 0, 3),
(234, 29, 4, 0, 3),
(234, 30, 4, 4, 3),
(234, 31, 4, 0, 3),
(234, 32, 4, 4, 3),
(234, 33, 4, 4, 3),
(235, 1, 4, 4, 1),
(235, 2, 4, 0, 1),
(235, 1, 4, 4, 3),
(235, 3, 4, 4, 3),
(235, 8, 4, 0, 3),
(235, 9, 4, 0, 3),
(235, 10, 4, 0, 3),
(235, 29, 4, 0, 3),
(235, 30, 4, 4, 3),
(235, 31, 4, 0, 3),
(235, 32, 4, 4, 3),
(235, 33, 4, 4, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enc_preguntas`
--

CREATE TABLE `enc_preguntas` (
  `idPregunta` int(11) NOT NULL,
  `idEncuesta` int(11) NOT NULL,
  `nombrePregunta` varchar(200) NOT NULL,
  `tipoRespuesta` tinyint(4) NOT NULL,
  `opciones` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enc_preguntas`
--

INSERT INTO `enc_preguntas` (`idPregunta`, `idEncuesta`, `nombrePregunta`, `tipoRespuesta`, `opciones`) VALUES
(445, 212, 'pregunta 3', 3, '¨¨¨¨¨'),
(446, 212, 'pregunta 4', 2, 'a¨b¨c¨d¨e¨'),
(447, 212, 'pregunta 5', 3, '¨¨¨¨¨'),
(448, 213, 'pregunta 1', 2, 'a¨e¨i¨o¨u¨'),
(443, 212, 'pregunta 1', 2, 'a¨e¨i¨o¨u¨'),
(444, 212, 'pregunta 2', 1, '¨¨¨¨¨'),
(442, 211, '¿Tu hijo tiene computadora en casa?', 1, '¨¨¨¨¨'),
(441, 211, '¿Desearías que tu hijo tenga una capacitación adicional de computación?', 1, '¨¨¨¨¨'),
(440, 211, '¿Cuántas horas pasa tu hijo frente a una computadora?', 2, '1. 0 Horas¨2. 1 Hora¨3. 2 Horas¨4. 3 Horas¨¨'),
(439, 211, 'En una escala de 1 al 10 ¿Qué interés tiene tu hijo en computación?', 3, '¨¨¨¨¨'),
(438, 210, '¿Tu hijo tiene computadora en casa?', 1, '¨¨¨¨¨'),
(437, 210, '¿Desearías que tu hijo tenga una capacitación adicional de computación?', 1, '¨¨¨¨¨'),
(436, 210, '¿Cuántas horas pasa tu hijo frente a una computadora?', 2, '1. 0 Horas¨2. 1 Hora¨3. 2 Horas¨4. 3 Horas¨¨'),
(434, 209, '1', 1, '¨¨¨¨¨'),
(435, 210, 'En una escala de 1 al 10 ¿Qué interés tiene tu hijo en computación?', 3, '¨¨¨¨¨'),
(432, 207, '¿Tu hijo tiene computadora en casa?', 1, '¨¨¨¨¨'),
(433, 208, '1', 1, '¨¨¨¨¨'),
(431, 207, '¿Desearías que tu hijo tenga una capacitación adicional de computación?', 1, '¨¨¨¨¨'),
(430, 207, '¿Cuántas horas pasa tu hijo frente a una computadora?', 2, '1. 0 Horas¨2. 1 Hora¨3. 2 Horas¨4. 3 Horas¨¨'),
(429, 207, 'En una escala de 1 al 10 ¿Qué interés tiene tu hijo en computación?', 3, '¨¨¨¨¨'),
(428, 206, '1', 1, '¨¨¨¨¨'),
(427, 205, '¿Desearías que tu hijo tenga una capacitación adicional de computación?', 1, '¨¨¨¨¨'),
(426, 205, 'En una escala de 1 al 10 ¿Qué interés tiene tu hijo en computación?', 3, '¨¨¨¨¨'),
(425, 205, '¿Cuántas horas pasa tu hijo frente a una computadora?', 2, '1. 0 Horas¨2. 1 Hora¨3. 2 Horas¨4. 3 Horas¨¨'),
(424, 205, '¿Tu hijo tiene computadora en casa?', 1, '¨¨¨¨¨'),
(423, 204, 'COMO CALIFICAS LAS METRICAS UTILIZADAS?', 2, 'A. EXCELENTES¨B. BUENAS¨C. REGULARESD¨D. REGULARES¨E. BUENAS¨'),
(422, 204, 'COMO CALIFICAS LA EDUCACIÓN EN LA ESCUELA?', 3, '¨¨¨¨¨'),
(421, 203, 'COMO CALIFICAS LAS METRICAS UTILIZADAS?', 2, 'A. EXCELENTES¨B. BUENAS¨C. REGULARESD¨D. REGULARES¨E. BUENAS¨'),
(420, 203, 'COMO CALIFICAS LA EDUCACIÓN EN LA ESCUELA?', 3, '¨¨¨¨¨'),
(419, 202, 'COMO CALIFICAS LAS METRICAS UTILIZADAS?', 2, 'A. EXCELENTES¨B. BUENAS¨C. REGULARESD¨D. REGULARES¨E. BUENAS¨'),
(418, 202, 'COMO CALIFICAS LA EDUCACIÓN EN LA ESCUELA?', 3, '¨¨¨¨¨'),
(417, 201, 'pregunta 5', 3, '¨¨¨¨¨'),
(416, 201, 'pregunta 4', 1, '¨¨¨¨¨'),
(415, 201, 'pregunta 3', 2, 'a¨b¨c¨d¨e¨'),
(414, 201, 'pregunta 2', 3, '¨¨¨¨¨'),
(413, 201, 'pregunta 1', 1, '¨¨¨¨¨'),
(412, 200, 's', 1, '¨¨¨¨¨'),
(411, 199, 'pregunta 1', 2, 'a¨b¨c¨d¨e¨'),
(410, 198, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(409, 198, 'pregunta 2', 3, '¨¨¨¨¨'),
(408, 198, 'pregunta 1', 1, '¨¨¨¨¨'),
(407, 197, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(406, 197, 'pregunta 2', 3, '¨¨¨¨¨'),
(405, 197, 'pregunta 1', 1, '¨¨¨¨¨'),
(404, 196, 'pregunta 3', 2, 'opcion 1¨opcion 2¨opcion 3¨opcion 4¨opcion 5¨'),
(403, 196, 'pregunta 2', 3, '¨¨¨¨¨'),
(402, 196, 'pregunta 1', 1, '¨¨¨¨¨'),
(401, 195, 'pregunta 3', 2, 'opcion 1¨opcion 2¨opcion 3¨opcion 4¨opcion 5¨'),
(400, 195, 'pregunta 2', 3, '¨¨¨¨¨'),
(399, 195, 'pregunta 1', 1, '¨¨¨¨¨'),
(398, 194, '3', 2, 'a¨b¨c¨d¨e¨'),
(397, 194, '2', 3, '¨¨¨¨¨'),
(396, 194, '1', 1, '¨¨¨¨¨'),
(395, 193, '3', 2, 'a¨b¨c¨d¨e¨'),
(394, 193, '2', 3, '¨¨¨¨¨'),
(393, 193, '1', 1, '¨¨¨¨¨'),
(392, 192, '3', 2, 'a¨b¨c¨d¨e¨'),
(391, 192, '2', 3, '¨¨¨¨¨'),
(390, 192, '1', 1, '¨¨¨¨¨'),
(389, 191, '3', 2, 'a¨b¨c¨d¨e¨'),
(388, 191, '2', 3, '¨¨¨¨¨'),
(387, 191, '1', 1, '¨¨¨¨¨'),
(386, 190, '3', 2, 'a¨b¨c¨d¨e¨'),
(385, 190, '2', 3, '¨¨¨¨¨'),
(384, 190, '1', 1, '¨¨¨¨¨'),
(383, 189, '3', 2, 'a¨b¨c¨d¨e¨'),
(382, 189, '2', 3, '¨¨¨¨¨'),
(379, 188, '1', 3, '¨¨¨¨¨'),
(380, 188, '2', 1, '¨¨¨¨¨'),
(381, 189, '1', 1, '¨¨¨¨¨'),
(378, 187, 'pregunta 2', 1, '¨¨¨¨¨'),
(377, 187, 'pregunta 1', 3, '¨¨¨¨¨'),
(374, 186, '4', 3, '¨¨¨¨¨'),
(375, 186, '5', 2, 'a¨e¨i¨o¨u¨'),
(376, 186, '6', 2, '1¨2¨3¨4¨5¨'),
(372, 186, '2', 1, '¨¨¨¨¨'),
(373, 186, '3', 1, '¨¨¨¨¨'),
(371, 186, '1', 3, '¨¨¨¨¨'),
(370, 185, 'pregunta 6', 2, 'a¨b¨c¨d¨e¨'),
(368, 185, 'pregunta 4', 1, '¨¨¨¨¨'),
(369, 185, 'pregunta 5', 3, '¨¨¨¨¨'),
(367, 185, 'pregunta 3', 2, 'a¨e¨i¨o¨u¨'),
(366, 185, 'pregunta 2', 3, '¨¨¨¨¨'),
(365, 185, 'pregunta 1', 1, '¨¨¨¨¨'),
(364, 184, 'vocales', 2, 'a¨e¨i¨o¨u¨'),
(363, 184, '5', 2, '1¨2¨3¨4¨5¨'),
(362, 184, 'niveles', 2, 'primero¨segundo¨tercero¨cuarto¨quinto¨'),
(361, 184, 'números', 2, 'uno¨dos¨tres¨cuatro¨cinco¨'),
(359, 184, '1', 2, '1¨2¨3¨4¨5¨'),
(360, 184, 'vocales', 2, 'a¨e¨i¨o¨u¨'),
(358, 183, '¿Cómo calificarías la educación en la escuela', 3, '¨¨¨¨¨'),
(357, 183, '¿Cómo calificarías las métricas utilizadas?', 2, 'excelente¨buena¨regular¨mala¨¨'),
(356, 182, 'COMO CALIFICAS LAS METRICAS UTILIZADAS?', 2, 'A. EXCELENTES¨B. BUENAS¨C. REGULARESD¨D. REGULARES¨E. BUENAS¨'),
(355, 182, 'COMO CALIFICAS LA EDUCACIÓN EN LA ESCUELA?', 3, '¨¨¨¨¨'),
(354, 181, '6', 2, 'primera, parte¨segunda, parte¨tercera, parte¨cuarta, parte¨quinta, parte¨'),
(352, 181, '4', 0, '¨¨¨¨¨'),
(353, 181, '5', 1, '¨¨¨¨¨'),
(350, 181, '2', 3, '¨¨¨¨¨'),
(351, 181, '3', 1, '¨¨¨¨¨'),
(349, 181, '1', 1, '¨¨¨¨¨'),
(348, 180, 'pregunta 6', 2, 'a¨e¨i¨o¨u¨'),
(347, 180, 'pregunta 5', 2, 'a, SI¨b, no¨c, no se¨d, tal vez¨¨'),
(346, 180, 'pregunta 4', 2, '1¨2¨3¨4¨5¨'),
(345, 180, 'pregunta 3', 1, '¨¨¨¨¨'),
(343, 180, 'pregunta 1', 1, '¨¨¨¨¨'),
(344, 180, 'pregunta 2', 3, '¨¨¨¨¨'),
(342, 179, '6', 1, '¨¨¨¨¨'),
(340, 179, '4', 1, '¨¨¨¨¨'),
(341, 179, '5', 3, '¨¨¨¨¨'),
(339, 179, '3', 2, 'a, primero¨b, segundo¨c, tercero¨d, cuarto¨e, quinto¨'),
(338, 179, '2', 2, '1¨2¨3¨4¨5¨'),
(337, 179, '1', 1, '¨¨¨¨¨'),
(335, 178, '5', 3, '¨¨¨¨¨'),
(336, 178, '6', 1, '¨¨¨¨¨'),
(334, 178, '4', 1, '¨¨¨¨¨'),
(332, 178, '2', 1, '¨¨¨¨¨'),
(333, 178, '3', 3, '¨¨¨¨¨'),
(330, 177, '4', 3, '¨¨¨¨¨'),
(331, 178, '1', 2, '1¨2¨3¨4¨5¨'),
(329, 177, '3', 1, '¨¨¨¨¨'),
(327, 177, '1', 1, '¨¨¨¨¨'),
(328, 177, '2', 2, '1¨2¨3¨4¨5¨'),
(325, 176, '1', 2, '1¨2¨3¨4¨5¨'),
(326, 176, '2', 2, '1¨2¨3¨4¨5¨'),
(324, 175, 'prueba 6', 1, '¨¨¨¨¨'),
(323, 175, 'prueba 5', 3, '¨¨¨¨¨'),
(322, 175, 'pregunta 4', 2, 'primero¨segundo¨tercero¨cuarto¨quinto¨'),
(321, 175, 'pregunta 3', 3, '¨¨¨¨¨'),
(320, 175, 'pregunta 2', 2, 'opcion a¨opcion b¨opcion c¨opcion d¨opcion d¨'),
(319, 175, 'pregunta 1', 1, '¨¨¨¨¨'),
(318, 174, '6', 1, '¨¨¨¨¨'),
(317, 174, '5', 2, 'uno¨dos¨tres¨cuatro¨cinco¨'),
(316, 174, '4', 1, '¨¨¨¨¨'),
(315, 174, '3', 3, '¨¨¨¨¨'),
(314, 174, '2', 2, 'a¨b¨c¨d¨e¨'),
(313, 174, '1', 1, '¨¨¨¨¨'),
(312, 173, 'prueba4', 3, '¨¨¨¨¨'),
(311, 173, 'prueba3', 2, 'A. 1¨B. 2¨C. 3¨D. 4¨E. 5¨'),
(310, 173, 'prueba2', 1, '¨¨¨¨¨'),
(309, 173, 'prueba1', 1, '¨¨¨¨¨'),
(308, 172, 'prueba4', 3, '¨¨¨¨¨'),
(307, 172, 'prueba3', 2, 'A. 1¨B. 2¨C. 3¨D. 4¨3. 5¨'),
(306, 172, 'prueba2', 1, '¨¨¨¨¨'),
(305, 172, 'prueba1', 1, '¨¨¨¨¨'),
(304, 171, 'Te llego?', 1, '¨¨¨¨¨'),
(303, 170, 'Te llego?', 1, '¨¨¨¨¨'),
(302, 169, 'sdfg', 1, '¨¨¨¨¨'),
(301, 168, 'te llego?', 1, '¨¨¨¨¨'),
(300, 167, 'tutores', 1, '¨¨¨¨¨'),
(299, 166, 'asdf', 1, '¨¨¨¨¨'),
(298, 165, 'asd', 1, '¨¨¨¨¨'),
(297, 165, 'asd', 1, '¨¨¨¨¨'),
(296, 165, 'adf', 1, '¨¨¨¨¨'),
(295, 164, 'asdf', 1, '¨¨¨¨¨'),
(294, 163, 'asdf', 1, '¨¨¨¨¨'),
(293, 162, 'asd', 1, '¨¨¨¨¨'),
(292, 161, 'prueba4', 3, '¨¨¨¨¨'),
(291, 161, 'prueba3', 2, 'A. 1¨B. 2¨C. 3¨D. 4¨3. 5¨'),
(290, 161, 'prueba2', 1, '¨¨¨¨¨'),
(289, 161, 'prueba1', 1, '¨¨¨¨¨'),
(288, 160, 'prueba4', 3, '¨¨¨¨¨'),
(287, 160, 'prueba3', 2, 'A. 1¨B. 2¨C. 3¨D. 4¨E. 5¨'),
(286, 160, 'prueba2', 1, '¨¨¨¨¨'),
(285, 160, 'prueba1', 1, '¨¨¨¨¨'),
(284, 159, 'prueba4', 3, '¨¨¨¨¨'),
(281, 159, 'prueba1', 1, '¨¨¨¨¨'),
(282, 159, 'prueba2', 1, '¨¨¨¨¨'),
(283, 159, 'prueba3', 2, 'A. 1¨B. 2¨C. 3¨D. 4¨3. 5¨'),
(280, 158, 'sadf', 3, '¨¨¨¨¨'),
(279, 158, 'asdf', 2, 'asdf¨asdf¨asdf¨asdf¨asdf¨'),
(277, 157, 'asdf', 2, 'a¨t¨e¨v¨f¨'),
(278, 158, 'asdf', 1, '¨¨¨¨¨'),
(275, 156, 'adsf', 3, '¨¨¨¨¨'),
(276, 157, 'adsf', 3, '¨¨¨¨¨'),
(274, 156, 'asdf', 2, 'a¨t¨e¨v¨f¨'),
(273, 156, 'asdf', 1, '¨¨¨¨¨'),
(267, 152, 'pregunta uno', 2, 'a, primero¨b, segundo¨c, tercero¨d, cuarto¨e, quinto¨'),
(268, 153, 'pregunta uno', 2, 'a, primero¨b, segundo¨c, tercero¨d, cuarto¨e, quinto¨'),
(269, 154, 'Pregunta 1', 1, ',,,,,'),
(270, 154, 'Pregunta 2', 2, 'Jean¨JM¨Mario¨Marco¨Dulce¨'),
(271, 155, 'Pregunta 1', 1, ',,,,,'),
(272, 155, 'Pregunta 2', 2, 'Jean¨JM¨Mario¨Marco¨Dulce¨'),
(449, 213, 'pregunta 2', 1, '¨¨¨¨¨'),
(450, 213, 'pregunta 3', 3, '¨¨¨¨¨'),
(451, 213, 'pregunta 4', 2, 'a¨b¨c¨d¨e¨'),
(452, 213, 'pregunta 5', 3, '¨¨¨¨¨'),
(453, 214, 'pregunta 1', 2, 'a¨e¨i¨o¨u¨'),
(454, 214, 'pregunta 2', 1, '¨¨¨¨¨'),
(455, 214, 'pregunta 3', 3, '¨¨¨¨¨'),
(456, 214, 'pregunta 4', 2, 'a¨b¨c¨d¨e¨'),
(457, 214, 'pregunta 5', 3, '¨¨¨¨¨'),
(458, 215, 'pregunta 1', 2, 'a¨e¨i¨o¨u¨'),
(459, 215, 'pregunta 2', 1, '¨¨¨¨¨'),
(460, 215, 'pregunta 3', 3, '¨¨¨¨¨'),
(461, 215, 'pregunta 4', 2, 'a¨b¨c¨d¨e¨'),
(462, 215, 'pregunta 5', 3, '¨¨¨¨¨'),
(463, 216, 'pregunta 1', 1, '¨¨¨¨¨'),
(464, 216, 'pregunta 2', 2, 'a¨b¨c¨d¨e¨'),
(465, 216, 'pregunta 3', 3, '¨¨¨¨¨'),
(466, 216, 'pregunta 4', 1, '¨¨¨¨¨'),
(467, 216, 'pregunta 5', 2, 'opcion 1¨opcion 2¨opcion 3¨opcion 4¨opcion 5¨'),
(468, 216, 'pregunta 6', 2, 'a¨b¨c¨d¨e¨'),
(469, 217, 'pregunta 1', 1, '¨¨¨¨¨'),
(470, 217, 'pregunta 2', 2, 'a¨b¨c¨d¨e¨'),
(471, 217, 'pregunta 3', 1, '¨¨¨¨¨'),
(472, 217, 'pregunta 4', 3, '¨¨¨¨¨'),
(473, 217, 'pregunta 5', 1, '¨¨¨¨¨'),
(474, 217, 'pregunta 6', 2, 'a¨e¨i¨o¨u¨'),
(475, 218, '1', 1, '¨¨¨¨¨'),
(476, 219, 'pregunta 1', 2, 'a¨e¨i¨o¨u¨'),
(477, 219, 'pregunta 2', 2, 'a¨b¨c¨d¨e¨'),
(478, 219, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(479, 220, '1', 2, 'a¨e¨i¨o¨u¨'),
(480, 220, '2', 2, '1¨2¨3¨4¨5¨'),
(481, 221, 'Uno', 2, 'A¨B¨C¨D¨E¨'),
(482, 222, 'Uno', 2, 'A¨B¨C¨D¨E¨'),
(483, 223, 'Uno', 2, 'A¨B¨C¨D¨E¨'),
(484, 223, 'Dos', 2, 'A¨B¨C¨D¨E¨'),
(485, 223, 'Tres', 2, 'A¨B¨C¨D¨E¨'),
(486, 224, 'Uno', 2, 'A¨B¨C¨D¨E¨'),
(487, 224, 'Dos', 2, '1¨2¨3¨4¨5¨'),
(488, 225, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN', 1, '¨¨¨¨¨'),
(489, 225, 'COMO CALIFICAS EL BENEFICIO DE LAS VACUNS', 0, '¨¨¨¨¨'),
(490, 226, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN?', 1, '¨¨¨¨¨'),
(491, 226, 'COMO CALIFICAS LA INFORMACIÓN ACERCA DE LA VACUNA PARA LA INFLUENZA?', 2, 'A. EXCELENTE¨B. BUENO¨C. REGULAR¨D. MALO¨E. PESIMO¨'),
(492, 226, 'CALIFICA DEL 1 AL 10 COMO CONSIDERAS LOS BENEFICIOS DE LAS VACUNAS', 3, '¨¨¨¨¨'),
(493, 227, 'CALIFICA DEL 1 AL 10 COMO CONSIDERAS LOS BENEFICIOS DE LAS VACUNAS', 3, '¨¨¨¨¨'),
(494, 227, 'COMO CALIFICAS LA INFORMACIÓN ACERCA DE LA VACUNA PARA LA INFLUENZA?', 2, 'A. EXCELENTE¨B. BUENO¨C. REGULAR¨D. MALO¨E. PESIMO¨'),
(495, 227, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN?', 1, '¨¨¨¨¨'),
(496, 228, 'CALIFICA DEL 1 AL 10 COMO CONSIDERAS LOS BENEFICIOS DE LAS VACUNAS', 3, '¨¨¨¨¨'),
(497, 228, 'COMO CALIFICAS LA INFORMACIÓN ACERCA DE LA VACUNA PARA LA INFLUENZA?', 2, 'A. EXCELENTE¨B. BUENO¨C. REGULAR¨D. MALO¨E. PESIMO¨'),
(498, 228, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN?', 1, '¨¨¨¨¨'),
(499, 229, 'CALIFICA DEL 1 AL 10 COMO CONSIDERAS LOS BENEFICIOS DE LAS VACUNAS', 3, '¨¨¨¨¨'),
(500, 229, 'COMO CALIFICAS LA INFORMACIÓN ACERCA DE LA VACUNA PARA LA INFLUENZA?', 2, 'A. EXCELENTE¨B. BUENO¨C. REGULAR¨D. MALO¨E. PESIMO¨'),
(501, 229, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN?', 1, '¨¨¨¨¨'),
(502, 230, 'CALIFICA DEL 1 AL 10 COMO CONSIDERAS LOS BENEFICIOS DE LAS VACUNAS', 3, '¨¨¨¨¨'),
(503, 230, 'COMO CALIFICAS LA INFORMACIÓN ACERCA DE LA VACUNA PARA LA INFLUENZA?', 2, 'A. EXCELENTE¨B. BUENO¨C. REGULAR¨D. MALO¨E. PESIMO¨'),
(504, 230, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN?', 1, '¨¨¨¨¨'),
(505, 231, 'CALIFICA DEL 1 AL 10 COMO CONSIDERAS LOS BENEFICIOS DE LAS VACUNAS', 3, '¨¨¨¨¨'),
(506, 231, 'COMO CALIFICAS LA INFORMACIÓN ACERCA DE LA VACUNA PARA LA INFLUENZA?', 2, 'A. EXCELENTE¨B. BUENO¨C. REGULAR¨D. MALO¨E. PESIMO¨'),
(507, 231, 'TU HIJO TIENE CARTILLA DE VACUNACIÓN?', 1, '¨¨¨¨¨'),
(508, 232, 'pregunta 1', 3, '¨¨¨¨¨'),
(509, 232, 'pregunta 2', 3, '¨¨¨¨¨'),
(510, 232, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(511, 232, 'pregunta 4', 2, '1¨2¨3¨4¨5¨'),
(512, 233, 'pregunta 1', 3, '¨¨¨¨¨'),
(513, 233, 'pregunta 2', 3, '¨¨¨¨¨'),
(514, 233, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(515, 233, 'pregunta 4', 2, '1¨2¨3¨4¨5¨'),
(516, 234, 'pregunta 1', 3, '¨¨¨¨¨'),
(517, 234, 'pregunta 2', 3, '¨¨¨¨¨'),
(518, 234, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(519, 234, 'pregunta 4', 2, '1¨2¨3¨4¨5¨'),
(520, 235, 'pregunta 1', 3, '¨¨¨¨¨'),
(521, 235, 'pregunta 2', 3, '¨¨¨¨¨'),
(522, 235, 'pregunta 3', 2, '1¨2¨3¨4¨5¨'),
(523, 235, 'pregunta 4', 2, '1¨2¨3¨4¨5¨');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enc_respuestas`
--

CREATE TABLE `enc_respuestas` (
  `id` int(11) NOT NULL,
  `idEncuestado` int(11) NOT NULL,
  `tipoEncuestado` tinyint(4) NOT NULL,
  `idPregunta` int(11) NOT NULL,
  `respuesta` tinyint(4) NOT NULL,
  `idEncuesta` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enc_respuestas`
--

INSERT INTO `enc_respuestas` (`id`, `idEncuestado`, `tipoEncuestado`, `idPregunta`, `respuesta`, `idEncuesta`) VALUES
(1, 1, 3, 4, 1, 2),
(2, 1, 3, 5, 9, 2),
(3, 1, 3, 6, 1, 2),
(4, 21, 1, 4, 1, 2),
(5, 21, 1, 5, 8, 2),
(6, 21, 1, 6, 1, 2),
(8, 1, 1, 1, 1, 1),
(9, 2, 1, 4, 1, 2),
(10, 2, 1, 5, 8, 2),
(11, 2, 1, 6, 1, 2),
(12, 2, 1, 4, 1, 2),
(13, 2, 1, 5, 8, 2),
(14, 2, 1, 6, 1, 2),
(15, 1, 1, 1, 1, 1),
(16, 2, 1, 71, 1, 61),
(17, 2, 1, 73, 1, 62),
(18, 2, 1, 72, 1, 62),
(19, 2, 1, 82, 1, 65),
(20, 2, 1, 83, 9, 65),
(21, 2, 1, 83, 9, 65),
(22, 1, 3, 84, 1, 66),
(23, 2, 1, 84, 0, 66),
(24, 2, 1, 84, 0, 66),
(25, 2, 1, 85, 0, 67),
(26, 2, 1, 85, 0, 67),
(27, 1, 1, 100, 2, 74),
(28, 1, 1, 99, 1, 74),
(29, 1, 1, 98, 8, 74),
(30, 2, 1, 110, 1, 78),
(31, 2, 1, 112, 8, 78),
(32, 2, 1, 113, 1, 79),
(33, 2, 1, 115, 10, 79),
(34, 2, 1, 116, 1, 80),
(35, 2, 1, 118, 10, 80),
(36, 2, 1, 150, 1, 110),
(37, 2, 1, 156, 1, 112),
(38, 2, 1, 158, 9, 112),
(39, 2, 1, 94, 1, 72),
(40, 2, 1, 93, 3, 72),
(41, 2, 1, 92, 8, 72),
(42, 2, 1, 161, 1, 115),
(43, 2, 1, 162, 1, 116),
(44, 2, 1, 163, 0, 116),
(45, 2, 1, 165, 7, 116),
(46, 3, 3, 169, 1, 118),
(47, 3, 3, 171, 9, 119),
(48, 3, 3, 172, 1, 119),
(49, 2, 1, 177, 2, 122),
(50, 2, 1, 179, 0, 122),
(51, 2, 1, 180, 3, 122),
(52, 2, 1, 181, 3, 122),
(53, 2, 1, 195, 8, 127),
(54, 2, 1, 196, 0, 127),
(55, 2, 1, 203, 1, 130),
(56, 2, 1, 204, 0, 130),
(57, 2, 1, 206, 9, 130),
(58, 2, 1, 216, 6, 134),
(59, 2, 1, 217, 1, 134),
(60, 2, 1, 219, 0, 134),
(61, 2, 1, 224, 1, 136),
(62, 2, 1, 225, 1, 136),
(63, 2, 1, 227, 1, 136),
(64, 2, 1, 230, 1, 138),
(65, 2, 1, 232, 1, 139),
(66, 2, 1, 234, 1, 140),
(67, 2, 1, 236, 1, 141),
(68, 2, 1, 238, 1, 142),
(69, 2, 1, 242, 2, 144),
(70, 2, 1, 243, 3, 144),
(71, 2, 1, 244, 4, 144),
(72, 2, 1, 245, 7, 144),
(73, 2, 1, 250, 1, 146),
(74, 2, 1, 251, 2, 146),
(75, 2, 1, 252, 2, 146),
(76, 2, 1, 253, 2, 146),
(77, 2, 1, 254, 2, 146),
(78, 2, 1, 246, 1, 145),
(79, 2, 1, 247, 2, 145),
(80, 2, 1, 248, 2, 145),
(81, 2, 1, 249, 4, 145),
(82, 2, 1, 86, 1, 68),
(83, 2, 1, 90, 1, 70),
(84, 2, 1, 91, 1, 71),
(85, 2, 1, 97, 1, 73),
(86, 2, 1, 96, 1, 73),
(87, 2, 1, 95, 2, 73),
(88, 2, 1, 100, 1, 74),
(89, 2, 1, 99, 2, 74),
(90, 2, 1, 98, 1, 74),
(91, 2, 1, 119, 1, 81),
(92, 2, 1, 120, 1, 81),
(93, 2, 1, 121, 2, 81),
(94, 2, 1, 122, 1, 82),
(95, 2, 1, 123, 1, 83),
(96, 2, 1, 124, 1, 84),
(97, 2, 1, 125, 1, 85),
(98, 2, 1, 126, 1, 86),
(99, 2, 1, 132, 1, 92),
(100, 2, 1, 128, 1, 88),
(101, 2, 1, 135, 1, 95),
(102, 2, 1, 148, 1, 108),
(103, 2, 1, 149, 1, 109),
(104, 2, 1, 159, 1, 113),
(105, 2, 1, 160, 1, 114),
(106, 2, 1, 170, 1, 119),
(107, 2, 1, 171, 2, 119),
(108, 2, 1, 172, 1, 119),
(109, 2, 1, 197, 1, 128),
(110, 2, 1, 198, 2, 128),
(111, 2, 1, 199, 1, 128),
(112, 2, 1, 173, 1, 120),
(113, 2, 1, 240, 1, 143),
(114, 2, 1, 241, 1, 143),
(115, 1, 3, 261, 1, 148),
(116, 1, 3, 262, 3, 148),
(117, 2, 1, 261, 0, 148),
(118, 2, 1, 262, 10, 148),
(119, 2, 1, 263, 3, 149),
(120, 2, 1, 269, 0, 154),
(121, 2, 1, 271, 0, 155),
(122, 2, 1, 276, 1, 157),
(123, 2, 1, 278, 0, 158),
(124, 2, 1, 280, 10, 158),
(125, 2, 1, 273, 1, 156),
(126, 2, 1, 275, 7, 156),
(127, 2, 1, 281, 0, 159),
(128, 2, 1, 282, 0, 159),
(129, 2, 1, 284, 10, 159),
(130, 1, 3, 299, 1, 166),
(131, 1, 3, 300, 0, 167),
(132, 2, 1, 303, 1, 170),
(133, 1, 3, 304, 1, 171),
(134, 1, 3, 305, 1, 172),
(135, 1, 3, 306, 0, 172),
(136, 1, 3, 308, 8, 172),
(137, 1, 3, 309, 1, 173),
(138, 1, 3, 310, 0, 173),
(139, 1, 3, 312, 8, 173),
(140, 1, 3, 313, 1, 174),
(141, 1, 3, 315, 9, 174),
(142, 1, 3, 316, 0, 174),
(143, 1, 3, 318, 0, 174),
(144, 1, 3, 284, 1, 159),
(145, 1, 3, 281, 1, 159),
(146, 1, 3, 282, 1, 159),
(147, 1, 3, 283, 4, 159),
(148, 1, 3, 292, 2, 161),
(149, 1, 3, 291, 2, 161),
(150, 1, 3, 290, 4, 161),
(151, 1, 3, 289, 1, 161),
(152, 1, 3, 288, 2, 160),
(153, 1, 3, 287, 1, 160),
(154, 1, 3, 286, 3, 160),
(155, 1, 3, 285, 8, 160),
(156, 2, 1, 330, 2, 177),
(157, 2, 1, 329, 2, 177),
(158, 2, 1, 327, 1, 177),
(159, 2, 1, 328, 2, 177),
(160, 2, 1, 325, 2, 176),
(161, 2, 1, 326, 2, 176),
(162, 1, 3, 302, 1, 169),
(163, 1, 3, 301, 1, 168),
(164, 1, 3, 298, 1, 165),
(165, 1, 3, 297, 2, 165),
(166, 1, 3, 296, 2, 165),
(167, 1, 3, 335, 2, 178),
(168, 1, 3, 336, 1, 178),
(169, 1, 3, 334, 2, 178),
(170, 1, 3, 332, 2, 178),
(171, 1, 3, 333, 4, 178),
(172, 1, 3, 331, 1, 178),
(173, 2, 1, 342, 2, 179),
(174, 2, 1, 340, 5, 179),
(175, 2, 1, 341, 1, 179),
(176, 2, 1, 339, 1, 179),
(177, 2, 1, 338, 3, 179),
(178, 2, 1, 337, 1, 179),
(179, 2, 1, 348, 1, 180),
(180, 2, 1, 347, 2, 180),
(181, 2, 1, 346, 1, 180),
(182, 2, 1, 345, 2, 180),
(183, 2, 1, 343, 3, 180),
(184, 2, 1, 344, 4, 180),
(185, 2, 1, 354, 2, 181),
(186, 2, 1, 352, 8, 181),
(187, 2, 1, 353, 1, 181),
(188, 1, 3, 342, 1, 179),
(189, 1, 3, 340, 2, 179),
(190, 1, 3, 341, 2, 179),
(191, 1, 3, 339, 1, 179),
(192, 1, 3, 338, 2, 179),
(193, 1, 3, 337, 1, 179),
(194, 2, 1, 355, 8, 182),
(195, 2, 1, 358, 1, 183),
(196, 2, 1, 357, 9, 183),
(197, 2, 1, 364, 3, 184),
(198, 2, 1, 363, 2, 184),
(199, 2, 1, 362, 4, 184),
(200, 2, 1, 361, 1, 184),
(201, 2, 1, 359, 4, 184),
(202, 2, 1, 360, 5, 184),
(203, 1, 1, 364, 3, 184),
(204, 1, 1, 363, 3, 184),
(205, 1, 1, 362, 3, 184),
(206, 1, 1, 361, 4, 184),
(207, 1, 1, 359, 4, 184),
(208, 1, 1, 360, 5, 184),
(209, 2, 1, 370, 2, 185),
(210, 2, 1, 368, 4, 185),
(211, 2, 1, 369, 3, 185),
(212, 2, 1, 367, 1, 185),
(213, 2, 1, 366, 7, 185),
(214, 2, 1, 365, 4, 185),
(215, 1, 1, 370, 1, 185),
(216, 1, 1, 368, 6, 185),
(217, 1, 1, 369, 4, 185),
(218, 1, 1, 367, 1, 185),
(219, 1, 1, 366, 6, 185),
(220, 1, 1, 365, 4, 185),
(221, 2, 1, 374, 6, 186),
(222, 2, 1, 375, 2, 186),
(223, 2, 1, 376, 1, 186),
(224, 2, 1, 372, 6, 186),
(225, 2, 1, 373, 2, 186),
(226, 2, 1, 371, 3, 186),
(227, 1, 1, 374, 3, 186),
(228, 1, 1, 375, 1, 186),
(229, 1, 1, 376, 2, 186),
(230, 1, 1, 372, 3, 186),
(231, 1, 1, 373, 3, 186),
(232, 1, 1, 371, 4, 186),
(233, 2, 1, 378, 1, 187),
(234, 2, 1, 377, 1, 187),
(235, 1, 1, 378, 7, 187),
(236, 1, 1, 377, 1, 187),
(237, 2, 1, 379, 6, 188),
(238, 2, 1, 380, 1, 188),
(239, 1, 1, 379, 2, 188),
(240, 1, 1, 380, 2, 188),
(241, 1, 3, 383, 1, 189),
(242, 1, 3, 382, 7, 189),
(243, 1, 3, 381, 2, 189),
(244, 2, 1, 383, 2, 189),
(245, 2, 1, 382, 6, 189),
(246, 2, 1, 381, 5, 189),
(247, 2, 1, 383, 2, 189),
(248, 2, 1, 382, 6, 189),
(249, 2, 1, 381, 5, 189),
(250, 2, 1, 268, 4, 153),
(251, 2, 1, 386, 1, 190),
(252, 2, 1, 385, 5, 190),
(253, 2, 1, 384, 3, 190),
(254, 2, 1, 386, 1, 190),
(255, 2, 1, 385, 10, 190),
(256, 2, 1, 384, 4, 190),
(257, 2, 1, 384, 1, 190),
(258, 2, 1, 385, 10, 190),
(259, 2, 1, 386, 4, 190),
(260, 2, 1, 384, 1, 190),
(261, 2, 1, 385, 10, 190),
(262, 2, 1, 386, 4, 190),
(263, 1, 1, 384, 1, 190),
(264, 1, 1, 385, 5, 190),
(265, 1, 1, 386, 1, 190),
(266, 2, 1, 387, 1, 191),
(267, 2, 1, 388, 7, 191),
(268, 2, 1, 389, 3, 191),
(269, 1, 1, 387, 1, 191),
(270, 1, 1, 388, 4, 191),
(271, 1, 1, 389, 1, 191),
(272, 1, 3, 393, 1, 193),
(273, 1, 3, 394, 8, 193),
(274, 1, 3, 395, 1, 193),
(275, 1, 3, 396, 1, 194),
(276, 1, 3, 397, 7, 194),
(277, 1, 3, 398, 1, 194),
(278, 2, 1, 396, 2, 194),
(279, 2, 1, 397, 3, 194),
(280, 2, 1, 398, 3, 194),
(281, 1, 3, 399, 1, 195),
(282, 1, 3, 400, 10, 195),
(283, 1, 3, 401, 1, 195),
(284, 1, 3, 402, 1, 196),
(285, 1, 3, 403, 9, 196),
(286, 1, 3, 404, 1, 196),
(287, 2, 1, 402, 1, 196),
(288, 2, 1, 403, 2, 196),
(289, 2, 1, 404, 1, 196),
(290, 2, 1, 405, 1, 197),
(291, 2, 1, 406, 3, 197),
(292, 2, 1, 407, 3, 197),
(293, 2, 1, 408, 0, 198),
(294, 2, 1, 409, 10, 198),
(295, 2, 1, 410, 5, 198),
(296, 1, 1, 408, 2, 198),
(297, 1, 1, 409, 1, 198),
(298, 1, 1, 410, 1, 198),
(299, 1, 1, 411, 1, 199),
(300, 1, 1, 412, 2, 200),
(301, 1, 1, 412, 2, 200),
(302, 1, 1, 412, 2, 200),
(303, 1, 1, 413, 1, 201),
(304, 1, 1, 414, 7, 201),
(305, 1, 1, 415, 1, 201),
(306, 1, 1, 416, 1, 201),
(307, 1, 1, 417, 9, 201),
(308, 2, 1, 413, 1, 201),
(309, 2, 1, 414, 4, 201),
(310, 2, 1, 415, 2, 201),
(311, 2, 1, 416, 2, 201),
(312, 2, 1, 417, 9, 201),
(313, 1, 3, 413, 1, 201),
(314, 1, 3, 414, 4, 201),
(315, 1, 3, 415, 3, 201),
(316, 1, 3, 416, 1, 201),
(317, 1, 3, 417, 8, 201),
(318, 3, 3, 413, 1, 201),
(319, 3, 3, 414, 7, 201),
(320, 3, 3, 415, 2, 201),
(321, 3, 3, 416, 1, 201),
(322, 3, 3, 417, 8, 201),
(323, 9, 3, 413, 1, 201),
(324, 9, 3, 414, 7, 201),
(325, 9, 3, 415, 5, 201),
(326, 9, 3, 416, 2, 201),
(327, 9, 3, 417, 8, 201),
(328, 10, 3, 413, 1, 201),
(329, 10, 3, 414, 7, 201),
(330, 10, 3, 415, 3, 201),
(331, 10, 3, 416, 2, 201),
(332, 10, 3, 417, 8, 201),
(333, 2, 1, 418, 10, 202),
(334, 1, 1, 418, 5, 202),
(335, 1, 1, 419, 1, 202),
(336, 1, 3, 418, 7, 202),
(337, 1, 3, 419, 1, 202),
(338, 3, 3, 418, 7, 202),
(339, 3, 3, 419, 2, 202),
(340, 2, 1, 420, 10, 203),
(341, 2, 1, 422, 2, 204),
(342, 1, 3, 424, 1, 205),
(343, 1, 3, 426, 8, 205),
(344, 1, 3, 427, 0, 205),
(345, 1, 3, 434, 1, 209),
(346, 3, 3, 435, 10, 210),
(347, 3, 3, 437, 0, 210),
(348, 3, 3, 438, 1, 210),
(349, 1, 3, 435, 8, 210),
(350, 1, 3, 436, 1, 210),
(351, 1, 3, 437, 2, 210),
(352, 1, 3, 438, 2, 210),
(353, 1, 1, 435, 6, 210),
(354, 1, 1, 436, 1, 210),
(355, 1, 1, 437, 2, 210),
(356, 1, 1, 438, 2, 210),
(357, 2, 1, 439, 10, 211),
(358, 2, 1, 440, 4, 211),
(359, 2, 1, 441, 2, 211),
(360, 2, 1, 442, 2, 211),
(361, 1, 1, 439, 10, 211),
(362, 1, 1, 440, 2, 211),
(363, 1, 1, 441, 2, 211),
(364, 1, 1, 442, 2, 211),
(365, 3, 3, 439, 8, 211),
(366, 3, 3, 440, 3, 211),
(367, 3, 3, 441, 1, 211),
(368, 3, 3, 442, 1, 211),
(369, 2, 1, 444, 1, 212),
(370, 2, 1, 445, 10, 212),
(371, 2, 1, 447, 10, 212),
(372, 1, 3, 448, 1, 213),
(373, 1, 3, 449, 2, 213),
(374, 1, 3, 450, 8, 213),
(375, 1, 3, 451, 4, 213),
(376, 1, 3, 452, 8, 213),
(377, 3, 3, 454, 0, 214),
(378, 3, 3, 455, 10, 214),
(379, 3, 3, 457, 9, 214),
(380, 2, 1, 454, 1, 214),
(381, 2, 1, 455, 7, 214),
(382, 2, 1, 457, 3, 214),
(383, 12, 1, 463, 1, 216),
(384, 12, 1, 465, 8, 216),
(385, 12, 1, 466, 0, 216),
(386, 12, 1, 469, 0, 217),
(387, 12, 1, 471, 1, 217),
(388, 12, 1, 472, 2, 217),
(389, 12, 1, 473, 1, 217),
(390, 12, 1, 475, 1, 218),
(391, 12, 1, 478, 1, 219),
(392, 12, 1, 480, 1, 220),
(393, 12, 1, 484, 1, 223),
(394, 12, 1, 486, 1, 224),
(395, 12, 1, 487, 4, 224),
(396, 2, 1, 488, 1, 225),
(397, 2, 1, 488, 1, 225),
(398, 3, 3, 488, 1, 225),
(399, 3, 3, 488, 1, 225),
(400, 3, 3, 488, 1, 225),
(401, 3, 3, 488, 1, 225),
(402, 2, 1, 488, 1, 225),
(403, 2, 1, 488, 1, 225),
(404, 3, 3, 488, 1, 225),
(405, 3, 3, 488, 1, 225),
(406, 3, 3, 488, 1, 225),
(407, 3, 3, 488, 1, 225),
(408, 3, 3, 488, 1, 225),
(409, 3, 3, 488, 1, 225),
(410, 3, 3, 488, 1, 225),
(411, 3, 3, 488, 1, 225),
(412, 3, 3, 488, 1, 225),
(413, 3, 3, 488, 1, 225),
(414, 3, 3, 488, 1, 225),
(415, 3, 3, 488, 1, 225),
(416, 3, 3, 488, 1, 225),
(417, 3, 3, 488, 1, 225),
(418, 3, 3, 488, 1, 225),
(419, 3, 3, 488, 1, 225),
(420, 3, 3, 488, 1, 225),
(421, 3, 3, 488, 1, 225),
(422, 3, 3, 488, 1, 225),
(423, 3, 3, 488, 1, 225),
(424, 3, 3, 488, 0, 225),
(425, 3, 3, 488, 0, 225),
(426, 3, 3, 488, 0, 225),
(427, 3, 3, 488, 0, 225),
(428, 3, 3, 488, 0, 225),
(429, 3, 3, 488, 0, 225),
(430, 3, 3, 488, 0, 225),
(431, 3, 3, 488, 0, 225),
(432, 3, 3, 488, 0, 225),
(433, 3, 3, 488, 0, 225),
(434, 3, 3, 488, 0, 225),
(435, 3, 3, 488, 0, 225),
(436, 3, 3, 488, 0, 225),
(437, 3, 3, 488, 0, 225),
(438, 3, 3, 488, 0, 225),
(439, 3, 3, 488, 0, 225),
(440, 3, 3, 488, 0, 225),
(441, 3, 3, 488, 0, 225),
(442, 3, 3, 488, 0, 225),
(443, 3, 3, 488, 0, 225),
(444, 3, 3, 488, 0, 225),
(445, 3, 3, 488, 0, 225),
(446, 3, 3, 488, 0, 225),
(447, 3, 3, 488, 0, 225),
(448, 3, 3, 488, 0, 225),
(449, 3, 3, 488, 0, 225),
(450, 3, 3, 488, 0, 225),
(451, 3, 3, 488, 0, 225),
(452, 3, 3, 488, 0, 225),
(453, 3, 3, 488, 0, 225),
(454, 3, 3, 488, 0, 225),
(455, 3, 3, 488, 0, 225),
(456, 2, 1, 488, 1, 225),
(457, 2, 1, 488, 1, 225),
(458, 2, 1, 488, 1, 225),
(459, 2, 1, 488, 1, 225),
(460, 2, 1, 488, 1, 225),
(461, 2, 1, 488, 1, 225),
(462, 2, 1, 488, 1, 225),
(463, 2, 1, 488, 1, 225),
(464, 2, 1, 488, 1, 225),
(465, 2, 1, 488, 1, 225),
(466, 2, 1, 488, 1, 225),
(467, 2, 1, 488, 1, 225),
(468, 2, 1, 488, 1, 225),
(469, 2, 1, 488, 1, 225),
(470, 3, 3, 490, 1, 226),
(471, 2, 1, 488, 1, 225),
(472, 2, 1, 488, 1, 225),
(473, 2, 1, 488, 1, 225),
(474, 2, 1, 488, 1, 225),
(475, 3, 3, 492, 10, 226),
(476, 3, 3, 493, 9, 227),
(477, 2, 1, 493, 1, 227),
(478, 2, 1, 495, 0, 227),
(479, 3, 3, 494, 2, 227),
(480, 3, 3, 495, 1, 227),
(481, 3, 3, 496, 10, 228),
(482, 3, 3, 497, 1, 228),
(483, 3, 3, 498, 2, 228),
(484, 2, 1, 496, 5, 228),
(485, 2, 1, 498, 2, 228),
(486, 32, 3, 499, 5, 229),
(487, 2, 1, 499, 9, 229),
(488, 32, 3, 500, 3, 229),
(489, 2, 1, 501, 0, 229),
(490, 32, 3, 501, 1, 229),
(491, 1, 3, 499, 5, 229),
(492, 1, 3, 500, 5, 229),
(493, 1, 3, 501, 2, 229),
(494, 2, 1, 502, 9, 230),
(495, 2, 1, 503, 1, 230),
(496, 2, 1, 504, 2, 230),
(497, 1, 3, 502, 2, 230),
(498, 1, 3, 503, 4, 230),
(499, 1, 3, 504, 2, 230),
(500, 32, 3, 502, 5, 230),
(501, 32, 3, 503, 5, 230),
(502, 32, 3, 504, 1, 230),
(503, 30, 3, 512, 10, 233),
(504, 30, 3, 513, 10, 233),
(505, 30, 3, 514, 5, 233),
(506, 30, 3, 515, 5, 233),
(507, 3, 3, 512, 2, 233),
(508, 3, 3, 513, 3, 233),
(509, 3, 3, 514, 4, 233),
(510, 3, 3, 515, 5, 233),
(511, 33, 3, 512, 9, 233),
(512, 33, 3, 513, 9, 233),
(513, 33, 3, 514, 4, 233),
(514, 33, 3, 515, 1, 233),
(515, 33, 3, 516, 10, 234),
(516, 33, 3, 517, 9, 234),
(517, 33, 3, 518, 4, 234),
(518, 33, 3, 519, 4, 234),
(519, 3, 3, 516, 10, 234),
(520, 1, 3, 516, 10, 234),
(521, 3, 3, 517, 10, 234),
(522, 3, 3, 518, 5, 234),
(523, 1, 3, 517, 10, 234),
(524, 1, 3, 518, 5, 234),
(525, 1, 3, 519, 5, 234),
(526, 3, 3, 519, 5, 234),
(527, 30, 3, 516, 1, 234),
(528, 30, 3, 517, 1, 234),
(529, 32, 3, 516, 7, 234),
(530, 30, 3, 518, 1, 234),
(531, 30, 3, 519, 1, 234),
(532, 32, 3, 517, 7, 234),
(533, 32, 3, 518, 4, 234),
(534, 32, 3, 519, 4, 234),
(535, 30, 3, 520, 3, 235),
(536, 1, 3, 520, 8, 235),
(537, 30, 3, 521, 4, 235),
(538, 1, 3, 521, 8, 235),
(539, 3, 3, 520, 5, 235),
(540, 1, 3, 522, 4, 235),
(541, 30, 3, 522, 3, 235),
(542, 1, 3, 523, 4, 235),
(543, 30, 3, 523, 4, 235),
(544, 3, 3, 522, 1, 235),
(545, 3, 3, 521, 10, 235),
(546, 3, 3, 523, 5, 235),
(547, 32, 3, 520, 7, 235),
(548, 32, 3, 521, 8, 235),
(549, 32, 3, 522, 1, 235),
(550, 32, 3, 523, 4, 235),
(551, 33, 3, 520, 8, 235),
(552, 33, 3, 521, 8, 235),
(553, 33, 3, 522, 4, 235),
(554, 33, 3, 523, 4, 235);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `idpais` int(11) DEFAULT NULL,
  `nombreEstado` varchar(100) DEFAULT NULL,
  `lada` varchar(10) DEFAULT NULL,
  `abreviatura` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idEstado`, `idpais`, `nombreEstado`, `lada`, `abreviatura`) VALUES
(1, 1, 'Aguascalientes', NULL, 'Ags.'),
(2, 1, 'Baja California', NULL, 'BC'),
(3, 1, 'Baja California Sur', NULL, 'BCS'),
(4, 1, 'Campeche', NULL, 'Camp.'),
(5, 1, 'Coahuila de Zaragoza', NULL, 'Coah.'),
(6, 1, 'Colima', NULL, 'Col.'),
(7, 1, 'Chiapas', NULL, 'Chis.'),
(8, 1, 'Chihuahua', NULL, 'Chih.'),
(9, 1, 'Distrito Federal', NULL, 'DF'),
(10, 1, 'Durango', NULL, 'Dgo.'),
(11, 1, 'Guanajuato', NULL, 'Gto.'),
(12, 1, 'Guerrero', NULL, 'Gro.'),
(13, 1, 'Hidalgo', NULL, 'Hgo.'),
(14, 1, 'Jalisco', NULL, 'Jal.'),
(15, 1, 'México', NULL, 'Mex.'),
(16, 1, 'Michoacán de Ocampo', NULL, 'Mich.'),
(17, 1, 'Morelos', NULL, 'Mor.'),
(18, 1, 'Nayarit', NULL, 'Nay.'),
(19, 1, 'Nuevo León', NULL, 'NL'),
(20, 1, 'Oaxaca', NULL, 'Oax.'),
(21, 1, 'Puebla', NULL, 'Pue.'),
(22, 1, 'Querétaro', NULL, 'Qro.'),
(23, 1, 'Quintana Roo', NULL, 'Q. Roo'),
(24, 1, 'San Luis Potosí', NULL, 'SLP'),
(25, 1, 'Sinaloa', NULL, 'Sin.'),
(26, 1, 'Sonora', NULL, 'Son.'),
(27, 1, 'Tabasco', NULL, 'Tab.'),
(28, 1, 'Tamaulipas', NULL, 'Tamps.'),
(29, 1, 'Tlaxcala', NULL, 'Tlax.'),
(30, 1, 'Veracruz de Ignacio de la Llave', NULL, 'Ver.'),
(31, 1, 'Yucatán', NULL, 'Yuc.'),
(32, 1, 'Zacatecas', NULL, 'Zac.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado`
--

CREATE TABLE `grado` (
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL,
  `grado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grado`
--

INSERT INTO `grado` (`idInstitucion`, `idNivel`, `grado`) VALUES
(1, 2, 1),
(1, 2, 2),
(1, 2, 3),
(1, 2, 4),
(1, 2, 5),
(1, 2, 6),
(1, 3, 1),
(1, 3, 2),
(1, 3, 3),
(1, 4, 1),
(1, 4, 2),
(1, 4, 3),
(1, 4, 4),
(1, 4, 5),
(1, 4, 6),
(8, 1, 1),
(8, 1, 2),
(8, 1, 3),
(8, 2, 1),
(8, 2, 2),
(8, 2, 3),
(8, 2, 4),
(8, 2, 5),
(8, 2, 6),
(8, 3, 1),
(8, 3, 2),
(8, 3, 3),
(8, 4, 1),
(8, 4, 2),
(8, 4, 3),
(8, 4, 4),
(8, 4, 5),
(8, 4, 6),
(10, 1, 1),
(10, 1, 2),
(10, 1, 3),
(10, 3, 1),
(10, 3, 2),
(10, 3, 3),
(11, 2, 1),
(11, 2, 2),
(11, 2, 3),
(11, 2, 4),
(11, 2, 5),
(11, 2, 6),
(13, 1, 1),
(13, 1, 2),
(13, 1, 3),
(13, 2, 1),
(13, 2, 2),
(13, 2, 3),
(13, 2, 4),
(13, 2, 5),
(13, 2, 6),
(15, 2, 1),
(15, 2, 2),
(15, 2, 3),
(15, 2, 4),
(15, 2, 5),
(15, 2, 6),
(16, 3, 1),
(17, 2, 1),
(17, 2, 2),
(17, 2, 3),
(17, 2, 4),
(17, 2, 5),
(17, 2, 6),
(18, 4, 1),
(18, 4, 2),
(18, 4, 3),
(23, 2, 1),
(23, 2, 2),
(23, 2, 3),
(24, 2, 1),
(24, 2, 2),
(24, 2, 3),
(25, 3, 1),
(25, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `idgrupo` int(40) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL,
  `grado` int(11) NOT NULL,
  `grupo` varchar(2) NOT NULL,
  `contrasena` varchar(45) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `materianombre` varchar(50) NOT NULL DEFAULT '1',
  `idmateria` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`idgrupo`, `idInstitucion`, `idNivel`, `grado`, `grupo`, `contrasena`, `status`, `materianombre`, `idmateria`) VALUES
(1, 1, 2, 1, 'A', 'ACCXTUF4', 1, 'español', 1),
(2, 1, 2, 1, 'B', '8M4TSIDM', 1, 'matematica', 2),
(3, 1, 2, 1, 'C', '8VNLQIXE', 1, 'inglés', 3),
(4, 1, 2, 2, 'A', '0TO1CMV6', 1, '0', 0),
(5, 1, 2, 2, 'B', 'D8YWDJWR', 1, '0', 0),
(6, 1, 2, 2, 'C', '1V7VL2TU', 1, '0', 0),
(7, 1, 2, 3, 'A', 'QG7RQXC7', 1, '0', 0),
(8, 1, 2, 3, 'B', '9ROAJU8L', 1, '0', 0),
(9, 1, 2, 3, 'C', 'BJF8SM8R', 1, '0', 0),
(10, 1, 2, 4, 'A', 'PI5B8Z4G', 1, '0', 0),
(11, 1, 2, 4, 'B', '4FMALOLE', 1, '0', 0),
(12, 1, 2, 4, 'C', 'D2KWNGGE', 1, '0', 0),
(13, 1, 2, 5, 'A', '56RX43G5', 1, '0', 0),
(14, 1, 2, 5, 'B', '8IY4RAAF', 1, '0', 0),
(15, 1, 2, 5, 'C', 'ESW6CTRZ', 1, '0', 0),
(16, 1, 2, 6, 'A', 'CRBWMGLK', 1, '0', 0),
(17, 1, 2, 6, 'B', 'U8Z1AXJW', 1, '0', 0),
(18, 1, 2, 6, 'C', 'OVNA6YCS', 1, '0', 0),
(19, 1, 3, 1, 'A', 'JI2GZCSX', 1, '0', 0),
(20, 1, 3, 1, 'B', '7UYAGTK3', 1, '0', 0),
(21, 1, 3, 1, 'C', 'FM3HD4H1', 1, '0', 0),
(22, 1, 3, 2, 'A', 'YM8UG09I', 1, '0', 0),
(23, 1, 3, 2, 'B', 'JL34H8O9', 1, '0', 0),
(24, 1, 3, 2, 'C', 'XAG06KF0', 1, '0', 0),
(25, 1, 3, 3, 'A', 'VE2BAPBK', 1, '0', 0),
(26, 1, 3, 3, 'B', '4ROSLF2M', 1, '0', 0),
(27, 1, 3, 3, 'C', 'F205UIGY', 1, '0', 0),
(28, 1, 4, 1, 'A', 'J3U5RO6B', 1, '0', 0),
(29, 1, 4, 1, 'B', '7KXJ4N1F', 1, '0', 0),
(30, 1, 4, 1, 'C', '69Z3N8R3', 1, '0', 0),
(31, 1, 4, 2, 'A', '7XSJ3530', 1, '0', 0),
(32, 1, 4, 2, 'B', '1A3N85SZ', 1, '0', 0),
(33, 1, 4, 2, 'C', 'E3N4SZMP', 1, '0', 0),
(34, 1, 4, 3, 'A', '356SJEBY', 1, '0', 0),
(35, 1, 4, 3, 'B', 'U5GB76MH', 1, '0', 0),
(36, 1, 4, 3, 'C', 'EU8IK0A0', 1, '0', 0),
(37, 1, 4, 4, 'A', 'H093DP3C', 1, '0', 0),
(38, 1, 4, 4, 'B', 'WE9UBYQ6', 1, '0', 0),
(39, 1, 4, 4, 'C', 'CQ0H7K0A', 1, '0', 0),
(40, 1, 4, 5, 'A', 'MS8RS9NF', 1, '0', 0),
(41, 1, 4, 5, 'B', 'GAQFSSFC', 1, '0', 0),
(42, 1, 4, 5, 'C', 'K079ZW1S', 1, '0', 0),
(43, 1, 4, 6, 'A', 'QPFZ3CAH', 1, '0', 0),
(44, 1, 4, 6, 'B', 'Q34DVVW8', 1, '0', 0),
(45, 1, 4, 6, 'C', '9Y2538MQ', 1, '0', 0),
(46, 8, 1, 1, 'A', '1FJNW0R2', 1, '1', 1),
(47, 8, 1, 1, 'B', '6DWH36PZ', 1, '1', 1),
(48, 8, 1, 1, 'C', 'NS0666FB', 1, '1', 1),
(49, 8, 1, 1, 'D', 'OEAMVAHW', 1, '1', 1),
(50, 8, 1, 2, 'A', 'P1JL1BO8', 1, '1', 1),
(51, 8, 1, 2, 'B', 'OKQRQGRE', 1, '1', 1),
(52, 8, 1, 2, 'C', '8RKFY0QM', 1, '1', 1),
(53, 8, 1, 2, 'D', 'E089AP5Z', 1, '1', 1),
(54, 8, 1, 3, 'A', 'RPLT092O', 1, '1', 1),
(55, 8, 1, 3, 'B', 'USGK88YH', 1, '1', 1),
(56, 8, 1, 3, 'C', 'ZJWYJNKX', 1, '1', 1),
(57, 8, 1, 3, 'D', 'NS6YICYA', 1, '1', 1),
(58, 8, 2, 1, 'A', '9494A1P6', 1, '1', 1),
(59, 8, 2, 1, 'B', 'XHR6950I', 1, '1', 1),
(60, 8, 2, 1, 'C', '84ZV9TKM', 1, '1', 1),
(61, 8, 2, 1, 'D', '3G0O3FID', 1, '1', 1),
(62, 8, 2, 2, 'A', 'KRHUT71Q', 1, '1', 1),
(63, 8, 2, 2, 'B', 'PSXZXYH6', 1, '1', 1),
(64, 8, 2, 2, 'C', '3H1CAMZE', 1, '1', 1),
(65, 8, 2, 2, 'D', '2Z26FLJZ', 1, '1', 1),
(66, 8, 2, 3, 'A', 'C1U58VWY', 1, '1', 1),
(67, 8, 2, 3, 'B', 'OTXMSFSV', 1, '1', 1),
(68, 8, 2, 3, 'C', 'WT86G7KJ', 1, '1', 1),
(69, 8, 2, 3, 'D', '6NPM98ML', 1, '1', 1),
(70, 8, 2, 4, 'A', '9GQICNG0', 1, '1', 1),
(71, 8, 2, 4, 'B', 'HEM9TF4P', 1, '1', 1),
(72, 8, 2, 4, 'C', '9CWPKH8R', 1, '1', 1),
(73, 8, 2, 4, 'D', '5XDE6ZZG', 1, '1', 1),
(74, 8, 2, 5, 'A', 'FQYSEFSV', 1, '1', 1),
(75, 8, 2, 5, 'B', 'TF4MU9C3', 1, '1', 1),
(76, 8, 2, 5, 'C', 'M8T6Q2XV', 1, '1', 1),
(77, 8, 2, 5, 'D', '0B96A9NQ', 1, '1', 1),
(78, 8, 2, 6, 'A', '0LIE1B9V', 1, '1', 1),
(79, 8, 2, 6, 'B', 'REHLOUPA', 1, '1', 1),
(80, 8, 2, 6, 'C', '2IHSLFOL', 1, '1', 1),
(81, 8, 2, 6, 'D', 'QYR18ES8', 1, '1', 1),
(82, 8, 3, 1, 'A', '7ZBJHAYU', 1, '1', 1),
(83, 8, 3, 1, 'B', 'ZQHE684V', 1, '1', 1),
(84, 8, 3, 1, 'C', 'NB6JRGEN', 1, '1', 1),
(85, 8, 3, 1, 'D', '0UXYLGNS', 1, '1', 1),
(86, 8, 3, 2, 'A', 'FZCW9AR9', 1, '1', 1),
(87, 8, 3, 2, 'B', '19N7IR25', 1, '1', 1),
(88, 8, 3, 2, 'C', '29PUP4HQ', 1, '1', 1),
(89, 8, 3, 2, 'D', 'ZFOKWCDB', 1, '1', 1),
(90, 8, 3, 3, 'A', 'BP8LZ0U1', 1, '1', 1),
(91, 8, 3, 3, 'B', '9I8S9BYC', 1, '1', 1),
(92, 8, 3, 3, 'C', 'KN6ASN1R', 1, '1', 1),
(93, 8, 3, 3, 'D', '3PBZ2PBE', 1, '1', 1),
(94, 8, 4, 1, 'A', '8FD8Y46Z', 1, '1', 1),
(95, 8, 4, 1, 'B', 'C9LCJ38V', 1, '1', 1),
(96, 8, 4, 1, 'C', 'T8S2TGFV', 1, '1', 1),
(97, 8, 4, 1, 'D', 'OEOPEUAM', 1, '1', 1),
(98, 8, 4, 2, 'A', 'ANV8R184', 1, '1', 1),
(99, 8, 4, 2, 'B', 'BUGUYPQR', 1, '1', 1),
(100, 8, 4, 2, 'C', 'YITRY9NN', 1, '1', 1),
(101, 8, 4, 2, 'D', 'OCD26NPH', 1, '1', 1),
(102, 8, 4, 3, 'A', 'AKQ2MY6X', 1, '1', 1),
(103, 8, 4, 3, 'B', 'TNSRDIIB', 1, '1', 1),
(104, 8, 4, 3, 'C', '1C30MQNA', 1, '1', 1),
(105, 8, 4, 3, 'D', '21C9O2QZ', 1, '1', 1),
(106, 8, 4, 4, 'A', 'MG19F879', 1, '1', 1),
(107, 8, 4, 4, 'B', 'VZ09IIKK', 1, '1', 1),
(108, 8, 4, 4, 'C', 'VNLHD9RG', 1, '1', 1),
(109, 8, 4, 4, 'D', 'A4PZ6GYT', 1, '1', 1),
(110, 8, 4, 5, 'A', 'W02C8AL4', 1, '1', 1),
(111, 8, 4, 5, 'B', 'ALDT4YDZ', 1, '1', 1),
(112, 8, 4, 5, 'C', 'LZHZ88FI', 1, '1', 1),
(113, 8, 4, 5, 'D', 'D5HKLGDH', 1, '1', 1),
(114, 8, 4, 6, 'A', 'GGUPRGT1', 1, '1', 1),
(115, 8, 4, 6, 'B', '27U6586R', 1, '1', 1),
(116, 8, 4, 6, 'C', '7NQFW6YA', 1, '1', 1),
(117, 8, 4, 6, 'D', 'BFUWW8EC', 1, '1', 1),
(118, 10, 3, 1, 'A', 'EFLADVVA', 1, '1', 1),
(119, 10, 3, 1, 'B', '0HA7SB8R', 1, '1', 1),
(120, 10, 3, 1, 'C', 'WZZXESIL', 1, '1', 1),
(121, 10, 3, 1, 'D', '8IL2Q8N5', 1, '1', 1),
(122, 10, 3, 2, 'A', 'N9F14AC4', 1, '1', 1),
(123, 10, 3, 2, 'B', 'SMCKYKCU', 1, '1', 1),
(124, 10, 3, 2, 'C', 'KBSY4BKC', 1, '1', 1),
(125, 10, 3, 2, 'D', 'U5FKE2Q2', 1, '1', 1),
(126, 10, 3, 3, 'A', 'B63GHGLA', 1, '1', 1),
(127, 10, 3, 3, 'B', '2XV0I7U2', 1, '1', 1),
(128, 10, 3, 3, 'C', 'JN1NYM0S', 1, '1', 1),
(129, 10, 3, 3, 'D', 'SFD6H38T', 1, '1', 1),
(130, 11, 2, 1, 'A', 'CP4YNZ41', 1, '1', 1),
(131, 11, 2, 1, 'B', 'DP1C5RSH', 1, '1', 1),
(132, 11, 2, 1, 'C', '65XJ1NVM', 1, '1', 1),
(133, 11, 2, 1, 'D', 'UHANNL80', 1, '1', 1),
(134, 11, 2, 2, 'A', 'ACZYB3ZO', 1, '1', 1),
(135, 11, 2, 2, 'B', 'S10YSTFY', 1, '1', 1),
(136, 11, 2, 2, 'C', 'ZDI01DMW', 1, '1', 1),
(137, 11, 2, 2, 'D', 'VXJJJRJT', 1, '1', 1),
(138, 11, 2, 3, 'A', '4JSFMS4F', 1, '1', 1),
(139, 11, 2, 3, 'B', 'U5ENYTMX', 1, '1', 1),
(140, 11, 2, 3, 'C', '64X8IK4E', 1, '1', 1),
(141, 11, 2, 3, 'D', 'HNX0FHUJ', 1, '1', 1),
(142, 11, 2, 4, 'A', '0NZNF33A', 1, '1', 1),
(143, 11, 2, 4, 'B', '8HX7AJ5H', 1, '1', 1),
(144, 11, 2, 4, 'C', 'O3P6NTL5', 1, '1', 1),
(145, 11, 2, 4, 'D', 'HI5W00G0', 1, '1', 1),
(146, 11, 2, 5, 'A', 'NFO3IRER', 1, '1', 1),
(147, 11, 2, 5, 'B', '8BZJV41J', 1, '1', 1),
(148, 11, 2, 5, 'C', '7QQVKB02', 1, '1', 1),
(149, 11, 2, 5, 'D', 'T6YU7EVU', 1, '1', 1),
(150, 11, 2, 6, 'A', 'UJYCAC4J', 1, '1', 1),
(151, 11, 2, 6, 'B', 'O33J842F', 1, '1', 1),
(152, 11, 2, 6, 'C', 'VSBG4BIX', 1, '1', 1),
(153, 11, 2, 6, 'D', 'IGRPVNKP', 1, '1', 1),
(154, 10, 1, 1, 'A', 'URD4EE9X', 1, '1', 1),
(155, 10, 1, 1, 'B', 'IITM0ZQ5', 1, '1', 1),
(156, 10, 1, 1, 'C', '2242TKPH', 1, '1', 1),
(157, 10, 1, 1, 'D', '7K5Q9LW4', 1, '1', 1),
(158, 10, 1, 2, 'A', 'CA8ROIO7', 1, '1', 1),
(159, 10, 1, 2, 'B', '1IU1HK7K', 1, '1', 1),
(160, 10, 1, 2, 'C', 'NCNGWDX3', 1, '1', 1),
(161, 10, 1, 2, 'D', 'X3U7ORB1', 1, '1', 1),
(162, 10, 1, 3, 'A', '1KSQ3HX4', 1, '1', 1),
(163, 10, 1, 3, 'B', 'ZS5GDD10', 1, '1', 1),
(164, 10, 1, 3, 'C', 'POHM1FPZ', 1, '1', 1),
(165, 10, 1, 3, 'D', 'IK66BI8D', 1, '1', 1),
(166, 13, 1, 1, 'A', '2QYOEY59', 1, '1', 1),
(167, 13, 1, 1, 'B', 'RDRH456A', 1, '1', 1),
(168, 13, 1, 2, 'A', '4G6RNQJC', 1, '1', 1),
(169, 13, 1, 2, 'B', 'Q2RCNX2Q', 1, '1', 1),
(170, 13, 1, 3, 'A', 'N1E1ZJAR', 1, '1', 1),
(171, 13, 1, 3, 'B', 'X1816FCB', 1, '1', 1),
(172, 15, 2, 1, 'A', 'VM5NA1SU', 1, '1', 1),
(173, 15, 2, 1, 'B', '57093KB8', 1, '1', 1),
(174, 15, 2, 1, 'C', 'JLEDVE9D', 1, '1', 1),
(175, 15, 2, 2, 'A', 'XP9B9654', 1, '1', 1),
(176, 15, 2, 2, 'B', 'TBS3DKXJ', 1, '1', 1),
(177, 15, 2, 2, 'C', 'SYSVJ442', 1, '1', 1),
(178, 15, 2, 3, 'A', 'QIGLXPZV', 1, '1', 1),
(179, 15, 2, 3, 'B', 'F87OEDT8', 1, '1', 1),
(180, 15, 2, 3, 'C', 'OMB269LY', 1, '1', 1),
(181, 15, 2, 4, 'A', '8DURIYU8', 1, '1', 1),
(182, 15, 2, 4, 'B', 'HAUFZTAF', 1, '1', 1),
(183, 15, 2, 4, 'C', '2I3GVXOK', 1, '1', 1),
(184, 15, 2, 5, 'A', 'J0MQ97PH', 1, '1', 1),
(185, 15, 2, 5, 'B', 'LJ93I3C0', 1, '1', 1),
(186, 15, 2, 5, 'C', 'E6FE0QT2', 1, '1', 1),
(187, 15, 2, 6, 'A', '8XJ4U8OE', 1, '1', 1),
(188, 15, 2, 6, 'B', '8A4IIU03', 1, '1', 1),
(189, 15, 2, 6, 'C', 'EA7WEJWS', 1, '1', 1),
(190, 13, 2, 1, 'A', '5MNJL1YQ', 1, '1', 1),
(191, 13, 2, 1, 'B', 'QGXLL467', 1, '1', 1),
(192, 13, 2, 2, 'A', 'N8BXN5G8', 1, '1', 1),
(193, 13, 2, 2, 'B', 'BBS3U4OZ', 1, '1', 1),
(194, 13, 2, 3, 'A', 'QBIBDH23', 1, '1', 1),
(195, 13, 2, 3, 'B', 'X0PI4VQR', 1, '1', 1),
(196, 13, 2, 4, 'A', '41PR650H', 1, '1', 1),
(197, 13, 2, 4, 'B', 'HSLBXAAO', 1, '1', 1),
(198, 13, 2, 5, 'A', 'LT0YA227', 1, '1', 1),
(199, 13, 2, 5, 'B', '3RQ7NGYR', 1, '1', 1),
(200, 13, 2, 6, 'A', 'IOIPUJ7B', 1, '1', 1),
(201, 13, 2, 6, 'B', 'BSM92WXO', 1, '1', 1),
(202, 17, 2, 1, 'A', '0UHY80TV', 1, '1', 1),
(203, 17, 2, 1, 'B', '9MZDOX3L', 1, '1', 1),
(204, 17, 2, 1, 'D', 'K2XQK2LN', 1, '1', 1),
(205, 17, 2, 1, 'C', 'ZHTXONF7', 1, '1', 1),
(206, 18, 4, 1, 'A', 'JUPNR26O', 1, '1', 1),
(207, 18, 4, 1, 'B', 'A8FJG0OR', 1, '1', 1),
(208, 18, 4, 1, 'C', '141B1ZSE', 1, '1', 1),
(209, 1, 2, 1, 'D', '570JLMWP', 1, '1', 1),
(210, 23, 2, 1, 'A', 'MF5DFQOA', 1, '1', 1),
(211, 23, 2, 1, 'B', 'L868OARK', 1, '1', 1),
(212, 23, 2, 2, 'A', '5P0UKIPO', 1, '1', 1),
(213, 23, 2, 2, 'B', 'TUURXRQR', 1, '1', 1),
(214, 23, 2, 3, 'A', 'VDTNVI38', 1, '1', 1),
(215, 23, 2, 3, 'B', 'V64F2MD9', 1, '1', 1),
(216, 24, 2, 1, 'A', 'LG4UM85A', 1, '1', 1),
(217, 24, 2, 1, 'B', 'OWUAX02G', 1, '1', 1),
(218, 24, 2, 2, 'A', 'CX6QKG0O', 1, '1', 1),
(219, 24, 2, 2, 'B', 'TRGJNWZ0', 1, '1', 1),
(220, 24, 2, 3, 'A', 'BY30N6OZ', 1, '1', 1),
(221, 24, 2, 3, 'B', 'PRSFAYCK', 1, '1', 1),
(222, 25, 3, 1, 'A', '5YNQ99JW', 1, '1', 1),
(223, 25, 3, 1, 'B', '1DXQ3WY6', 1, '1', 1),
(224, 25, 3, 2, 'A', 'IDQ590ZN', 1, '1', 1),
(225, 25, 3, 2, 'B', '16L5U78V', 1, '1', 1),
(226, 25, 3, 2, 'B', 'HS3RF5DY', 1, '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `idHorario` int(11) NOT NULL,
  `idGrupo` int(11) NOT NULL,
  `idSubgrupo` int(11) NOT NULL,
  `idCiclo` int(11) NOT NULL,
  `inicio` datetime NOT NULL,
  `fin` datetime DEFAULT NULL,
  `dia` varchar(15) NOT NULL,
  `horaIni` varchar(20) NOT NULL,
  `horaFin` varchar(20) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `clase` varchar(15) NOT NULL DEFAULT 'naranja'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`idHorario`, `idGrupo`, `idSubgrupo`, `idCiclo`, `inicio`, `fin`, `dia`, `horaIni`, `horaFin`, `status`, `clase`) VALUES
(1, 1, 2, 3, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(2, 1, 2, 3, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(3, 1, 2, 3, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(4, 1, 2, 3, '2017-10-05 07:00:00', '2017-10-05 08:00:00', 'jueves', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(6, 28, 1, 3, '2017-10-02 08:00:00', '2017-10-02 09:00:00', 'lunes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(7, 28, 1, 3, '2017-10-03 08:00:00', '2017-10-03 09:00:00', 'martes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(8, 28, 1, 3, '2017-10-04 08:00:00', '2017-10-04 09:00:00', 'miércoles', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(9, 28, 1, 3, '2017-10-05 08:00:00', '2017-10-05 09:00:00', 'jueves', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(11, 29, 3, 3, '2017-10-02 08:00:00', '2017-10-02 09:00:00', 'lunes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(12, 29, 3, 3, '2017-10-03 08:00:00', '2017-10-03 09:00:00', 'martes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(13, 29, 3, 3, '2017-10-04 08:00:00', '2017-10-04 09:00:00', 'miércoles', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(14, 29, 3, 3, '2017-10-05 08:00:00', '2017-10-05 09:00:00', 'jueves', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(15, 29, 3, 3, '2017-10-06 08:00:00', '2017-10-06 09:00:00', 'viernes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(16, 29, 4, 3, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(17, 29, 4, 3, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(18, 29, 4, 3, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(19, 29, 4, 3, '2017-10-05 07:00:00', '2017-10-05 08:00:00', 'jueves', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(20, 29, 4, 3, '2017-10-06 07:00:00', '2017-10-06 08:00:00', 'viernes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(21, 30, 7, 3, '2017-10-02 09:00:00', '2017-10-02 10:00:00', 'lunes', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(22, 30, 7, 3, '2017-10-03 09:00:00', '2017-10-03 10:00:00', 'martes', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(23, 30, 7, 3, '2017-10-04 09:00:00', '2017-10-04 10:00:00', 'miércoles', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(24, 30, 7, 3, '2017-10-05 09:00:00', '2017-10-05 10:00:00', 'jueves', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(25, 30, 7, 3, '2017-10-06 09:00:00', '2017-10-06 10:00:00', 'viernes', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(26, 30, 8, 3, '2017-10-02 10:00:00', '2017-10-02 11:00:00', 'lunes', '10:00:00 am', '11:00:00 am', 1, 'naranja'),
(27, 30, 8, 3, '2017-10-03 10:00:00', '2017-10-03 11:00:00', 'martes', '10:00:00 am', '11:00:00 am', 1, 'naranja'),
(28, 30, 8, 3, '2017-10-04 10:00:00', '2017-10-04 11:00:00', 'miércoles', '10:00:00 am', '11:00:00 am', 1, 'naranja'),
(29, 30, 8, 3, '2017-10-05 10:00:00', '2017-10-05 11:00:00', 'jueves', '10:00:00 am', '11:00:00 am', 1, 'naranja'),
(30, 30, 8, 3, '2017-10-06 10:00:00', '2017-10-06 11:00:00', 'viernes', '10:00:00 am', '11:00:00 am', 1, 'naranja'),
(31, 1, 9, 1, '2017-10-02 06:20:00', '2017-10-02 08:00:00', 'lunes', '6:20:00 am', '8:00:00 am', 1, 'naranja'),
(32, 1, 9, 1, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(33, 1, 9, 1, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(34, 1, 9, 1, '2017-10-05 07:00:00', '2017-10-05 08:00:00', 'jueves', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(35, 1, 9, 1, '2017-10-06 07:00:00', '2017-10-06 08:00:00', 'viernes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(37, 1, 2, 3, '2017-10-06 07:00:00', '2017-10-06 08:00:00', 'viernes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(38, 28, 10, 4, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(39, 28, 10, 4, '2017-10-02 08:10:00', '2017-10-02 09:10:00', 'lunes', '8:10:00 am', '9:10:00 am', 1, 'naranja'),
(40, 94, 12, 9, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(41, 94, 12, 9, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(42, 94, 12, 9, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(43, 94, 12, 9, '2017-10-05 07:00:00', '2017-10-05 08:00:00', 'jueves', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(44, 94, 12, 9, '2017-10-06 07:00:00', '2017-10-06 08:00:00', 'viernes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(45, 94, 13, 9, '2017-10-02 08:00:00', '2017-10-02 09:00:00', 'lunes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(46, 94, 13, 9, '2017-10-04 08:00:00', '2017-10-04 09:00:00', 'miércoles', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(47, 94, 13, 9, '2017-10-06 08:00:00', '2017-10-06 09:00:00', 'viernes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(48, 94, 14, 9, '2017-10-02 09:00:00', '2017-10-02 10:00:00', 'lunes', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(49, 94, 14, 9, '2017-10-03 09:00:00', '2017-10-03 10:00:00', 'martes', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(50, 94, 14, 9, '2017-10-04 09:00:00', '2017-10-04 10:00:00', 'miércoles', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(51, 94, 14, 9, '2017-10-05 09:00:00', '2017-10-05 10:00:00', 'jueves', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(52, 94, 14, 9, '2017-10-06 09:00:00', '2017-10-06 10:00:00', 'viernes', '9:00:00 am', '10:00:00 am', 1, 'naranja'),
(53, 118, 15, 10, '2017-10-02 07:00:00', NULL, 'lunes', '7:00:00 am', NULL, 1, 'naranja'),
(54, 118, 15, 10, '2017-10-03 07:00:00', NULL, 'martes', '7:00:00 am', NULL, 1, 'naranja'),
(55, 118, 15, 10, '2017-10-04 07:00:00', NULL, 'miércoles', '7:00:00 am', NULL, 1, 'naranja'),
(56, 118, 15, 10, '2017-10-05 07:00:00', NULL, 'jueves', '7:00:00 am', NULL, 1, 'naranja'),
(57, 118, 15, 10, '2017-10-06 07:00:00', NULL, 'viernes', '7:00:00 am', NULL, 1, 'naranja'),
(58, 130, 16, 11, '2017-10-02 07:00:00', NULL, 'lunes', '7:00:00 am', NULL, 1, 'naranja'),
(59, 130, 16, 11, '2017-10-03 07:00:00', NULL, 'martes', '7:00:00 am', NULL, 1, 'naranja'),
(60, 130, 16, 11, '2017-10-04 07:00:00', NULL, 'miércoles', '7:00:00 am', NULL, 1, 'naranja'),
(61, 130, 16, 11, '2017-10-05 07:00:00', NULL, 'jueves', '7:00:00 am', NULL, 1, 'naranja'),
(62, 130, 16, 11, '2017-10-06 07:00:00', NULL, 'viernes', '7:00:00 am', NULL, 1, 'naranja'),
(63, 154, 17, 12, '2017-10-02 07:30:00', '2017-10-02 08:30:00', 'lunes', '7:30:00 am', '8:30:00 am', 1, 'naranja'),
(64, 154, 17, 12, '2017-10-04 07:30:00', NULL, 'miércoles', '7:30:00 am', NULL, 1, 'naranja'),
(65, 154, 17, 12, '2017-10-03 07:00:00', '2017-10-03 07:00:00', 'martes', '7:00:00 am', '2017-10-10T07:00:00', 1, 'naranja'),
(66, 154, 17, 12, '2017-10-05 07:30:00', NULL, 'jueves', '7:30:00 am', NULL, 1, 'naranja'),
(67, 154, 17, 12, '2017-10-06 07:20:00', NULL, 'viernes', '7:20:00 am', NULL, 1, 'naranja'),
(68, 1, 1, 3, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(69, 1, 1, 3, '2017-10-03 08:00:00', '2017-10-03 09:00:00', 'martes', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(70, 1, 1, 3, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(71, 1, 1, 3, '2017-10-05 08:00:00', '2017-10-05 09:00:00', 'jueves', '8:00:00 am', '9:00:00 am', 1, 'naranja'),
(72, 1, 1, 3, '2017-10-06 07:00:00', '2017-10-06 08:00:00', 'viernes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(73, 166, 19, 13, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(74, 2, 20, 1, '2017-10-02 07:00:00', '2017-10-02 07:50:00', 'lunes', '7:00:00 am', '7:50:00 am', 0, 'naranja'),
(75, 2, 20, 1, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(76, 2, 20, 1, '2017-10-04 07:30:00', NULL, 'miércoles', '7:30:00 am', NULL, 1, 'naranja'),
(77, 2, 20, 1, '2017-10-06 07:30:00', NULL, 'viernes', '7:30:00 am', NULL, 1, 'naranja'),
(78, 3, 21, 1, '2017-10-04 08:30:00', NULL, 'miércoles', '8:30:00 am', NULL, 1, 'naranja'),
(79, 202, 22, 16, '2017-10-02 07:00:00', '2017-10-02 07:00:00', 'lunes', '7:00:00 am', '2017-10-02T07:00:00', 1, 'naranja'),
(80, 202, 23, 16, '2017-10-02 09:00:00', '2017-10-02 09:00:00', 'lunes', '9:00:00 am', '2017-10-02T09:00:00', 1, 'naranja'),
(81, 202, 24, 16, '2017-10-03 07:00:00', '2017-10-03 07:00:00', 'martes', '7:00:00 am', '2017-10-03T07:00:00', 1, 'naranja'),
(82, 202, 25, 16, '2017-10-04 07:00:00', NULL, 'miércoles', '7:00:00 am', NULL, 1, 'naranja'),
(83, 205, 26, 16, '2017-10-02 07:30:00', NULL, 'lunes', '7:30:00 am', NULL, 1, 'naranja'),
(84, 205, 27, 16, '2017-10-03 07:10:00', NULL, 'martes', '7:10:00 am', NULL, 1, 'naranja'),
(85, 202, 22, 16, '2017-10-05 07:00:00', '2017-10-05 07:00:00', 'jueves', '7:00:00 am', '2017-10-05T07:00:00', 1, 'naranja'),
(86, 202, 25, 16, '2017-10-03 09:00:00', '2017-10-03 09:00:00', 'martes', '9:00:00 am', '2017-10-03T09:00:00', 1, 'naranja'),
(87, 206, 28, 17, '2017-10-02 07:00:00', '2017-10-02 07:00:00', 'lunes', '7:00:00 am', '2017-10-02T07:00:00', 1, 'naranja'),
(88, 206, 28, 17, '2017-10-03 07:10:00', '2017-10-03 07:10:00', 'martes', '7:10:00 am', '2017-10-03T07:10:00', 1, 'naranja'),
(89, 1, 29, 1, '2017-10-03 07:50:00', '2017-10-03 08:40:00', 'martes', '7:50:00 am', '8:40:00 am', 1, 'naranja'),
(90, 1, 33, 1, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 0, 'naranja'),
(91, 1, 33, 1, '2017-10-06 07:00:00', '2017-10-06 08:00:00', 'viernes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(92, 1, 33, 1, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(93, 1, 30, 1, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(94, 1, 30, 1, '2017-10-05 07:00:00', '2017-10-05 08:00:00', 'jueves', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(95, 1, 31, 1, '2017-10-02 08:00:00', '2017-10-02 09:10:00', 'lunes', '8:00:00 am', '9:10:00 am', 1, 'naranja'),
(96, 1, 31, 1, '2017-10-03 08:00:00', '2017-10-03 09:10:00', 'martes', '8:00:00 am', '9:10:00 am', 1, 'naranja'),
(97, 1, 31, 1, '2017-10-04 08:00:00', '2017-10-04 09:10:00', 'miércoles', '8:00:00 am', '9:10:00 am', 1, 'naranja'),
(98, 1, 31, 1, '2017-10-05 08:00:00', '2017-10-05 09:10:00', 'jueves', '8:00:00 am', '9:10:00 am', 1, 'naranja'),
(99, 1, 31, 1, '2017-10-06 08:00:00', '2017-10-06 09:10:00', 'viernes', '8:00:00 am', '9:10:00 am', 1, 'naranja'),
(100, 1, 35, 1, '2017-10-02 09:10:00', '2017-10-02 10:10:00', 'lunes', '9:10:00 am', '10:10:00 am', 1, 'naranja'),
(101, 1, 35, 1, '2017-10-03 09:10:00', '2017-10-03 10:10:00', 'martes', '9:10:00 am', '10:10:00 am', 1, 'naranja'),
(102, 1, 35, 1, '2017-10-04 09:10:00', '2017-10-04 10:10:00', 'miércoles', '9:10:00 am', '10:10:00 am', 1, 'naranja'),
(103, 1, 35, 1, '2017-10-05 09:10:00', '2017-10-05 10:10:00', 'jueves', '9:10:00 am', '10:10:00 am', 1, 'naranja'),
(104, 1, 35, 1, '2017-10-06 09:10:00', '2017-10-06 10:10:00', 'viernes', '9:10:00 am', '10:10:00 am', 1, 'naranja'),
(105, 1, 29, 1, '2017-10-02 07:00:00', '2017-10-02 08:30:00', 'lunes', '7:00:00 am', '8:30:00 am', 1, 'naranja'),
(106, 1, 30, 1, '2017-10-05 10:10:00', '2017-10-05 10:10:00', 'jueves', '10:10:00 am', '2017-10-05T10:10:00', 0, 'naranja'),
(107, 216, 39, 19, '2017-10-02 07:30:00', '2017-10-02 07:50:00', 'lunes', '7:30:00 am', '7:50:00 am', 1, 'naranja'),
(108, 216, 39, 19, '2017-10-04 07:30:00', '2017-10-04 07:50:00', 'miércoles', '7:30:00 am', '7:50:00 am', 0, 'naranja'),
(109, 216, 39, 19, '2017-10-06 07:30:00', '2017-10-06 07:50:00', 'viernes', '7:30:00 am', '7:50:00 am', 1, 'naranja'),
(110, 216, 39, 19, '2017-10-04 08:00:00', '2017-10-04 08:30:00', 'miércoles', '8:00:00 am', '8:30:00 am', 1, 'naranja'),
(111, 216, 39, 19, '2017-10-03 07:30:00', '2017-10-03 07:50:00', 'martes', '7:30:00 am', '7:50:00 am', 1, 'naranja'),
(112, 216, 39, 19, '2017-10-05 07:30:00', '2017-10-05 08:00:00', 'jueves', '7:30:00 am', '8:00:00 am', 1, 'naranja'),
(113, 217, 41, 19, '2017-10-02 07:00:00', '2017-10-02 07:30:00', 'lunes', '7:00:00 am', '7:30:00 am', 1, 'naranja'),
(114, 217, 41, 19, '2017-10-03 07:00:00', '2017-10-03 07:30:00', 'martes', '7:00:00 am', '7:30:00 am', 1, 'naranja'),
(115, 217, 41, 19, '2017-10-04 07:00:00', '2017-10-04 07:30:00', 'miércoles', '7:00:00 am', '7:30:00 am', 1, 'naranja'),
(116, 217, 41, 19, '2017-10-05 07:00:00', '2017-10-05 07:30:00', 'jueves', '7:00:00 am', '7:30:00 am', 1, 'naranja'),
(117, 217, 41, 19, '2017-10-06 07:00:00', '2017-10-06 07:30:00', 'viernes', '7:00:00 am', '7:30:00 am', 1, 'naranja'),
(118, 222, 42, 20, '2017-10-02 07:00:00', '2017-10-02 08:00:00', 'lunes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(119, 222, 42, 20, '2017-10-04 07:00:00', '2017-10-04 08:00:00', 'miércoles', '7:00:00 am', '8:00:00 am', 0, 'naranja'),
(120, 222, 42, 20, '2017-10-03 07:00:00', '2017-10-03 08:00:00', 'martes', '7:00:00 am', '8:00:00 am', 1, 'naranja'),
(121, 222, 42, 20, '2017-10-04 08:20:00', '2017-10-04 08:20:00', 'miércoles', '8:20:00 am', '2017-10-04T08:20:00', 0, 'naranja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Insignias`
--

CREATE TABLE `Insignias` (
  `id` int(11) NOT NULL,
  `idTipoInsignia` int(11) NOT NULL,
  `idReceptor` int(11) NOT NULL,
  `tipoReceptor` int(11) NOT NULL COMMENT '0: Alumno 1:Grupo',
  `idEmisor` int(11) NOT NULL,
  `idSubGrupo` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Insignias`
--

INSERT INTO `Insignias` (`id`, `idTipoInsignia`, `idReceptor`, `tipoReceptor`, `idEmisor`, `idSubGrupo`, `fecha`) VALUES
(1, 1, 3, 0, 2, 1, '2017-11-11'),
(2, 1, 3, 0, 2, 1, '2017-11-11'),
(3, 1, 3, 0, 2, 1, '2017-11-13'),
(4, 2, 11, 0, 2, 1, '2017-11-13'),
(5, 6, 1, 0, 2, 1, '2017-11-13'),
(6, 1, 3, 0, 2, 1, '2017-11-11'),
(7, 1, 3, 0, 2, 1, '2017-11-11'),
(8, 1, 0, 1, 21, 1, '2017-11-14'),
(9, 1, 0, 1, 21, 1, '2017-11-23'),
(10, 3, 0, 1, 21, 4, '2017-11-23'),
(11, 3, 3, 0, 21, 1, '2017-12-11'),
(14, 3, 3, 0, 2, 1, '2017-12-13'),
(13, 4, 40, 0, 2, 1, '2017-12-13'),
(15, 1, 0, 1, 2, 1, '2017-12-13'),
(16, 4, 3, 0, 2, 1, '2017-12-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion`
--

CREATE TABLE `institucion` (
  `idInstitucion` int(11) NOT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `contacto` varchar(100) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  `ciudad` varchar(80) DEFAULT NULL,
  `idestado` int(11) DEFAULT NULL,
  `idpais` int(11) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `cp` varchar(8) DEFAULT NULL,
  `nivelescolar` varchar(45) DEFAULT NULL,
  `siglas` varchar(10) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `desc` varchar(1200) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `lng` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT 'logo_defualt.jpg',
  `banner` varchar(50) DEFAULT NULL,
  `codregistro` varchar(45) DEFAULT NULL,
  `mision` varchar(10000) DEFAULT NULL,
  `vision` varchar(10000) DEFAULT NULL,
  `valores` varchar(10000) DEFAULT NULL,
  `reglamento` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `institucion`
--

INSERT INTO `institucion` (`idInstitucion`, `clave`, `nombre`, `contacto`, `direccion`, `id_usuario`, `fecha_alta`, `fecha_baja`, `activo`, `ciudad`, `idestado`, `idpais`, `telefono`, `cp`, `nivelescolar`, `siglas`, `correo`, `desc`, `lat`, `lng`, `logo`, `banner`, `codregistro`, `mision`, `vision`, `valores`, `reglamento`) VALUES
(1, '09.028.502/0001', 'Colegio Hispanoamericano', 'Juan Perez', 'Benigno Arriaga 805, De Tequisquiapan, 78233 San Luis, S.L.P', 1, '2017-09-14', NULL, '1', 'San Luis Potosí', 24, 1, '01 444 813 8205', '78233', 'Maternal - Preparatoria', 'CHI', 'admin@hispano.com', 'El Instituto Hispano Inglés es un colegio católico fundado por la Congregación de las Hermanas de la Caridad del Verbo Encarnado en 1937 en la Ciudad de San Luis Potosí, S.L.P.', '22.1475339', '-100.987012', '2017120815127702651.jpg', '2017110815101611631.jpg', 'tUfivTU7g8', 'La Universidad Politécnica de San Luis Potosí, está comprometida con la excelencia en la formación integral y humana, y con el aprendizaje, el desarrollo y la aplicación del nuevo conocimiento.\r\nLa Universidad Politécnica une a estudiantes, docentes y personal administrativo para crear una comunidad de la más alta calidad académica basada en un modelo educativo abierto y flexible, y comprometida con el progreso social y económico del estado y del país.', '\"Aspiramos a ser una Universidad consolidada en su tarea de formar profesionales competentes, que se distinguen por su aportación al desarrollo de México y de San Luis Potosí y cuentan con reconocimiento en sus campos de actividad profesional y en su vida comunitaria.\r\n\r\nLa Universidad Politécnica de San Luis Potosí representa un espacio de desarrollo intelectual que atrae el mejor talento, tanto de estudiantes como profesores, que se sienten orgullosos de pertenecer a esta comunidad académica.\"', 'Integridad\r\nResponsabilidad\r\nTrascendencia\r\nConstancia\r\nResponsabilidad Social\r\nRespeto\r\nAfecto\r\nServicio\r\nLibertad\r\nJusticia\r\nEsfuerzo\r\nSolidaridad\r\nTrabajo en Equipo\r\nCompromiso\r\nMejora Continua\r\nCongruencia', '------------------------------------'),
(3, 'NMAKLN1789SEPSLP', 'Marista', 'Pedro Perez', 'Benigno Arriaga 608, Col. Moderna C.P. 78270 San Luis Potosí, S.L.P.', 7, '2017-09-26', NULL, '1', 'SLP', 24, 1, '8135465', NULL, NULL, NULL, 'adm', 'El Instituto Potosino Marista, es dirigido por los Hermanos Maristas, Desde 1935 ofrece a la sociedad potosina la opción de una formación integral, de acuerdo a la concepción cristiana de la persona, de la vida y del mundo.', '22.1496857', '-100.98800849999998', NULL, '', 'OIHO4IBI', NULL, NULL, NULL, NULL),
(4, NULL, 'Colegio Hispano Mexicano', NULL, NULL, 9, '2017-09-26', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '(9FUPCDN', NULL, NULL, NULL, NULL),
(5, 'IUHugIJ/78', '1 de Mayo', 'Luis Alberto Mendez', '---------', 10, '2017-09-26', NULL, '1', 'SLP', 24, 1, '863879', NULL, NULL, NULL, 'alberto@gmail.com', 'hbiohuihuih', '22.1497904', '-100.94722539999998', NULL, '', 'QSDT5HJT', 'kjgiuhk', 'bjhjk', 'nbjh', 'hvhjbkj'),
(6, NULL, 'Lala', NULL, NULL, 19, '2017-09-27', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'H3SMC4IU', NULL, NULL, NULL, NULL),
(7, NULL, 'lalo', NULL, NULL, 20, '2017-09-27', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Q4102I68', NULL, NULL, NULL, NULL),
(8, 'KFGK6678976HVJKL', 'Jose Guadalupe Zuno', 'Jesus Corono', 'Av RIcardo B Anaya 760', 53, '2017-10-09', NULL, '1', 'San Luis Potosí', 24, 1, '6787657', NULL, NULL, NULL, 'zuno@gmail.com|', 'Gran Escuela', '22.1496744', '-100.94677139999999', NULL, '', '2KNI0R58', NULL, NULL, NULL, NULL),
(9, NULL, 'Juan Martin Huerta Hernandez', NULL, NULL, 67, '2017-10-10', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'EOMKB0G9', NULL, NULL, NULL, NULL),
(10, 'gig3h33njk3b', 'Benito juarez', 'Martha Lopez', 'lalalalalala', 68, '2017-10-10', NULL, '1', 'SLP', 24, 1, '543', NULL, NULL, NULL, '3243243', '...................', '22.1495026', '-100.98695470000001', '', '', 'PO5TSC4T', NULL, NULL, NULL, NULL),
(11, '23G4U4GU2I3H4', 'Agustin Tapia Miranda', '----------', '--------', 72, '2017-10-10', NULL, '1', 'SLP', 24, 1, '87879809', NULL, NULL, NULL, 'angelo@gmail.com', 'yhuigiuhi', '22.13487229999999', '-100.9125429', NULL, '', '1PPUKT8F', NULL, NULL, NULL, NULL),
(12, '23G4JH2K34', 'Cecyte', '------------', '--------------', 87, '2017-10-18', NULL, '1', 'Guadalajara', 14, 1, '--------------', NULL, NULL, NULL, 'carrillo@gmail.com', '-----------------------------------', '20.6721519', '-103.38638860000003', NULL, '', 'UIX9MOF8', NULL, NULL, NULL, NULL),
(13, '2089', 'Colegio Victoria', 'Juan José Melgarejo', 'Av. Francisco P. Mariel 120', 88, '2017-10-18', NULL, '1', 'San Luis Potosí', 24, 1, '4442859341', NULL, NULL, NULL, 'jmelgarejo@myappcollege.com', 'Escuela primaria.', NULL, NULL, NULL, '', '8OU7OTR0', 'Misión.', 'Visión.', 'Valores.', 'Reglamento.'),
(14, NULL, 'El Potosino', NULL, NULL, 91, '2017-10-19', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'L8CVGMHL', NULL, NULL, NULL, NULL),
(15, 'DPR323435J', 'Alejandro Arrez', 'Juan José Melgarejo', 'Benigno Arriaga 224, Col. Tequis', 92, '2017-10-19', NULL, '1', 'San Luis Potosí', 24, 1, '8202020', NULL, NULL, NULL, 'adsacqs@csddcd.com', 'hsgcsauicvqyvcipqbivñbqeiñvqchiñvcqiñvcqdiw', '22.1538606', '-100.98811510000002', NULL, '', 'P9IAO5XD', NULL, NULL, NULL, NULL),
(16, 'abcd1234', 'ESFAA', 'Profr. Salmerón', 'Juárez', 93, '2017-10-22', NULL, '1', 'Puebla', 21, 1, '21341234134', NULL, NULL, NULL, 'jfkjakf@jflkjla.com', 'la escuela bla bla bla...', '19.814851', '-97.35952939999999', '20171022150871532793.jpg', '', '2MDZ4VI8', 'Misión.', 'Visión.', 'Valores.', 'Reglamento.'),
(17, 'KH3HI2HO3I2J', 'Motolinea', 'hkjn', 'jhb', 94, '2017-10-23', NULL, '1', 'La Paz', 3, 1, '78878', NULL, NULL, NULL, 'lala@gmail.com', 'huuihijio', '22.149119', '-101.01003200000002', '20171027150912207594.jpg', '20171027150912210294.jpg', 'R70FK29R', 'Somos una Comunidad Educativa católica, que comprometida con nuestra vocación magisterial, brindamos una formación humana-cristiana a niños, adolescentes y jóvenes, en un ambiente de acogida, libertad responsable, respeto y solidaridad, según el Carisma Vocacional-Sacerdotal y Mariano de los fundadores.', 'Ser comunidades educativas en pastoral que, en estrecha colaboración con la familia, brinden un espacio de formación integral donde niños adolescentes y jóvenes desarrollen sus pontencialidades y las competencias necesarias para optar por la vida, el compromiso con su propia realización trascendente, la fraternidad y el servicio.', '1. Educamos integralmente a la persona desde la perspectiva humana-cristiana, y de nuestro Carisma vocacional – sacerdotal.\r\n \r\n2. Educamos en un ambiente de cordialidad, alegría, respeto, armonía y seguridad que favorece las relaciones interpersonales entre los miembros de la comunidad educativa.\r\n \r\n3. Educamos en un proceso de calidad y excelencia académica, que permite el desarrollo de habilidades, hábitos y capacidades cognitivas, en la búsqueda del conocimiento, con sentido creativo, crítico, ético, aplicando los avances tecnológicos  y científicos con una visión humanista.\r\n \r\n4. Educamos en una escuela abierta, que responde a las necesidades, carencias y aspiraciones de cada alumno que favorece la inserción satisfactoria en la sociedad.\r\n \r\n5. Contamos con un personal docente capacitado, profesional, en constante actualización e identificado con el Carisma Institucional, que le permite responder a las exigencias de la educación de calidad que se ofrece.\r\n \r\n6. Compartimos con los laicos la misión educativa, quienes complementan nuestra labor educativa en la tarea de ejercer un protagonismo que nos lleva a formar líderes comprometidos en el servicio a los demás.\r\n \r\n7. Trabajamos en estrecha colaboración con los Padres de Familia, primeros responsables de la formación de sus hijos, buscando juntos herramientas orientadas al cumplimiento de nuestra misión educativa.\r\n \r\n8. Educamos a partir de un modelo educativo propio, que fundamentado en los valores de un humanismo cristiano, promueve la formación de la persona en orden a su fin último y al bien de las sociedades.\r\n \r\n9. Educamos en un proceso evangelizador que lleve a los alumnos a realizar una integración entre fe, cultura y vida.\r\n \r\n10.   Educamos para un compromiso solidario, ante un mundo empobrecido.', 'AVISO DE PRIVACIDAD\r\n.\r\n\r\n.\r\nEn cumplimientos a La Ley Federal de Protección de Datos Personales en Posesión de los Particulares y de su Reglamento, y con el fin de asegurar la protección y privacidad de datos personales; así como regular los derechos de acceso, rectificación, cancelación y oposición del tratamiento de los mismos, MOTOLINIA DE SAN LUIS, A. C. proporciona el correspondiente AVISO.\r\n.\r\nIdentidad y domicilio del RESPONSABLE\r\n.\r\nMotolinia de San Luis, A.C., con domicilio en Basalenque No. 140, Col. Jardín, C.P. 78270, San Luis Potosí, S.L.P., es el Responsable del uso y confidencialidad de los datos personales que sean solicitados a los alumnos, aspirantes a ser alumnos, padres o tutores con el fin de asegurar la privacidad de los datos proporcionados, con objetivo de vincularse con los servicios académicos proporcionados por el Responsable.\r\n.\r\nFinalidades \r\n.\r\nEl Responsable puede recabar datos personales, datos personales sensibles y datos personales financieros de forma personal, a través de las formas correspondientes con la firma de padres o tutores, y en casos específicos, a través de vía telefónica, pagina web, correo electrónico, confiando en la autenticidad de los datos que sean proporcionados.\r\n.\r\nLas finalidades por la que el Responsable recaba datos personales, pudieran ser las siguientes:\r\n.\r\nPrestación del servicio educativo\r\nIntegración de expediente académico\r\nIntegración de expediente administrativo\r\nInformación de contacto para enviar comunicados, localizar a padres o tutores\r\nEstudio y otorgamiento de becas\r\nEvaluar calidad en el servicio\r\nRealizar estadísticas sobre los servicios educativos, actividades deportivas y culturales\r\nCumplir con requisitos de la Secretaría de Educación Pública y otras dependencias gubernamentales\r\nFines promocionales, publicitarios o de prospección promocional.\r\n.\r\nDatos personales\r\n.\r\nEl Responsable podrá recabar de manera enunciativa más no limitativa:\r\n \r\nDatos del alumno: Nombre completo, domicilio, CURP, lugar y fecha de nacimiento, correo electrónico, teléfono, nacionalidad, escuela de procedencia.\r\n.\r\nDatos académicos: Escuela de procedencia, trayectoria educativa, calificaciones, certificados, idiomas, antecedentes escolares.\r\n.\r\nDatos de salud: Historial clínico, estado de salud, enfermedades, restricciones alimenticias, alergias, grupo sanguíneo, tratamientos médicos o psicológicos, datos de doctor para casos de emergencia.\r\n.\r\nDatos sensibles: Aficiones, deportes, creencias religiosas, asociaciones a las que pertenece, actividades extracurriculares, comportamiento social, familiar, emotivo, salud, deportivo, etc., así como otro tipo de dato solicitado en entrevista de admisión y/o seguimiento que permitan evaluar al alumno y su familia o entorno familiar.\r\n.\r\nDatos sobre los padres o tutores: Nombre completo del padre y madre o tutores del alumno, estado civil, domicilio completo de la familia o de cada uno de los padres en caso de no vivir juntos, teléfonos, correos electrónicos, nivel de estudios. Ocupación, domicilio del trabajo, empleo o negocio, puesto que desempeña, horarios de trabajo. RFC, CURP, domicilio fiscal y demás datos necesarios para facturación.\r\n.\r\nDatos financieros: Ingresos, egresos, declaraciones de impuestos, recibos de nómina y en general cualquier dato sobre la situación económica.\r\n.\r\n.\r\nEn caso no contar con alguno de los datos citados, podría existir la posibilidad de que el Responsable no pueda proporcionar los servicios propios de la relación jurídica que se establezca con el titular.\r\n.\r\nAsí mismo, se informa que para cumplir con las finalidades previstas en el presente Aviso, podrán ser recabados y tratados datos personales sensibles descritos en los incisos anteriores. Es por esto que el Responsable se compromete a que los mismos sean tratados bajo estrictas medidas de seguridad, garantizando su confidencialidad.\r\n.\r\nTransferencia de datos\r\n.\r\nEl Responsable podrá realizar transferencia de sus datos a entidades proveedoras de servicios para el cumplimiento de las obligaciones contractuales acordadas, las cuales asumirán las mismas obligaciones o responsabilidades que el Responsable ha asumido con el Titular en el presente aviso.\r\n.\r\nAsí también, con autoridades educativas correspondientes, de acuerdo al nivel educativo, con el objeto de registrar y certificar los estudios de alumno.\r\n.\r\nEl tratamiento de sus datos personales será el que resulte necesario, adecuado y relevante en relación con las finalidades previstas en este Aviso de Privacidad.\r\n.\r\nOpciones y medios para limitar el uso o divulgación de los datos personales.\r\nEl Responsable cuenta con las medidas de seguridad, administrativas, técnicas y físicas necesarias y suficientes para proteger sus datos personales contra daño, pérdida, alteración, destrucción, uso, acceso o tratamiento no autorizado. Son salvaguardados en bases de datos y equipo de cómputo que cuentan con la seguridad para prevenir fugas de información, controles de acceso físico y lógico, controles ambientales, sistemas de protección anti-intrusos, herramientas de protección antivirus, filtrado web, siendo estas algunas herramientas para mantener la seguridad de los datos en los sistemas de el Responsable.\r\n.\r\nDerechos de Titulares de los datos Personales\r\n.\r\nEl Titular podrá ejercer los derechos de acceso, rectificación, cancelación y oposición de uso de sus datos personales o la revocación del consentimiento, deberá solicitarse por escrito, debiendo contener y acompañar con lo siguiente:\r\nNombre del titular y domicilio u otro medio para comunicarle la respuesta de la solicitud.\r\n.\r\nDocumentos que acrediten su identidad.\r\n.\r\nDescripción clara y precisa de los datos personales de los que se busca ejercer alguno de los derechos, ya sea, acceso, rectificación, cancelación, oposición.\r\n.\r\nCualquier otro elemento o documento que facilite la localización de los datos personales.\r\n.\r\nCambios al Aviso de Privacidad\r\n.\r\nEl Responsable se reserva el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente Aviso, para la atención de novedades legislativas u jurisprudenciales, políticas internas, nuevos requerimientos para la prestación u ofrecimiento de los servicios que ofrece el Responsable.\r\n.\r\nConsentimiento general\r\n.\r\nHabiéndose puesto a disposición del Titular este Aviso de Privacidad y sus modificaciones y no habiendo manifestado inmediatamente oposición alguna, se entenderá que el Titular ha consentido el tratamiento de sus datos.\r\n.\r\nDe conformidad con los artículos aplicables de la Ley Federal para la Protección de Datos Personales en Posesión de los Particulares, por medio de la presente:\r\n.\r\nEl Titular reconoce haber leído este Aviso de Privacidad y entendido sus alcances.\r\n.\r\nEl Titular otorga el consentimiento expreso para el tratamiento de sus datos personales, datos personales sensibles y financieros.\r\n.\r\nEl Titular acepta que el responsable pueda transferir sus datos personales a terceros distintos del encargado.\r\n.\r\nEste consentimiento expreso puede documentarse a través de firma autógrafa, electrónica o cualquier mecanismo de autenticación.\r\n.'),
(18, '243532afdsag', 'Preparatoria Federal', 'Julio Zavaleta', 'Mina y Gpe Victoria', 99, '2017-10-24', NULL, '1', 'Teziutlán', 21, 1, '143414143', NULL, NULL, NULL, 'jzava@gmail.com', 'fjdfjqoeriuoiaalkdaslñfjdoidjafdlkfjñlads', NULL, NULL, NULL, '', '6U44SF7N', NULL, NULL, NULL, NULL),
(19, 'DPJKSJNDNSJ4', 'Escuela Secundaria Técnica No. 1', 'Lic. Rahuel Gonzalez', 'Av. Mariano Jimenez 1234', 114, '2017-10-25', NULL, '1', 'San Luis Potosí', 24, 1, '4442317929', NULL, NULL, NULL, 'rahuel.gonzalez@gmail.com', 'kcocqbeibvubiohnòqn ksncq`jbndjc qsncèioqn`1u csnqòd', '22.1392195', '-100.98738750000001', '201710251508975580114.jpg', '', '823PAJXJ', NULL, NULL, NULL, NULL),
(22, NULL, 'Benito  Juárez', NULL, NULL, 142, '2017-11-08', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'logo_defualt.jpg', NULL, '2T0CPPND', NULL, NULL, NULL, NULL),
(23, 'JSH20171108SLP', 'Jesus Silva Herzog', 'Ivette Palacios', '78434, Marcela 622, La Lomita 2da Secc, Soledad de Graciano Sánchez, S.L.P.', 143, '2017-11-08', NULL, '1', 'San Luis Potosi', 24, 1, '5552343212', NULL, NULL, NULL, 'Ivette@SilvaHerzog.com', NULL, '22.1711441', '-100.93505240000002', 'logo_defualt.jpg', NULL, '18QD7ROU', 'Garantizar que los educandos adquieran conocimientos básicos útiles para su vida diaria.', 'ser una institución educativa básica donde imparta educación integral.', 'Responsabilidad.- Responder por las acciones realizadas\r\nHonestidad.- Cualidad humana, que consiste en expresarse con coherencia y sinceridad.', 'Ser responsable en el cumplimiento del uniforme escolar.'),
(24, 'AC20171109SLP', 'appycollege', 'Andy AppyCollege', 'Francisco P. Mariel', 144, '2017-11-09', NULL, '1', 'San Luis Potosi', 24, 1, '555123454', NULL, NULL, NULL, 'andy@appycollege.com', NULL, NULL, NULL, '201711091510247319144.jpg', NULL, 'QIS7ELP7', 'Misión', 'Visión', 'Valores.', 'Reglamento'),
(25, NULL, 'benito juarez', NULL, NULL, 148, '2017-11-10', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'logo_defualt.jpg', '201711101510343608148.jpg', 'BYMWP6PA', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `idMateria` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL,
  `nombreMateria` varchar(40) NOT NULL,
  `creditos` int(11) DEFAULT NULL,
  `idNivel` int(11) NOT NULL,
  `grado` varchar(15) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`idMateria`, `idInstitucion`, `nombreMateria`, `creditos`, `idNivel`, `grado`, `activo`) VALUES
(0, 1, 'Inglés', NULL, 1, '1', 1),
(1, 1, 'Ciencias Sociales', NULL, 4, '1', 1),
(2, 1, 'Educación Física', NULL, 2, '1', 1),
(3, 1, 'Algebra', NULL, 4, '1', 1),
(4, 8, 'Fisica 1', NULL, 4, '1', 1),
(5, 8, 'Quimica 1', NULL, 4, '1', 1),
(6, 8, 'Inglés 1', NULL, 4, '1', 1),
(7, 1, '2', NULL, 2, '6', 1),
(8, 1, 'español', NULL, 3, '3', 1),
(9, 10, 'Español  1', NULL, 3, '1', 1),
(10, 11, 'Matematicas 1', NULL, 2, '1', 1),
(11, 10, 'Recortes', NULL, 1, '1', 1),
(12, 13, 'matemáticas', NULL, 1, '1', 1),
(13, 13, 'Diversiones', NULL, 1, '2', 1),
(14, 13, 'Diversiones', NULL, 1, '2', 1),
(15, 15, 'español', NULL, 2, '1', 1),
(16, 15, 'SOCIALES', NULL, 2, '1', 1),
(17, 1, 'TICs', NULL, 4, '1', 1),
(18, 17, 'Español 1', NULL, 2, '1', 1),
(19, 17, 'Matematicas 1', NULL, 2, '1', 1),
(20, 17, 'Educación Fisica 1', NULL, 2, '1', 1),
(21, 17, 'Civismo 1', NULL, 2, '1', 1),
(22, 18, 'ESpañol', NULL, 4, '1', 1),
(23, 1, 'Español', NULL, 2, '1', 1),
(24, 1, 'Matemáticas', NULL, 2, '1', 1),
(25, 1, 'Ciencias Sociales', NULL, 2, '1', 1),
(26, 23, 'español', NULL, 2, '1', 1),
(27, 23, 'matematicas', NULL, 2, '1', 1),
(28, 23, 'ciencias naturales', NULL, 2, '1', 1),
(29, 23, 'español', NULL, 2, '2', 1),
(30, 23, 'historia', NULL, 2, '2', 1),
(31, 23, 'ciencias naturales', NULL, 2, '2', 1),
(32, 23, 'matematicas', NULL, 2, '3', 1),
(33, 23, 'educación física', NULL, 2, '3', 1),
(34, 24, 'español', NULL, 2, '1', 1),
(35, 24, 'matematicas', NULL, 2, '1', 1),
(36, 24, 'español', NULL, 2, '2', 1),
(37, 24, 'historia', NULL, 2, '2', 1),
(38, 24, 'español', NULL, 2, '3', 1),
(39, 24, 'ciencias', NULL, 2, '3', 1),
(40, 25, 'ESPAÑOL', NULL, 3, '1', 1),
(41, 25, 'MATEMATICAS', NULL, 3, '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia_profesor`
--

CREATE TABLE `materia_profesor` (
  `idMateria` int(11) NOT NULL,
  `idProfesor` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia_profesor`
--

INSERT INTO `materia_profesor` (`idMateria`, `idProfesor`, `idInstitucion`) VALUES
(1, 1, 1),
(1, 2, 1),
(2, 1, 1),
(2, 20, 1),
(2, 21, 1),
(3, 1, 1),
(4, 9, 8),
(5, 10, 8),
(6, 11, 8),
(9, 12, 10),
(10, 13, 11),
(11, 12, 10),
(12, 14, 13),
(13, 14, 13),
(18, 15, 17),
(18, 16, 17),
(19, 15, 17),
(19, 16, 17),
(20, 15, 17),
(20, 16, 17),
(21, 15, 17),
(22, 17, 18),
(23, 1, 1),
(23, 6, 1),
(24, 6, 1),
(25, 1, 1),
(34, 23, 24),
(35, 24, 24),
(36, 23, 24),
(37, 24, 24),
(38, 23, 24),
(39, 24, 24),
(40, 25, 25),
(41, 25, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE `mensaje` (
  `idMensaje` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `desc` varchar(25) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `encargado` int(11) DEFAULT NULL,
  `destinatario` int(11) DEFAULT NULL,
  `tipo` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Asunto` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mensaje` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TipoDestinatario` bigint(20) NOT NULL,
  `IdDestinatario` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `IdEmisor` bigint(20) NOT NULL,
  `TipoEmisor` bigint(20) NOT NULL,
  `Visto` tinyint(1) DEFAULT 0,
  `idInstitucion` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Eliminado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `Asunto`, `Mensaje`, `TipoDestinatario`, `IdDestinatario`, `IdEmisor`, `TipoEmisor`, `Visto`, `idInstitucion`, `created_at`, `updated_at`, `Eliminado`) VALUES
(36, 'mensaje editado 5', '1234', 1, '3', 1, 1, 1, NULL, '2017-09-29 23:03:32', '2017-11-08 02:45:13', 0),
(38, 'Carta de pasantia', 'Te envio tu carta de pasantia.', 1, '1', 1, 1, 1, NULL, '2017-10-02 18:42:14', '2017-10-02 18:42:14', 1),
(39, 'mensaje de prueba', 'buenos dias esto es un mensaje de prueba  buenos dias esto es un mensaje de prueb', 1, '3', 1, 1, 1, NULL, '2017-10-02 18:55:39', '2017-10-02 18:55:39', 0),
(43, 'Mario', '-----------', 1, '2', 1, 1, 1, 1, '2017-10-02 21:23:52', '2017-10-02 21:23:52', 0),
(57, 'qwe', 'qwe', 4, '42', 1, 1, 1, 1, '2017-10-18 01:10:17', '2017-10-18 01:10:17', 0),
(58, 'Mensaje para administrador', 'Hola Mario', 1, '1', 4, 3, 1, 1, '2017-10-18 01:18:56', '2017-10-18 01:18:56', 1),
(59, 'Hola PROFESOR', 'Mensaje de prueba', 2, '2', 1, 1, 1, 1, '2017-10-18 01:19:41', '2017-10-18 01:19:41', 0),
(61, 'Mensaje para grupo', ':3', 7, '1', 1, 1, 1, 1, '2017-10-18 01:34:40', '2017-10-18 01:34:40', 0),
(62, 'Mensaje para grupo 2', ':3 x 2', 7, '2', 1, 1, 0, 1, '2017-10-18 01:35:41', '2017-10-18 01:35:41', 0),
(63, 'primer grado', 'Hola alumnos de primer grado', 6, '2-1', 1, 1, 1, 1, '2017-10-18 01:37:48', '2017-10-18 01:37:48', 0),
(64, '123', '123', 2, '3', 1, 1, 0, 1, '2017-10-20 00:36:02', '2017-10-20 00:36:02', 0),
(65, 'Mensaje de prueba', 'Mensaje de prueba', 1, '1', 1, 1, 1, 1, '2017-10-20 05:22:35', '2017-10-26 01:43:53', 1),
(66, '123', '123', 10, '2', 1, 1, 1, 1, '2017-10-21 01:43:01', '2017-10-21 01:43:01', 0),
(67, 'Hola', 'Mensaje para alumnos de 1a', 7, '1', 1, 1, 1, 1, '2017-10-21 01:48:14', '2017-10-21 01:48:14', 0),
(68, '1234', 'mensaje de prueba profesor', 3, '4', 1, 1, 1, 1, '2017-10-21 22:22:43', '2017-10-21 22:22:43', 0),
(70, 'prueba 70', 'Ultimo mensaje de prueba', 4, '52', 1, 1, 1, 1, '2017-10-23 12:30:09', '2017-10-23 12:30:09', 1),
(72, 'sdsz', 'mklmkml', 3, '95', 96, 3, 0, NULL, '2017-10-23 22:22:45', '2017-10-23 22:22:45', 0),
(73, 'Prueba', 'Prueba', 2, '48', 1, 1, 1, 1, '2017-10-24 01:29:43', '2017-10-24 01:29:43', 0),
(74, 'Hola', 'Hola', 4, '42', 18, 3, 1, NULL, '2017-10-24 01:48:03', '2017-10-24 01:48:03', 0),
(75, '1234', 'prueba', 3, '18', 1, 1, 1, 1, '2017-10-24 21:46:33', '2017-10-24 21:46:33', 0),
(76, 'Prueba1', 'prueba', 2, NULL, 101, 3, 0, NULL, '2017-10-24 23:55:18', '2017-10-24 23:55:18', 0),
(78, 'prueba2', 'prueba2', 7, '206', 101, 3, 0, NULL, '2017-10-24 23:57:49', '2017-10-24 23:57:49', 0),
(79, 'Profesores Institucion 1', 'Mensaje de prueba', 9, NULL, 1, 1, 1, 1, '2017-10-25 00:23:52', '2017-10-25 00:23:52', 1),
(83, 'Adeudo', 'Adeudo de colegiatura pendiente', 2, '48', 1, 1, 1, 1, '2017-10-25 21:33:16', '2017-10-25 21:33:16', 1),
(220, 'Mensaje de Confirmación', 'Hola Ana Paola, ya revise tu ensayo, mañana te doy la calificación.', 4, '52', 21, 3, 1, 1, '2017-11-08 04:27:00', '2017-11-08 04:27:00', 0),
(221, 'Ensayo', 'Ya revise tú ensayo, mañana te doy tu calificación.', 4, '52', 21, 3, 1, 1, '2017-11-08 04:32:11', '2017-11-08 04:32:11', 0),
(222, 'Investigación', 'ya califiqué tu investigación.', 4, '52', 21, 3, 1, 1, '2017-11-08 05:40:14', '2017-11-08 05:40:14', 0),
(238, 'Investigación', 'Recuerda que debes pasar conmigo mañana para entregarte tú calificación.', 4, '52', 21, 3, 1, 1, '2017-11-09 05:34:11', '2017-11-09 05:34:11', 0),
(239, 'saludos', 'Hola Daniela', 3, '23', 144, 1, 0, 24, '2017-11-10 01:48:13', '2017-11-10 01:48:13', 0),
(240, 'dfghjk', 'gfhjklñ', 3, '24', 144, 1, 0, 24, '2017-11-10 01:49:14', '2017-11-10 01:49:14', 0),
(241, 'HOla', 'qsdqwdqdedsd', 3, '23', 144, 1, 0, 24, '2017-11-10 01:51:11', '2017-11-10 01:51:11', 0),
(242, 'Saludos', 'hola Alex', 3, '24', 144, 1, 0, 24, '2017-11-10 02:05:41', '2017-11-10 02:05:41', 0),
(243, 'hola', 'dfghjkñ{ñljhgfd', 3, '25', 148, 1, 0, 25, '2017-11-11 04:12:04', '2017-11-11 04:12:04', 0),
(244, 'hola', 'sdfghjklñlkjhg', 1, '148', 148, 1, 0, 25, '2017-11-11 04:13:01', '2017-11-11 04:13:01', 0),
(245, 'Hola', 'Splash', 4, '86', 21, 3, 0, 1, '2017-11-14 01:20:32', '2017-11-14 01:20:32', 0),
(246, 'Hola', 'Splash', 4, '82', 21, 3, 0, 1, '2017-11-14 01:20:32', '2017-11-14 01:20:32', 0),
(247, 'Hola', 'Splash', 4, '52', 21, 3, 1, 1, '2017-11-14 01:20:32', '2017-11-14 01:20:32', 0),
(248, 'Posada Navideña', 'Se les invita a la posada navideña que tendrá nuevo lugar el día 18 de diciembre.\n\nsaludos.', 7, '1', 21, 3, 1, 1, '2017-12-07 07:47:56', '2017-12-07 07:47:56', 0),
(249, 'Posada navideña', 'vdhshjddjebejedjdebdb', 7, '1', 21, 3, 1, 1, '2017-12-07 14:30:02', '2017-12-07 14:30:02', 0),
(250, 'ggg', 'ggg', 7, '1', 21, 3, 0, 1, '2017-12-09 06:37:20', '2017-12-09 06:37:20', 0),
(251, 'HORARIO  INVERNAL', 'SE LES COMUNICA QUE A PARTIR DEL DÍA 11 DE DICIEMBRE SE LES DARÁ UNA HORA DE GRACIA PARA LLEGAR. \r\n\r\nSALUDOS.\r\n\r\nSEGE', 8, NULL, 1, 1, 1, 1, '2017-12-11 13:23:08', '2017-12-11 13:23:08', 0),
(252, 'ejsh', 'dj dhdh', 7, '1', 21, 3, 0, 1, '2017-12-13 00:28:04', '2017-12-13 00:28:04', 0),
(253, 'prueba', 'prueba', 9, NULL, 21, 3, 0, 1, '2017-12-13 00:28:51', '2017-12-13 00:28:51', 0),
(254, 'dhrh', 'djeheh', 7, '1', 21, 3, 0, 1, '2017-12-13 00:30:07', '2017-12-13 00:30:07', 0),
(255, 'Posada Navideña', 'Papás no falten a la posada navideña.', 7, '1', 21, 3, 1, 1, '2017-12-13 01:47:42', '2017-12-13 01:47:42', 0),
(256, 'suspensión de clases', 'se les avisa que mañana no va a haber clases hasta el dia viernes por las bajas temperaturas', 7, '1', 21, 3, 1, 1, '2017-12-13 12:18:27', '2017-12-13 12:18:27', 0),
(257, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:34', '2017-12-14 04:42:34', 0),
(258, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:34', '2017-12-14 04:42:34', 0),
(259, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:34', '2017-12-14 04:42:34', 0),
(260, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:34', '2017-12-14 04:42:34', 0),
(261, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:35', '2017-12-14 04:42:35', 0),
(262, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:35', '2017-12-14 04:42:35', 0),
(263, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:35', '2017-12-14 04:42:35', 0),
(264, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:36', '2017-12-14 04:42:36', 0),
(265, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:36', '2017-12-14 04:42:36', 0),
(266, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:36', '2017-12-14 04:42:36', 0),
(267, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:36', '2017-12-14 04:42:36', 0),
(268, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:36', '2017-12-14 04:42:36', 0),
(269, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:36', '2017-12-14 04:42:36', 0),
(270, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:37', '2017-12-14 04:42:37', 0),
(271, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:37', '2017-12-14 04:42:37', 0),
(272, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:37', '2017-12-14 04:42:37', 0),
(273, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:37', '2017-12-14 04:42:37', 0),
(274, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:37', '2017-12-14 04:42:37', 0),
(275, 'hxud', 'jckf', 4, '153', 21, 3, 0, 1, '2017-12-14 04:42:38', '2017-12-14 04:42:38', 0),
(276, 'hxud', 'jckf', 4, '52', 21, 3, 0, 1, '2017-12-14 04:42:38', '2017-12-14 04:42:38', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveles`
--

CREATE TABLE `niveles` (
  `idNivel` int(11) NOT NULL,
  `nombreNivel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `niveles`
--

INSERT INTO `niveles` (`idNivel`, `nombreNivel`) VALUES
(1, 'Preescolar'),
(2, 'Primaria'),
(3, 'Secundaria'),
(4, 'Preparatoria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_inst`
--

CREATE TABLE `nivel_inst` (
  `idInstitucion` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nivel_inst`
--

INSERT INTO `nivel_inst` (`idInstitucion`, `idNivel`) VALUES
(1, 2),
(1, 3),
(1, 4),
(7, 2),
(7, 3),
(7, 4),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(11, 4),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(15, 1),
(15, 2),
(16, 3),
(17, 2),
(18, 4),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(20, 4),
(21, 1),
(21, 2),
(21, 3),
(21, 4),
(22, 2),
(23, 2),
(24, 2),
(25, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('009b913801f9f232034d0d8d08334277bd95e6dfba59ae6d272bca1205b2ed0f17ee32d3281cf4c1', 48, 1, 'MyApp', '[]', 0, '2017-12-10 01:40:38', '2017-12-10 01:40:38', '2018-12-09 17:40:38'),
('016b693fd647edbaf383de6fce5fc4fc46804ae75e4b1526f4ff91e9263e4cd95ca72f59216d00b4', 147, 1, 'MyApp', '[]', 0, '2017-11-14 02:42:23', '2017-11-14 02:42:23', '2018-11-13 18:42:23'),
('0185380b3059f2def07f916ae513a1a5ecbe3a6b0781a7382c7472bedfa4dba20123ebd52ebd49f8', 157, 1, 'MyApp', '[]', 0, '2017-12-14 00:02:29', '2017-12-14 00:02:29', '2018-12-13 16:02:29'),
('01a8568db66601bd08526d53613b6c8952da54b13a5e63d6ccb9b6e610c9ff4a8b1f6463bdc6b208', 48, 1, 'MyApp', '[]', 0, '2017-11-30 03:48:08', '2017-11-30 03:48:08', '2018-11-29 19:48:08'),
('01a91693e1846966ae6b74e50806cde02356beb4ffa00836885bd1a9968102c50bcc78261efa3be4', 132, 1, 'MyApp', '[]', 0, '2017-11-03 04:31:50', '2017-11-03 04:31:50', '2018-11-02 21:31:50'),
('01d1ac3a5fc03f957cfb1e3b9832d10bf9f3500721b61f563cfc3fe58c855b076a67ff0c20531ea1', 55, 1, 'MyApp', '[]', 0, '2017-10-24 21:29:39', '2017-10-24 21:29:39', '2018-10-24 14:29:39'),
('0235bfedc5685c2e66fa90c46131ce302b90b1c4872e5e6e8efc42078ee29a906ac777f942262d29', 48, 1, 'MyApp', '[]', 0, '2017-10-31 00:57:34', '2017-10-31 00:57:34', '2018-10-30 17:57:34'),
('03705dfda307ca5ed888b2e6d1824cfdf734a37ff89b6e5370ac3bf4caac7fa237e4ba5e117a0e69', 129, 1, 'MyApp', '[]', 0, '2017-12-13 01:46:06', '2017-12-13 01:46:06', '2018-12-12 17:46:06'),
('0488e0ae042a525ab1bd4fdbba760003b9dd50733400cb3605d547790bbf17d20484f9d88d5d9449', 48, 1, 'MyApp', '[]', 0, '2017-10-08 01:13:22', '2017-10-08 01:13:22', '2018-10-07 18:13:22'),
('052eb6aae61a2f669e5250edf5b8f00744e6965c79abd2f92e267e8bf29dcec4fc63c5c26eca6e1a', 85, 1, 'MyApp', '[]', 0, '2017-12-13 23:41:03', '2017-12-13 23:41:03', '2018-12-13 15:41:03'),
('05a3f538c29b9f9286cf8b22fb60a036c4cb1f94671b2da320ab450b85356adedb0637997d14b0ce', 147, 1, 'MyApp', '[]', 0, '2017-11-14 00:22:18', '2017-11-14 00:22:18', '2018-11-13 16:22:18'),
('0691b32f47d15d8974e25dfc3fac465013cea5ce9a40cfec23e07edaeddaf1913a337b6da87f279d', 21, 1, 'MyApp', '[]', 0, '2017-12-09 05:59:18', '2017-12-09 05:59:18', '2018-12-08 21:59:18'),
('06aa4fd9b9ac7bab607260f350df6c78f2c24c3034cb2ffd4f70594d7f838913f7e746b223d53f85', 161, 1, 'MyApp', '[]', 0, '2017-12-14 01:05:08', '2017-12-14 01:05:08', '2018-12-13 17:05:08'),
('06d31b97ec4c22aa5c7024762171746dd904922b5d6b5a3c21eef2b54b6e5e33896776fd230b5753', 48, 1, 'MyApp', '[]', 0, '2017-11-02 23:34:49', '2017-11-02 23:34:49', '2018-11-02 16:34:49'),
('08806a7eafd44d303db2a8fd2054dc873c990b2aad45fbeb62e1877fd8ddf66068166fe6ed7c52b0', 48, 1, 'MyApp', '[]', 0, '2017-12-14 04:41:17', '2017-12-14 04:41:17', '2018-12-13 20:41:17'),
('0a9a943e3ed3345eaf1968410c0f6d032748c566dc6556dd1343f66a0af5f3b9bf7d605aeb9f61e4', 4, 1, 'MyApp', '[]', 0, '2017-10-02 23:26:51', '2017-10-02 23:26:51', '2018-10-02 18:26:51'),
('0c0fda9255b8a6be317118ccf8c5ad29f04056015a2cb91ccc64ef6cfe18abc1b4364bb0bac5395d', 48, 1, 'MyApp', '[]', 0, '2017-12-14 23:16:14', '2017-12-14 23:16:14', '2018-12-14 15:16:14'),
('0c2c36d6e5ae9eac3fad570a8564ab3ae58ec462d8588848707ca40925eaa615152abd68a9d2f7a9', 128, 1, 'MyApp', '[]', 0, '2017-10-31 00:36:36', '2017-10-31 00:36:36', '2018-10-30 17:36:36'),
('0c2d71756e61ff8ab55fc7b49ab1e3488cfbbaac0a28e26eabfdee9d6481d366d3975ff5acd07e89', 82, 1, 'MyApp', '[]', 0, '2017-10-12 23:26:21', '2017-10-12 23:26:21', '2018-10-12 16:26:21'),
('0d30cc2c46b79aa8b2205e3682f73e7525848ad1416dc49f6c26d3dbee7508a22856985a9dd1aee7', 21, 1, 'MyApp', '[]', 0, '2017-12-14 00:24:32', '2017-12-14 00:24:32', '2018-12-13 16:24:32'),
('0dd1f431aea83abae8fded95ebabd19899505c47e97aaed35b1b4909f8d566d08b2adb4e8b4f10e6', 34, 1, 'MyApp', '[]', 0, '2017-12-13 23:46:24', '2017-12-13 23:46:24', '2018-12-13 15:46:24'),
('108ae9d9e9de5bca832ae7a78947f2cf79b1a71201025b251290aa7a2a3725dc14d7546a9060c373', 48, 1, 'MyApp', '[]', 0, '2017-10-13 05:05:24', '2017-10-13 05:05:24', '2018-10-12 22:05:24'),
('123804bf596c25cda592824814f6d4f11fac2fcf67b831f9f2beddaaa6301ac22dc7b2338d05752b', 21, 1, 'MyApp', '[]', 0, '2017-12-07 11:27:54', '2017-12-07 11:27:54', '2018-12-07 03:27:54'),
('16a9c622c6f9f64686bd851098991e95e4a2f52cd45298d9d918fc510d79af7f784dff947b9e4cf4', 21, 1, 'MyApp', '[]', 0, '2017-11-03 03:02:51', '2017-11-03 03:02:51', '2018-11-02 20:02:51'),
('1931f8c304933b4c9621a1c75e3ff2c228a42517734a66c7fb8e506377c8ae32d8db3b68e9346443', 34, 1, 'MyApp', '[]', 0, '2017-11-09 04:41:37', '2017-11-09 04:41:37', '2018-11-08 20:41:37'),
('1ab9bb92ae54b471edbc1b88601240fae23298c8fd1ce4545d40ad2ace018f49d9929ba5f1a315f4', 55, 1, 'MyApp', '[]', 0, '2017-10-20 22:38:13', '2017-10-20 22:38:13', '2018-10-20 15:38:13'),
('1f57168006644562cea9c818c55273d0bf7a875af5e1b23deadf8b7e63619bae89df7a2de1835cbd', 80, 1, 'MyApp', '[]', 0, '2017-10-12 21:58:19', '2017-10-12 21:58:19', '2018-10-12 14:58:19'),
('1f8b944ff7a4276776cf33451c95c1ad486f1bea1c5cf4ec3e6d45e839c3b6371c7ebe9c25a6d6b6', 160, 1, 'MyApp', '[]', 0, '2017-12-14 01:05:48', '2017-12-14 01:05:48', '2018-12-13 17:05:48'),
('200ec3d2ba63fdab57e2871efeea031175cda2cfbe4ca9c2cbe61d1bc26149feb14d0b5cf2e16776', 116, 1, 'MyApp', '[]', 0, '2017-10-28 06:01:53', '2017-10-28 06:01:53', '2018-10-27 23:01:53'),
('206c4c52dda754ed342f97107d66e15bb347fb07a8b572b2bdc4b789dd3b5d62bc6bfb0d4d366e41', 126, 1, 'MyApp', '[]', 0, '2017-10-30 23:04:46', '2017-10-30 23:04:46', '2018-10-30 16:04:46'),
('20de4ff6cc7c2c3b4435d167e6191e03943225a601fa8f646379cce9d12bc1d86c0d9bc43080f0eb', 48, 1, 'MyApp', '[]', 0, '2017-11-08 23:57:45', '2017-11-08 23:57:45', '2018-11-08 15:57:45'),
('2114d6849359218961b512b4f2d8576d19c6e97df5f62025c34c831d46b91b77c6b8e94f7ae220db', 21, 1, 'MyApp', '[]', 0, '2017-11-12 03:22:16', '2017-11-12 03:22:16', '2018-11-11 19:22:16'),
('236d93b5808755c4971b08ba79ce66a1a8233b9cac738f2097bc8bbe30daffc9915c5d44245574ea', 150, 1, 'MyApp', '[]', 0, '2017-12-13 23:45:24', '2017-12-13 23:45:24', '2018-12-13 15:45:24'),
('2457bc02a07f748df40e93db7c4bcae9a1373b280a918eb4ed47c6bb3a08a91fad4a47458d9481a5', 69, 1, 'MyApp', '[]', 0, '2017-12-13 01:45:35', '2017-12-13 01:45:35', '2018-12-12 17:45:35'),
('24cbf56717faf1a8b6ceaf2649de6db29fb6b55ba7a1815af548e1c5cb8994245dce1a9b6612ef34', 48, 1, 'MyApp', '[]', 0, '2017-11-14 03:27:30', '2017-11-14 03:27:30', '2018-11-13 19:27:30'),
('265e635ebfdebfe11e7298631f828da4d8174d73f0485655127523df4cddd451579b49e371f58e96', 21, 1, 'MyApp', '[]', 0, '2017-12-08 03:04:08', '2017-12-08 03:04:08', '2018-12-07 19:04:08'),
('27a634bae0f22cb4c0426699a48d66c1ebe4dc46f0cd357eb3080bbbf42202da23653e2359e32f3d', 34, 1, 'MyApp', '[]', 0, '2017-12-10 02:08:38', '2017-12-10 02:08:38', '2018-12-09 18:08:38'),
('27eb3da77520674e89deabbc7715dec5fc939b245b883d0d924cf45d679f84652eec5267dd53d4a0', 21, 1, 'MyApp', '[]', 0, '2017-12-13 23:53:00', '2017-12-13 23:53:00', '2018-12-13 15:53:00'),
('28f323303522ad014bdf3a2fb71283d3feef6407e73590e726e5d2a7d734b8cdf2af578f0893599a', 48, 1, 'MyApp', '[]', 0, '2017-10-13 04:48:55', '2017-10-13 04:48:55', '2018-10-12 21:48:55'),
('297c238f16256ffb7341f7a733b08255cd3b5fee800c19801b1d4e1d3cdcf795e5a80452674ce50f', 52, 1, 'MyApp', '[]', 0, '2017-10-09 22:39:54', '2017-10-09 22:39:54', '2018-10-09 15:39:54'),
('2b00b625c9751fd8bdac0f9fe82c9aae225a60a6c5e346ab637335123cb37a2e7d4150b770d541f8', 71, 1, 'MyApp', '[]', 0, '2017-10-12 01:19:43', '2017-10-12 01:19:43', '2018-10-11 18:19:43'),
('2b48086bf03c59af5f5bddd1202ff4275151b656ce784a0600ee92c13790455fab7867a1bb5464ef', 48, 1, 'MyApp', '[]', 0, '2017-10-24 21:15:17', '2017-10-24 21:15:17', '2018-10-24 14:15:17'),
('2b9a83e125ebebe075309220addfa8351b6bdc040cef0e1dcd0092bc475cc256a16057af9603059e', 21, 1, 'MyApp', '[]', 0, '2017-11-07 00:54:11', '2017-11-07 00:54:11', '2018-11-06 16:54:11'),
('2c0721627a6cf5626a242fbf0906c28064ff3e0055a0afbc60a98eb6dbf831eacf0d8b2e596ba1c4', 115, 1, 'MyApp', '[]', 0, '2017-10-28 05:55:57', '2017-10-28 05:55:57', '2018-10-27 22:55:57'),
('2cf043e786d54c7faa4227af0f81dca72c33b54fa8beb5d39602f692f8a63fe620989e43a49add8e', 55, 1, 'MyApp', '[]', 0, '2017-10-24 21:37:46', '2017-10-24 21:37:46', '2018-10-24 14:37:46'),
('2d2f645830c5a0e9a082d43ed59e3e37eab3fbf980eef3d7bc44545e7687ed0b8422978eff3e5e37', 21, 1, 'MyApp', '[]', 0, '2017-12-13 00:46:37', '2017-12-13 00:46:37', '2018-12-12 16:46:37'),
('2dd82204277ef440ca92e808d2744155a22e53ba4b74bc176d354fa09d36b6e34e2402935a1f62f3', 35, 1, 'MyApp', '[]', 0, '2017-10-03 00:19:32', '2017-10-03 00:19:32', '2018-10-02 19:19:32'),
('2e25053200c3546df7bab6447a58e18d381e300823bc37139d415df8e4c8d8fd65fab1a96aa21f2d', 21, 1, 'MyApp', '[]', 0, '2017-11-18 00:09:59', '2017-11-18 00:09:59', '2018-11-17 16:09:59'),
('2e5104c6813f797616dd9e6d02f8447fc3f05cc667098818a81cabb6db6064ce23f9df90534f48bb', 48, 1, 'MyApp', '[]', 0, '2017-12-13 00:51:12', '2017-12-13 00:51:12', '2018-12-12 16:51:12'),
('2e62e3d621c1601d38c82ce5d615a4425cddfc7b3a1fa8d8f8c1e2e4744df219e48e66a32781dc29', 52, 1, 'MyApp', '[]', 0, '2017-10-08 01:20:55', '2017-10-08 01:20:55', '2018-10-07 18:20:55'),
('2eadb56f98215206bc57e5492c59193d746d39d0a1e99570973beadcddaaed11422335c29936ebc8', 132, 1, 'MyApp', '[]', 0, '2017-11-03 04:27:44', '2017-11-03 04:27:44', '2018-11-02 21:27:44'),
('2f431895c8d05adb4382e23d0a99004f54869f3e5338dae71c6b48ec6e7b6d858098cfbd3b0d2a9b', 4, 1, 'MyApp', '[]', 0, '2017-10-09 22:40:15', '2017-10-09 22:40:15', '2018-10-09 15:40:15'),
('2f5fa7305b505fb6cc216f1c2e4a6f120a50245d3db938774a1af4b1b8d81c24c420b329c6262d3a', 55, 1, 'MyApp', '[]', 0, '2017-10-22 00:01:02', '2017-10-22 00:01:02', '2018-10-21 17:01:02'),
('2f81d15a0ff2fbe7db1e990eb415870d8da9bd79528c35ed4492f2fa7243acf4273a99e3df18835d', 147, 1, 'MyApp', '[]', 0, '2017-11-11 04:01:06', '2017-11-11 04:01:06', '2018-11-10 20:01:06'),
('30663fd7b86f0ec1fd2a1603371ced4a1fa988f450faf604b4dd2c790522346a4929deffd5eb9e12', 85, 1, 'MyApp', '[]', 0, '2017-10-13 02:59:48', '2017-10-13 02:59:48', '2018-10-12 19:59:48'),
('3092837482d876e12f18bf7e23a5ea99db05d246c1a106aeeb5d2e78fb394790aa8cf97590ce0a18', 48, 1, 'MyApp', '[]', 0, '2017-10-24 03:18:23', '2017-10-24 03:18:23', '2018-10-23 20:18:23'),
('317777f046e05a7e5761167e237f8e5f798ca01f74c9a5a4d92019137524938944232454ae52b806', 21, 1, 'MyApp', '[]', 0, '2017-11-14 08:19:40', '2017-11-14 08:19:40', '2018-11-14 00:19:40'),
('33b63dd69fc76a8087cb80e67f183759ff5a47ad7daa94e60d578a0a2db03d17b498a203cf3a4f30', 34, 1, 'MyApp', '[]', 0, '2017-11-16 04:19:40', '2017-11-16 04:19:40', '2018-11-15 20:19:40'),
('34438c501b6fb6ce394f9ded8b9486a08e5399e8e67368ef89c5d0cdab2516a4269bfb51e1b5c7b5', 119, 1, 'MyApp', '[]', 0, '2017-10-30 22:41:39', '2017-10-30 22:41:39', '2018-10-30 15:41:39'),
('347ae6c687d7f54fc013913d3bc5fd52cd850e6283130580d5e52c31cf0002155e5012dc5128848f', 48, 1, 'MyApp', '[]', 0, '2017-10-08 01:21:49', '2017-10-08 01:21:49', '2018-10-07 18:21:49'),
('35e7647962b22338adeb7e5ff3cf00b791b4bfae110d8525ab31507172bb2f10a9b2be9fcdeaf742', 127, 1, 'MyApp', '[]', 0, '2017-10-31 00:06:33', '2017-10-31 00:06:33', '2018-10-30 17:06:33'),
('36794352d94fb11baac1cbbe6122a21cc2918eb25c3bcb60e9c06f063abb0458e3985c8a9041a93b', 4, 1, 'MyApp', '[]', 0, '2017-10-09 22:25:28', '2017-10-09 22:25:28', '2018-10-09 15:25:28'),
('37713caf65225741b3f5a23285250f3e1b3cb40be2c52024b7a476233d55768b5977c4da7d6d9f84', 48, 1, 'MyApp', '[]', 0, '2017-11-08 23:30:34', '2017-11-08 23:30:34', '2018-11-08 15:30:34'),
('38978be50f3f27ad20db11b187c2cfe848a074f4d8613f5f501d81257df65f8be2d554ac2513024f', 48, 1, 'MyApp', '[]', 0, '2017-11-14 08:16:12', '2017-11-14 08:16:12', '2018-11-14 00:16:12'),
('389f3e15eee1ebd984dee9bb148485d4a5024f0aa23b684c7da2b217b7359441a4b3bf1261663210', 21, 1, 'MyApp', '[]', 0, '2017-11-07 00:56:28', '2017-11-07 00:56:28', '2018-11-06 16:56:28'),
('3a5cfa5dcee33c99c0c76001cada35cf3799b1c054f6de521a347488647ea12bf79dec656deab7d7', 151, 1, 'MyApp', '[]', 0, '2017-12-14 23:08:18', '2017-12-14 23:08:18', '2018-12-14 15:08:18'),
('3d3911c54205d4f3192d20d3afb54dbb8293eb8950813460eaff51bfa41ed98c6a7f82c4082eed5a', 55, 1, 'MyApp', '[]', 0, '2017-11-11 04:40:12', '2017-11-11 04:40:12', '2018-11-10 20:40:12'),
('3dc5a6020916a1ceb8a185d488a517e7bdc41a4c89f5967c7c6987f92b83608d92434c7d134166f9', 48, 1, 'MyApp', '[]', 0, '2017-12-13 23:50:45', '2017-12-13 23:50:45', '2018-12-13 15:50:45'),
('4135e5b6093dc6ee6b53f480aae70cbcac2578abb1de87134ddd2695218ae83958732f196f1afbdb', 21, 1, 'MyApp', '[]', 0, '2017-11-15 04:19:48', '2017-11-15 04:19:48', '2018-11-14 20:19:48'),
('415045475245d0bc654de728f4d7d0ad0fc06180b9c8f79564be2f08d32b3a63e3f69fe63aa32eff', 48, 1, 'MyApp', '[]', 0, '2017-10-24 21:49:27', '2017-10-24 21:49:27', '2018-10-24 14:49:27'),
('419730a76e9fb4637eaa0c3c9386dd752d5bf980926bf359774e226b7eed9ddaafdc2f262cbabd9a', 99, 1, 'MyApp', '[]', 0, '2017-10-27 00:50:20', '2017-10-27 00:50:20', '2018-10-26 17:50:20'),
('4207f376258a862af3e659c050bc3fa6841aa94d0fed7cc68916473b466d28b5333a901b7747fccd', 48, 1, 'MyApp', '[]', 0, '2017-10-13 02:22:04', '2017-10-13 02:22:04', '2018-10-12 19:22:04'),
('463b7bfe42396710343ab7b36fd98c021b71d683b4bcf122f665716f9f2b988fc59417314e130a7d', 21, 1, 'MyApp', '[]', 0, '2017-12-13 01:04:53', '2017-12-13 01:04:53', '2018-12-12 17:04:53'),
('4766a0f8da90328f1dc1df3a6747bc0269d10c415f6870db871186b895a1e79f15d515bf80534c0d', 48, 1, 'MyApp', '[]', 0, '2017-10-31 01:07:56', '2017-10-31 01:07:56', '2018-10-30 18:07:56'),
('4772c5ecdcab6d126b4a64cf6a12ffe42ea23bd0fdcfa8ccb07297b280256d324c04c0e27ea04182', 55, 1, 'MyApp', '[]', 0, '2017-10-22 00:16:51', '2017-10-22 00:16:51', '2018-10-21 17:16:51'),
('49286f85abd80863bca369634c43f81cb32e11b0524505b8b92a7b591778f93583f94e2f506ae559', 112, 1, 'MyApp', '[]', 0, '2017-10-26 05:22:07', '2017-10-26 05:22:07', '2018-10-25 22:22:07'),
('4d303b0fe054c299d41731ef198059ef4d21667d165154a161dea8416665288bf600020b721aabb8', 21, 1, 'MyApp', '[]', 0, '2017-11-02 03:44:44', '2017-11-02 03:44:44', '2018-11-01 20:44:44'),
('4d4271c8e23d32a877d519d9e14f94f922faa52cb80b38f6e46bc99d7de8e380b90d41adf1206195', 48, 1, 'MyApp', '[]', 0, '2017-10-27 01:37:06', '2017-10-27 01:37:06', '2018-10-26 18:37:06'),
('4e5065020dc4d952acba17a6074ea4f83f9532da2a946f6d9ace318f56557acfbc66a46ae6d9e9cb', 62, 1, 'MyApp', '[]', 0, '2017-10-10 05:17:04', '2017-10-10 05:17:04', '2018-10-09 22:17:04'),
('4e89767be7840c7d6a7bb148e75d5e2177740b3dbd85cf7ce2533e47eece104168529bcc1a2bea17', 122, 1, 'MyApp', '[]', 0, '2017-10-30 22:58:14', '2017-10-30 22:58:14', '2018-10-30 15:58:14'),
('4f604d28c8dccc14afed63fce720adcf786f0ae50afeb619386caea051366178cc2472a826777f20', 21, 1, 'MyApp', '[]', 0, '2017-11-29 07:47:40', '2017-11-29 07:47:40', '2018-11-28 23:47:40'),
('4f915d4e6f29fe83b20f7cc79286b06ef45c6dab3f32a47620ffedd0699b73ecc011e40ecf13f670', 21, 1, 'MyApp', '[]', 0, '2017-11-12 02:56:58', '2017-11-12 02:56:58', '2018-11-11 18:56:58'),
('502928356159ed0ccf32322b3ef998b415f309bbad1f380ed82abb6d4d648581f1afbd4de66ca7c5', 21, 1, 'MyApp', '[]', 0, '2017-11-07 05:24:26', '2017-11-07 05:24:26', '2018-11-06 21:24:26'),
('50d01d54cfcb5d405a1c8d140e5113ff9353c92e3ccddd24384f8f0ca32c010de2ecda9c9be528b4', 21, 1, 'MyApp', '[]', 0, '2017-10-27 01:05:45', '2017-10-27 01:05:45', '2018-10-26 18:05:45'),
('512b54715e5b0553e519a38fc4c67708bb026cea7c47d1b41e1587f983dbdb1f00cd5758271b40ac', 34, 1, 'MyApp', '[]', 0, '2017-12-12 02:59:29', '2017-12-12 02:59:29', '2018-12-11 18:59:29'),
('523377cd7bc62d970b269d1bd8c91a1fc4e1b9ffe07cabeecda4e3f4e5821848e98aa9399777fb52', 112, 1, 'MyApp', '[]', 0, '2017-10-26 05:20:21', '2017-10-26 05:20:21', '2018-10-25 22:20:21'),
('54938b6637b1bb56b41b117b69cfbd27ddf5ba57386b8a1ab136c78c45f901a7dcca0d6787eb9908', 55, 1, 'MyApp', '[]', 0, '2017-10-20 04:48:08', '2017-10-20 04:48:08', '2018-10-19 21:48:08'),
('55852e9e423203001ce4c891316dd1694b4bf400258094d43c1ce4aeaef513e95686f2a90c6b695d', 48, 1, 'MyApp', '[]', 0, '2017-10-09 21:20:42', '2017-10-09 21:20:42', '2018-10-09 14:20:42'),
('55aedb16bd5f05ade3cb51b5b6eb4218decc7f083b5eb1d0a3cad1c68798935414bdd7bc5f85dbba', 34, 1, 'MyApp', '[]', 0, '2017-12-13 00:36:35', '2017-12-13 00:36:35', '2018-12-12 16:36:35'),
('56fd838bc3d1dcdfdff9550353796ee98d46c0c31a8a671deca9202639d323804baf0ef294aa76e6', 129, 1, 'MyApp', '[]', 0, '2017-10-31 00:51:35', '2017-10-31 00:51:35', '2018-10-30 17:51:35'),
('579a6a75f2ef248dfbc9e2668c954baef76b2bc1407cde2e89d56a0cb3921ff0cf429bc5fee09645', 151, 1, 'MyApp', '[]', 0, '2017-12-14 04:37:54', '2017-12-14 04:37:54', '2018-12-13 20:37:54'),
('5953ab0c5a04a7aa33f93da2b071729685669c8db73832c3e60d41dff0d2804fd6ddd712a474ed35', 21, 1, 'MyApp', '[]', 0, '2017-12-14 04:38:50', '2017-12-14 04:38:50', '2018-12-13 20:38:50'),
('5971abb34969e8392b3defe8a46e1c95b844c24f7e24b4e69a7c39bb0702d5786bf3d12cc518d84f', 55, 1, 'MyApp', '[]', 0, '2017-10-26 22:03:11', '2017-10-26 22:03:11', '2018-10-26 15:03:11'),
('5b9ffc2926f234db0c0552319aa11811e622cbad2aa2ffe5ce5fc9dd7a7ae1cbbde82adf991c20f0', 34, 1, 'MyApp', '[]', 0, '2017-10-03 00:19:15', '2017-10-03 00:19:15', '2018-10-02 19:19:15'),
('5bfdf179650c447d0474a4e7434f82dfe8fc1adc12b29d401b015532d490cc47230c8d4166364811', 21, 1, 'MyApp', '[]', 0, '2017-11-16 07:27:58', '2017-11-16 07:27:58', '2018-11-15 23:27:58'),
('5c26b952bfe61076d13b827af6a8eb72a1279f4e54af62188555b2f817d5a1b56c7bb3484dd22932', 48, 1, 'MyApp', '[]', 0, '2017-11-14 00:11:42', '2017-11-14 00:11:42', '2018-11-13 16:11:42'),
('5cb8e953969e3791ab629e967fce976e128b19f299e65ca247e7bc7cd1411f81123cc0d654f186de', 86, 1, 'MyApp', '[]', 0, '2017-10-13 03:01:33', '2017-10-13 03:01:33', '2018-10-12 20:01:33'),
('5d9ea748bc737ab3d6ed4a994f84e998ccdb3dc27134d72fbf258a1872e6642c2daf2464497acb37', 55, 1, 'MyApp', '[]', 0, '2017-10-27 02:03:12', '2017-10-27 02:03:12', '2018-10-26 19:03:12'),
('5e11f6bbafcbd07f41f9b7cc6121bf3d93d56691c2215e60bab5542cfa98e850b0113c8e1da1f7e7', 48, 1, 'MyApp', '[]', 0, '2017-12-11 13:30:16', '2017-12-11 13:30:16', '2018-12-11 05:30:16'),
('5ea0e752034d3f6f173caf4376ab318c29091f7bdeb67d4791c8211b6b18f80b1a226658d3afa7bd', 55, 1, 'MyApp', '[]', 0, '2017-11-03 02:37:41', '2017-11-03 02:37:41', '2018-11-02 19:37:41'),
('649234b60501785691648c54c3bd6dd9f6c96411617344d34e19e4489b6790a00031e48281c78230', 48, 1, 'MyApp', '[]', 0, '2017-11-08 04:20:56', '2017-11-08 04:20:56', '2018-11-07 20:20:56'),
('65664164c209a5385bbcb62999782ca38d45f0da9626f82ed88408cf0d7889cbfea9b7294842d3b4', 48, 1, 'MyApp', '[]', 0, '2017-12-13 01:36:08', '2017-12-13 01:36:08', '2018-12-12 17:36:08'),
('66a12ee75a6286aa54262013d483b7716c655a19d41e108416953b483cec970687f053e932c5b1c6', 51, 1, 'MyApp', '[]', 0, '2017-10-08 01:18:27', '2017-10-08 01:18:27', '2018-10-07 18:18:27'),
('66b361f4a3f45250fb656f06663c8f6677c3f39b8da0ab60c3b1e3a7e926b9cc69ed1b5355e6c0ad', 117, 1, 'MyApp', '[]', 0, '2017-10-30 22:27:29', '2017-10-30 22:27:29', '2018-10-30 15:27:29'),
('66e98f205a9172c119c0a29cc0f0824ab8acc4dda41cfbc4ba78a2c6aad1e165db80761846628d0c', 136, 1, 'MyApp', '[]', 0, '2017-11-03 04:31:25', '2017-11-03 04:31:25', '2018-11-02 21:31:25'),
('68d9014aa6c4904a30f291b6825b621ff5cd385aa25093d61284b2552d1c25f6468e56cf898840ac', 48, 1, 'MyApp', '[]', 0, '2017-10-24 22:06:23', '2017-10-24 22:06:23', '2018-10-24 15:06:23'),
('6927a104edb4c3bde35ff1b9d4d5f9916b898eb6fa17bb69d8d4fa524d1c019823b226e071c9b1cb', 34, 1, 'MyApp', '[]', 0, '2017-12-10 01:35:02', '2017-12-10 01:35:02', '2018-12-09 17:35:02'),
('6d7b4aba132b9973b297f3be20b9b8f4c6e472a86828539dd54d9b2737e91c9e52e76ac78561f80a', 21, 1, 'MyApp', '[]', 0, '2017-11-02 22:47:38', '2017-11-02 22:47:38', '2018-11-02 15:47:38'),
('6e91b719253ce742d65d1da64ac2cd178c81b53217b94ed8cad81c4e4757552c42ed34b0aa987768', 21, 1, 'MyApp', '[]', 0, '2017-12-13 01:38:50', '2017-12-13 01:38:50', '2018-12-12 17:38:50'),
('70b710b05e488805cc1e54fe94ca550c63c84de18f11213632152a952ed92acea730efd64f60403a', 48, 1, 'MyApp', '[]', 0, '2017-12-14 03:43:02', '2017-12-14 03:43:02', '2018-12-13 19:43:02'),
('719ec04de4029a60f5d57bd7150bdee6b8f06d1c14894c33e6a7b02e0349b52ceeef77cf544c6e72', 156, 1, 'MyApp', '[]', 0, '2017-12-14 00:02:42', '2017-12-14 00:02:42', '2018-12-13 16:02:42'),
('71cbe9d6b49d5cca38bbc3a6732edecd1117152eb787e2027b027c13dc7a079767312198b018fe7d', 34, 1, 'MyApp', '[]', 0, '2017-12-10 01:59:55', '2017-12-10 01:59:55', '2018-12-09 17:59:55'),
('74a88abedc16ea637c3f66e3fa5bc4cf82ec4216a487e4037bf759bc0698338bd8091aaf820479ba', 21, 1, 'MyApp', '[]', 0, '2017-12-08 03:06:03', '2017-12-08 03:06:03', '2018-12-07 19:06:03'),
('7526e050609cae755525a34044ce5e6bdab7e34973b283855dc35d1b75a2885b265d07068c9762a5', 36, 1, 'MyApp', '[]', 0, '2017-10-03 00:20:59', '2017-10-03 00:20:59', '2018-10-02 19:20:59'),
('75a9fa73c07d6b24e201a871a40d679a182cc78c22a2420e5e5b08c6b4d724a84d35bd668257ab7b', 70, 1, 'MyApp', '[]', 0, '2017-10-11 23:00:42', '2017-10-11 23:00:42', '2018-10-11 16:00:42'),
('768e89d7a176f532da62cebc54284f449ccf2f0720d3324c9b38c7327414eef49f15ec8ee6b2f62a', 21, 1, 'MyApp', '[]', 0, '2017-12-07 04:41:07', '2017-12-07 04:41:07', '2018-12-06 20:41:07'),
('769d1767c2039de04604d03a64d5cc994c2e2310fa77d50b9f61f16ca02743b282af5eaec8d03fcb', 147, 1, 'MyApp', '[]', 0, '2017-11-11 02:36:26', '2017-11-11 02:36:26', '2018-11-10 18:36:26'),
('772ba1f3ce0fb1b4168fa8dfc1ff7d7c7d2846be333662678e66104020a5cf721310e80014fa4868', 21, 1, 'MyApp', '[]', 0, '2017-10-13 02:55:16', '2017-10-13 02:55:16', '2018-10-12 19:55:16'),
('789adafda00cb15bcefab742131b257ae6446997031840340625b360a54a8d5697b707356c7a6f06', 48, 1, 'MyApp', '[]', 0, '2017-11-23 07:22:24', '2017-11-23 07:22:24', '2018-11-22 23:22:24'),
('7d484b4d0601ef9ee90836ae92e357c9bacb69a3b429745da638abc301fbcde6aca208b8df991967', 55, 1, 'MyApp', '[]', 0, '2017-10-21 23:56:21', '2017-10-21 23:56:21', '2018-10-21 16:56:21'),
('7dedce6769cfbdb3d7b0c02efaddaebece2b69eee94bdca0e3577959403d2ec7177922fc1efc6ca7', 21, 1, 'MyApp', '[]', 0, '2017-11-22 01:26:53', '2017-11-22 01:26:53', '2018-11-21 17:26:53'),
('7e29c8cc7535e29ed573499204a545cd99d97bf5b0273eda86d6f8f0913c02d87645979182f12500', 21, 1, 'MyApp', '[]', 0, '2017-11-24 00:25:48', '2017-11-24 00:25:48', '2018-11-23 16:25:48'),
('7fef1b3d1fda8941f4b6fa9a336b3d718c2764dd6fd9fc3ff2dde3112767466d188d42e361dec24a', 129, 1, 'MyApp', '[]', 0, '2017-10-31 00:53:17', '2017-10-31 00:53:17', '2018-10-30 17:53:17'),
('80033a9b364832b13b55db38e6e6df18e7ccacbc4ac81c8eb65d9fda82bd528074dc7cf9ed5ff91f', 118, 1, 'MyApp', '[]', 0, '2017-10-30 22:38:44', '2017-10-30 22:38:44', '2018-10-30 15:38:44'),
('810d9e726587f5184bae07e976602af29eedaae245bf17a9407c14a4273146830321fa163c3dda39', 151, 1, 'MyApp', '[]', 0, '2017-12-13 23:50:53', '2017-12-13 23:50:53', '2018-12-13 15:50:53'),
('81a9099e66be21a588755dc166a96f71a43f8ddf36d504a51c23a5cde0c70bf05103a89e7ff05de4', 21, 1, 'MyApp', '[]', 0, '2017-10-27 01:10:36', '2017-10-27 01:10:36', '2018-10-26 18:10:36'),
('82126c756a6065e8e8f1246c950bae5d6d6a17916de46e924f1658331f83b8b92f7c7046aacf7966', 125, 1, 'MyApp', '[]', 0, '2017-10-30 22:59:54', '2017-10-30 22:59:54', '2018-10-30 15:59:54'),
('822f55324ac51cb21588b0f83a20ace7d05993659c42d84a69cad8569633e67f7c0153e1b6d2cd58', 55, 1, 'MyApp', '[]', 0, '2017-10-24 21:41:33', '2017-10-24 21:41:33', '2018-10-24 14:41:33'),
('8250e94fa7390ffae720596f1e71f86d283ea4213a40e90b672e26cbf19d2d3236417740bfba0ef7', 61, 1, 'MyApp', '[]', 0, '2017-10-10 05:21:03', '2017-10-10 05:21:03', '2018-10-09 22:21:03'),
('8332e11b305a6705efdb56050404169ef0ce2745a3b3c0cba910a3081b3b89944f01ca8607f062e8', 81, 1, 'MyApp', '[]', 0, '2017-10-12 23:27:13', '2017-10-12 23:27:13', '2018-10-12 16:27:13'),
('874267fc572f2c0ec0e6652c9ad6d0769ded07b4ed5b4b5e3c83a496f5e3195bf2a16077761d2a4c', 48, 1, 'MyApp', '[]', 0, '2017-11-02 06:28:26', '2017-11-02 06:28:26', '2018-11-01 23:28:26'),
('878be6258c6b20b08b704a8d6ab6e0cab04c7b57638bfb17f2ce1cd04df042a08e1a6f4f0c2b4081', 21, 1, 'MyApp', '[]', 0, '2017-11-28 02:53:11', '2017-11-28 02:53:11', '2018-11-27 18:53:11'),
('880590851684d84351365f127dac9350342cc1dd8a92facac7ad1ea9a9a4be777c5f844850ed23d1', 48, 1, 'MyApp', '[]', 0, '2017-12-14 01:29:25', '2017-12-14 01:29:25', '2018-12-13 17:29:25'),
('8aa4fc9830aef17e0ad8eb83099dad25a9be317635b7b94629f57cf12aea92b5888d25c2cc872c6b', 21, 1, 'MyApp', '[]', 0, '2017-11-09 02:07:11', '2017-11-09 02:07:11', '2018-11-08 18:07:11'),
('8ab66cbb4bd2e77b4df6422a6fd5d14b4a4a44b830f1c39f6326c2249f2bb65faf50b9c1f6676343', 70, 1, 'MyApp', '[]', 0, '2017-10-11 23:09:54', '2017-10-11 23:09:54', '2018-10-11 16:09:54'),
('8bbf75183bceb0d52fe84b03f9535d05d4f1ea82cbedc4a32af662a234b06f049c939ad314e37d50', 147, 1, 'MyApp', '[]', 0, '2017-11-11 02:36:22', '2017-11-11 02:36:22', '2018-11-10 18:36:22'),
('8c3e404e41f9197546b7dffc4e4961719e40f15307288054292cb723889f5d560f6edecefef043b8', 48, 1, 'MyApp', '[]', 0, '2017-12-13 23:14:18', '2017-12-13 23:14:18', '2018-12-13 15:14:18'),
('9076c35679c1fcd1236494545a1636e8df19bc24e0adc67fdd10e60717ddcff0a19efd884e659264', 48, 1, 'MyApp', '[]', 0, '2017-11-12 02:03:45', '2017-11-12 02:03:45', '2018-11-11 18:03:45'),
('90c2fc27edb7b3cbb58e02e3341598f77f66180eacffb0a39627183aacf579a596ef78e5342309a6', 21, 1, 'MyApp', '[]', 0, '2017-11-07 05:23:30', '2017-11-07 05:23:30', '2018-11-06 21:23:30'),
('918d526126c77e736ab53a1a3df22d331c2d39517fa308e5d80dca14e72b0e71cef1859d6297bc86', 48, 1, 'MyApp', '[]', 0, '2017-10-13 02:44:18', '2017-10-13 02:44:18', '2018-10-12 19:44:18'),
('9292632f0f7cbb26aad70652226e267b1339caab7cb12b53f478846cafb35a5f38440f56156b13d8', 139, 1, 'MyApp', '[]', 0, '2017-11-08 08:03:36', '2017-11-08 08:03:36', '2018-11-08 00:03:36'),
('92c060da33177801cd9fc376093c7cc99766d8b0a3d91b8c603aa7e5e6f3c0a9e31920fa909c701e', 48, 1, 'MyApp', '[]', 0, '2017-12-08 23:38:24', '2017-12-08 23:38:24', '2018-12-08 15:38:24'),
('935ada49450d24a8c785092e71c220b80213d25a1c4725503b6fdd9f632403fc74493daa03c02b06', 21, 1, 'MyApp', '[]', 0, '2017-11-29 03:03:14', '2017-11-29 03:03:14', '2018-11-28 19:03:14'),
('9362d9abac25882c042d416cff1d8f4665b52558e8c01302425ab3a1496f8f30a3191133497593c1', 48, 1, 'MyApp', '[]', 0, '2017-10-27 02:04:03', '2017-10-27 02:04:03', '2018-10-26 19:04:03'),
('94b5e9cf4379d12cad03734f921fe2789cc9ebf7679bf2b97ac490c2a314cdebe75e5654097adedd', 34, 1, 'MyApp', '[]', 0, '2017-11-09 01:02:01', '2017-11-09 01:02:01', '2018-11-08 17:02:01'),
('954a8e6687a1b94614ac29824367268d6a553f85062e1fee521e7587e7e0f7a7557ca0d4107afb8d', 79, 1, 'MyApp', '[]', 0, '2017-10-12 22:03:50', '2017-10-12 22:03:50', '2018-10-12 15:03:50'),
('966f6f23938a8bde34a4e54123e24af7a53f2f6151d2f9993e12ea455930da5972a5910f2a93b180', 4, 1, 'MyApp', '[]', 0, '2017-10-09 22:30:33', '2017-10-09 22:30:33', '2018-10-09 15:30:33'),
('968d31aa257d6b2b03acac8aa4a06f046b7885ffbbccc4b5991e832ed7f9ac657b8d568ae4c0345c', 21, 1, 'MyApp', '[]', 0, '2017-11-12 03:30:00', '2017-11-12 03:30:00', '2018-11-11 19:30:00'),
('987171f65691605d9754ecd37381ca4dc29d2e27bba3e8a88971a892e315c057176f44e77d3d2cd1', 48, 1, 'MyApp', '[]', 0, '2017-11-14 02:12:14', '2017-11-14 02:12:14', '2018-11-13 18:12:14'),
('98ab5ca14e3fe449dd12aadae3ee01cbfbaa55d49eed9bedccd63ef0c4db33156bee79241a218425', 48, 1, 'MyApp', '[]', 0, '2017-10-10 04:46:42', '2017-10-10 04:46:42', '2018-10-09 21:46:42'),
('98d94170f7898c01e95db66f85c64e1e72ae8edca3575a1361fe2621d6680585f50fcd8b0d705781', 34, 1, 'MyApp', '[]', 0, '2017-12-13 01:40:08', '2017-12-13 01:40:08', '2018-12-12 17:40:08'),
('996b31a4534f355d3f95049e9a2b95b62e715e0cff0a14d950617347fa1efb734bd3cbc84474c464', 151, 1, 'MyApp', '[]', 0, '2017-12-14 01:06:26', '2017-12-14 01:06:26', '2018-12-13 17:06:26'),
('99b0e862d686f4f2a90c93a4039fa55435236d0caf0ef0884de0a06ab130d7aa13536aee91d8bb75', 113, 1, 'MyApp', '[]', 0, '2017-10-26 05:21:42', '2017-10-26 05:21:42', '2018-10-25 22:21:42'),
('9aca36005853a13ba0af3714edcf13d73f0886abd5b8404f00424cd14bd2f2d4729a0d7ef817a6f4', 48, 1, 'MyApp', '[]', 0, '2017-12-07 11:22:44', '2017-12-07 11:22:44', '2018-12-07 03:22:44'),
('9b81ea88d46c9e77683ff99e8906cb4b7e7230e1e645e92f659aec4fe24a08a24a97aece8bd770b4', 48, 1, 'MyApp', '[]', 0, '2017-10-27 02:02:44', '2017-10-27 02:02:44', '2018-10-26 19:02:44'),
('9c2df1627c044c0054353bbb885962eab5ea82818d7dd76ee05719dc7c050000809bf8ceef7d2459', 48, 1, 'MyApp', '[]', 0, '2017-11-28 23:45:52', '2017-11-28 23:45:52', '2018-11-28 15:45:52'),
('9c799986500cf42b03cf87a9da62f3175447d767c70fbb4c4f763a6d11dde0051f8c9d112793edb0', 130, 1, 'MyApp', '[]', 0, '2017-10-31 00:52:56', '2017-10-31 00:52:56', '2018-10-30 17:52:56'),
('9ca585a94cb3b424d1fd781de80d305a05af202c3c0af9687cbb16c245ff30f088acebd4e2c9ad21', 55, 1, 'MyApp', '[]', 0, '2017-10-20 01:41:14', '2017-10-20 01:41:14', '2018-10-19 18:41:14'),
('9cdc85930fd9828ec8a62e4238be3b960c542a0b30757260caac85c2a9813160137afcb3e790d082', 120, 1, 'MyApp', '[]', 0, '2017-10-30 22:48:45', '2017-10-30 22:48:45', '2018-10-30 15:48:45'),
('9daccb6b0aee5b4408524dacf765e6b0bc9df0d642673c3e36dc55dbfc8b2df1cd78d5d7f14f2c87', 48, 1, 'MyApp', '[]', 0, '2017-12-07 14:26:53', '2017-12-07 14:26:53', '2018-12-07 06:26:53'),
('9dda78d9c012fb51832e121307671653c9042749b5e9576c726f721ef1568a0ec31c4f5fbe455e82', 55, 1, 'MyApp', '[]', 0, '2017-10-31 23:34:53', '2017-10-31 23:34:53', '2018-10-31 16:34:53'),
('9e0260243bc3d5dce1a65e42ffad730ad5898774e4a410caef88ad5e5a3e5aaa63c826fd23f0ba04', 61, 1, 'MyApp', '[]', 0, '2017-10-10 05:38:53', '2017-10-10 05:38:53', '2018-10-09 22:38:53'),
('a06fde371863893b12840a03af42b7c7ad7f99725c9e49527f700be134b0c69ad716e68e3d1acbfb', 21, 1, 'MyApp', '[]', 0, '2017-12-14 04:34:05', '2017-12-14 04:34:05', '2018-12-13 20:34:05'),
('a084cdf51dd637abbffe84d04f9407bd0aaa21c81b9219756c5b0057688f6b979e7f3d94cf59cb6c', 55, 1, 'MyApp', '[]', 0, '2017-10-24 23:32:47', '2017-10-24 23:32:47', '2018-10-24 16:32:47'),
('a1af9c0968bc8a0a24aaa445c933acd764f08a89f5369394614e3cc5ce1675e5d4e7223c1d7356e9', 48, 1, 'MyApp', '[]', 0, '2017-11-03 05:00:05', '2017-11-03 05:00:05', '2018-11-02 22:00:05'),
('a5b50b9d280bb25b3ec553abf8d29ddd9300fe4d518352e72068a24ffc440b87bcb99614a2b970d1', 48, 1, 'MyApp', '[]', 0, '2017-11-07 04:41:52', '2017-11-07 04:41:52', '2018-11-06 20:41:52'),
('a5f53ced1578b4f4ad76a6082bdb8450f8779c9241ca4564df505d0aa10a4cf6b062e9df8df3c905', 48, 1, 'MyApp', '[]', 0, '2017-10-12 05:41:46', '2017-10-12 05:41:46', '2018-10-11 22:41:46'),
('a61641fe1a376886f6749f452390f9c5bf88a84e0127fa9a1678448cf1d056ed41b11c0bcc35747d', 48, 1, 'MyApp', '[]', 0, '2017-10-18 23:41:08', '2017-10-18 23:41:08', '2018-10-18 16:41:08'),
('a6a863aa5ba0c76ce958cac34e3ccea81c32ae4c4898b8dcfb9414ad781547bb84d5dab4940848dd', 21, 1, 'MyApp', '[]', 0, '2017-11-07 05:48:39', '2017-11-07 05:48:39', '2018-11-06 21:48:39'),
('a72a2304eb6caeb06fa5d99fcb12a5cd908db483fa11fbb6d3ffd98a293c604f409fa51a301a1846', 153, 1, 'MyApp', '[]', 0, '2017-12-13 23:50:21', '2017-12-13 23:50:21', '2018-12-13 15:50:21'),
('a73e73febc89fa204d0e5513c9bb6b355cfd69e0ad747cfb1a905dddbd3fd5d2394b9f62c7c3e70d', 55, 1, 'MyApp', '[]', 0, '2017-11-11 08:06:12', '2017-11-11 08:06:12', '2018-11-11 00:06:12'),
('a8763a5f771329396fa12ce4245f1301c1b20b4dcdcf2132b4fea4c579c930cc45b3a5d218386dcd', 48, 1, 'MyApp', '[]', 0, '2017-10-31 06:54:19', '2017-10-31 06:54:19', '2018-10-30 23:54:19'),
('a9057052ced9a880b578f3005efdddd2d5db85c7702a5542b0a37dd264f69daeda942bc91c3857c8', 55, 1, 'MyApp', '[]', 0, '2017-11-01 06:49:27', '2017-11-01 06:49:27', '2018-10-31 23:49:27'),
('a909894a4395a99297f6eefd6aae8bd280f6f8a5bb393a3b7d8a4ade3b9530282b70db8addabf448', 48, 1, 'MyApp', '[]', 0, '2017-11-08 00:34:13', '2017-11-08 00:34:13', '2018-11-07 16:34:13'),
('a951c595ea38ef47a0b2620cfb46e051eb9df153f46cc22786f8381beff359be12dd5db9a9410d09', 81, 1, 'MyApp', '[]', 0, '2017-10-12 23:25:08', '2017-10-12 23:25:08', '2018-10-12 16:25:08'),
('a9aae157bdcc10a66de7a3cf0d1984b3d34a4c04402d85fe2765efe286a2ecb2c5771ceee497a08f', 21, 1, 'MyApp', '[]', 0, '2017-12-07 14:29:09', '2017-12-07 14:29:09', '2018-12-07 06:29:09'),
('a9b69406a4e3cd650040684d53e073c9032b3d78356b6f974ab07dedb6f4901a7f2ba031f6aa2494', 48, 1, 'MyApp', '[]', 0, '2017-10-11 05:37:11', '2017-10-11 05:37:11', '2018-10-10 22:37:11'),
('ae272910e4aa206685e4d4e4ad67a298f39ef11cc0f5ad52b32af623095319a180f56c4ce293754d', 21, 1, 'MyApp', '[]', 0, '2017-11-10 00:22:20', '2017-11-10 00:22:20', '2018-11-09 16:22:20'),
('ae88c53bc4c1f736fd9a30906f7415a32ed60bea943c5aeab43b8275a663f5ee127545752390ef95', 48, 1, 'MyApp', '[]', 0, '2017-11-11 03:11:22', '2017-11-11 03:11:22', '2018-11-10 19:11:22'),
('aee62e3f65e603e9266c64121a1cb3f7dd31528046900a197a29cc9a2844fe25a3e76734397f5288', 79, 1, 'MyApp', '[]', 0, '2017-10-12 21:55:59', '2017-10-12 21:55:59', '2018-10-12 14:55:59'),
('af64bb4d3c4fef0e0916970b6e4469d06092adedddca1692c9fe62f95b3e860c07f4dd3c697e9700', 70, 1, 'MyApp', '[]', 0, '2017-10-11 01:09:54', '2017-10-11 01:09:54', '2018-10-10 18:09:54'),
('afbeaa90a1594007a55ba9ea60299fcaf17ba04c392a640c2793236485898f8f56a9695961c83b2d', 34, 1, 'MyApp', '[]', 0, '2017-12-10 01:39:52', '2017-12-10 01:39:52', '2018-12-09 17:39:52'),
('afd6f3ff31b71d48eb311b7bcc3ef893ef33a21cbaaed18c45ff0cdd3d35d9701580ca0c52b3ab65', 48, 1, 'MyApp', '[]', 0, '2017-10-20 23:57:49', '2017-10-20 23:57:49', '2018-10-20 16:57:49'),
('b06905cffe99c1a718dd9e4dfd76a525ffc8b1c2dce1dd5b3daf8253bcbdf15ee3ef7f5caac39a66', 147, 1, 'MyApp', '[]', 0, '2017-11-14 00:12:09', '2017-11-14 00:12:09', '2018-11-13 16:12:09'),
('b0ad7bc7ead9e8395b9e30a1624098c61c34fbeb4b50957fcbe38ad6204b9596b44815df745499e9', 21, 1, 'MyApp', '[]', 0, '2017-11-03 00:31:25', '2017-11-03 00:31:25', '2018-11-02 17:31:25'),
('b2645e111c6c23d5fa484f8df054d3ada46096ae5e0b999c8371b74f08c70772a6d6fbfe2749e9f3', 21, 1, 'MyApp', '[]', 0, '2017-11-11 07:51:56', '2017-11-11 07:51:56', '2018-11-10 23:51:56'),
('b4fed5b96584f439e39181f5cf193c2532b2d94ed5dad34a195700aca976d5ce99a9b12941c11ea3', 21, 1, 'MyApp', '[]', 0, '2017-12-10 02:07:02', '2017-12-10 02:07:02', '2018-12-09 18:07:02'),
('b54e996b3a1fd95e48f29078e969e6e30b5ddf29d482c9ff8432dac77ac9780506b414eb8fa5d538', 21, 1, 'MyApp', '[]', 0, '2017-12-07 04:33:55', '2017-12-07 04:33:55', '2018-12-06 20:33:55'),
('b57db91a0fd0addf34713a88bd0dba6e112dbff3e0e43b28aec331030b01dc561e94db0c35f2de6d', 48, 1, 'MyApp', '[]', 0, '2017-10-26 03:27:14', '2017-10-26 03:27:14', '2018-10-25 20:27:14'),
('b64d215189dce37137f1968f9a992ac5ec25c5aecf1b820631a4b838c9f9fcae51f182f78e40d355', 147, 1, 'MyApp', '[]', 0, '2017-11-11 02:56:42', '2017-11-11 02:56:42', '2018-11-10 18:56:42'),
('b6a22bc5b5b07517fffee864cabe8bd8df150de8cee3ac94255005cafc9b8a8f6c511861056f4d9a', 34, 1, 'MyApp', '[]', 0, '2017-11-09 04:35:31', '2017-11-09 04:35:31', '2018-11-08 20:35:31'),
('b86b4839709d6bda74e8a6765a36232064c90e10bfb8d646c06214d36e69934197a045bd229fb7d0', 18, 1, 'MyApp', '[]', 0, '2017-11-09 02:05:49', '2017-11-09 02:05:49', '2018-11-08 18:05:49'),
('b8aadcb4c7a2cc2db0ea93dba8702c6af42b6b2e71a952d5b7b1a7aad4756cbdbe257d1e60349aa1', 69, 1, 'MyApp', '[]', 0, '2017-12-13 02:00:22', '2017-12-13 02:00:22', '2018-12-12 18:00:22'),
('b9fa9af9ae2a3f8cfa535c0857acb74408e742b4de3cb1f7063b12962e55fa84cdd87947db91612f', 48, 1, 'MyApp', '[]', 0, '2017-12-07 14:32:46', '2017-12-07 14:32:46', '2018-12-07 06:32:46'),
('bb982d39213e1c9d8bfe269b519120332fc903ae472f9d35a215c68a3ca29cf5c81c91840b183349', 48, 1, 'MyApp', '[]', 0, '2017-10-26 02:32:03', '2017-10-26 02:32:03', '2018-10-25 19:32:03'),
('bc1ed1eb2e95e34f871f45830a99f6d0ca2ea1aea6b9a4367435719c8c3f825ababc5b7e656bcc39', 21, 1, 'MyApp', '[]', 0, '2017-12-08 03:37:19', '2017-12-08 03:37:19', '2018-12-07 19:37:19'),
('bc46dd157eca9edf7f7d58365afe4f5494eb84c7b148ca9572d2d330cd022c99d846500d58c36242', 28, 1, 'MyApp', '[]', 0, '2017-10-03 00:02:14', '2017-10-03 00:02:14', '2018-10-02 19:02:14'),
('bfaaa051627e34044b70d45fdf5cf95146a295b2a4eebc4e89bf72020f947d734b1a09ce6a853a6a', 55, 1, 'MyApp', '[]', 0, '2017-11-07 05:26:17', '2017-11-07 05:26:17', '2018-11-06 21:26:17'),
('c465847f499f1751cd7027be41e657772ae98cd6c343d351661f53631a2672f262623f362e1f79a8', 48, 1, 'MyApp', '[]', 0, '2017-11-30 03:55:14', '2017-11-30 03:55:14', '2018-11-29 19:55:14'),
('c4cd5e72ee2b84deeed49446f8c33d81ecfd5f015adb83b6fc65c307e23250042d88cabc04ce63a4', 48, 1, 'MyApp', '[]', 0, '2017-10-17 04:34:58', '2017-10-17 04:34:58', '2018-10-16 21:34:58'),
('c548efffd34577f4303a501f3dcddb0e00a09bfb3e40f403bf8be1082fc2bb689706687dd0c5561f', 21, 1, 'MyApp', '[]', 0, '2017-11-14 03:23:44', '2017-11-14 03:23:44', '2018-11-13 19:23:44'),
('c9578f5c1bcd7f0c55fd693be16810f1dfcb18ed4626209146b027876d1cbd5d3449cf4e732b05d6', 70, 1, 'MyApp', '[]', 0, '2017-10-11 22:58:48', '2017-10-11 22:58:48', '2018-10-11 15:58:48'),
('caed77930b8215382aeb3819ca02b661e58b67cee049c68db57630d46bc9a8f1b2510f2cd6f7ef08', 27, 1, 'MyApp', '[]', 0, '2017-10-03 00:01:38', '2017-10-03 00:01:38', '2018-10-02 19:01:38'),
('cb41b69b41db4999ece23711fe8407f287a3626471ac80bfcbf3189fa9c1615926cf1a72d42787db', 61, 1, 'MyApp', '[]', 0, '2017-10-10 05:16:29', '2017-10-10 05:16:29', '2018-10-09 22:16:29'),
('cf7d5719347bd187aaf34cc87f99504ea1bde2de8d1d888fa32faa1f3b0b3732961e704764833a06', 48, 1, 'MyApp', '[]', 0, '2017-11-14 03:03:38', '2017-11-14 03:03:38', '2018-11-13 19:03:38'),
('d01732951809d0216fdd01af01ba1ba6a5ef39d5379c54a81a7d566885e1ad3e38761c6962af8fb2', 34, 1, 'MyApp', '[]', 0, '2017-11-07 00:53:16', '2017-11-07 00:53:16', '2018-11-06 16:53:16'),
('d059471cd7929dfd11aafcc5ee5b61282d5abfc852befe763377afcc6a2dc4f60a6327d69cdaaac1', 48, 1, 'MyApp', '[]', 0, '2017-11-12 03:13:00', '2017-11-12 03:13:00', '2018-11-11 19:13:00'),
('d0ed167a8c71351b995c0d144ad905c3f5b8860f662914246410bf7652d07752d64b16eb082f7e53', 21, 1, 'MyApp', '[]', 0, '2017-11-15 03:40:27', '2017-11-15 03:40:27', '2018-11-14 19:40:27'),
('d27d219220bcf622ee5269c73f3a997ee1f19ece871084e5c12564fc83bb434dba9841d4f5551e4d', 48, 1, 'MyApp', '[]', 0, '2017-10-13 02:35:50', '2017-10-13 02:35:50', '2018-10-12 19:35:50'),
('d493dfc7d68c346f24933c308c620bf7ffbb28a5a763a79695b0f11a9b261c64cada75ac0d144154', 48, 1, 'MyApp', '[]', 0, '2017-10-26 02:32:45', '2017-10-26 02:32:45', '2018-10-25 19:32:45'),
('d4d5b02fd599e18de38bc82b012cee730c0da58d8a430de3d70324ce6c473d97e035b1cefb04f676', 55, 1, 'MyApp', '[]', 0, '2017-10-17 00:21:13', '2017-10-17 00:21:13', '2018-10-16 17:21:13'),
('d6868bdcc60f290f7e3f3fb49dff5e65b980bbecaa370b448fd534611b538773dc21faf93164a314', 121, 1, 'MyApp', '[]', 0, '2017-10-30 22:53:24', '2017-10-30 22:53:24', '2018-10-30 15:53:24'),
('d7df9bec281aae739e51e802d7542f67738ab3a35ea9451059a24c3ff1ad29fc158870bafa73db95', 48, 1, 'MyApp', '[]', 0, '2017-12-14 03:48:32', '2017-12-14 03:48:32', '2018-12-13 19:48:32'),
('d9adfe796729439df86f3bed4a86e478bcec0dc4b435b02b6b98425c5897e80c4554552c1a7df9e9', 34, 1, 'MyApp', '[]', 0, '2017-11-03 03:07:14', '2017-11-03 03:07:14', '2018-11-02 20:07:14'),
('dae5c0f17f122a5f2179605e0e3d737edcdf80720dcf474d9e599d2b8a266a13e1a4772d726a473f', 48, 1, 'MyApp', '[]', 0, '2017-11-02 00:17:24', '2017-11-02 00:17:24', '2018-11-01 17:17:24'),
('db5ca56f7524134631633fba5963d47441dd4563e2c73e21a6ce4c81e16f310ebf1fd83f3ea9a2ee', 48, 1, 'MyApp', '[]', 0, '2017-10-27 02:09:04', '2017-10-27 02:09:04', '2018-10-26 19:09:04'),
('df046691768b8972cb3590553f2adb960d6954f1cbb77d1e15fcab442ac20cfc11f8de76c412bbfd', 79, 1, 'MyApp', '[]', 0, '2017-10-12 21:58:53', '2017-10-12 21:58:53', '2018-10-12 14:58:53'),
('df7db081b475d52665b78b9ee985401ca4b3e27c3f92da9a7653ebb3eb672cce4755797cd3918c52', 55, 1, 'MyApp', '[]', 0, '2017-10-22 00:10:32', '2017-10-22 00:10:32', '2018-10-21 17:10:32'),
('dfeb06c605ebe44f90d2df95b55c1ac52ff049f610dca86a3e76c989d4159ed8605b1dba6019a3aa', 28, 1, 'MyApp', '[]', 0, '2017-10-03 00:59:08', '2017-10-03 00:59:08', '2018-10-02 19:59:08'),
('dff4a7af044d0284fa9d60682a2b6456ab87af1045ffc769d69d2d834dbb6348f42fa5bd58ce3ce6', 47, 1, 'MyApp', '[]', 0, '2017-10-08 01:10:42', '2017-10-08 01:10:42', '2018-10-07 18:10:42'),
('e035e6ec515ab2d30501074955dd54d4603c26ba12ab2e626a0d8f92f3dddf40c9fdd78b418c2c98', 55, 1, 'MyApp', '[]', 0, '2017-10-17 00:03:59', '2017-10-17 00:03:59', '2018-10-16 17:03:59'),
('e08a5cb662f831404ee5f20622efd0975e691babcdd7afed743e35b55f4cc6a70a71580f4954a5c6', 34, 1, 'MyApp', '[]', 0, '2017-12-13 00:54:30', '2017-12-13 00:54:30', '2018-12-12 16:54:30'),
('e0de43b2b3360db8223c63f0f34949d6697004419a8ad95bb5fc6cb5f86149c61f8ecc88395802b0', 21, 1, 'MyApp', '[]', 0, '2017-11-29 07:46:42', '2017-11-29 07:46:42', '2018-11-28 23:46:42'),
('e1a2495efd56f41fbaf0517c140a7e433f1348dc6670743acbeb95d55fbccfa8b032f0a6fb23cfb5', 55, 1, 'MyApp', '[]', 0, '2017-10-24 21:39:49', '2017-10-24 21:39:49', '2018-10-24 14:39:49'),
('e4408a22e75dfab04b7acec569f0255433c8a5a1a2679058bcbc1b7a67fe814484b3a0e3c34263b3', 21, 1, 'MyApp', '[]', 0, '2017-10-27 01:05:59', '2017-10-27 01:05:59', '2018-10-26 18:05:59'),
('e4cb5ed2db80c26de0a8761e068dae51a2f1c34cc88b708c355e73541731e91c1b262085decc3975', 55, 1, 'MyApp', '[]', 0, '2017-11-02 23:31:28', '2017-11-02 23:31:28', '2018-11-02 16:31:28'),
('e779f0353625ea2e6a4122655060b0f87acfe9be8c323843787df9e217a5e5801603a9c39e01d202', 141, 1, 'MyApp', '[]', 0, '2017-11-08 23:27:33', '2017-11-08 23:27:33', '2018-11-08 15:27:33'),
('e77f3badebae2645dd6adb86f8a2ad1da852a21e75e3bd615809855192cb5a543d780baba6754be4', 21, 1, 'MyApp', '[]', 0, '2017-12-13 23:24:39', '2017-12-13 23:24:39', '2018-12-13 15:24:39'),
('e7976f1cece225d3842086a5a3f9391272211b9ced793f1d88d550bb963f06e717c4ae885392500b', 34, 1, 'MyApp', '[]', 0, '2017-12-09 05:25:50', '2017-12-09 05:25:50', '2018-12-08 21:25:50'),
('e82def6ee331ccc790aff0aa7ebf0a7a8fd271dcf0b9afedc314a3b549c329647f8a8d6cda6c8777', 151, 1, 'MyApp', '[]', 0, '2017-12-13 23:45:52', '2017-12-13 23:45:52', '2018-12-13 15:45:52'),
('ece33268b110335a2ad99ead50e0475c523f8f82101284a023270124b69393ffa9c753f169f8ba19', 21, 1, 'MyApp', '[]', 0, '2017-11-14 04:19:05', '2017-11-14 04:19:05', '2018-11-13 20:19:05'),
('ed03008d34ebb24149e4f095422d058333f39374c61ef4ee241be98de8b19aaa4d0917ebddedca5c', 55, 1, 'MyApp', '[]', 0, '2017-10-26 21:47:34', '2017-10-26 21:47:34', '2018-10-26 14:47:34'),
('ed9608c494240beb8faa235f8eeb4c9d4d9090db70b521333393c2704e621ac8133403af1251bd75', 48, 1, 'MyApp', '[]', 0, '2017-11-03 03:43:49', '2017-11-03 03:43:49', '2018-11-02 20:43:49'),
('ee2f472ef3a2efa2ddc07e477524e6d234c5647e29db5fb195bbb510e0273ca6d403e8912da1c4fa', 42, 1, 'MyApp', '[]', 0, '2017-11-09 01:01:29', '2017-11-09 01:01:29', '2018-11-08 17:01:29'),
('eeb145fb511cd86e52b390e0ae56a1363439930ee083627efd4aeffe2add58463428985a2cd9fedc', 55, 1, 'MyApp', '[]', 0, '2017-10-23 23:49:16', '2017-10-23 23:49:16', '2018-10-23 16:49:16'),
('f034e8fd4f205f5af0248f04cc3bc0617d05753bfd33113c4592a1d7613a55394a6597f211bd7088', 48, 1, 'MyApp', '[]', 0, '2017-11-09 04:19:03', '2017-11-09 04:19:03', '2018-11-08 20:19:03'),
('f0a44820cd2d0a75ffc90e95a36db3401ba77fa579f67a7f110db02fca1f1d438350b13587025c7e', 55, 1, 'MyApp', '[]', 0, '2017-10-17 05:19:18', '2017-10-17 05:19:18', '2018-10-16 22:19:18'),
('f19528bdf0db1d61f6f81ecbf000945b2fb6e7066f50dc4f6ffc42627d71edca3fd3af44d8b24869', 55, 1, 'MyApp', '[]', 0, '2017-10-22 00:01:30', '2017-10-22 00:01:30', '2018-10-21 17:01:30'),
('f207a0154092a99017cd9c7eda6f25aa066f6af4802d99e7a8cca1593d033c470548479a49ae3df0', 85, 1, 'MyApp', '[]', 0, '2017-10-13 03:02:00', '2017-10-13 03:02:00', '2018-10-12 20:02:00'),
('f298a2638f6f4c7f5ad17499ae532d21dd3f6ba3694fc0c4bef3aa1d9956639e2e4fca4ddd4494ef', 160, 1, 'MyApp', '[]', 0, '2017-12-14 01:02:58', '2017-12-14 01:02:58', '2018-12-13 17:02:58'),
('f2d4773f8b610a65efbc8a9197c8b1acb44d214a0cfaeb984a1150a3aa3228b9591dbf538b11e8a0', 48, 1, 'MyApp', '[]', 0, '2017-10-27 00:22:05', '2017-10-27 00:22:05', '2018-10-26 17:22:05'),
('f3a2f9c8fedc7e484cf8b2f4d317098994b399ed4199a32c3996b6822be9ba21c5decf79df1f3c9b', 48, 1, 'MyApp', '[]', 0, '2017-10-22 00:31:36', '2017-10-22 00:31:36', '2018-10-21 17:31:36'),
('f4720f06a41384c3c9be8eae48d0f7e9f272b3a3201e6c81684260a833c7dc0f6976926cf4766b98', 21, 1, 'MyApp', '[]', 0, '2017-12-13 12:37:02', '2017-12-13 12:37:02', '2018-12-13 04:37:02'),
('f55511e88b48079f05ef8d5eb7ba4b2248955fd217760268d22fbbfa0c5ecee8d59b22533f2043f9', 48, 1, 'MyApp', '[]', 0, '2017-12-13 12:16:14', '2017-12-13 12:16:14', '2018-12-13 04:16:14'),
('f55586de8eda318a71050316ba999fb6791f3c2cfb67c6220540eb6c34d39d0e1a7928574c901b29', 21, 1, 'MyApp', '[]', 0, '2017-12-13 12:16:26', '2017-12-13 12:16:26', '2018-12-13 04:16:26'),
('f5b88ba387d266dd4297ed75cbed245872774c546f85a31a24345aca97d480a1964d692b9247baee', 141, 1, 'MyApp', '[]', 0, '2017-11-08 23:27:05', '2017-11-08 23:27:05', '2018-11-08 15:27:05'),
('f6b573af035a6a0bcce599ffba62c832be1fe9f1cf3c8117d7e23e937647085b98eb437e593a5250', 156, 1, 'MyApp', '[]', 0, '2017-12-13 23:59:45', '2017-12-13 23:59:45', '2018-12-13 15:59:45'),
('f6b9e865ce0546123d6819d2cf39d5a1e3517cf85d84df77584ed9812d9c8df8d1b18609a907fe96', 46, 1, 'MyApp', '[]', 0, '2017-10-08 01:10:00', '2017-10-08 01:10:00', '2018-10-07 18:10:00'),
('f6f510db7a87f941ac64a9ffe062bb9da923939de8dc2cead5a496b86953cb2bf05140d76cbe84ef', 48, 1, 'MyApp', '[]', 0, '2017-12-13 01:34:32', '2017-12-13 01:34:32', '2018-12-12 17:34:32'),
('f885f19cfcf7f1e7e68a81dfa9d22115828d330278d9831d44d224a20f0ab84ccc316efd3a4fd366', 48, 1, 'MyApp', '[]', 0, '2017-11-14 01:33:23', '2017-11-14 01:33:23', '2018-11-13 17:33:23'),
('f8d314d7031196a5669cbd10928be681e54b7849a2b2832269d7c48ce1a77b0fa2498c6a5e6c1dca', 21, 1, 'MyApp', '[]', 0, '2017-11-18 00:22:15', '2017-11-18 00:22:15', '2018-11-17 16:22:15'),
('fab9a360409586ac261728f5add7f620a252f92c18e26fce63247fe13a2ea702854a7b75d40425ce', 48, 1, 'MyApp', '[]', 0, '2017-11-28 02:54:49', '2017-11-28 02:54:49', '2018-11-27 18:54:49'),
('fb1ab8ef0b5324964553e53db5b1adfd34e3e18679bde22b55e2be9569796b48eed2ca9332d0c680', 34, 1, 'MyApp', '[]', 0, '2017-12-14 23:28:40', '2017-12-14 23:28:40', '2018-12-14 15:28:40'),
('fece0e6ef55efc5be504c09430dfc60e19ab89beea61ec1840286dec428390f89eca26162086876e', 48, 1, 'MyApp', '[]', 0, '2017-10-11 00:45:54', '2017-10-11 00:45:54', '2018-10-10 17:45:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'yHDA8sezyxIT30wCOZpUCYLCV9kZ7IDpudIXwDlD', 'http://localhost', 1, 0, 0, '2017-09-26 03:14:55', '2017-09-26 03:14:55'),
(2, NULL, 'Laravel Password Grant Client', 'eW3lc29UUpKd04Ij3WSJCqM14V02NaxiHJCggDW0', 'http://localhost', 0, 1, 0, '2017-09-26 03:14:55', '2017-09-26 03:14:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-09-26 03:14:55', '2017-09-26 03:14:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `idPais` int(11) NOT NULL,
  `nombrePais` varchar(45) DEFAULT NULL,
  `idioma` varchar(45) DEFAULT NULL,
  `lada` varchar(5) DEFAULT NULL,
  `abrev` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`idPais`, `nombrePais`, `idioma`, `lada`, `abrev`) VALUES
(1, 'México', 'Español', '+521', 'MEX');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo`
--

CREATE TABLE `periodo` (
  `idPeriodo` int(11) NOT NULL,
  `idNivel` int(11) NOT NULL,
  `nombrePeriodo` varchar(30) NOT NULL,
  `idInstitucion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodo`
--

INSERT INTO `periodo` (`idPeriodo`, `idNivel`, `nombrePeriodo`, `idInstitucion`) VALUES
(1, 2, 'PRIMER PARCIAL', 1),
(2, 2, 'SEGUNDO PARCIAL', 1),
(3, 2, 'TERCER PARCIAL', 1),
(4, 2, 'CUARTO PARCIAL', 1),
(5, 2, 'QUINTO PARCIAL', 1),
(6, 3, '1', 1),
(7, 3, '2', 1),
(8, 3, '3', 1),
(9, 3, '4', 1),
(10, 3, '5', 1),
(11, 4, '1', 1),
(12, 4, '2', 1),
(13, 4, '3', 1),
(14, 1, '1', 8),
(15, 1, '2', 8),
(16, 1, '3', 8),
(17, 1, '4', 8),
(18, 1, '5', 8),
(19, 2, '1', 8),
(20, 2, '2', 8),
(21, 2, '3', 8),
(22, 2, '4', 8),
(23, 2, '5', 8),
(24, 3, '1', 8),
(25, 3, '2', 8),
(26, 3, '3', 8),
(27, 3, '4', 8),
(28, 3, '5', 8),
(29, 4, '1', 8),
(30, 4, '2', 8),
(31, 4, '3', 8),
(32, 3, '1', 10),
(33, 3, '2', 10),
(34, 3, '3', 10),
(35, 3, '4', 10),
(36, 3, '5', 10),
(37, 2, '1', 11),
(38, 2, '2', 11),
(39, 2, '3', 11),
(40, 2, '4', 11),
(41, 2, '5', 11),
(42, 1, '1', 10),
(43, 1, '2', 10),
(44, 1, '3', 10),
(45, 1, '4', 10),
(46, 1, '5', 10),
(47, 1, '1', 13),
(48, 1, '2', 13),
(49, 2, '1', 15),
(50, 2, '2', 15),
(51, 2, '3', 15),
(52, 2, '4', 15),
(53, 2, '5', 15),
(54, 2, '1', 13),
(55, 2, '2', 13),
(56, 2, '3', 13),
(57, 2, '4', 13),
(58, 2, '5', 13),
(59, 3, '2017-2018', 16),
(60, 2, '1', 17),
(61, 2, '2', 17),
(62, 2, '3', 17),
(63, 2, '4', 17),
(64, 2, '5', 17),
(65, 2, '6', 17),
(66, 4, 'PRIMERO', 18),
(67, 4, 'SEGUNDO', 18),
(68, 4, 'CUARTO', 18),
(69, 4, 'QUINTO', 18),
(70, 2, 'Final', 1),
(71, 2, 'primer periodo', 23),
(72, 2, 'segundo periodo', 23),
(73, 2, 'tercer periodo', 23),
(74, 2, 'cuarto periodo', 23),
(75, 2, 'quinto periodo', 23),
(76, 2, 'PRIMER PARCIAL', 24),
(77, 2, 'SEGUNDO PARCIAL', 24),
(78, 2, 'TERCER PARCIAL', 24),
(79, 3, 'PRIMER BIMESTRE', 25),
(80, 3, 'SEGUNDO BIMESTRE', 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idprofesor` int(11) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `id_institucion` int(11) DEFAULT NULL,
  `nombreprof` varchar(75) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `teloficina` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT '1',
  `tokenFCM` varchar(160) DEFAULT '0',
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`idprofesor`, `correo`, `id_institucion`, `nombreprof`, `apepat`, `apemat`, `genero`, `fechanac`, `telefono`, `celular`, `teloficina`, `f_alta`, `f_baja`, `f_mod`, `activo`, `tokenFCM`, `iduser`) VALUES
(1, 'miguel@gmail.com', 1, 'Miguel ', 'Torres', 'Reyna', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 18),
(2, 'jorge@gmail.com', 1, 'Karla', 'Martinez', 'Martinez', 'F', '1997-04-18', '676767674764746', '56587446464', NULL, NULL, NULL, NULL, NULL, 'eVURRowSfxA:APA91bHtFsBZSBfteR_NvmgaIdOFr8ULuew-jR79tkZ-txJS9vOs_kJaHpz-WTpEoDUzH4w50wK-WAoS1gaWmrxFC3Q1gfphAX6a5VGJIOOvQiVOLLBxfPRkuHbuZvO35vvhxNpUoouX', 21),
(3, 'victor@gmail.com', 3, 'Victor', 'Ruiz', 'Mena', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 23),
(6, 'gerardo@gmail.com', 1, 'Gerardo', 'Rodriguez', 'Pérez', 'M', '1985-07-18', '8181818', '4444464748', NULL, '2017-10-05', NULL, NULL, NULL, '0', 27),
(9, 'mireles@gmail.com', 8, 'Victor', 'Mireles', 'Fernandez', 'M', '1970-08-16', '345677', '5678654', NULL, '2017-10-09', NULL, NULL, '1', '0', 55),
(10, 'cmartinez@gmail.com', 8, 'Carlos', 'Martinez', 'Ruiz', 'M', '1980-12-24', '5645347', '467589', NULL, '2017-10-09', NULL, NULL, '1', '0', 58),
(11, 'gabo@gmail.com', 8, 'Gabriel', 'Mendoza', 'Mendoza', 'M', '1975-06-04', '56897865', '75668776', NULL, '2017-10-09', NULL, NULL, '1', '0', 59),
(12, 'juanmhh@gmail.com', 10, 'Juan', 'Huerta', 'Hernandez', 'M', '1992-08-05', '014441556292', '4441556292', NULL, '2017-10-10', NULL, NULL, '1', 'cL5NHK9NjJs:APA91bFgC4VtqeJjo6T6_7kuo6FdbZjYGuaPopmX54SLHfQUDdwhfClKwiQ2AfB4yThbAGiMoz8yZ47vIP54x9kcUzUDsUTWsZat-O2en5YpfyKrS0IVNRmEgfDGRM__wpIfj0eQU3k3', 69),
(13, 'ciel@gmail.com', 11, 'Ciel', 'ciel', 'ciel', 'M', '2017-10-10', '13434134', '1342313', NULL, '2017-10-10', NULL, NULL, '1', '0', 73),
(14, 'alejandro.arrez@gmail.com', 13, 'Alex', 'Arrez', 'Méndez', 'M', '1974-12-06', '0987654321', '0987654321', NULL, '2017-10-19', NULL, NULL, '1', '0', 90),
(15, 'jrobles@motolinea.com', 17, 'Jean', 'Robles', 'L.', 'M', '2006-06-13', '32423423424', '24524524', NULL, '2017-10-23', NULL, NULL, '1', '0', 95),
(16, 'dulce@motolinea.com', 17, 'Dulce', 'P', 'R', 'F', '2017-10-23', '3242432424', '324234234', NULL, '2017-10-23', NULL, NULL, '1', '0', 96),
(17, 'luisa@gmail.com', 18, 'Luisa', 'Torres', 'M', 'F', '1985-01-08', '13415123515', '16545134141', NULL, '2017-10-24', NULL, NULL, '1', '0', 101),
(18, 'federico@motolinea.com', 17, 'Federico', 'Silva', 'Herzog', 'M', '2011-01-01', '789365478', '4786970', NULL, '2017-10-24', NULL, NULL, '1', '0', 110),
(19, 'jesusr@gmail.com', 17, 'Jesus', 'Robledo', 'López', NULL, '1999-07-15', '4444546137', NULL, NULL, NULL, NULL, NULL, '1', '0', 128),
(20, 'edgar@motolinea.com', 1, 'Edgar', 'Mendoza', 'Mena', 'M', '2017-10-31', '5678654', '5678654', NULL, '2017-10-31', NULL, NULL, '1', '0', 131),
(21, 'jeanalbert@gmail.com', 1, 'Jean', 'Robles', 'Bedolla', NULL, '1999-07-15', '4444', NULL, NULL, NULL, NULL, NULL, '1', '0', 139),
(22, 'miguelito@gmail.com', 1, 'Ruiz', 'Miguelito', 'Chavez', NULL, '1983-07-15', '54645797887', NULL, NULL, NULL, NULL, NULL, '1', '0', 141),
(23, 'daniela@appycollege.com', 24, 'Daniela', 'Appy', 'College', 'F', '1992-06-15', '4444345433', '4445432232', NULL, '2017-11-09', NULL, NULL, '1', '0', 145),
(24, 'alex@appycollege.com', 24, 'Alex', 'Appy', 'College', 'M', '1990-05-16', '4442454323', '6668789879', NULL, '2017-11-09', NULL, NULL, '1', '0', 146),
(25, 'dulce01@gmail.com', 25, 'Dulce', 'Medina', 'Castillo', 'F', '1995-06-12', '65432345', '134325434', NULL, '2017-11-10', NULL, NULL, '1', '0', 149);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_alumno_tarea`
--

CREATE TABLE `r_alumno_tarea` (
  `idAlumno` int(11) NOT NULL,
  `idTarea` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `r_alumno_tarea`
--

INSERT INTO `r_alumno_tarea` (`idAlumno`, `idTarea`, `status`) VALUES
(3, 2, 1),
(3, 5, 0),
(3, 9, 0),
(3, 10, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subgrupo`
--

CREATE TABLE `subgrupo` (
  `idSubgrupo` int(11) NOT NULL,
  `idGrupo` int(11) NOT NULL,
  `idMateria` int(11) NOT NULL,
  `idProfesor` int(11) NOT NULL,
  `idCiclo` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subgrupo`
--

INSERT INTO `subgrupo` (`idSubgrupo`, `idGrupo`, `idMateria`, `idProfesor`, `idCiclo`, `estatus`) VALUES
(1, 1, 1, 2, 3, 1),
(2, 1, 3, 1, 3, 1),
(3, 29, 3, 1, 3, 1),
(4, 29, 1, 2, 3, 1),
(7, 30, 1, 2, 3, 1),
(8, 30, 3, 1, 3, 1),
(10, 28, 1, 2, 4, 1),
(11, 28, 3, 1, 4, 1),
(12, 94, 4, 9, 9, 1),
(13, 94, 5, 10, 9, 1),
(14, 94, 6, 11, 9, 1),
(15, 118, 9, 12, 10, 1),
(16, 130, 10, 13, 11, 1),
(17, 154, 11, 12, 12, 1),
(18, 154, 11, 12, 12, 1),
(19, 166, 12, 14, 13, 1),
(20, 2, 2, 1, 1, 1),
(21, 3, 2, 1, 1, 1),
(22, 202, 18, 15, 16, 1),
(23, 202, 19, 15, 16, 1),
(24, 202, 20, 15, 16, 1),
(25, 202, 21, 15, 16, 1),
(26, 205, 18, 16, 16, 1),
(27, 205, 19, 16, 16, 1),
(28, 206, 22, 17, 17, 1),
(29, 1, 2, 1, 1, 1),
(30, 1, 24, 6, 1, 1),
(31, 1, 23, 1, 1, 1),
(32, 1, 25, 1, 1, 1),
(33, 1, 2, 21, 1, 1),
(34, 1, 23, 1, 1, 1),
(35, 1, 25, 1, 1, 1),
(36, 1, 2, 21, 1, 1),
(37, 1, 24, 6, 1, 1),
(38, 1, 25, 1, 1, 1),
(39, 216, 34, 23, 19, 1),
(40, 217, 34, 23, 19, 1),
(41, 217, 34, 23, 19, 1),
(42, 222, 40, 25, 20, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `idTareas` int(11) NOT NULL,
  `idSubgrupo` int(11) DEFAULT NULL,
  `fechaentrega` date DEFAULT NULL,
  `titulo` varchar(60) DEFAULT NULL,
  `descripcion` varchar(600) DEFAULT NULL,
  `anexo` varchar(100) DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`idTareas`, `idSubgrupo`, `fechaentrega`, `titulo`, `descripcion`, `anexo`, `condicion`) VALUES
(1, 1, '2017-11-07', 'Tarea 1', 'Hagan la tarea', NULL, 0),
(5, 1, '2017-10-13', 'Primer tarea', 'haganla', 'Anteproyecto Mario Eduardo Mejía Concha 120214 ITI.pdf', 1),
(6, 12, '2017-10-13', 'La tareas funcionan', 'Si que siiiii', 'AnteproyectoJ.pdf', 1),
(8, 15, '2017-10-19', 'asdasd', 'asdasdasd', NULL, 0),
(9, 1, '2017-11-07', 'Tarea de matemáticas', 'Calcular derivadas', NULL, 1),
(11, 26, '2017-10-26', 'dsfasfsd', 'jnjoijoi', 'Entrega Final TDE.pdf', 1),
(14, 9, '2017-10-27', 'hola', 'Hacer una Investigacion', NULL, 1),
(15, 12, '2017-10-30', 'test', 'android', NULL, 1),
(16, 12, '2017-10-31', 'bdjdjf', 'ndndndnfn', NULL, 1),
(17, 1, '2017-10-31', 'TAREA DE PRUEBA', 'ESTA ES UNA TAREA DE PRUEBA', NULL, 1),
(19, 1, '2017-11-02', 'NAVIDAD', 'Tarea de la revolución Mexicana, se pide un material especial para la navidad.', 'ICR TELMEX ENERO.pdf', 1),
(20, 1, '2017-11-06', 'Revolucion Española', 'Hacer un ensayo de 300 palabras sobre los acontecimientos de la Revolución Española', NULL, 1),
(30, 1, '2017-11-08', 'Recortes', 'Traer una revista o periodico que puedan recortar para realizar la actividad de clase.', NULL, 1),
(32, 1, '2017-11-09', 'Investigación', 'Investigar sobre los 4 estados de la materia, pueden ser recortes o puro texto.', NULL, 1),
(43, 12, '2017-11-10', 'a', 'a', '201711101510358285.jpg', 1),
(44, 12, '2017-11-10', 'a', 'a', '201711111510358820.jpg', 1),
(45, 1, '2017-12-20', 'Dia de San Nicolás', 'Redactar la biografía de San Nicolás en papel maché.', NULL, 1),
(46, 7, '2017-12-09', 'prueba tarea', 'bla bla bla', '201712081512767861.jpg', 1),
(47, 7, '2017-12-09', 'prueba tarea', 'bla bla bla', '201712081512767861.jpg', 1),
(48, 1, '2017-12-09', 'bla bla bla', 'bla bla bla', '201712081512767919.jpg', 1),
(49, 1, '2017-12-09', 'bla bla bla', 'bla bla bla', '201712081512767920.jpg', 1),
(50, 1, '2017-12-08', 'ggg', 'hgg', NULL, 1),
(51, 1, '2017-12-14', 'Biografía de Santa', 'se pide una biografía completa con imágenes', NULL, 1),
(52, 1, '2017-12-09', 'Tarea', 'Yaces una Investigación de Clint Eastwood', NULL, 1),
(53, 1, '2017-12-12', 'Informe de Santa Claus', 'quien es santa claus? \n\n150 palabras', NULL, 1),
(54, 1, '2017-12-12', 'ahahah', 'zvzvs', NULL, 1),
(55, 1, '2017-12-14', 'trabajo sobre navidad', 'Desarrollar un resumen de la navidad', NULL, 1),
(56, 1, '2017-12-14', 'aminoacidos', 'dibujar los enlaces carbónicos de los aminoácidos', '201712131513139948.jpg', 1),
(57, 1, '2017-12-14', 'aminoacidos', 'dibujar los enlaces carbónicos de los aminoácidos', '201712131513139948.jpg', 1),
(58, 1, '2017-12-13', 'prueba', 'prueba', NULL, 1),
(59, 1, '2017-12-13', 'prueba', 'prueba', NULL, 1),
(60, 1, '2017-12-13', 'prueba1', 'prueba1', '201712131513140107.jpg', 1),
(61, 1, '2017-12-13', 'prueba1', 'prueba1', '201712131513140108.jpg', 1),
(62, 1, '2017-12-13', 'kfkf', 'jxjxnc', NULL, 1),
(63, 1, '2017-12-13', 'El Mouse', '¿Que es el Mouse?', '201712131513178850.jpg', 1),
(64, 1, '2017-12-13', 'svev', 'rjrbrv', '201712131513180053.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas_vistas`
--

CREATE TABLE `tareas_vistas` (
  `idTareasVistas` int(11) NOT NULL,
  `idTarea` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `visto` int(1) NOT NULL COMMENT '0=no, 1=si'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tareas_vistas`
--

INSERT INTO `tareas_vistas` (`idTareasVistas`, `idTarea`, `idUsuario`, `visto`) VALUES
(1, 45, 48, 1),
(2, 32, 48, 1),
(3, 30, 48, 1),
(4, 9, 48, 1),
(5, 20, 48, 1),
(6, 19, 48, 1),
(7, 48, 34, 1),
(8, 48, 48, 1),
(9, 49, 48, 1),
(10, 50, 48, 1),
(11, 52, 34, 1),
(12, 51, 48, 1),
(13, 52, 48, 1),
(14, 53, 48, 1),
(15, 54, 48, 1),
(16, 55, 48, 1),
(17, 59, 48, 1),
(18, 58, 48, 1),
(19, 61, 48, 1),
(20, 63, 48, 1),
(21, 64, 34, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoAlerta`
--

CREATE TABLE `TipoAlerta` (
  `id` int(11) NOT NULL,
  `Tipo` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `TipoAlerta`
--

INSERT INTO `TipoAlerta` (`id`, `Tipo`) VALUES
(1, 'Sismo'),
(2, 'Incendio'),
(3, 'Simulacro'),
(4, 'Violencia Cercana'),
(5, 'Robo'),
(6, 'Otro'),
(7, 'Aseguar Salon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposinsignia`
--

CREATE TABLE `tiposinsignia` (
  `idTipoInsignia` int(11) NOT NULL,
  `NombreInsignia` varchar(150) NOT NULL,
  `rutaImagen` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiposinsignia`
--

INSERT INTO `tiposinsignia` (`idTipoInsignia`, `NombreInsignia`, `rutaImagen`) VALUES
(1, 'Tu Tarea Supera la Gravedad', 'INSIGNIASF_Mesa_de_trabajo_1.png'),
(2, 'Deslumbrante', 'INSIGNIASF-02.png'),
(3, 'Felicidadezzz', 'INSIGNIASF-03.png'),
(4, 'La Rapidéz es tu Segundo Nombre', 'INSIGNIASF-04.png'),
(5, '¡Feliz Navidad!', 'INSIGNIASF-05.png'),
(6, 'Ho-Ho-Ho ¡Feliz Navidad!', 'INSIGNIASF-06.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_users`
--

CREATE TABLE `tipo_users` (
  `idtipo_users` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE `tutor` (
  `idtutor` int(11) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `nombreTutor` varchar(75) DEFAULT NULL,
  `apepat` varchar(45) DEFAULT NULL,
  `apemat` varchar(45) DEFAULT NULL,
  `genero` char(2) DEFAULT NULL,
  `fechanac` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `teloficina` varchar(45) DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_mod` date DEFAULT NULL,
  `activo` char(2) DEFAULT '1',
  `tokenFCM` varchar(160) DEFAULT '0',
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tutor`
--

INSERT INTO `tutor` (`idtutor`, `correo`, `nombreTutor`, `apepat`, `apemat`, `genero`, `fechanac`, `telefono`, `celular`, `teloficina`, `f_alta`, `f_baja`, `f_mod`, `activo`, `tokenFCM`, `iduser`) VALUES
(1, 'josue@gmail.com', 'Josue', 'Gimenez', 'Castro', 'M', '1970-04-17', '8918237', '4441090105', NULL, '2017-10-06', NULL, NULL, '1', 'd78Jer4PnDU:APA91bHb8gDkIQVpH4tSuy6rc-pR0l4W8TPoMKE03Ho42alP9ldWIExKNBGDT6-TqIQKl0t9iRTh1tFsMgkmq4mbf0znmq5k2RYzYrhNy_CmPln-WOgTR0renYNHNbJ_c_h2V5InlVNt', 34),
(3, 'juanmartin.h.h@gmail.com', 'Juan Martin', 'Huerta', 'Hernandez', NULL, '1992-08-05', '4441175199', NULL, NULL, NULL, NULL, NULL, '1', 'fDc7sV4jyIA:APA91bEaFbzN9ee7ZVYtOB4RMagpe5xho-eXxkAfRJqwwkV3ibnUsbjLDFzcylE7MTp4NYL1arqBSG0P451pCeomNwDu9EZDXZ-MsWBP3oLEkz7QjEwAJb_xU0Wc5G3TEJU3i5Tx_fYW', 48),
(4, 'david@gmail.com', 'David', 'Mejia', 'Mendez', 'M', '1962-02-28', '678765', '765678', NULL, '2017-10-09', NULL, NULL, '1', '0', 56),
(5, 'jm@gmail.com', 'Juan', 'Huerta', 'Hernandez', NULL, '1992-08-05', '4', NULL, NULL, NULL, NULL, NULL, '1', '0', 61),
(6, 'aaron@gmail.com', 'Aaron', 'Huerta', 'Hernandez', 'M', '1995-07-21', '444444444', '4444444', NULL, '2017-10-10', NULL, NULL, '1', '0', 70),
(7, 'powerq@gmail.com', 'power', 'ade', 'ion', 'M', '1970-03-23', '1342453', '342524524', NULL, '2017-10-10', NULL, NULL, '1', '0', 74),
(8, 'jmelgarejo@a.com', 'juan', 'melgarejo', 'murrieta', NULL, '1992-08-12', '4442859341', NULL, NULL, NULL, NULL, NULL, '1', '0', 79),
(9, 'martin_huerta@gmail.com', 'Hernández', 'Martin', 'Huerta', NULL, '1992-08-05', '4441556292', NULL, NULL, NULL, NULL, NULL, '1', '0', 81),
(10, 'jj@gmail.com', 'juan', 'melgareji', 'murrieta', NULL, '1972-09-13', '4442859341', NULL, NULL, NULL, NULL, NULL, '1', 'eCrvHsUzR5c:APA91bG7hjZRwwBMvIGHmIimwbDCNtlCdQF5YCxQlZYv-nm93V0aZK-f4bsQPclMth-X_BpjNt7H3yzXD-oG6wY2AnZ87-Ns0ux3w-iXVfZprjX7fG4hwJQkjN97FDqhaQ9ZDD8m311r', 85),
(11, 'jmelgarejo@okventa.com', 'Juan', 'Mel', 'Gibson', 'M', '1972-09-13', '1234567890', '1234567890', NULL, '2017-10-19', NULL, NULL, '1', '0', 89),
(12, 'luis@motolinea.com', 'Luis', 'Perez', 'Perez', 'M', '2017-10-06', '67576', '67565675', NULL, '2017-10-23', NULL, NULL, '1', '0', 97),
(13, 'jmelgarejo@gmail.com', '|Juanjo', 'Mel', 'Mur', 'M', '1972-09-13', '1434134143', '1234567890', NULL, '2017-10-24', NULL, NULL, '1', '0', 100),
(14, 'tino@gmail.com', 'Tino', 'Becerra', 'Tejeda', 'M', '1985-01-08', '0987654321', '1234567890', NULL, '2017-10-24', NULL, NULL, '1', '0', 102),
(25, 'pao@gmail.com', 'paola', 'huerta', 'huerta', NULL, '1999-07-15', '4444', NULL, NULL, NULL, NULL, NULL, '1', '0', 126),
(26, 'marisol@gmail.com', 'Marisol', 'Hernández', 'Hernández', NULL, '1999-07-15', '4441561878', NULL, NULL, NULL, NULL, NULL, '1', '0', 127),
(27, 'jmhh@gmail.com', 'Juan Martin', 'Huerta', 'Hernández', NULL, '1999-07-15', '4444175199', NULL, NULL, NULL, NULL, NULL, '1', 'cL5NHK9NjJs:APA91bFgC4VtqeJjo6T6_7kuo6FdbZjYGuaPopmX54SLHfQUDdwhfClKwiQ2AfB4yThbAGiMoz8yZ47vIP54x9kcUzUDsUTWsZat-O2en5YpfyKrS0IVNRmEgfDGRM__wpIfj0eQU3k3', 129),
(28, 'alex@gmail.com', 'Alejandro', 'Arrez', 'Méndez', NULL, '1973-12-06', '4442317929', NULL, NULL, NULL, NULL, NULL, '1', '0', 132),
(29, 'dulcemedina@gmail.com', 'Dulce', 'Medina', 'Castillo', 'F', '1993-06-21', '444333444', '444333444', NULL, '2017-12-13', NULL, NULL, '1', 'eCrvHsUzR5c:APA91bG7hjZRwwBMvIGHmIimwbDCNtlCdQF5YCxQlZYv-nm93V0aZK-f4bsQPclMth-X_BpjNt7H3yzXD-oG6wY2AnZ87-Ns0ux3w-iXVfZprjX7fG4hwJQkjN97FDqhaQ9ZDD8m311r', 150),
(30, 'devjeanr@gmail.com', 'JEAN ALBERT', 'ROBLES', 'BEDOLLA', NULL, '1993-12-03', '4444238763', NULL, NULL, NULL, NULL, NULL, '1', 'eVURRowSfxA:APA91bHtFsBZSBfteR_NvmgaIdOFr8ULuew-jR79tkZ-txJS9vOs_kJaHpz-WTpEoDUzH4w50wK-WAoS1gaWmrxFC3Q1gfphAX6a5VGJIOOvQiVOLLBxfPRkuHbuZvO35vvhxNpUoouX', 151),
(31, 'marco.reyna.cronos@gmail.com', 'Marco Antonio', 'reyna', 'gallegos', 'M', '2017-12-13', '+524444259139', '4444259139', NULL, '2017-12-13', NULL, NULL, '1', '0', 154),
(32, 'dianatr@gmail.com', 'DIANA', 'TORRES', 'MEZA', NULL, '1997-05-15', '54548799757', NULL, NULL, NULL, NULL, NULL, '1', 'd5c0kC71ZFE:APA91bHPfafNviEuAt2RWEGsCnMZ2bZBaSH2bI5dUPISr4dS95klhhhOAJDyxwpjGKGZJKQ8WlpGjaXr7QzBCcy63Sad3j_umRuRTqeKWRc_aHbshPWADUGiyFvTxBdlDL45ZAl3J-yh', 156),
(33, 'marco_reyna@gmail.com', 'MARCO', 'REINA', 'GALLEGOS', NULL, '1999-07-15', '4441576112', NULL, NULL, NULL, NULL, NULL, '1', 'euj_Ogi4h7s:APA91bGTRDdC8CiIz5vDjKGx0DTq5gnUiy-_NkUDleE3JxuIV4yTjmWOYvEAYL9YAmwpdX8I8fqhbWshcxWR7ECttPZhm-Hk2H9nIxcXb8pzFzWtE8r4S2VrvrGj_LKwH1x0qwNES4oT', 160);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutoralumno`
--

CREATE TABLE `tutoralumno` (
  `idtutor` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tutoralumno`
--

INSERT INTO `tutoralumno` (`idtutor`, `idalumno`) VALUES
(1, 1),
(1, 14),
(3, 3),
(3, 7),
(4, 4),
(4, 5),
(5, 6),
(7, 8),
(8, 9),
(9, 10),
(10, 11),
(12, 12),
(14, 13),
(15, 15),
(27, 16),
(28, 17),
(29, 39),
(30, 40),
(31, 41),
(32, 42),
(33, 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor_institucion`
--

CREATE TABLE `tutor_institucion` (
  `idTutor` int(11) NOT NULL,
  `idInstitucion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_user` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `imagen` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'profile_small.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `tipo_user`, `remember_token`, `created_at`, `updated_at`, `imagen`) VALUES
(1, 'Mario Eduardo', 'mario@gmail.com', '$2y$10$SC00a5X6FclMW/JuFNA67enQm3mXIU1B2aWtEzuD.TBj/G98KQ4T.', 1, '1RPsxPALyuApZU1YGwpqXZKwJZa0K55uBBzDSEdW6eDpliod72yZEL3KN1OO', '2017-09-06 23:26:14', '2017-11-09 01:44:36', '2017110815101630761.jpg'),
(2, 'Juan', 'juan@gmail.com', '$2y$10$DHoWzV/Fe2IT62iIGHgg7O3TS3Eqb6.SK0CpEmLsGFK8TQ3t/gysS', 5, 'uj5b0PCwLszmdM68ixsHqIi8n08s2L4oDaHvmpNEcRLSzNCnIfMz8IwPKbcc', '2017-09-07 02:42:42', '2017-09-07 02:42:42', 'profile_small.jpg'),
(3, 'Luis', 'luis@gmail.com', '$2y$10$OEnsKoTPY6xwXlKK3UgDVePX01eiP.w8pWsHyX0D3gmmbEQ1YGCwK', 2, 'V24TsMPsxDVHMpEr1N8CWJkCIGGVIfCdTV7FMBOebLdA2xUSrW8pvj7mJE2u', '2017-09-07 20:54:17', '2017-09-07 20:54:17', 'profile_small.jpg'),
(4, 'Mayra', 'mayra@gmail.com', '$2y$10$uhIDvyDufb94AhYYXOmZ/e4GVWfC9re0vKdmnEENXa49/LzCuVQ4q', 3, 'Cx9mkSoGErqafvuGhSl1NR3IU036dkgi6tC43N7XcEIp5KpYcNT3Haxg1h1Q', '2017-09-07 20:57:22', '2017-09-07 20:57:22', 'profile_small.jpg'),
(5, 'Pepe', 'pepe@gmail.com', '$2y$10$wJ8b2482TKmGNb9s9fbqHujko/fkRgEKkX2K13IArkTDRX3j0zdGe', 4, 'eWY5VwpjM6VxJ0oDPVKHVib7if9VqvgypqfhnJqsFVZ4TLLKVovBghAWxH8y', '2017-09-08 19:22:49', '2017-09-08 19:22:49', 'profile_small.jpg'),
(7, 'Pedro Perez', 'pedroperez@gmail.com', '$2y$10$N/LslkFWotspc22etfxi3OwF6dwkvcFhXfsSCKvwdsLuKgAsyd3DW', 1, 'tnwjKTfQhdQ3T3eGc1iNNgVPKuYBdgNWUH2cVaHum2dEylskU4Y2HH4kcYnC', '2017-09-26 20:13:23', '2017-09-26 20:13:23', 'profile_small.jpg'),
(9, 'Jose Jose', 'jose@gmail.com', '$2y$10$iTZir6IDmdczCVEoXc9tpeeLTRNvpyLvha1hBaOkYIvS/nY9Pe4ou', 1, 'WkI8W15c85y2q1oOOqYGgvyrT2aIrYG9yzONLEK6xLf6lfDpXcA6D9EdhBDo', '2017-09-26 20:42:35', '2017-09-26 20:42:35', 'profile_small.jpg'),
(10, 'Luis Alberto Mendez', 'alberto@gmail.com', '$2y$10$hveQMlnjloAXYNY0Zy/46u5vFufFWzF/K/NZHmZkJj3F0nzFNItFC', 1, '4LmHHKwd9bDEG9bUHVFoq2TzKuoB9eWu3CJfFxp2wF9kxBKepJXp0WF6EX9z', '2017-09-26 21:12:44', '2017-09-26 21:12:44', 'profile_small.jpg'),
(18, 'Miguel Flores', 'miguel@gmail.com', '$2y$10$rnsM1CYGo96K6fmFOSTvx.F3z5JXfjMs75Opt33aUvrqVMbg2X4Tu', 3, 'R52May4KNNAx4Ox3wzO2gtRyENS37gDeaykLQb6YPZqf1w1WgPbn344WzExS', '2017-09-27 00:48:31', '2017-09-27 00:48:31', 'profile_small.jpg'),
(19, 'lala', 'lala@gmail.com', '$2y$10$xzF5cAa2z0bUEVHVUMB3we/bCV3DEkkAQIaE/oQxEmVJ0K6glmNWy', 1, NULL, '2017-09-28 00:14:56', '2017-09-28 00:14:56', 'profile_small.jpg'),
(20, 'lalo', 'lalo@gmail.com', '$2y$10$2LA0mSeDQsShpnCQYQ310eMfG44u77cSz2XeN5G8ciLDN1.aj/VN6', 1, NULL, '2017-09-28 00:16:37', '2017-09-28 00:16:37', 'profile_small.jpg'),
(21, 'Karla', 'jorge@gmail.com', '$2y$10$/XtIo0pwXPhf26phzIofteXm/hVF95iS0BchimWjNj7NzD.CaX76W', 3, 'wzmuZhu7iNuIpo24tM6RjqpP4Xn6VPiTZBjsEYlSjyCAuJQif9PDiidpQi2T', '2017-09-28 22:55:30', '2017-11-09 01:09:25', '20171108151016096521.jpg'),
(23, 'Victor Ruiz', 'victor@gmail.com', '$2y$10$V1thaPVUjH5r6C/fAm.dG.141VKNJ9MflRelq9Yi2cqUBeJ7Zzk8y', 3, 'gLkTgPWu8iCry8DIG1ShFZcO7QgGSIToZIXZGuImJCdDTjd6RObU6eMp9zvK', '2017-09-28 22:56:54', '2017-09-28 22:56:54', 'profile_small.jpg'),
(27, 'Gerardo', 'gerardo@gmail.com', '$2y$10$vIQ3ZdH7BGLYV/Y5t2dg2eLxfn5IslL1GyOgwqZP.5EBkZ24sTAUS', 3, NULL, '2017-10-06 00:16:47', '2017-10-06 00:16:47', 'profile_small.jpg'),
(34, 'Josue', 'josue@gmail.com', '$2y$10$5xA9Toyba7IgGRTEs98sKeODjVMVRXRuoIb1.hBcY4l.AEaO5ZOuy', 2, 'ZtTeVndxdZtojV53ld27NBTcWKmQAp9zljrqnEiwyLwsQMiIesQsoVYPkTNH', '2017-10-06 21:12:58', '2017-10-06 21:12:58', 'profile_small.jpg'),
(42, 'Gilberto', 'gilberto@gmail.com', '$2y$10$R..ua/y6PJqveoPaxtezDej/v0Yak4tixBx9NdWb73ZexD1wFAsQ2', 4, 'V1TLfebgSpgxg0F4SmQYT7a3CNxbtuVfjlpJ1lnFVn7yHRIuZ1FsB3nP1LLB', '2017-10-06 23:12:41', '2017-10-06 23:12:41', '20171019150837168690.jpg'),
(48, 'Juan Martin', 'juanmartin.h.h@gmail.com', '$2y$10$/8fbGAmcXVrcE3nm8Im1XeBhLeUw.0peYNdOwAYf5q/bv66XSyrYa', 2, 'Jfrfz162N062DusowsXx54cI1wzBqLUy1KncVQL0qwNtZlSH4XeOHLOylIcD', '2017-10-08 01:13:22', '2017-10-08 01:13:22', 'profile_small.jpg'),
(52, 'Ana Paola', 'ana_h_h@gmail.com', '$2y$10$yJVeEFxClbUh31cMgvgmxu18LsPcdYoXXySM38AVw9Jx3HZz8yvka', 4, 'In3hivLoJREOSxqWUw3Dxwb7a1GAJi838bi3fGEKfWy2p21d7HK39kW2FYeU', '2017-10-08 01:20:55', '2017-11-09 01:08:58', '20171108151016093852.jpg'),
(53, 'Jesus Corono', 'zuno@gmail.com', '$2y$10$at3eShFoYqAGF/VpymUCTe0vMAIEzbYVw4pJht.BL/UgpuBZtQMoy', 1, 'QVEUEKjuz6eLlISQTa8NKyjYBx17oTqPZPzkHFH3GQD9WauY70Q7ImPZK7Bg', '2017-10-09 07:14:52', '2017-10-09 07:14:52', 'profile_small.jpg'),
(55, 'Victor', 'mireles@gmail.com', '$2y$10$ZRgTbiZSVeBhy.vG2VNS/O2s5Lxy86eC5L1I5asrOv8.zQWPXPC.S', 3, 'uSHadTqDSyiHDPL90QyBSjY6xL0fSzIlpSdz8P7cSBOQEtbyE7KAGbCTnxY7', '2017-10-09 07:34:49', '2017-10-19 00:23:32', '20171018150834741255.jpg'),
(56, 'David', 'david@gmail.com', '$2y$10$mAGmbIRuNfRdWW48Qib/XOwpDl8RFI7L5/7UZ77IUhw1dB2wA2fF6', 2, 'HGEX0cezbxkMDcZScDgZNlewZ8kISZ781osMUnTNllyQD4fdyK5KW95ZqJOQ', '2017-10-09 07:36:22', '2017-10-09 07:36:22', 'profile_small.jpg'),
(57, 'Rosa Maria', 'rosa@gmail.com', '$2y$10$XOCKExZV9nBQ4HM5yubIYOEb5LzIAJ7Ugklhk6tCc8eetX/V7tJl6', 4, 'YPhMw4gL7VZT6rCYPbYsm3xf24vnID70w4Li0jOd7kFzlSD22S7TTQJHeJtO', '2017-10-09 07:38:43', '2017-10-09 07:38:43', 'profile_small.jpg'),
(58, 'Carlos', 'cmartinez@gmail.com', '$2y$10$GuN0Ir.7c4AuarfwJaFHu.56mZryZd/yCgIlxb/kBNKbMP5/638Qu', 3, 'SBfslgv9H5Sgd8iOjxHzPlTCTojOh4kzUo65Oghmf4YEgb8bQUFu2YRZFFKo', '2017-10-09 07:55:52', '2017-10-09 07:55:52', 'profile_small.jpg'),
(59, 'Gabriel', 'gabo@gmail.com', '$2y$10$CNa5GbDRC7aRWMdm7PSd3u7emHHKVCtIxf/kUkLoEUSeKnjjzL/O6', 3, 'hkxxspuOivkZnxIFmvEUyaYY4argVxOQd4UWwazfUPO3HfRJs8nibe9fGoyQ', '2017-10-09 07:57:36', '2017-10-09 07:57:36', 'profile_small.jpg'),
(60, 'Lorenzo', 'lorenzo@gmail.com', '$2y$10$wiGotTr.ZG5wiEgggvl6YORyfIxcljLe6FQYwjbxHnNszo/7DT.je', 4, 'WrOkjtbM1wa5dEc30XTU5V6pulj7C5LqkpEsYtISwCPiv31VJ7P2JMl4mqLf', '2017-10-09 08:39:25', '2017-10-09 08:39:25', '20171017150825776421.jpg'),
(61, 'Juan', 'jm@gmail.com', '$2y$10$XIqDHMEG4A4EbMhkk0M6u.YBjoFYlyPn4EsQvmoXlT9/luxWKGMPG', 2, NULL, '2017-10-10 05:16:29', '2017-10-10 05:16:29', 'profile_small.jpg'),
(62, 'Hector', 'hector@gmail.com', '$2y$10$7lPyNp3yfZicKlbmjB8.yuT7aWmhx36O7DSleqDoy7i7.AuhKSJ3y', 4, NULL, '2017-10-10 05:17:04', '2017-10-10 05:17:04', 'profile_small.jpg'),
(67, 'Juan Martin Huerta Hernandez', 'juan_martin.h.h@gmail.com', '$2y$10$46LrkhBf7GJEiu6tsSAyNueg/kIlHk8Oi9vLbjP6GyEpvwcU24AiW', 1, 'STovLaXPUOjm83EN9BxMeSNm0pBnOGiLPNZXXgfPAnNdPEr2iu0KwrETO7zP', '2017-10-11 00:25:31', '2017-10-11 00:25:31', 'profile_small.jpg'),
(68, 'Martha Lopez', 'martha@gmail.com', '$2y$10$fKXHrAsQ4n84xgN1P1VbUO4pFiBfyDEk2akdCvvvV21JwKILy0IV.', 1, 'eTznWdyeYopqEiPG5NyTzcIGw1rpYbESGOhGVnvUVp4bL5PbahOGp7sjh0SP', '2017-10-11 00:26:23', '2017-10-11 00:26:23', 'profile_small.jpg'),
(69, 'Juan', 'juanmhh@gmail.com', '$2y$10$tVbaBDdgDUoP4qWZ4yOMeuhHZAN3GPN.X3VWiiKllMu7qsHgfxCWC', 3, 'kTNlb4CwRiUEHKt16WZ4U27KBB72bfzhElPMdVFIaXxcLBSYmeU9SZudgh1K', '2017-10-11 00:29:25', '2017-10-11 00:29:25', 'profile_small.jpg'),
(70, 'Aaron', 'aaron@gmail.com', '$2y$10$NM/iKa.JGhBBcb71j24eoOh9RiOxZ0XX66L3vbUJOlrTI1QTc7r4y', 2, 'ZrzfKgOmqA7REgOde5GmSSm1GmBVOZNZmcMtyriq9ThE929AqiOchDPXJJ4p', '2017-10-11 01:05:43', '2017-10-11 01:05:43', 'profile_small.jpg'),
(71, 'Miguel', 'miguel_h_h@gmail.com', '$2y$10$hBFzrSph.5emb2TsMDqMEupkNInhyeu2FAS1DKN1aQ9S7Q6bR6Wlu', 4, 'ZUz1qOLj5w3lh2lrvLTsaILnJOXT6C8w2Y93EsiUHeYOI3uqcv82EuCwdIPi', '2017-10-11 01:07:19', '2017-11-09 01:11:44', '20171108151016110471.jpg'),
(72, 'Angelo Bernal', 'angelo@gmail.com', '$2y$10$.Bjob1dmT5grZ6UhECh.XuhWI2IYSE.P/HE0gYriRSTNoDcF55xXu', 1, '2ddxN3MuB6OdIiDqEtkn0nasdWdUxp62uHjjwQQ0kRy4daw3Gy62uFUgRz8J', '2017-10-11 03:15:58', '2017-10-11 03:15:58', 'profile_small.jpg'),
(73, 'Ciel', 'ciel@gmail.com', '$2y$10$2yGZ4vRUcaQhkJJ5j.mLJe8bMIslR3gJVw5dUa8ddCtlrVeN1uWbO', 3, 'PrUzFS7ythUrxGTytYW6Q4Kt03je1DvHSe28THiDr5Iz2lllvlQZf3wVBxf1', '2017-10-11 03:52:52', '2017-10-11 03:52:52', 'profile_small.jpg'),
(74, 'power', 'powerq@gmail.com', '$2y$10$hY4s1B9vz8IvbID9pG9Mz.ZMj1Cc6girOhFVHaiOrSubpaN/9uLYm', 2, NULL, '2017-10-11 03:57:04', '2017-10-11 03:57:04', 'profile_small.jpg'),
(75, 'Ari', 'arizona@gmail.com', '$2y$10$ymbwzwT3OTK3Wxag7HPMtug70ROUFC1dcrilWGFJMVvha6.Zryl4e', 4, NULL, '2017-10-11 03:57:43', '2017-10-11 03:57:43', 'profile_small.jpg'),
(79, 'juan', 'jmelgarejo@a.com', '$2y$10$7be96N62k41UIpB183wMLOSrRsRJzwxKGlPmlaQTSRKHzUvAxKb7a', 2, NULL, '2017-10-12 21:55:59', '2017-10-12 21:55:59', 'profile_small.jpg'),
(80, 'luis', 'luis@abc.com', '$2y$10$Hhw0qcJWBys1EjvqVhH8.eRF/EKcfWKDf7aUA7benkvaIAX/2u/ji', 4, NULL, '2017-10-12 21:58:19', '2017-10-12 21:58:19', 'profile_small.jpg'),
(81, 'Hernández', 'martin_huerta@gmail.com', '$2y$10$wqByDjkepdavrGJ9O1zKsO.dFVWuWaYHYrD5HsMV7q.5eJiq5tWeW', 2, 'cR3yWfNaOxKt8uOxKlcr5GpXyqc2bf2u21kth2EA9vR5Qli30Ax5o1p2pE6S', '2017-10-12 23:25:08', '2017-10-12 23:25:08', 'profile_small.jpg'),
(82, 'Jose', 'jose_huerta@gmail.com', '$2y$10$tp.xNM5f8.l2MvfeXmDRe.ioyR5vSPaYmPwCiDqp8I.tYwuYxPnPi', 4, NULL, '2017-10-12 23:26:21', '2017-10-12 23:26:21', 'profile_small.jpg'),
(85, 'juan', 'jj@gmail.com', '$2y$10$0lYrUQ2vr.RrZVeUITowE.Dsk.nAafQ5REGITee3qoIN2aUmV47yK', 2, NULL, '2017-10-13 02:59:48', '2017-10-13 02:59:48', 'profile_small.jpg'),
(86, 'sebas', 'sebas@gmail.com', '$2y$10$0vzVycja7f2XyqAdePGOlOs4HB3HO9T3Y0ILLjwrNojCOC1nzUNI6', 4, NULL, '2017-10-13 03:01:33', '2017-10-13 03:01:33', 'profile_small.jpg'),
(87, 'Pedro Carrillo', 'carrillo@gmail.com', '$2y$10$nY1yJafpQz10anfH99ASVu8cL6IAdh9ulDl14bfuIGNHTvzCP74Ya', 1, 'dcm94V0fQFMij939M4BpVb1apvzLFw8B7bovcInBZd8RZ78lFT1zNbceG1ZW', '2017-10-18 23:59:35', '2017-10-18 23:59:35', 'profile_small.jpg'),
(88, 'Juan José Melgarejo', 'jmelgarejo@myappcollege.com', '$2y$10$/o/A1uquCNgR9kAO9u1no.mIbBz7IAwxgg2d6zn9c9JDza0.ILnIC', 1, 'JEntmhgsJzugBwTP7fMFqzx3GRBEYCnoxHXpLZBQFPy3L2BqYer5eXCPs1V0', '2017-10-19 02:23:09', '2017-10-19 02:23:09', 'profile_small.jpg'),
(89, 'Juan', 'jmelgarejo@okventa.com', '$2y$10$2y52r.Kd.IsFT2KTPFjrD.MWujs9tHjoiVaE4ZdTf44AHUB6W1GzW', 2, 'pdnECJTSnjKwL8aNsfBCEWxGrAqJlN58Xf6Mn5GNTG4zzsTeUDoa5g0Rv8Y4', '2017-10-19 07:04:11', '2017-10-19 07:04:11', 'profile_small.jpg'),
(90, 'Alex', 'alejandro.arrez@gmail.com', '$2y$10$ZdTyOSvWRgYqyXyizcBk.uhDNZbSmMXrJn1G/QJ0OJkvOLKTu525m', 3, 'oVzu9xGHyYcSh7N3Z8HQ9MzOZ99qnRdT6kfs97ZgniYemIlT9cyciadeKebz', '2017-10-19 07:06:25', '2017-10-19 07:08:06', '20171019150837168690.jpg'),
(91, 'Alejandro Arrez', 'alejandro_arrez@hotmail.com', '$2y$10$JmFqevD0o8.pqbHdBV8pLu4QOvrBVDpsmVr0njoeiRhiF3g0/1QFy', 1, NULL, '2017-10-19 21:19:46', '2017-10-19 21:19:46', 'profile_small.jpg'),
(92, 'Alejandro Arrez', 'alex@markesales.com', '$2y$10$TULnlFZidt3EJKkvY1OC3.FRSdoC0wUlaib/oT3m/jODJAyvjL3HC', 1, '3hH7LrnvnBdJmtow0BaDgrZTUtYZPdaqTzAIVMW9c65NojzjHezKXBCGh9d7', '2017-10-19 21:27:58', '2017-10-19 21:27:58', 'profile_small.jpg'),
(93, 'Ioannes', 'jmelgarejo@mexicotodo.com', '$2y$10$tOkfoYPCnq1RAiRKJel/POKnHG5tTNGmD88Qk.hsZlo2Biph2ZV2a', 1, '6qdgkipYIfND9Hrpf2iaJn7exE7ZlBUWi5CKjiIYEdTwDntwuwtJraTUE6oW', '2017-10-23 06:28:31', '2017-10-23 06:28:31', 'profile_small.jpg'),
(94, 'Mario Mejia', 'motolinea@gmail.com', '$2y$10$kCgWuw6hXg5/a4rmM/kMFOqOhAZXE2cu98Di.lWawcXxL4goB4XXy', 1, '5vKoQwRHjYrpdCmUU6ATq1wIpQGpR7vcExSCb6rJuj9A5cfCTPbAY9nwijYT', '2017-10-23 21:24:02', '2017-10-26 21:59:19', '20171026150902995994.jpg'),
(95, 'Jean', 'jrobles@motolinea.com', '$2y$10$lsMRP4Ktn2sH/j3fIE7lvO54u2gl5aM0rYztzah0wHoNZFuXW75Su', 3, 'Hh63GNbM4rO4ByiNOBL31UchGKkP2wagzsFQJNr716YScbf17ALjAq36Qba1', '2017-10-23 22:05:02', '2017-10-23 22:05:02', 'profile_small.jpg'),
(96, 'Dulce', 'dulce@motolinea.com', '$2y$10$Cq4YHcmPjw3chJUSEYBCiuCZtZkT5KqLoRe0ynSRTGmvvPdlp.ajW', 3, 'CgjiYzK6Najtztqvx6gjHLR5CwiwNKoWj5ZFYcjNpJ0tUmoG5kZ35QshFMnS', '2017-10-23 22:17:49', '2017-10-23 22:17:49', 'profile_small.jpg'),
(97, 'Luis', 'luis@motolinea.com', '$2y$10$KeFjG0O711gtqOtdw0r4xeEx/ZgUxfHQHuR9MXcM//AAKE.9bDRLm', 2, 'pzuclIkND67cDFcXTd7EMRfCfTv558wsGpmAKA3FnBI4DpCFmqDt63fJLehQ', '2017-10-23 22:31:37', '2017-10-23 22:31:37', 'profile_small.jpg'),
(98, 'Luisito', 'luisito@motolinea.com', '$2y$10$O6nIWlVLoOJ8qItjQVMMie0xw2UFUcA.ABO/aYVUAIFiDzpzPI2Be', 4, NULL, '2017-10-23 22:32:52', '2017-10-23 22:32:52', 'profile_small.jpg'),
(99, 'Rogelio Preza', 'roger@gmail.com', '$2y$10$QO/S8HAu8OhwOgO1W6NSnueBUDZ4hdW1A3kDbZL0f8aNy7deaITMy', 1, 'MJQLMxqckMvStITaZweSdWscxVLLDeNaMpr7vIdOVpHigaeW4Hjst9xsOO9a', '2017-10-24 21:23:48', '2017-10-24 21:24:58', '20171024150885509899.jpg'),
(100, '|Juanjo', 'jmelgarejo@gmail.com', '$2y$10$N.fjops3f.LoAqzmDfwLRudu0ABg.JhNKVx0Z3lEwSNAGaM8XvhZ6', 2, '8yQhBWvKJZWxCPDl0rpVbgBGHU3JLvBXeCAi1wFSdbNCysXGGpp4HuT66bNo', '2017-10-24 22:05:11', '2017-10-24 22:13:24', '201710241508858004100.jpg'),
(101, 'Luisa', 'luisa@gmail.com', '$2y$10$cN8UDWKJ3iCMBCY9OiMMH.YkZuNC6VWXtZ0bM618Bf21lgif8I4FS', 3, '7BYCb3YpmCRSxo0IB1MyGoNV5BuDXLyld0ST5aMqzIxuKWM1UojPiQJ5FX2e', '2017-10-24 23:35:38', '2017-10-24 23:36:19', '201710241508862979101.jpg'),
(102, 'Tino', 'tino@gmail.com', '$2y$10$csJfKA6rtKVmiMQQ2AM/yeMmBZp9DUxbiKlWN/Avu6.hAjlrXgmpW', 2, 'SYuOXbLi40I6sG4EfDo02eUg0UZs0fDJELE64vq6YQsKaZsubP5jU3Ox2oHO', '2017-10-25 00:00:53', '2017-10-25 00:00:53', 'profile_small.jpg'),
(103, 'Vanessa', 'vane@gmail.com', '$2y$10$FcMLPjsOVY04tz.GKUUIVOm2KC4MztDdo93fW8pHFcEoOX.uVFZq6', 4, NULL, '2017-10-25 00:02:10', '2017-10-25 00:02:10', 'profile_small.jpg'),
(104, 'Abigail', 'abi@gmail.com', '$2y$10$Vch9TJdDm9lvjLqZjslKZOFGtxnIwoWOaqq95.Op72IF3w2ruffMi', 4, NULL, '2017-10-25 00:10:41', '2017-10-25 00:10:41', 'profile_small.jpg'),
(105, 'Abigail', 'aby@gmail.com', '$2y$10$1F9KFDn9aYS3r6.QOZ5hCum73I3CPirZGgkegfdI0q2hP9QTMDnMi', 4, NULL, '2017-10-25 01:20:56', '2017-10-25 01:20:56', 'profile_small.jpg'),
(106, 'Abigail', 'abigail@gmail.com', '$2y$10$7GdzyCX.nfwewU2R64w6cuVt93WMtyivdtztkp5.ocB740fGnRfjy', 4, NULL, '2017-10-25 01:24:22', '2017-10-25 01:24:22', 'profile_small.jpg'),
(107, 'Abigail', 'abigail1@gmail.com', '$2y$10$gr3G5i7TSqsbn1prmgCg4OKT21OPZwc9zFypXvCEM6.7bfsZb8yuC', 4, NULL, '2017-10-25 01:24:53', '2017-10-25 01:24:53', 'profile_small.jpg'),
(108, 'andrea', 'andrea@gmail.com', '$2y$10$X0rvB8U7ixxM2nCJPBGUNOcl5OR5HHH574rEUf6Je2ukB1eCDwOQS', 4, NULL, '2017-10-25 01:32:47', '2017-10-25 01:32:47', 'profile_small.jpg'),
(109, 'hj', 'tino2@gmail.com', '$2y$10$QpWgLdksQZJxIDf5Xlg/8OEs0183JRK2emZbXcVOTIQOoWD3XB3c2', 4, NULL, '2017-10-25 01:36:48', '2017-10-25 01:36:48', 'profile_small.jpg'),
(110, 'Federico', 'federico@motolinea.com', '$2y$10$72OTrKQPqF3UuYGWGYng5OfLp71uDgyOG7lqpNROg1NG2snTUFWWe', 3, '0Aab0Gl2gQ2VCZO2kpQbFWkY9GUJeHu997x4y1F04FLruhwId9FyUi9flma0', '2017-10-25 05:45:56', '2017-10-25 05:45:56', 'profile_small.jpg'),
(111, 'Carla', 'carla@motolinea.com', '$2y$10$VEjswo0ZyV8IB.NlIW3Yx.GEO5cN3URzrcTAX7.0RQKYaLWcCy9AW', 4, 'TMJF7OigcKBQaqp6Lk3AmKCAUJMeYrVg8aYorVRNHJywUOOndu6ZHTosU2S9', '2017-10-25 23:58:43', '2017-10-25 23:58:43', 'profile_small.jpg'),
(112, 'Elizabeth', 'eliza@gmail.com', '$2y$10$MSVncS4Zl1c/OmtG5m57suGWvJDHbN2FLQjf/2M8/VFvVb7vSzJ/K', 2, NULL, '2017-10-26 05:20:21', '2017-10-26 05:20:21', 'profile_small.jpg'),
(113, 'Mario', 'marior@gmail.com', '$2y$10$q00fM.HxiUmunNAe36u52.c3YnH2TqON7FE/QhAPToXkf1JksAuye', 4, NULL, '2017-10-26 05:21:42', '2017-10-26 05:21:42', 'profile_small.jpg'),
(114, 'Nestor Pérez', 'nestor.perez@gmail.com', '$2y$10$6Dh8/5J5WtYoTTyUL3Y4XutEMtaRqw7QhTfHLgfAKTOXtRy3qVfcO', 1, 'ztriNzA5NYBxwpAHElg3J3oZl76goAFmfu89Koy9zWzrNsWdKBIJpN9FN5Sl', '2017-10-26 06:26:38', '2017-10-26 06:26:38', 'profile_small.jpg'),
(126, 'paola', 'pao@gmail.com', '$2y$10$ov7qSLwhfomxQS1xRoaEE.ayzvBCuQNpR7LE6kK/K59951vPPVCkq', 2, NULL, '2017-10-30 23:04:46', '2017-10-30 23:04:46', '20171017150825516021.jpg'),
(127, 'Marisol', 'marisol@gmail.com', '$2y$10$L52kY4bMKSewQ/fTqwtimOHR1f6kIsNOOAinO4cB/0wM6temZ8MXy', 2, NULL, '2017-10-31 00:06:33', '2017-10-31 00:06:33', '201710301509383196.jpg'),
(128, 'Jesus', 'jesusr@gmail.com', '$2y$10$hgbGBNOr9Apu1YeycDDaS.oZjoV30EFqv7ayPuFuhx8dPPA8kQDwa', 3, NULL, '2017-10-31 00:36:36', '2017-10-31 00:36:36', '201710301509384997.jpg'),
(129, 'Juan Martin', 'jmhh@gmail.com', '$2y$10$B4qEjuZJHDrLJYnRu4Cg.e1E8w8QLFcupmZprf6270EHlPde0hGDO', 2, NULL, '2017-10-31 00:51:35', '2017-10-31 00:51:35', '201710301509385896.jpg'),
(130, 'Juan', 'juan18@gmail.com', '$2y$10$C6kLUHWyVx5hR2A7eWvuw.niM4SKNGyWXhNPTuch.PpQpkvFMNPg6', 4, NULL, '2017-10-31 00:52:56', '2017-10-31 00:52:56', '201710301509385977.jpg'),
(131, 'Edgar', 'edgar@motolinea.com', '$2y$10$LRevm1xt0MCMreLNeq5PfONcCXfLodyf0Do.Vi9EtWaYCWdzIbLKe', 3, NULL, '2017-11-01 03:43:45', '2017-11-01 03:43:45', 'profile_small.jpg'),
(132, 'Alejandro', 'alex@gmail.com', '$2y$10$9qarKFAXdc3BtlCmS6Grw.gvtKoJ0LaqGABtoRG9SJjm1KtJBMb9O', 2, NULL, '2017-11-03 04:27:44', '2017-11-03 04:27:44', '201711021509658065.jpg'),
(136, 'Diego', 'alejandro@gmail.com', '$2y$10$W0tcT/FTQY1DnttJqNN8huHOzaQ4RyKUqai3AWWCp.f1HuL.kbpDW', 4, NULL, '2017-11-03 04:31:25', '2017-11-03 04:31:25', '201711021509658286.jpg'),
(139, 'Jean', 'jeanalbert@gmail.com', '$2y$10$wnjwGXB8lUMLDQxz0rYVueRPpyylpiwCy3/WE9pzFf2C12r5Z8.x.', 3, 'Iut91MVB3mPXydTmxREJNO6jBY971ZJdOiWqsHl4pKi3uOTTWQbvaMLquyFk', '2017-11-08 08:03:36', '2017-11-08 08:03:36', 'profile_small.jpg'),
(141, 'Ruiz', 'miguelito@gmail.com', '$2y$10$zo5UUtpPjSnaS5.mZe719e13bq9D8fHm/llmrw700CxGksLvcgBgO', 3, NULL, '2017-11-08 23:27:04', '2017-11-08 23:27:04', '201711081510154827.jpg'),
(142, 'Dulce Medina', 'dulce@benitojuarez.com', '$2y$10$SrjeskkjeUDXi/CjtNMfWutCtMpRLmyUjsPhqRlmCDIYszeaTWSWu', 1, 'VDVD3fPcandKCw4rOfFr9ZUyENrYt6m3PQXmRRcQryUJ54V6PUY8yTtJ6lnB', '2017-11-09 06:46:12', '2017-11-09 06:46:12', 'profile_small.jpg'),
(143, 'Ivette Palacios', 'Ivette@SilvaHerzog.com', '$2y$10$f7UXkWeG5qkoSFQwOrw3iuoDZ9.0UgCtWca3jfmpVCS3cOf42FYie', 1, 'PAeTxZ4Ke1ViaVm1MLdgFKFFj44bB7RddnhcUxYzqzq95WsljLBVRPVr6EOc', '2017-11-09 07:22:38', '2017-11-09 07:22:38', 'profile_small.jpg'),
(144, 'Andy Appycollege', 'andy@appycollege.com', '$2y$10$W/xDM8.OCBAAaroExeTVF.MxOD49QishLveWNaVT6XhbQPr6mi69q', 1, 'CzurYnTGqoAdMNsMCuCaPg9A13d4NA3oskWkaIpIMAMxVy6BVqGyBN8m6sOv', '2017-11-10 01:04:43', '2017-11-10 01:04:43', 'profile_small.jpg'),
(145, 'Daniela', 'daniela@appycollege.com', '$2y$10$ib47EnyOSF28l2fMmyBfKec6eB21rjud6MQGUegUqO68AFPksa0w.', 3, 'h3PDDUYqUezWhL4xJn3EzsottKr2Oxy5Kl5d1atM8wOFQp0H8BS0TCnHPUXX', '2017-11-10 01:28:17', '2017-11-10 01:28:17', 'profile_small.jpg'),
(146, 'Alex', 'alex@appycollege.com', '$2y$10$45uXMv.aTZtRqonTJImmLuLlahbUmZ7CJMAodvTPJGz3vJnR8x.yi', 3, NULL, '2017-11-10 01:29:55', '2017-11-10 01:29:55', 'profile_small.jpg'),
(147, 'Juan Martin', 'juanmartin@gmail.com', '$2y$10$itWmOo80Zo9.Gq9VCalcnOUkUb.43ACqy7wiybQ/ciF8ei9ruly8u', NULL, NULL, '2017-11-11 02:36:22', '2017-11-11 02:36:22', 'profile_small.jpg'),
(148, 'marco gallegos', 'marco@gmail.com', '$2y$10$t06J6ja1dKctM60wjwG2f.R80a8zNMr00aD3V9XCrVW0bkQ5UdErq', 1, NULL, '2017-11-11 03:35:49', '2017-11-11 03:35:49', 'profile_small.jpg'),
(149, 'Dulce', 'dulce01@gmail.com', '$2y$10$kgbfOEXZm59oDwe0Iiv.vefUtySIPwVOka1P4AO/Zrkufsxlq7vfm', 3, NULL, '2017-11-11 04:04:02', '2017-11-11 04:04:02', 'profile_small.jpg'),
(150, 'Dulce', 'dulcemedina@gmail.com', '$2y$10$3G/Mi9LJQKAGJj9My/p9f.ayy0gBnT4WMBVlAbX4zHSSMlHer44Ly', 2, 'obbHDH2tMOutMmAvSgNEpsDgCCgIEwRfJXIp3aCiKAx8lqB6XYvj8AZiU8Cb', '2017-12-13 23:44:56', '2017-12-13 23:44:56', 'profile_small.jpg'),
(151, 'JEAN ALBERT', 'devjeanr@gmail.com', '$2y$10$hne2gx1D5AAs5kf9jhySduMLOy1DqZBmaekmiDjnrrYM6QIdYy7BG', 2, NULL, '2017-12-13 23:45:52', '2017-12-14 02:32:08', '201712131513189928151.jpg'),
(152, 'Rosa', 'rosita@gmail.com', '$2y$10$GWXGykD4LertWI2mjSTjPu15p.OYHXVZqW..2yKZ/t0pf0sdRSySG', 4, NULL, '2017-12-13 23:47:57', '2017-12-13 23:47:57', 'profile_small.jpg'),
(153, 'EMMA', 'emma@gmail.com', '$2y$10$FWBLHyGmsiJW8vh5jfNt6.ZG.IY2/b5nlGavD6VQq8o6fHt4Fap1S', 4, NULL, '2017-12-13 23:50:21', '2017-12-13 23:50:21', '201712131513180224.jpg'),
(154, 'Marco Antonio', 'marco.reyna.cronos@gmail.com', '$2y$10$7Xydbm6IocL01eQPOaYLzOKNh7umKfpF1x0CMqFMne2/Wf3V5/yu.', 2, NULL, '2017-12-13 23:51:16', '2017-12-13 23:51:16', 'profile_small.jpg'),
(155, 'Citlalli', 'citlalli@gmail.com', '$2y$10$OpdFnLq.kaVHvxywAiEB3.X6kagStgMQB68rUbsi.4skc.93qNJoS', 4, NULL, '2017-12-13 23:52:41', '2017-12-13 23:52:41', 'profile_small.jpg'),
(156, 'DIANA', 'dianatr@gmail.com', '$2y$10$D2D1CSCMtCxBSwiYHyWEieFMCn4wtTsJE3LX9dZkPKJxs49SFrGGy', 2, NULL, '2017-12-13 23:59:45', '2017-12-13 23:59:45', '201712131513180786.jpg'),
(157, 'ARTURITO', 'arturito@gmail.com', '$2y$10$y.oMts7MNxNIXkdEmwYmJePmdD6rYxlPrQe3wlUh/IDToC8R.6NfS', 4, NULL, '2017-12-14 00:02:29', '2017-12-14 00:02:29', '201712131513180950.jpg'),
(160, 'MARCO', 'marco_reyna@gmail.com', '$2y$10$DCmki3E7sB/ilegbIXFG2uh.enanzYQeJGMkAm0gPaYLRlBp8Gb3m', 2, NULL, '2017-12-14 01:02:58', '2017-12-14 01:02:58', '201712131513184581.jpg'),
(161, 'MAURICIO', 'mauricio_reyna@gmail.com', '$2y$10$Ski9E/yKeG4CRT1CDR11teI2OkcdutxWpSsOizp/QwdH/Me9qDYFC', 4, NULL, '2017-12-14 01:05:08', '2017-12-14 01:05:08', '201712131513184710.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadmin`),
  ADD KEY `FKinst_idx` (`id_institucion`),
  ADD KEY `FKinuser_idx` (`iduser`);

--
-- Indices de la tabla `alerta`
--
ALTER TABLE `alerta`
  ADD PRIMARY KEY (`idAlerta`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idAlumno`),
  ADD KEY `FKinst_idx` (`idinstitucion`),
  ADD KEY `FKiduser_idx` (`iduser`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD KEY `FKgrupo_idx` (`idSubgrupo`);

--
-- Indices de la tabla `avisos`
--
ALTER TABLE `avisos`
  ADD PRIMARY KEY (`idAviso`);

--
-- Indices de la tabla `calendario`
--
ALTER TABLE `calendario`
  ADD PRIMARY KEY (`idCalendario`),
  ADD KEY `FKgrupo_idx` (`id`);

--
-- Indices de la tabla `calendario_asistencia`
--
ALTER TABLE `calendario_asistencia`
  ADD PRIMARY KEY (`idCalendarioAsistencia`);

--
-- Indices de la tabla `calif`
--
ALTER TABLE `calif`
  ADD PRIMARY KEY (`idcalif`),
  ADD UNIQUE KEY `idSubgrupo` (`idSubgrupo`,`idalumno`,`idPeriodo`),
  ADD KEY `FKmateria_idx` (`idSubgrupo`),
  ADD KEY `fkalumno_idx` (`idalumno`);

--
-- Indices de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  ADD PRIMARY KEY (`idCiclo`);

--
-- Indices de la tabla `conocimientos_adicionales`
--
ALTER TABLE `conocimientos_adicionales`
  ADD PRIMARY KEY (`idConocimientos`);

--
-- Indices de la tabla `contenidos`
--
ALTER TABLE `contenidos`
  ADD PRIMARY KEY (`idContenido`);

--
-- Indices de la tabla `datos_medicos`
--
ALTER TABLE `datos_medicos`
  ADD PRIMARY KEY (`idHistorialMedico`);

--
-- Indices de la tabla `destinatarios`
--
ALTER TABLE `destinatarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `directorio`
--
ALTER TABLE `directorio`
  ADD PRIMARY KEY (`idDirectorio`);

--
-- Indices de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD PRIMARY KEY (`idEncuesta`);

--
-- Indices de la tabla `enc_preguntas`
--
ALTER TABLE `enc_preguntas`
  ADD PRIMARY KEY (`idPregunta`);

--
-- Indices de la tabla `enc_respuestas`
--
ALTER TABLE `enc_respuestas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`),
  ADD KEY `FKpais|_idx` (`idpais`);

--
-- Indices de la tabla `grado`
--
ALTER TABLE `grado`
  ADD PRIMARY KEY (`idInstitucion`,`idNivel`,`grado`),
  ADD KEY `idInstitucion` (`idInstitucion`,`idNivel`,`grado`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`idgrupo`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`idHorario`);

--
-- Indices de la tabla `Insignias`
--
ALTER TABLE `Insignias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD PRIMARY KEY (`idInstitucion`),
  ADD KEY `FKusuario` (`id_usuario`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`idMateria`);

--
-- Indices de la tabla `materia_profesor`
--
ALTER TABLE `materia_profesor`
  ADD PRIMARY KEY (`idMateria`,`idProfesor`,`idInstitucion`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`idMensaje`),
  ADD KEY `FKenc_idx` (`encargado`),
  ADD KEY `FKdest_idx` (`destinatario`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `niveles`
--
ALTER TABLE `niveles`
  ADD PRIMARY KEY (`idNivel`);

--
-- Indices de la tabla `nivel_inst`
--
ALTER TABLE `nivel_inst`
  ADD PRIMARY KEY (`idInstitucion`,`idNivel`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `periodo`
--
ALTER TABLE `periodo`
  ADD PRIMARY KEY (`idPeriodo`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idprofesor`),
  ADD KEY `FKinst_idx` (`id_institucion`),
  ADD KEY `FKinuser_idx` (`iduser`);

--
-- Indices de la tabla `r_alumno_tarea`
--
ALTER TABLE `r_alumno_tarea`
  ADD PRIMARY KEY (`idAlumno`,`idTarea`);

--
-- Indices de la tabla `subgrupo`
--
ALTER TABLE `subgrupo`
  ADD PRIMARY KEY (`idSubgrupo`);

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`idTareas`),
  ADD KEY `FKgrupo_idx` (`idSubgrupo`);

--
-- Indices de la tabla `tareas_vistas`
--
ALTER TABLE `tareas_vistas`
  ADD PRIMARY KEY (`idTareasVistas`);

--
-- Indices de la tabla `TipoAlerta`
--
ALTER TABLE `TipoAlerta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiposinsignia`
--
ALTER TABLE `tiposinsignia`
  ADD PRIMARY KEY (`idTipoInsignia`);

--
-- Indices de la tabla `tipo_users`
--
ALTER TABLE `tipo_users`
  ADD PRIMARY KEY (`idtipo_users`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`idtutor`),
  ADD KEY `FKiduser_idx` (`iduser`);

--
-- Indices de la tabla `tutoralumno`
--
ALTER TABLE `tutoralumno`
  ADD PRIMARY KEY (`idtutor`,`idalumno`),
  ADD KEY `FKalumno_idx` (`idalumno`);

--
-- Indices de la tabla `tutor_institucion`
--
ALTER TABLE `tutor_institucion`
  ADD PRIMARY KEY (`idTutor`,`idInstitucion`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `alerta`
--
ALTER TABLE `alerta`
  MODIFY `idAlerta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `idAlumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `avisos`
--
ALTER TABLE `avisos`
  MODIFY `idAviso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `calendario`
--
ALTER TABLE `calendario`
  MODIFY `idCalendario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `calendario_asistencia`
--
ALTER TABLE `calendario_asistencia`
  MODIFY `idCalendarioAsistencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `calif`
--
ALTER TABLE `calif`
  MODIFY `idcalif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;
--
-- AUTO_INCREMENT de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  MODIFY `idCiclo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `conocimientos_adicionales`
--
ALTER TABLE `conocimientos_adicionales`
  MODIFY `idConocimientos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contenidos`
--
ALTER TABLE `contenidos`
  MODIFY `idContenido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `datos_medicos`
--
ALTER TABLE `datos_medicos`
  MODIFY `idHistorialMedico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `destinatarios`
--
ALTER TABLE `destinatarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `directorio`
--
ALTER TABLE `directorio`
  MODIFY `idDirectorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  MODIFY `idEncuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT de la tabla `enc_preguntas`
--
ALTER TABLE `enc_preguntas`
  MODIFY `idPregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;
--
-- AUTO_INCREMENT de la tabla `enc_respuestas`
--
ALTER TABLE `enc_respuestas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=555;
--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `idgrupo` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT de la tabla `Insignias`
--
ALTER TABLE `Insignias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `institucion`
--
ALTER TABLE `institucion`
  MODIFY `idInstitucion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `idMateria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `niveles`
--
ALTER TABLE `niveles`
  MODIFY `idNivel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `periodo`
--
ALTER TABLE `periodo`
  MODIFY `idPeriodo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idprofesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `subgrupo`
--
ALTER TABLE `subgrupo`
  MODIFY `idSubgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `idTareas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT de la tabla `tareas_vistas`
--
ALTER TABLE `tareas_vistas`
  MODIFY `idTareasVistas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `TipoAlerta`
--
ALTER TABLE `TipoAlerta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `tiposinsignia`
--
ALTER TABLE `tiposinsignia`
  MODIFY `idTipoInsignia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
  MODIFY `idtutor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `FKinstAdmin` FOREIGN KEY (`id_institucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKinuserAdmon` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `FKiduser2` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKinst2` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `FKpais` FOREIGN KEY (`idpais`) REFERENCES `pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD CONSTRAINT `FKusuario` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD CONSTRAINT `FKdest` FOREIGN KEY (`destinatario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKenc` FOREIGN KEY (`encargado`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `FKinst00` FOREIGN KEY (`id_institucion`) REFERENCES `institucion` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKinuser0` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD CONSTRAINT `FKiduser` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
