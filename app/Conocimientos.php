<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conocimientos extends Model
{
   protected $table='conocimientos_adicionales';
   protected $primaryKey="idConocimientos";
   public $timestamps=false;
   protected $filetable=[
   'idAlumno',
   'conocimientos1',
   'conocimientos2',
   'conocimientos3',
   ];
   protected $guarded=[];
}
