<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Alumno;
use DB;
use Intervention\Image\ImageManagerStatic as Image;

class TutorEstadisticas extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tutor.TutorEstadisticas');
    }      
}