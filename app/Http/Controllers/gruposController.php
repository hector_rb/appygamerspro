<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\grupo;
use App\grado;
use App\periodo;
use Illuminate\Support\Facades\Redirect;

class gruposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $grupo=DB::table('grupo')
        ->select('grupo.grado','grupo.grupo','niveles.nombreNivel','grupo.contrasena')
        ->join('niveles','niveles.idNivel','=','grupo.idNivel')
        ->where('grupo.idInstitucion','=',$id)
        ->get();
        return view('Admin.grupos',['grupos'=>$grupo]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $iduser=Auth::user()->id;

        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;

        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {

                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        return view('Admin.crearGrupos',['niv'=>$niv]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $l=['A','B','C','D','E','F','G','H','I','J','K','L','M'];
        $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $su = strlen($an) - 1;
        $id=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$id)->first();
        $idinst=$inst->idInstitucion;
        for ($i=1; $i <=$request->grados ; $i++) { 
            $gdo= new grado;
            $gdo->idInstitucion=$idinst;
            $gdo->idNivel=$request->nivel;
            $gdo->grado=$i;
            $gdo->save();
            for ($j=1; $j <= $request->grupos ; $j++) { 
                $gpo= new grupo;
                $gpo->idInstitucion=$idinst;
                $gpo->idNivel=$request->nivel;
                $gpo->grado=$i;
                $gpo->grupo=$l[$j-1];
                $gpo->contrasena=substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1);
                $gpo->save();
            }

        }
        for ($j=1; $j <= $request->periodos ; $j++) { 
                $po= new periodo;
                $po->idInstitucion=$idinst;
                $po->idNivel=$request->nivel;
                $po->nombrePeriodo=$j;                
                $po->save();
            }

        return Redirect::to('admin/grupos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
