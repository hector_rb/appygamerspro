<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Mensaje as Mensaje;
use DB;

class mensajes extends Controller
{

    public function index()//metodo que muestra los mensajes recibidos, si se modifica se debe copiar en recibidos
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
        $id=$institucion->idInstitucion;
        $query=DB::table('mensajes')->latest()->get();
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])->count();//muestra los mensajes que no ha visto el usuario
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])//mensajes para el admin
                             ->orWhere([ ['TipoDestinatario','12'],['idInstitucion',$id ]])//mensaje para todos los administradores
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$id ]])//mensaje para todos en la institucion
                            ->orderBy('created_at', 'desc')->get();

        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Admin.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

     public function papelera($id)//metodo que manda mensajes a la papelera
     {
         DB::table('mensajes')
             ->where('id', $id)
             ->update(['Eliminado' => 1]);
             return redirect()->back();

     }

    public function create()//crea un mensaje
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        $query=DB::table('destinatarios')->get();
        return \View::make('Admin.mensajesCreate',['Destinatarios'=>$query],compact('sinver'));
    }

    public function store(Request $request)
    {
        $mensaje = new Mensaje;
        $usuario=Auth::user()->id;
	    $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
	    $id=$institucion->idInstitucion;
	    
	    //llenado de datos del mensaje
        $mensaje->Mensaje=$request->Mensaje;
        $mensaje->IdEmisor=Auth::user()->id;
        $mensaje->TipoEmisor=Auth::user()->tipo_user;
        $mensaje->TipoDestinatario=$request->TipoDestinatario;
        $mensaje->idInstitucion=$id;
        if($request->TipoDestinatario==2)//mensaje para tutor por medio del hijo
        {
            $alumno=DB::table('alumno')->select('idAlumno')->where('iduser',$request->IdDestinatario)->get()->first();
            $tutor=DB::table('tutoralumno')->select('idtutor')->where('idalumno',$alumno->idAlumno)->get()->first();
            $final=DB::table('tutor')->select('iduser')->where('idtutor',$tutor->idtutor)->get()->first();
            $mensaje->IdDestinatario=$final->iduser;
        }
        else //cualquier otro caso
        $mensaje->IdDestinatario=$request->IdDestinatario;
        $mensaje->Asunto=$request->Asunto;
        $mensaje->save();
        return redirect('admin/mensajes');

    }

  public function show($id)//metodo que muestra el mensaje
    {
        
         $usuario=Auth::user()->id;
         $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])->count();
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Visto' => 1]);

        $mensaje= Mensaje::find($id);

        switch ($mensaje->TipoEmisor)//quien lo envio
         {
            case '1':
                $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
            break;
            case '2':
                $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
            break;
            case '3':
                $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
            break;
            case '4':
                $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
            break;
        }

        switch ($mensaje->TipoDestinatario)//para quien va el mensaje
         {
            case '1':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '2':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '3':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '4':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '5':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '6':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '7':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->get();
            break;
            case '8':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos') as Nombre,idgrupo as id"))->where('idgrupo','1')->get();
            break;
            case '12':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Administradores') as Nombre,idgrupo as id"))->where('idgrupo','1')->get();
            break;


        }
        return \View::make('Admin.mensajeShow',compact('mensaje','variable','variable2','sinver'));
    }


    public function edit($id)//metodo que muestra la vita para editar un mensaje
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $mensaje= Mensaje::find($id);
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        return \View::make('Admin.mensajesEdit',compact('mensaje','sinver'));
        $mensaje = Mensaje::find($id);
    }


    public function enviados() //metodo que muestra los mensajes enviados
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $query=DB::table('mensajes')->latest()->get();
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdEmisor',$usuario ],['TipoEmisor',$tipo]])->get();
        $var=DB::table('users')->select('id','name as Nombre')->get();
        $grupo=  DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->get();
        return \View::make('Admin.enviados',compact('mensajes','var','grupo','sinver'));
    }

    public function update(Request $request, $id) //metodo de actualizacion 
    {
        request()->validate([
            'Asunto' => 'required',
            'Mensaje' => 'required',
        ]);
        Mensaje::find($id)->update($request->all());
        return redirect('admin/mensajes');
    }

    public function destroy($id) //permite eliminar un mensaje (solo mensajes enviados)
    {
        $mensaje = Mensaje::find($id);
        $mensaje->delete();
        return redirect()->back();
    }


    public function search(Request $request) //permite usar el textbox de busqueda en recibidos
    {
         $usuario=Auth::user()->id;
         $tipo=Auth::user()->tipo_user;
         $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
         $mensajes = Mensaje::where('Asunto','like','%'.$request->Asunto.'%')->get();
         $alumno=DB::table('users')->select('id','name as Nombre')->get();
         $profesor=DB::table('users')->select('id','name as Nombre')->get();//
         return \View::make('Admin.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    public function recibidos() //mensajes recibidos, si se modifica algo aqui se tiene que copiar en index()
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
        $id=$institucion->idInstitucion;
        $query=DB::table('mensajes')->latest()->get();
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])->count();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])//filtros por usuario
                            ->orWhere([ ['TipoDestinatario','12'],['idInstitucion',$id ]])//todos los administradores (para cuando se implemente el s.a.)
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$id ]])//todos en la institucion
                            ->orderBy('created_at', 'desc')->get();

        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Admin.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    public function muestra($id)
    {
        $mensaje= Mensaje::find($id);
        return \View::make('Admin.mensajesShow',compact('mensaje'));
        $mensaje = Mensaje::find($id);
    }

    public function getDestinatario(Request $request) //metodo que permite obtener los destinatarios en base al select en MensajesCreate.blade.php
    {
        $usuario=Auth::user()->id;
	    $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
	    
        switch ($request->id) 
        {
            case '1':
                $admin=DB::table('institucion')
                ->select('id_usuario')
                ->where('idInstitucion',$institucion->idInstitucion)
                ->get()
                ->first();
                $variable = DB::table('users')->select('name as Nombre','id')->where('id',$admin->id_usuario)->get();
                //$variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','1')->get();
                break;
            case '2':
                $variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','2')->get();
                break;
            case '3':
                $variable=DB::table('profesor')
                ->select('nombreprof as Nombre','idprofesor as id')
                ->where('id_institucion',$institucion->idInstitucion)->get();
                //$variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','3')->get();
            break;
            case '4':
                $variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','4')->get();
            break;
            case '5':
                $variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','5')->get();
            break;
            case '6':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(idNivel, ' ', grupo) as Nombre,idgrupo as id"))->where('Nombre',$mensaje->IdDestinatario)->get();
            break;
            case '7':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->where('idgrupo',$mensaje->IdDestinatario)->get();
            break;
            case '8':

            break;
            case '10':
                $variable= DB::table('Niveles')->select('nombreNivel as Nombre','idNivel as id')->get();
            break;
          
            
        }
        return $variable;
    }
    
      public function getAlumnos(Request $request) //metodo que permite obtener los alumnos
    {
        $alumnos=DB::table('alumno')->select(DB::raw("CONCAT(nombreAlumno, ' ',apepat,' ',apemat) as nombreAlumno,idgrupo,iduser"))->where('idgrupo',$request->id)->get(); //('iduser','idgrupo','nombreAlumno')->where('idgrupo',$request->id)->get();
        return $alumnos;
    }

    public function eliminados()//metodo que muestra los mensajes eliminados
    {
            $usuario=Auth::user()->id;
            $tipo=Auth::user()->tipo_user;
            $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
            //$mensajes=Mensaje::all();
            $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','1']])->get();
            $alumno=DB::table('users')->select('id','name as Nombre')->get();
            $profesor=DB::table('users')->select('id','name as Nombre')->get();
            $admin=DB::table('users')->select('id','name as Nombre')->get();
            return \View::make('Admin.recibidos',compact('mensajes','alumno','profesor','sinver','admin'));
    }
    
  
}
