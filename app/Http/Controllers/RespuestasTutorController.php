<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Encuestas;
use App\Preguntas;
use App\Completar;
use App\Respuestas;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EncuestasFormRequest;
use DB;
use Auth;

class RespuestasTutorController extends Controller
{
   

public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		
		return view ("Tutor.encuestas.index");
	}

	public function create()
	{
		$iduser=Auth::user()->id;
		$tutor=DB::table('tutor')
		->select('idtutor')
		->where('iduser','=',$iduser)
		->first();
		$encuestas=DB::table('enc_completa as enc')
		->select('enc.idEncuesta','enc.numPreguntas','encuesta.nombre','encuesta.descripcion')
		->join('encuesta','encuesta.idEncuesta','=','enc.idEncuesta')
		->where('enc.idEncuestado','=',$tutor->idtutor)
		->where('enc.tipoEncuestado','=',3)
		->where('enc.contestadas','=',0)
		->get();
		return view("Tutor.encuestas.create",["encuestas"=>$encuestas]);
	}
	
	public function getPreguntasTutor(Request $request)
	{
		$preguntas=DB::table('enc_preguntas as pre')
		->where('pre.idEncuesta','=',$request->idenc)
		->OrderBy('idPregunta','asc')
		->get();
		return $preguntas;
	}

	public function store(Request $request)
	{

		$iduser=Auth::user()->id;
		$tutor=DB::table('tutor')
		->select('idtutor')
		->where('iduser','=',$iduser)
		->first();
        $preguntas=DB::table('enc_preguntas as pre')
        ->select('pre.idPregunta')
		->where('pre.idEncuesta','=',$request->encuestas)
		->orderBy('pre.idPregunta','asc')
		->get();
		
		$v=0;
		foreach ($preguntas as $key) {
			$Respuestas = new Respuestas;		
			$Respuestas->idEncuestado=$tutor->idtutor;
			$Respuestas->tipoEncuestado=3;
			$Respuestas->respuesta=$request->get('respuesta'.$v);
			$Respuestas->idPregunta=$key->idPregunta;
			$Respuestas->idEncuesta=$request->encuestas;
			$Respuestas->save();
			$v=$v+1;
		}
		DB::table('enc_completa')
		->where('idEncuesta','=',$request->encuestas)
		->where('idEncuestado','=',$tutor->idtutor)
		->where('tipoEncuestado','=',3)
		->update(['contestadas'=> $v]);
		
		return Redirect::to('/tutor/encuestas/create');
	}
	
	
	public function show($id)
	{
		return view("tutor.encuestas.show");

	}
	
	
	public function edit($id)
	{
		return view("tutor.encuestas.edit");
		
	
	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		
	
	}
}