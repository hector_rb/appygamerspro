<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Encuestas;
use App\Preguntas;
use App\Completar;
use App\Respuestas;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EncuestasFormRequest;
use DB;
use Auth;

class RespuestasController extends Controller
{
   
public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		
		return view ("Profesor.encuestas.index");
	}

	public function create()
	{
		$iduser=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$iduser)->first();
		$encuestas=DB::table('enc_completa as enc')
		->select('enc.idEncuesta','enc.numPreguntas','encuesta.nombre','encuesta.descripcion')
		->join('encuesta','encuesta.idEncuesta','=','enc.idEncuesta')
		->where('enc.idEncuestado','=',$prof->idprofesor)
		->where('enc.tipoEncuestado','=',1)
		->where('enc.contestadas','=',0)
		->where('encuesta.estatus','=',1)
		->get();
		//echo($encuestas);
		return view("Profesor.encuestas.create",["encuestas"=>$encuestas]);
	}
	public function getPreguntas(Request $request)
	{
		$preguntas=DB::table('enc_preguntas as pre')
		->where('pre.idEncuesta','=',$request->idenc)
		->OrderBy('idPregunta','asc')
		->get();
		return $preguntas;
	}
	public function store(Request $request)
	{

		$iduser=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$iduser)->first();
        $preguntas=DB::table('enc_preguntas as pre')
        ->select('pre.idPregunta')
		->where('pre.idEncuesta','=',$request->encuestas)
		->orderBy('pre.idPregunta','asc')
		->get();
		
		$v=0;
		foreach ($preguntas as $key) {
			$Respuestas=new Respuestas;		
			$Respuestas->idEncuestado=$prof->idprofesor;
			$Respuestas->tipoEncuestado=1;
			$Respuestas->respuesta=$request->get('respuesta'.$v);
			$Respuestas->idPregunta=$key->idPregunta;
			$Respuestas->idEncuesta=$request->encuestas;
			$Respuestas->save();
			//echo($Respuestas);
			$v=$v+1;
		}
		//echo($prof->idprofesor);
		DB::table('enc_completa')
		->where('idEncuesta','=',$request->encuestas)
		->where('idEncuestado','=',$prof->idprofesor)
		->where('tipoEncuestado','=',1)
		->update(['contestadas'=> $v]);
		
		return Redirect::to('/profesor/encuestas/create');
	}
	
	
	public function show($id)
	{
		return view("profesor.encuestas.show");

	}
	
	
	public function edit($id)
	{
		return view("profesor.encuestas.edit");
		
	
	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		
	
	}
}