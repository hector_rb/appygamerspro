<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\profesor;
use Illuminate\Support\Facades\Redirect;

class perfilProfesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ima=Auth::user()->imagen;
        $informacion=DB::table('profesor')
        ->select('idprofesor','nombreProf','apepat','apemat','genero', 'fechanac','telefono','celular','matricula')
        ->where('iduser','=',Auth::user()->id)
        ->first();
        return view('Profesor/profileProf',['img'=>$ima,'info'=>$informacion]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveImage(Request $request)
    {
        //$blob=$request->file('croppedImage');
        //->storeAs(public_path().'img/profile_pics','pp.jpg');   
       // $file = file_put_contents(public_path().'img/profile_pics/pp.jpg', ;
        //echo utf8_decode($request->file('croppedImage'));
        /*if($request->hasFile('croppedImage')!=''){
            $blob=$request->file('croppedImage');
            $time=time();  
            $file = file_put_contents(public_path().'/img/profile_pics/'.$time.'.'.$blob->extension(), $blob);
            $dir=directorio::findOrFail(4);
            $dir->imagen=$blob;
            $dir->update();
        }*/
        //$data = Input::all();
        $id=Auth::user()->id;
        $user=User::findOrFail($id);
        $png_url = date('Ymd').time().$id.".jpg";
        $path = public_path().'/img/profile_pics/' . $png_url;

        $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
        if($upload)
        {
            $user->imagen=$png_url;
            $user->update();
            return $png_url;
        }
        


    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $save=profesor::findOrFail($id);
        $save->nombreProf=$request->nombre;
        $save->apepat=$request->apepat;
        $save->apemat=$request->apemat;
        $save->genero=$request->genero;
        $save->fechanac=$request->fechanac;
        $save->telefono=$request->tel;
        $save->celular=$request->cel;
        $save->matricula=$request->matricula;
        $save->save();

        $idu=Auth::user()->id;
        $user=User::findOrFail($idu);
        $user->name=$id=$request->nombre;
        $user->save();
        return Redirect::to('/profesor/perfilProf');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
