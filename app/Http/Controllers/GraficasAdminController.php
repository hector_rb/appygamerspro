<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Graficas;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\GraficasFormRequest;
use DB;
use Auth;

class GraficasAdminController extends Controller
{
            public function __construct()
	{
		
	}

	public function index(Request $request)
	{
	    
	$user=Auth::user()->id;
		$iduser=Auth::user()->id;
		$tipo=Auth::user()->tipo_user;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$user)->first();
        $sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('grupo.idInstitucion','=',$inst->idInstitucion)
		->get();

		$institucion=DB::table('institucion')->select('logo','nombre','ciudad','clave','direccion', 'cp')
		->where('idInstitucion','=',$inst->idInstitucion)
		->get();
		//echo($avisos);
			return view('Admin.graficas.index',["sg"=>$sg, "institucion" => $institucion]);
		}

	public function TotalPersonas(Request $request){
		$iduser=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$iduser)->first();
		$alumno=DB::table('alumno')->select('idAlumno')
		->where('idinstitucion','=',$inst->idInstitucion)
		->where('activo','=','1')
		->count();
		$profesor=DB::table('profesor')->select('idprofesor')
		->where('id_institucion','=',$inst->idInstitucion)
		->count();
		$tutor=DB::table('tutor')->select('tutor.idtutor')
		->join('tutoralumno','tutoralumno.idtutor','=','tutor.idtutor')
		->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
		->where('alumno.idinstitucion','=',$inst->idInstitucion)
		->groupBy('tutoralumno.idtutor')
		->get();
		$sismo=DB::table('alerta')->select('idAlerta')
		->where('idUser','=',$iduser)
		->where('tipoAlerta','=','1')
		->count();

		$incendio=DB::table('alerta')->select('idAlerta')
		->where('idUser','=',$iduser)
		->where('tipoAlerta','=','2')
		->count();

		$violencia=DB::table('alerta')->select('idAlerta')
		->where('idUser','=',$iduser)
		->where('tipoAlerta','=','4')
		->count();

		$robo=DB::table('alerta')->select('idAlerta')
		->where('idUser','=',$iduser)
		->where('tipoAlerta','=','5')
		->count();

		$altas=DB::table('alumno')->select('idAlumno')
		->where('idinstitucion','=',$inst->idInstitucion)
		->where('activo','=','1')
		->count();

		$bajas=DB::table('alumno')->select('idAlumno')
		->where('idinstitucion','=',$inst->idInstitucion)
		->where('activo','=','0')
		->count();

		$hombres=DB::table('alumno')->select('idAlumno')
		->where('idinstitucion','=',$inst->idInstitucion)
		->where('activo','=','1')
		->where ('genero', '=', 'M')
		->count();

		$mujeres=DB::table('alumno')->select('idAlumno')
		->where('idinstitucion','=',$inst->idInstitucion)
		->where('activo','=','1')
		->where ('genero', '=', 'F')
		->count();

		

		return ["alumnos" => $alumno, "profesores" => $profesor, 'tutores' => count($tutor), "sismo" => $sismo, "incendio" => $incendio, "violencia" => $violencia, "robo" => $robo, "altas" => $altas, "bajas" => $bajas, "hombres" => $hombres, "mujeres" => $mujeres, ];
	}

	public function getFotoProfesores(Request $request)
	{
		$sg=$request->idsg;
		$id=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')->where('id_usuario','=',$id)->first();
		$profesor=DB::table('profesor as p')->select('p.nombreProf','p.apepat','p.apemat','p.correo','p.telefono','p.celular','users.imagen','sg.idSubgrupo')
		->join('users','users.id','=','p.iduser')
		->join('subgrupo as sg','sg.idProfesor','=','p.idprofesor')
		->where('sg.idSubgrupo','=',$sg)
		->groupBy('p.idprofesor')
		->get();
		return $profesor;
	}
	
	public function getCalifCount(Request $request)
	{
		$sg=$request->idsg;
		$id=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')->where('id_usuario','=',$id)->first();
		$calif=DB::table('calif')->select('calif.idcalif','calif.idSubgrupo','calif.idPeriodo','calif.calif', 'calif.fecha','grupo.idgrupo','alumno.idinstitucion','alumno.idAlumno','alumno.nombreAlumno','alumno.apepat','alumno.apemat')
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('grupo.idInstitucion','=',$inst->idInstitucion)
		->where('calif.idSubgrupo','=',$sg)
		->where('subgrupo.estatus','=',1)
		->groupBy('idAlumno')
		->get();
		return $calif;
	}

	public function getCalifAprobCount(Request $request)
	{
		$sg=$request->idsg;
		$id=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')->where('id_usuario','=',$id)->first();
		$calif=DB::table('calif')->select('calif.idcalif','calif.idSubgrupo','calif.idPeriodo','calif.calif', 'calif.fecha','grupo.idgrupo','alumno.idAlumno','alumno.nombreAlumno','alumno.apepat','alumno.apemat')
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('grupo.idInstitucion','=',$inst->idInstitucion)
		->where('calif.idSubgrupo','=',$sg)
		->where('subgrupo.estatus','=',1)
		->where('calif.calif','>','6')
		->groupBy('idAlumno')
		->get();
		return $calif;
	}

	public function getMaterias(Request $request)
	{
		$id=Auth::user()->id;
		$materia=DB::table('materia')->select('materia.idMateria','materia.idInstitucion','materia.nombreMateria')
		->groupBy('idMateria')
		->get();
		return $materia;
	}

	public function getPromedioGrupo(Request $request)
	{
		$sg=$request->idsg;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','calif.calif','calif.idPeriodo','grupo.grado','grupo.grupo',DB::raw('AVG(calif.calif) as promedio'))
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('subgrupo','subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('calif.idSubgrupo','=',$sg)
		->groupBy('idAlumno')
		->get();
		return $calif;
	}

	public function getPromedioAlumnoSobresaliente(Request $request)
	{
		$sg=$request->idsg;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.nombreAlumno','users.imagen as imagen', 'alumno.apepat','alumno.apemat',DB::raw('AVG(calif.calif) as promedio'))
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('users','users.id','=','alumno.iduser')
		->where('calif.idSubgrupo','=',$sg)
		->groupBy('calif.idAlumno')
		->orderBy('promedio','desc')
		->take(3)
		->get();

		return $calif;
	}


	public function getPromedioAlumnoRegular(Request $request)
	{
		$sg=$request->idsg;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','users.imagen as imagen','alumno.apemat',DB::raw('AVG(calif.calif) as promedio'))
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('users','users.id','=','alumno.iduser')
		->where('calif.idSubgrupo','=',$sg)
		->groupBy('calif.idAlumno')
		->orderBy('promedio','asc')
		->take(3)
		->get();

		return $calif;
	}

	public function graficasCalif(Request $request)
	{
		$user=Auth::user()->id;
		$tipo=Auth::user()->tipo_user;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$user)->first();
        $Qsubgrupo=DB::table('subgrupo')
		->select('subgrupo.idSubgrupo','materia.idMateria','materia.nombreMateria','grupo.grado','grupo.grupo','grupo.idInstitucion')
		->join('ciclo','ciclo.idCiclo','=','subgrupo.idCiclo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('grupo','grupo.idGrupo','=','subgrupo.idGrupo')
		->where('ciclo.status','=',1)
		->where('subgrupo.idSubgrupo','=',$request->idsg)
		->first();

		if($Qsubgrupo)
		{
			$sg=$Qsubgrupo->idSubgrupo;
			$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
			$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

			$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
			->leftjoin('calif','calif.idPeriodo','=','periodo.idPeriodo')
			->where('periodo.idNivel','=',$grupo->idNivel)
			->where('calif.idSubgrupo','=',$sg)
			->groupBy('periodo.idPeriodo')
			->get();

			$califs = null;
			foreach ($periodo as $p) {
				$calPer=DB::table('calif')->select('calif')
				->where('idSubgrupo','=',$sg)
				->where('idPeriodo','=',$p->idPeriodo)
				->get();
				$prom=0;
				$cont=1;
				foreach ($calPer as $c) {
					$cont++;
					$prom=$prom+$c->calif;
				}
				
				$prom=$prom/$cont;

				$califs[] = [$p->nombrePeriodo];
				$barra[]=round($prom,2);


			}
			$barra[]=0;
			if($califs == null)
				$califs="";

			return ["califs"=>$califs, "barra"=>$barra, "sg"=>$Qsubgrupo];
		}
	}

	public function create()
	{
		/*$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asistencia=DB::table('asistencia')->select('asistencia.asistencia','asistencia.idAlumno', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->groupBy('idAlumno')
		->get();
		return view('Profesor.graficas.create',["asistencia"=>$asistencia]);*/
		$user=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion');
		$Qsubgrupo=DB::table('subgrupo')
		->select('subgrupo.idSubgrupo','materia.nombreMateria','grupo.grado','grupo.grupo')
		->join('ciclo','ciclo.idCiclo','=','subgrupo.idCiclo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('grupo','grupo.idGrupo','=','subgrupo.idGrupo')
		->where('ciclo.status','=',1)
		->first();

		$sg=$Qsubgrupo->idSubgrupo;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->leftjoin('asistencia','asistencia.idPeriodo','=','periodo.idPeriodo')
		->where('periodo.idNivel','=',$grupo->idNivel)
		->where('asistencia.idSubgrupo','=',$sg)
		->groupBy('periodo.idPeriodo')
		->get();
		
		foreach ($periodo as $p) {
			$asisPer=DB::table('asistencia')->select('asistencia')
			->where('idSubgrupo','=',$sg)
			->where('idPeriodo','=',$p->idPeriodo)
			->get();
			$prom=0;
			$cont=1;
			foreach ($asisPer as $as) {
				$cont++;
				$prom=$as->asistencia;
			}
			$prom=$cont;
			$asis[]=[$p->nombrePeriodo,$prom];

		}
		
		return view('Admin.graficas.create',["asis"=>$asis,"sg"=>$Qsubgrupo]);
}

public function TotalAsistencias(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=DB::table('subgrupo')->select('idSubgrupo')->get();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.estatus','=',1)
		->get();
		return $asis;
	}

	public function getMayorAsistencia(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','=','A')
		->get();

		//SELECT idAlumno, count(CASE WHEN asistencia = 'A' THEN 1 END) as A, count(CASE WHEN asistencia = 'F' THEN 1 END) as F, count(CASE WHEN asistencia = 'R' THEN 1 END) as R FROM asistencia WHERE idSubgrupo = 1 GROUP BY idAlumno
		return $asis;
	}

		public function getRetardo(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','=','R')
		->get();
		return $asis;
	}

		public function getFalta(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','=','F')
		->get();
		return $asis;
	}

	public function menuAdmin(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
        $avisos=DB::table('avisos')->select('avisos.fecha')->where('avisos.idInstitucion','=',$inst->idInstitucion)->where('avisos.estatus','=',0)->where('avisos.fecha','>=',date('Y-m-d'))->count();
        $eventos=DB::table('calendario')->select('calendario.inicio')->where('calendario.idInstitucion','=',$inst->idInstitucion)->where('calendario.status','=','1')->where('calendario.inicio','>=',date('Y-m-d'))->count();
		return (["sinver"=>$sinver, "avisos"=>$avisos, "eventos"=>$eventos]);
	}

}
