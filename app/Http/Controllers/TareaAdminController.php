<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Tarea;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TareaFormRequest;
use DB;
use Auth;

class TareaAdminController extends Controller
{
    
     public function __construct()
	{
		
	}
	
	
	public function index(Request $request)
	{
		$id=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$id)->first();
	    $tareas=DB::table('tareas')->select('tareas.idTareas','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tareas.titulo','tareas.descripcion','tareas.anexo','tareas.fechaentrega')
		->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->where('tareas.condicion','=','1')
		->where('grupo.idInstitucion','=',$inst->idInstitucion)
		->orderBy('tareas.idTareas','desc')
		->paginate(15);
		return view("Admin.tareas.index",["tareas"=>$tareas]);

	
	}
	public function create()
	{
		return view("Admin.tareas.create");
	}
	public function store(TareaFormRequest $request)
	{
		
	}
	
	
	public function show($id)
	{
		$tarea=DB::table('tareas')->select('tareas.idTareas','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tareas.titulo','tareas.descripcion','tareas.anexo','tareas.fechaentrega')
		->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->where('tareas.condicion','=','1')
    ->where('tareas.idTareas',$id)
    ->first();
		return view("Admin.tareas.show", ["tareas"=>$tarea]);
	
	}
	
	
	public function edit($id)
	{
		return view("Admin.tareas.tareasEdit",["tareas"=>Tarea::findOrFail($id)]);
		
	
	}
	public function update(TareaFormRequest $request,$id)
	{
	
	}
	public function destroy($id)
	{
		$tareas=Tarea::findOrFail($id);
		$tareas->condicion='0';
		$tareas->update();
		return Redirect::to('/Admin/tareas');
	
	}
	
}
