<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Upload;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException; 
use DB;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UploadsController extends Controller
{

  //Metodos para subir archivo ----------------------------------------------------------------------
   public function getIndex()
   {
   		return view("admin/adminUsuarios");
   }


//esta funcion la llamo desde la vista
    public function postSave()
    { 
      $file=Input::file('file');
      return $this->subirArchivo($file);
    }


//descargar
    public function descargarPlantilla()
    {
   return response()->download('/home/myappc5/www.appycollege.com/archivos-excel/PLANTILLA.xlsx');
    }


/*
   public function postSave()
   {
    /*
   		if(!\Input::file("file"))
    		{
          return redirect('admin/adminUsuarios')->with('error-message', 'Tienes que subir un archivo');
    		}
 
      $mime = \Input::file('file')->getMimeType();
      $extension = strtolower(\Input::file('file')->getClientOriginalExtension());
      $fileName = uniqid().'.'.$extension;
      $path = "files_uploaded";

      switch ($mime) 
      {
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        if (\Request::file('file')->isValid())
          {  
            if(Input::hasFile('file'))
              {
                  $paths = Input::file('file');
                  $data = Excel::load($paths, function($reader) {})->get();
                  
                  if(!empty($data) && $data->count())
                      {
                        foreach ($data as $key => $value) 
                        {                          
                          $array =  (array) $value;                  
                          $insert[] = [
                                       'name'     => $value->name, 
                                       'email'    => $value->email,
                                       'password' => bcrypt($value->password), 
                                       'tipo_user'=> $value->tipo_user 
                                     ];
                        }

                        $filtrado=array_filter($insert, function($var){return !is_null($var);});
                        //dump($value->name);     

                        $r = DB::table('users')->pluck('email');
/*


                        foreach ($r as $r) 
                        {
                          if($r!==$value->email)
                          {
                            
                          }
                          else
                          {
                            return redirect('admin/adminUsuarios')->with('error-message', 'Registro repetido');
                          }
                        }



                        

                            if(!empty($filtrado))
                        {
                          DB::table('users')->insert($filtrado);
                      }
              }
               \Request::file('file')->move($path, $fileName);
               $upload = new Upload();
               $upload->filename = $fileName;

               if($upload->save())
                    {
                      return redirect('admin/adminUsuarios')->with('success-message', 'El archivo ha sido subido');
                    }
                else
                    {
                        \File::delete($path."/".$fileName);
                        return redirect('admin/adminUsuarios')->with('error-message', 'An error ocurred saving data into database');
                    }
          }
                break;

                case "application/vnd.ms-excel":
                    if (\Request::file('file')->isValid())
                      {
                           \Request::file('file')->move($path, $fileName);
                           $upload = new Upload();
                           $upload->filename = $fileName;
                            
                           if($upload->save())
                              {
                                
                                return redirect('admin/adminUsuarios')->with('success-message', 'El archivo ha sido subido');
                              } 
                      else
                        {
                           \File::delete($path."/".$fileName);
                          return redirect('admin/adminUsuarios')->with('error-message', 'An error ocurred saving data into database');
                        }
                      }
                break;
              
                default: 
                    return redirect('admin/adminUsuarios')->with('error-message', 'La extension del archivo es incorrecta');
                break;
      }
   }
}
*/

/*
public function leerArchivo(file $archivo)
{
          $paths = Input::file($archivo);
          $data = Excel::load($paths, function($reader) {})->get();
                  
          if(!empty($data) && $data->count())
             {
                foreach ($data as $key => $value) 
                {                          
                  $array =  (array) $value;                  
                  $insert[] = [
                                'name'     => $value->name, 
                                'email'    => $value->email,
                                'password' => bcrypt($value->password), 
                                'tipo_user'=> $value->tipo_user 
                              ];
                }

                $filtrado=array_filter($insert, function($var){return !is_null($var);});

                if(!empty($filtrado))
                  {
                    DB::table('users')->insert($filtrado);
                  }
              }
}
*/
public function subirArchivo($archivo)
{
    if($archivo!=true)
      {
        return redirect('admin/adminUsuarios')->with('error-message', 'Debes subir un archivo');
      }
      $mime      = $archivo->getMimeType();
      $extension = strtolower($archivo->getClientOriginalExtension());
      $fileName  = Carbon::now()->micro.'.'.$extension;
      $path      = "archivos-excel";

      switch ($mime) 
      {
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        if ($archivo->isValid())
          {  
            $archivo->move($path, $fileName);
            $upload = new Upload();
            $upload->filename = $fileName;

            if($upload->save())
              {        
                return $this->leerArchivo("archivos-excel/".$fileName);
              }
            else
              {
              \File::delete($path."/".$fileName);
               return redirect('admin/adminUsuarios')->with('error-message', 'An error ocurred saving data into database');
              }
            }
        break;

        case "application/vnd.ms-excel":
        if ($archivo->isValid())
          {
            $archivo->move($path, $fileName);
            $upload = new Upload();
            $upload->filename = $fileName;
                            
            if($upload->save())
              {
                return $this->leerArchivo("archivos-excel/".$fileName);
              } 
            else
               {
                  \File::delete($path."/".$fileName);
                  return redirect('admin/adminUsuarios')->with('error-message', 'An error ocurred saving data into database');
                }
          }
       break;            
    
        default: 
                return redirect('admin/adminUsuarios')->with('error-message', 'La extension del archivo es incorrecta');
        break;
          }
      }

public function leerArchivo($ruta)
  {
    $data = Excel::load($ruta, function($reader) {})->get();
    if(!empty($data) && $data->count())
     {
      foreach ($data as $key => $value) 
        {         
            
        $headerRow = $data->first()->keys()->toArray();   
      if (count($headerRow)!=8)
      {
        return redirect('admin/adminUsuarios')->with('error-message', 'El archivo no mantiene el formato de la plantilla');
      }
      else
      {
          if($headerRow[0]!='nombre'
          &&$headerRow[1]!='apellidopaterno' 
          && $headerRow[2]!='apellidomaterno' 
          && $headerRow[3]!='correo' 
          && $headerRow[4]!='telefono' 
          && $headerRow[5]!='celular' 
          && $headerRow[6]!='password' 
          && $headerRow[7]!='matricula')
        {
          return redirect('admin/adminUsuarios')->with('error-message', 'El archivo no mantiene el formato de la plantilla');
        }
      }
        
          $array =  (array) $value;                  
          $user[] = [
                       'name'     => $value->nombre, 
                       'email'    => $value->correo,
                       'password' => bcrypt($value->password), 
                       'tipo_user'=> "3", 
                       'matricula'=> $value->matricula,
                        ];
           $correo = DB::table('users')->where('email', '=', $value->correo)->get();
           if(count($correo) > 0)
           {
             return redirect('admin/adminUsuarios')->with('error-message', 'El correo '.$value->correo.' ya existe');
           }
           else
           {
             $filtrado=array_filter($user, function($var){return !is_null($var);});
             //DB::table('users')->insert($filtrado);
             $this->llenarUser($ruta);
             return redirect('admin/adminUsuarios')->with('success-message', 'Registros almacenados');
           }
         }
      }
      
      else
      {
        return redirect('admin/adminUsuarios')->with('error-message', 'El archivo esta vacio');
      }
      
    }

public function llenarUser($ruta)
    {
      $data = Excel::load($ruta, function($reader) {})->get();
      if(!empty($data) && $data->count())
       {
         foreach ($data as $key => $value) 
          {                          
            $array =  (array) $value;                  
            $user = [
                        'name'     => $value->nombre, 
                        'email'    => $value->correo,
                        'password' => bcrypt($value->contraseña), 
                        'tipo_user'=> "3" ,
                        'matricula'=> $value->matricula,
                        ];

            $profesor = [
                            'nombreprof'     => $value->nombre,
                            'apepat'         => $value->apellidopaterno,
                            'apemat'         => $value->apellidomaterno,
                            'correo'         => $value->correo,
                            'telefono'       => $value->telefono,
                            'celular'        => $value->celular,
                            'iduser'         => null,
                            'id_institucion' => null,
                            'matricula'      => $value->matricula,  
                         ];
           $correo = DB::table('users')->where('email', '=', $value->correo)->get();
           if(count($correo) > 0)
            {
              return redirect('admin/adminUsuarios')->with('error-message', 'El correo '.$value->correo.' ya existe en la BD');
            }
           else
            {
              //$filtrado=array_filter($user, function($var){return !is_null($var);});
              DB::table('users')->insert($user);
              //funcion pa llenar prof
              $idProf = DB::table('users')->where('email', '=', $value->correo)->pluck('id')->first();
              //$this->llenarProfesor($idProf,$ruta);
              $profesor['idUser'] = $idProf;
              $this->llenarProfesor($profesor);
            }
          }
        }
        $this->borrarArchivo($ruta);
    }






public function llenarProfesor($profesor)
   {
          $id=(Auth::user()->id);
          $profesor['id_institucion'] = DB::table('institucion')->where('id_usuario', '=', $id)->pluck('idInstitucion')->first();
          /*
          $data = Excel::load($ruta, function($reader) {})->get();
          if(!empty($data) && $data->count())
            {
                foreach($data as $key => $value)
                  {
                    $array = (array) $value;
                    $profesor[] = [
                                      'nombreprof'     => $value->nombre,
                                      'apepat'         => $value->apellidopaterno,
                                      'apemat'         => $value->apellidomaterno,
                                      'correo'         => $value->correo,
                                      'telefono'       => $value->telefono,
                                      'celular'        => $value->celular,
                                      'iduser'         => $idUser,
                                      'id_institucion' => $idInstitucion,
                                   ];
                  }
            } 
          */
          $filtrado=array_filter($profesor, function($var){return !is_null($var);});
          DB::table('profesor')->insert($filtrado);              
    }

public function borrarArchivo($ruta)
{ 
  \File::delete($ruta);
}


}

/* flujo de datos:

1) vista
2) subir archivo 
3) leer archivo 
4) llenar tabla usuario
5) llenar tabla profesor
6) eliminar archivo 
7) vista */