<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EncuestasFormRequest;
use DB;
use Auth;

class ChatTutorController extends Controller
{
    public function __construct()
	{
		
	}

	

	public function index(Request $request)
	{
		$a=session('idTutelado','');
		$sg=null;
		$id=Auth::user()->id;
		$tutor=DB::table('tutor')->where('iduser','=',$id)->first();
        if($a=='')
        {
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }
        if($a!=0)
        {
            $sg=DB::table('subgrupo as sg')->select('sg.idGrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tutor.idtutor','tutor.iduser','tutor.nombreTutor','tutor.apepat','tutor.apemat','profesor.iduser as profesor','profesor.nombreprof','profesor.apepat as profapepat','profesor.apemat as profapemat','users.imagen')
                    ->join('profesor','profesor.idprofesor','=','sg.idProfesor')
                    ->join('users','users.id','=','profesor.iduser')
                    //->join('chat_p_t as chat','chat.idProfesor','=','profesor.iduser')
                    ->join('grupo','grupo.idgrupo','=','sg.idGrupo')
                    ->join('materia','materia.idMateria','=','sg.idMateria')
                    ->join('niveles','niveles.idNivel','=','grupo.idNivel')
                    ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
                    ->join('tutoralumno as ta','ta.idalumno','=','alumno.idAlumno')
                    ->join('tutor','tutor.idtutor','=','ta.idtutor')
                    ->where('alumno.idAlumno','=',$a)
                    //->where('chat.idTutor','=',$tutor->iduser)
                    ->groupBy('profesor.iduser')
                    //->orderBy('grupo.grupo')
                    ->get();
        }
        return view("Tutor.chats.index",["sg"=>$sg, "tutor"=>$tutor]);
        
        
	}

	public function getChatsTutor(Request $request){
        $iduser=Auth::user()->id;
        $tutor=DB::table('tutor')->where('iduser','=',$iduser)->first();
        $sg=$request->id;
        $chats=DB::table('chat_p_t')->where('idTutor','=',$tutor->iduser)->where('idProfesor','=',$request->idprofselec)
        ->update(['ultimoMsj'=>$request->get('mensaje'),'timestamp'=>date("Y-m-d H:i:s")]);
        echo($request->idprofselec.'<br>');
        echo($request->get('mensaje'));
        //$chats->save();
        //return Redirect::to('/profesor/chats/create');
    }
	
	public function store(Request $request)
	{
	
	}
	
	
	public function show($id)
	{

	}
	
	public function destroy($id)
	{
		
	
	}
}
