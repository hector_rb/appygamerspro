<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class avisosAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $iduser=Auth::user()->id;
        $inst= DB::table('alumno')->select('idinstitucion')->where('iduser','=',$iduser)->first();
        $id=$inst->idinstitucion;
        $avisos=DB::table('avisos')
        ->where('estatus','=',0)
        ->where([['idInstitucion','=',$id],['destinatario','=',4]])
        ->orWhere([['idInstitucion','=',$id],['destinatario','=',1]])
        ->orderBy('fecha','desc')
        ->paginate(10);


        return view('Alumno.avisos.index',['avisos'=>$avisos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $iduser=Auth::id();
        $inst= DB::table('alumno')->select('idinstitucion')->where('iduser','=',$iduser)->first();
        $idins=$inst->idinstitucion;
        $aviso=DB::table('avisos')
        ->where('idInstitucion','=',$idins)
        ->where('idAviso','=',$id)
        ->first();
        return view('Alumno.avisos.show',['aviso'=>$aviso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
