<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\tutor;
use Illuminate\Support\Facades\Redirect;

class perfilTutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ima=Auth::user()->imagen;
        $informacion=DB::table('tutor')
        ->select('idtutor','nombreTutor','apepat','apemat','genero', 'fechanac','telefono','celular')
        ->where('iduser','=',Auth::user()->id)
        ->first();
        return view('Tutor/profileTut',['img'=>$ima,'info'=>$informacion]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function saveImage(Request $request)
    {     
        $id=Auth::user()->id;
        $user=User::findOrFail($id);
        $png_url = date('Ymd').time().$id.".jpg";
        $path = public_path().'/img/profile_pics/' . $png_url;

        $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
        if($upload)
        {
            $user->imagen=$png_url;
            $user->update();
            return $png_url;
        }
        


    }

    public function getTutelados(Request $request)
    {   
        $a=$request->session()->get('idTutelado');
        $request->session()->put('idTutelado', $a);
        $id=Auth::user()->id;
        $alum=DB::table('alumno')->where('idtutor','=',$id)->get();
        if($a=='')
        {
            $request->session()->put('alumno', $alum[0]->idAlumno);
            $a=$request->session()->get('idTutelado');
        }
        return $alum;
    }

    public function setTutelado(Request $request)
    {
        $request->session()->put('idTutelado', $request->idalumno);
        
        return 'success';
    }

    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $save=Tutor::findOrFail($id);
        $save->nombreTutor=$request->nombre;
        $save->apepat=$request->apepat;
        $save->apemat=$request->apemat;
        $save->genero=$request->genero;
        $save->fechanac=$request->fechanac;
        $save->telefono=$request->tel;
        $save->celular=$request->cel;
        $save->save();

        $idu=Auth::user()->id;
        $user=User::findOrFail($idu);
        $user->name=$id=$request->nombre;
        $user->save();
        return Redirect::to('/tutor/perfilTut');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
