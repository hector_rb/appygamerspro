<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class eventosAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id=Auth::user()->id;
        $alum=DB::table('alumno')->select('idgrupo')->where('iduser','=',$id)->first();
        $gpo=DB::table('grupo')->select('idInstitucion','idNivel','grado','grupo')->where('idgrupo','=',$alum->idgrupo)->first();
        
        if($gpo->idNivel==1)
        {
            $niv='K';
        }
        if($gpo->idNivel==2)
        {
            $niv='P';
        }
        if($gpo->idNivel==3)
        {
            $niv='S';
        }
        if($gpo->idNivel==4)
        {
            $niv='B';
        }

        $query = DB::table('calendario')->where('idInstitucion','=',$gpo->idInstitucion)
        ->where('status','=',1)
        ->where('tipoDestinatario','=',1)->get();
        
        $query2 = DB::table('calendario')
        ->where('idInstitucion','=',$gpo->idInstitucion)
        ->where('tipoDestinatario','=',2)
        ->where('id','=',$gpo->idNivel)
        ->where('status','=',1)->get();

         $query3 = DB::table('calendario')
        ->where('idInstitucion','=',$gpo->idInstitucion)
        ->where('tipoDestinatario','=',3)
        ->where('id','=',$gpo->grado.$niv)
        ->where('status','=',1)->get();   

        $query4 = DB::table('calendario')
        ->where('idInstitucion','=',$gpo->idInstitucion)
        ->where('tipoDestinatario','=',4)
        ->where('id','=',$alum->idgrupo)
        ->where('status','=',1)->get();  

        $array=[];
        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $array[] = ['id'=>$q->idCalendario, 'title'=>$q->titulo, 'description' => $q->desc, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        if(count($query2)>0)
        {
            foreach ($query2 as $values => $q ) {
                $array[] = ['id'=>$q->idCalendario, 'title'=>$q->titulo, 'description' => $q->desc, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        if(count($query3)>0)
        {
            foreach ($query3 as $values => $q ) {
                $array[] = ['id'=>$q->idCalendario, 'title'=>$q->titulo, 'description' => $q->desc, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        if(count($query4)>0)
        {
            foreach ($query4 as $values => $q ) {
                $array[] = ['id'=>$q->idCalendario, 'title'=>$q->titulo, 'description' => $q->desc, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }

        return view('Alumno.eventos.index', ['array'=>$array]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
