<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class avisosTutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }

        $iduser=$a;
        /*$inst= DB::table('alumno')->select('idinstitucion')->where('idalumno','=',$iduser)->first();
        $idinst=$inst->idinstitucion;*/
        $grupo=DB::table('alumno')->select('alumno.idgrupo')
        ->where('idAlumno','=',$iduser)
        ->first();

        
        if (count ($grupo)>0)
        {
        $avisos=DB::table('avisos')->select('avisos.*','grupo.grado','grupo.grupo')
        ->join('alumno', function ($join) use ($iduser)  {
                $join->on('alumno.idinstitucion','=','avisos.idInstitucion')
                ->where('alumno.idalumno','=',$iduser);

        })
        ->leftJoin('grupo','grupo.idgrupo','=','avisos.idgrupo')
        ->where('estatus','=',0)
        ->where('avisos.destinatario','=',2)
        ->orWhere('avisos.destinatario','=',4)
        ->orWhere('avisos.destinatario','=',1)
        ->orWhere('avisos.destinatario','=',5)
        ->orWhere('avisos.destinatario','=',6)
        ->orwhere('avisos.idgrupo','=',$grupo->idgrupo)
        ->orderBy('fecha','desc')
        //->paginate(10)
        //dd($grupo);
        ->get();
        }
        else
        {
            $avisos=null;
            
            
        }
        
        return view('Tutor.avisos.index',['avisos'=>$avisos]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }
        $iduser=$a;        
        /*$inst= DB::table('profesor')->select('id_institucion')->where('iduser','=',$iduser)->first();
        $idins=$inst->id_institucion;*/
        $aviso=DB::table('avisos')
        ->join('alumno','alumno.idinstitucion','=','avisos.idInstitucion')
        ->where('alumno.idalumno','=',$iduser)
        ->where('avisos.idAviso','=',$id)
        ->first();
        return view('Tutor.avisos.show',['aviso'=>$aviso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
