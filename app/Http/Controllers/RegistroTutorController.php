<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\User;
use App\Tutor;

class RegistroTutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.RegistroTutor');
    }

    public function Registrar(Request $request)
    {
    	$input=$request->all();
    	$user= new User();
    	$user->fill([
    		'name'      =>$input['name'],
    		'email'     =>$input['email'],
    		'password'  =>bcrypt($input['password']),
    		'tipo_user' =>"2",
    	]);
    	$user->save();

    	$tutor = new Tutor();
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
    	$tutor->fill([
    	'idtutor'      =>$user->id,
        'idInstitucion'=>$id,
    	'correo'       =>$input['email'],
    	'nombreTutor'  =>$input['name'], 
    	'apepat'       =>$input['apepat'],
    	'apemat'       =>$input['apemat'],
    	'genero'       =>$input['genero'],
    	'fechanac'     =>$input['fechanac'],
    	'telefono'     =>$input['tel'],
    	'celular'      =>$input['cel'],
    	]);
    	$tutor->save();
        return view('Admin.RegistroTutor');
    }
}