<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\MensajesR;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\MensajesRFormRequest;
use DB;
use Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class MensajesRController extends Controller
{
    public function __construct()
	{
		
	}
	
	
	public function index(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
		$mensajes=DB::table('mensajes')->select('mensajes.id','mensajes.Asunto','mensajes.Mensaje','mensajes.TipoDestinatario','mensajes.idDestinatario','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.Visto','mensajes.idInstitucion','mensajes.created_at as fecha','mensajes.updated_at','mensajes.Eliminado','destinatarios.TipoDestinatario as destinatario','users.name as usuarios')
		->join('destinatarios','destinatarios.id','=','mensajes.TipoEmisor')
		->join('users','users.id','=','mensajes.IdEmisor')
		->where('mensajes.idInstitucion','=',$id)
		->where('mensajes.idDestinatario','=',$iduser)
		->where('mensajes.Eliminado','=','0')
		->orderBy('mensajes.id','desc')
		//->paginate(10);
		->get();
		//->paginate(7);
		return view("Admin.mensajesR.index", ["mensajes"=>$mensajes, "sinver"=>$sinver]);

	
	}
	public function create(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
        $admin=DB::table('institucion')
        ->select('id_usuario')
        ->where('idInstitucion',$id)
        ->get()
        ->first();
        $variable = DB::table('users')->select('name as Nombre','id','id as iduser')
        	->where('id',$admin->id_usuario)->get();
        $variable2=DB::table('profesor')->select('profesor.nombreprof as Nombre','profesor.idprofesor as id','users.name','users.id as iduser','profesor.iduser','users.imagen')
        	->join('users','users.id','=','profesor.iduser')
            ->where('profesor.id_institucion',$id)->get();
        $variable3=DB::table('alumno')->select('alumno.nombreAlumno as Nombre','alumno.idAlumno as id','users.name','alumno.iduser','grupo.grado','grupo.grupo','users.id as iduser')
        	->join('users','users.id','=','alumno.iduser')
        	->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
            ->where('alumno.idinstitucion',$id)->get();
        $variable4=DB::table('tutoralumno as ta')->select('tutor.nombreTutor as Nombre','tutor.idtutor as id','users.name','tutor.iduser','users.id as iduser')
        	->join('tutor','tutor.idtutor','=','ta.idtutor')
        	->join('alumno','alumno.idAlumno','=','ta.idalumno')
        	->join('users','users.id','=','tutor.iduser')
        	->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
            ->where('alumno.idinstitucion',$id)->get();
        //echo($prof);
		return view("Admin.mensajesR.create",["variable"=>$variable, "variable2"=>$variable2, "variable3"=>$variable3, "variable4"=>$variable4,"sinver"=>$sinver]);
	}
	public function store(MensajesRFormRequest $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        if ($request->hasFile('anexo')){
				$file=Input::file('anexo');
	        	$file->move(public_path().'/archivo/mensajes/',$file->getClientOriginalName());
		    	
		    }
        foreach ($request->IdDestinatario as $key) {
        
	        $mensajesR=new MensajesR;		
			$mensajesR->Asunto=$request->get('Asunto');
			$mensajesR->Mensaje=$request->get('Mensaje');
			$op=explode(",",$key);
			$mensajesR->TipoDestinatario=$op[0];
			$mensajesR->IdDestinatario=$op[1];
			$mensajesR->IdEmisor=$iduser;
			$mensajesR->TipoEmisor=1;
			$mensajesR->Visto=0;
			$mensajesR->idInstitucion=$id;
			$mensajesR->created_at=date("Y-m-d H:i:s");
			$mensajesR->updated_at=date("Y-m-d H:i:s");
			$mensajesR->Eliminado=0;
			if($request->hasFile('anexo')){
			$mensajesR->anexo=$file->getClientOriginalName();	
			}
		   if($op[0]==4)
		    {
		        $this->avisarInsignia($op[1]);
		    }
		    if($op[0]==3)
		    {
		        $this->avisarProfe($op[1]);
		    }
		    $mensajesR->save();
		    //echo($mensajesR);
		 }   
			
        	
		return Redirect::to('/admin/mensajesR');
	}
	
	
	public function show($id)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Visto' => 1]);
        $mensajes=DB::table('mensajes')->select('mensajes.Asunto','mensajes.Mensaje','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.anexo','mensajes.created_at as fecha','users.name as usuarios','destinatarios.TipoDestinatario as destinatario')
        ->join('users','users.id','=','mensajes.IdEmisor')
        ->join('destinatarios','destinatarios.id','=','mensajes.TipoEmisor')
        ->where('mensajes.id','=',$id)
        ->where('mensajes.idInstitucion','=',$i)
        ->get();
		return view("Admin.mensajesR.show",["mensajes"=>$mensajes, "sinver"=>$sinver]);

	}

	public function verEnviados(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
        $mensajes=DB::table('mensajes')->select('mensajes.Asunto','mensajes.Mensaje','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.anexo','mensajes.created_at as fecha','users.name as usuarios','destinatarios.TipoDestinatario as destinatario')
        ->join('users','users.id','=','mensajes.IdEmisor')
        ->join('destinatarios','destinatarios.id','=','mensajes.TipoEmisor')
        ->where('mensajes.id','=',$request->id)
        ->where('mensajes.idInstitucion','=',$i)
        ->get();
		return view("Admin.mensajesR.show",["mensajes"=>$mensajes, "sinver"=>$sinver]);

	}

	public function Enviado(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
		$mensajes=DB::table('mensajes')->select('mensajes.id','mensajes.Asunto','mensajes.Mensaje','mensajes.TipoDestinatario','mensajes.idDestinatario','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.Visto','mensajes.idInstitucion','mensajes.created_at as fecha','mensajes.updated_at','mensajes.Eliminado','destinatarios.TipoDestinatario as destinatario','users.name as usuarios')
		->join('destinatarios','destinatarios.id','=','mensajes.TipoDestinatario')
		->join('users','users.id','=','mensajes.IdDestinatario')
		->where('mensajes.idInstitucion','=',$id)
		->where('mensajes.idEmisor','=',$iduser)
		->where('mensajes.Eliminado','=','0')
		->orderBy('mensajes.id','desc')
		//->paginate(7);
		->get();
		return view ("Admin.mensajesR.enviados",["mensajes"=>$mensajes, "sinver"=>$sinver]);
	}

	public function papelera(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
		$mensajes=DB::table('mensajes')->select('mensajes.id','mensajes.Asunto','mensajes.Mensaje','mensajes.TipoDestinatario','mensajes.idDestinatario','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.Visto','mensajes.idInstitucion','mensajes.created_at as fecha','mensajes.updated_at','mensajes.Eliminado','destinatarios.TipoDestinatario as destinatario','users.name as usuarios')
		->join('destinatarios','destinatarios.id','=','mensajes.TipoEmisor')
		->join('users','users.id','=','mensajes.IdEmisor')
		->where('mensajes.idInstitucion','=',$id)
		//->where('mensajes.idDestinatario','=',$iduser)
		//->where('mensajes.idEmisor','=',$iduser)
		->where('mensajes.Eliminado','=','1')
		->orderBy('mensajes.id','desc')
		//->paginate(7);
		->get();
		/*$mensajes2=DB::table('mensajes')->select('mensajes.id','mensajes.Asunto','mensajes.Mensaje','mensajes.TipoDestinatario','mensajes.idDestinatario','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.Visto','mensajes.idInstitucion','mensajes.created_at as fecha','mensajes.updated_at','mensajes.Eliminado','destinatarios.TipoDestinatario as destinatario','users.name as usuarios')
		->join('destinatarios','destinatarios.id','=','mensajes.TipoDestinatario')
		->join('users','users.id','=','mensajes.IdDestinatario')
		->where('mensajes.idInstitucion','=',$id)
		->orwhere('mensajes.idDestinatario','=',$iduser)
		->where('mensajes.idEmisor','=',$iduser)
		->where('mensajes.Eliminado','=','1')
		->orderBy('mensajes.id','desc')
		->paginate(7);*/
		return view ("Admin.mensajesR.papelera",["mensajes"=>$mensajes]);
	}
	
	public function edit($id)
	{
		return view("");
		
	
	}
	public function update(MensajesRFormRequest $request,$id)
	{
	
	}
	public function destroy($id)
	{
		$mensajesR=MensajesR::findOrFail($id);
		$mensajesR->Eliminado='1';
		$mensajesR->update();
		return Redirect::to('/admin/mensajesR');
	
	}
	
	public function avisarInsignia($alumno)
	{
	    
	    $idalumno = DB::table("alumno")->select('alumno.idAlumno')
	    ->where('alumno.iduser',$alumno)
	    ->first();
	     $tutoralumno = DB::table("tutoralumno")->select('tutor.*', 'alumno.nombreAlumno')
                  ->join('tutor', 'tutoralumno.idtutor', '=', 'tutor.idtutor')
                  ->join('alumno', 'tutoralumno.idalumno', '=', 'alumno.idAlumno')
                  ->where('tutoralumno.idalumno', $idalumno->idAlumno)
                  ->first($alumno);
                  //dd($alumno);
                 // echo ($user->idAlumno);
                 //echo json_encode($tutoralumno);
                 // echo "<br><br>";
                 //dd($insignia);
                  if($tutoralumno){
                      
                       if($tutoralumno->tokenFCM!="0"){
                     
                             $this->sendNotification(
                             "Mensaje", 
                             $tutoralumno->nombreAlumno . " Ha recibido un mensaje nuevo", 
                            $tutoralumno->tokenFCM,
                            0,
                            $tutoralumno->dispositivo
                            );
                        }
                      
                  }
	}
	
		public function avisarProfe($profe)
	{
	    
	   
	     $profesor = DB::table("profesor")->select('profesor.*')
                  ->where('profesor.iduser', $profe)
                  ->first();
                  //dd($profe);
                 // echo ($user->idAlumno);
                 //echo json_encode($tutoralumno);
                 // echo "<br><br>";
                 //dd($profe);
                  if($profe){
                      
                       if($profesor->tokenFCM!="0"){
                     
                             $this->sendNotification(
                             "Mensaje", 
                             "Has recibido un mensaje nuevo", 
                            $profesor->tokenFCM,
                            0,
                            $profesor->dispositivo
                            );
                        }
                      
                  }
	}
	
		public function sendNotification($titulo, $mensaje, $token, $id=0, $dispositivo){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

    }
	
}
