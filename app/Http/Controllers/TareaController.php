<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Tarea;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TareaFormRequest;
use DB;
use Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


class TareaController extends Controller
{
     public function __construct()
	{
		
	}
	
	
	public function index(Request $request)
	{
		// if ($request)
		// {
		// 	$query=trim($request->get('searchText'));
		// 	$tareas=DB::table('tareas as t')
		// 	->join('materia as m','t.idmateria','=','m.idMateria')
		// 	->select('t.idTareas','t.idgrupo','t.fechaentrega','t.titulo','t.descripcion','t.anexo','m.nombreMateria as materia')
		// 	->where('t.titulo','LIKE','%'.$query.'%')
		// 	->where('t.condicion','=','1')
		// 	->orderBy('t.idTareas','desc')
		// 	->paginate(7);
		// 	return view("Profesor.tareas.index",["tareas"=>$tareas, "searchText"=>$query]);
		// }
		$id=Auth::user()->id;
		$query=trim($request->get('searchText'));
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$tareas=DB::table('tareas')->select('tareas.idTareas','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tareas.titulo','tareas.descripcion','tareas.anexo','tareas.fechaentrega')
		->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->where('materia.nombreMateria','LIKE','%'.$query.'%')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('tareas.condicion','=','1')
		->orderBy('tareas.fechaentrega','desc')
		->paginate(7);
		return view("Profesor.tareas.index",["tareas"=>$tareas, "searchText"=>$query]);

	
	}
	public function create()
	{
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();
		//$materia=DB::table('materia')->where('activo','=','1')->get();
		//$grupo=DB::table('grupo')->get();
		//echo($sg);
		return view("Profesor.tareas.create",["sg"=>$sg]);
	}
	public function store(TareaFormRequest $request)
	{
		$tareas=new Tarea;		
		$tareas->idSubgrupo=$request->get('idSubgrupo');
		$tareas->fechaentrega=$request->get('fechaentrega');
		$tareas->titulo=$request->get('titulo');
		$tareas->descripcion=$request->get('descripcion');
		if (Input::hasFile('anexo')){
         $file=Input::file('anexo');
         $file->move(public_path().'/anexo/tareas/',$file->getClientOriginalName());
            $tareas->anexo=$file->getClientOriginalName();
        }
		$tareas->save();
		$this->alumno($request);
		return Redirect::to('/profesor/tareas');
	}
	
	
	public function show($id)
	{

		$tarea=DB::table('tareas')->select('tareas.idTareas','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tareas.titulo','tareas.descripcion','tareas.anexo','tareas.fechaentrega')
		->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->where('tareas.condicion','=','1')
    ->where('tareas.idTareas',$id)
    ->first();

    //dd($tarea);
		return view("Profesor.tareas.show",["tareas"=>$tarea]);

	}
	
	
	public function edit($id)
	{
		return view("Profesor.tareas.tareasEdit",["tareas"=>Tarea::findOrFail($id)]);
		
	
	}
	public function update(TareaFormRequest $request,$id)
	{
		/*$tareas=Tarea::findOrFail($id);
		$tareas->idmateria=$request->get('idmateria');
		$tareas->idgrupo=$request->get('idgrupo');
		$tareas->titulo=$request->get('titulo');
		$tareas->descripcion=$request->get('descripcion');
		
		if (Input::hasFile('anexo')){
         $file=Input::file('anexo');
         $file->move(public_path().'/anexo/tareas/',$file->getClientOriginalName());
         $tareas->imagen=$file->getClientOriginalName();
        }
			
		$tareas->update();
		return Redirect::to('/profesor/tareas');*/
	
	}
	public function destroy($id)
	{
		$tareas=Tarea::findOrFail($id);
		$tareas->condicion='0';
		$tareas->update();
		return Redirect::to('/profesor/tareas');
	
	}
	
	
	public function alumno($subgrupo)
	{
		 $grupo=DB::table('subgrupo')
		 ->select('idGrupo')
		 ->where('idSubgrupo','=',$subgrupo->get('idSubgrupo'))
		 ->first();
        $alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','alumno.iduser', 'grupo.grado','grupo.grupo', 'grupo.idgrupo')
        ->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
        ->join('users','alumno.iduser','=','users.id')
        ->where('alumno.idgrupo','=',$grupo->idGrupo)
            ->orderBy('alumno.apepat', 'asc')
            ->orderBy('alumno.apemat', 'asc')
        ->get();
        
     
        foreach ($alum as $user) {
            
       $this->avisarTarea($user->idAlumno);
        
            
                   
       }

	
	}
	
	
	
	public function avisarTarea($alumno)
	{
	    
	     $tutoralumno = DB::table("tutoralumno")->select('tutor.*', 'alumno.nombreAlumno')
                  ->join('tutor', 'tutoralumno.idtutor', '=', 'tutor.idtutor')
                  ->join('alumno', 'tutoralumno.idalumno', '=', 'alumno.idAlumno')
                  ->where('tutoralumno.idalumno',$alumno)
                  ->first();
                 // echo ($user->idAlumno);
                 // echo json_encode($tutoralumno);
                 // echo "<br><br>";
                  if($tutoralumno){
                      
                       if($tutoralumno->tokenFCM!="0"){
                     
                             $this->sendNotification(
                             "Tarea", 
                            "Hay una nueva tarea", 
                            $tutoralumno->tokenFCM,
                            0,
                            $tutoralumno->dispositivo
                            );
                        }
                      
                  }
	}
	
	
	public function sendNotification($titulo, $mensaje, $token, $id=0, $dispositivo){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

         $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

    }
	
	
}
