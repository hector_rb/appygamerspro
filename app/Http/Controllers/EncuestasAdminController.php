<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Encuestas;
use App\Preguntas;
use App\Completar;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EncuestasFormRequest;
use DB;
use Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

use App\Services\PayUService\Exception;

class EncuestasAdminController extends Controller
{
    public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $encuesta=DB::table('encuesta')->select('encuesta.idEncuesta','encuesta.nombre','encuesta.descripcion','encuesta.fecha','encuesta.idInstitucion')
        ->where('encuesta.idInstitucion','=',$id)
        ->where('encuesta.estatus','=','1')
        ->OrderBy('encuesta.idEncuesta','desc')
        ->get();
		return view ("Admin.encuestas.index",["encuesta"=>$encuesta]);
	}

	public function create()
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();

        /*$grupo=DB::table('grupo')
        ->select('subgrupo.idGrupo','subgrupo.idCiclo','ciclo.nombreCiclo' ,'grupo.idNivel','niveles.nombreNivel' ,'grupo.grado','grupo.grupo')
        ->join('subgrupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('niveles','grupo.idNivel','=','niveles.idNivel')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->where('grupo.idInstitucion','=',$id)
        ->groupBy('subgrupo.idGrupo','subgrupo.idCiclo')->get();*/
		return view("Admin.encuestas.create",["niv"=>$niv]);
	}

	public function getGruposEncuestas(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->id;
		$grupo=DB::table('grupo')
        ->select('ciclo.nombreCiclo','ciclo.status' ,'grupo.idNivel','grupo.idgrupo','niveles.nombreNivel' ,'grupo.grado','grupo.grupo')
        ->join('subgrupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('niveles','grupo.idNivel','=','niveles.idNivel')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->where('grupo.idInstitucion','=',$id)
        ->where('niveles.idNivel','=',$idNivel)
        ->where('ciclo.status','=',1)
        ->groupBy('niveles.idNivel','grupo.grado')->get();
		return $grupo;
	}
	
	public function store(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;

		$encuestas=new Encuestas;		
		$encuestas->idAdmin=$id;
		$encuestas->idInstitucion=$id;
		$encuestas->nombre=$request->get('nombreEncuesta');
		$encuestas->descripcion=$request->get('Descripcion');
		$encuestas->fecha=date('Y-m-d');
		$encuestas->save();

		for ($n=1;$n<=$request->pre;$n++) {

			$preguntas=new Preguntas;
			$preguntas->idencuesta=$encuestas->idEncuesta;
			$preguntas->nombrePregunta=$request->get('selectMateria'. $n);	
			$preguntas->tipoRespuesta=$request->get('tipopregunta'.$n);
			if($request->ree == 1)
			{		
				$preguntas->opciones=$request->get('opcion_tipopregunta'.$n);
			}
			else
			{
				for($i=0;$i<5;$i++)
				{
					$preguntas->opciones=$preguntas->opciones.$request->get('opcion_tipopregunta'.$n)[$i].'¨';
				}
				
			}
			
			$preguntas->save();
		}
		
		
		foreach ($request->destinatario as $key) {
			switch ($key) {
				case 1:
					foreach ($request->selectGrupo as $gr) {
						
						$profe=DB::table('profesor')->select('profesor.idprofesor')
						->join('subgrupo','subgrupo.idProfesor','=','profesor.idprofesor')
						->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
						->join('niveles','niveles.idNivel','=','grupo.idNivel')
						->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
						->where('niveles.idNivel','=',$request->idSubGrupo)
						->where('grupo.idgrupo','=',$gr)
						->where('ciclo.status','=',1)
						->groupBy('profesor.idprofesor')
						->get();
						foreach ($profe as $id_profe) {
							$com= new Completar;
							$com->idEncuestado=$id_profe->idprofesor;
							$com->idEncuesta= $encuestas->idEncuesta;
							$com->numPreguntas=$n-1;
							$com->tipoEncuestado=1;
							$com->contestadas=0;
							$com->save();
							$this->avisarProfe($id_profe->idprofesor, $encuestas->idEncuesta);
						}
					}
					break;
				case 2:
					foreach ($request->selectGrupo as $gr) {
						
						$alumno=DB::table('alumno')->select('alumno.idAlumno')
						
						->join('grupo','grupo.idgrupo','=','alumno.idGrupo')
						->join('subgrupo','subgrupo.idGrupo','=','grupo.idgrupo')
						->join('niveles','niveles.idNivel','=','grupo.idNivel')
						->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
						->where('niveles.idNivel','=',$request->idSubGrupo)
						->where('grupo.idgrupo','=',$gr)
						->where('ciclo.status','=',1)
						->groupBy('alumno.idAlumno')
						->get();

						foreach ($alumno as $alu) {
							$com= new Completar;
							$com->idEncuestado=$alu->idAlumno;
							$com->idEncuesta= $encuestas->idEncuesta;
							$com->numPreguntas=$n-1;
							$com->tipoEncuestado=2;
							$com->contestadas=0;
							$com->save();
						}
					}
					break;
				case 3:
					foreach ($request->selectGrupo as $gr) {
						
						$tutor=DB::table('tutor')->select('tutor.idtutor')
						
						->join('tutoralumno','tutoralumno.idtutor','=','tutor.idtutor')
						->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
						->join('grupo','grupo.idgrupo','=','alumno.idGrupo')
						->join('subgrupo','subgrupo.idGrupo','=','grupo.idgrupo')
						->join('niveles','niveles.idNivel','=','grupo.idNivel')
						->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
						->where('niveles.idNivel','=',$request->idSubGrupo)
						->where('grupo.idgrupo','=',$gr)
						->where('ciclo.status','=',1)
						->groupBy('tutor.idtutor')
						->get();
						
						foreach ($tutor as $tu) {
							$com= new Completar;
							$com->idEncuestado=$tu->idtutor;
							$com->idEncuesta= $encuestas->idEncuesta;
							$com->numPreguntas=$n-1;
							$com->tipoEncuestado=3;
							$com->contestadas=0;
							$com->save();
							$this->avisarTutor($tu->idtutor, $encuestas->idEncuesta);
						}
					}
					break;
				default:
					
					break;
			}
		}

		
		return Redirect::to('/admin/encuestas/create');
	}
	
	
	public function show( $id)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $encuesta=DB::table('encuesta')->select('encuesta.idEncuesta','encuesta.nombre','encuesta.descripcion','encuesta.fecha','encuesta.idInstitucion')
        ->where('encuesta.idEncuesta','=',$id)
        ->where('encuesta.estatus','=','1')
        ->get();
        $tienc=DB::table('enc_completa')
        ->select('idEncuesta','tipoEncuestado')
        ->where('idEncuesta','=',$id)
        ->groupBy('tipoEncuestado')
        ->get();
        $preguntas=DB::table('enc_preguntas')
        ->select('idEncuesta','nombrePregunta','tipoRespuesta','opciones','idPregunta')
        ->where('idEncuesta','=',$id)
        ->orderBy('idPregunta','ASC')
        ->get();
        //echo($preguntas);
		return view ("Admin.encuestas.show",["encuesta"=>$encuesta,"tienc"=>$tienc, "preguntas"=>$preguntas, "idenc"=>$id,]);

	}
    public function getPreguntas(Request $request)
	{
	$preguntas=DB::table('enc_preguntas as preguntas')
        ->select('preguntas.idEncuesta','preguntas.nombrePregunta','preguntas.tipoRespuesta','preguntas.opciones','preguntas.idPregunta')
        ->where('preguntas.idEncuesta','=',$request->idEncuesta)
        ->groupBy('preguntas.nombrePregunta')
        ->get();
        return $preguntas;	
	}
	public function getContador(Request $request)
	{

	$tipo=DB::table('enc_preguntas as re')
			        ->select('idEncuesta','idPregunta','tipoRespuesta','opciones','nombrePregunta')
			        ->where('idPregunta','=',$request->idpre)
			        ->OrderBy('nombrePregunta','ASC')
			        ->get();
	$t=$tipo[0]->tipoRespuesta;
    switch ($t) {
    	case 1:
    		$Si=DB::table('enc_respuestas as re')
		        ->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',1)->count();
		    $No=DB::table('enc_respuestas as re')
		        ->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',2)->count();
		        //echo($request->idpre);
		    return ['tipo'=>$t,'Si'=>$Si,'No'=>$No, 'pregunta'=>$tipo[0]->nombrePregunta];
    	break;
    	case 2:
    		//$op=explode(",",$tipo[0]->opciones,5);
    		$opciones=DB::table('enc_preguntas as re')
    		->select('opciones')
	    		->where('idPregunta','=',$request->idpre)
	    		->get();
	    		//echo($opciones);
    		$primero=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',1)->count();
		    $segundo=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',2)->count();
		    $tercero=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',3)->count();
		    $cuarto=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',4)->count();
		    $quinto=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',5)->count();
    		/*$con=[null,null,null,null,null];
    		$i=0;
    		foreach ($op as $key) {
    			if($key!="")
    			{
    				$con[$i]=DB::table('enc_respuestas as re')
				        ->select('idEncuesta','idPregunta','respuesta')
				        ->where('idPregunta','=',$request->idpre)
				        ->where('respuesta','=',$i+1)->count();
    			}
    		}*/
			return ['tipo'=>$t,'opciones'=>$opciones,'primero'=>$primero, 'segundo'=>$segundo, 'tercero'=>$tercero, 'cuarto'=>$cuarto, 'quinto'=>$quinto,'pregunta'=>$tipo[0]->nombrePregunta];
    		break;
    	case 3:
    		$uno=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',1)->count();
		    $dos=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',2)->count();
		    $tres=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',3)->count();
		    $cuatro=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',4)->count();
		    $cinco=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',5)->count();
		    $seis=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',6)->count();
		    $siete=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',7)->count();
		    $ocho=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',8)->count();
		    $nueve=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',9)->count();
		    $diez=DB::table('enc_respuestas as re')
    		->select('idEncuesta','idPregunta','respuesta')
		        ->where('idPregunta','=',$request->idpre)
		        ->where('respuesta','=',10)->count();
		    return ['tipo'=>$t,'uno'=>$uno, 'dos'=>$dos, 'tres'=>$tres, 'cuatro'=>$cuatro, 'cinco'=>$cinco, 'seis'=>$seis, 'siete'=>$siete, 'ocho'=>$ocho, 'nueve'=>$nueve, 'diez'=>$diez,'pregunta'=>$tipo[0]->nombrePregunta];
    		break;
    }
	
	}
	public function reenviar(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
  
        $niv= DB::table('niveles')
        ->join('nivel_inst','nivel_inst.idNivel','=','niveles.idNivel')
        ->where('idInstitucion','=',$i)
        ->get();
        /*$niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$iduser);
                })->get();*/
        $encuesta=DB::table('encuesta')->select('encuesta.idEncuesta','encuesta.nombre','encuesta.descripcion','encuesta.fecha','encuesta.idInstitucion')
        ->where('encuesta.idEncuesta','=',$request->idEncuesta)
        ->where('encuesta.estatus','=','1')
        ->get();
        $tienc=DB::table('enc_completa')
        ->select('idEncuesta','tipoEncuestado')
        ->where('idEncuesta','=',$request->idEncuesta)
        ->groupBy('tipoEncuestado')
        ->get();
        $preguntas=DB::table('enc_preguntas as preguntas')
        ->select('preguntas.idEncuesta','preguntas.nombrePregunta','preguntas.tipoRespuesta','preguntas.opciones','preguntas.idPregunta')
        ->where('preguntas.idEncuesta','=',$request->idEncuesta)
        ->groupBy('preguntas.nombrePregunta')
        ->get();

	return view ("Admin.encuestas.reenviar",["encuesta"=>$encuesta,"niv"=>$niv, "tienc"=>$tienc, "preguntas"=>$preguntas, "idenc"=>$request]);

	}

	public function edit($id)
	{

	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		$encuesta=Encuestas::findOrFail($id);
		$encuesta->estatus='0';
		$encuesta->update();
		return Redirect::to('/admin/encuestas/');
	
	}
	
	
	//Notificaciones
	
    public function avisarProfe($id, $encuesta){
        
     
        
        $fcm = DB::table("profesor")->select('tokenFCM')
      ->where('idprofesor',$id)
      ->first();
    
    if($fcm->tokenFCM !="0")
        $this->sendNotification(
            "Encuesta", 
            "Hay una nueva encuesta para contestar" , 
             $fcm->tokenFCM,
             $encuesta
        );
        
        

        
    }
    
     public function avisarTutor($id, $encuesta){
        
 
           $fcm = DB::table("tutor")->select('tokenFCM')
      ->where('idtutor',$id)
      ->first();
    
    if($fcm->tokenFCM !="0")
        $this->sendNotification(
            "Encuesta", 
            "Hay una nueva encuesta para contestar" , 
             $fcm->tokenFCM,
             $encuesta
        );

        
        
    }
    
     public function sendNotification($titulo, $mensaje, $token, $id=0){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

    }
	
	

}
