<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Alumno;
use DB;
use Intervention\Image\ImageManagerStatic as Image;

class TutorRegistroAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser=Auth::user()->id;
        $inst=DB::table('tutor')->where('idtutor','=',$iduser)->pluck('idInstitucion');
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($inst)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$inst);
                })->get();
        return view('tutor.RegistroAlumno',["niv"=>$niv]);
    }

    public function getGrupo(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst=DB::table('tutor')->where('idtutor','=',$iduser)->pluck('idInstitucion');
        $idNivel=$request->id;
        $grupo= DB::table('grado')->select('grado.grado','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','grado.idNivel')
        ->where('grado.idInstitucion','=',$inst)        
        ->where('grado.idNivel','=',$idNivel)->get();
        return $grupo;
    }

      
}