<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Mensaje as Mensaje;
use DB;


class AlumnoMensajes extends Controller
{
    public function index()
    {
        //si se modifica el index se debe copiar en recibidos 
       $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $var=DB::table('alumno')->select('idAlumno as id','idgrupo','idInstitucion')->where('iduser',$usuario)->get()->first();
        $id=$var->idInstitucion;
        $migrupo=$var->idgrupo;
        $var2=DB::table('grupo')->select('grado','idNivel')->where('idgrupo',$migrupo)->get()->first();
        $minivel=$var2->idNivel;
        $migrado=$var2->grado;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])//este es el filtro de mensajes para que le lleguen a alumno
                        ->orWhere([ ['IdDestinatario',$minivel],['TipoDestinatario','10']])//por nivel
                        ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$id]])//a todos por institucion
                        ->orWhere([ ['IdDestinatario',$migrupo],['TipoDestinatario','7']])//por grupo
                        ->orWhere([ ['IdDestinatario',$minivel.'-'.$migrado],['TipoDestinatario','6']])//por grado
                        ->orderBy('created_at', 'desc')->get();//ordena de el mas nuevo al mas viejo
        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        $admin=DB::table('users')->select('id','name as Nombre')->get();
        $grado=DB::table('grado')->select('idNivel','grado as Nombre')->get();
        $grupo=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo"))->get();
        return \View::make('Alumno.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver','admin'));
    }

    public function papelera($id)//permite mandar al basurero los mensajes
    {
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Eliminado' => 1]);
            return redirect()->back();

    }

    public function desaparece($id)//se puede implementar para borrar los mensajes (solo no seran visibles para el usuario)
    {
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Eliminado' => 2]);
            //return redirect()->back();

    }

    public function create($id)
    {
        $mensaje = Mensaje::find($id);
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Eliminado' => 1]);
        return redirect()->back();
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //cuando ocurre un show se tiene que actualizar el mensaje la variable visto.
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Visto' => 1]);
        $var=DB::table('alumno')->select('idAlumno as id','idgrupo')->where('iduser',$usuario)->get()->first();;
        $migrupo=$var->idgrupo;
        $var2=DB::table('grupo')->select('grado','idNivel')->where('idgrupo',$migrupo)->get()->first();
        $minivel=$var2->idNivel;
        $migrado=$var2->grado;

        $mensaje= Mensaje::find($id);
        switch ($mensaje->TipoEmisor)//obtener quien lo envio
         {
            case '1':
                $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
            break;
            case '3':
                $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
            break;

        }

        switch ($mensaje->TipoDestinatario)//para quien lo envio
         {
            case '1'://admin
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '2'://profesor
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '3'://turor
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '4'://alumno
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '5'://Super Admin
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '6'://grado
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Escolaridad: ' ,idNivel, ' Grado: ', grado) as Nombre,grado as id,idNivel"))->distinct()->where([['grado',$mensaje->IdDestinatario],['idNivel',$minivel] ])->get();
            break;
            case '7'://grupo
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->where('idgrupo',$mensaje->IdDestinatario)->get();
            break;
            case '8':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos') as Nombre,idgrupo as id"))->where('idgrupo','1')->get();
            break;
            case '10':
                $variable2=DB::table('niveles')->select('nombreNivel as Nombre')->where('idNivel',$minivel)->get();
            break;

        }
        return \View::make('Alumno.Mensajes.mensajeShow',compact('mensaje','variable','variable2','sinver'));
    }

    public function edit($id)
    {

    }

    public function search(Request $request)
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        $mensajes = Mensaje::where([['Asunto','like','%'.$request->Asunto.'%'],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->get();
        $alumno=DB::table('Alumnos')->select('id','Nombre')->get();
        $profesor=DB::table('Profesores')->select('id','Nombre')->get();
        return \View::make('Alumno.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    public function update(Request $request, $id)
    {
        //
    }
    public function recibidos()//recibidos e index son iguales
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $var=DB::table('alumno')->select('idAlumno as id','idgrupo','idInstitucion')->where('iduser',$usuario)->get()->first();
        $id=$var->idInstitucion;
        $migrupo=$var->idgrupo;
        $var2=DB::table('grupo')->select('grado','idNivel')->where('idgrupo',$migrupo)->get()->first();
        $minivel=$var2->idNivel;
        $migrado=$var2->grado;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])
                        ->orWhere([ ['IdDestinatario',$minivel],['TipoDestinatario','10']])
                        ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$id]])
                        ->orWhere([ ['IdDestinatario',$migrupo],['TipoDestinatario','7']])
                        ->orWhere([ ['IdDestinatario',$minivel.'-'.$migrado],['TipoDestinatario','6']])
                        ->orderBy('created_at', 'desc')->get();        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        $admin=DB::table('users')->select('id','name as Nombre')->get();
        $grado=DB::table('grado')->select('idNivel','grado as Nombre')->get();
        $grupo=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo"))->get();
        return \View::make('Alumno.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver','admin'));
    }


    public function eliminados()//muestra los mensajes que estan en la papelera
    {
        $query=DB::table('mensajes')->latest()->get();
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','1']])->get();
        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        $admin=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Alumno.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver','admin'));
    }

    public function destroy($id)
    {

    }
}
