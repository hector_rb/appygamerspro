<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use app\User;
use DB;
use Intervention\Image\ImageManagerStatic as Image;

class perfilAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ima=Auth::user()->imagen;
        
        return view('Admin/profileAdmin',['img'=>$ima,]);
    }

    public function saveImage(Request $request)
    {
       
        $id=Auth::user()->id;
        $user=User::findOrFail($id);
        $png_url = date('Ymd').time().$id.".jpg";
        $path = public_path().'/img/profile_pics/' . $png_url;

        $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
        if($upload)
        {
            $user->imagen=$png_url;
            $user->update();
            return $png_url;
        }
        


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
