<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use DB;

class juegosController extends Controller
{
    public function index()
    {
        $juegos=DB::table('juego')->get();
        return view('Admin.AppyGamers.games',['juego'=>$juegos]);
    }
}
