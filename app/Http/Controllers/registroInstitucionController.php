<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\institucionRequest;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use DB;
use App\institucion;

class registroInstitucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id=Auth::user()->id;
        $val= DB::table('institucion')->where('id_usuario','=',$id)->get();
        if ($val[0]->idestado==''&&$val[0]->idpais=='')
        {
            $pais=DB::table('pais')->get();
            $estado=DB::table('estado')->get();
            return view ('Admin.registroInstitucionFirst',['array'=>$val,'estado'=>$estado,'pais'=>$pais]);
        }
        else
        {
            $array=DB::table('institucion')
                ->join('pais', function ($join) use ($id)  {
                $join->on('institucion.idpais','=','pais.idPais')
                     ->where('id_usuario','=',$id);
                })
                ->join('estado', function ($join) use ($id)  {
                $join->on('institucion.idestado','=','estado.idEstado')
                     ->where('id_usuario','=',$id);
                })->get();
                $array2=DB::table('estado')->get();
                $pais=DB::table('pais')->get();
            return view ('Admin.registroInstitucion',['array'=>$array,'estado'=>$array2,'pais'=>$pais]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveImage(Request $request)
    {
        
        $id=Auth::user()->id;
        $esc=DB::table('institucion')->select('idInstitucion')
        ->where('id_usuario','=',$id)
        ->first();
        $inst=institucion::findOrFail($esc->idInstitucion);
        if($request->tipo==1)
        {
            $png_url = date('Ymd').time().$id.".jpg";
            $path = public_path().'/img/logos/' . $png_url;

            $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
            if($upload)
            {
                $inst->logo=$png_url;
                $inst->update();
                return 'logo';
            }
        }
        if($request->tipo==2)
        {
            $png_url = date('Ymd').time().$id.".jpg";
            $path = public_path().'/img/banner/' . $png_url;

            $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
            if($upload)
            {
                $inst->banner=$png_url;
                $inst->update();
                return 'banner';
            }
        }


    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InstitucionRequest $request, $id)
    {
        //
        $save = institucion::findOrFail($id);
        $save->clave=$request->get('clave');
        $save->idestado=$request->get('estado');
        $save->ciudad=$request->get('ciudad');
        $save->direccion=$request->get('direccion');
        $save->contacto=$request->get('contacto');
        $save->idpais=$request->get('pais');
        $save->telefono=$request->get('telefono');
        $save->correo=$request->get('correo');
        $save->desc=$request->get('desc');
        $save->lat=$request->get('lat');
        $save->lng=$request->get('lng');
        $save->update();
        return Redirect::to('admin/RegistroInstitucion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
