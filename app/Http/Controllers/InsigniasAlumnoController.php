<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Insignias;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\InsigniasFormRequest;
use DB;
use Auth;

class InsigniasAlumnoController extends Controller
{
	 public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		$id=Auth::user()->id;
		$alumno=DB::table('alumno')
		->select('alumno.idAlumno','alumno.nombreAlumno','alumno.iduser')
		->where('alumno.iduser','=',$id)
		->first();
		$insignias= DB::table('Insignias as insignias')->select('insignias.id','insignias.idTipoInsignia','insignias.idReceptor','insignias.idEmisor','insignias.fecha','insignias.idSubGrupo','insignias.fecha','tiposinsignia.NombreInsignia','tiposinsignia.rutaImagen','profesor.nombreprof','profesor.apepat','profesor.apemat','materia.nombreMateria')
        ->join('tiposinsignia','tiposinsignia.idTipoInsignia','=','insignias.idTipoInsignia')
        ->join('subgrupo','subgrupo.idSubgrupo','=','insignias.idSubGrupo')
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->join('profesor','profesor.idprofesor','=','insignias.idEmisor')
        ->where('insignias.idReceptor','=',$alumno->idAlumno)
        ->where('insignias.tipoReceptor','=',0)
        //->where('insignias.idEmisor','=','profesor.iduser')
        ->orderBy('insignias.id','desc')
        ->get();
        $insigniasgpo=DB::table('Insignias as insignias')
        ->select('insignias.id','insignias.idTipoInsignia','insignias.tipoReceptor','insignias.idEmisor','insignias.idSubGrupo','insignias.fecha','subgrupo.idSubgrupo','subgrupo.idProfesor','profesor.nombreprof','profesor.apepat','profesor.apemat','materia.nombreMateria','tiposinsignia.rutaImagen')
        ->join('tiposinsignia','tiposinsignia.idTipoInsignia','=','insignias.idTipoInsignia')
        ->join('subgrupo','subgrupo.idSubgrupo','=','insignias.idSubGrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->join('profesor','profesor.idprofesor','=','insignias.idEmisor')
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
        //->where('alumno.idAlumno','=',$alumno->idAlumno)
        ->where('insignias.tipoReceptor','=',1)
        ->groupBy('insignias.id')
        ->orderBy('insignias.id','desc')
        ->get();
		//echo($insigniasgpo);
		return view ("Alumno.insignias.index",["insignias"=>$insignias, "insigniasgpo"=>$insigniasgpo]);
	}

	public function create()
	{
		return view("Alumno.insignias.create");
	}
	
	public function store(Request $request)
	{
		return Redirect::to('/alumno/insignias');
	}
	
	
	public function show($id)
	{
		return view("Alumno.insignias.show");

	}
	
	
	public function edit($id)
	{
		return view("Alumno.insignias.edit");
		
	
	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		
	
	}

}
