<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Asistencia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\AsistenciaFormRequest;
use DB;
use Auth;


class AsistenciaAlumnoController extends Controller
{
     public function index(Request $request)
	{
		$user=Auth::user()->id;
		$alum=DB::table('alumno')->select('idgrupo')->where('iduser','=',$user)->first();
		$grupo=DB::table('grupo')->select('idNivel','idInstitucion')->where('idgrupo','=',$alum->idgrupo)->first();
		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->where('idNivel','=',$grupo->idNivel)
		->where('idInstitucion','=',$grupo->idInstitucion)
		->get();

		return view("Alumno.asistencia.index",["periodo"=>$periodo]);
		
	
	}

	public function getAsistencia(Request $request)
    {
    	$user=Auth::user()->id;
		$alum=DB::table('alumno')->select('idAlumno')->where('iduser','=',$user)->first();
        $idalumno=$alum->idAlumno;
        $periodo=$request->idP;
        

        $asis=DB::table('asistencia')->select('materia.nombreMateria',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('subgrupo','subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
        ->join('materia','subgrupo.idMateria','=','materia.idMateria')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('asistencia.idAlumno','=',$idalumno)
		->where('asistencia.idPeriodo','=',$periodo)
		->where('ciclo.status','=',1)
		->groupBy('asistencia.idSubgrupo')
		->get();

        return $asis;
    }
}
