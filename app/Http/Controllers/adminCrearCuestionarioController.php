<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\pregunta;
use App\cuestionario;
use App\juegoCuestionario;
use App\Upload;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException; 
use Excel;
use Carbon\Carbon;

class adminCrearCuestionarioController extends Controller
{
    public function index()
    {
       
    }
    public function altaCuestionario(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        $preguntas=DB::table('juego-preguntas')->where('idInstitucion','=',$id)->get();
        
        
        $input=$request->all();
        $flag=$request->pregunta[0];

        if($request->registradas=="on" && $request->nuevas==NULL )
        {
            $this->registradas($request);
        }

        if($request->nuevas=="on" && $request->registradas==null )
        {
            $this->nuevas($request);
        }

        if($request->excel=="on")
        {
            $this->altaCuestionarioExcel($request);
        }

        if($request->nuevas=="on" && $request->registradas=="on")
        {
           $this->ambos($request);
        }
        return view('Admin.AppyGamers.Cuestionarios.agregarCuestionario',["niv"=>$niv,'preguntas'=>$preguntas]);
    }

    public function registradas(request $request)
    {
        $input=$request->all();
        $cuestionario= new cuestionario();
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
     
            $cuestionario = new cuestionario();
            $cuestionario->fill([
            'nombre'        =>$input['nombre'],
            'descripcion'   =>$input['descripcion'],
            'fecha_inicio'  =>$input['fInicio'],
            'fecha_fin'     =>$input['fFinal'], 
            'grado'         =>$input['selectGrupo'],
            'categoria'     =>$input['materia'],
            'idinstitucion' =>$id,
            'nivel'         =>$input['idSubGrupo'],
            ]);
            $cuestionario->save();
            $idCuest=$cuestionario->id_cuestionario;

            foreach ($input['preguntaR'] as $recorrido) {
                $juegoCuestionario = new juegoCuestionario();
                $juegoCuestionario->fill([
                'id_cuestionario'=>$idCuest,
                'id_pregunta'    =>$recorrido,
                    ]);
                $juegoCuestionario->save();
                unset($juegoCuestionario);
            }   
        

    }

    public function nuevas(Request $request)
    {
        $input=$request->all();
        $cuestionario= new cuestionario();      
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $var=sizeof($input['pregunta']);
        $cuestionario = new cuestionario();
        $cuestionario->fill([
        'nombre'        =>$input['nombre'],
        'descripcion'   =>$input['descripcion'],
        'fecha_inicio'  =>$input['fInicio'],
        'fecha_fin'     =>$input['fFinal'], 
        'grado'         =>$input['selectGrupo'],
        'categoria'     =>$input['materia'],
        'idinstitucion' =>$id,
        'nivel'         =>$input['idSubGrupo'],
        ]);
        $cuestionario->save();
        for($i=0;$i<$var;$i++)
        {
          $pregunta= new pregunta();
          $pregunta->fill([
            'idInstitucion'=>$id,
            'grado'        =>$input['selectGrupo'][0],
            'id_nivel'     =>$input['idSubGrupo'],
            'pregunta'     =>$input['pregunta'][$i],
            'opcion1'      =>$input['op1'][$i],
            'opcion2'      =>$input['op2'][$i],
            'opcion3'      =>$input['op3'][$i],
            'opcion4'      =>$input['op4'][$i],
            'respuesta'    =>$input['respuesta'.$i],
            'id_categoria' =>$input['materia'],
          ]);
         $pregunta->save();
         $juegoCuestionario = new juegoCuestionario();
         $juegoCuestionario->fill([
         'id_cuestionario'=>$cuestionario->id_cuestionario,
         'id_pregunta'    =>$pregunta->id_pregunta,
             ]);
         $juegoCuestionario->save();
         unset($juegoCuestionario);
         unset($pregunta);
        }
        
    }

    public function altaCuestionarioExcel(request $request)
    {
        $input=$request->all();

        $file = $request->file('file');
        $extension = strtolower($file->getClientOriginalExtension());
        $fileName  = Carbon::now()->micro.'.'.$extension;
        $path      = "archivos-excel";
        $file->move($path, $fileName);
        $upload = new Upload();
        $upload->filename = $fileName;
        $ruta= "archivos-excel/".$fileName;
        $data = Excel::load($ruta, function($reader) {})->get();

        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id_institucion=$inst->idInstitucion;
        $grado=$input['selectGrupo'];
        $id_nivel=$input['idSubGrupo'];
        $id_categoria=$input['materia'];
        
        $cuestionario = new cuestionario();
        $cuestionario->fill([
        'nombre'        =>$input['nombre'],
        'descripcion'   =>$input['descripcion'],
        'fecha_inicio'  =>$input['fInicio'],
        'fecha_fin'     =>$input['fFinal'], 
        'grado'         =>$input['selectGrupo'],
        'categoria'     =>$input['materia'],
        'idinstitucion' =>$id_institucion,
        'nivel'         =>$input['idSubGrupo'],
        ]);
         $cuestionario->save();
        
        $pregunta= new pregunta();
        if(!empty($data) && $data->count())
          {
             foreach ($data as $key => $value) 
              {  $pregunta= new pregunta();                        
                 $array =  (array) $value;                  
                 $pregunta->fill([
                       'idInstitucion' =>$id_institucion, 
                       'grado'         =>$grado,
                       'id_nivel'      =>$id_nivel, 
                       'pregunta'      =>$value->pregunta,
                       'opcion1'       =>$value->opcion1,
                       'opcion2'       =>$value->opcion2,
                       'opcion3'       =>$value->opcion3,
                       'opcion4'       =>$value->opcion4,
                       'respuesta'     =>$value->respuesta,
                       'id_categoria'  =>$id_categoria,
                     ]);
                  $pregunta->save();
                  $juegoCuestionario = new juegoCuestionario();
                  $juegoCuestionario->fill([
                  'id_cuestionario'=>$cuestionario->id_cuestionario,
                  'id_pregunta'    =>$pregunta->id_pregunta,
                      ]);
                  $juegoCuestionario->save();
                  unset($juegoCuestionario);
                  unset($pregunta);
           }
         }
    }

    public function ambos(request $request)
    {

        $input=$request->all();
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $juegoCuestionario = new juegoCuestionario();
        $var=sizeof($input['pregunta']);
            $cuestionario = new cuestionario();
            $cuestionario->fill([
            'nombre'        =>$input['nombre'],
            'descripcion'   =>$input['descripcion'],
            'fecha_inicio'  =>$input['fInicio'],
            'fecha_fin'     =>$input['fFinal'], 
            'grado'         =>$input['selectGrupo'],
            'categoria'     =>$input['materia'],
            'idinstitucion' =>$id,
            'nivel'         =>$input['idSubGrupo'],
            ]);
            $cuestionario->save();
            $idCuest=$cuestionario->id_cuestionario;
        for($i=0;$i<$var;$i++)
        {
          $pregunta= new pregunta();
          $pregunta->fill([
            'idInstitucion'=>$id,
            'grado'        =>$input['selectGrupo'][0],
            'id_nivel'     =>$input['idSubGrupo'],
            'pregunta'     =>$input['pregunta'][$i],
            'opcion1'      =>$input['op1'][$i],
            'opcion2'      =>$input['op2'][$i],
            'opcion3'      =>$input['op3'][$i],
            'opcion4'      =>$input['op4'][$i],
            'respuesta'    =>$input['respuesta'.$i],
            'id_categoria' =>$input['materia'],
          ]);
         $pregunta->save();
         $juegoCuestionario->fill([
         'id_cuestionario'=>$idCuest,
         'id_pregunta'    =>$pregunta->id_pregunta,
             ]);
         $juegoCuestionario->save();
         unset($pregunta);
        }
           foreach ($input['preguntaR'] as $recorrido) {
            $juegoCuestionario = new juegoCuestionario();
                $juegoCuestionario->fill([
                'id_cuestionario'=>$idCuest,
                'id_pregunta'    =>$recorrido,
                    ]);
                $juegoCuestionario->save();
                unset($juegoCuestionario);
            } 
    }
}
