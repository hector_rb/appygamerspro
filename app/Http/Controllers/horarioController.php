<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\horario;
use App\subgrupo;

class horarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        $prof=DB::table('profesor')->where('id_institucion','=',$id)->where('estatus','=',0)->get();

        $horarios=DB::table('grupo')
        ->select('subgrupo.idGrupo','subgrupo.idCiclo','ciclo.nombreCiclo' ,'grupo.idNivel','niveles.nombreNivel' ,'grupo.grado','grupo.grupo')
        ->join('subgrupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('niveles','grupo.idNivel','=','niveles.idNivel')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->where('grupo.idInstitucion','=',$id)
        ->groupBy('subgrupo.idGrupo','subgrupo.idCiclo')->get();

        return view('Admin.horario',['niv'=>$niv, 'horarios'=>$horarios]);
    }
    public function getGrupos(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->idNivel;
        $grado=$request->grado;
        $materia= DB::table('grupo')->where('idInstitucion','=',$id)->where('idNivel','=',$idNivel)->where('grado','=',$grado)->get();
        return $materia;

    }

    public function getProf(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idMat=$request->idMat;
        //$grado=$request->grado;
        $prof= DB::table('materia_profesor')->join('profesor', function ($join) use ($idMat)  {
                $join->on('materia_profesor.idProfesor','=','profesor.idprofesor')
                     ->where('idMateria','=',$idMat)
                     ->where('estatus','=',0);
                })->get();
        //$materia= DB::table('grupo')->where('idInstitucion','=',$id)->where('idNivel','=',$idNivel)->where('grado','=',$grado)->get();
        return $prof;

    }

    public function getCiclo(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->idNivel;
        $ciclo= DB::table('ciclo')->where('idInstitucion','=',$id)->where('idNivel','=',$idNivel)->get();
        return $ciclo;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        $prof=DB::table('profesor')->where('id_institucion','=',$id)->where('estatus','=',0)->get();


        return view('Admin.nuevoHorario',['niv'=>$niv]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->materia as $mat=>$valor ) 
        {
            $check = DB::table('subgrupo')
                    ->where('idGrupo','=',$request->grupo)
                    ->where('idMateria','=',$valor)
                    ->where('idCiclo','=',$request->ciclo)
                    ->get();
            if(count($check)==0)
            {
                $sg= new subgrupo;            
                $sg->idGrupo=$request->grupo;
                $sg->idMateria=$valor;
                $sg->idProfesor=$request->profesor[$mat];
                $sg->idCiclo=$request->ciclo;
                $sg->save();
            }

        }  
        $id=$request->grupo;
        $id2=$request->ciclo;
        $matgrupo= DB::table('subgrupo')
            ->join('materia', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idMateria', '=', 'materia.idMateria')               
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })
            ->join('profesor', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idProfesor','=','profesor.idprofesor')
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2)
                 ->where('profesor.estatus','=',0);
            })->get();

        return view('Admin.crearHorario',['matgrupo'=>$matgrupo,'idGrupo'=>$id,'idCiclo'=>$id2]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        //
        $id2=$request->ciclo;
        $matgrupo= DB::table('subgrupo')
            ->join('materia', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idMateria', '=', 'materia.idMateria')               
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })
            ->join('profesor', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idProfesor','=','profesor.idprofesor')
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2)->where('profesor.estatus','=',0);
            })->get();

        $query=DB::table('horario')
        ->select('grupo.grado','grupo.grupo','horario.idHorario','profesor.nombreprof','materia.nombreMateria','horario.inicio','horario.fin','horario.clase','niveles.nombreNivel')
                ->join('subgrupo','subgrupo.idSubgrupo','=','horario.idSubgrupo')
                ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
                ->join('profesor','profesor.idprofesor','=','subgrupo.idProfesor')
                ->join('materia','materia.idMateria','=','subgrupo.idMateria')
                ->join('niveles','niveles.idNivel','=','grupo.idNivel')
                ->where('horario.idGrupo','=',$id)
                ->where('horario.status','=',1)
                ->where('profesor.estatus','=',0)
                ->where('horario.idCiclo','=',$request->ciclo)->get();

        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $horario[] = ['id'=>$q->idHorario, 'title'=>$q->nombreMateria, 'description' => $q->nombreprof, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        else
        {
            $horario=[];
            $query=DB::table('grupo')
            ->select('grupo.grado','grupo.grupo','niveles.nombreNivel')
            ->join('niveles','niveles.idNivel','=','grupo.idNivel')
            ->where('grupo.idgrupo','=',$id)
            ->get();
        }
        return view('Admin.verHorario', ['horario'=>$horario,'matgrupo'=>$matgrupo,'idGrupo'=>$id,'idCiclo'=>$id2,'datos'=>$query]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        //
        $id2=$request->ciclo;
        $matgrupo= DB::table('subgrupo')
            ->join('materia', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idMateria', '=', 'materia.idMateria')               
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })
            ->join('profesor', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idProfesor','=','profesor.idprofesor')
                 ->where('subgrupo.idGrupo', '=', $id)
                 ->where('profesor.estatus','=',0)
                 ->where('subgrupo.idCiclo','=',$id2);
            })->get();

        $query=DB::table('horario')
        ->select('grupo.grado','grupo.grupo','horario.idHorario','profesor.nombreprof','materia.nombreMateria','horario.inicio','horario.fin','horario.clase','niveles.nombreNivel')
                ->join('subgrupo','subgrupo.idSubgrupo','=','horario.idSubgrupo')
                ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
                ->join('profesor','profesor.idprofesor','=','subgrupo.idProfesor')
                ->join('materia','materia.idMateria','=','subgrupo.idMateria')
                ->join('niveles','niveles.idNivel','=','grupo.idNivel')
                ->where('horario.idGrupo','=',$id)
                ->where('horario.status','=',1)
                ->where('profesor.estatus','=',0)
                ->where('horario.idCiclo','=',$request->ciclo)->get();

        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $horario[] = ['id'=>$q->idHorario, 'title'=>$q->nombreMateria, 'description' => $q->nombreprof, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        else
        {
            $horario=[];
            $query=DB::table('grupo')
            ->select('grupo.grado','grupo.grupo','niveles.nombreNivel')
            ->join('niveles','niveles.idNivel','=','grupo.idNivel')
            ->where('grupo.idgrupo','=',$id)
            ->get();
        }
        //echo($matgrupo);
        return view('Admin.editarHorario', ['horario'=>$horario,'matgrupo'=>$matgrupo,'idGrupo'=>$id,'idCiclo'=>$id2,'datos'=>$query]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function event(Request $request)
    {   
        $id=Auth::user()->id;
        if ($request->type==1)
        {
            $save= new horario;
            $save->idSubgrupo=$request->sg;
            $save->idGrupo=$request->grupo;
            $save->idCiclo=$request->ciclo;
            $save->inicio=$request->start;
            $save->dia=$request->dia;
            $save->horaIni=$request->ini;
            $save->clase=$request->class;
            $save->save();
            return (['status'=>'success','eventid'=>$save->idHorario]);
        }
        if($request->type ==2) 
        {
            $eventid = $request->id;
            $inicio =$request->start;
            $fin = $request->end;
            $dia=$request->dia;
            $horaini=$request->horaini;
            $horafin=$request->horafin;
            DB::table('horario')->where('idHorario', '=', $eventid)->update(['fin' => $fin, 'inicio'=>$inicio, 'dia'=>$dia,'horaIni'=>$horaini,'horaFin'=>$horafin]);
              return (['status'=>'success']);
        }
        if($request->type ==3) 
        {
            $eventid = $request->id;
            DB::table('horario')->where('idHorario', '=', $eventid)->update(['status' => 0]);
              return (['status'=>'success']);
        }
    }
}