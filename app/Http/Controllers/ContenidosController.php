<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class ContenidosController extends Controller
{
    public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $niv= DB::table('niveles')
        ->join('nivel_inst','nivel_inst.idNivel','=','niveles.idNivel')
        ->where('idInstitucion','=',$i)
        ->get();
		$query=trim($request->get('searchText'));
		$contenidos=DB::table('contenidos')->select('idContenido','contenidos.titulo','contenidos.descripcion','contenidos.anexo','contenidos.imagen','contenidos.año','contenidos.editorial','contenidos.autores','categoria.nombre as categoria')
		->join('categoria','categoria.idCategoria','=','contenidos.idCategoria')
		//->orderBy('contenidos.idContenido','ASC')
		->where('categoria.nombre','LIKE','%'.$query.'%')
		->orwhere('contenidos.autores','LIKE','%'.$query.'%')
		->orwhere('contenidos.editorial','LIKE','%'.$query.'%')
		->orwhere('contenidos.titulo','LIKE','%'.$query.'%')
		->get();
		return view ("Admin.contenidos.index",['niv'=>$niv, 'contenidos'=>$contenidos, "searchText"=>$query]);
	}

	public function create()
	{
		
	}

	public function store(Request $request)
	{
	  
		return Redirect::to('/admin/contenidos');
		
	}

	
	public function show( $id)
	{   
		$contenido=DB::table('contenidos')
        ->select('titulo','descripcion','anexo')
        ->where('idContenido','=',$id)
        ->first();
        return view("Admin.contenidos.show",['contenido'=>$contenido]);

	}
	
	public function videos(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $niv= DB::table('niveles')
        ->join('nivel_inst','nivel_inst.idNivel','=','niveles.idNivel')
        ->where('idInstitucion','=',$i)
        ->get();
		$query2=trim($request->get('searchText2'));
		/*$categoria=DB::table('categoria')->select('nombre',
			'idCategoria')
		->get();*/
		$categoria=DB::table('contenidos_videos as videos')
		->select('categoria.nombre','categoria.idCategoria')
		->join('categoria','categoria.idCategoria','=','videos.idCategoria')
		->orderBy('videos.idCategoria','DESC')
		->where('categoria.nombre','LIKE','%'.$query2.'%')
		->orwhere('videos.titulo','LIKE','%'.$query2.'%')
		->orwhere('videos.descripcion','LIKE','%'.$query2.'%')
		->groupBy('categoria.idCategoria')
		->get();
		$videos=DB::table('contenidos_videos as videos')->select('videos.idvideo','videos.titulo','videos.descripcion','videos.anexo','categoria.nombre as categoria','categoria.idCategoria as idCategoria')
		->join('categoria','categoria.idCategoria','=','videos.idCategoria')
		->orderBy('videos.idCategoria','DESC')
		->where('categoria.nombre','LIKE','%'.$query2.'%')
		->orwhere('videos.titulo','LIKE','%'.$query2.'%')
		->orwhere('videos.descripcion','LIKE','%'.$query2.'%')
		//->groupBy('categoria')
		->get();
		//echo($videos);
		return view ("Admin/contenidos/videos",['videos'=>$videos, 'categoria'=>$categoria, "searchText2"=>$query2]);
	}

	public function vervideo(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
        $niv= DB::table('niveles')
        ->join('nivel_inst','nivel_inst.idNivel','=','niveles.idNivel')
        ->where('idInstitucion','=',$i)
        ->get();
		$videos=DB::table('contenidos_videos as videos')->select('videos.idvideo','videos.titulo','videos.descripcion','videos.anexo','categoria.nombre as categoria','categoria.idCategoria as idCategoria')
		->join('categoria','categoria.idCategoria','=','videos.idCategoria')
		->get();
		return view ("Admin.contenidos.vervideo",['videos'=>$videos]);
	}

	public function edit($id)
	{

	}
	public function update(ExpedientesFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		return Redirect::to('/admin/contenidos/');
	
	}
}
