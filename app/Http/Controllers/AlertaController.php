<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Alerta;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AlertaFormRequest;
use DB;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class AlertaController extends Controller
{
         public function __construct()
	{

	}



	public function index()
	{
	    /*$usuario=Auth::user()->id;
	    $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
        $alerta=DB::table('alerta')->join('users','id','alerta.idUser')->where('idInstitucion',$institucion->idInstitucion)->get();*/
        //$creador=DB::table('users')->select('name','id')->where('id',$alerta->idUser)->get();
        //return view('Admin.alerta.enviados',compact('alerta','creador'));

        //$query=DB::table('TipoAlerta')->get();
		//return view("Admin.alerta.create",['Destinatarios'=>$query]);
		$usuario=Auth::user()->id;
	    $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
        $alerta=DB::table('alerta')->select('alerta.idAlerta','alerta.tipoAlerta','alerta.descripcion','alerta.created_at','alerta.condicion','alerta.idInstitucion','alerta.idUser','alerta.updated_at')
        ->join('users','id','alerta.idUser')
        ->where('alerta.idUser','=',$usuario)
        ->where('alerta.idInstitucion','=',$institucion->idInstitucion)
        ->where('alerta.condicion','=',1)
        ->orderBy('alerta.idAlerta','desc')
        ->get();
        //echo($alerta);
        return view('Admin.alerta.enviados',['alerta'=>$alerta]);
//
	}
	public function create()
	{
            $query=DB::table('TipoAlerta')->get();
    		return view("Admin.alerta.create",['Destinatarios'=>$query]);
	}

	public function store(Request $request)
	{
	    $usuario=Auth::user()->id;
	    $institucion=DB::table('institucion')->select('idInstitucion')->where('id_usuario',$usuario)->get()->first();
		$alerta=new Alerta;
		$alerta->tipoAlerta=$request->get('tipoAlerta');
		$alerta->descripcion=$request->get('desc');
		$alerta->condicion=1;
		$alerta->idUser=$usuario;
		$alerta->created_at=date('Y-m-d H:i:s');
		$alerta->idInstitucion=$institucion->idInstitucion;
		$alerta->save();
		$id = $alerta->idAlerta;
		
		$this->fcmProfe($institucion->idInstitucion, $request->get('desc'), $id);
		$this->fcmTutor($institucion->idInstitucion, $request->get('desc'), $id);
		$this->fcmAlumno($institucion->idInstitucion, $request->get('desc'), $id);
		
		return Redirect::to('/admin/alerta');
	}


	public function show($id)
	{
		return view("Admin.alerta.recibidos",["alerta"=>Alerta::findOrFail($id)]);

	}


	public function edit($id)
	{
		return view("Admin.alerta.edit",["alerta"=>Alerta::findOrFail($id)]);


	}
	public function update(AlertaFormRequest $request,$id)
	{
		return Redirect::to('/Admin/alerta');

	}
	public function destroy($id)
	{
		$alerta=Alerta::findOrFail($id);
		$alerta->condicion='0';
		$alerta->update();
		return Redirect::to('/admin/alerta');

	}

    public function enviados()
    {
        $alerta=DB::table('alerta')->get();
        return view('Admin.alerta.enviados',compact('alerta'));
    }
    
     public function fcmProfe($ins, $mensaje, $id)
    {
        
         $fcm = DB::table('profesor')
         ->select('tokenFCM')
         ->where('id_institucion','=',$ins)
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token, $mensaje, $id);
         }
        
    }
    
    public function fcmTutor($ins, $mensaje, $id)
    {
        
         $fcm = DB::table('alumno')
         ->select('tutor.tokenFCM', 'tutor.dispositivo')
         ->join('tutoralumno', 'alumno.idAlumno','tutoralumno.idalumno')
         ->join('tutor','tutoralumno.idtutor','tutor.idtutor')
         ->where('alumno.idinstitucion','=',$ins)
         ->groupBy('tutor.idtutor')
         ->get();
       
         
         foreach($fcm as $token)
         {
            // echo($token->dispositivo);
             $this->avisar($token, $mensaje, $id, $token->dispositivo);
         }
        
    }
    
    public function fcmAlumno($ins, $mensaje, $id)
    {
        
         $fcm = DB::table('alumno')
         ->select('tokenFCM')
         ->where('idinstitucion','=',$ins)
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token, $mensaje, $id);
         }
        
    }
    
    public function avisar($token, $mensaje, $id, $dispositivo="android")
	{
	    

                
                 // echo json_encode($tutoralumno);
                 // echo "<br><br>";
                  
                      
                       if($token->tokenFCM!="0"){
                     
                             $this->sendNotification(
                            "Alerta", 
                            $mensaje, 
                            $token->tokenFCM,
                            $id,
                            $dispositivo
                            );
                        }
                      
                  
	
	}
	
	public function sendNotification($titulo, $mensaje, $token, $id, $dispositivo){
	    
	    $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

    }

}
