<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Asistencia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\AsistenciaFormRequest;
use DB;
use Auth;

class AsistenciaTutorController extends Controller
{
     public function index()
	{	
		$a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }

		$id=$a;

		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->join('grupo', function ($join) use ($id)  {
                $join->on('grupo.idNivel','=','periodo.idNivel');
                $join->on('grupo.idInstitucion','=','periodo.idInstitucion');
                     
                })
		->join('alumno','alumno.idgrupo','=','grupo.idGrupo')
		->where('alumno.idAlumno','=',$id)
		->get();
		return view("Tutor.asistencia.index",["periodos"=>$periodo]);
	
	}

	public function getAsistencia(Request $request)
    {
        $idalumno=session('idTutelado');
        $periodo=$request->idP;
        

        $asis=DB::table('asistencia')->select('materia.nombreMateria',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('subgrupo','subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
        ->join('materia','subgrupo.idMateria','=','materia.idMateria')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('asistencia.idAlumno','=',$idalumno)
		->where('asistencia.idPeriodo','=',$periodo)
		->where('ciclo.status','=',1)
		->groupBy('asistencia.idSubgrupo')
		->get();

        return $asis;
    }
}
