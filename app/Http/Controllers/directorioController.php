<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\directorio;
use DB;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Redirect;

class directorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user=Auth::user()->id;
        $dir=DB::table('directorio')
        ->select('idDirectorio','nombre','telefono','email','puesto','descripcion','imagen')
        ->where('idUser','=',$user)
        ->where('estatus','=',1)
        ->get();
        return view('Admin/directorio/directorio',['dir'=>$dir]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Admin/directorio/crearDirectorio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')->where('id_usuario','=',$user)->first();
        $dir=new directorio;
        $dir->nombre=$request->nombre;
        $dir->telefono=$request->tel;
        $dir->email=$request->email;
        $dir->puesto=$request->puesto;
        $dir->descripcion=$request->desc;
        $dir->idUser=$user;
        $dir->idInstitucion=$inst->idInstitucion;
        $dir->save();
        return view('Admin/directorio/directorioImagen',['id'=>$dir->idDirectorio,'img'=>$dir->imagen]);
        //return Redirect::to('admin/directorio');

    }
    public function saveImageDir(Request $request)
    {
        //$blob=$request->file('croppedImage');
        //->storeAs(public_path().'img/profile_pics','pp.jpg');   
       // $file = file_put_contents(public_path().'img/profile_pics/pp.jpg', ;
        //echo utf8_decode($request->file('croppedImage'));
        /*if($request->hasFile('croppedImage')!=''){
            $blob=$request->file('croppedImage');
            $time=time();  
            $file = file_put_contents(public_path().'/img/profile_pics/'.$time.'.'.$blob->extension(), $blob);
            $dir=directorio::findOrFail(4);
            $dir->imagen=$blob;
            $dir->update();
        }*/
        //$data = Input::all();
        $id=$request->id;
        $dir=directorio::findOrFail($id);
        $png_url = date('Ymd').time().$id.".jpg";
        $path = public_path().'/img/contact/' . $png_url;

        $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
        if($upload)
        {
            $dir->imagen=$png_url;
            $dir->update();
        }
        


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user=Auth::user()->id;
        $dir=DB::table('directorio')
        ->select('idDirectorio','nombre','telefono','email','puesto','descripcion','imagen')
        ->where('idUser','=',$user)
        ->where('idDirectorio','=',$id)
        ->first();
        return view('Admin/directorio/editarDirectorio',['dir'=>$dir]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dir=directorio::findOrFail($id);
        $dir->nombre=$request->nombre;
        $dir->telefono=$request->tel;
        $dir->email=$request->email;
        $dir->puesto=$request->puesto;
        $dir->descripcion=$request->desc;
        $dir->update();
        return Redirect::to('/admin/directorio');
    }

    public function cambiarFiltro(Request $request)
    {
            //dd($request->all());
        $contactos = $request->all();
        foreach ($contactos['contactos'] as $key) {
            $dir=directorio::findOrFail($key['id']);
            if(isset($key['verProfesor']) )
                $dir->verProfesor=$key['verProfesor'];
            else
                $dir->verProfesor=0;

            if(isset($key['verTutor']))
                $dir->verTutor=$key['verTutor'];
            else
                $dir->verTutor=0;

            $dir->update();
            
        }
        
        return Redirect::to('/admin/directorio/filtro');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dir=directorio::findOrFail($id);
        $dir->estatus='0';
        $dir->update();
        return Redirect::to('/admin/directorio');
    
    }

    public function filtro(Request $request)
    {
        //
        $user=Auth::user()->id;
        $dir=DB::table('directorio')
        ->select('idDirectorio','nombre','telefono','email','puesto','descripcion','imagen','verProfesor','verTutor')
        ->where('idUser','=',$user)
        ->where('estatus','=',1)
        ->get();
        return view('Admin/directorio/filtro',['dir'=>$dir]);
    }
}
