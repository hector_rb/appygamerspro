<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\avisosFormRequest;
use Illuminate\Support\Facades\Redirect;
use App\avisos;
use Auth;
use DB;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


class avisosProfesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $iduser=Auth::user()->id;
        $inst= DB::table('profesor')->select('id_institucion')->where('iduser','=',$iduser)->first();
        $id=$inst->id_institucion;
        $avisos=DB::table('avisos')->select('avisos.idAviso','avisos.asunto','avisos.cuerpo','avisos.idUser','avisos.destinatario','avisos.fecha','avisos.idgrupo','grupo.grupo','grupo.grado')
        ->leftJoin('grupo','grupo.idgrupo','=','avisos.idgrupo')
        ->where('estatus','=',0)
        ->where('avisos.idInstitucion','=',$id)
        ->where('avisos.idUser','=',$iduser)
        ->orderBy('avisos.fecha','desc')
        ->paginate(10);


        return view('Profesor.avisos.index',['avisos'=>$avisos]);
    }

    public function create()
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $prof=DB::table('profesor')->select('idprofesor','id_institucion')->where('iduser','=',$iduser)->first();
        $grupo = DB::table('grupo')->select('grupo.idgrupo','grupo.grado','grupo.grupo','niveles.idNivel','niveles.nombreNivel')
        ->join('subgrupo','subgrupo.idGrupo','=','grupo.idgrupo')
        ->join('niveles','niveles.idNivel','=','grupo.idNivel')
        ->where('grupo.idInstitucion','=',$prof->id_institucion)
        ->where('subgrupo.idprofesor','=',$prof->idprofesor)
        ->where('subgrupo.estatus','=',1)
        //->where('grupo.idgrupo','=','subgrupo.idGrupo')
        ->groupBy('grupo.idgrupo')
        ->get();

        return view("Profesor.avisos.create",["grupo"=>$grupo]);
    }
    public function store(Request $request)
    {
        $id=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$id)->first();
        $prof=DB::table('profesor')->select('idprofesor','id_institucion')->where('iduser','=',$id)->first();
        
        foreach ($request->destinatario as $key) {

            $aviso= new avisos;
            $aviso->asunto=$request->asunto;
            $aviso->cuerpo=$request->cuerpo;
            $aviso->idInstitucion=$prof->id_institucion;
            $aviso->idUser=$id;
            $aviso->fecha=date('Y-m-d H:i:s');
            $op=explode(",",$key);
            $aviso->destinatario=$op[0];
            
            if($op[0]==6)
            {
                //echo($op[1]);
                $aviso->idgrupo=$op[1];
                $this->fcmAlumnoGrupo($prof->id_institucion, $op[1]);
                $this->fcmTutor($prof->id_institucion);
            }
             if($op[0]==2)
            {
                
                $this->fcmTutor($prof->id_institucion);
            }
            if($op[0]==3)
            {
                
                $this->fcmProfe($prof->id_institucion);
                
            }
            if($op[0]==4)
            {
                
                $this->fcmAlumno($prof->id_institucion);
                $this->fcmTutor($prof->id_institucion);
                
            }
            
            if($op[0]==5)
            {
            
                $this->fcmAlumno($prof->id_institucion);
                $this->fcmTutor($prof->id_institucion);
                
            }
            
             if($op[0]==1)
            {
                
                $this->fcmProfe($prof->id_institucion);
                $this->fcmTutor($prof->id_institucion);
                $this->fcmAlumno($prof->id_institucion);
            }
            //echo($aviso);
            $aviso->save();
        }
        
        
   return Redirect::to('/profesor/avisosProf');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $iduser=Auth::id();
        $inst= DB::table('profesor')->select('id_institucion')->where('iduser','=',$iduser)->first();
        $idins=$inst->id_institucion;
        $aviso=DB::table('avisos')
        ->where('idInstitucion','=',$idins)
        ->where('idAviso','=',$id)
        ->first();
        return view('Profesor.avisos.show',['aviso'=>$aviso]);
    }

    public function enviados(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('profesor')->select('id_institucion')->where('iduser','=',$iduser)->first();
        $id=$inst->id_institucion;
        $avisos=DB::table('avisos')
        ->leftJoin('grupo','grupo.idgrupo','=','avisos.idgrupo')
        ->where('estatus','=',0)
        ->where('idUser','=', $iduser)
        ->orderBy('fecha','desc')
        ->get();
        //return $avisos;
        return view('Profesor.avisos.enviados',['avisos'=>$avisos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $avisos=avisos::findOrFail($id);
        $avisos->estatus='1';
        $avisos->update();
        return Redirect::to('profesor/avisosProf');
    }
    
     public function fcmProfe($ins)
    {
        
         $fcm = DB::table('profesor')
         ->select('tokenFCM', 'dispositivo')
         ->where('id_institucion','=',$ins)
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token, $token->dispositivo);
         }
        
    }
    
    public function fcmTutor($ins)
    {
        
         $fcm = DB::table('alumno')
         ->select('tutor.tokenFCM', 'tutor.dispositivo')
         ->join('tutoralumno', 'alumno.idAlumno','tutoralumno.idalumno')
         ->join('tutor','tutoralumno.idtutor','tutor.idtutor')
         ->where('alumno.idinstitucion','=',$ins)
         ->groupBy('tutor.idtutor')
         ->get();
       
       
         
         foreach($fcm as $token)
         {
             echo ($token->dispositivo);
             $this->avisar($token, $token->dispositivo);
         }
        
    }
    
    public function fcmAlumno($ins)
    {
        
         $fcm = DB::table('alumno')
         ->select('tokenFCM')
         ->where('idinstitucion','=',$ins)
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
    public function fcmAlumnoGrupo($ins, $grupo)
    {
        
         $fcm = DB::table('alumno')
         ->select('tokenFCM')
         ->where('idinstitucion','=',$ins)
         ->where('idgrupo','=',$grupo)
         ->get();
       //echo($fcm);
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
    public function avisar($token, $dispositivo="android")
	{
	    

                
                 // echo json_encode($tutoralumno);
                 // echo "<br><br>";
                  
                      
                       if($token->tokenFCM!="0"){
                     
                             $this->sendNotification(
                            "Aviso Escolar", 
                           "Tienes un Nuevo Aviso!", 
                            $token->tokenFCM,
                            0,
                            $dispositivo
                            );
                        }
                      
                  
	
	}
    
    	public function sendNotification($titulo, $mensaje, $token, $id=0, $dispositivo){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

     $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }
    }
}
