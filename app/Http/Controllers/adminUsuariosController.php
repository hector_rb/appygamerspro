<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\adminUsuarios;
use App\subgrupo;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use Auth;
use DB;

class adminUsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $iduser=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')
        ->where('id_usuario','=',$iduser)
        ->first();
        $profs=DB::table('profesor as p')->select('p.idprofesor','p.nombreprof','p.apepat','p.apemat','p.correo','u.imagen', 'p.matricula')
        ->join('users as u','u.id','=','p.iduser')
        ->where('p.id_institucion','=',$inst->idInstitucion)
        ->where('p.estatus','=',0)
        ->get();
        return view('Admin.registro.index',['profs'=>$profs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
   
        if($request->has('idSubgrupo')){
            for ($i=0; $i<count($request->idSubgrupo); $i++) {
                DB::table('subgrupo')
                ->join('profesor','profesor.idprofesor','=','subgrupo.idProfesor')
                ->where('subgrupo.idSubgrupo','=',$request->idSubgrupo[$i])
                ->update(['subgrupo.idProfesor'=> $request->profesor[$i],'profesor.estatus'=> 1]);
            }
        }
        
        DB::table('profesor')
        ->where('idprofesor','=',$request->idprofesor)
        ->update(['estatus'=>1]);

        
        return Redirect::to('/admin/adminUsuarios/ProfesoresEliminados');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $iduser=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')
        ->where('id_usuario','=',$iduser)
        ->first();
        $profs=DB::table('subgrupo')->select('subgrupo.idSubgrupo','subgrupo.idGrupo','subgrupo.idMateria','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','subgrupo.idProfesor')
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->join('niveles','niveles.idNivel','=','grupo.idNivel')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->join('profesor','profesor.idprofesor','=','subgrupo.idProfesor')
        ->join('users as u','u.id','=','profesor.iduser')
        ->where('profesor.estatus','=',0)
        ->where('subgrupo.idProfesor','=',$id)
        ->get();
        $profesor=DB::table('profesor')
                  ->join('users as u','u.id','=','profesor.iduser')
                  ->select('profesor.nombreprof','profesor.apepat','profesor.apemat','profesor.fechanac','profesor.telefono','profesor.correo','profesor.celular','profesor.genero','u.imagen')
                  ->where('idprofesor','=',$id)
                  ->get();

        //echo($profs);
        return view ('Admin.registro.show',['profs'=>$profs,'profesor'=>$profesor]);
    }

    public function ProfesoresEliminados()
    {
        //
        $iduser=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')
        ->where('id_usuario','=',$iduser)
        ->first();
        $profs=DB::table('profesor as p')->select('p.idprofesor','p.nombreprof','p.apepat','p.apemat','p.correo','u.imagen')
        ->join('users as u','u.id','=','p.iduser')
        ->where('p.id_institucion','=',$inst->idInstitucion)
        ->where('p.estatus','=',1)
        ->get();
        return view('Admin.registro.ProfesoresEliminados',['profs'=>$profs]);
    }

    public function edit($id)
    {
        return view('Admin.registro.edit',["subgrupo"=>subgrupo::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profs=adminUsuarios::findOrFail($id);
        $profs->estatus='1';
        $profs->update();

        return Redirect::to('/admin/adminUsuarios');
    }


    public function reactivar(Request $request)
    {
        $profs=adminUsuarios::findOrFail($request->id);
        $profs->estatus='0';
        $profs->update();
       
        return Redirect::to('/admin/adminUsuarios');
    }

    public function reasignarMaterias(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')
        ->where('id_usuario','=',$iduser)
        ->first();
        $prof=DB::table('subgrupo as sg')
                ->join('materia as m','m.idMateria','=','sg.idMateria')
                ->join('grupo as g','g.idgrupo','=','sg.idGrupo')
                ->join('profesor','profesor.idprofesor','=','sg.idProfesor')
                ->select('sg.idProfesor','m.nombreMateria','g.grado','g.grupo','sg.idSubgrupo','profesor.nombreprof','profesor.apepat','profesor.apemat')
                ->where('sg.idProfesor','=',$request->id)
                ->get();
        $sg=DB::table('profesor')
        ->select('idprofesor','nombreprof','apepat','apemat')
        ->where('id_institucion','=',$inst->idInstitucion)
        ->where('estatus','=','0')
        ->get();

       
        return view('Admin.registro.reasignarMaterias',['prof'=>$prof, 'sg'=>$sg,'id'=>$request->id]);
    }

}
