<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Asistencia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\AsistenciaFormRequest;
use DB;
use Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class AsistenciaController extends Controller
{
    public function __construct()
	{
		
	}
	public function index(Request $request)
	{
		/*$query=trim($request->get('searchText'));
		$asistencia=DB::table('asistencia as a')
		->join('alumno as b','a.idAlumno','=','b.idAlumno')
		->join('grupo as g','a.idSubgrupo','=','g.idgrupo')
		->join('profesor','a.idgrupo','=','a.idgrupo')
		->select ('a.idAlumno','a.idSubgrupo','a.fecha','a.asistencia','b.apepat as apepat','b.apemat as apemat','b.nombre as nombre')
		->where('a.idSubgrupo','LIKE','%'.$query.'%')
		->orwhere('a.fecha','LIKE','%'.$query.'%')

		->orderBy('a.idAlumno','asc')
		->paginate(10);*/
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();
		return view('Profesor.asistencias.index',['sg'=>$sg]);

	}
	public function create()
	{
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();
		return view("Profesor.asistencias.create",['sg'=>$sg]);
	}

	public function getAlumno(Request $request)
	{
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idgrupo')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo', 'alumno.matricula')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idgrupo','=',$grupo->idgrupo)
		->orderBy('alumno.apepat')
		->get();
    
		return $alum;
	}

	public function getFechasAsistencia(Request $request)
	{
		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$user)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$periodo=$request->periodo;
		$fechas=DB::table('periodo')->select('asistencia.fecha','asistencia.idPeriodo')
		->leftjoin('asistencia','asistencia.idPeriodo','=','periodo.idPeriodo')
		->where('periodo.idNivel','=',$grupo->idNivel)
		->where('periodo.idInstitucion','=',$prof->id_institucion)
		->where('asistencia.idSubgrupo','=',$sg)
		->where('asistencia.idPeriodo','=',$periodo)
		->groupBy('asistencia.fecha')
		->get();
		return $fechas;

	}

	public function getAsistencia(Request $request)
	{
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;

		$asis=DB::table('asistencia')->select('asistencia.idAlumno', 'alumno.nombreAlumno','alumno.apepat','alumno.matricula','alumno.apemat',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->where('idSubgrupo','=',$subgrupo)
		->where('idPeriodo','=',$periodo)
		->groupBy('idAlumno')
		->get();

		//SELECT idAlumno, count(CASE WHEN asistencia = 'A' THEN 1 END) as A, count(CASE WHEN asistencia = 'F' THEN 1 END) as F, count(CASE WHEN asistencia = 'R' THEN 1 END) as R FROM asistencia WHERE idSubgrupo = 1 GROUP BY idAlumno
		return $asis;
	}

	public function getFechasAsistencia2(Request $request)
	{
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$fechas=DB::table('periodo')->select('asistencia.fecha','asistencia.idPeriodo');
		$asis=DB::table('asistencia')->select('asistencia.fecha','asistencia.idAlumno', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat', 
			DB::raw('(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->where('idSubgrupo','=',$subgrupo)
		->where('idPeriodo','=',$periodo)
		->where('fecha','=', $request->fecha)
		->groupBy('idAlumno')
		->get();

		//SELECT idAlumno, count(CASE WHEN asistencia = 'A' THEN 1 END) as A, count(CASE WHEN asistencia = 'F' THEN 1 END) as F, count(CASE WHEN asistencia = 'R' THEN 1 END) as R FROM asistencia WHERE idSubgrupo = 1 GROUP BY idAlumno
		return $asis;
	}



	public function store(AsistenciaFormRequest $request)
	{
		//dd($request->get('idAlumno'));
		
		$Subgrupo = $request->idSubgrupo;
		$fecha=$request->fecha;
		$periodo=$request->periodo;
		foreach($request->asis as $key => $a) {
				
				$asis=new Asistencia;
				$asis->idSubgrupo=$Subgrupo;
				$asis->idAlumno=$request->id[$key];
				$asis->idPeriodo=$periodo;
				$asis->asistencia=$a;
				$asis->fecha=$fecha;
				$asis->save();
				
				 if($asis->asistencia=="F"){
				 $this->avisar($request->id[$key], "¡Falta!", "Se registro una falta"); 
				}
				if($asis->asistencia=="A"){
				  $this->avisar($request->id[$key], "Hoy si asistió", "Se registro una asistencia"); 
				}
				
				if($asis->asistencia=="R"){
				  $this->avisar($request->id[$key], "¡Retardo!", "Se registro un retardo"); 
				}
				
				
  			
		}
		
		return Redirect::to('/profesor/asistencias');
		/*
		$asistencia=new Asistencia;
		$asistencia->idAlumno=$request->get('idAlumno');
		$asistencia->idgrupo=$request->get('idgrupo');
		$asistencia->fecha=$request->get('fecha');
		$asistencia->asistencia=$request->get('asistencia');
		$asistencia->save();
		*/
		return Redirect::to('/profesor/asistencias');
	}
	public function show($id)
	{
		return view("Profesor.asistencias.show",["asistencia"=>Asistencia::findOrFail($id)]);
		
	}
	public function edit($id, Request $request)
	{
		$asistencia=DB::table('asistencia')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->where('asistencia.fecha','=',$id)
		->where('asistencia.idPeriodo','=',$request->periodo)
		->where('asistencia.idSubgrupo','=',$request->grupo) 
		->get();
		return view("Profesor.asistencias.edit",["asistencia"=>$asistencia, 'periodo'=>$request->periodo, 'sg'=>$request->grupo, 'id'=>$id

	]);
		
	
	}
	public function update(Request $request, $id)
	{
		foreach($request->asistencia as $idAlumno => $asis ) {

			$buscar = DB::table('asistencia')
				->where('idAlumno', '=', $idAlumno)
				->where('idSubgrupo', '=', $request->idSubgrupo)
				->where('fecha', '=', $id)
				->first();

			if(count($buscar)>0)
			{
				DB::table('asistencia')
				   ->where('idAlumno', '=', $idAlumno)
				   ->where('idSubgrupo', '=', $request->idSubgrupo)
				   ->where('fecha', '=', $id)
				   ->update(['asistencia'=> $asis]);
				   
				   if($asis=="F"){
				 $this->avisar($idAlumno, "¡Falta!", "Se registro una falta"); 
				}
				if($asis=="A"){
				  $this->avisar($idAlumno, "Hoy si asistió", "Se registro una asistencia"); 
				}
				
				if($asis=="R"){
				  $this->avisar($idAlumno, "¡Retardo!", "Se registro un retardo"); 
				}
			}
			else{

				Asistencia::create([
				    'idAlumno' => $idAlumno,
				    'idSubgrupo'=> $request->idSubgrupo,
				    'idPeriodo' => $request->periodo,
				    'fecha'=> $id,
				    'asistencia'=> $asis,
				  ]);
				  
				   if($asis=="F"){
				  $this->avisar($idAlumno, "¡Falta!", "Se registro una falta");
				}
					if($asis=="A"){
				  $this->avisar($idAlumno, "Hoy si asistió", "Se registro una asistencia"); 
				}
				
				if($asis=="R"){
				  $this->avisar($idAlumno, "¡Retardo!", "Se registro un retardo"); 
				}

			}
			/*
			if($request->id[$key]=="")
				{
					$asistencia=new Asistencia;
					$asistencia->idalumno=$request->iduser[$key];
					$asistencia->idSubgrupo=$request->get('idgrupo');
					$asistencia->fecha=$request->get('fecha');
					$asistencia->asistencia=$request->get('asistencia');
					$asistencia->save();
				}
				else
				{
					$asistencia=Asistencia::findOrFail($request->id[$key]);
					$asistencia->calif=$c;
					$asistencia->update();
				}
		*/
		}
 
		return Redirect::to('/profesor/asistencias');

	}

	public function destroy($id)
	{
		$asistencia=Asistencia::findOrFail($id);
		$asistencia->update();
		return Redirect::to('/profesor/asistencias');
	
	}
	
	public function avisar($alumno, $titulo, $mensaje)
	{
	    
	$tutoralumno = DB::table("tutoralumno")->select('tutor.*', 'alumno.nombreAlumno')
                  ->join('tutor', 'tutoralumno.idtutor', '=', 'tutor.idtutor')
                  ->join('alumno', 'tutoralumno.idalumno', '=', 'alumno.idAlumno')
                  ->where('tutoralumno.idalumno',$alumno)
                  ->first();
                 // echo ($user->idAlumno);
                 // echo json_encode($tutoralumno);
                 // echo "<br><br>";
                  if($tutoralumno){
                      
                       if($tutoralumno->tokenFCM!="0"){
                     
                             $this->sendNotification(
                             $titulo, 
                             $mensaje, 
                             $tutoralumno->tokenFCM,
                             0,
                             $tutoralumno->dispositivo
                            );
                        }
                      
                  }
	
	}
	
	public function sendNotification($titulo, $mensaje, $token, $id=0, $dispositivo){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

    }
    
}
