<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\horario;

class HorarioProfesorController extends Controller
{
    public function index()
    {
    	$iduser=Auth::user()->id;
    	$prof=DB::table('profesor')->select('idprofesor','id_institucion','nombreprof','apepat','apemat')
    	->where('iduser','=',$iduser)
    	->first();
    	$horario=DB::table('subgrupo')->select('niveles.nombreNivel','grupo.grado','grupo.grupo','ciclo.nombreCiclo','subgrupo.idGrupo','subgrupo.idCiclo')
    	->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
    	->join('niveles','grupo.idNivel','=','niveles.idNivel')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->where('grupo.idInstitucion','=',$prof->id_institucion)
        ->where('subgrupo.idProfesor','=',$prof->idprofesor)
        ->groupBy('grupo.idgrupo')
        ->get();
    	return view('Profesor.horario.index',['horario'=>$horario, 'prof'=>$prof]);
    }

    public function show($id, Request $request)
    {
        //
        $iduser=Auth::user()->id;
        $prof=DB::table('profesor')->select('idprofesor','id_institucion')
    	->where('iduser','=',$iduser)
    	->first();
        $id2=$request->ciclo;
        $matgrupo= DB::table('subgrupo')
            ->join('materia', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idMateria', '=', 'materia.idMateria')               
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })
            ->join('profesor', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idProfesor','=','profesor.idprofesor')
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })->get();

        $query=DB::table('horario')
        ->select('grupo.grado','grupo.grupo','horario.idHorario','profesor.nombreprof','materia.nombreMateria','horario.inicio','horario.fin','horario.clase','niveles.nombreNivel')
                ->join('subgrupo','subgrupo.idSubgrupo','=','horario.idSubgrupo')
                ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
                ->join('profesor','profesor.idprofesor','=','subgrupo.idProfesor')
                ->join('materia','materia.idMateria','=','subgrupo.idMateria')
                ->join('niveles','niveles.idNivel','=','grupo.idNivel')
                ->where('horario.idGrupo','=',$id)
                ->where('horario.status','=',1)
                ->where('horario.idCiclo','=',$request->ciclo)
                ->where('subgrupo.idProfesor','=',$prof->idprofesor)
                ->get();


        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $horario[] = ['id'=>$q->idHorario, 'title'=>$q->nombreMateria, 'description' => $q->nombreprof, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        else
        {
            $horario=[];
        }
        return view('Profesor.horario.show', ['horario'=>$horario,'matgrupo'=>$matgrupo,'idGrupo'=>$id,'idCiclo'=>$id2,'datos'=>$query]);
    }
}
