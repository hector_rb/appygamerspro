<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\alumno;
use DB;
use Intervention\Image\ImageManagerStatic as Image;

class RegistroAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();

        $tutor=DB::table('tutor')->where('idInstitucion','=',$id)->get();
        return view('Admin.RegistroAlumno',["niv"=>$niv,"tutor"=>$tutor]);
    }

    public function Registrar(Request $request)
    {
    	$input=$request->all();
    	$user= new User();
    	$user->fill([
    		'name'      =>$input['name'],
    		'email'     =>$input['email'],
    		'password'  =>bcrypt($input['password']),
    		'tipo_user' =>"3",
    	]);
    	$user->save();

    	$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $idi=$inst->idInstitucion;

    	$alumno = new Alumno();
    	$alumno->fill([
    	'idAlumno'     =>$user->id,
    	'idinstitucion'=>$idi,
    	'nombreAlumno' =>$input['name'],
    	'apepat'       =>$input['apepat'],
    	'apemat'       =>$input['apemat'],
    	'genero'       =>$input['genero'],
    	'fechanac'     =>$input['fechanac'],
    	'telefono'     =>$input['cel'],
    	'correo'       =>$input['email'],
    	'idgrupo'      =>$input['grado'],
    	'nivel'        =>$input['nivel'],
    	'idtutor'      =>$input['tutor'],
    	]);
    	$alumno->save();
    	$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();

        $tutor=DB::table('tutor')->get();
        return view('Admin.RegistroAlumno',["niv"=>$niv,"tutor"=>$tutor]);
    }

    public function getGruposPreguntas(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->id;
        $grupo= DB::table('grado')->select('grado.grado','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','grado.idNivel')
        ->where('grado.idInstitucion','=',$id)        
        ->where('grado.idNivel','=',$idNivel)->get();
        return $grupo;
    }

   
}