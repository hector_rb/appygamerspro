<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

use App\User;
use App\Tutor;
use App\Alumno;
use App\Profesor;
use App\TutorAlumno;
use App\Institucion;
use App\Grupo;
use DB;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();

            if ($user->id == "1") {
                 return response()->json(['error'=>'Unauthorised1'], 401);
            }
            else {
                $success['id'] = $user->id;
                $success['email'] = $user->email;
                $success['nombre'] = $user->name;
                $success['tipo_user'] = $user->tipo_user;
                $success['imagen'] = $user->imagen;
                return response()->json(['success' => $success], $this->successStatus);
            }
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
    * Register api
    *
    * @return \Illuminate\Http\Response
    */
   public function register(Request $request){
       // Se validan los datos,cada uno de los campos definidos en la funcion son validados
       $validator = Validator::make($request->all(), [
           'name' => 'required',
           'email' => 'required|email',
           'password' => 'required',
           'tipo_user' => 'required',
       ]);

       // Se hace comprobacion de que los datos son validados
       if ($validator->fails()) {
           return response()->json(['error'=>$validator->errors()], 401);
       }

       // Se crea un array a partir del response, y se encripta la contraseña
       $input = $request->all();
       $input['password'] = bcrypt($input['password']);

       // Se crea un usuario apartir del array creado anteriormente.
       $user = User::create([
           'name' => $input['name'],
           'email' => $input['email'],
           'tipo_user' => $input['tipo_user'],
           'password' => $input['password'],
       ]);

       // Dependiendo del tipo de usuario se hace el registro pertinente
       switch ($input['tipo_user']) {
            case '2':
                $this->saveTutor($input, $user->id);
                break;
            case '3':
                 $this->saveProfesor($input, $user->id);
                 break;
            case '4':
                $this->saveAlumno($input, $user->id);
                break;
            default:
                break;
       }

       //Se prepara la respuesta que sera retornada al usuario
       $success['id'] = $user->id;
       $success['name'] =  $user->name;
       $success['token'] =  $user->createToken('MyApp')->accessToken;
       return response()->json(['success'=>$success], $this->successStatus);

   }

   // Funcion para guardar los datos del tutor en la tabla correspondiente
   private function saveTutor($input, $id){
       Tutor::create([
           'apepat'      => $input['apep'],
           'apemat'      => $input['apem'],
           'nombreTutor' => $input['name'],
           'fechanac'    => $input['fena'],
           'correo'      => $input['email'],
           'telefono'    => $input['cell'],
           'iduser'      => $id,
       ]);
   }

   // Funcion para guardar los datos del profesor en la tabla correspondiente
   private function saveProfesor($input, $id){

       $clave = $input['clave'];
       $institucion = Institucion::where('codregistro', $clave)->first();

       Profesor::create([
           'id_institucion' => $institucion->idInstitucion,
           'apepat'   => $input['apep'],
           'apemat'   => $input['apem'],
           'nombreprof'   => $input['name'],
           'fechanac' => $input['fena'],
           'correo'   => $input['email'],
           'telefono' => $input['cell'],
           'iduser'   => $id,
       ]);
   }

   // Funcion para guardar los datos del alumno en la tabla correspondiente
   private function saveAlumno($input, $id){

       $clave = $input['clave'];
       $grupo = Grupo::where('contrasena', $clave)->first();

       $alumno = Alumno::create([
           'idinstitucion' => $grupo->idInstitucion,
           'apepat'        => $input['apep'],
           'apemat'        => $input['apem'],
           'nombreAlumno'  => $input['name'],
           'fechanac'      => $input['fena'],
           'correo'        => $input['email'],
           'telefono'      => $input['cell'],
           'iduser'        => $id,
           'idgrupo'       => $grupo->idgrupo,
       ]);

       $tutor = Tutor::where('iduser', $input['idtutor'])->first();

       TutorAlumno::create([
           'idtutor' => $tutor->idtutor,
           'idalumno' => $alumno->idAlumno,
       ]);
   }

   /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details(){
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function checkCode(Request $request){
        $input = $request->all();
        $valid = "0";

        if($input["tipo"] == "Tutor"){ // Tutor

            $val=DB::table('grupo')
            ->where('contrasena','=',$input["codigo"])
            ->first();

            if($val){

                $val2=DB::table('grupo')
                ->select('institucion.nombre', 'institucion.direccion', 'institucion.telefono', 'institucion.correo', 'institucion.contacto')
                ->join('institucion', 'grupo.idInstitucion', '=', 'institucion.idInstitucion')
                ->where('grupo.contrasena','=',$input["codigo"])
                ->first();

                //$valid = $val2;
                return response()->json(['success' => $val2], $this->successStatus);
            }

        } else if($input["tipo"] == "Profesor"){ // Profesor

            $val=DB::table('institucion')
            ->where('codregistro','=',$input["codigo"])
            ->first();

            if($val){
                //$valid = $val;
                return response()->json(['success' => $val], $this->successStatus);
            }

        }
        
        return response()->json("0", 403);

        //return response()->json(['error' => "tipo o codigo incorrecto"], 400);

    }

}
