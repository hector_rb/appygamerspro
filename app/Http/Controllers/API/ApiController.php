<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;

class ApiController extends Controller
{
    public $successStatus = 200;

    public function uploadImage(Request $request){
        $input = $request->all();

        $validator = Validator::make($request->all(), [
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $imageName =date('Ymd').time(). '.' . $input["image"]->getClientOriginalExtension();
        $path = public_path()."/img/profile_pics/";
        $input["image"]->move($path, $imageName);


        DB::table('users')
            ->where('id', $input['idUser'])
            ->update(['imagen' => $imageName]);

        return response()->json(['success'=>$path . $imageName], $this->successStatus);
    }
    
    public function updateAlumnoImagen(Request $request){
        $input = $request->all();
        
        $validator = Validator::make($request->all(), [
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        
        $user = DB::table('alumno')->select('users.*')
        ->join('users', 'alumno.iduser', 'users.id')
        ->where('idAlumno', $input['idAlumno'])
        ->first();
        
        $path = public_path()."/img/profile_pics/";
        
        //Eliminar la imagen antigua
        $file= $user->imagen;
        $filename = $path . $file;
        File::delete($filename);
        
        
        //Guardar la nueva imagen en el servidor y modificar la base de datos.
        $imageName =date('Ymd').time(). '.' . $input["image"]->getClientOriginalExtension();
        $input["image"]->move($path, $imageName);
        DB::table('users')
            ->where('id', $user->id)
            ->update(['imagen' => $imageName]);

        return response()->json(['success'=>$imageName], $this->successStatus);
        
    }
    
    public function updateAlumnoImagenR(Request $request){
        $input = $request->all();
        
        $validator = Validator::make($request->all(), [
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        
        $path = public_path()."/img/profile_pics/";
        
        //Guardar la nueva imagen en el servidor y modificar la base de datos.
        $imageName =date('Ymd').time(). '.' . $input["image"]->getClientOriginalExtension();
        $input["image"]->move($path, $imageName);

        return response()->json(['success'=>$imageName], $this->successStatus);
        
    }
    
    public function updateProfesorImagen(Request $request){
        $input = $request->all();
        
        $validator = Validator::make($request->all(), [
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        
        $user = DB::table('users')
        ->where('id', $input['idUsuario'])
        ->first();
        
        $path = public_path()."/img/profile_pics/";
        
        //Eliminar la imagen antigua
        $file= $user->imagen;
        $filename = $path . $file;
        File::delete($filename);
        
        
        //Guardar la nueva imagen en el servidor y modificar la base de datos.
        $imageName =date('Ymd').time(). '.' . $input["image"]->getClientOriginalExtension();
        $input["image"]->move($path, $imageName);
        DB::table('users')
            ->where('id', $user->id)
            ->update(['imagen' => $imageName]);

        return response()->json(['success'=>$imageName], $this->successStatus);
        
    }
    
    public function updateTutorImagen(Request $request) {
        
        $input = $request->all();
        
        $validator = Validator::make($request->all(), [
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        
        $user = DB::table('users')
        ->where('id', $input['idUser'])
        ->first();
        
        $path = public_path()."/img/profile_pics/";
        
        //Eliminar la imagen antigua
        $file= $user->imagen;
        $filename = $path . $file;
        File::delete($filename);
        
        
        //Guardar la nueva imagen en el servidor y modificar la base de datos.
        $imageName =date('Ymd').time(). '.' . $input["image"]->getClientOriginalExtension();
        $input["image"]->move($path, $imageName);
        
        // Actualizar el nombre de la imagen en la base de datos
        
        DB::table('users')
            ->where('id', $user->id)
            ->update(['imagen' => $imageName]);

        return response()->json(['success'=>$imageName], $this->successStatus);
        
    }
    
    public function uploadImageTarea(Request $request){
        $input = $request->all();

        $validator = Validator::make($request->all(), [
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $imageName =date('Ymd').time(). '.' . $input["image"]->getClientOriginalExtension();
        $path = public_path().$input["path"];
        $input["image"]->move($path, $imageName);


        DB::table('tareas')
            ->where('idTareas', $input['idTarea'])
            ->update(['anexo' => $imageName]);

        return response()->json(['success'=>$path . $imageName], $this->successStatus);
    }
}
