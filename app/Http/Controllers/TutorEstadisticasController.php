<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Alumno;
use DB;
use Intervention\Image\ImageManagerStatic as Image;

class TutorEstadisticasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        $id=Auth::user()->id;
        $alum=DB::table('juego-info-general')
            ->join('alumno','juego-info-general.id_usuario','=','alumno.idAlumno')
            ->select('juego-info-general.tiempo','juego-info-general.id_usuario','juego-info-general.id_juego')
            ->where('alumno.idtutor','=',$id)
            ->groupBy('juego-info-general.id_usuario')
            ->get();
        foreach ($alum as $juego) 
        {
         $arreglo[]=DB::table('juego-info-general')
                     ->join('alumno','alumno.idAlumno','=','juego-info-general.id_usuario')
                     ->join('juego','juego.id_juego','=','juego-info-general.id_juego')
                     ->where('id_usuario','=',$juego->id_usuario)
                     ->get();
         
        }
        return view('tutor.TutorEstadisticas',["tutelados"=>$arreglo]);
    }
}