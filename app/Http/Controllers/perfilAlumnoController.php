<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\alumno;
use Illuminate\Support\Facades\Redirect;

class perfilAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ima=Auth::user()->imagen;
        $informacion=DB::table('alumno')
        ->select('idAlumno','nombreAlumno','apepat','apemat','genero', 'fechanac','telefono','matricula')
        ->where('iduser','=',Auth::user()->id)
        ->first();
        return view('Alumno/profileAlum',['img'=>$ima,'info'=>$informacion]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function saveImage(Request $request)
    {
       
        $id=Auth::user()->id;
        $user=User::findOrFail($id);
        $png_url = date('Ymd').time().$id.".jpg";
        $path = public_path().'/img/profile_pics/' . $png_url;

        $upload=Image::make(file_get_contents($request->croppedImage))->save($path);     
        if($upload)
        {
            $user->imagen=$png_url;
            $user->update();
            return $png_url;
        }
        


    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $save=alumno::findOrFail($id);
        $save->nombreAlumno=$request->nombre;
        $save->apepat=$request->apepat;
        $save->apemat=$request->apemat;
        $save->genero=$request->genero;
        $save->fechanac=$request->fechanac;
        $save->telefono=$request->tel;
        $save->matricula=$request->matricula;
        $save->save();

        $idu=Auth::user()->id;
        $user=User::findOrFail($idu);
        $user->name=$id=$request->nombre;
        $user->save();
        return Redirect::to('/alumno/perfilAlum');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
