<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Auth as At;
class registrarAlumno extends Controller
{
    //
     /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'tipo_user' => 'required|max:1',
            'password' => 'required|string|min:6|confirmed',
            'apepat' => 'string|max:45',
            'apemat' => 'string|max:45',
            'fechanac' => 'date_format:"Y-m-d',
            'tel' =>'string|max:45',
            'cel' =>'string|max:45',
            'name_esc' =>'string|max:10',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'tipo_user' => $data['tipo_user'],
            'password' => bcrypt($data['password']),
            'admin'=>2,
        ]);
    }
    public function valCod(Request $request)
    {   
        if($request->tipo==1)
        {
            $val=DB::table('institucion')
            ->where('codregistro','=',$request->codigo)
            ->first();
            if($val)
                return 1;
            else
                return 0;
        }
        if($request->tipo=2)
        {
            $val=DB::table('grupo')
            ->where('contrasena','=',$request->codigo)
            ->first();
            if($val)
                return 1;
            else
                return 0;

        }

    }
}
