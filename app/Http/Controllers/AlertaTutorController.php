<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Alerta;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AlertaFormRequest;
use DB;

class AlertaTutorController extends Controller
{
     public function __construct()
	{
		
	}
	
	
	public function index(Request $request)
	{
		if ($request)
		{
			$query=trim($request->get('searchText'));
			$alerta=DB::table('alerta')
			->select('idAlerta','titulo','descripcion','fecha')
			->where('titulo','LIKE','%'.$query.'%')
			->where('condicion','=','1')
			->orderBy('idAlerta','desc')
			->paginate(7);
			return view("Tutor.alerta.index",["alerta"=>$alerta, "searchText"=>$query]);
		}
	
	}
	public function create()
	{
		return view("Admin.alerta.create");
	}
	public function store(TareaFormRequest $request)
	{
	}
	
	
	public function show($id)
	{
		return view("Admin.alerta.show",["alerta"=>Alerta::findOrFail($id)]);
		
	}
	
	
	public function edit($id)
	{
		return view("Admin.alerta.edit",["alerta"=>Alerta::findOrFail($id)]);
		
	
	}
	public function update(AlertaFormRequest $request,$id)
	{
		return Redirect::to('/Admin/alerta');
	
	}
	public function destroy($id)
	{
		return Redirect::to('/Admin/alerta');
	
	}
}
