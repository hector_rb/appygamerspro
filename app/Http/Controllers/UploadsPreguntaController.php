<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Upload;
use App\Materia;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException; 
use DB;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UploadsPreguntaController extends Controller
{

   public function getIndex()
   {
   		return view("admin/Preguntas");
   }


//esta funcion la llamo desde la vista
    public function postSave(Request $request)
    { 
      //echo($request;
      $file = $request->file('file');
      $idN  = $request->idNivel;
      return $this->subirArchivo($file,$idN);
    }


//descargar
    public function descargarPlantillaMateria()
    {
     return response()->download('/home/myappc5/www.appycollege.com/archivos-excel/PLANTILLA_MATERIA.xlsx');
    }

public function subirArchivo($archivo,$id)
{  
    if(is_null($archivo))
    {
      return response()->json(4);
    }

    $mime      = $archivo->getMimeType();
    $extension = strtolower($archivo->getClientOriginalExtension());
    $fileName  = Carbon::now()->micro.'.'.$extension;
    $path      = "archivos-excel";

      switch ($mime) 
      {
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        if ($archivo->isValid())
          {  
            $archivo->move($path, $fileName);
            $upload = new Upload();
            $upload->filename = $fileName;

            if($upload->save())
              {        
                return $this->leerArchivo("archivos-excel/".$fileName,$id);
              }
            else
              {
              \File::delete($path."/".$fileName);
               //return redirect('admin/adminEscolar')->with('error-message', 'An error ocurred saving data into database');
              //
              }
            }
        break;

        case "application/vnd.ms-excel":
        if ($archivo->isValid())
          {
            $archivo->move($path, $fileName);
            $upload = new UploadMateria();
            $upload->filename = $fileName;
                            
            if($upload->save())
              {
                return $this->leerArchivo("archivos-excel/".$fileName,$id);
              } 
            else
               {
                  \File::delete($path."/".$fileName);
                  return redirect('admin/adminEscolar')->with('error-message', 'An error ocurred saving data into database');
                }
          }
       break;            
    
        default: 
          //return redirect('admin/adminEscolar')->with('error-message', 'La extension del archivo es incorrecta');
          return response()->json(3);
        break;
          }
      }

public function leerArchivo($ruta,$idN)
  {
    $id=(Auth::user()->id);
    $idInstitucion= DB::table('institucion')->where('id_usuario', '=', $id)->pluck('idInstitucion')->first();
    $data = Excel::load($ruta, function($reader) {})->get();
    if(!empty($data) && $data->count())
     {
      foreach ($data as $key => $value) 
        {                          
          $array =  (array) $value;                  
          $materia[] = [
                       'nombreMateria' =>$value->nombre, 
                       'creditos'      =>$value->creditos,
                       'grado'         =>$value->grado, 
                       'matricula'     =>$value->matricula,
                       'idInstitucion' =>$idInstitucion,
                       'idNivel'       =>(Integer)$idN,
                        ];
              $filtrado=array_filter($materia, function($var){return !is_null($var);});
              //return $filtrado;
              DB::table('materia')->insert($filtrado);
              // Materia::create($materia);
              unset($materia);
         }
      }
      return response()->json(1);
    }

public function borrarArchivo($ruta)
{ 
  \File::delete($ruta);
}


}

/* flujo de datos:

1) vista
2) subir archivo 
3) leer archivo 
4) llenar tabla usuario
5) llenar tabla profesor
6) eliminar archivo 
7) vista 