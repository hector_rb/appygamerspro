<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Expedientes;
use App\Conocimientos;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ExpedientesFormRequest;
use DB;
use Auth;

class ExpedientesTutorController extends Controller
{
   public function __construct()
	{
		
	}
	public function index(Request $request)
	{
		$a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }

        $idalumno=$a;
                if($idalumno!=0){
                $alumno=DB::table('alumno')->select('alumno.idAlumno','alumno.idinstitucion','alumno.nombreAlumno','alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo','alumno.genero','alumno.fechanac','alumno.telefono','alumno.correo','tutor.nombreTutor','tutor.apepat as tutorapepat','tutor.apemat as tutorapemat','tutor.correo as tutorcorreo','tutor.telefono as tutortelefono','tutor.celular as tutorcelular','users.imagen')
        		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
        		->join('tutoralumno','tutoralumno.idalumno','=','alumno.idAlumno')
        		->join('tutor','tutor.idtutor','=','tutoralumno.idtutor')
        		->join('users','users.id','=','alumno.iduser')
        		//->join('datos_medicos as dm','dm.idAlumno','=','alumno.idAlumno')
        		->where('alumno.idAlumno','=',$idalumno)
        		->get();
        		$asistencia=DB::table('asistencia')->select('asistencia')->where('idAlumno','=',$idalumno)->count();
        		$insignias=DB::table('Insignias as i')->select('i.idReceptor','i.tipoReceptor','i.fecha','ti.rutaImagen','i.fecha')
        		->join('tiposinsignia as ti','ti.idTipoInsignia','=','i.idTipoInsignia')
        		->where('i.idReceptor','=',$idalumno)
        		->where('i.tipoReceptor','=',0)
        		->get();
        		$datosmedicos=DB::table('datos_medicos')->select('idAlumno','alergias','enfermedades','enfCro','cirugias','numSeguro','tipoSangre','numEme')
        		->where('idAlumno','=',$idalumno)
        		->get();
        		$conocimientos=DB::table('conocimientos_adicionales')->select('idAlumno','conocimiento1','conocimiento2','conocimiento3')
        		->where('idAlumno','=',$idalumno)
        		->get();
        
        		$calif=DB::table('calif')->select('calif.calif')
        		->where('idalumno','=',$idalumno)
        		->avg('calif.calif');
        
        		if(!$calif)
        			$calif=0.0;
                }
                else
                {
                    $alumno=0;
                    $asistencia=0;
                    $insignias=0;
                    $datosmedicos=0;
                    $conocimientos=0;
                    $calif=0;
                }
		return view ("Tutor.expedientes.index",["alumno"=>$alumno, "asistencia"=>$asistencia, "insignias"=>$insignias, "datosmedicos"=>$datosmedicos, "conocimientos"=>$conocimientos,"calif"=>$calif]);
	}

	public function create()
	{
		
	}

	public function datosmedicos(Request $request)
	{
	
	}

	public function store(Request $request)
	{

		return Redirect::to('/admin/expedientes');
		
	}

	
	public function show( $id)
	{   

	}

	public function edit($id)
	{

	}
	public function update(ExpedientesFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		return Redirect::to('/admin/expedientes/');
	
	}
}
