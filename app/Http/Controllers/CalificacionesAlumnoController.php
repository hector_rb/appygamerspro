<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Calificaciones;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CalificacionesFormRequest;
use DB;
use Auth;

class CalificacionesAlumnoController extends Controller
{
    public function index(Request $request)
	{
		$user=Auth::user()->id;
		$alum=DB::table('alumno')->select('idgrupo')->where('iduser','=',$user)->first();
		$grupo=DB::table('grupo')->select('idNivel','idInstitucion')->where('idgrupo','=',$alum->idgrupo)->first();
		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->where('idNivel','=',$grupo->idNivel)
		->where('idInstitucion','=',$grupo->idInstitucion)
		->get();
		return view("Alumno.calificaciones.index",["periodo"=>$periodo]);
		
	}

	public function getCalificaciones(Request $request)
	{	
		$user=Auth::user()->id;
		$alum=DB::table('alumno')->select('idAlumno')->where('iduser','=',$user)->first();
		$id=$alum->idAlumno;
		$periodo=$request->idP;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.idAlumno','alumno.nombreAlumno','alumno.apepat','alumno.apemat','calif.calif','grupo.grado','grupo.grupo','materia.nombreMateria')
		->join('subgrupo','subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('alumno','alumno.idAlumno','=','calif.idAlumno')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->where('calif.idAlumno','=',$id)
		->where('calif.idPeriodo','=',$periodo)
		->where('ciclo.status','=',1)
		->get();

		return $calif;

	}

}
