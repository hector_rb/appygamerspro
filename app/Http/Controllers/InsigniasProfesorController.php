<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Insignias;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\InsigniasFormRequest;
use DB;
use Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class InsigniasProfesorController extends Controller
{
     public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();
		//echo(json_encode($sg));
		return view("Profesor.insignias.index",["sg"=>$sg]);
	}

	public function create()
	{
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();
		return view("Profesor.insignias.create",["sg"=>$sg]);
	}
	public function getAlumnoInsignia(Request $request)
	{

		/*$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$id)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idinstitucion','=',$prof->id_institucion)
		->get();

		return $alum;
		*/
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('grupo.idgrupo','=',$subgrupo->idGrupo)
		->get();

		return $alum;
	}

	
	public function store(Request $request){

		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
	
	if($request->selectGrupo)
	{
		$Insignias=new Insignias;		
		$Insignias->id=NULL;
		$Insignias->idTipoInsignia=$request->get('idTipoInsignia');
		$Insignias->idReceptor=0;
		$Insignias->tipoReceptor=1;
		$Insignias->idEmisor=$prof->idprofesor;
		$Insignias->idSubGrupo=$request->get('idSubGrupo');
		$Insignias->fecha=date('Y-m-d');
		$Insignias->save();
		$this->alumno($request->get('idSubGrupo'), $Insignias->id);
	}
	else
	{
		foreach ($request->selectAlumno as $key ) 
		{
			$Insignias=new Insignias;		
			$Insignias->id=NULL;
			$Insignias->idTipoInsignia=$request->get('idTipoInsignia');
			$Insignias->idReceptor=$key;
			$Insignias->tipoReceptor=0;
			$Insignias->idEmisor=$prof->idprofesor;
			$Insignias->idSubGrupo=$request->get('idSubGrupo');
			$Insignias->fecha=date('Y-m-d');
			$Insignias->save();
			$this->avisarInsignia($key, $Insignias->id);
		}
	}
		return Redirect::to('/profesor/insignias/create');
	}
	
	
	public function show($id)
	{
		return view("Profesor.insignias.show");

	}
	
	
	public function edit($id)
	{
		return view("Profesor.insignias.insignias",["insignias"=>insignias::findOrFail($id)]);
		
	
	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		
	
	}
	
	public function alumno($subgrupo, $insignia)
	{
		 $grupo=DB::table('subgrupo')
		 ->select('idGrupo')
		 ->where('idSubgrupo','=',$subgrupo)
		 ->first();
		 
        $alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','alumno.iduser', 'grupo.grado','grupo.grupo', 'grupo.idgrupo')
        ->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
        ->join('users','alumno.iduser','=','users.id')
        ->where('alumno.idgrupo','=',$grupo->idGrupo)
            ->orderBy('alumno.apepat', 'asc')
            ->orderBy('alumno.apemat', 'asc')
        ->get();
        
     
        foreach ($alum as $user) {
            
       $this->avisarInsignia($user->idAlumno, $insignia);
        
            
                   
       }

	
	}
	
	
	
	public function avisarInsignia($alumno, $insignia)
	{
	    
	     $tutoralumno = DB::table("tutoralumno")->select('tutor.*', 'alumno.nombreAlumno')
                  ->join('tutor', 'tutoralumno.idtutor', '=', 'tutor.idtutor')
                  ->join('alumno', 'tutoralumno.idalumno', '=', 'alumno.idAlumno')
                  ->where('tutoralumno.idalumno',$alumno)
                  ->first();
                 // echo ($user->idAlumno);
                 // echo json_encode($tutoralumno);
                 // echo "<br><br>";
                 //dd($insignia);
                  if($tutoralumno){
                      
                       if($tutoralumno->tokenFCM!="0"){
                     
                             $this->sendNotification(
                             "Insignia", 
                             $tutoralumno->nombreAlumno . " Ha recibido una Insignia!", 
                            $tutoralumno->tokenFCM,
                            $insignia,
                            $tutoralumno->dispositivo
                            );
                        }
                      
                  }
	}
	
	
	public function sendNotification($titulo, $mensaje, $token, $id, $dispositivo){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

    }
	
}
