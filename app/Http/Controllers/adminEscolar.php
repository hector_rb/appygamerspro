<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\grupo;
use App\materia;
use App\periodo;
use App\grado;
use App\ciclo;
use App\profesor_materia;

class adminEscolar extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();


        $prof=DB::table('profesor')->where('id_institucion','=',$id)->where('estatus','=',0)->get();
        return view('Admin.adminEscolar',['niv'=>$niv,'prof'=>$prof]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGrados(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->idNivel;
        $grados= DB::table('grado')->select('grado.grado','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','grado.idNivel')
        ->where('grado.idInstitucion','=',$id)        
        ->where('grado.idNivel','=',$idNivel)->get();
        return $grados;

    }


    public function getPeriodos(Request $request)
    {
        $user=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')->where('id_usuario','=',$user)->first();
        $nivel=$request->idNivel;
        $periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','periodo.idNivel')
        ->where('periodo.idNivel','=',$nivel)
        ->where('periodo.idInstitucion','=',$inst->idInstitucion)
        ->get();
        return $periodo;
    }

    public function getMaterias(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->idNivel;
        $grado=$request->grado;
        $materia= DB::table('materia')->select('materia.idMateria','materia.nombreMateria','materia.grado' ,'niveles.nombreNivel','materia.matricula')
        ->join('niveles','niveles.idNivel','=','materia.idNivel')
        ->where('materia.idInstitucion','=',$id)
        ->where('materia.idNivel','=',$idNivel)
        ->where('grado','=',$grado)
        ->get();
        return $materia;

    }

    public function getGrupos(Request $request)
    {
       $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->idNivel;
        $grado=$request->grado;
        $grupo=DB::table('grupo')
        ->select('grupo.idgrupo','grupo.grado','grupo.grupo','niveles.nombreNivel','grupo.contrasena','grupo.matricula')
        ->join('niveles','niveles.idNivel','=','grupo.idNivel')
        ->where('grupo.idInstitucion','=',$id)
        ->where('grupo.idNivel','=',$idNivel)
        ->where('grupo.grado','=',$grado)
        ->get();
        return $grupo;
    }

    public function saveGrupo(Request $request)
    {
        $l=['A','B','C','D','E','F','G','H','I','J','K','L','M'];
        $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $su = strlen($an) - 1;
        $id=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$id)->first();
        $idinst=$inst->idInstitucion;         
        $gpo= new grupo;
        $gpo->idInstitucion=$idinst;
        $gpo->idNivel=$request->idNivel;
        $gpo->grado=$request->grado;
        $gpo->grupo=$request->grupo;
        $gpo->matricula=$request->matricula;
        $gpo->contrasena=substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1);
        $gpo->save();
        return json_encode($gpo);
    }

    public function saveMateria(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();

        $mat=new materia;
        $mat->idInstitucion=$inst->idInstitucion;
        $mat->idNivel=$request->idNivel;
        $mat->grado=$request->grado;
        $mat->nombreMateria=$request->materia;
        $mat->matricula=$request->matricula;
        $mat->save();

        return json_encode($mat);
    }

    public function savePeriodo(Request $request)
    {
        $id=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$id)->first();
        $idinst=$inst->idInstitucion;
        $po= new periodo;
        $po->idInstitucion=$idinst;
        $po->idNivel=$request->idNivel;
        $po->nombrePeriodo=$request->periodo;                
        $po->save();
        return json_encode($po);
    }

    public function saveGrado(Request $request)
    {
        $id=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$id)->first();
        $idinst=$inst->idInstitucion;
        $gdo= new grado;
        $gdo->idInstitucion=$idinst;
        $gdo->idNivel=$request->idNivel;
        $gdo->grado=$request->grado;
        $gdo->save();
        return json_encode($gdo);
    }

    public function saveCiclo(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $ciclo= new ciclo;
        $ciclo->nombreCiclo=$request->ciclo;
        $ciclo->inicioCiclo=$request->inicio;
        $ciclo->finCiclo=$request->fin;
        $ciclo->idInstitucion=$id;
        $ciclo->idNivel=$request->idNivel;
        $ciclo->save();
        return json_encode($ciclo);
    }

    public function setCiclo(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $cam=DB::table('ciclo')->select('idCiclo')
        ->where('idInstitucion','=',$id)
        ->where('idNivel','=',$request->idNivel)
        ->get();
        foreach ($cam as $c) {
            $ciclo=ciclo::findOrFail($c->idCiclo);
            $ciclo->status=0;
            $ciclo->update();
        }
        $ciclo=ciclo::findOrFail($request->ciclo);
        $ciclo->status=1;
        $ciclo->update();
        return $ciclo;
    }

    public function setMatProf(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();

        foreach ($request->materia as $m) {
            $val=DB::table('materia_profesor as mp')
            ->join('profesor','profesor.idprofesor','=','mp.idProfesor')
            ->where('profesor.idprofesor','=',0)
            ->where('mp.idProfesor','=',$request->prof)
            ->where('mp.idMateria','=',$m)
            ->first();
            if($val)
            {
                $val=DB::table('materia_profesor')->select('materia.nombreMateria')
                ->join('materia','materia.idMateria','=','materia_profesor.idMateria')
                ->where('materia_profesor.idProfesor','=',$request->prof)
                ->where('materia_profesor.idMateria','=',$m)
                ->first();
                $status=[$val,'s'=>1];
                return json_encode($status);
            }
        }
        
        foreach ($request->materia as $m) {
            # code...
            $mat=new profesor_materia;
            $mat->idInstitucion=$inst->idInstitucion;
            $mat->idProfesor=$request->prof;
            $mat->idMateria=$m;
            $mat->save();
        }
        $status=[$mat,'s'=>0];
        return json_encode($status);
    }

    public function updateGrados(Request $request)
    {
        DB::table('calendario')->where('idCalendario', '=', $eventid)
        ->update(['desc' => $desc]);

    }

    public function updatePeriodo(Request $request)
    {
        $po= periodo::findOrFail($request->idPeriodo);
        
        $po->nombrePeriodo=$request->periodo;                
        $po->update();
        return json_encode($po);

    }

    public function updateMateria(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();

        $mat= materia::findOrFail($request->idMateria);
        $mat->nombreMateria=$request->materia;
        $mat->save();

        return json_encode($mat);
    }

    public function updateGrupo(Request $request)
    {
        $gpo= grupo::findOrFail($request->idGrupo);        
        $gpo->grupo=$request->grupo;
        $gpo->update();
        return json_encode($gpo);
    }

    public function deleteGrado(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $val=DB::table('grupo')
            ->where('grado','=',$request->grado)
            ->where('idInstitucion','=',$inst->idInstitucion)
            ->where('idNivel','=',$request->idNivel)
            ->first();
            if($val)
            {
                
                $status=[$val,'s'=>1];
                return json_encode($status);
            }
            else
            {
                DB::table('grado')
                ->where('grado','=',$request->grado)
                ->where('idInstitucion','=',$inst->idInstitucion)
                ->where('idNivel','=',$request->idNivel)->delete();
                $status=[$val,'s'=>0];
                return json_encode($status);
            }

    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
