<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Graficas;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\GraficasFormRequest;
use DB;
use Auth;

class GraficasTutorController extends Controller
{
    //

			public function __construct()
				{
					
				}
				public function index(Request $request)
			{
				$a=session('idTutelado','');
		        if($a=='')
		        {
		            $id=Auth::user()->id;
		            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
		            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','alumno.idInstitucion','users.imagen','alumno.idgrupo')
		            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
		            ->join('users','users.id','=','alumno.iduser')
		            //->where('tutoralumno.idtutor','=',$tutor->idtutor)
		            ->where('tutoralumno.idalumno','=','alumno.idAlumno')->get();
		            if(count ($alum)>0)
		            {
		                session(['idTutelado', $alum[0]->idalumno]);
		                $a=session('idTutelado');
		            }
		            else
		            {
		                $a=0;
		            }
		        }

		        $idalumno=$a;

		        $user=Auth::user()->id;
		        $tutor=DB::table('tutor')->select('idtutor')->where('iduser','=',$user)->first();
				$tipo=Auth::user()->tipo_user;
				$alumno=DB::table('tutoralumno')->select('alumno.idInstitucion','alumno.idgrupo')
		            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
		            //->where('tutoralumno.idtutor','=',$tutor->idtutor)
		            ->get();
		        if (count($alumno)>0)
		        {
    				$avisos=DB::table('avisos')->select('avisos.fecha','avisos.idInstitucion','avisos.idgrupo')
    		        ->where('avisos.idInstitucion','=',$alumno[0]->idInstitucion)
    		        ->where('avisos.destinatario','=',2)
    		        //->where('avisos.idgrupo','=',$alumno[0]->idgrupo)
    		        ->where('avisos.estatus','=',0)
    		        ->where('avisos.fecha','>=',date('Y-m-d 00:00:00:'))
    		        ->count();
    		        $eventos=DB::table('calendario')->select('calendario.inicio')
    		        ->where('calendario.status','=','1')
    		        ->where('calendario.idInstitucion','=',$alumno[0]->idInstitucion)
    		        ->where('calendario.inicio','>=',date('Y-m-d 00:00:00'))
    		        ->count();
    		        $tareas=DB::table('tareas')->select('tareas.fechaentrega')
    		        ->leftjoin('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
    		        ->where('tareas.condicion','=',1)
    		        ->where('subgrupo.idGrupo','=',$alumno[0]->idInstitucion)
    		        ->where('tareas.fechaentrega','>=',date('Y-m-d 00:00:00'))
    		        ->count();
    		        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$user ],['TipoDestinatario',$tipo]])->count();
		        }
		        else
		        {
		            $sinver=0;
		            $avisos=0;
		            $eventos=0;
		            $tareas=0;
		        }
				//echo ($tareas);
		    
			return view('Tutor.graficas.index',["sinver"=>$sinver, "avisos"=>$avisos, "eventos"=>$eventos, "tareas"=>$tareas]);
		
		}
			public function create(Request $request)
				{
					
				}

			public function show($id)
				{
				return view("Tutor.graficas.show",["graficas"=>Graficas::findOrFail($id)]);
						
				}
			public function edit($id)
					{
				return view("Tutor.graficas.edit",["graficas"=>Graficas::findOrFail($id)]);
						
					}
			public function update(CalificacionesFormRequest $request,$id)
			{

			}
			public function destroy($id)
			{

			}

}