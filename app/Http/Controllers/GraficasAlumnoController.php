<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Graficas;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\GraficasFormRequest;
use DB;
use Auth;

class GraficasAlumnoController extends Controller
{
    public function __construct()
				{
					
				}
			public function index(Request $request)
			{
				$user=Auth::user()->id;
				$tipo=Auth::user()->tipo_user;
				$alumno=DB::table('alumno')->select('alumno.idinstitucion','alumno.idgrupo')
				//->join('subgrupo','subgrupo.idGrupo','=','alumno.idgrupo')
				->where('iduser','=',$user)
				->first();
		        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$user ],['TipoDestinatario',$tipo]])->count();
		        $avisos=DB::table('avisos')->select('avisos.fecha')
		        ->where('avisos.idInstitucion','=',$alumno->idinstitucion)
		        ->where('avisos.idgrupo','=',$alumno->idgrupo)
		        ->where('avisos.estatus','=',0)
		        ->where('avisos.fecha','>=',date('Y-m-d'))
		        ->count();
		        $tareas=DB::table('tareas')->select('tareas.fechaentrega','tareas.idSubgrupo','subgrupo.idGrupo')
		        ->leftjoin('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
		        ->where('tareas.condicion','=',1)
		        ->where('subgrupo.idGrupo','=',$alumno->idgrupo)
		        ->where('tareas.fechaentrega','>=',date('Y-m-d'))
		        ->count();
		        $eventos=DB::table('calendario')
		        ->select('calendario.inicio')
		        ->where('calendario.tipoDestinatario','=',4)
		        ->where('calendario.status','=','1')
		        ->where('calendario.inicio','>=',date('Y-m-d'))
		        ->count();
		
			//echo ($tareas);
			return view('Alumno.graficas.index',["sinver"=>$sinver, "avisos"=>$avisos, "eventos"=>$eventos, "tareas"=>$tareas]);
		
		}
			public function create(Request $request)
				{
					
				}

			public function show($id)
				{
				return view("Alumno.graficas.show");
						
				}
			public function edit($id)
					{
						
					}
			public function update(CalificacionesFormRequest $request,$id)
			{

			}
			public function destroy($id)
			{

			}
}
