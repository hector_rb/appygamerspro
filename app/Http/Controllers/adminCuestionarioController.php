<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\pregunta;
use App\cuestionario;
use App\juegoCuestionario;

class adminCuestionarioController extends Controller
{
    public function index()
    {
       
    }

    public function agregarCuestionario()
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        $preguntas=DB::table('juego-preguntas')->where('idInstitucion','=',$id)->get();
        return view('Admin.AppyGamers.Cuestionarios.agregarCuestionario',["niv"=>$niv],['preguntas'=>$preguntas]);
    }

    public function getGruposCuestionarios(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->id;
        $grupo= DB::table('grado')->select('grado.grado','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','grado.idNivel')
        ->where('grado.idInstitucion','=',$id)        
        ->where('grado.idNivel','=',$idNivel)->get();
        return $grupo;
    }

    public function getCuestionario()
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        $cuestionario=cuestionario::where('idinstitucion',$id)->get();
        $cat=DB::table("juego-categoria")->get();
        return view('Admin.AppyGamers.Cuestionarios.verCuestionario', ["cuestionario"=>$cuestionario,"niv"=>$niv,"cat"=>$cat]);
    }

    public function editarCuestionario(request $request,$idR)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $nivel= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        // $idR=$request->id;
        $cuestionario=cuestionario::where('id_cuestionario',$idR)->get();
        $preguntas=cuestionario::find($idR)->preguntas->all();
        $pre=DB::table('juego-preguntas')->where('idInstitucion','=',$id)->get();
        $cat=DB::table("juego-categoria")->get();
        return view('Admin.AppyGamers.Cuestionarios.editarCuestionario',["nivel"=>$nivel,"cuestionario"=>$cuestionario,"preguntas"=>$preguntas,"pre"=>$pre,"id"=>$id,"cat"=>$cat]);
    }
    
    public function editarCuestionarioDos(request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $nivel= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        // $idR=$request->id;
        $cuestionario=cuestionario::where('id_cuestionario',$request->id)->get();
        $preguntas=cuestionario::find($request->id)->preguntas->all();
        $pre=DB::table('juego-preguntas')->where('idInstitucion','=',$id)->get();
        $cat=DB::table("juego-categoria")->get();
        return view('Admin.AppyGamers.Cuestionarios.editarCuestionario',["nivel"=>$nivel,"cuestionario"=>$cuestionario,"preguntas"=>$preguntas,"pre"=>$pre,"id"=>$id,"cat"=>$cat]);
    }
    
    public function validarCasos(request $request)
    {   
        $input=$request->all();
        $this->editadoCuestionario($request);
        
        if($request->registradas=="on" && $request->preguntaR)
        {
           $this->editadoAgregadasCuestionario($request);
        }
        if($request->nuevas=="on")
        {
           $this->editadoNuevasCuestionario($request);
        }
        return redirect()->route('cuestionario.editar',['id'=>$request->id_cuestionario]);
    }


    public function editadoCuestionario(request $request)
    {
       $input=$request->all();
       $id_editado=$request->id_cuestionario;
       $editado=cuestionario::where('id_cuestionario',$id_editado)->first();
       $editado->update([
            'descripcion'   =>$input['descripcion'],
            'fecha_inicio'  =>$input['fInicio'],
            'fecha_fin'     =>$input['fFinal'], 
            'categoria'     =>$input['materia'],
       ]);
    }
    
    public function buscar(Request $request)
    {
        $input=$request->all();
        $resultado=DB::table("juego-cuestionario")
        ->where(function ($query) use ($input) {
            foreach($input as $clave => $valor)
            {
                if($valor!=null){
                    if($clave == "activo")
                    {
                        switch($valor)
                        {
                            case 1 :
                                $query->where("fecha_fin",">",Carbon::now()->format("Y-m-d"));
                                break;
                            case 0:
                                $query->where("fecha_fin","<=",Carbon::now()->format("Y-m-d"));
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                         $query->where($clave,$valor);
                    }
                }
            }
        })
        ->get();
        return response()->json($resultado);
    }
    
    public function destroy(Request $request, $id)
    {
        $input=$request->all();
        $cuestionario=$request->id_cuestionario;
        $consulta= DB::table("juego-preguntas-cuestionario")->where([["id_cuestionario","=",$cuestionario],["id_pregunta",'=',$id]])->delete();
        if($consulta)
        {
		  return response()->json('Exito');
        }
        else
        {
          return response()->json('Error');
        }
        return redirect()->route('cuestionario.editar',['id'=>$request->id_cuestionario]);
    }

    public function eliminarPregunta(request $request)
    {
        $input=$request->all();
        $cuestionario=$request->id_cuestionario;
        $pregunta=$request->id_pregu;
        DB::table('juego-preguntas-cuestionario')
        ->where('id_cuestionario','=',$cuestionario)
        ->where('id_pregunta','=',$pregunta)
        ->delete();
        return redirect()->route('cuestionario.editar',['id'=>$request->id_cuestionario]);
    }

    public function editadoAgregadasCuestionario(request $request)
    {
        $input=$request->all();
        foreach ($input['preguntaR'] as $recorrido) 
        {
                $juegoCuestionario = new juegoCuestionario();
                $juegoCuestionario->fill([
                'id_cuestionario'=>$request->id_cuestionario,
                'id_pregunta'    =>$recorrido,
                    ]);
                $juegoCuestionario->save();
                unset($juegoCuestionario);
        }   
    }

    public function editadoNuevasCuestionario(request $request)
    {
        $input=$request->all();
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $var=sizeof($input['pregunta']);
        for($i=0;$i<$var;$i++)
        {
          $pregunta= new pregunta();
          $pregunta->fill([
            'idInstitucion'=>$input['institucion'],
            'grado'        =>$input['grado'],
            'id_nivel'     =>$input['nivel'],
            'pregunta'     =>$input['pregunta'][$i],
            'opcion1'      =>$input['op1'][$i],
            'opcion2'      =>$input['op2'][$i],
            'opcion3'      =>$input['op3'][$i],
            'opcion4'      =>$input['op4'][$i],
            'respuesta'    =>$input['respuesta'.$i],
            'id_categoria' =>$input['categoria'],
          ]);
         $pregunta->save();
         $juegoCuestionario = new juegoCuestionario();
         $juegoCuestionario->fill([
         'id_cuestionario'=>$input['id_cuestionario'],
         'id_pregunta'    =>$pregunta->id_pregunta,
             ]);
         $juegoCuestionario->save();
         unset($juegoCuestionario);
         unset($pregunta);
        } 
    }
    
    public function getPreguntasCategoria(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idCat=$request->Cat;
        $pregunta= DB::table('juego-preguntas')->where('id_categoria','=',$idCat)->where("idInstitucion","=",$id)->get();
        return $pregunta;
    }
}
