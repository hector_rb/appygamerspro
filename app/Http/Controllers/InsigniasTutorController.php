<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Insignias;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\InsigniasFormRequest;
use DB;
use Auth;
class InsigniasTutorController extends Controller
{
    public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		
		$a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)
            ->where('tutoralumno.idalumno','=','alumno.idAlumno')->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }

        $idalumno=$a;
        $insignias= DB::table('Insignias as insignias')
        ->select('insignias.id','insignias.idTipoInsignia','insignias.idReceptor','insignias.idEmisor','insignias.idSubGrupo','insignias.fecha','tiposinsignia.NombreInsignia','tiposinsignia.rutaImagen','profesor.nombreprof','profesor.apepat','profesor.apemat','materia.nombreMateria')
        ->join('subgrupo','subgrupo.idSubgrupo','=','insignias.idSubGrupo')
        ->join('tiposinsignia','tiposinsignia.idTipoInsignia','=','insignias.idTipoInsignia')
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->join('profesor','profesor.idprofesor','=','insignias.idEmisor')
        ->where('insignias.idReceptor','=',$idalumno)
        ->where('insignias.tipoReceptor','=',0)
        ->orderBy('insignias.id','desc')
        ->get();

        $insigniasgpo=DB::table('Insignias as insignias')
        ->select('insignias.id','insignias.idTipoInsignia','insignias.tipoReceptor','insignias.idEmisor','insignias.idSubGrupo','insignias.fecha','subgrupo.idSubgrupo','profesor.nombreprof','profesor.apepat','profesor.apemat','materia.nombreMateria','tiposinsignia.rutaImagen')
        ->join('tiposinsignia','tiposinsignia.idTipoInsignia','=','insignias.idTipoInsignia')
        ->join('subgrupo','subgrupo.idSubgrupo','=','insignias.idSubGrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->join('profesor','profesor.idprofesor','=','insignias.idEmisor')
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
        ->join('tutoralumno','tutoralumno.idalumno','=','alumno.idAlumno')
        ->where('alumno.idAlumno','=',$idalumno)
        ->where('insignias.tipoReceptor','=',1)
        ->get();
		return view ("Tutor.insignias.index",["insignias"=>$insignias,"insigniasgpo"=>$insigniasgpo]);
	}

	public function create()
	{
		return view("Tutor.insignias.create");
	}
	
	public function store(Request $request)
	{
		return Redirect::to('/tutor/insignias');
	}
	
	
	public function show($id)
	{
		return view("tutor.insignias.show");

	}
	
	
	public function edit($id)
	{
		return view("tutor.insignias.edit");
		
	
	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		
	
	}
}
