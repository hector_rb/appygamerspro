<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class eventosTutorController extends Controller
{
    public function index()
    {
        $a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','alumno.idInstitucion','users.imagen','alumno.idgrupo')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)
            ->where('tutoralumno.idalumno','=','alumno.idAlumno')->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }
        $iduser=$a;
        
        $query = DB::table('calendario')
        ->join('alumno', 'alumno.idinstitucion','=','calendario.idInstitucion')
        ->where('alumno.idAlumno','=',$iduser)
        ->where('status','=',1)
        ->get();
        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $array[] = ['id'=>$q->idCalendario, 'title'=>$q->titulo, 'description' => $q->desc, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        else
        {
        $array=[];

        }
        //echo($query);
        return view('Tutor.eventos.index',['array'=>$array]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
