<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profesor;
use DB;
use Auth;

class profesorDashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $id=Auth::user()->id;
        /*$p=DB::table('profesor')->where('iduser','=',$id)->first();
        $q=profesor::find($p->idprofesor);
        $inst=$q->institucion()->get();
        dd($q);*/
        $array=DB::table('profesor')->join('institucion', function ($join) use ($id)  {
                $join->on('institucion.idInstitucion','=','profesor.id_institucion')
                     ->where('iduser','=',$id);
                })->get();
        
        return redirect('profesor/graficas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
