<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Mensaje as Mensaje;
use DB;

class TutorMensajes extends Controller
{
    public function index() //muestra los mensajes recibidos, si se modifica algoa qui se debe copiar en recibidos()
    {
         $a=session('idTutelado',''); //permite obtener el alumno que el tutor selecciono
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }
        
        
        $query=DB::table('mensajes')->latest()->get();
        $usuario=Auth::user()->id;
        
        $tutor=DB::table('tutor')->select('idtutor')->where('iduser',$usuario)->get()->first();
        $hijos=DB::table('tutoralumno')
                ->join('alumno','tutoralumno.idalumno','=','alumno.idAlumno')
                ->select('tutoralumno.idalumno','nombreAlumno')
                ->where('idtutor',$tutor->idtutor)->get();
        $alumno=DB::table('tutoralumno')->select('idalumno')->where('idtutor',$tutor->idtutor)->get()->first();
        $idAlumno=DB::table('alumno')->select('iduser','idgrupo','idinstitucion')->where('idAlumno',$a)->get()->first();//en base al hijo que se seleccion se realizan las busqueda de alumno
        
        $grado=DB::table('grupo')->select('grado','idNivel')->where('idgrupo',$idAlumno->idgrupo)->get()->first();//su grado
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();//contador de mensajes sin ver
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0'],['idInstitucion',$idAlumno->idinstitucion]])//filtros de tutor por medio del hijo seleccionado
                            ->orWhere([ ['TipoDestinatario','11'],['idInstitucion',$idAlumno->idinstitucion]]) // en caso de que se reciba un mensaje de todos los tutores de la institucion
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$idAlumno->idinstitucion]]) //para todos en la institucion
                            ->orWhere([ ['TipoDestinatario','7'],['idDestinatario',$idAlumno->idgrupo]]) //por grupo del hijo seleccionado
                            ->orWhere([ ['TipoDestinatario','6'],['idDestinatario',$grado->idNivel.'-'.$grado->grado],['idInstitucion',$idAlumno->idinstitucion]]) //por grado del hijo seleccionado 
                            ->orWhere([ ['TipoDestinatario','4'],['IdDestinatario',$idAlumno->iduser]]) //mensaje para el hijo 
                            ->orderBy('created_at', 'desc')->get();
        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Tutor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver','hijos'));
    }

    public function papelera($id) //metodo que permite enviar a la papelera
    {
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Eliminado' => 1]);
            return redirect()->back();

    }

    public function desaparece($id)//metodo que se puede implementar para no ver los mensajes eliminados
    {
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Eliminado' => 2]);
            //return redirect()->back();

    }

    public function create($id)//no se utiliza actualmente
    {
        $mensaje = Mensaje::find($id);
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Eliminado' => 1]);
        return redirect()->back();
    }

    public function store(Request $request)
    {

    }

    public function show($id) //metodo que muestra el mensaje seleccionado
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Visto' => 1]);
            
            
            
        $a=session('idTutelado',''); //obteiene el hijo seleccionado
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }
        


         $var=DB::table('alumno')->select('idAlumno as id','idgrupo')->where('idAlumno',$a)->get()->first(); //en base al hijo seleccionado obtiene su grado grupo nivel etc.
        $migrupo=$var->idgrupo;
        $var2=DB::table('grupo')->select('grado','idNivel')->where('idgrupo',$migrupo)->get()->first();
        $minivel=$var2->idNivel;
        $migrado=$var2->grado;
        
    
        $mensaje= Mensaje::find($id);
        switch ($mensaje->TipoEmisor) //quien lo envia
         {
             case '1':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
             case '2':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
             case '3':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
             case '4':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
        }

        switch ($mensaje->TipoDestinatario)// para quien va dirigido
         {
            case '1':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '2':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '3':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '4':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '5':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '6':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Escolaridad: ' ,idNivel, ' Grado: ', grado) as Nombre,grado as id,idNivel"))->distinct()->where([['grado',$mensaje->IdDestinatario],['idNivel',$minivel] ])->get();
            break;
            case '7':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->where('idgrupo',$mensaje->IdDestinatario)->get();
            break;
            case '8':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos') as Nombre,idgrupo as id"))->where('idgrupo','2')->get();
            break;
             case '9':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Profesores') as Nombre"))->distinct()->get();
            break;
             case '10':
                $variable2=DB::table('niveles')->select('nombreNivel as Nombre')->where('idNivel',$minivel)->get();
            break;
             case '11':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos los tutores') as Nombre,idgrupo as id"))->where('idgrupo','1')->get();
            break;
            case '12':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Administradores') as Nombre,idgrupo as id"))->where('idgrupo','1')->get();
            break;
            
        }
        return \View::make('Tutor.Mensajes.mensajeShow',compact('mensaje','variable','variable2','sinver'));
    }

    public function edit($id)
    {
        //
    }

    public function search(Request $request)
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        $mensajes = Mensaje::where([['Asunto','like','%'.$request->Asunto.'%'],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->get();
        $alumno=DB::table('alumno')->select('idAlumno as id','nombreAlumno as Nombre')->get();
        $profesor=DB::table('profesor')->select('idprofesor as id','nombreprof as Nombre')->get();
        return \View::make('Tutor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    public function update(Request $request, $id)
    {
        //
    }
    public function recibidos() //mensajes recibidos, si se modifica algo aqui se debe copiar en index()
    {
        $a=session('idTutelado',''); //obtiene el hijo seleccionado
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }
        
        
        $query=DB::table('mensajes')->latest()->get();
        $usuario=Auth::user()->id;
        
        $tutor=DB::table('tutor')->select('idtutor')->where('iduser',$usuario)->get()->first();
        $hijos=DB::table('tutoralumno')
                ->join('alumno','tutoralumno.idalumno','=','alumno.idAlumno')
                ->select('tutoralumno.idalumno','nombreAlumno')
                ->where('idtutor',$tutor->idtutor)->get();
        $alumno=DB::table('tutoralumno')->select('idalumno')->where('idtutor',$tutor->idtutor)->get()->first();
        $idAlumno=DB::table('alumno')->select('iduser','idgrupo','idinstitucion')->where('idAlumno',$a)->get()->first();// en base al hijo seleccionado obtiene grado grupo nivel etc
        
        $grado=DB::table('grupo')->select('grado','idNivel')->where('idgrupo',$idAlumno->idgrupo)->get()->first();
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0'],['idInstitucion',$idAlumno->idinstitucion]])
                            ->orWhere([ ['TipoDestinatario','11'],['idInstitucion',$idAlumno->idinstitucion]])   //todos los tutores de la institucion                         
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$idAlumno->idinstitucion]])//filtro por institucion
                            ->orWhere([ ['TipoDestinatario','7'],['idDestinatario',$idAlumno->idgrupo]]) //por grupo
                            ->orWhere([ ['TipoDestinatario','6'],['idDestinatario',$grado->idNivel.'-'.$grado->grado],['idInstitucion',$idAlumno->idinstitucion]]) //por grado
                            ->orWhere([ ['TipoDestinatario','4'],['IdDestinatario',$idAlumno->iduser]]) //por alumno
                            ->orderBy('created_at', 'desc')->get();
        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Tutor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver','hijos'));
    }

    public function eliminados() //permite ver los mensajes eliminados
    {
        
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','1']])->get();
        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Tutor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    public function destroy($id)
    {
        //
    }
    
    public function getMensajes(Request $request) 
    {
        $usuario=Auth::user()->id;
        $tutor=DB::table('tutor')->select('idtutor')->where('iduser',$usuario)->get()->first();
        $idAlumno=DB::table('alumno')->select('iduser','idgrupo','idinstitucion')->where('idAlumno',$request->idalumno)->get()->first();
        $grado=DB::table('grupo')
                    ->select('grado','idNivel')
                    ->where('idgrupo',$idAlumno->idgrupo)
                    ->get()->first();
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])
                            ->orWhere([ ['TipoDestinatario','11'],['idInstitucion',$idAlumno->idinstitucion]])
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$idAlumno->idinstitucion]])
                            ->orWhere([ ['TipoDestinatario','7'],['idDestinatario',$idAlumno->idgrupo]])
                            ->orWhere([ ['TipoDestinatario','6'],['idDestinatario',$grado->idNivel.'-'.$grado->grado],['idInstitucion',$idAlumno->idinstitucion]])
                            ->orWhere([ ['TipoDestinatario','4'],['IdDestinatario',$idAlumno->iduser]])
                            ->orderBy('created_at', 'desc')->get();
        return $mensajes;
    }
        
    
}
