<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Calificaciones;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CalificacionesFormRequest;
use DB;
use Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class CalificacionesController extends Controller
{
     public function __construct()
	{
		
	}
	public function index(Request $request)
	{
		
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();

		return view('Profesor.calificaciones.index',["sg"=>$sg]);
	
	
	}
	public function create()
	{
		/*$alumno=DB::table('alumno')->where('activo','=','A')->get();
		$grupo=DB::table('grupo')->get();*/
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)->where('ciclo.status','=',1)
		->get();

		return view("Profesor.calificaciones.create",["sg"=>$sg]);
	}

	public function getPeriodosProfesor(Request $request)
	{
		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$user)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->where('idNivel','=',$grupo->idNivel)
		->where('idInstitucion','=',$prof->id_institucion)
		->get();
		return $periodo;
	}

	public function getPeriodosCalificados(Request $request)
	{
		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$user)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->leftjoin('calif','calif.idPeriodo','=','periodo.idPeriodo')
		->where('periodo.idNivel','=',$grupo->idNivel)
		->where('periodo.idInstitucion','=',$prof->id_institucion)
		->where('calif.idSubgrupo','=',$sg)
		->groupBy('periodo.idPeriodo')
		->get();
		return $periodo;

		//SELECT * FROM periodo  left join calif on periodo.idPeriodo = calif.idPeriodo WHERE periodo.idNivel = 4 AND periodo.idInstitucion = 1 AND calif.idcalif IS NULL GROUP BY periodo.idPeriodo
	}

	public function getPeriodosNoCalificados(Request $request)
	{
		//SELECT periodo.idPeriodo, periodo.nombrePeriodo , calif.idcalif FROM periodo JOIN institucion on periodo.idInstitucion = institucion.idInstitucion 
		//JOIN grupo on institucion.idInstitucion = grupo.idInstitucion 
		//JOIN subgrupo on grupo.idgrupo = subgrupo.idGrupo 
		//LEFT JOIN calif ON periodo.idPeriodo = calif.idPeriodo AND subgrupo.idSubgrupo = calif.idSubgrupo 
		//WHERE periodo.idInstitucion = 1 AND subgrupo.idSubgrupo = 1 AND periodo.idNivel = 4 and calif.idcalif is null 
		//GROUP BY periodo.idPeriodo

		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$user)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->join('institucion','institucion.idInstitucion','=','periodo.idInstitucion')
		->join('grupo','institucion.idInstitucion','=','grupo.idInstitucion')
		->join('subgrupo','subgrupo.idGrupo','=','grupo.idgrupo')
		->leftJoin('calif', function ($join)   {
            $join->on('periodo.idPeriodo','=','calif.idPeriodo')
            	 ->on('subgrupo.idSubgrupo','=','calif.idSubgrupo');
        	})
		->where('periodo.idInstitucion','=', $prof->id_institucion)
		->where('subgrupo.idSubgrupo','=',$sg)
		->where('periodo.idNivel','=',$grupo->idNivel)
		->where('calif.idcalif','=',NULL)
		->groupBy('periodo.idPeriodo')
		->get();

		return $periodo;

	}

	public function getAlumnoCalifProfesor(Request $request)
	{
		
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idgrupo')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo','alumno.matricula')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idgrupo','=',$grupo->idgrupo)
		->get();

		return $alum;
	}

	public function getCalificacionesProfesor(Request $request)
	{
		$sg=$request->idsg;
		$periodo=$request->idP;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		/*$grupo=DB::table('grupo')->select('idgrupo')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idgrupo','=',$grupo->idgrupo)
		->get();*/
		$calif=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','calif.calif','calif.idcalif','alumno.matricula')
		->leftJoin('calif', function ($join) use ($sg,$periodo)  {
            $join->on('alumno.idAlumno','=','calif.idAlumno')
                 ->where('calif.idSubgrupo', '=', $sg)
                 ->where('calif.idPeriodo','=',$periodo);
        	})
		->where('alumno.idgrupo','=',$subgrupo->idGrupo)
		->groupBy('idAlumno')
		->orderBy('alumno.apepat')
		->get();
		return $calif;
	}

	public function store(CalificacionesFormRequest $request)
	{
		
		$idSubgrupo = $request->idSubgrupo;
		$fecha=$request->fecha;
		$periodo=$request->periodo;
		if(count($request->calif)>0)
		{
			foreach($request->calif as $key => $c ) {
					
					$calificaciones=new Calificaciones;
					$calificaciones->idSubgrupo=$idSubgrupo;
					$calificaciones->idalumno=$request->id[$key];
					$calificaciones->calif=$c;
					$calificaciones->fecha=$fecha;
					$calificaciones->idPeriodo=$periodo;
					$this->avisar($request->id[$key], "Nueva calificación", "hay una nueva calificación");
					$calificaciones->save();
	  			
			}
		}

		
		return Redirect::to('/profesor/calificacion');
	}
	public function show($id)
	{
		
		
	}
	public function edit($id, Request $request)
	{
		$sg=$id;
		$periodo=$request->periodo;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		/*$grupo=DB::table('grupo')->select('idgrupo')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idgrupo','=',$grupo->idgrupo)
		->get();*/
		$calif=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','calif.calif','calif.idcalif')
		->leftJoin('calif', function ($join) use ($sg,$periodo)  {
            $join->on('alumno.idAlumno','=','calif.idAlumno')
                 ->where('calif.idSubgrupo', '=', $sg)
                 ->where('calif.idPeriodo','=',$periodo);
        	})
		->where('alumno.idgrupo','=',$subgrupo->idGrupo)
		->get();
		return view('Profesor.calificaciones.edit',['calif'=>$calif,'sg'=>$sg,'periodo'=>$periodo]);
	
	}
	public function update(Request $request,$id)
	{
		/*$calificaciones=new Calificaciones;
		$calificaciones->idmateria=$request->get('idmateria');
		$calificaciones->idalumno=$request->get('idalumno');
		$calificaciones->calif=$request->get('calif');
		$calificaciones->desc=$request->get('desc');
		$calificaciones->fecha=$request->get('fecha');
		$calificaciones->save();
		return Redirect::to('/profesor/calificaciones');*/
		
		$idSubgrupo = $id;
		$fecha= date("Y-m-d");
		$periodo=$request->periodo;
		foreach($request->calif as $key => $c ) {
					
				if($request->id[$key]=="")
				{
					$calificaciones=new Calificaciones;
					$calificaciones->idSubgrupo=$idSubgrupo;
					$calificaciones->idalumno=$request->iduser[$key];
					$calificaciones->calif=$c;
					$calificaciones->fecha=$fecha;
					$calificaciones->idPeriodo=$periodo;
					$this->avisar($request->id[$key], "Nueva calificación", "hay una nueva calificación");
					$calificaciones->save();
				}
				else
				{
					$calificaciones=Calificaciones::findOrFail($request->id[$key]);
					$calificaciones->calif=$c;
					$this->avisar($calificaciones->idalumno, "Nueva calificación", "hay una nueva calificación");
					$calificaciones->update();
				}
  			
		}
		
	return Redirect::to('/profesor/calificacion');
	
	}
	public function destroy($id)
	{
		$calificaciones=Calificaciones::findOrFail($id);
		$calificaciones->update();
		return Redirect::to('/profesor/calificacion');
	
	}
	
		public function avisarInsignia($alumno)
	{
	    
	    $idalumno = DB::table("alumno")->select('alumno.idAlumno')
	    ->where('alumno.iduser',$alumno)
	    ->first();
	     $tutoralumno = DB::table("tutoralumno")->select('tutor.*', 'alumno.nombreAlumno')
                  ->join('tutor', 'tutoralumno.idtutor', '=', 'tutor.idtutor')
                  ->join('alumno', 'tutoralumno.idalumno', '=', 'alumno.idAlumno')
                  ->where('tutoralumno.idalumno', $idalumno->idAlumno)
                  ->first($alumno);
                  //dd($alumno);
                 // echo ($user->idAlumno);
                 //echo json_encode($tutoralumno);
                 // echo "<br><br>";
                 //dd($insignia);
                  if($tutoralumno){
                      
                       if($tutoralumno->tokenFCM!="0"){
                     
                             $this->sendNotification(
                             "Mensaje", 
                             $tutoralumno->nombreAlumno . " Ha recibido un mensaje nuevo", 
                            $tutoralumno->tokenFCM,
                            0,
                            $tutoralumno->dispositivo
                            );
                        }
                      
                  }
	}
	
		public function avisar($alumno, $titulo, $mensaje)
	{
	    //echo(" alumno ");
	    //echo($alumno);
	$tutoralumno = DB::table("tutoralumno")->select('tutor.*', 'alumno.nombreAlumno')
                  ->join('tutor', 'tutoralumno.idtutor', '=', 'tutor.idtutor')
                  ->join('alumno', 'tutoralumno.idalumno', '=', 'alumno.idAlumno')
                  ->where('tutoralumno.idalumno',$alumno)
                  ->first();
                 // echo ($user->idAlumno);
                 //echo json_encode($tutoralumno);
                 //echo "<br><br>";
                  if($tutoralumno){
                      
                       if($tutoralumno->tokenFCM!="0"){
                            //echo ($tutoralumno->dispositivo);
                             $this->sendNotification(
                             $titulo, 
                             $mensaje, 
                             $tutoralumno->tokenFCM,
                             0,
                             $tutoralumno->dispositivo
                            );
                        }
                      
                  }
	
	}
	
	
	public function sendNotification($titulo, $mensaje, $token, $id=0, $dispositivo){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($titulo)
            		->setBody($mensaje);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        if($dispositivo=="ios")
        {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        }
        else
        {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

    }
}
