<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;

class preguntasjuegoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPreguntasJuego(Request $request)
    {
        $input= $request->all();
        $usuario=DB::table('alumno')
                 ->where('idAlumno','=',$input['idusuario'])
                 ->first();
        
        if($request->categoria==0)
        {
            $preguntas=DB::table('juego-preguntas')
                        ->where('idinstitucion','=',$usuario->idinstitucion)
                        ->where('grado','=',$usuario->idgrupo)
                        ->get();
        }
        else
        {
            $preguntas=$preguntas=DB::table('juego-preguntas')
                        ->where('idinstitucion','=',$usuario->idinstitucion)
                        ->where('grado','=',$usuario->idgrupo)
                        ->where('id_categoria','=',$input['categoria'])
                        ->get();
        }
        
        return response()->json(['Items'=>$preguntas]);
    }   
    public function guardaRespuesta(Request $request)
    {
        $pre=DB::table('juego-respuestas')->insert([
            'id_usuario' => $request->idusuario,
            'id_pregunta' => $request->pregunta,
            'respuesta' =>$request->respuesta]);
        if($pre)
        {
            return response()->json([$pre],201);
        }
        else
        {
            return response()->json([$pre],401);
        }
    }
    
    public function guardaEstadisticas(Request $request)
    {
        $pre=DB::table('juego-info-general')
             ->where('id_usuario','=',$request->idusuario)
             ->where('id_juego','=',$request->idjuego)
             ->get();
        $record=DB::table("juego-scores")
                ->where('idusuario','=',$request->idusuario)
                ->where('id_categoria','=',$request->categoria)
                ->get();
                
        if(count($pre)>0)
        {
            $tiempo=$pre[0]->tiempo+$request->tiempo;
            $contestadas=$pre[0]->preguntas_contestadas+$request->contestadas;
            $correctas=$pre[0]->correctas+$request->correctas;
            $incorrectas=$pre[0]->incorrectas+$request->incorrectas;
            $actualiza=DB::table('juego-info-general')
                        ->where('id_usuario','=',$pre[0]->id_usuario)       
                        ->update(['tiempo'=>$tiempo,
                                    'preguntas_contestadas'=>$contestadas,
                                    'correctas'=>$correctas,
                                    'incorrectas'=>$incorrectas,
                            ]);
        }
        else
        {
            $actualiza=DB::table('juego-info-general')
                        ->insert([  'id_usuario'=>$request->idusuario,
                                    'id_juego'=>$request->idjuego,
                                    'tiempo'=>$request->tiempo,
                                    'preguntas_contestadas'=>$request->contestadas,
                                    'correctas'=>$request->correctas,
                                    'incorrectas'=>$request->incorrectas,
                                ]);
        }
        if(count($record)>0)
        {
            if($record[0]->score < $request->score)
            {
                DB::table("juego-scores")
                    ->where('idusuario','=',$request->idusuario)
                    ->where('id_categoria','=',$request->categoria)
                    ->update(['score'=>$request->score]);
            }
        }
        else
        {
            DB::table("juego-scores")
                ->insert([
                    'idusuario'=>$request->idusuario,
                    'id_categoria'=>$request->categoria,
                    'score'=>$request->score
                    ]);
        }
        return response()->json("success",201);   
    }
}
