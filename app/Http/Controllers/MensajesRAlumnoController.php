<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\MensajesR;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\MensajesRFormRequest;
use DB;
use Auth;


class MensajesRAlumnoController extends Controller
{
    public function __construct()
	{
		
	}
	
	
	public function index(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        //$id=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
		$mensajes=DB::table('mensajes')->select('mensajes.id','mensajes.Asunto','mensajes.Mensaje','mensajes.TipoDestinatario','mensajes.idDestinatario','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.Visto','mensajes.idInstitucion','mensajes.created_at as fecha','mensajes.updated_at','mensajes.Eliminado','destinatarios.TipoDestinatario as destinatario','users.name as usuarios')
		->join('destinatarios','destinatarios.id','=','mensajes.TipoEmisor')
		->join('users','users.id','=','mensajes.IdEmisor')
		//->where('mensajes.idInstitucion','=',$id)
		->where('mensajes.idDestinatario','=',$iduser)
		->where('mensajes.Eliminado','=','0')
		->orderBy('mensajes.id','desc')
		//->paginate(10);
		->get();
		//->paginate(7);
		return view("Alumno.mensajesR.index", ["mensajes"=>$mensajes, "sinver"=>$sinver]);

	
	}
	public function create(Request $request)
	{
		
		return view("alumno.mensajesR.create");
	}
	public function store(MensajesRFormRequest $request)
	{
			
		return Redirect::to('/alumno/mensajesR');
	}
	
	
	public function show($id)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        //$i=$inst->idInstitucion;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$iduser],['Eliminado','=','0']])->count();
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Visto' => 1]);
        $mensajes=DB::table('mensajes')->select('mensajes.Asunto','mensajes.Mensaje','mensajes.IdEmisor','mensajes.TipoEmisor','mensajes.anexo','mensajes.created_at as fecha','users.name as usuarios','destinatarios.TipoDestinatario as destinatario')
        ->join('users','users.id','=','mensajes.IdEmisor')
        ->join('destinatarios','destinatarios.id','=','mensajes.TipoEmisor')
        ->where('mensajes.id','=',$id)
        //->where('mensajes.idInstitucion','=',$i)
        ->get();
		return view("Alumno.mensajesR.show",["mensajes"=>$mensajes, "sinver"=>$sinver]);

	}

	
	
	public function edit($id)
	{
		return view("");
		
	
	}
	public function update(MensajesRFormRequest $request,$id)
	{
	
	}
	public function destroy($id)
	{
		$mensajesR=MensajesR::findOrFail($id);
		$mensajesR->Eliminado='1';
		$mensajesR->update();
		return Redirect::to('/alumno/mensajesR');
	
	}

}
