<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Tarea;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TareaFormRequest;
use DB;
use Auth;

class TareaAlumnoController extends Controller
{
    
	public function index(Request $request)
	{
		/*$query=trim($request->get('searchText'));
			$tareas=DB::table('tareas as t')
			->join('materia as m','t.idmateria','=','m.idMateria')
			->join('tutoralumno as c', 't.idgrupo','=','c.idgrupo')
			->select('t.idTareas','t.idgrupo','t.fechaentrega','t.titulo','t.descripcion','t.anexo','m.nombre as materia')
			->where('t.titulo','LIKE','%'.$query.'%')
			->where('t.condicion','=','1')
			->orderBy('t.idTareas','desc')
			->paginate(7);*/
		$id=Auth::user()->id;
		$alum=DB::table('alumno')->select('idAlumno')->where('iduser','=',$id)->first();
        $query=trim($request->get('searchText'));
        $tareas=DB::table('tareas')->select('tareas.idTareas','tareas.titulo','tareas.descripcion','materia.nombreMateria','tareas.fechaentrega','tareas.anexo')
        ->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')        
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')        
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->where('tareas.titulo','LIKE','%'.$query.'%')
        ->where('tareas.condicion','=','1')
        ->where('ciclo.status','=',1)
        ->where('alumno.idAlumno','=',$alum->idAlumno)
        ->orderBy('tareas.idTareas','desc')
        ->paginate(7);
		return view ("Alumno.tareas.index" ,["tareas"=>$tareas]);
	
	}
	
	public function create()
	{
		$materia=DB::table('materia')->where('activo','=','1')->get();
		$grupo=DB::table('grupo')->get();
		return view("Alumno.tareas.create",["materia"=>$materia],["grupo"=>$grupo]);
	}
	
	public function show($id)
	{
		$tareas=DB::table('tareas')->select('tareas.idTareas','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tareas.titulo','tareas.descripcion','tareas.anexo','tareas.fechaentrega')
		->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->where('tareas.condicion','=','1')
    ->where('tareas.idTareas',$id)
    ->first();

    return view("Alumno.tareas.show",["tareas"=>$tareas]);
	}
}
