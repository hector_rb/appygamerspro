<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Chats;
use Auth;
use DB;

class ChatProfesorController extends Controller
{
    public function __construct()
	{
	}

	    public function index()
    {
        return view('Profesor.chats.index');
    }

    public function create()
    {
        $iduser=Auth::user()->id;
        $prof=DB::table('profesor')->select('idprofesor','iduser','nombreprof','apepat','apemat')->where('iduser','=',$iduser)->first();
        $sg=DB::table('subgrupo as sg')->select('sg.idGrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel','tutor.idtutor','tutor.iduser','tutor.nombreTutor','tutor.apepat','tutor.apemat','alumno.nombreAlumno','chat.ultimoMsj','chat.timestamp','users.imagen')
        ->join('grupo','grupo.idgrupo','=','sg.idGrupo')
        ->join('materia','materia.idMateria','=','sg.idMateria')
        ->join('niveles','niveles.idNivel','=','grupo.idNivel')
        ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
        ->join('tutoralumno as ta','ta.idalumno','=','alumno.idAlumno')
        ->join('tutor','tutor.idtutor','=','ta.idtutor')
        ->join('chat_p_t as chat','chat.idTutor','=','tutor.iduser')
        ->join('users','users.id','=','tutor.iduser')
        ->where('sg.idProfesor','=',$prof->idprofesor)
        //->orderBy('grupo.grupo')
        //->where('chat.idProfesor','=',$prof->iduser)
        ->groupBy('tutor.idTutor')
        ->orderBy('chat.timestamp','desc')
        ->get();
        /*$chat=DB::table('chat_p_t as chat')->select('chat.idChat','chat.referencia','chat.idProfesor','chat.idTutor','chat.ultimoMsj','chat.timestamp','chat.estatus')
        ->join('profesor','profesor.idprofesor','=','')
        ->where('chat.estatus','=',1)
        ->get();*/
        /*$chat=DB::table('chat_p_t as chat')->select('chat.idChat','chat.referencia','chat.idProfesor','chat.idTutor','chat.ultimoMsj','chat.timestamp')
        ->where('chat.idProfesor','=',$prof->iduser)
        ->get();*/
        //echo($sg);
        echo($sg);
        //return view('Profesor.chats.create',["sg"=>$sg, "prof"=>$prof]);
    }

    public function getChats(Request $request){
        $iduser=Auth::user()->id;
        $prof=DB::table('profesor')->select('idprofesor','iduser')->where('iduser','=',$iduser)->first();
        $sg=$request->id;
        $chats=DB::table('chat_p_t')->where('idProfesor','=',$prof->iduser)->where('idTutor','=',$request->idtutorselec)
        ->update(['ultimoMsj'=>$request->get('mensaje'),'timestamp'=>date("Y-m-d H:i:s")]);
        echo($request->idtutorselec.'<br>');
        echo($request->get('mensaje'));
        //$chats->save();
        //return Redirect::to('/profesor/chats/create');
    }

    public function show($id)
    {
       
        return view('Profesor.chats.show');
    }
 
}
