<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;

class preguntasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $niv= DB::table('niveles')->join('nivel_inst', function ($join) use ($id)  {
                $join->on('nivel_inst.idNivel','=','niveles.idNivel')
                     ->where('idInstitucion','=',$id);
                })->get();
        return view("Admin.Preguntas.preguntas",["niv"=>$niv]);
    }
    
    public function Pregistradas()
    {
        $iduser=Auth::user()->id;
        $inst=DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $preguntas=DB::table('juego-preguntas')->where('idInstitucion','=',$id)->get();
        return view('Admin/Preguntas/registradas',['preguntas'=>$preguntas]);
    }
    
    public function GetPregunta(Request $request)
    {
        if($request->cat==0)
        {
            $preguntas=DB::table('preguntas')->get();
        }
        else
        {
            $preguntas=DB::table('preguntas')->where('materia','=',$request->cat)->get();
        }
        
        return response()->json(['Items'=>$preguntas]);
    }
    public function RegistraPregunta(Request $request)
    {
        $input=$request->all();
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->id;

        $input=$request->all();
        $var=sizeof($input['pregunta']);

            for($i=0;$i<$var;$i++)
        {
             DB::table('juego-preguntas')->insert([
            'idInstitucion'=>$id,
            'grado'        =>$input['selectGrupo'][0],
            'id_nivel'     =>$input['idSubGrupo'],
            'pregunta'     =>$input['pregunta'][$i],
            'opcion1'      =>$input['op1'][$i],
            'opcion2'      =>$input['op2'][$i],
            'opcion3'      =>$input['op3'][$i],
            'opcion4'      =>$input['op4'][$i],
            'respuesta'    =>$input['respuesta'.$i],
            'id_categoria' =>$input['materia'],
            ]);
        }
       return Redirect::to('/admin/Preguntas')->with('message', 'Datos registrados');;
    }

public function getGruposPreguntas(Request $request)
    {
        $iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $id=$inst->idInstitucion;
        $idNivel=$request->id;
        $grupo= DB::table('grado')->select('grado.grado','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','grado.idNivel')
        ->where('grado.idInstitucion','=',$id)        
        ->where('grado.idNivel','=',$idNivel)->get();
        return $grupo;
    }
}
