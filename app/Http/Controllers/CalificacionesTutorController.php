<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Calificaciones;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CalificacionesFormRequest;
use DB;
use Auth;

class CalificacionesTutorController extends Controller
{
	public function index(Request $request)
	{
		$a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }

		$id=$a;

		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->join('grupo', function ($join) use ($id)  {
                $join->on('grupo.idNivel','=','periodo.idNivel');
                $join->on('grupo.idInstitucion','=','periodo.idInstitucion');
                     
                })
		->join('alumno','alumno.idgrupo','=','grupo.idGrupo')
		->where('alumno.idAlumno','=',$id)
		->get();
		return view("Tutor.calificaciones.index",["periodos"=>$periodo]);
	}

	public function getPeriodos(Request $request)
	{
		$id=$request->id;
		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->join('grupo', function ($join) use ($id)  {
                $join->on('grupo.idNivel','=','periodo.idNivel');
                $join->on('grupo.idInstitucion','=','periodo.idInstitucion');                 
                })
		->join('alumno','alumno.idgrupo','=','grupo.idGrupo')
		->where('alumno.idAlumno','=',$id)
		->get();
		return $periodo;
	}


	public function getCalificaciones(Request $request)
	{
		$id=session('idTutelado');
		$periodo=$request->idP;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.idAlumno','alumno.nombreAlumno','alumno.apepat','alumno.apemat','calif.calif','grupo.grado','grupo.grupo','materia.nombreMateria')
		->join('subgrupo','subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('alumno','alumno.idAlumno','=','calif.idAlumno')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->where('calif.idAlumno','=',$id)
		->where('calif.idPeriodo','=',$periodo)
		->get();

		return $calif;

	}
	
    
}
