<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class horarioAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $idalumno=Auth::user()->id;
        $alum= DB::table('alumno')->where('iduser','=',$idalumno)->first();
        $horarios=DB::table('grupo')
        ->select('subgrupo.idGrupo','subgrupo.idCiclo','ciclo.nombreCiclo' ,'grupo.idNivel','niveles.nombreNivel' ,'grupo.grado','grupo.grupo')
        ->join('subgrupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('niveles','grupo.idNivel','=','niveles.idNivel')
        ->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
        ->where('grupo.idgrupo','=',$alum->idgrupo)
        ->groupBy('ciclo.idCiclo')->get();
        return view('Alumno.horario.index',['horarios'=>$horarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        //
        $id2=$request->ciclo;
        $matgrupo= DB::table('subgrupo')
            ->join('materia', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idMateria', '=', 'materia.idMateria')               
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })
            ->join('profesor', function ($join) use ($id,$id2)  {
            $join->on('subgrupo.idProfesor','=','profesor.idprofesor')
                 ->where('subgrupo.idGrupo', '=', $id)->where('subgrupo.idCiclo','=',$id2);
            })->get();

        $query=DB::table('horario')
        ->select('grupo.grado','grupo.grupo','horario.idHorario','profesor.nombreprof','materia.nombreMateria','horario.inicio','horario.fin','horario.clase','niveles.nombreNivel')
                ->join('subgrupo','subgrupo.idSubgrupo','=','horario.idSubgrupo')
                ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
                ->join('profesor','profesor.idprofesor','=','subgrupo.idProfesor')
                ->join('materia','materia.idMateria','=','subgrupo.idMateria')
                ->join('niveles','niveles.idNivel','=','grupo.idNivel')
                ->where('horario.idGrupo','=',$id)
                ->where('horario.status','=',1)
                ->where('horario.idCiclo','=',$request->ciclo)->get();

        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $horario[] = ['id'=>$q->idHorario, 'title'=>$q->nombreMateria, 'description' => $q->nombreprof, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        else
        {
            $horario=[];
        }
        return view('Alumno.horario.show', ['horario'=>$horario,'matgrupo'=>$matgrupo,'idGrupo'=>$id,'idCiclo'=>$id2,'datos'=>$query]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
