<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Encuestas;
use App\Preguntas;
use App\Completar;
use App\Respuestas;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EncuestasFormRequest;
use DB;
use Auth;

class RespuestasAlumnoController extends Controller
{
       

public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		
		return view ("alumno.encuestas.index");
	}

	public function create()
	{
		$iduser=Auth::user()->id;
		$alumno=DB::table('alumno')
		->select('idAlumno')
		->where('iduser','=',$iduser)
		->first();
		$encuestas=DB::table('enc_completa as enc')
		->select('enc.idEncuesta','enc.numPreguntas','encuesta.nombre','encuesta.descripcion')
		->join('encuesta','encuesta.idEncuesta','=','enc.idEncuesta')
		//->where('enc.idEncuestado','=',$tutor->idtutor)
		->where('enc.tipoEncuestado','=',2)
		->where('enc.contestadas','=',0)
		->groupBy('enc.idEncuesta')
		->get();
		return view("Alumno.encuestas.create",["encuestas"=>$encuestas]);
	}

	public function getPreguntasAlumno(Request $request)
	{
		$preguntas=DB::table('enc_preguntas as pre')
		->where('pre.idEncuesta','=',$request->idenc)
		->get();
		return $preguntas;
	}

	public function store(Request $request)
	{

		$iduser=Auth::user()->id;
		$alumno=DB::table('alumno')
		->select('idAlumno')
		->where('iduser','=',$iduser)
		->first();
        $preguntas=DB::table('enc_preguntas as pre')
        ->select('pre.idPregunta')
		->where('pre.idEncuesta','=',$request->encuestas)
		->get();
		
		$v=0;
		foreach ($preguntas as $key) {
			$Respuestas=new Respuestas;		
			$Respuestas->idEncuestado=$alumno->idAlumno;
			$Respuestas->tipoEncuestado=2;
			$Respuestas->respuesta=$request->get('respuesta'.$v);
			$Respuestas->idPregunta=$key->idPregunta;
			$Respuestas->idEncuesta=$request->encuestas;
			$Respuestas->save();
			$v=$v+1;
		}
		DB::table('enc_completa')
		->where('idEncuesta','=',$request->encuestas)
		//->where('idEncuestado','=',$tutor->idtutor)
		->update(['contestadas'=> $v]);
		//echo($Respuestas);
		return Redirect::to('/alumno/encuestas/create');
	}
	
	
	public function show($id)
	{
		return view("tutor.encuestas.show");

	}
	
	
	public function edit($id)
	{
		return view("tutor.encuestas.edit");
		
	
	}
	public function update(InsigniasFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		
	
	}
}
