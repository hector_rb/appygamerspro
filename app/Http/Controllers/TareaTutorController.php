<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Tarea;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TareaFormRequest;
use DB;
use Auth;

class TareaTutorController extends Controller
{
    
	
	public function index(Request $request)
	{	
		$a=session('idTutelado','');
        if($a=='')
        {
            $id=Auth::user()->id;
            $tutor=DB::table('tutor')->where('iduser','=',$id)->first();
            $alum=DB::table('tutoralumno')->select('tutoralumno.idalumno','alumno.nombreAlumno','users.imagen')
            ->join('alumno','alumno.idAlumno','=','tutoralumno.idalumno')
            ->join('users','users.id','=','alumno.iduser')
            ->where('tutoralumno.idtutor','=',$tutor->idtutor)->get();
            if(count ($alum)>0)
            {
                session(['idTutelado', $alum[0]->idalumno]);
                $a=session('idTutelado');
            }
            else
            {
                $a=0;
            }
        }

        $idalumno=$a;
        $tareas= DB::table('tareas')
        ->select('tareas.titulo','tareas.descripcion','materia.nombreMateria','tareas.fechaentrega','tareas.anexo')
        ->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')        
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->where('alumno.idAlumno','=',$idalumno)
        ->where('tareas.condicion','=','1')
        ->orderBy('tareas.idTareas','desc')
        ->get();
		return view ("Tutor.tareas.index" ,["tareas"=>$tareas]);
	
	}

	public function getTareas(Request $request)
    {
        $idalumno=$request->id;
        $tareas= DB::table('tareas')
        ->select('tareas.titulo','tareas.descripcion','materia.nombreMateria','tareas.fechaentrega','tareas.anexo')
        ->join('subgrupo','subgrupo.idSubgrupo','=','tareas.idSubgrupo')        
        ->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
        ->join('alumno','alumno.idgrupo','=','grupo.idgrupo')
        ->join('materia','materia.idMateria','=','subgrupo.idMateria')
        ->where('alumno.idAlumno','=',$idalumno)
        ->where('tareas.condicion','=','1')
        ->orderBy('tareas.idTareas','desc')
        ->get();

        return $tareas;
    }
}
