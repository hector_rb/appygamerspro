<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Profesor;
use App\Asistencia;
use App\Mensaje;
use App\Tarea;
use App\Calificacion;
use App\Insignias;
use Carbon\Carbon;


use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


use App\Chats;
use Auth;

class ChatTutorApiController extends Controller
{
    public $successStatus = 200;
    
    public function crearReferenciaTutor(Request $request){
        $input = $request->all();
        
        $claveR = $input['idProfesor'] . $input['idTutor'];
        
        $referencia = DB::table('chat_p_t')
        ->where('referencia', $claveR)
        ->first();
        if(count($referencia) > 0){
            
            //return response()->json(['success'=>$this->successStatus], $this->successStatus);
            
        } else {
            
            DB::table('chat_p_t')->insert([
                'referencia' => $claveR,
                'idProfesor' => $input['idProfesor'],
                'idTutor'    => $input['idTutor'],
                'timestamp'  => Carbon::now()->toDateTimeString()
            ]);

            //return response()->json(['success'=>$this->successStatus], $this->successStatus);

        }

       //return view('Profesor.chats.create'); 
    }
    
    public function obtenerChatProfesor(Request $request){
        
        $input = $request->all();
        
        $chats = DB::table('chat_p_t')
        ->select(DB::raw("CONCAT(tutor.nombreTutor, ' ', tutor.apepat, ' ', tutor.apemat) as nombre"), 'users.imagen' ,'tutor.iduser as id', 'chat_p_t.ultimoMsj', 'chat_p_t.timestamp')
        ->join('tutor', 'chat_p_t.idTutor', 'tutor.iduser')
        ->join('users', 'tutor.iduser', 'users.id')
        ->where('idProfesor', $input['idProfesor'])
        ->orderBy('timestamp', 'DESC')
        ->get();
        
        return response()->json(['success'=>$this->successStatus, 'data'=> $chats], $this->successStatus);
        
    }
    
    public function obtenerChatTutor(Request $request){
        
        /*SELECT profesor.nombreprof as nombre, profesor.apepat as paterno, profesor.apemat as materno, profesor.iduser as id 
         *FROM profesor 
         *JOIN subgrupo ON profesor.idprofesor = subgrupo.idProfesor 
         *JOIN materia ON subgrupo.idMateria = materia.idMateria 
         *WHERE subgrupo.idGrupo = 1 
         *GROUP BY profesor.idprofesor
        */
        
        $input = $request->all();
        
        $chats = DB::table('profesor')
        ->select(DB::raw("CONCAT(profesor.nombreprof,' ',profesor.apepat,' ',profesor.apemat) as nombre"), 'users.imagen', 'profesor.iduser as id', 'materia.nombreMateria', 'chat_p_t.ultimoMsj', 'chat_p_t.timestamp', 'profesor.celular')
        ->join('subgrupo', 'profesor.idprofesor', 'subgrupo.idProfesor')
        ->join('materia', 'subgrupo.idMateria', 'materia.idMateria')
        ->leftJoin('chat_p_t', 'profesor.iduser', 'chat_p_t.idProfesor')
        ->where('subgrupo.idGrupo', $input['idGrupo'])
        ->groupBy('profesor.idprofesor')
        ->join('users', 'profesor.iduser', 'users.id')
        ->orderBy('chat_p_t.timestamp', 'DESC')
        ->get();
        
        return response()->json(['success'=>$this->successStatus, 'data'=> $chats], $this->successStatus);
        
    }

    public function ultimomsj(Request $request)
    {
        $input = $request->all();
        
        DB::table('chat_p_t')
            ->where('referencia', $input['referencia'])
            ->update(['ultimoMsj' => $input['mensaje']]);
            
        DB::table('chat_p_t')
            ->where('referencia', $input['referencia'])
            ->update(['timestamp'  => Carbon::now()->toDateTimeString()]);
            
        return response()->json(['success'=>$this->successStatus], $this->successStatus);
    }
    
    public function notificacionProfe(Request $request){
      $input = $request->all();
    
      $profe = DB::table("profesor")->select('tokenFCM')
      ->where('iduser',$input["id"])
      ->first();

      if($profe->tokenFCM!="0")
      {
        $this->sendNotification(
            "Chat", 
             $input["nombre"].": ".$input["mensaje"], 
            $profe->tokenFCM,
            $input["emisor"]
        );
      }
    
      return response()->json(['success'=>"210", ], $this->successStatus);
    
    }
    
    public function notificacionTutor(Request $request){
      $input = $request->all();
    
      $tutor = DB::table("tutor")->select('tokenFCM')
      ->where('iduser',$input["id"])
      ->first();

      if($tutor->tokenFCM!="0")
      {
        $this->sendNotification(
            "Chat", 
            $input["nombre"].": ".$input["mensaje"], 
            $tutor->tokenFCM,
            $input["emisor"]
        );
      }
    
      return response()->json(['success'=>"210", ], $this->successStatus);
    
    }
    
    public function sendNotification($titulo, $mensaje, $token, $id){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

    }
    
    
}
