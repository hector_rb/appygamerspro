<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use DB;

class juegoTutorDetallesController extends Controller
{
    public function index(Request $request)
    {
    	$juego=$request->juego;
        $juego=DB::table('juego')->where('nombre','=',$juego)->get();
        return view('Tutor.juegoTutor',['juego'=>$juego]);
    }
}
