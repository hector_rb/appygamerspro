<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\calendario;
use Auth;
use DB;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class eventosAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id=Auth::user()->id;
        $query = DB::table('calendario')->where('iduser','=',$id)->where('status','=',1)->get();
        if(count($query)>0)
        {
            foreach ($query as $values => $q ) {
                $array[] = ['id'=>$q->idCalendario, 'title'=>$q->titulo, 'description' => $q->desc, 'start'=>$q->inicio, 'end'=>$q->fin, 'className'=>$q->clase];
                
            }
        }
        else
        {
            $array=[];

        }

        return view('Admin.eventos.eventosAdmin', ['array'=>$array]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detallesCalendario(Request $request)
    {
        $grupo= DB::table('calendario as c')->select('g.grado','g.grupo')
        ->join('grupo as g','g.idgrupo','=','c.id')
        ->where('c.idCalendario','=',$request->id)
        ->where('c.status','=',1)
        ->first();

        $nivel= DB::table('calendario as c')->select('niv.nombreNivel')
        ->join('niveles as niv','niv.idNivel','=','c.id')
        ->where('c.idCalendario','=',$request->id)
        ->where('c.status','=',1)
        ->first();

        $eventos= DB::table('calendario as c')->select('c.idCalendario','c.titulo','c.inicio','c.fin','c.tipoDestinatario','c.id')
        ->where('c.idCalendario','=',$request->id)
        ->where('c.status','=',1)
        ->first();

        $calendario_asistencia= DB::table('calendario as c')->select('c.idCalendario','c.titulo','ca.asistencia','users.name','users.tipo_user','ca.idCalendario')
        ->join('calendario_asistencia as ca','ca.idCalendario','=','c.idCalendario')
        ->join('users','users.id','=','ca.idUsuario')
        ->where('c.idCalendario','=',$request->id)
        ->where('c.status','=',1)
        ->get();
        return view ("Admin.eventos.detallesCalendario",["eventos"=>$eventos, "calendario_asistencia"=>$calendario_asistencia, "nivel" => $nivel, "grupo"=>$grupo]);
    }

    public function SiCalendario(Request $request){

        $si= DB::table('calendario as c')
        ->select('ca.idCalendarioAsistencia')
        ->join ('calendario_asistencia as ca','ca.idCalendario','=','c.idCalendario')
        ->where('c.idCalendario','=', $request->idCalendario)
        ->where('ca.asistencia','=',1)
        ->count();

        $no= DB::table('calendario as c')
        ->select('ca.idCalendarioAsistencia')
        ->join ('calendario_asistencia as ca','ca.idCalendario','=','c.idCalendario')
        ->where('c.idCalendario','=', $request->idCalendario)
        ->where('ca.asistencia','=',0)
        ->count();

        return[ "si"=>$si, "no"=>$no];
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function GetNivel()
    {
        $id=Auth::user()->id;
        $inst=DB::table('institucion')->select('idInstitucion')
        ->where('id_usuario','=',$id)->first();
        $nivel=DB::table('nivel_inst')->select('nivel_inst.idNivel','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','nivel_inst.idNivel')
        ->where('nivel_inst.idInstitucion','=',$inst->idInstitucion)
        ->get();

        return $nivel;
    }

    public function event(Request $request)
    {   
        $id=Auth::user()->id;
        if ($request->type==1)
        {
            $iduser=Auth::user()->id;
            $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
            $id=$inst->idInstitucion;
            $save= new calendario;
            $save->titulo=$request->title;
            $save->desc=$request->description;
            $save->idInstitucion=$id;
            $save->inicio=$request->startdate;
            $save->fin=$request->startdate;
            $save->iduser=$iduser;
            $save->tipoDestinatario=$request->tipo;
            $save->id=$request->id;
            $save->clase=$request->class;
            $save->save();
            if($request->tipo==1)
            {
                $this->fcmTutor($id);
                $this->fcmAlumno($id);
                $this->fcmProfe($id);
               
            }
             if($request->tipo==2)
            {
                $this->fcmProfexnivel($id, $request->id);
                $this->fcmAlumnoxnivel($id, $request->id);
                $this->fcmTutorxnivel($id, $request->id);
            }
            
            return (['status'=>'success','eventid'=>$save->idCaledario]);
        }
        if($request->type ==2) 
        {
            $eventid = $request->id;
            $title = $request->title;
            DB::table('calendario')->where('idCalendario', '=', $eventid)->update(['titulo' => $title]);
              return (['status'=>'success']);
        }
        if($request->type ==3) 
        {
            $eventid = $request->id;
            $desc = $request->desc;
            DB::table('calendario')->where('idCalendario', '=', $eventid)->update(['desc' => $desc]);
              return (['status'=>'success']);
        }
        if($request->type ==4) 
        {
            $eventid = $request->id;
            $inicio =$request->start;
            $fin = $request->end;
            DB::table('calendario')->where('idCalendario', '=', $eventid)->update(['fin' => $fin, 'inicio'=>$inicio]);
              return (['status'=>'success']);
        }
        if($request->type ==5) 
        {
            $eventid = $request->id;
            DB::table('calendario')->where('idCalendario', '=', $eventid)->update(['status' => 0]);
              return (['status'=>'success']);
        }
    }
    
    
    public function fcmTutor($ins)
    {
        
         $fcm = DB::table('alumno')
         ->select('tutor.tokenFCM')
         ->join('tutoralumno', 'alumno.idAlumno','tutoralumno.idalumno')
         ->join('tutor','tutoralumno.idtutor','tutor.idtutor')
         ->where('alumno.idinstitucion','=',$ins)
         ->groupBy('tutor.idtutor')
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
    public function fcmAlumno($ins)
    {
        
         $fcm = DB::table('alumno')
         ->select('tokenFCM')
         ->where('idinstitucion','=',$ins)
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
    public function fcmProfe($ins)
    {
        
         $fcm = DB::table('profesor')
         ->select('tokenFCM')
         ->where('id_institucion','=',$ins)
         ->get();
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
     public function fcmProfexnivel($ins, $niv)
    {
        
         $fcm = DB::table('subgrupo')
         ->select('profesor.tokenFCM')
         ->join("grupo","subgrupo.idGrupo", "grupo.idgrupo")
         ->join("profesor", "subgrupo.idProfesor", "profesor.idprofesor")
         ->where('grupo.idNivel','=',$niv)
         ->where("grupo.idInstitucion", $ins)
         ->groupBy('subgrupo.idProfesor')
         ->get();
         
       
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
    public function fcmAlumnoxnivel($ins, $niv)
    {
        
         $fcm = DB::table('grupo')
         ->select('alumno.tokenFCM')
         ->join("alumno","grupo.idgrupo", "alumno.idgrupo")
         ->where('grupo.idNivel','=',$niv)
         ->where("grupo.idInstitucion", $ins)
         ->get();
         
         
       
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    public function fcmTutorxnivel($ins, $niv)
    {
        
         $fcm = DB::table('grupo')
         ->select('tutor.tokenFCM')
         ->join("alumno","grupo.idgrupo", "alumno.idgrupo")
         ->join("tutoralumno", "alumno.idAlumno", "tutoralumno.idalumno")
         ->join("tutor", "tutoralumno.idtutor", "tutor.idtutor")
         ->where('grupo.idNivel','=',$niv)
         ->where("grupo.idInstitucion", $ins)
         ->groupBy("tutor.idtutor")
         ->get();
         
       
       
         
         foreach($fcm as $token)
         {
             $this->avisar($token);
         }
        
    }
    
    
     public function avisar($token)
	{
	    

                
                 // echo json_encode($tutoralumno);
                 // echo "<br><br>";
                  
                      
                       if($token->tokenFCM!="0"){
                     
                             $this->sendNotification(
                            "Evento Escolar", 
                           "¡Avisanos si asistirás!", 
                            $token->tokenFCM
                            );
                        }
                      
                  
	
	}
	
	public function sendNotification($titulo, $mensaje, $token, $id=0){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $titulo]);
        $dataBuilder->addData(['messege' => $mensaje]);
        $dataBuilder->addData(['id' => $id]);

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();

        /*$tokens = array(
            "cItoGJFZXR8:APA91bH2asc2-Hb0TtFGoRDVBKVsAGw5cQ1gOG5-J-ytu4N1qp9bMbAAJ7PjCMqGSgBrFwH4w6XCSlSEnxWJte2PQikBhSzsDa3601LQzQ2zz1llFZ-L_VVPCB6ue2avxf1OwcJswnZs",
        ); */

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

    }
    
}
