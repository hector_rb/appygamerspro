<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Mensaje as Mensaje;
use DB;


class ProfesorMensaje extends Controller
{
   
    
    public function eliminados() //metodo que permite ver los mensajes eliminados
    {
        $query=DB::table('mensajes')->latest()->get();
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','1']])->get();
        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Profesor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    public function index() //muestra los mensajes recibidos, si se modifica algo aqui se debe modificar recibidos()
    {
         $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
        $id=$institucion->id_institucion;
        $tipo=Auth::user()->tipo_user;
        $query=DB::table('mensajes')->latest()->get();
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])->count();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])//filtro para profesor
                            ->orWhere([ ['TipoDestinatario','9'],['idInstitucion',$id ]])//filtro para profesores
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$id ]])//filtro de institucion
                            ->orderBy('created_at', 'desc')->get();


        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Profesor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function papelera($id)//metodo que permite enviar mensajes a la papelera
     {
         DB::table('mensajes')
             ->where('id', $id)
             ->update(['Eliminado' => 1]);
             return redirect()->back();

     }


    public function create()//metodo create para mostrar la vista del mensaje a crear 
    {
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        $query=DB::table('destinatarios')->get();
        return \View::make('Profesor.Mensajes.mensajesCreate',['Destinatarios'=>$query],compact('sinver'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //metodo que almacena el mensaje
    {
        $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
        $id=$institucion->id_institucion;
        $mensaje = new Mensaje;
        $mensaje->Mensaje=$request->Mensaje;
        $mensaje->IdEmisor=Auth::user()->id;
        $mensaje->TipoEmisor=Auth::user()->tipo_user;
        $mensaje->TipoDestinatario=$request->TipoDestinatario;
        $mensaje->idInstitucion=$id;
        
        if($request->TipoDestinatario==2)//en caso de que el destinatario sea un tutor se enviara por medio de el alumno
        {
            $alumno=DB::table('alumno')->select('idAlumno')->where('iduser',$request->IdDestinatario)->get()->first();
            $tutor=DB::table('tutoralumno')->select('idtutor')->where('idalumno',$alumno->idAlumno)->get()->first();
            $final=DB::table('tutor')->select('iduser')->where('idtutor',$tutor->idtutor)->get()->first();
            $mensaje->IdDestinatario=$final->iduser;
        }
        else //en cualquier otro caso...
            $mensaje->IdDestinatario=$request->IdDestinatario;
        $mensaje->Asunto=$request->Asunto;

        $mensaje->save();

        return redirect('profesor/mensajes');

    }

    
    public function show($id)//metodo que muestra mensajes 
    {
        
        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        DB::table('mensajes')
            ->where('id', $id)
            ->update(['Visto' => 1]);

        $mensaje=DB::table('mensajes')->where('id',$id)->get()->first();// Mensaje::find($id);
        switch ($mensaje->TipoEmisor) //quien lo envia
         {
             case '1':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
             case '2':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
             case '3':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
             case '4':
                 $variable=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdEmisor)->get();
             break;
        }

       switch ($mensaje->TipoDestinatario) //para quien es dirigido 
         {
            case '1':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '2':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '3':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '4':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '5':
                $variable2=DB::table('users')->select('name as Nombre')->where('id',$mensaje->IdDestinatario)->get();
            break;
            case '6':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(idNivel, ' ', grado) as Nombre,idgrupo as id"))->where([[(DB::raw("CONCAT(idNivel,'-', grado)")),$mensaje->IdDestinatario],['idInstitucion',$mensaje->idInstitucion]])->distinct()->get();
            break;
            case '7':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->where('idgrupo',$mensaje->IdDestinatario)->get();
            break;
            case '8':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos') as Nombre,idgrupo as id"))->where('idgrupo','2')->get();
            break;
             case '9':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Profesores') as Nombre"))->distinct()->get();
            break;
             case '10':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos') as Nombre,idgrupo as id"))->get();
            break;
             case '11':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Todos') as Nombre,idgrupo as id"))->get();
            break;
            case '12':
                $variable2=DB::table('grupo')->select(DB::raw("CONCAT('Administradores') as Nombre,idgrupo as id"))->where('idgrupo','1')->get();
            break;
            
        }
        return \View::make('Profesor.Mensajes.mensajeShow',compact('mensaje','variable','variable2','sinver'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function enviados() //permite ver los mensajes que ha enviado el profesor
    {

        $usuario=Auth::user()->id;
        $tipo=Auth::user()->tipo_user;
        $query=DB::table('mensajes')->latest()->get();
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
        //$mensajes=Mensaje::all();
        $mensajes=Mensaje::where([ ['IdEmisor',$usuario ],['TipoEmisor',$tipo]])->get();
        $var=DB::table('users')->select('id','name as Nombre')->get();
        $grupo=  DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->get();
        return \View::make('Profesor.Mensajes.enviados',compact('mensajes','var','grupo','sinver'));

    }

    public function update(Request $request, $id)
    {
        /*$mensaje = Mensaje::find($request->id);
        $mensaje->Asunto = $request->Asunto;
        $mensaje->Mensaje = $request->Mensaje;
        $mensaje->save();
        return redirect('Admin/mensaje');*/

        request()->validate([
            'Asunto' => 'required',
            'Mensaje' => 'required',
        ]);
        Mensaje::find($id)->update($request->all());
        return redirect()->route('mensajes.index')
                        ->with('success','Se modifico correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mensaje = Mensaje::find($id);
        $mensaje->delete();
        return redirect()->back();
    }


    public function search(Request $request)
    {
         $usuario=Auth::user()->id;
         $tipo=Auth::user()->tipo_user;
         $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo]])->count();
         $mensajes = Mensaje::where('Asunto','like','%'.$request->Asunto.'%')->get();
         $alumno=DB::table('alumno')->select('id','nombre as Nombre')->get();
         $profesor=DB::table('profesor')->select('id','nombreprof as Nombre')->get();
         return \View::make('Profesor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));

    }

    public function recibidos()// muestra los mensajes recibidos, si se modifica algo en esta parte de igual manera se debe modificar el index
    {

        $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
        $id=$institucion->id_institucion;
        $tipo=Auth::user()->tipo_user;
        $query=DB::table('mensajes')->latest()->get();
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])->count();
        $mensajes=Mensaje::where([ ['IdDestinatario',$usuario ],['TipoDestinatario',$tipo],['Eliminado','=','0']])
                            ->orWhere([ ['TipoDestinatario','9'],['idInstitucion',$id ]])
                            ->orWhere([ ['TipoDestinatario','8'],['idInstitucion',$id ]])
                            ->orderBy('created_at', 'desc')->get();


        $alumno=DB::table('users')->select('id','name as Nombre')->get();
        $profesor=DB::table('users')->select('id','name as Nombre')->get();
        return \View::make('Profesor.Mensajes.recibidos',compact('mensajes','alumno','profesor','sinver'));

    }


    public function muestra($id)
    {
        $mensaje= Mensaje::find($id);
        return \View::make('Profesor.Mensajes.mensajesShow',compact('mensaje'));
        $mensaje = Mensaje::find($id);
    }

    public function getDestinatario(Request $request) //metodo que permite obtener el destinatario segun la seleccion del select en MensajeCreate.blade.php
    {
        $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
        
        switch ($request->id) 
        {
            case '1':
                $admin=DB::table('institucion')
                ->select('id_usuario')
                ->where('idInstitucion',$institucion->id_institucion)
                ->get()
                ->first();
                $variable = DB::table('users')->select('name as Nombre','id')->where('id',$admin->id_usuario)->get();
                //$variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','1')->get();
                break;
            case '2':
                $variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','2')->get();
                break;
            case '3':
                $variable=DB::table('profesor')
                ->select('nombreprof as Nombre','idprofesor as id')
                ->where('id_institucion',$institucion->id_institucion)->get();
                //$variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','3')->get();
            break;
            case '4':
                $variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','4')->get();
            break;
            case '5':
                $variable = DB::table('users')->select('name as Nombre','id')->where('tipo_user','5')->get();
            break;
            case '6':
                $variable=DB::table('grupo')->select(DB::raw("CONCAT('Escolaridad: ' ,idNivel, ' Grado: ', grado) as Nombre,CONCAT(idNivel,'-', grado) as id"))->distinct()->where('idInstitucion',  $institucion->idInstitucion)->get();
            break;
            case '7':
                $variable=  DB::table('grupo')->select(DB::raw("CONCAT(grado, ' ', grupo) as Nombre,idgrupo as id"))->get();
            break;
            case '8':

            break;
            case '10':
                $variable= DB::table('Niveles')->select('nombreNivel as Nombre','idNivel as id')->get();
            break;
            case '11':

            break;
        }
        return $variable;

    }
    
    
     public function getAlumnosP(Request $request) //permite obtener los alumnos.
    {
       $alumnos=DB::table('alumno')->select(DB::raw("CONCAT(nombreAlumno, ' ',apepat,' ',apemat) as nombreAlumno,idgrupo,iduser"))->where('idgrupo',$request->id)->get(); //('iduser','idgrupo','nombreAlumno')->where('idgrupo',$request->id)->get();
        return $alumnos;
    }
    
     public function getNivelP()//permite obtener niveles de la institucion
    {
       $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
       $nivel=DB::table('nivel_inst')->select('nivel_inst.idNivel','niveles.nombreNivel')
        ->join('niveles','niveles.idNivel','=','nivel_inst.idNivel')
        ->where('nivel_inst.idInstitucion','=',$institucion->id_institucion)
        ->get();

        return $nivel;
    }
    
     public function getGradosP(Request $request) //obtiene los grados de la institucion
    {
         $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
        $id=$institucion->id_institucion;
        $idNivel=$request->idNivel;
        $grados= DB::table('grado')->where( [['idInstitucion',$id],[ 'idNivel',$request->idNivel] ])->get();//
        return $grados;

    }
    
     public function getGruposP(Request $request) //permite obtener los grupos de la institucion
    {
        $usuario=Auth::user()->id;
	    $institucion=DB::table('profesor')->select('id_institucion')->where('iduser',$usuario)->get()->first();
        $id=$institucion->id_institucion;
        $idNivel=$request->idNivel;
        $grado=$request->grado;
        $materia= DB::table('grupo')
        ->where('idInstitucion','=',$id)->where('idNivel','=',$idNivel)->where('grado','=',$grado)
        ->get();
        return $materia;

    }
    
   
}
