<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Graficas;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\GraficasFormRequest;
use DB;
use Auth;

class GraficasProfesorController extends Controller
{
    public function __construct()
	{
		
	}
	public function index(Request $request)
	{
		/*$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		//$calif=DB::table('calif')->select('calif.idcalif','calif.idSubgrupo','calif.idPeriodo','calif.calif', 'calif.fecha','subgrupo.idProfesor','alumno.idAlumno','alumno.nombreAlumno','alumno.apepat','alumno.apemat')
		//->join('alumno','alumno.idAlumno','=','calif.idalumno')
		$calif=DB::table('calif')->select('calif.idcalif','calif.idSubgrupo','calif.idPeriodo','calif.calif', 'calif.fecha','subgrupo.idProfesor')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('calif.idPeriodo','=',$periodo)
		->where('subgrupo.estatus','=',1)
		//->groupBy('idAlumno')
		->groupBy('idPeriodo')
		->get();

		$user=Auth::user()->id;
		$tipo=Auth::user()->tipo_user;
        $sinver=DB::table('mensajes')->where([['Visto',0],['IdDestinatario',$user ],['TipoDestinatario',$tipo]])->count();
		$prof=DB::table('profesor')->select('id_institucion','idprofesor')->where('iduser','=',$user)->first();
		$Qsubgrupo=DB::table('subgrupo')
		->select('subgrupo.idSubgrupo','materia.nombreMateria','grupo.grado','grupo.grupo')
		->join('ciclo','ciclo.idCiclo','=','subgrupo.idCiclo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('grupo','grupo.idGrupo','=','subgrupo.idGrupo')
		->where('ciclo.status','=',1)
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->first();
		if($Qsubgrupo)
		{
			$sg=$Qsubgrupo->idSubgrupo;
			$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
			$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

			$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
			->leftjoin('calif','calif.idPeriodo','=','periodo.idPeriodo')
			->where('periodo.idNivel','=',$grupo->idNivel)
			->where('periodo.idInstitucion','=',$prof->id_institucion)
			->where('calif.idSubgrupo','=',$sg)
			->groupBy('periodo.idPeriodo')
			->get();
			
			foreach ($periodo as $p) {
				$calPer=DB::table('calif')->select('calif')
				->where('idSubgrupo','=',$sg)
				->where('idPeriodo','=',$p->idPeriodo)
				->get();
				$prom=0;
				$cont=1;
				foreach ($calPer as $c) {
					$cont++;
					$prom=$prom+$c->calif;
				}
				$prom=$prom/$cont;
				$califs[]=[$p->nombrePeriodo,$prom];

			}
			return view('Profesor.graficas.index',["califs"=>$califs,"sg"=>$Qsubgrupo, "sinver"=>$sinver]);
			//dd ($califs);
		}
		else
		{
			
			$califs[]=['1',0];
			$Qsubgrupo=['nombreMateria'=>'(No hay materias)','grado'=>0,'grupo'=>0];
			$Qsubgrupo=(object)$Qsubgrupo;
			//dd($califs);
			return view('Profesor.graficas.index',["califs"=>$califs,"sg"=>$Qsubgrupo, "sinver"=>$sinver]);
		}

	*/

		$id=Auth::user()->id;
		$tipo=Auth::user()->tipo_user;
		$sinver=DB::table('mensajes')
        ->where([['Visto',0],['IdDestinatario',$id ],['TipoDestinatario',$tipo]])->count();
        $prof=DB::table('profesor')->select('idprofesor')
        ->where('iduser','=',$id)
        ->first();
        $avisos=DB::table('avisos')->select('avisos.fecha')
        ->where('avisos.destinatario','=',3)
        ->where('avisos.estatus','=',0)
		        ->where('avisos.fecha','>=',date('Y-m-d'))
        ->count();
        $eventos=DB::table('calendario')->select('calendario.inicio')
        ->where('calendario.tipoDestinatario','=',4)
        ->where('calendario.status','=','1')
        ->where('calendario.inicio','>=',date('Y-m-d'))
        ->count();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('ciclo.status','=',1)
		->get();
		return view('Profesor.graficas.index',["sg"=>$sg, "avisos"=>$avisos, "eventos"=>$eventos, "sinver"=>$sinver]);	
	}

	public function graficasCalifProf(Request $request)
	{
		$user=Auth::user()->id;
		$tipo=Auth::user()->tipo_user;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$user)->first();
        $Qsubgrupo=DB::table('subgrupo')
		->select('subgrupo.idSubgrupo','materia.idMateria','materia.nombreMateria','grupo.grado','grupo.grupo','grupo.idInstitucion')
		->join('ciclo','ciclo.idCiclo','=','subgrupo.idCiclo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('grupo','grupo.idGrupo','=','subgrupo.idGrupo')
		->where('ciclo.status','=',1)
		->where('subgrupo.idSubgrupo','=',$request->idsg)
		->first();

		if($Qsubgrupo)
		{
			$sg=$Qsubgrupo->idSubgrupo;
			$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
			$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

			$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
			->leftjoin('calif','calif.idPeriodo','=','periodo.idPeriodo')
			->where('periodo.idNivel','=',$grupo->idNivel)
			->where('calif.idSubgrupo','=',$sg)
			->groupBy('periodo.idPeriodo')
			->get();

			$califs = null;
			foreach ($periodo as $p) {
				$calPer=DB::table('calif')->select('calif')
				->where('idSubgrupo','=',$sg)
				->where('idPeriodo','=',$p->idPeriodo)
				->get();
				$prom=0;
				$cont=0;
				foreach ($calPer as $c) {
					$cont++;
					$prom=$prom+$c->calif;
					
				}
				$prom=$prom/$cont;
				$califs[] = [$p->nombrePeriodo];
				$barra[]=round($prom,2);

			}
			$barra[]=0;
			if($califs == null)
				$califs="";

			return ["califs"=>$califs, "barra"=>$barra, "sg"=>$Qsubgrupo];
		}
	}

	public function create()
	{
		/*$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asistencia=DB::table('asistencia')->select('asistencia.asistencia','asistencia.idAlumno', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->groupBy('idAlumno')
		->get();
		return view('Profesor.graficas.create',["asistencia"=>$asistencia]);*/
		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion','idprofesor')->where('iduser','=',$user)->first();
		$Qsubgrupo=DB::table('subgrupo')
		->select('subgrupo.idSubgrupo','materia.nombreMateria','grupo.grado','grupo.grupo')
		->join('ciclo','ciclo.idCiclo','=','subgrupo.idCiclo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('grupo','grupo.idGrupo','=','subgrupo.idGrupo')
		->where('ciclo.status','=',1)
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->first();

		$sg=$Qsubgrupo->idSubgrupo;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();

		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->leftjoin('asistencia','asistencia.idPeriodo','=','periodo.idPeriodo')
		->where('periodo.idNivel','=',$grupo->idNivel)
		->where('periodo.idInstitucion','=',$prof->id_institucion)
		->where('asistencia.idSubgrupo','=',$sg)
		->groupBy('periodo.idPeriodo')
		->get();
		
		foreach ($periodo as $p) {
			$asisPer=DB::table('asistencia')->select('asistencia')
			->where('idSubgrupo','=',$sg)
			->where('idPeriodo','=',$p->idPeriodo)
			->get();
			$prom=0;
			$cont=1;
			foreach ($asisPer as $as) {
				$cont++;
				$prom=$as->asistencia;
			}
			$prom=$cont;
			$asis[]=[$p->nombrePeriodo,$prom];

		}

		return view('Profesor.graficas.create',["asis"=>$asis,"sg"=>$Qsubgrupo]);
	}


	public function getPeriodosProfesor(Request $request)
	{
		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$user)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->where('idNivel','=',$grupo->idNivel)
		->where('idInstitucion','=',$prof->id_institucion)
		->get();
		return $periodo;
	}

	//Request $request
	//
	public function getPromedioGrupoProf(Request $request)
	{	
		$id=Auth::user()->id;
		$sg=$request->idsg;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','calif.calif','calif.idPeriodo','grupo.grado','grupo.grupo',DB::raw('AVG(calif.calif) as promedio'))
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('subgrupo','subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('calif.idSubgrupo','=',$sg)
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->groupBy('idAlumno')
		->get();
		return $calif;
	}

	public function getPromedioAlumnoSobresalienteProf(Request $request)
	{	
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=$request->idsg;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','calif.calif','users.imagen as imagen',DB::raw('AVG(calif.calif) as promedio'))
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('users','users.id','=','alumno.iduser')
		->join('subgrupo','subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->where('calif.idSubgrupo','=',$sg)
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->groupBy('calif.idAlumno')
		->orderBy('promedio','dessc')
		->take(3)
		->get();
		return $calif;
	}

	public function getPromedioAlumnoRegularProf(Request $request)
	{	
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$sg=$request->idsg;
		$calif=DB::table('calif')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','calif.calif','users.imagen as imagen',DB::raw('AVG(calif.calif) as promedio'))
		->join('alumno','alumno.idAlumno','=','calif.idalumno')		
		->join('users','users.id','=','alumno.iduser')
		->join('subgrupo','subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->where('calif.idSubgrupo','=',$sg)
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		//->where('calif','<','8')
		->groupBy('idAlumno')
		->orderBy('promedio','asc')
		->take(3)
		->get();
		return $calif;
	}

	public function TotalAsistencias(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=DB::table('subgrupo')->select('idSubgrupo')->get();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.estatus','=',1)
		->get();
		return $asis;
	}

	public function getMayorAsistencia(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','=','A')
		->get();

		//SELECT idAlumno, count(CASE WHEN asistencia = 'A' THEN 1 END) as A, count(CASE WHEN asistencia = 'F' THEN 1 END) as F, count(CASE WHEN asistencia = 'R' THEN 1 END) as R FROM asistencia WHERE idSubgrupo = 1 GROUP BY idAlumno
		return $asis;
	}

		public function getRetardo(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','=','R')
		->get();
		return $asis;
	}

		public function getFalta(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat','asistencia.asistencia')
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','=','F')
		->get();
		return $asis;
	}

		public function getAlumnosRetardos(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('subgrupo.estatus','=',1)
		->where('asistencia.asistencia','>','2')
		->groupBy('idAlumno')
		->get();
		return $asis;
	}

	public function getAlumnosFaltas(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$asis=DB::table('asistencia')->select('subgrupo.idProfesor','grupo.grado','grupo.grupo','asistencia.idAlumno','alumno.idgrupo', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','asistencia.idSubgrupo')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('subgrupo.estatus','=',1)
		->groupBy('idAlumno')
		->get();
		return $asis;
	}


	public function getCalifCount(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		
		$periodo=$request->periodo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$calif=DB::table('calif')->select('calif.idcalif','calif.idSubgrupo','calif.idPeriodo','calif.calif', 'calif.fecha','subgrupo.idProfesor')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','calif.idSubgrupo')
		->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('calif.idSubgrupo','=',$subgrupo)
		->where('subgrupo.estatus','=',1)
		//->groupBy('idAlumno')
		->get();
		return $calif;
	}

	public function getCalifAprobCount(Request $request)
	{
		$id=Auth::user()->id;
		$subgrupo=$request->idSubgrupo;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$calif=DB::table('calif')->select('calif.idcalif','calif.idSubgrupo','calif.idPeriodo','calif.calif', 'calif.fecha','subgrupo.idProfesor','alumno.idAlumno','alumno.nombreAlumno','alumno.apepat','alumno.apemat')
		->join('alumno','alumno.idAlumno','=','calif.idalumno')
		->join('subgrupo', 'subgrupo.idSubgrupo','=','calif.idSubgrupo')
		//->where('subgrupo.idProfesor','=',$prof->idprofesor)
		->where('calif.idSubgrupo','=',$subgrupo)
		->where('subgrupo.estatus','=',1)
		->where('calif.calif','>','6')
		//->groupBy('idAlumno')
		->get();
		return $calif;
	}

	public function getMaterias(Request $request)
	{
		$id=Auth::user()->id;
		$prof=DB::table('profesor')->select('idprofesor')->where('iduser','=',$id)->first();
		$materia=DB::table('materia_profesor')->select('materia_profesor.idMateria','materia_profesor.idProfesor','materia_profesor.idInstitucion','materia.nombreMateria')
		->join('materia','materia.idMateria','=','materia_profesor.idMateria')
		->where('materia_profesor.idProfesor','=',$prof->idprofesor)
		->groupBy('idMateria')
		->get();
		return $materia;
	}

	public function MenuProfesor(Request $request)
	{
		$id=Auth::user()->id;
		$tipo=Auth::user()->tipo_user;
		$sinver=DB::table('mensajes')
        ->where([['Visto',0],['IdDestinatario',$id ],['TipoDestinatario',$tipo]])->count();
        $prof=DB::table('profesor')->select('idprofesor')
        ->where('iduser','=',$id)
        ->first();
        $avisos=DB::table('avisos')->select('avisos.fecha')
        ->where('avisos.destinatario','=',3)
        ->where('avisos.estatus','=',0)
		        ->where('avisos.fecha','>=',date('Y-m-d'))
        ->count();
        $eventos=DB::table('calendario')->select('calendario.inicio')
        ->where('calendario.tipoDestinatario','=',4)
        ->where('calendario.status','=','1')
        ->where('calendario.inicio','>=',date('Y-m-d'))
        ->count();
		return (["sinver"=>$sinver, "avisos"=>$avisos, "eventos"=>$eventos]);
	}

}
