<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Expedientes;
use App\Conocimientos;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ExpedientesFormRequest;
use DB;
use Auth;


class ExpedientesAdminController extends Controller
{
	public function __construct()
	{
		
	}
	public function index(Request $request)
	{
		$id=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$id)->first();
	    $alumno=DB::table('alumno')->select('alumno.idAlumno','alumno.idinstitucion','alumno.nombreAlumno','alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo','users.imagen','alumno.matricula')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->join('users','users.id','=','alumno.iduser')
		->where('alumno.idinstitucion','=',$inst->idInstitucion)
		//->where('grupo.idgrupo','=','alumno.idgrupo')
		//->orderBy('alumno.idAlumno','asc')
		//->paginate(30)
		->orderBy('grupo.grado','asc')
		->get();
		//echo($alumno);
		return view ("Admin.expedientes.index",["alumno"=>$alumno]);
	}

	public function create()
	{
		
	}

	public function datosmedicos(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
	    $alumno=DB::table('alumno')->select('alumno.idAlumno','alumno.idinstitucion','alumno.nombreAlumno','alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo','alumno.genero','alumno.fechanac','alumno.telefono','alumno.correo','users.imagen')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->join('users','users.id','=','alumno.iduser')
		->where('alumno.idinstitucion','=',$inst->idInstitucion)
		->where('alumno.idAlumno','=',$request->idAlumno)
		->get();
		return view ("Admin.expedientes.datosmedicos",["alumno"=>$alumno]);
	}

	public function conocimientos(Request $request)
	{
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
	    $alumno=DB::table('alumno')->select('alumno.idAlumno','alumno.idinstitucion','alumno.nombreAlumno','alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo','alumno.genero','alumno.fechanac','alumno.telefono','alumno.correo')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idinstitucion','=',$inst->idInstitucion)
		->where('alumno.idAlumno','=',$request->idAlumno)
		->get();
		return view ("Admin.expedientes.conocimientos",["alumno"=>$alumno]);
	}

	public function store(Request $request)
	{
		/*
		
		$a=true;
		if ($a==true);
			{
				echo
				"*/
				$expedientes=new Expedientes;
				$expedientes->idAlumno=$request->get('idAlumno');
				$expedientes->alergias=$request->get('alergias');
				$expedientes->enfermedades=$request->get('enfermedades');
				$expedientes->enfCro=$request->get('enfCro');
				$expedientes->cirugias=$request->get('cirugias');
				$expedientes->numSeguro=$request->get('numSeguro');
				$expedientes->tipoSangre=$request->get('tipoSangre');
				$expedientes->numEme=$request->get('numEme');
				$expedientes->save();
				/*";
				$expedientes->save();
			}
		if ($a=false);
			{		
				echo
				"	*/
				$conocimientos=new Conocimientos;	
				$conocimientos->idAlumno=$request->get('datos');
				$conocimientos->conocimiento1=$request->get('conocimiento1');
				$conocimientos->conocimiento2=$request->get('conocimiento2');
				$conocimientos->conocimiento3=$request->get('conocimiento3');
				$conocimientos->save();
				/*";
				$conocimientos->save();
			}
		*/

		return Redirect::to('/admin/expedientes');
		
	}

	
	public function show( $id)
	{   
		$iduser=Auth::user()->id;
        $inst= DB::table('institucion')->where('id_usuario','=',$iduser)->first();
        $i=$inst->idInstitucion;
	    $alumno=DB::table('alumno')->select('alumno.idAlumno','alumno.idinstitucion','alumno.nombreAlumno','alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo','alumno.genero','alumno.fechanac','alumno.telefono','alumno.correo','tutor.nombreTutor','tutor.apepat as tutorapepat','tutor.apemat as tutorapemat','tutor.correo as tutorcorreo','tutor.telefono as tutortelefono','tutor.celular as tutorcelular','users.imagen')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->join('tutoralumno','tutoralumno.idalumno','=','alumno.idAlumno')
		->join('tutor','tutor.idtutor','=','tutoralumno.idtutor')
		->join('users','users.id','=','alumno.iduser')
		//->join('datos_medicos as dm','dm.idAlumno','=','alumno.idAlumno')
		->where('alumno.idinstitucion','=',$inst->idInstitucion)
		->where('alumno.idAlumno','=',$id)
		->get();
		$asistencia=DB::table('asistencia')->select('asistencia')
		->where('idAlumno','=',$id)
		->where('asistencia','=','F')
		->count();
		$insignias=DB::table('Insignias as i')->select('i.idReceptor','i.tipoReceptor','i.fecha','ti.rutaImagen','i.fecha')
		->join('tiposinsignia as ti','ti.idTipoInsignia','=','i.idTipoInsignia')
		->where('i.idReceptor','=',$id)
		->where('i.tipoReceptor','=',0)
		->get();
		$datosmedicos=DB::table('datos_medicos')->select('idAlumno','alergias','enfermedades','enfCro','cirugias','numSeguro','tipoSangre','numEme')
		->where('idAlumno','=',$id)
		->get();
		$conocimientos=DB::table('conocimientos_adicionales')->select('idAlumno','conocimiento1','conocimiento2','conocimiento3')
		->where('idAlumno','=',$id)
		->get();

		$calif=DB::table('calif')->select('calif.calif')
		->where('idalumno','=',$id)
		->avg('calif.calif');
		if(!$calif)
			$calif="sin calificar";

		return view ("Admin.expedientes.show",["alumno"=>$alumno, "idalu"=>$id, "asistencia"=>$asistencia, "insignias"=>$insignias, "datosmedicos"=>$datosmedicos, "conocimientos"=>$conocimientos,"calif"=>$calif]);

	}

	public function edit($id)
	{

	}
	public function update(ExpedientesFormRequest $request,$id)
	{
		
	
	}
	public function destroy($id)
	{
		return Redirect::to('/admin/expedientes/');
	
	}
}
