<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Asistencia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\AsistenciaFormRequest;
use DB;
use Auth;


class AsistenciaAdminController extends Controller
{
   	  public function __construct()
	{
		
	}
	public function index(Request $request)
	{
		$id=Auth::user()->id;
		$inst=DB::table('institucion')->select('idInstitucion')
		->where('id_usuario','=',$id)->first();
		$sg=DB::table('subgrupo')->select('subgrupo.idSubgrupo','grupo.grado','grupo.grupo','materia.nombreMateria','niveles.nombreNivel')
		->join('grupo','grupo.idgrupo','=','subgrupo.idGrupo')
		->join('materia','materia.idMateria','=','subgrupo.idMateria')
		->join('niveles','niveles.idNivel','=','grupo.idNivel')
		->join('ciclo','subgrupo.idCiclo','=','ciclo.idCiclo')
		->where('ciclo.status','=',1)
		->where('grupo.idInstitucion','=',$inst->idInstitucion)
		->where('grupo.status','=',1)
		->get();
		return view('Admin.asistencia.index',['sg'=>$sg]);

	}
	public function create()
	{
		return view("Admin.asistencia.create",['sg'=>$sg]);
	}

	public function getPeriodosAdmin(Request $request)
	{
		$user=Auth::user()->id;
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel','idInstitucion')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$periodo=DB::table('periodo')->select('periodo.idPeriodo','periodo.nombrePeriodo')
		->where('idNivel','=',$grupo->idNivel)
		->where('idInstitucion','=',$grupo->idInstitucion)
		->get();
		return $periodo;
	}

	public function getAlumno(Request $request)
	{
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idgrupo')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$alum=DB::table('alumno')->select('alumno.idAlumno','alumno.nombreAlumno', 'alumno.apepat','alumno.apemat','grupo.grado','grupo.grupo')
		->join('grupo','grupo.idgrupo','=','alumno.idgrupo')
		->where('alumno.idgrupo','=',$grupo->idgrupo)
		->get();

		return $alum;
	}

	public function getFechasAsistencia(Request $request)
	{
		$user=Auth::user()->id;
		$prof=DB::table('profesor')->select('id_institucion')->where('iduser','=',$user)->first();
		$sg=$request->id;
		$subgrupo=DB::table('subgrupo')->select('idGrupo')->where('idSubgrupo','=',$sg)->first();
		$grupo=DB::table('grupo')->select('idNivel')->where('idgrupo','=',$subgrupo->idGrupo)->first();
		$periodo=$request->periodo;
		$fechas=DB::table('periodo')->select('asistencia.fecha','asistencia.idPeriodo')
		->leftjoin('asistencia','asistencia.idPeriodo','=','periodo.idPeriodo')
		->where('periodo.idNivel','=',$grupo->idNivel)
		->where('periodo.idInstitucion','=',$prof->id_institucion)
		->where('asistencia.idSubgrupo','=',$sg)
		->where('asistencia.idPeriodo','=',$periodo)
		->groupBy('asistencia.fecha')
		->get();
		return $fechas;

	}

	public function getAsistencia(Request $request)
	{
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;

		$asis=DB::table('asistencia')->select('asistencia.idAlumno', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat',
			DB::raw('sum(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('sum(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('sum(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->where('idSubgrupo','=',$subgrupo)
		->where('idPeriodo','=',$periodo)
		->groupBy('idAlumno')
		->get();

		//SELECT idAlumno, count(CASE WHEN asistencia = 'A' THEN 1 END) as A, count(CASE WHEN asistencia = 'F' THEN 1 END) as F, count(CASE WHEN asistencia = 'R' THEN 1 END) as R FROM asistencia WHERE idSubgrupo = 1 GROUP BY idAlumno
		return $asis;
	}

	public function getFechasAsistencia2(Request $request)
	{
		$subgrupo=$request->idSubgrupo;
		$periodo=$request->periodo;
		$fechas=DB::table('periodo')->select('asistencia.fecha','asistencia.idPeriodo');
		$asis=DB::table('asistencia')->select('asistencia.fecha','asistencia.idAlumno', 'alumno.nombreAlumno','alumno.apepat','alumno.apemat', 
			DB::raw('(CASE WHEN asistencia = "A" THEN 1 END) as A'), 
			DB::raw('(CASE WHEN asistencia = "F" THEN 1 END) as F'), 
			DB::raw('(CASE WHEN asistencia = "R" THEN 1 END) as R'))
		->join('alumno','alumno.idAlumno','=','asistencia.idAlumno')
		->where('idSubgrupo','=',$subgrupo)
		->where('idPeriodo','=',$periodo)
		->where('fecha','=', $request->fecha)
		->groupBy('idAlumno')
		->get();

		//SELECT idAlumno, count(CASE WHEN asistencia = 'A' THEN 1 END) as A, count(CASE WHEN asistencia = 'F' THEN 1 END) as F, count(CASE WHEN asistencia = 'R' THEN 1 END) as R FROM asistencia WHERE idSubgrupo = 1 GROUP BY idAlumno
		return $asis;
	}



	public function store(AsistenciaFormRequest $request)
	{
		//dd($request->get('idAlumno'));
		
		$Subgrupo = $request->idSubgrupo;
		$fecha=$request->fecha;
		$periodo=$request->periodo;
		foreach($request->asis as $key => $a) {
				
				$asis=new Asistencia;
				$asis->idSubgrupo=$Subgrupo;
				$asis->idAlumno=$request->id[$key];
				$asis->idPeriodo=$periodo;
				$asis->asistencia=$a;
				$asis->fecha=$fecha;
				$asis->save();
  			
		}
		
		return Redirect::to('/profesor/asistencia');
		/*
		$asistencia=new Asistencia;
		$asistencia->idAlumno=$request->get('idAlumno');
		$asistencia->idgrupo=$request->get('idgrupo');
		$asistencia->fecha=$request->get('fecha');
		$asistencia->asistencia=$request->get('asistencia');
		$asistencia->save();
		*/
		return Redirect::to('/profesor/asistencia');
	}
	public function show($id)
	{
		return view("Profesor.asistencia.show",["asistencia"=>Asistencia::findOrFail($id)]);
		
	}
	public function edit($id)
	{
		return view("Profesor.asistencia.edit",["asistencia"=>Asistencia::findOrFail($id)]);
		
	
	}
	public function update(AsistenciaFormRequest $request,$id)
	{
		$asistencia=new Asistencia;
		$asistencia->idalumno=$request->get('idAlumno');
		$asistencia->idgrupo=$request->get('idgrupo');
		$asistencia->fecha=$request->get('fecha');
		$asistencia->asistencia=$request->get('asistencia');
		$asistencia->save();
		return Redirect::to('/profesor/asistencia');
	
	}
	public function destroy($id)
	{
		$asistencia=Asistencia::findOrFail($id);
		$asistencia->update();
		return Redirect::to('/profesor/asistencia');
	
	}
}
