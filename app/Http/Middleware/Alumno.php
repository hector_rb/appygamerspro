<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Closure;

class Alumno
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    Protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth=$auth;
    }

    public function handle($request, Closure $next)
    {

        switch($this->auth->user()->tipo_user)
        {
            case '1':
                    return redirect()->to('admin');
                    break;
            case '2':
                    return redirect()->to('tutor');
                    break;
            case '3':
                    return redirect()->to('profesor');
                    break;
            case '4':
                    //return redirect()->to('alumno');
                    break;
            case '5':
                    return redirect()->to('super-admin');
                    break;
            default:
                    return redirect()->to('login');
                    break;          

        }
        return $next($request);
    }
}
