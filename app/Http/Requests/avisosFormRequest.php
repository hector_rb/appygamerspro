<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class avisosFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'asunto'=>'required|max:200|string',
            'cuerpo'=>'required|max:19000|string',
            //'destinatarios'=>'required|numeric|max:4|min:1'
            
        ];
    }
}
