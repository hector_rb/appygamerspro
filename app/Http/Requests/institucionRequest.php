<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class institucionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
        'clave'=>'required',
        'nombre'=>'max:100',
        'estado'=>'required',
        'ciudad'=>'required',
        'direccion'=>'required',
        'contacto'=>'required',
        'imagen'=>'mimes:jpeg,bmp,png,jpg',
        ];
    }
}
