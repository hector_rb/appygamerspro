<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpedientesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alergias'=>'max:60',
            'enfermedades'=>'max:60',
            'enfCro'=>'max:60',
            'cirugias'=>'max:60', 
            'numSeguro'=>'max:60',
            'tipoSangre'=>'required'|'max:11',
            'numEme'=>'max:11',
        ];
    }
}
