<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tutoralumno extends Model
{
    //
    protected $table='tutoralumno';

    protected $primaryKey = 'idtutor';

    public $timestamps=false;

    protected $fillable =[
        'idalumno',
        ];

    protected $guarded=[];
}
