<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ciclo extends Model
{
    //
    protected $table='ciclo';

    protected $primaryKey = 'idCiclo';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idNivel',
        'nombreCiclo',
        'inicioCiclo',
        'finCiclo'
        
        ];

    protected $guarded=[];
}
