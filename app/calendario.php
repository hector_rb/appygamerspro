<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class calendario extends Model
{
    //
    protected $table='calendario';

    protected $primaryKey = 'idCaledario';

    public $timestamps=false;

    protected $fillable =[
        'titulo',
        'desc',
        'inicio',
        'fin',
        'idInstitucion',
        'iduser',
        'clase',
        
        ];

    protected $guarded=[];
}
