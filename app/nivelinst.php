<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nivelinst extends Model
{
    //
    protected $table='nivel_inst';

    protected $primaryKey = 'idInstitucion';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idNivel',
        
        ];
}
