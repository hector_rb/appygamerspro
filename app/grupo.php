<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class grupo extends Model
{
    //
    protected $table='grupo';

    protected $primaryKey = 'idgrupo';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idNivel',
        'grado',
        'grupo',
        'contrasena',
        
        ];

    protected $guarded=[];
}
