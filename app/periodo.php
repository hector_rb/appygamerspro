<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class periodo extends Model
{
    //
    protected $table='periodo';

    protected $primaryKey = 'idPeriodo';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idNivel',
        'nombrePeriodo',
        
        ];

    protected $guarded=[];
}
