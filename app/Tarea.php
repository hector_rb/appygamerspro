<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
   protected $table='tareas';
   protected $primaryKey="idTareas";
   public $timestamps=false;
   protected $filetable=[
   'idSubgrupo',
   'fechaentrega',
   'titulo',
   'descripcion',
   'anexo',
   ];
   protected $guarded=[];
}
