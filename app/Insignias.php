<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insignias extends Model
{
   protected $table='Insignias';
   protected $primaryKey="id";
   public $timestamps=false;
   protected $filetable=[
   'idTipoInsignia',
   'idReceptor',
   'idEmisor',
   'idSubgrupo',
   'fecha',
   ];
   protected $guarded=[];
}
