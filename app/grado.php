<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class grado extends Model
{
    //
    protected $table='grado';

    protected $primaryKey = 'idInstitucion';

    public $timestamps=false;

    protected $fillable =[
        'idInstitucion',
        'idNivel',
        'grado',
        
        ];

    protected $guarded=[];
}
