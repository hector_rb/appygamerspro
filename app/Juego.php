<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juego extends Model
{
    protected $table='juego';
    protected $primaryKey = 'id_juego';
    protected $fillable =[
        'nombre',
        'descripcion',
        'imagen',
        'status',
        'premium',
        'descripcion_larga',
        ];
    protected $guarded=[];
}
