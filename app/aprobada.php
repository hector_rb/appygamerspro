<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aprobada extends Model
{
    //

    protected $table='aprobadas';

    protected $primaryKey = 'idAprobada';

    public $timestamps=false;

    protected $fillable =[
    	'correo',
        'codigo',
        'estatus'
        ];

    protected $guarded=[];
}
