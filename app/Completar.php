<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Completar extends Model
{
   protected $table='enc_completa';
   protected $primaryKey="idEncuesta";
   public $timestamps=false;
   protected $filetable=[
   'idEncuestado',
   'numPreguntas',
   'contestadas',
   'tipoEncuestado',
   ];

   
   protected $guarded=[];
}
