<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificaciones extends Model
{
   protected $table='calif';
   protected $primaryKey="idcalif";
   public $timestamps=false;
   protected $filetable=[
   'idSubgrupo',
   'idalumno',
   'calif',
   'fecha',
   'idPeriodo',
   ];
   protected $guarded=[];
}
