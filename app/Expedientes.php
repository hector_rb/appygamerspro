<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expedientes extends Model
{
   protected $table='datos_medicos';
   protected $primaryKey="idHistorialMedico";
   public $timestamps=false;
   protected $filetable=[
   'idAlumno',
   'alergias',
   'enfermedades',
   'enfCro',
   'cirugias',
   'numSeguro',
   'tipoSangre',
   'numEme',
   ];
   protected $guarded=[];
}
