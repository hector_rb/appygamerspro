<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alerta extends Model
{
   protected $table='alerta';
   protected $primaryKey="idAlerta";
   protected $filetable=[
   'TipoAlerta',
   'descripcion',
   ];
   protected $guarded=[];
}
