<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestas extends Model
{
   protected $table='encuesta';
   protected $primaryKey="idEncuesta";
   public $timestamps=false;
   protected $filetable=[
   'idAdmin',
   'idInstitucion',
   'nombre',
   'descripcion',
   'fecha',
   'estatus',
   ];

   
   protected $guarded=[];
}


