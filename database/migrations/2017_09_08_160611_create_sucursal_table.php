<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sucursal', function (Blueprint $table) {
            $table->increments('idSuc');
            $table->integer('idPais')->references('id')->on('pais');
            $table->integer('idEstado')->references('id')->on('estado');
            $table->integer('idCiudad')->references('id')->on('ciudad');
            $table->string('cp');
            $table->string('direccion');
            $table->string('IdColonia')->references('id')->on('localidad');
            $table->string('direccion');
            $table->string('localizacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
