<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\institucion;
use App\profesor;
use App\nivelinst;
use App\tutor;
use App\alumno;
use App\tutoralumno;
use Auth as At;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }
    public function showRegistrationForm2()
    {
        return view('auth.register2');
    }
    public function showRegistrationForm3()
    {
        return view('Tutor.registrarAlumno');
    }

    public function showRegistrationForm4()
    {
        return view('Admin.registro.registrarProfesor');
    }

    public function showRegistrationForm5() {

        return view('Admin.expedientes.altaAlumnos');

    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);
        if($request->admin==1)
        {
            if ($user->tipo_user==3) {
                $prof=new profesor;
                $prof->iduser=$user->id;
                $prof->correo=$user->email;
                $prof->nombreprof=$user->name;
                $prof->apepat=$request->apepat;
                $prof->apemat=$request->apemat;
                $prof->matricula=$request->matricula;
                $prof->fechanac=$request->fechanac;
                $prof->telefono=$request->tel;
                $prof->celular=$request->cel;
                $prof->f_alta=date('Y-m-d');
                $prof->genero=$request->genero;
                $iduser=Auth::user()->id;
                $inst=DB::table('institucion')->select('idInstitucion')
                ->where('id_usuario','=',$iduser)
                ->first();
                $prof->id_institucion=$inst->idInstitucion;
                $prof->save();
                return Redirect::to('/admin/adminUsuarios');
            }

            if ($user->tipo_user == 4) {

                return Redirect::to('/admin/expedientes');   

            }

        }
        if ($request->admin==2)
        {
            if($user->tipo_user==1)
            {

                $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $su = strlen($an) - 1;    
                $inst = new institucion;
                $inst->nombre=$request->name_esc;
                $inst->id_usuario=$user->id;
                $inst->fecha_alta=date("Y-m-d");
                $inst->codregistro=substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1) .
                substr($an, rand(0, $su), 1);

                $inst->save();
                foreach ($request->nivel as $key) {
                    $ni= new nivelinst;
                    $ni->idInstitucion=$inst->idInstitucion;
                    $ni->idNivel=$key;
                    $ni->save();
                }


            }
            if ($user->tipo_user==2) {
                $tutor=new tutor;
                $tutor->iduser=$user->id;
                $tutor->correo=$user->email;
                $tutor->nombreTutor=$user->name;
                $tutor->apepat=$request->apepat;
                $tutor->apemat=$request->apemat;
                $tutor->fechanac=$request->fechanac;
                $tutor->telefono=$request->tel;
                $tutor->celular=$request->cel;
                $tutor->f_alta=date('Y-m-d');            
                $tutor->genero=$request->genero;
                $tutor->save();

            }
            if ($user->tipo_user==3) {
                $prof=new profesor;
                $prof->iduser=$user->id;
                $prof->correo=$user->email;
                $prof->nombreprof=$user->name;
                $prof->apepat=$request->apepat;
                $prof->apemat=$request->apemat;
                $prof->matricula=$request->matricula;
                $prof->fechanac=$request->fechanac;
                $prof->telefono=$request->tel;
                $prof->celular=$request->cel;
                $prof->f_alta=date('Y-m-d');
                $prof->genero=$request->genero;
                $inst=DB::table('institucion')->where('codregistro','=',$request->clave)->first();
                $prof->id_institucion=$inst->idInstitucion;
                $prof->save();
                return Redirect::to('/admin/adminUsuarios');
            }
            if ($user->tipo_user==4) {
                $alum=new alumno;
                $alum->iduser=$user->id;
                $alum->correo=$user->email;
                $alum->nombreAlumno=$user->name;
                $alum->apepat=$request->apepat;
                $alum->apemat=$request->apemat;
                $alum->fechanac=$request->fechanac;
                $alum->telefono=$request->tel;
                $alum->f_alta=date('Y-m-d');
                $alum->genero=$request->genero;
                $gpo=DB::table('grupo')->where('contrasena','=',$request->clave)->first();
                $alum->idgrupo=$gpo->idgrupo;
                $alum->idinstitucion=$gpo->idInstitucion;
                $alum->matricula=$request->matricula;
                $alum->save();

                if(At::user()->tipo_user= 1 ){
                $tutal=new tutoralumno;
                $idtutor=At::user()->id;
                $tutor="-1";
                $tutal->idtutor=$tutor;
                $tutal->idalumno=$alum->idAlumno;
                $tutal->save();
                return Redirect::to('/admin/expedientes');
                }
                else{

                $tutal=new tutoralumno;
                $idtutor=At::user()->id;
                $tutor=DB::table('tutor')->where('iduser','=',$idtutor)->first();
                $tutal->idtutor=$tutor->idtutor;
                $tutal->idalumno=$alum->idAlumno;
                $tutal->save();
                }
            }
        }
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}