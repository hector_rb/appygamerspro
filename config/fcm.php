<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAuc30frI:APA91bF1RlcCx46HWg4250_UC-d2Xe7K3X9Zurwq-r5Z0-1d5mjFLmbLCtOxhLTeWIEdjhKfySv1mV2yClz1hjcfP7rFICoH95mVy3kngxJLm9aw_ZMK25_92otb_Ga3Kw6JzDRsD_JR'),
        'sender_id' => env('FCM_SENDER_ID', '798024302258'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
